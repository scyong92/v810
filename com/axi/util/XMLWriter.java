package com.axi.util;

import java.io.*;
import java.lang.reflect.*;
import java.util.*;

/**
 * This class works in a similar manner to Java serialization.  It takes a reference
 * to a class and will write its contents out along with all the entire class graph
 * that it refers to (both directly and indirectly).  Java serialization writes everything
 * out in a binary format.  This class writes everything out in an XML format.  It will
 * only write out files that implement the Serilizable interface.  It will not write
 * out variables that are declared transient.  Note that the XMLReader class requires that
 * all classes must have a default constructor in order that they can be read back in.
 * This class will write out some built in Java classes correctly even if they do not
 * have default constructors or have transient variables.  Any class derived from the
 * AbstractCollection or AbstractMap will be handled correctly.  Also the non-mutable classes Boolean,
 * Integer, Double, Float, and Integer are handled properly even though they do not
 * provide default constructors.
 *
 * @author Bill Darbie
 */
public class XMLWriter
{
  private String spaces = "";
  private Map<Object, Integer> _writtenObjects = new HashMap<Object, Integer>();
  private int _objectId;
  private Writer _os;

  /**
   * @author Bill Darbie
   */
  public XMLWriter(Writer os)
  {
    _os = os;
  }

  /**
   * @author Bill Darbie
   */
  public void writeXML(Object object) throws IOException
  {
    _os.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?>\n");
    _os.write("<!-- 5dx panel data file in XML format -->\n");

    _objectId = 0;
    _writtenObjects.clear();
    write(object, "root");
  }

  /**
   * @author Bill Darbie
   */
  private void write(Object object, String variableName) throws IOException
  {
    boolean newLine = true;
    // get the name of the class
    Class clazz = object.getClass();
    String className = clazz.getName();
    String name = XMLParseUtil.getXMLNameFromClassName(className);
    if (name == null)
      name = className;

    // figure out if the object has been written out already.  If it has
    // been written out find its unique ID, otherwise assign it a unique ID
    boolean objectAlreadyPersisted = false;
    Integer id = _writtenObjects.get(object);
    if (id == null)
    {
      objectAlreadyPersisted = false;
      ++_objectId;
      id = new Integer(_objectId);
      _writtenObjects.put(object, id);
    }
    else
    {
      objectAlreadyPersisted = true;
      newLine = false;
    }

    // write out the XML line for the object
    if (clazz.isArray())
    {
      int length = Array.getLength(object);
      _os.write("\n" + spaces + "<" + variableName + " " + XMLParseUtil.TYPE + "=\"" + name + "\" " + XMLParseUtil.NUMELEMENTS + "=\"" + length + "\" " + XMLParseUtil.ID + "=\"" + id + "\">");
    }
    else
    {
      _os.write("\n" + spaces + "<" + variableName + " " + XMLParseUtil.TYPE + "=\"" + name + "\" " + XMLParseUtil.ID + "=\"" + id + "\">");
    }

    spaces += "  ";

    // if the object has not been written out before gather all the variable information and
    // write it out
    if (objectAlreadyPersisted == false)
    {
      // if the object does not implement the Serializable interface throw an exception
      if ((object instanceof Serializable) == false)
        throw new NotSerializableException(className);

      // stop climbing up the inheritence hiearchy when you get to the Object class
      while(clazz != Object.class)
      {
        if (clazz.isArray())
        {
          // the array case is a special case - handle it here
          int length = Array.getLength(object);
          Class componentType = clazz.getComponentType();
          String origComponentName = componentType.getName();
          String componentName = XMLParseUtil.getXMLNameFromClassName(origComponentName);
          if (componentName == null)
            componentName = origComponentName;

          boolean primitive = componentType.isPrimitive();

          for(int i = 0; i < length; ++i)
          {
            Object value = Array.get(object, i);
            if (value == null)
            {
              _os.write("\n" + spaces + "<" + XMLParseUtil.ELEMENT + i + " type=\"" + componentName + "\">null</" + XMLParseUtil.ELEMENT + i + ">\n");
            }
            else
            {
              if (primitive)
                _os.write("\n" + spaces + "<" + XMLParseUtil.ELEMENT + i + " type=\"" + componentName + "\">" + value + "</" + XMLParseUtil.ELEMENT + i + ">");
              else
                write(value, XMLParseUtil.ELEMENT + i);
            }
          }
        }
        else if ((clazz == String.class) ||
                 (clazz == Integer.class) ||
                 (clazz == Float.class) ||
                 (clazz == Double.class) ||
                 (clazz == Boolean.class))
        {
          // handle Strings and other non-mutable types as if they are primitive types so the stream stays more compact
          _os.write(object.toString());
          newLine = false;
        }
        else if (clazz == AbstractCollection.class)
        {
          AbstractCollection collection = (AbstractCollection)object;
          int i = -1;
          Iterator it = collection.iterator();
          for (Object collectionObject: collection)
          {
            ++i;
            write(collectionObject, XMLParseUtil.ELEMENT + i);
          }
        }
        else if (clazz == AbstractMap.class)
        {
          AbstractMap map = (AbstractMap)object;
          for (Object key: map.keySet())
          {
            Object value = map.get(key);
            write(key, XMLParseUtil.KEY);
            write(value, XMLParseUtil.VALUE);
          }
        }
        else
        {
          // handle the non-array case here
          Field[] fields = clazz.getDeclaredFields();
          for(int i = 0; i < fields.length; ++i)
          {
            Field field = fields[i];
            // allow setting of private fields
            field.setAccessible(true);
            int modifiers = field.getModifiers();
            if ((Modifier.isTransient(modifiers) == false) &&
                (Modifier.isStatic(modifiers) == false))
            {
              String fieldName = field.getName();
              Class fieldType = field.getType();
              String origFieldTypeName = fieldType.getName();
              String fieldTypeName = XMLParseUtil.getXMLNameFromClassName(origFieldTypeName);
              if (fieldTypeName == null)
                fieldTypeName = origFieldTypeName;

              Object value = null;
              try
              {
                value = field.get(object);
              }
              catch(IllegalAccessException e)
              {
                e.printStackTrace();
              }
              if (fieldType.isPrimitive())
              {
                _os.write("\n" + spaces + "<" + fieldName + " type=\"" + fieldTypeName + "\">" + value + "</" + fieldName + ">");
              }
              else if (value == null)
              {
                _os.write("\n" + spaces + "<" + fieldName + " type=\"" + fieldTypeName + "\">null</" + fieldName + ">");
              }
              else
                write(value, fieldName);
            }
          }
        }
        clazz = clazz.getSuperclass();
      }
    }
    spaces = spaces.substring(2, spaces.length());
    if (newLine)
      _os.write("\n" + spaces + "</" + variableName + ">");
    else
    {
      _os.write("</" + variableName + ">");
      newLine = true;
    }
  }

  /**
   * @author Bill Darbie
   */
  public void flush() throws IOException
  {
    _os.flush();
  }

  /**
   * @author Bill Darbie
   */
  public void close() throws IOException
  {
    _os.close();
  }
}
