/**
 * This is the source file for FileUtil JNI layer
 * @author George A. David
 */
#ifdef _MSC_VER
  #pragma warning(disable: 4786)
#endif

#include <windows.h>

#include "cpp/util/src/sptAssert.h"
#include "JniUtil.h"

#include "com_axi_util_FileUtil.h"

using namespace std;


/**
 * get the free space of the path
 * 
 * @param path the path to determine the available space
 * @author George A. David
 */
JNIEXPORT jlong JNICALL 
Java_com_axi_util_FileUtil_nativeGetAvailableDiskSpaceInBytes(JNIEnv* pEnv, jclass object, jstring path)
{
  try
  {
    ULARGE_INTEGER freeBytesAvailable;
    ULARGE_INTEGER totalNumberOfBytes;
    ULARGE_INTEGER totalNumberOfFreeBytes;
    
    GetDiskFreeSpaceEx(getSTLString(pEnv, path).c_str(), &freeBytesAvailable, &totalNumberOfBytes, &totalNumberOfFreeBytes);
    
    return (((jlong)freeBytesAvailable.HighPart) << 32L) | (jlong)freeBytesAvailable.LowPart;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

