package com.axi.util;

/**
 * @author Chong, Wei Chin
 */
public class DongleInitialiseException extends LocalizedException
{
  /**
   * @author Chong, Wei Chin
   */
  public DongleInitialiseException(String exception)
  {
    super(new LocalizedString("BUS_SOFTWARE_LICENSE_EXCEPTION_KEY",new Object[]{exception}) );
  }
}