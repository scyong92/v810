package com.axi.util;

/**
 * This class is thrown when attempting to
 * retrieve from a collection an entry that
 * does not exist.
 *
 * @author George A. David
 */
public class EntryNotFoundException extends Exception
{
  private String _entry;

  /**
   * @author George A. David
   */
  public EntryNotFoundException(String entry)
  {
    super("The " + entry + " was not found in the Java collection as expected");
    Assert.expect(entry != null);
    Assert.expect(entry.length() > 0);
    _entry = entry;
  }

  /**
   * @author Bill Darbie
   */
  public String getEntry()
  {
    return _entry;
  }
}
