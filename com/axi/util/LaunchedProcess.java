package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * @author Roy Williams
 */
public class LaunchedProcess
{
  private Process _process = null;
  private BufferedReader _processStdout = null;
  private StreamHandler _outputStreamHandler = null;
  private StreamHandler _errorStreamHandler = null;

  static private long _WAIT_TILL_PROCESS_RUNNING_TIMEOUT = 250; // milliseconds

  /**
   * @author Roy Williams
   * @author Bob Balliew
   */
  public LaunchedProcess(String command,
                         String envp[],
                         File baseDirectory,
                         boolean combineStdoutWithStderr,
                         PrintWriter printWriter) throws IOException
  {
    Assert.expect(command != null);
    Assert.expect(baseDirectory.exists());

    // printWriter == null is okay => sending output to /dev/null
    // Expect.expect(envp != null);  // null is okay - indicates not changed to Runtime object

    _process = Runtime.getRuntime().exec(command, envp, baseDirectory);

    if (combineStdoutWithStderr)
    {
      // Read stdout from the child process and write to printWriter from this UnitTest.
      _outputStreamHandler = new StreamHandler(this, _process.getInputStream(), printWriter);
    }
    else
    {
      _processStdout = new BufferedReader(new InputStreamReader(_process.getInputStream()));
    }

    // Read stderr from the child process and write to printWriter from this UnitTest.
    _errorStreamHandler = new StreamHandler(this, _process.getErrorStream(), printWriter);

    // Give the launched process a chance to start up.
    try
    {
      Thread.sleep(_WAIT_TILL_PROCESS_RUNNING_TIMEOUT);
    }
    catch (InterruptedException e)
    {
      Assert.expect(false);
    }

    // Complain if this process has already terminated.
    try
    {
      throw new IOException("launch of " + command + " unexpectedly terminated with an exit value of " + _process.exitValue());
    }
    catch (IllegalThreadStateException e)
    {
      // Actually, this is good!  It tells us that the Process has not died.
    }
  }

  /**
   * @author Greg Esparza
   */
  public LaunchedProcess(String command, List<String> environmentVariables, File baseDirectory) throws IOException
  {
    Assert.expect(command != null);
    Assert.expect(baseDirectory.exists());

    String[] env = new String[environmentVariables.size()];
    environmentVariables.toArray(env);
    _process = Runtime.getRuntime().exec(command, env, baseDirectory);

    // Set the print writer to null so input and error streams will write to dev/null
    PrintWriter printWriter = null;

    // Read stdout from the child process and write to printWriter fromt this UnitTest.
    _outputStreamHandler = new StreamHandler(this, _process.getInputStream(), printWriter);

    // Read stderr from the child process and write to printWriter fromt this UnitTest.
    _errorStreamHandler = new StreamHandler(this, _process.getErrorStream(), printWriter);

    // Give the launched process a chance to start up.
    try
    {
      Thread.sleep(_WAIT_TILL_PROCESS_RUNNING_TIMEOUT);
    }
    catch (InterruptedException e)
    {
      Assert.expect(false);
    }

    // Complain if this process has already terminated.
    try
    {
      throw new IOException("launch of " + command + " unexpectedly terminated with an exit value of " + _process.exitValue());
    }
    catch (IllegalThreadStateException e)
    {
      // Actually, this is good!  It tells us that the Process has not died.
    }
  }

  /**
   * @author Roy Williams
   */
  public void closingStreamHandler(StreamHandler streamHandler)
  {
    Assert.expect(streamHandler != null);
    if (_errorStreamHandler == streamHandler)
      _errorStreamHandler = null;
    if (_outputStreamHandler == streamHandler)
      _outputStreamHandler = null;
  }

  /**
   * If the caller provides a waitForFlushMilliSeconds then the running process
   * will be allowed to continue execution till the time expires or there is
   * no activity to the standard output for the wait duration of 500 milliseconds.
   *
   * @author Roy Williams
   */
  public synchronized void destroy(int waitTimeout, int waitForFlushMilliSeconds)
  {
    if (_outputStreamHandler != null)
    {
      long lastOutputStreamActiveTraffic = _outputStreamHandler.lastActiveTraffic();
      long currentOutputStreamActiveTraffic = 0;

      while (waitForFlushMilliSeconds > 0)
      {
        try
        {
          wait(waitTimeout);
          currentOutputStreamActiveTraffic = _outputStreamHandler.lastActiveTraffic();
        }
        catch (InterruptedException e)
        {
          Assert.expect(false);
        }

        waitForFlushMilliSeconds -= waitTimeout;
        lastOutputStreamActiveTraffic = currentOutputStreamActiveTraffic;

        if ((_errorStreamHandler == null) || (currentOutputStreamActiveTraffic <= lastOutputStreamActiveTraffic))
        {
          waitForFlushMilliSeconds = 0;
          break;
        }
      }
    }

    _process.destroy();

    try
    {
      if (_processStdout != null)
        _processStdout.close();
    }
    catch (IOException ioe)
    {
      // do nothing
    }

    if (_outputStreamHandler != null)
      _outputStreamHandler.close();

    if (_errorStreamHandler != null)
      _errorStreamHandler.close();
  }

  /**
   * @author Roy Williams
   */
  public static void setWaitTillProcessRunningTimeout(long time)
  {
    Assert.expect(time > 0);
    _WAIT_TILL_PROCESS_RUNNING_TIMEOUT = time;
  }

  /**
   * @author Bob Balliew
   */
  public BufferedReader getStdoutReader()
  {
    return _processStdout;
  }
}
