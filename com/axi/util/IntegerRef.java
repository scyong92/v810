package com.axi.util;

import java.io.*;

/**
* IntegerRef allows ints to be passed by reference.  An IntegerRef
* can be passed to a method and have its value changed by the method that
* it was passed into.
* This class is needed because the java.lang.Integer class does not provide
* a set method.
*
* @author Bill Darbie
*/
public class IntegerRef implements Serializable
{
  private int _value;

  /**
  * Create an IntegerRef with a value of 0.
  *
  * @author Bill Darbie
  */
  public IntegerRef()
  {
    _value = 0;
  }

  /**
  * Create an IntegerRef with the value passed in.
  *
  * @param value is the integer value that this class contains
  * @author Bill darbie
  */
  public IntegerRef(int value)
  {
    _value = value;
  }

  /**
  * @author Bill darbie
  */
  public IntegerRef(IntegerRef integerRef)
  {
    Assert.expect(integerRef != null);

    _value = integerRef._value;
  }


  /**
  * Set this class to contain an integer with the value that is passed in.
  *
  * @param value is the value that this class should contain
  * @author Bill Darbie
  */
  public void setValue(int value)
  {
    _value = value;
  }

  /**
  * Get an int representation of the integer contained in this class
  *
  * @return the int contained in this class
  * @author Bill Darbie
  */
  public int getValue()
  {
    return _value;
  }

  /**
   * @author Bill Darbie
   */
  public boolean equals(Object rhs)
  {
    if (rhs == this)
      return true;

    if (rhs instanceof IntegerRef)
    {
      IntegerRef integerRef = (IntegerRef)rhs;
      if (_value == integerRef._value)
        return true;
    }

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public int hashCode()
  {
    return _value;
  }

  /**
   * @author Peter Esbensen
   */
  public void increment()
  {
    ++_value;
  }

}
