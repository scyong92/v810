#include "JniUtil.h"
#include "util/src/sptAssert.h"
#include "util/src/Ping.h"

#include "com_axi_util_ICMPPacketUtil.h"

using namespace std;

/*
 * Class:     com_axi_util_ICMPPacketUtil
 * Method:    nativeGetMTUSize
 * Signature: (Ljava/lang/String;Ljava/lang/String;)I
 * Author:    Rex Shang
 */
JNIEXPORT jint JNICALL Java_com_axi_util_ICMPPacketUtil_nativeGetMTUSize
  (JNIEnv *pEnv, jobject object, jstring localIpAddress, jstring remoteIpAddress)
{
  try
  {
    Ping* pingUtil = NULL;
    string remoteAddress = getSTLString(pEnv, remoteIpAddress);

    if (localIpAddress != NULL)
      pingUtil = new Ping(remoteAddress, getSTLString(pEnv, localIpAddress));
    else
      pingUtil = new Ping(remoteAddress);

    int mtuSize = pingUtil->measureMTU();

    delete pingUtil;
    pingUtil = NULL;

    return mtuSize;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_util_ICMPPacketUtil
 * Method:    nativeCanHostTransmitPacketSize
 * Signature: (Ljava/lang/String;IZ)Z
 * Author:    Rex Shang
 */
JNIEXPORT jboolean JNICALL Java_com_axi_util_ICMPPacketUtil_nativeCanHostTransmitPacketSize
  (JNIEnv *pEnv, jobject object, jstring ipAddress, jint packetSize, jboolean okToFragment)
{
  jboolean isOk = false;
  
  try
  {
    Ping* pingUtil = new Ping(getSTLString(pEnv, ipAddress));
    int error = pingUtil->ping(packetSize, okToFragment);
    if (error == 0)
      isOk = true;

    delete pingUtil;
    pingUtil = NULL;

    return isOk;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return isOk;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return isOk;
  }
}
