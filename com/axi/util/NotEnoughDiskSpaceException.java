package com.axi.util;

import java.io.*;

/**
 * Throw this class instead of java.io.IOException because this one
 * provides a getFileName() method.
 *
 * @author Laura Cormos
 */
public class NotEnoughDiskSpaceException extends IOException
{
  private String _baseFileName;
  private String _parentPath;
  private String _fileName;

  /**
   * @author Laura COrmos
   */
  public NotEnoughDiskSpaceException(String parentPath, String baseFileName)
  {
    super("There is not enough disk space in " + parentPath + " to write file " + baseFileName + ".");
    Assert.expect(baseFileName != null);
    Assert.expect(parentPath != null);

    _fileName = parentPath + baseFileName;
    _baseFileName = baseFileName;
    _parentPath = parentPath;
  }

  /**
   * @author Laura Cormos
   */
  public String getFileName()
  {
    return _fileName;
  }

  /**
   * @author Laura Cormos
   */
  public String getParentPath()
  {
    return _parentPath;
  }

  /**
   * @author Laura Cormos
   */
  public String getBaseFileName()
  {
    return _baseFileName;
  }
}
