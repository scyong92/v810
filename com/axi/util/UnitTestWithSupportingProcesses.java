package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * @author Roy Williams
 */
public abstract class UnitTestWithSupportingProcesses extends UnitTest
{
  private List<LaunchedProcess> _processes = new ArrayList<LaunchedProcess>();

  /**
   * Launch a program specified by the command.  The program must (obviously) be
   * in your PATH.
   * <p>
   * The user may provide an environment (envp) for the command to inherit.  The
   * environment comes in the form of an array of "key=value" entries.   It must
   * be comprehensive.  That is, not just the ones you wish to add.  A null value
   * in this position is also legal.  It tells the Runtime object to use the current,
   * unchanged environment.
   *
   * @author Roy Williams
   */
  protected synchronized BufferedReader launch(String command,
                                               String envp[],
                                               File baseDirectory,
                                               boolean combineStdoutWithStderr,
                                               PrintWriter printWriter) throws IOException
  {
    Assert.expect(command != null);
    Assert.expect(envp != null);
    Assert.expect(baseDirectory != null);
    Assert.expect(printWriter != null);

    // Launch the process.
    LaunchedProcess process = new LaunchedProcess(command, envp, baseDirectory, combineStdoutWithStderr, printWriter);
    _processes.add(process);
    return process.getStdoutReader();
  }

  /**
   * As each process completes, it will call into have itself removed.  The
   * caller is the stdout and stderr streams.   Thus, there will be more than one
   * caller.  Thus, _processes.size() will go to zero when the first of these two
   * connections is closed.   Be prepared to handle the null pointer case.
   *
   * @author Roy Williams
   */
  private synchronized void terminated(LaunchedProcess process)
  {
    Assert.expect(process != null);
    _processes.remove(process);
  }

  /**
   * @author Roy Williams
   */
  protected String[] getUnitTestSystemEnvironment()
  {
    Map<String, String> unitTestSystemEnvironmentMap = new HashMap<String, String>(System.getenv());
    Vector<String> environment = new Vector<String>();
    for (Map.Entry<String, String> entry : unitTestSystemEnvironmentMap.entrySet())
    {
      String key = entry.getKey();
      String value = entry.getValue();
      environment.add(key + "=" + value);
    }

    String[] env = new String[environment.size()];
    return environment.toArray(env);
  }

  /**
   * @author Roy Williams
   */
  protected synchronized int processCount()
  {
    return _processes.size();
  }

  /**
   * If (and only if) there were launched processes, all will be terminated
   * at the end of the execute method of this class.
   *
   * @author Roy Williams
   */
  protected synchronized void terminateAllLaunchedProcesses(int waitForFlushMilliSeconds)
  {
    ArrayList<LaunchedProcess> destroyList = new ArrayList<LaunchedProcess>(_processes);
    for (LaunchedProcess process : destroyList)
    {
      // inner try/catch block to insure that each process launched is killed.
      try
      {
        process.destroy(500, waitForFlushMilliSeconds);
      }
      catch (Exception e)
      {
        // do nothing
      }
      finally
      {
        _processes.remove(process);
      }
    }
  }

}
