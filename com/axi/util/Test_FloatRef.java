package com.axi.util;

import java.io.*;

/**
 * Test class for FloatRef.
 *
 * @author Matt Wharton
 */
public class Test_FloatRef extends UnitTest
{
  /**
   * @author Matt Wharton
   */
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_FloatRef());
  }

  /**
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    FloatRef floatRef = new FloatRef();

    // check that the default value of FloatRef is 0.0
    Expect.expect(floatRef.getValue() == 0.0f);

    // now test the constructor
    floatRef = new FloatRef(2.2f);
    Expect.expect(floatRef.getValue() == 2.2f);

    // now test the setValue() method
    floatRef.setValue(-10.0f);
    Expect.expect(floatRef.getValue() == -10.0f);
  }
}
