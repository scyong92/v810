package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * @author Patrick Lacz
 */
public class Test_StatisticsUtil extends UnitTest
{
  /**
   * @author Patrick Lacz
   */
  private Test_StatisticsUtil()
  {
    // do nothing
  }

  /**
   * @author Patrick Lacz
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_StatisticsUtil());
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @author Patrick Lacz
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testInvalidInputs();
    testNorms();
    testConvergeOnMean();
    testMoments();
    testPercentile();
    testQuartileRanges();
    //Lim, Lay Ngor - XCR1743:Benchmark
    testConstraintInput();
    //Ngie Xing - XCR-2027 
    testExcludeOutliers();
  }

  /**
   * @author Peter Esbensen
   */
  private void testQuartileRanges()
  {
    // these values might seem a little weird but they are by design -- see the javadoc for the quartileRanges function
    float[] fiveItemTestArray = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f };
    float[] quartileRanges = StatisticsUtil.quartileRanges(fiveItemTestArray);
    Expect.expect( Arrays.equals(quartileRanges, new float[] { 1.0f, 1.5f, 3.0f, 4.5f, 5.0f } ) );

    float[] fourItemTestArray = { 1.0f, 2.0f, 3.0f, 4.0f };
    quartileRanges = StatisticsUtil.quartileRanges(fourItemTestArray);
    Expect.expect( Arrays.equals(quartileRanges, new float[] { 1.0f, 1.5f, 2.5f, 3.5f, 4.0f }) );

    float[] threeItemTestArray = { 1.0f, 2.0f, 3.0f };
    quartileRanges = StatisticsUtil.quartileRanges(threeItemTestArray);
    Expect.expect( Arrays.equals(quartileRanges, new float[] { 1f, 1f, 2f, 3f, 3f }) );

    float[] twoItemTestArray = { 1f, 2f };
    quartileRanges = StatisticsUtil.quartileRanges(twoItemTestArray);
    Expect.expect( Arrays.equals(quartileRanges, new float[] { 1f, 1f, 1.5f, 2f, 2f }) );

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        float[] oneItemTestArray = { 1f };
        StatisticsUtil.quartileRanges(oneItemTestArray);
      }
    });
  }


  /**
   * @author Peter Esbensen
   */
  private void testMoments()
  {
    // test bad inputs
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        StatisticsUtil.getCentralMoment(null, 0);
      }
    });
    Expect.expectAssert(new RunnableWithExceptions()
        {
          public void run()
          {
            StatisticsUtil.getCentroid(null);
          }
    });
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        StatisticsUtil.getRawMoment(null, 0);
      }
    });
    final float[] array = new float[] { 0, 0, 1, 3, 1 };
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        StatisticsUtil.getCentralMoment(array, -1);
      }
    });
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        StatisticsUtil.getRawMoment(array, -1);
      }
    });

    // test zeroth moments
    float centralMoment = StatisticsUtil.getCentralMoment(array, 0);
    Expect.expect(MathUtil.fuzzyEquals(centralMoment, 5.0));
    float rawMoment = StatisticsUtil.getRawMoment(array, 0);
    Expect.expect(MathUtil.fuzzyEquals(rawMoment, 5.0));
    float centroid = StatisticsUtil.getCentroid(array);
    Expect.expect(MathUtil.fuzzyEquals(centroid, 3.0));

    // test first moments
    centralMoment = StatisticsUtil.getCentralMoment(array, 1);
    Expect.expect(MathUtil.fuzzyEquals(centralMoment, 0.0));
    rawMoment = StatisticsUtil.getRawMoment(array, 1);
    Expect.expect(MathUtil.fuzzyEquals(rawMoment, 15.0));

    // test second moments
    centralMoment = StatisticsUtil.getCentralMoment(array, 2);
    Expect.expect(MathUtil.fuzzyEquals(centralMoment, 2.0));
    rawMoment = StatisticsUtil.getRawMoment(array, 2);
    Expect.expect(MathUtil.fuzzyEquals(rawMoment, 47.0));
  }

  /**
   * @author Patrick Lacz
   */
  public void testNorms()
  {
    float array1D[] = new float[50];
    for (int i = 0 ; i < array1D.length ; ++i)
      array1D[i] = i*i;

    float array3D[][] = new float[80][3];
    for (int i = 0 ; i < array3D.length ; ++i)
    {
      array3D[i][0] = i*i;
      array3D[i][1] = (float)Math.sin((i/20.0) * Math.PI);
      array3D[i][2] = (float)Math.cos((i/60.0) * Math.PI);
    }

    float uniformArray1D[] = new float[30];
    Arrays.fill(uniformArray1D, 10.0f);

    float mean = StatisticsUtil.mean(array1D);
    Expect.expect(Math.abs(mean - 808.5) < 0.00001);
    float stdDev = StatisticsUtil.standardDeviation(array1D, mean);
    Expect.expect(Math.abs(stdDev - 738.6288) < 0.001);

    float result1D[] = StatisticsUtil.norm(array1D, mean);
    Expect.expect(result1D != null);

    float mean3D[] = StatisticsUtil.mean(array3D);
    Expect.expect(mean3D != null);
    float expectedMean[] = {2093.5f, 0.0f, -0.197326f};
    for (int d = 0 ; d < mean3D.length ; ++d)
      Expect.expect(Math.abs(mean3D[d] - expectedMean[d]) < Math.max(expectedMean[d]*0.0001,0.000001));

    float stdDev3D[] = StatisticsUtil.standardDeviation(array3D, mean3D);
    Expect.expect(stdDev3D != null);
    float expectedStdDev[] = {1897.47f, 0.711568f, 0.7238363f};
    for (int d = 0 ; d < stdDev3D.length ; ++d)
      Expect.expect(Math.abs(stdDev3D[d] - expectedStdDev[d]) < expectedStdDev[d]*0.0001);

    float stdUniform1D[] = StatisticsUtil.normMahalanobis(uniformArray1D, 10.0f);
    Expect.expect(ArrayUtil.max(stdUniform1D) == ArrayUtil.min(stdUniform1D));
    Expect.expect(MathUtil.fuzzyEquals(stdUniform1D[0], 0.0f));

    float result3D[] = StatisticsUtil.normEuclidian(array3D, mean3D);
    Expect.expect(result3D != null);

    Set<Integer> validSet = StatisticsUtil.convergeOnValidSet(array3D, StatisticsNormEnum.EUCLIDIAN, 900.5f);
    Expect.expect(validSet != null);
    Expect.expect(validSet.size() == 19);

    float mean3D_2[] = StatisticsUtil.convergeOnMean(array3D, 5.0f, 2.0f);
    Expect.expect(mean3D_2 != null);

   Set<Integer> valid1DSet = StatisticsUtil.convergeOnValidSet(array1D, StatisticsNormEnum.EUCLIDIAN, 900.5f);
    Expect.expect(validSet != null);
  }

  /**
   * Utility function to avoid structred data.
   * @author Patrick Lacz
   */
  public float[] shuffleList(float[] listToShuffle, long seed)
  {
    List<Float> valueList = new LinkedList<Float>();
    for (float value : listToShuffle)
      valueList.add(value);

    Collections.shuffle(valueList, new Random(seed));

    float shuffledList[] = new float[listToShuffle.length];
    int i = 0;
    for (float value : valueList)
    {
      shuffledList[i] = value;
      ++i;
    }
    return shuffledList;
  }

  /**
   * @author Patrick Lacz
   */
  public void testConvergeOnMean()
  {
    // Two Towers Data: two repeated data points of the same height
    float twoTowersData[] = new float[200];
    for (int i = 0 ; i < 100 ; ++i)
      twoTowersData[i] = 40.0f;
    for (int i = 100 ; i < 200 ; ++i)
      twoTowersData[i] = 60.0f;

    float mean1d = StatisticsUtil.convergeOnMean(twoTowersData, 0.0001f, 1.0f);
    Expect.expect(Math.abs(mean1d - StatisticsUtil.mean(twoTowersData)) < 0.0001);

    mean1d = StatisticsUtil.convergeOnMean(shuffleList(twoTowersData, 54321), 0.0001f, 1.0f);
    Expect.expect(Math.abs(mean1d - StatisticsUtil.mean(twoTowersData)) < 0.0001);


    // Heel-toe distrubted data
    //   ,.
    // _/##\___/\_
    float heelMean = 5.0f;
    float heelToeData[] = createHeelToeDistribution(
      500,  // number of samples
      heelMean,  // heel mean
      1.5f,  // heel deviation
      15.0f, // toe mean
      0.5f,  // toe deviation
      0.2f, // toe size (% of total)
      54231); // seed

    float mean_using_1_0 = StatisticsUtil.convergeOnMean(heelToeData, 0.0001f, 1.0f);
    //Expect.expect(Math.abs(mean_using_1_0 - heelMean) < 0.0001);

    mean1d = StatisticsUtil.convergeOnMean(shuffleList(heelToeData, 666444), 0.0001f, 1.0f);
    Expect.expect(mean1d == mean_using_1_0);

    // don't attenuate outliers as much
    mean1d = StatisticsUtil.convergeOnMean(shuffleList(heelToeData, 121212), 0.0001f, 0.5f);
    Expect.expect(mean1d >= mean_using_1_0);

    // attenuate outliers more
    mean1d = StatisticsUtil.convergeOnMean(shuffleList(heelToeData, 212121), 0.0001f, 2.f);
    Expect.expect(mean1d <= mean_using_1_0);
    Expect.expect(mean1d >= heelMean);

  }

  /**
   *  Heel-toe distrubted data
   *     ,.
   *   _/##\___/\_
   * @author Patrick Lacz
   */
  private float[] createHeelToeDistribution(int numberOfSamples, float heelMean, float heelDeviation, float toeMean, float toeDeviation, float toeSize, long seed)
  {
    float heelToeData[] = new float[numberOfSamples];
    Random random = new Random(seed);

    int numberOfHeelSamples = (int)Math.floor(numberOfSamples*(1.f-toeSize));
    for (int i = 0 ; i < numberOfHeelSamples ; ++i)
    {
      float value = (float)random.nextGaussian() * heelDeviation + heelMean;
      heelToeData[i] = value;
    }
    for (int i = numberOfHeelSamples ; i < numberOfSamples ; ++i)
    {
      float value = (float)random.nextGaussian() * toeDeviation + toeMean;
      heelToeData[i] = value;
    }

    return heelToeData;
  }

  /**
   * @author Patrick Lacz
   */
  public void testInvalidInputs()
  {
    // test null parameters

    // Mean
    try {
      StatisticsUtil.mean((float[])null);
      Expect.expect(false);
    } catch (AssertException e) {  }
    try {
      StatisticsUtil.mean((float[][])null);
      Expect.expect(false);
    } catch (AssertException e) {  }

    // Median
    try {
      StatisticsUtil.median((float[])null);
      Expect.expect(false);
    } catch (AssertException e) {  }
    try {
      StatisticsUtil.median((float[][])null);
      Expect.expect(false);
    } catch (AssertException e) {  }

    // Quartile Ranges
    try {
      StatisticsUtil.quartileRanges((float[])null);
      Expect.expect(false);
    } catch (AssertException e) {  }
    try {
      StatisticsUtil.quartileRanges((float[][])null);
      Expect.expect(false);
    } catch (AssertException e) {  }
    try {
      float tooSmallArray[] = { 1.f };
      StatisticsUtil.quartileRanges(tooSmallArray);
      Expect.expect(false);
    } catch (AssertException e) {  }

    // Standard Deviation
    try {
      StatisticsUtil.standardDeviation((float[])null);
      Expect.expect(false);
    } catch (AssertException e) {  }
    try {
      StatisticsUtil.standardDeviation((float[])null, 0.f);
      Expect.expect(false);
    } catch (AssertException e) {  }
    try {
      StatisticsUtil.standardDeviation((float[][])null, (float[])null);
      Expect.expect(false);
    } catch (AssertException e) {  }
    try {
      float tooSmallArray[] = { 1.f };
      StatisticsUtil.standardDeviation(tooSmallArray);
      Expect.expect(false);
    } catch (AssertException e) {  }
    try {
      float tooSmallArray[] = { 1.f };
      StatisticsUtil.standardDeviation(tooSmallArray, 0.f);
      Expect.expect(false);
    } catch (AssertException e) {  }
  }

  /**
   * @author Matt Wharton
   */
  private void testPercentile()
  {
    // Test asserts.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        StatisticsUtil.percentile(null, 0.5f);
      }
    });

    // Test out of bounds percentiles.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        StatisticsUtil.percentile(new float[5], -0.2f);
      }
    });

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        StatisticsUtil.percentile(null, 2f);
      }
    });

    // Test the happy path.
    float[] testData = { 95.1772f, 95.1567f, 95.1937f, 95.1959f, 95.1442f, 95.0610f,
        95.1591f, 95.1195f, 95.1065f, 95.0925f, 95.1990f, 95.1682f };
    float estimatedPercentile = StatisticsUtil.percentile(testData, 0.90f);
    Expect.expect(MathUtil.fuzzyEquals(estimatedPercentile, 95.19807f));
  }
  
 /**
   * @author Lim, Lay Ngor
   */
  public void testConstraintInput()
  {
    // test 0  parameters
    float array1D[] = new float[50];
    for (int i = 0 ; i < array1D.length ; ++i)
      array1D[i] = i*i;
    
    final int startAtFirstIndex = 0;    
    final int startAtMiddleIndex = 25;
    final int endAtMiddletIndex = 25;
    final int endAtLastIndex = array1D.length;//final index non-inclusive for mean, max & min.
    
    // Mean
    try {
      float mean = StatisticsUtil.mean(array1D, startAtFirstIndex, endAtMiddletIndex);
      Expect.expect(mean == 196.f);
    } catch (AssertException e) {  }
    try {
      float mean = StatisticsUtil.mean(array1D, startAtMiddleIndex, endAtLastIndex);
      Expect.expect(MathUtil.fuzzyEquals(mean, 1421.f));
    } catch (AssertException e) {  }
    try {
      float mean = StatisticsUtil.mean(array1D, startAtFirstIndex, endAtLastIndex);
      Expect.expect(MathUtil.fuzzyEquals(mean, 808.5f));
    } catch (AssertException e) {  }    

    // Max
    try {
      float max = StatisticsUtil.maximum(array1D, startAtFirstIndex, endAtMiddletIndex);
      Expect.expect(max == 576.f);
    } catch (AssertException e) {  }
    try {
      float max = StatisticsUtil.maximum(array1D, startAtMiddleIndex, endAtLastIndex);
      Expect.expect(max == 2401.f);
    } catch (AssertException e) {  }
    try {
      float max = StatisticsUtil.maximum(array1D, startAtFirstIndex, endAtLastIndex);
      Expect.expect(max == 2401.f);
    } catch (AssertException e) {  }    

    // Min
    try {
      float min = StatisticsUtil.minimum(array1D, startAtFirstIndex, endAtMiddletIndex);
      Expect.expect(min == 0.f);
    } catch (AssertException e) {  }
    try {
      float min = StatisticsUtil.minimum(array1D, startAtMiddleIndex, endAtLastIndex);
      Expect.expect(min == 625.f);
    } catch (AssertException e) {  }
    try {
      float min = StatisticsUtil.minimum(array1D, startAtFirstIndex, endAtLastIndex);
      Expect.expect(min == 0.f);
    } catch (AssertException e) {  }
  }  
  
  /**
   * @author Ngie Xing
   */
  private void testExcludeOutliers()
  {
    //Sample from http://www.wikihow.com/Calculate-Outliers
    float array[] = {69, 69, 70, 70, 70, 70, 71, 71, 71, 72, 73, 300};

    try
    {
      //Validate Outliers Outer Fences 
      float outerFences[] = StatisticsUtil.getOutliersOuterFences(array);
      Expect.expect(outerFences[0] == 65.5);
      Expect.expect(outerFences[1] == 76);
    } catch (AssertException e){}
    try
    {
      //Validate Array Outliers Outer Fences 
      float expectedResult[] = {69, 69, 70, 70, 70, 70, 71, 71, 71, 72, 73};
      float arrayWithoutOutliers[] = StatisticsUtil.getExcludedOutliersArray(array);
      Expect.expect(Arrays.equals(arrayWithoutOutliers, expectedResult));
    } catch (AssertException e){}
    try
    {
      Set<Integer> outlier = StatisticsUtil.detectOutlierIndex(array);
      Expect.expect(outlier.contains(11)); //element value = 300
      //Expect.expect(array[outlier[0]] == 300); //element value = 300
    } catch (AssertException e){}
    try
    {
      float arrayForTest[] = {69, 69, 70, 70, 70, 70, 300, 71, 71, 72, 73, 71};
      float outlier = StatisticsUtil.meanExcludeOutlier(arrayForTest, 1, 11);
      //total = 636
      //numberOfData = 9  
      //mean => 70.66666666666667
      Expect.expect(MathUtil.fuzzyEquals(outlier, (double)70.66666666666667) );
    } catch (AssertException e){}
    try
    {
      float medianExcludedOutliers = StatisticsUtil.medianExcludedOutliers(array);
      Expect.expect(medianExcludedOutliers == 70);
    } catch (AssertException e){}
    try
    {
      float expectedResult[] = {69, 70, 70, 71, 73}; //{Q0,Q1,Q2,Q3,Q4}
      float quartileRangesExcludedOutliers[] = StatisticsUtil.quartileRangesExcludedOutliers(array);
      Expect.expect(Arrays.equals(quartileRangesExcludedOutliers, expectedResult) );
    } catch (AssertException e){}
    try
    {
      float percentileExcludedOutliers = StatisticsUtil.percentileExcludedOutliers(array, 0.9f);
      Expect.expect(percentileExcludedOutliers == 72.8f);
    } catch (AssertException e){}
  }
}
