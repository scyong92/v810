package com.axi.util;

import java.util.*;


/**
 * @author George A. David
 */
public class DoubleCoordinateDistanceComparator implements Comparator<DoubleCoordinate>
{
  DoubleCoordinate _referencePoint;

  /**
   * @author George A. David
   */
  public DoubleCoordinateDistanceComparator(DoubleCoordinate referencePoint)
  {
    Assert.expect(referencePoint != null);

    _referencePoint = referencePoint;

  }

  /**
   * @author George A. David
   */
  public int compare(DoubleCoordinate lhs, DoubleCoordinate rhs)
  {
    Assert.expect(_referencePoint != null);

    double firstDistance = MathUtil.calculateDistanceSquared(lhs.getX(), lhs.getY(), _referencePoint.getX(), _referencePoint.getY());
    double secondDistance = MathUtil.calculateDistanceSquared(rhs.getX(), rhs.getY(), _referencePoint.getX(), _referencePoint.getY());

    if(firstDistance < secondDistance)
      return -1;
    if(firstDistance > secondDistance)
      return 1;
    else
      return 0;
  }
}
