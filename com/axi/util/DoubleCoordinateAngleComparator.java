package com.axi.util;

import java.util.*;


/**
 * @author George A. David
 */
public class DoubleCoordinateAngleComparator implements Comparator<DoubleCoordinate>
{
  DoubleCoordinate _referencePoint;

  /**
   * @author George A. David
   */
  public DoubleCoordinateAngleComparator(DoubleCoordinate referencePoint)
  {
    Assert.expect(referencePoint != null);

    _referencePoint = referencePoint;

  }

  /**
   * @author George A. David
   */
  public int compare(DoubleCoordinate lhs, DoubleCoordinate rhs)
  {
    Assert.expect(_referencePoint != null);

    double firstAngle = Math.atan2(lhs.getY() - _referencePoint.getY(), lhs.getX() - _referencePoint.getX());
    double secondAngle = Math.atan2(rhs.getY() - _referencePoint.getY(), rhs.getX() - _referencePoint.getX());

    if(firstAngle < secondAngle)
      return -1;
    if(firstAngle > secondAngle)
      return 1;
    else
      return 0;
  }
}
