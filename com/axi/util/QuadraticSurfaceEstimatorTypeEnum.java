package com.axi.util;

/**
 * Enumeration of surface fit types available from with QuadraticSurfaceEstimator.java
 *
 * Copyright (c) 2008, Agilent Technologies Inc., All rights reserved.
 * @author John Heumann
 */
public class QuadraticSurfaceEstimatorTypeEnum extends Enum
{
  private static int _index = -1;

  public static final QuadraticSurfaceEstimatorTypeEnum BILINEAR = new QuadraticSurfaceEstimatorTypeEnum(++_index);
  public static final QuadraticSurfaceEstimatorTypeEnum LINEAR_X_QUADRATIC_Y = new QuadraticSurfaceEstimatorTypeEnum(++_index);
  public static final QuadraticSurfaceEstimatorTypeEnum QUADRATIC= new QuadraticSurfaceEstimatorTypeEnum(++_index);

  /**
   * @author John Heumann
   */
  private QuadraticSurfaceEstimatorTypeEnum(int id)
  {
    super(id);
  }

}
