package com.axi.util;

import java.io.*;
import java.util.*;
import java.util.zip.*;
import javax.swing.*;
import javax.swing.table.*;

public class JTableToTextFileExporter
{
  private boolean _addChecksum = false;
  private Adler32 _checksumUtil = new Adler32();
  private PrintWriter _os = null;

  /**
   * @author Erica Wheatcroft
   */
  public JTableToTextFileExporter()
  {
    // do nothing
  }

  /**
   * @author Erica Wheatcroft
   */
  private void write(String stringToPrint)
  {
    Assert.expect(stringToPrint != null);

    if(_addChecksum)
      _checksumUtil.update(stringToPrint.getBytes());

    _os.print(stringToPrint);
  }

  /**
   * Given a table, write out the scan path definition defined by this table to the specified file name.
   * @author Erica Wheatcroft
   */
  public void writeFile(JTable table,
                        String fileName,
                        String columnDelimiter,
                        boolean addChecksum) throws FileNotFoundException
  {
    Assert.expect(table != null);
    Assert.expect(fileName != null);
    Assert.expect(columnDelimiter != null);

    _addChecksum = addChecksum;

    try
    {
      _os = new PrintWriter(fileName);

      TableModel model = table.getModel();

      // lets write out the column names
      for(int i = 0; i < model.getColumnCount(); i++)
        write(model.getColumnName(i) + columnDelimiter);

      _os.println();

      // lets write out the rows now
      for(int rowIndex = 0; rowIndex < model.getRowCount(); rowIndex++)
      {
        for(int columnIndex = 0; columnIndex < model.getColumnCount(); columnIndex++)
          write(model.getValueAt(rowIndex, columnIndex).toString() + columnDelimiter);

        _os.println();
      }

      if(_addChecksum)
        _os.println(_checksumUtil.getValue());

      _os.close();
    }
    finally
    {
      if (_os != null)
        _os.close();
    }

  }
}
