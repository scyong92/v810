package com.axi.util;

import java.io.*;
import java.awt.geom.*;

import com.axi.util.*;

/**
* This class represents a two dimensional set of coordinates.
*
* @author Bill Darbie
* @author Matt Wharton
*/
public class DoubleCoordinate implements Serializable
{
  // This class is really just wrapping a java.awt.geom.Point2D.Double.
  // NOTE: This IS a serializable class and the Point2D is supposed to be transient.  This is handled
  // via the overridden readObject/writeObject methods below.
  private transient Point2D.Double _point = null;

  /**
   * Construct this class and set the coordinates to 0,0.
   *
   * @author Bill Darbie
   */
  public DoubleCoordinate()
  {
    _point = new Point2D.Double(0d, 0d);
  }

  /**
   * Make a deep copy of the object passed in.
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public DoubleCoordinate(DoubleCoordinate rhs)
  {
    Assert.expect(rhs != null);

    _point = new Point2D.Double(rhs.getX(), rhs.getY());
  }

  /**
   * Construct the class with the coordinates passed in.
   *
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public DoubleCoordinate(double x, double y)
  {
    _point = new Point2D.Double(x, y);
  }

  /**
   * Construct a DoubleCoordinate from an IntCoordinate
   *
   * @author George A. David
   * @author Matt Wharton
   */
  public DoubleCoordinate(IntCoordinate intCoordinate)
  {
    Assert.expect(intCoordinate != null);

    _point = new Point2D.Double(intCoordinate.getX(), intCoordinate.getY());
  }

  /**
   * Create a DoubleCoordinate from a Point2D
   * @author George A. David
   * @author Matt Wharton
   */
  public DoubleCoordinate(Point2D point)
  {
    Assert.expect(point != null);

    _point = new Point2D.Double(point.getX(), point.getY());
  }

  /**
   * Set the coordinates to the point
   * @author George A. David
   * @author Matt Wharton
   */
  public void setLocation(Point2D point)
  {
    Assert.expect(point != null);

    _point.setLocation(point);
  }


  /**
   * Set the coordinates to new values
   *
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public void setXY(double x, double y)
  {
    _point.setLocation(x, y);
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public void setX(double x)
  {
    _point.setLocation(x, _point.getY());
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public double getX()
  {
    return _point.getX();
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public void setY(double y)
  {
    _point.setLocation(_point.getX(), y);
  }

  /**
   * Return a DoubleCoordinate that is the result of multiplying this DoubleCoordinate by the given factor.  This is
   * vector scaling.<p>
   *
   * For example,<p>
   * <code>
   * DoubleCoordinate x = new IntCoordinate(2,3);
   * DoubleCoordinate y = x.scale(2.0f);
   *
   * // y now equals (4.0, 6.0)
   * </code>
   *
   * @author Peter Esbensen
   */
  public DoubleCoordinate scale(double scalingFactor)
  {
    return new DoubleCoordinate((_point.getX() * scalingFactor), (_point.getY() * scalingFactor));
  }

  /**
   * @author Bill Darbie
   */
  public double getY()
  {
    return _point.getY();
  }

  /**
   * Create a new coordinate by rotating it about the origin (0, 0)
   * @author George A. David
   */
  public DoubleCoordinate getRotatedCoordinate(double degreesRotation)
  {
    return getRotatedCoordinate(degreesRotation, new DoubleCoordinate(0, 0));
  }

  /**
   * Create a new coordinate by rotating it about a specified coordinate
   * @author George A. David
   */
  public DoubleCoordinate getRotatedCoordinate(double degreesRotation, DoubleCoordinate pointOfRotation)
  {
    DoubleCoordinate coordinate = null;
    if(degreesRotation == 0)
    {
      coordinate = new DoubleCoordinate(this);
    }
    else
    {
      double x = getX() - pointOfRotation.getX();
      double y = getY() - pointOfRotation.getY();
      double radians = Math.toRadians(degreesRotation);
      double sinRadians = Math.sin(radians);
      double cosRadians = Math.cos(radians);
      // 2d rotation
      // Xn = Xo cos(theta) - Yo sin(theta)
      // Yn = Yo cos(theta) + Xo sin(theta)
      // theta is in radians
      double newXCoord = x * cosRadians - y * sinRadians;
      double newYCoord = y * cosRadians + x * sinRadians;
      coordinate = new DoubleCoordinate(newXCoord + pointOfRotation.getX(), newYCoord + pointOfRotation.getY());
    }

    return coordinate;
  }

  /**
   * Rotate this coordinate by about the origin (0, 0)
   * @author George A. David
   */
  public void rotateCoordinate(double degreesRotation)
  {
    rotateCoordinate(degreesRotation, new DoubleCoordinate(0, 0));
  }

  /**
   * Rotate coordinate about a specified coordinate
   * @author George A. David
   * @author Matt Wharton
   */
  public void rotateCoordinate(double degreesRotation, DoubleCoordinate pointOfRotation)
  {
    if(degreesRotation != 0)
    {
      double x = getX() - pointOfRotation.getX();
      double y = getY() - pointOfRotation.getY();
      double radians = Math.toRadians(degreesRotation);
      double sinRadians = Math.sin(radians);
      double cosRadians = Math.cos(radians);
      // 2d rotation
      // Xn = Xo cos(theta) - Yo sin(theta)
      // Yn = Yo cos(theta) + Xo sin(theta)
      // theta is in radians
      _point.setLocation( x * cosRadians - y * sinRadians, y * cosRadians + x * sinRadians );
    }
  }

  /**
   * @author Bill Darbie
   */
  public boolean equal(Object object)
  {
    boolean equal = false;

    if ((object != null) && (object instanceof IntCoordinate))
    {
      IntCoordinate coord = (IntCoordinate)object;
      if ((coord.getX() == getX()) && (coord.getY() == getY()))
        equal = true;
    }

    return equal;
  }

  /**
   * @author Bill Darbie
   */
  public int hashCode()
  {
    return _point.hashCode();
  }

  /**
   * @author Matt Wharton
   */
  protected void deserializePoint(ObjectInputStream is) throws IOException
  {
    Assert.expect(is != null);

    double tempX = is.readDouble();
    double tempY = is.readDouble();
    _point = new Point2D.Double(tempX, tempY);
  }

  /**
   * @author Matt Wharton
   */
  protected void serializePoint(ObjectOutputStream os) throws IOException
  {
    Assert.expect(os != null);
    Assert.expect(_point != null);

    os.writeDouble(_point.getX());
    os.writeDouble(_point.getY());
  }

  /**
   * @author Matt Wharton
   */
  private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
  {
    is.defaultReadObject();
    deserializePoint(is);
  }

  /**
   * @author Matt Wharton
   */
  private void writeObject(ObjectOutputStream os) throws IOException
  {
    os.defaultWriteObject();
    serializePoint(os);
  }
}
