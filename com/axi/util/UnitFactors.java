package com.axi.util;

/**
* Provides unit conversion factors
* for various units of voltage, current and time.
*
* <p>These should be used to convert between different units of magnitude and scale.
*
* <p>Format for conversion factors:
* <br>SMALLERUNIT_PER_LARGERUNIT
*
* @author Quan Mueller
*/
public class UnitFactors
{
  // voltage unit factors
  public static final double VOLTS_PER_KILOVOLT = 1000.0;

  // current unit factors
  public static final double MICROAMPS_PER_AMP = 1000000.0;

  // time unit factors
  public static final double MILLISECS_PER_SEC = 1000.0;
  public static final double SECS_PER_MIN = 60.0;
}