package com.axi.util;

import com.axi.util.Enum;

/**
 * This enumerates the different types of
 * available USB drivers.
 * @author George A. David
 */
public class UsbDriverTypeEnum extends Enum
{
  private static int _index = -1;
  public static UsbDriverTypeEnum FTDI = new UsbDriverTypeEnum(++_index);


  /**
   * @author George A. David
   */
  private UsbDriverTypeEnum(int id)
  {
    super(id);
  }
}
