package com.axi.util;

import java.io.*;
import java.math.*;
import java.nio.charset.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

/**
 * Converts Strings to other data types or other data types to Strings.
 * If the String is not in the right format
 * or does not meet the function requirements, a BadFormatException is thrown.
 * This exception is not a runtime exception (i.e. NumberFormatException, AssertException),
 * so a try/catch statement is required by the compiler in order to use these functions.
 *
 * @author Keith Lee
 */
public class StringUtil
{
  private static final String _HTML_START_PRE_TAG = "<PRE>";
  private static final String _HTML_END_PRE_TAG = "</PRE>";

  private static String _separator;

  private static DateFormat _dateFormat;
  private static DateFormat _timeFormat;

  // define pattern for "2007-03-08_09-21-27-880"
  private static Pattern _dateTimePattern = Pattern.compile("(\\d+)-(\\d+)-(\\d+)_(\\d+)-(\\d+)-(\\d+)-(\\d+)");
  private static GregorianCalendar _calendar1 = new GregorianCalendar();
  private static GregorianCalendar _calendar2 = new GregorianCalendar();

  private static String _hexRegularExpressionPattern = "([0-9|A-F|a-f]+)";
  private static Pattern _hourTimePattern = Pattern.compile("(\\d+):(\\d+):(\\d+)");

  /**
   * @author Bill Darbie
   */
  static
  {
    _separator = System.getProperty("line.separator", null);
    Assert.expect(_separator != null);

    _dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);
    _timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM);
  }

  /**
   * @author Bill Darbie
   */
  public static String getLineSeparator()
  {
    return _separator;
  }

  /**
   * Converts the specified String to a BigDecimal.
   *
   * @param bigDecimalStr the String to convert to a BigDecimal.
   * @return BigDecimal converted from String
   * @author Matt Wharton
   */
  public static BigDecimal convertStringToBigDecimal( String bigDecimalStr ) throws BadFormatException
  {
    Assert.expect( bigDecimalStr != null );

    BigDecimal bigDecimal = new BigDecimal( 0.0 );

    try
    {
      bigDecimal = new BigDecimal( bigDecimalStr );
    }
    catch( NumberFormatException nfe )
    {
      throw new BadFormatException(new LocalizedString("EX_STRING_TO_DOUBLE_BAD_FORMAT_KEY", new Object[]{bigDecimalStr}));
    }

    return bigDecimal;
  }

  /**
   * Converts the specified String to a BigInteger.
   *
   * @param bigIntegerStr the String to convert to a BigInteger.
   * @return BigInteger converted from String.
   * @author Matt Wharton
   *
   */
  public static BigInteger convertStringToBigInteger( String bigIntegerStr ) throws BadFormatException
  {
    Assert.expect( bigIntegerStr != null );

    BigInteger bigInteger = BigInteger.ZERO;

    try
    {
      bigInteger = new BigInteger( bigIntegerStr );
    }
    catch( NumberFormatException nfe )
    {
      throw new BadFormatException(new LocalizedString("EX_STRING_TO_INT_BAD_FORMAT_KEY", new Object[]{bigIntegerStr}));
    }

    return bigInteger;
  }

  /**
   * Converts a String into a double.
   *
   * @param doubleStr the String to convert to a double.
   * @return double converted from String.
   * @author Keith Lee
   */
  public static double convertStringToDouble(String doubleStr) throws BadFormatException
  {
    Assert.expect(doubleStr != null);

    double doubleNum;
    try
    {
      doubleNum = Double.parseDouble(doubleStr);
    }
    catch(NumberFormatException e)
    {
      BadFormatException  ex = new BadFormatException(new LocalizedString("EX_STRING_TO_DOUBLE_BAD_FORMAT_KEY", new Object[]{doubleStr}));
      ex.initCause(e);
      throw ex;
    }

    return doubleNum;
  }

  /**
   * Converts a String into a double greater than or equal to zero.
   *
   * @param doubleStr the String to convert to a positive double.
   * @return double converted from String.
   * @author Keith Lee
   */
  public static double convertStringToPositiveDouble(String doubleStr) throws BadFormatException, ValueOutOfRangeException
  {
    Assert.expect(doubleStr != null);

    double doubleNum = convertStringToDouble(doubleStr);

    if (doubleNum < 0)
      throw new ValueOutOfRangeException(doubleNum, 0.0, Double.MAX_VALUE);

    return doubleNum;
  }

  /**
   * Converts a String into a double less than or equal to zero.
   *
   * @param doubleStr the String to convert to a negative double.
   * @return double converted from String.
   * @author Keith Lee
   */
  public static double convertStringToNegativeDouble(String doubleStr) throws BadFormatException, ValueOutOfRangeException
  {
    Assert.expect(doubleStr != null);

    double doubleNum = convertStringToDouble(doubleStr);
    if (doubleNum > 0)
      throw new ValueOutOfRangeException(doubleNum, Double.MIN_VALUE, 0);

    return doubleNum;
  }

  /**
   * Converts a String into a double within the specified range.
   *
   * @param doubleStr the String to convert to a double within the range.
   * @param min the minimum value of the range : greater than or equal to.
   * @param max the maximum value of the range : less than or equal to.
   * @return double converted from String.
   * @author Keith Lee
   */
  public static double convertStringToDouble(String doubleStr, double min, double max) throws BadFormatException, ValueOutOfRangeException
  {
    Assert.expect(doubleStr != null);
    Assert.expect(min <= max);

    double doubleNum = convertStringToDouble(doubleStr);
    if ((doubleNum < min) || (doubleNum > max))
      throw new ValueOutOfRangeException(doubleNum, min, max);

    return doubleNum;
  }

  /**
   * Converts the specified String into a float.
   *
   * @author Matt Wharton
   */
  public static float convertStringToFloat(String floatStr) throws BadFormatException
  {
    Assert.expect(floatStr != null);

    float floatNum = 0f;
    try
    {
      floatNum = Float.parseFloat(floatStr);
    }
    catch (NumberFormatException nfe)
    {
      BadFormatException bfex = new BadFormatException(new LocalizedString("EX_STRING_TO_FLOAT_BAD_FORMAT_KEY", new Object[]{floatStr}));
      bfex.initCause(nfe);
      throw bfex;
    }

    return floatNum;
  }

  /**
   * Converts a String into an int.
   *
   * @param intStr the String to convert to an int.
   * @return int converted from String.
   * @author Keith Lee
   */
  public static int convertStringToInt(String intStr) throws BadFormatException
  {
    Assert.expect(intStr != null);

    int intNum;
    double doubleIntValue = 0.0;
    double tempDoubleValue = 0.0;
    try
    {
      tempDoubleValue = Double.parseDouble(intStr);
      doubleIntValue = Math.rint(tempDoubleValue);
      if (tempDoubleValue != doubleIntValue)
        throw new BadFormatException(new LocalizedString("EX_STRING_TO_INT_BAD_FORMAT_KEY", new Object[]{intStr}));

      if ((doubleIntValue < Integer.MIN_VALUE) || (doubleIntValue > Integer.MAX_VALUE))
        throw new BadFormatException(new LocalizedString("EX_STRING_TO_INT_BAD_FORMAT_KEY", new Object[]{intStr}));

      intNum = (int)doubleIntValue;
    }
    catch(NumberFormatException e)
    {
      BadFormatException ex = new BadFormatException(new LocalizedString("EX_STRING_TO_INT_BAD_FORMAT_KEY", new Object[]{intStr}));
      ex.initCause(e);
      throw ex;
    }

    return intNum;
  }

  /**
   * Converts a String into an "strict" int. For example, the value "1.0"
   * is not a strict int and therefore will result in an exception.
   *
   * @param intStr the String to convert to an int.
   * @return int converted from String.
   * @author Keith Lee
   */
  public static int convertStringToStrictInt(String intStr) throws BadFormatException
  {
    Assert.expect(intStr != null);

    if (intStr.indexOf('.') != -1)
    {
      throw new BadFormatException(new LocalizedString("EX_STRING_TO_INT_BAD_FORMAT_KEY", new Object[]{intStr}));
    }

    int intNum = convertStringToInt(intStr);

    return intNum;
  }

  /**
   * Converts a String into an "strict" int. For example, the value "1.0"
   * is not a strict int and therefore will result in an exception.
   *
   * @param intStr the String to convert to an int.
   * @return int converted from String.
   * @author Keith Lee
   */
  public static int convertStringToStrictInt(String intStr, int minValue, int maxValue) throws BadFormatException, ValueOutOfRangeException
  {
    Assert.expect(intStr != null);
    Assert.expect(minValue <= maxValue);

    if (intStr.indexOf('.') != -1)
      throw new BadFormatException(new LocalizedString("EX_STRING_TO_INT_BAD_FORMAT_KEY", new Object[]{intStr}));

    int intNum = convertStringToStrictInt(intStr);
    if (intNum > maxValue || intNum < minValue)
      throw new ValueOutOfRangeException(intNum, minValue, maxValue);

    return intNum;
  }

  /**
   * Converts a String into an "strict" int. For example, the value "1.0"
   * is not a strict int and therefore will result in an exception.
   *
   * @param intStr the String to convert to an int.
   * @return int converted from String.
   * @author Keith Lee
   */
  public static int convertStringToPositiveStrictInt(String intStr) throws BadFormatException, ValueOutOfRangeException
  {
    Assert.expect(intStr != null);

    if (intStr.indexOf('.') != -1)
    {
      throw new BadFormatException(new LocalizedString("EX_STRING_TO_INT_BAD_FORMAT_KEY", new Object[]{intStr}));
    }

    int intNum = convertStringToStrictInt(intStr);
    if (intNum < 0)
      throw new ValueOutOfRangeException(intNum, 0, Integer.MAX_VALUE);

    return intNum;
  }

  /**
   * Converts a String into a positive int greater than or equal to zero.
   *
   * @param intStr the String to convert to a positive int.
   * @return int converted from String.
   * @author Keith Lee
   */
  public static int convertStringToPositiveInt(String intStr) throws BadFormatException, ValueOutOfRangeException
  {
    Assert.expect(intStr != null);

    int intNum = convertStringToInt(intStr);
    if (intNum < 0)
      throw new ValueOutOfRangeException(intNum, 0, Integer.MAX_VALUE);

    return intNum;
  }

  /**
   * Converts a String into an int less than or equal to zero.
   *
   * @param intStr the String to convert to a negative int.
   * @return int converted from String.
   * @author Keith Lee
   */
  public static int convertStringToNegativeInt(String intStr) throws BadFormatException, ValueOutOfRangeException
  {
    Assert.expect(intStr != null);

    int intNum = convertStringToInt(intStr);
    if (intNum > 0)
      throw new ValueOutOfRangeException(intNum, Integer.MIN_VALUE, 0);

    return intNum;
  }

  /**
   * Converts a String into an int within the specified range.
   *
   * @param intStr the String to convert to an int within the range.
   * @param min the minimum value of the range : greater than or equal to.
   * @param max the maximum value of the range : less than or equal to.
   * @return int converted from String.
   * @author Keith Lee
   */
  public static int convertStringToInt(String intStr, int min, int max) throws BadFormatException, ValueOutOfRangeException
  {
    Assert.expect(intStr != null);
    Assert.expect(min <= max);

    int intNum = convertStringToInt(intStr);
    if ((intNum < min) || (intNum > max))
      throw new ValueOutOfRangeException(intNum, min, max);

    return intNum;
  }

  /**
   * Converts a String into a boolean.
   *
   * @param booleanStr the String to convert to a boolean.
   * @return boolean converted from String.
   * @author Keith Lee
   */
  public static boolean convertStringToBoolean(String booleanStr) throws BadFormatException
  {
    Assert.expect(booleanStr != null);

    boolean booleanVal;
    if (booleanStr.equalsIgnoreCase("TRUE"))
    {
      booleanVal = true;
    }
    else if (booleanStr.equalsIgnoreCase("FALSE"))
    {
      booleanVal = false;
    }
    else
    {
      throw new BadFormatException(new LocalizedString("EX_STRING_TO_BOOLEAN_BAD_FORMAT_KEY", new Object[]{booleanStr}));
    }

    return booleanVal;
  }
  
  /**
   * review put into StringUtil
   * @param booleanString
   * @return 
   * @author Wei Chin
   */
  public static Boolean convertNumberStringToBoolean(String booleanString) throws BadFormatException
  {
    Assert.expect(booleanString != null);
    
    if(booleanString.equals("0"))
    {
      return new Boolean(false);
    }
    else if (booleanString.equalsIgnoreCase("1"))
    {
      return new Boolean(true);
    }
    else
    {
      throw new BadFormatException(new LocalizedString("EX_STRING_TO_BOOLEAN_BAD_FORMAT_KEY", new Object[]{booleanString}));
    }
  }  
  
  /**
   * Converts a String into a BitSet
   *
   * @param integerStr the String to be converted into a BitSet
   * @return BitSet converted from a String
   * @author George A. David
   */
  public static BitSet convertStringToBitSet(String integerStr) throws BadFormatException
  {
    Assert.expect(integerStr != null);

    int intValue = convertStringToInt(integerStr);
    List<Boolean> bitList = new ArrayList<Boolean>();
    int quotient = 1;
    int remainder = 0;
    Boolean on = new Boolean(true);
    Boolean off = new Boolean(false);

    /////////////////////////////////////////////////////////////////////////
    // To convert an integer to a binary number, we first take the original
    // number and divide by 2. If the remainder is not 0, then the first bit
    // is set to 1. Otherwise, the first bit is set to 0. For the next time
    // around we take the quotient and divide it by 2. Then we set the bits
    // as discussed above. We continue this process until the quotient is 0.
    // Example: Covert integer 41 to a binary number.
    //       quotient     remainder   coefficient
    // 41/2    20            1/2        a0 = 1
    // 20/2    10             0         a1 = 0
    // 10/2     5             0         a2 = 0
    // 5/2      2            1/2        a3 = 1
    // 2/2      1             0         a4 = 0
    // 1/2      0            1/2        a5 = 1
    //
    // answer: 41 = a5 a4 a3 a2 a1 a0 = 101001
    //         41 = a0*(2^0) + a1*(2^1) + ...  an*(2^n)
    //         41 = 1*(2^0) + 0*(2^1) + 0*(2^2) + 1*(2^3) + 0*(2^4) + 1*(2^5)
    // George A. David
    /////////////////////////////////////////////////////////////////////////
    while(quotient !=0)
    {
      quotient = intValue % 2;
      remainder = intValue - quotient;
      intValue = quotient;
      if (remainder == 0)
      {
        bitList.add(off);
      }
      else
      {
        bitList.add(on);
      }
    }

    //create a BitSet and set the bits as appropriate
    BitSet bitSet = new BitSet(bitList.size());
    for(int index = 0; index < bitList.size(); ++index)
    {
      if (((Boolean)bitList.get(index)).equals(on))
      {
        bitSet.set(index);
      }
    }
    return bitSet;
  }

  /**
   * Converts a String into a long.
   *
   * @param longStr the String to convert to a long.
   * @return long converted from String.
   * @author Keith Lee
   */
  public static long convertStringToLong(String longStr) throws BadFormatException
  {
    Assert.expect(longStr != null);

    long longNum;
    double doubleLongValue = 0.0;
    double tempDoubleValue = 0.0;
    try
    {
      tempDoubleValue = Double.parseDouble(longStr);
      doubleLongValue = Math.rint(tempDoubleValue);
      if (tempDoubleValue != doubleLongValue)
        throw new BadFormatException(new LocalizedString("EX_STRING_TO_LONG_BAD_FORMAT_KEY", new Object[]{longStr}));

      if ((doubleLongValue < Long.MIN_VALUE) || (doubleLongValue > Long.MAX_VALUE))
        throw new BadFormatException(new LocalizedString("EX_STRING_TO_LONG_BAD_FORMAT_KEY", new Object[]{longStr}));
      longNum = (long)doubleLongValue;
    }
    catch(NumberFormatException e)
    {
      BadFormatException ex = new BadFormatException(new LocalizedString("EX_STRING_TO_LONG_BAD_FORMAT_KEY", new Object[]{longStr}));
      ex.initCause(e);
      throw ex;
    }

    return longNum;
  }

  /**
   * Converts a String into an "strict" long. For example, the value "1.0"
   * is not a strict int and therefore will result in an exception.
   *
   * @author Bill Darbie
   */
  public static long convertStringToStrictLong(String longStr) throws BadFormatException
  {
    Assert.expect(longStr != null);

    if (longStr.indexOf('.') != -1)
    {
      throw new BadFormatException(new LocalizedString("EX_STRING_TO_LONG_BAD_FORMAT_KEY", new Object[]{longStr}));
    }

    long longNum = convertStringToLong(longStr);

    return longNum;
  }

  /**
   * Converts a String into a long greater than or equal to zero.
   *
   * @param longStr the String to convert to a positive long.
   * @return long converted from String.
   * @author Keith Lee
   */
  public static long convertStringToPositiveLong(String longStr) throws BadFormatException, ValueOutOfRangeException
  {
    Assert.expect(longStr != null);

    long longNum = convertStringToLong(longStr);
    if (longNum < 0)
      throw new ValueOutOfRangeException(longNum, 0, Long.MAX_VALUE);

    return longNum;
  }

  /**
   * Converts a String into a long less than or equal to zero.
   *
   * @param longStr the String to convert to a negative long.
   * @return long converted from String.
   * @author Keith Lee
   */
  public static long convertStringToNegativeLong(String longStr) throws BadFormatException, ValueOutOfRangeException
  {
    Assert.expect(longStr != null);

    long longNum = convertStringToLong(longStr);
    if (longNum > 0)
      throw new ValueOutOfRangeException(longNum, Long.MIN_VALUE, 0);

    return longNum;
  }

  /**
   * Converts a String into a long within the specified range.
   *
   * @param longStr the String to convert to a long within the range.
   * @param min the minimum value of the range : greater than or equal to.
   * @param max the maximum value of the range : less than or equal to.
   * @return  long converted from String.
   * @author Keith Lee
   */
  public static long convertStringToLong(String longStr, long min, long max) throws BadFormatException, ValueOutOfRangeException
  {
    Assert.expect(longStr != null);
    Assert.expect(min <= max);

    long longNum = convertStringToLong(longStr);
    if ((longNum < min) || (longNum > max))
      throw new ValueOutOfRangeException(longNum, min, max);

    return longNum;
  }

  /**
   * Counts the number of times a character appears in the string.
   * @param stringToCheck the string to look for charToCount
   * @param charToCount the character to look for and count
   * @return the number of times the character appears in the string
   * @author George A. David
   */
  public static int countInstancesOfChar(String stringToCheck, char charToCount)
  {
    Assert.expect(stringToCheck != null);
    int count = 0;
    for(int index = 0; index < stringToCheck.length(); ++index)
    {
      if (stringToCheck.charAt(index) == charToCount)
        ++count;
    }

    return count;
  }

  /**
   * @return a new String with every occurance of stringToBeReplaced replaced with replacementString.
   * @author Bill Darbie
   */
  public static String replace(String originalString, String stringToBeReplaced, String replacementString)
  {
    Assert.expect(originalString != null);
    Assert.expect(stringToBeReplaced != null);
    Assert.expect(replacementString != null);

    if (stringToBeReplaced.equals(replacementString))
      return originalString;

    String string = originalString;
    int startIndex = string.lastIndexOf(stringToBeReplaced);
    int endIndex = stringToBeReplaced.length() + startIndex;
    while(startIndex != -1)
    {
      StringBuffer str = new StringBuffer(string);
      str.replace(startIndex, endIndex, replacementString);
      string = str.toString();
      startIndex = string.lastIndexOf(stringToBeReplaced, startIndex - 1);
      endIndex = stringToBeReplaced.length() + startIndex;
    }

    return string;
  }

  /**
   * @author Greg Esparza
   */
  static String getPreFormattedStringWithoutHtmlTags(String preFormattedStringWithHtmlTags)
  {
    Assert.expect(preFormattedStringWithHtmlTags != null);

    String tempStr = preFormattedStringWithHtmlTags.toUpperCase();

    int htmlStartTagIndex = tempStr.indexOf(_HTML_START_PRE_TAG);
    int htmlEndPreTagIndex = tempStr.indexOf(_HTML_END_PRE_TAG);
    if ((htmlStartTagIndex != -1) && (htmlEndPreTagIndex != -1))
      return preFormattedStringWithHtmlTags.substring(htmlStartTagIndex + _HTML_START_PRE_TAG.length(), htmlEndPreTagIndex - 1);
    else
      return preFormattedStringWithHtmlTags;
  }

  /**
   * Get a string in the form of <PRE>preformattedString</PRE>
   *
   * @param originalString is in the form of "possibly more string data at the beginning <PRE>preFormattedString</PRE> possibly more string data at the end"
   * @return a string in the form of <PRE>preformattedString</PRE>
   * @author Greg Esparza
   */
  private static String getPreFormattedSubstring(String originalString)
  {
    Assert.expect(originalString != null);

    String tempStr = originalString.toUpperCase();

    int htmlStartTagIndex = tempStr.indexOf(_HTML_START_PRE_TAG);
    int htmlEndPreTagIndex = tempStr.indexOf(_HTML_END_PRE_TAG);

    if ((htmlStartTagIndex != -1) && (htmlEndPreTagIndex != -1))
      return originalString.substring(htmlStartTagIndex, htmlEndPreTagIndex + _HTML_END_PRE_TAG.length());
    else
      return originalString;
  }

  /**
   * @author Greg Esparza
   */
  private static boolean isPreFormattedString(String stringToCheck)
  {
    Assert.expect(stringToCheck != null);

    boolean isPreformatted = false;

    //If the string is long enough, check it
    if (stringToCheck.length() >= _HTML_START_PRE_TAG.length())
    {
      String preformatStart = stringToCheck.substring(0, _HTML_START_PRE_TAG.length());

      isPreformatted = preformatStart.equalsIgnoreCase(_HTML_START_PRE_TAG);
    }

    return isPreformatted;
  }

  /**
   * @author Greg Esparza
   */
  private static List<String> getStringFormatTypes(String originalString)
  {
    Assert.expect(originalString != null);

    List<String> stringTypes = new ArrayList<String>();
    StringBuffer tempStr = new StringBuffer();
    String preFormattedStr = null;
    boolean foundPreFormattedStr = false;
    int charIndex = 0;

    while (charIndex < originalString.length())
    {
      char ch = originalString.charAt(charIndex);
      if (ch == '<')
      {
        preFormattedStr = originalString.substring(charIndex);

        // We just found a "<" character which is the first character of this "stringToCheck"
        // so the only thing we need to do is see if this substring is an HTML preformat tag.
        if (isPreFormattedString(preFormattedStr))
        {
          // If the tempStr length is not empty, then there is an unformatted string
          // ahead of the preformatted string so add it to the list.
          if (tempStr.length() > 0)
            stringTypes.add(tempStr.toString());

          // Add the preformatted string to the
          preFormattedStr = getPreFormattedSubstring(preFormattedStr);

          // Set the char index to start at the next character after the preformatted substring
          charIndex += preFormattedStr.length();

          // Add the preformatted string to the list
          stringTypes.add(preFormattedStr);

          // Indicate that we found a preformatted substring
          foundPreFormattedStr = true;

          // Reset the tempStr
          tempStr = new StringBuffer();
        }
      }

      // If we did not find a preformatted substring, then keep adding characters sequentially.
      // Otherwise, we found a preformatted substring so reset the flag.
      if (foundPreFormattedStr == false)
      {
        tempStr.append(ch);
        ++charIndex;
      }
      else
        foundPreFormattedStr = false;
    }

    // If we never find a preformatted substring then the we will add the one and only unformatted first string.
    // Otherwise, we will add the last unformattedString
    if (tempStr.length() > 0)
      stringTypes.add(tempStr.toString());

    Assert.expect(stringTypes != null);
    return stringTypes;
  }

  /**
   * Put a newLine at the first space found at or after lineLength.
   * Any existing tabs will be removed.  Exisiting newlines will
   * not be removed.
   * @author Bill Darbie
   */
  public static String format(String originalString, int lineLength)
  {
    Assert.expect(originalString != null);
    Assert.expect(lineLength > 0);

    String finalStr = new String();

    // First, look for any strings that are preformatted and store all format types in the list.
    List<String> stringFormatTypes = getStringFormatTypes(originalString);

    for (String stringType : stringFormatTypes)
    {
      // If the string is not preformatted, then we can modify this portion of the string with our
      // internal formatting.
      if (isPreFormattedString(stringType) == false)
      {
        int count = lineLength;
        StringBuffer sb = new StringBuffer();

        if (stringType.length() > lineLength)
        {
          for (int i = 0; i < stringType.length(); ++i)
          {
            char ch = stringType.charAt(i);
            if (((i + 1 > count) && (ch == ' ')) || (ch == '\n'))
            {
              // add lineLength to count so we will not replace the next space with a newline for another
              // lineLength characters
              sb.append(_separator);
              count = i + _separator.length() + lineLength;
            }
            else
              sb.append(ch);
          }
        }
        else
          sb.append(stringType);

        stringType = sb.toString();
        stringType = stringType.replaceAll("\\t", " ");
        stringType = stringType.replaceAll(_separator + " +", _separator);
      }
      else
        // If the string is preformatted, then remove the preformat HTML tags before we concatenate
        stringType = getPreFormattedStringWithoutHtmlTags(stringType);

      finalStr += stringType;
    }

    return finalStr;
  }

  /**
   * Put a newLine at the first space found at or after lineLength.
   * Any existing tabs will be removed.  Exisiting newlines will
   * not be removed.
   * @author Bill Darbie
   */
  public static String format(String originalString, int lineLength, String seperator)
  {
    Assert.expect(originalString != null);
    Assert.expect(lineLength > 0);

    String finalStr = new String();

    // First, look for any strings that are preformatted and store all format types in the list.
    List<String> stringFormatTypes = getStringFormatTypes(originalString);

    for (String stringType : stringFormatTypes)
    {
      // If the string is not preformatted, then we can modify this portion of the string with our
      // internal formatting.
      if (isPreFormattedString(stringType) == false)
      {
        int count = lineLength;
        StringBuffer sb = new StringBuffer();

        if (stringType.length() > lineLength)
        {
          for (int i = 0; i < stringType.length(); ++i)
          {
            char ch = stringType.charAt(i);
            if (((i + 1 > count) && (ch == ' ')) || (ch == '\n'))
            {
              // add lineLength to count so we will not replace the next space with a newline for another
              // lineLength characters
              sb.append(seperator);
              count = i + seperator.length() + lineLength;
            }
            else
              sb.append(ch);
          }
        }
        else
          sb.append(stringType);

        stringType = sb.toString();
        stringType = stringType.replaceAll("\\t", " ");
        stringType = stringType.replaceAll(_separator + " +", _separator);
      }
      else
        // If the string is preformatted, then remove the preformat HTML tags before we concatenate
        stringType = getPreFormattedStringWithoutHtmlTags(stringType);

      finalStr += stringType;
    }

    return finalStr;
  }

 /*
  * This function will format the remaining pay per use credit time into Hours : Minutes : Seconds.
  * @param timeInMilliSeconds the pay Per Use remaining time in milliseconds.
  * @param showTenthsOfSecond boolean to indicate if the elapsed time should diplsay tenths of a second
  *        as well.
  * @return the formated time in H:M:S if showTenthsOfSecond is true the format will be H:M:S.TS
  * @author Erica Wheatcroft
  */
  public static String convertMilliSecondsToElapsedTime(long timeInMilliSeconds, boolean showTenthsOfSecond)
  {
    Assert.expect(timeInMilliSeconds >= 0, "StringUtil.convertMilliSecondsToElapsedTime() - Time Remaining cannot be negative.");

    long remainingSeconds = timeInMilliSeconds / 1000;
    long remainingHours = 0;
    if ( remainingSeconds > 3600 )
    {
      remainingHours = remainingSeconds / 3600 ;
      remainingSeconds %= 3600;
    }
    long remainingMinutes = 0;
    if ( remainingSeconds > 60 )
    {
      remainingMinutes = remainingSeconds / 60;
      remainingSeconds %= 60;
    }
    long remainingMillis =  timeInMilliSeconds - (remainingSeconds * 1000 +
                                                  remainingMinutes * 1000 * 60 +
                                                  remainingHours * 1000 * 60 * 60);
    long remainingTenthsOfSecond = Math.round( remainingMillis / 100.0 );

    if ( remainingHours == 60 )
    {
      remainingMinutes = 0;
      remainingHours += 1;
    }

    String remainingTestTimeString = remainingHours + " : ";

    // check to see if minutes is a single digit if so add a zero so that it looks nice
    if (remainingMinutes < 10 )
      remainingTestTimeString = remainingTestTimeString + "0" + remainingMinutes + " : ";
    else
      remainingTestTimeString = remainingTestTimeString + "" + remainingMinutes + " : ";

    // do the same thing for seconds.
    if ( remainingSeconds < 10 )
      remainingTestTimeString = remainingTestTimeString + "0" + remainingSeconds;
    else
      remainingTestTimeString = remainingTestTimeString + "" + remainingSeconds;

    if (showTenthsOfSecond)
      remainingTestTimeString += "." + remainingTenthsOfSecond;

    return remainingTestTimeString;
  }

  /**
   * Format the time in milli seconds to a short date and time string.
   * In this string, the time is displayed first because it is the most
   * important information when screen real-estate is limited.
   * @author Rex Shang
   */
  public static String convertMilliSecondsToTimeAndDate(long timeInMilliSeconds)
  {
    Assert.expect(timeInMilliSeconds >= 0,
                  "StringUtil.convertMilliSecondsToTimeAndDate() - Time cannot be negative.");
    Date date = new Date(timeInMilliSeconds);

    String timeAndDateString = _timeFormat.format(date) + " " + _dateFormat.format(date);

    return timeAndDateString;
  }

  /**
   * Convert time in milliseconds to a date-time timestamp in
   * format "YYYY-MM-DD_HH-MM-SS-ss"
   * @author George Booth
   * @author Bill Darbie
   */
  public static String convertMilliSecondsToTimeStamp(long timeInMilliSeconds)
  {
    Assert.expect(timeInMilliSeconds >= 0,
                  "StringUtil.convertMilliSecondsToTimeAndDate() - Time cannot be negative.");

    _calendar1.setTimeInMillis(timeInMilliSeconds);
    NumberFormat nf = NumberFormat.getNumberInstance();
    nf.setMinimumIntegerDigits(2);
    String timeStamp = _calendar1.get(Calendar.YEAR) + "-" +
                     nf.format(_calendar1.get(Calendar.MONTH) + 1) + "-" +
                     nf.format(_calendar1.get(Calendar.DAY_OF_MONTH)) + "_" +
                     nf.format(_calendar1.get(Calendar.HOUR_OF_DAY)) + "-" +
                     nf.format(_calendar1.get(Calendar.MINUTE)) + "-" +
                     nf.format(_calendar1.get(Calendar.SECOND)) + "-" +
                     nf.format(_calendar1.get(Calendar.MILLISECOND));
    return timeStamp;
  }
  
  /**
   * @author Kee Chin Seong
   * @param dateTime
   * @return
   * @throws BadFormatException 
   */
  public static long convertStringToTimeInMilliSecondsUsingDate(String dateTime) throws BadFormatException
  {
    Assert.expect(dateTime != null);
    
    long timeInMillis = 0;
    Matcher matcher = _dateTimePattern.matcher(dateTime);
    if (matcher.matches())
    {
      String milliSecondStr = matcher.group(7);

      int milliSeconds = Integer.valueOf(milliSecondStr);
      SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
      formatter.setLenient(false);

      try
      {
         Date oldDate = formatter.parse(dateTime);
         timeInMillis = oldDate.getTime();
      }
      catch(ParseException ex)
      {
          System.out.println(ex.getMessage());
      }
      timeInMillis += milliSeconds;
    }
    else
    {
      throw new BadFormatException(new LocalizedString("EX_STRING_TO_LONG_BAD_FORMAT_KEY", new Object[]{dateTime}));
    }

    return timeInMillis;
  }

  /**
   * Convert a date-time string to equivalent time in millseconds
   * Expect format "YYYY-MM-DD_HH-MM-SS-ss"
   * YYYY = year
   * MM = month (Jan = 0)
   * DD = day (1 = first day of the month)
   * HH = hour
   * MM = minutes
   * SS = seconds
   * ss = milliseconds
   * @author George Booth
   * @author Bill Darbie
   */
  public static long convertStringToTimeInMilliSeconds(String dateTime) throws BadFormatException
  {
    Assert.expect(dateTime != null);
//    System.out.println("dateTime = " + dateTime);
    long timeInMillis = 0;
    Matcher matcher = _dateTimePattern.matcher(dateTime);
    if (matcher.matches())
    {
      String yearStr = matcher.group(1);
      String monthStr = matcher.group(2);
      String dayStr = matcher.group(3);
      String hourStr = matcher.group(4);
      String minuteStr = matcher.group(5);
      String secondStr = matcher.group(6);
      String milliSecondStr = matcher.group(7);
      int year = Integer.valueOf(yearStr);
      int month = Integer.valueOf(monthStr) - 1;
      int day = Integer.valueOf(dayStr);
      int hour = Integer.valueOf(hourStr);
      int minute = Integer.valueOf(minuteStr);
      int second = Integer.valueOf(secondStr);
      int milliSeconds = Integer.valueOf(milliSecondStr);
      _calendar2.setTimeInMillis(0);
      _calendar2.set(year, month, day, hour, minute, second);
      timeInMillis = _calendar2.getTimeInMillis();
      timeInMillis += milliSeconds;
    }
    else
    {
      throw new BadFormatException(new LocalizedString("EX_STRING_TO_LONG_BAD_FORMAT_KEY", new Object[]{dateTime}));
    }

    return timeInMillis;
  }

  /**
   * @author Laura Cormos
   */
  public static boolean isEmpty(String stringToTest)
  {
    Assert.expect(stringToTest != null);

    String emptyStringPattern = "^\\s*$";
    if (Pattern.matches(emptyStringPattern, stringToTest))
      return true;
    return false;

  }

  /**
   * @return true if the string is alphanumeric, false if not
   * @author George A. David
   */
  public static boolean isAlphaNumeric(String stringToTest)
  {
    Assert.expect(stringToTest != null);

    boolean isAlphaNumeric = false;
    // try to convert the string to a double, if it fails, then it must
    // be an alpha numeric string
    try
    {
      convertStringToDouble(stringToTest);
    }
    catch(BadFormatException bfe)
    {
      isAlphaNumeric = true;
    }

    return isAlphaNumeric;
  }

  /**
   * Determines whether the specified string is numeric.
   *
   * @return true is the string is numeric, false otherwise.
   * @author Matt Wharton
   */
  public static boolean isNumeric( String stringToTest )
  {
    Assert.expect( stringToTest != null );

    boolean isNumeric = true;
    // Attempt to convert the string to a double.  If it works, the string is numeric.
    try
    {
      convertStringToDouble( stringToTest );
    }
    catch ( BadFormatException bfe )
    {
      isNumeric = false;
    }

    return isNumeric;
  }

  /**
   * Determines whether the specified string represents an integer.
   *
   * @return true is the string represents an integer, false otherwise.
   * @author Matt Wharton
   */
  public static boolean isInteger( String stringToTest )
  {
    Assert.expect( stringToTest != null );

    boolean isInteger = (stringToTest.length() != 0);  // Empty strings aren't representations of ints.
    // Check to see if this String contains any characters that aren't digits.
    for ( int i = 0; isInteger && i < stringToTest.length(); ++i )
    {
      if ( Character.isDigit( stringToTest.charAt( i ) ) == false )
      {
        isInteger = false;
      }
    }

    return isInteger;
  }

  /**
   * @return true if the string starts begins with a non-numeric character.
   * @author George A. David
   */
  public static boolean startsWithNumber(String stringToTest)
  {
    Assert.expect(stringToTest != null);

    boolean startsWithNumber = true;
    // try to convert the string to an int, if it fails, then it must
    // be a non-numeric  characters
    try
    {
      convertStringToInt(stringToTest.substring(0, 1));
    }
    catch(BadFormatException bfe)
    {
      startsWithNumber = false;
    }

    return startsWithNumber;
  }

  /**
   * @return true if the string starts begins with a character (ignoreCase).
   * @author Wei Chin, Chong
   */
  public static boolean startWithIgnoreCase(String stringToTest, String startString)
  {
    Assert.expect(stringToTest != null);

    return stringToTest.toUpperCase().startsWith(startString.toUpperCase());
  }

  /**
   * Beginning with the first character of the string,
   * it converts as much of the string as possible to an integer.
   *
   * @author George A. David
   */
  public static int legacyConvertStringToInt(String integerString) throws BadFormatException
  {
    Assert.expect(integerString != null);

    int beginIndex = 0;
    // we must account for the positive or negative sign
    if (integerString.charAt(0) == '+' || integerString.charAt(0) == '-')
    {
      beginIndex = 1;
    }

    // the string must begin with an integer
    boolean startsWithInteger = false;
    int integer = 0;

    for(int index = beginIndex + 1; index < integerString.length() + 1; ++index)
    {
      try
      {
        integer = convertStringToInt(integerString.substring(0, index));
        startsWithInteger = true;
      }
      catch(BadFormatException bfe)
      {
        if (startsWithInteger == false)
        {
          // an invalid string was passed in
          throw new BadFormatException(new LocalizedString("EX_STRING_TO_START_WITH_NUMBER_KEY", new Object[]{integerString}));
        }
        else
        {
          // we have done converting what we can.
          break;
        }

      }
    }

    Assert.expect(startsWithInteger);
    return integer;
  }

  /**
   * Beginning with the first character of the string,
   * it converts as much of the string as possible to an integer.
   *
   * @author George A. David
   */
  public static int legacyConvertStringToInt(String integerString, int minValue, int maxValue) throws BadFormatException, ValueOutOfRangeException
  {
    Assert.expect(integerString != null);
    Assert.expect(minValue <= maxValue);

    int integer = legacyConvertStringToInt(integerString);
    if (integer < minValue || integer > maxValue)
    {
      throw new ValueOutOfRangeException(integer, minValue, maxValue);
    }

    return integer;
  }

  /**
   * Beginning with the first character of the string,
   * it converts as much of the string as possible to a positive integer.
   *
   * @author George A. David
   */
  public static int legacyConvertStringToPositiveInt(String integerString) throws BadFormatException, ValueOutOfRangeException
  {
    Assert.expect(integerString != null);

    int positiveInteger = legacyConvertStringToInt(integerString);

    if (positiveInteger < 0)
    {
      // an invalid string was passed in
      throw new ValueOutOfRangeException(positiveInteger, 0, Integer.MAX_VALUE);
    }

    return positiveInteger;
  }

  /**
   * Beginning with the first character of the string,
   * it converts as much of the string as possible to a negative integer.
   *
   * @author George A. David
   */
  public static int legacyConvertStringToNegativeInt(String integerString) throws BadFormatException, ValueOutOfRangeException
  {
    Assert.expect(integerString != null);

    int positiveInteger = legacyConvertStringToInt(integerString);

    if (positiveInteger > 0)
      throw new ValueOutOfRangeException(positiveInteger, Integer.MIN_VALUE, 0);

    return positiveInteger;
  }

  /**
   * Beginning with the first character of the string,
   * it converts as much of the string as possible to a double.
   *
   * @author George A. David
   */
  public static double legacyConvertStringToDouble(String doubleString) throws BadFormatException
  {
    Assert.expect(doubleString != null);

    int beginIndex = 0;
    // we must account for the positive or negative sign
    if (doubleString.charAt(0) == '+' || doubleString.charAt(0) == '-')
    {
      beginIndex = 1;
    }

    // the string must begin with a double
    boolean startsWithDouble = false;
    double doubleValue = 0;

    for(int index = beginIndex + 1; index < doubleString.length() + 1; ++index)
    {
      try
      {
        doubleValue = convertStringToDouble(doubleString.substring(0, index));
        startsWithDouble = true;
      }
      catch(BadFormatException bfe)
      {
        if (startsWithDouble == false)
        {
          // an invalid string was passed in
          throw new BadFormatException(new LocalizedString("EX_STRING_TO_START_WITH_NUMBER_KEY", new Object[]{doubleString}));
        }
        else
        {
          // we have done converting what we can.
          break;
        }

      }
    }

    Assert.expect(startsWithDouble);
    return doubleValue;
  }

  /**
   * Beginning with the first character of the string,
   * it converts as much of the string as possible to a positive double.
   *
   * @author George A. David
   */
  public static double legacyConvertStringToPositiveDouble(String doubleString) throws BadFormatException, ValueOutOfRangeException
  {
    Assert.expect(doubleString != null);

    double positiveDouble = legacyConvertStringToDouble(doubleString);

    if (positiveDouble < 0)
      throw new ValueOutOfRangeException(positiveDouble, 0, Double.MAX_VALUE);

    return positiveDouble;
  }

  /**
   * Beginning with the first character of the string,
   * it converts as much of the string as possible to a double.
   *
   * @author George A. David
   */
  public static double legacyConvertStringToDouble(String doubleString, double minValue, double maxValue) throws BadFormatException, ValueOutOfRangeException
  {
    Assert.expect(doubleString != null);
    Assert.expect(minValue <= maxValue);

    double doubleValue = legacyConvertStringToDouble(doubleString);
    if (doubleValue < minValue || doubleValue > maxValue)
    {
      throw new ValueOutOfRangeException(doubleValue, minValue, maxValue);
    }

    return doubleValue;
  }

  /**
   * Convert to a pascal-style string, which is a simple array of chars, with the first char
   * being a number describing the size of the string.  The string cannot be longer than 255 characters long.
   *
   * @author Peter Esbensen
   */
  public static byte[] convertStringToPascalString(String string)
  {
    Assert.expect(string != null);
    Assert.expect(string.length() <= 255);

    byte[] byteString = string.getBytes();
    byte[] pascalString = new byte[byteString.length + 1];
    System.arraycopy(byteString, 0, pascalString, 1, byteString.length);
    pascalString[0] = (byte)byteString.length;

    return pascalString;
  }

  /**
   * Converts an integer to a String and pads the string with leading zeros
   * to make the length of the string at least minDigits.
   * @param intNumber the integer to be converted to a String
   * @param minDigits the minimum number of digits for the resulting integer
   * @return String
   * @author Kay Lannen
   */
  public static String convertIntegerToZeroPaddedString( int intNumber, int minDigits )
  {
    NumberFormat numberFormat = NumberFormat.getNumberInstance();
    numberFormat.setMinimumIntegerDigits( minDigits );
    numberFormat.setGroupingUsed( false );
    return( numberFormat.format(intNumber) );
  }

  /**
   * Joins the Strings in the specified Collection<String> together into one string seperated by the specified delimiter.
   *
   * @author Matt Wharton
   */
  public static String join(Collection<String> stringsToJoin, char delimiter)
  {
    Assert.expect(stringsToJoin != null);

    String[] stringsToJoinAsArray = stringsToJoin.toArray(new String[0]);
    return join(stringsToJoinAsArray, delimiter);
  }

  /**
   * Joins the specified String[] together into one string seperated by the specified delimiter.
   *
   * @author Matt Wharton
   */
  public static String join(String[] stringsToJoin, char delimiter)
  {
    Assert.expect(stringsToJoin != null);

    StringBuilder joinedStringBuilder = new StringBuilder();
    for (int i = 0; i < stringsToJoin.length; ++i)
    {
      joinedStringBuilder.append(stringsToJoin[i]);

      // Add a delimeter if we're not on the last item.
      if (i < (stringsToJoin.length - 1))
      {
        joinedStringBuilder.append(delimiter);
      }
    }

    return joinedStringBuilder.toString();
  }

  /**
   * Joins the Strings in the specified Collection<String> together into one string seperated by the specified delimiter.
   *
   * @author Matt Wharton
   */
  public static String join(Collection<String> stringsToJoin, CharSequence delimiter)
  {
    Assert.expect(stringsToJoin != null);
    Assert.expect(delimiter != null);

    String[] stringsToJoinAsArray = stringsToJoin.toArray(new String[0]);
    return join(stringsToJoinAsArray, delimiter);
  }

  /**
   * Joins the specified String[] together into one string seperated by the specified delimiter.
   *
   * @author Matt Wharton
   */
  public static String join(String[] stringsToJoin, CharSequence delimiter)
  {
    Assert.expect(stringsToJoin != null);
    Assert.expect(delimiter != null);

    StringBuilder joinedStringBuilder = new StringBuilder();
    for (int i = 0; i < stringsToJoin.length; ++i)
    {
      joinedStringBuilder.append(stringsToJoin[i]);

      // Add a delimeter if we're not on the last item.
      if (i < (stringsToJoin.length - 1))
      {
        joinedStringBuilder.append(delimiter);
      }
    }

    return joinedStringBuilder.toString();
  }

  private final static int ENCODING_EXPANSION = 3;  // Assumes most codes are byte size (2 hex digits plus comma).
  private final static int MIN_BUFFER_SIZE = 16;  // Avoid very small buffer sizes.
  /**
   * Convert a comma delimited string of hexadecimal codes into a string where
   * each character has the corresponding Unicode code point.
   * E.g., codesInHex of "42,6f,62" returns "Bob"
   *
   * @param codesInHex a comma delimited string of the hexadecimal codes.
   * @return the sequence of converted (Unicode code point) characters.
   * @author Bob Balliew
   */
  public static String convertHexCodedStringIntoUnicodeString(String codesInHex) throws CharConversionException, NumberFormatException
  {
    Assert.expect(codesInHex != null);
    StringBuffer stringBuf = new StringBuffer((codesInHex.length() / ENCODING_EXPANSION) + MIN_BUFFER_SIZE);
    /*
     * Allow any "VISIBLE" 'comma' character defined in the Unicode characters set:
     * ARABIC COMMA is '\u060c'.
     * ARMENIAN COMMA is '\u055d'.
     * ETHIOPIC COMMA is '\u1363'.
     * LATIN-1 MIDDLE DOT (Georgian comma) is '\u00b7'.
     * CJK IDEOGRAPHIC COMMA is '\u3001'.
     */
    final String DELIMITERS = ",\u060c\u055d\u1363\u00b7\u3001";
    StringTokenizer tokenizer = new StringTokenizer(codesInHex, DELIMITERS, false);

    while (tokenizer.hasMoreTokens())
    {
      String token = tokenizer.nextToken().trim();
      final int HEX_RADIX = 16;
      int number;

      try
      {
        number = Integer.parseInt(token, HEX_RADIX);
      }
      catch (NumberFormatException nfe)
      {
        throw new NumberFormatException(token);
      }

      if ((number < Character.MIN_VALUE) || (number > Character.MAX_VALUE))
        throw new CharConversionException(token);

      stringBuf.append((char)number);
    }

    return stringBuf.toString();
  }

  /**
   * Convert each character (Unicode code point) in the specified string into
   * its hexadecimal code.  Generate a comma delimited string of the hexadecimal
   * codes.
   * E.g., codePoints of "Bob" returns "42,6f,62"
   *
   * @param codePoints the sequence of characters to convert.
   * @return a comma delimited string of the hexadecimal codes.
   * @author Bob Balliew
   */
  public static String convertUnicodeStringIntoHexCodedString(String codePoints)
  {
    Assert.expect(codePoints != null);
    StringBuffer stringBuf = new StringBuffer((ENCODING_EXPANSION * codePoints.length()) + MIN_BUFFER_SIZE);
    final char sep = ',';

    if (codePoints.length() > 0)
    {
      // Convert the first (index = 0) character without a leading delimiter.
      stringBuf.append(Integer.toHexString(codePoints.charAt(0)));

      // Convert the second through Nth characters with leading (separating) delimiter.
      // If length of stringIn is 1 the loop is never excuted and no delimiter is output.
      for (int i = 1; i < codePoints.length(); ++i)
      {
        stringBuf.append(sep);
        stringBuf.append(Integer.toHexString(codePoints.charAt(i)));
      }
    }
    else
      stringBuf.append(sep);  // Ensure zero code points produces a non-empty string ",".

    return stringBuf.toString();
  }

  /**
   * @param csvString
   * @return
   * @throws CharConversionException
   * @throws NumberFormatException
   */
  public static double[] convertCSVStringToDoubleArray(String csvString) throws CharConversionException, NumberFormatException
  {
    Assert.expect(csvString != null);

    final String DELIMITERS = ",";
    StringTokenizer tokenizer = new StringTokenizer(csvString, DELIMITERS, false);
    double[] doubleArray = new double[tokenizer.countTokens()];
    int count = 0;

    while (tokenizer.hasMoreTokens())
    {
      String token = tokenizer.nextToken().trim();
      double number;

      try
      {
        number = Double.parseDouble(token);
        doubleArray[count] = number;

        count++;
      }
      catch (NumberFormatException nfe)
      {
        throw new NumberFormatException(token);
      }
    }

    return doubleArray;
  }


  /**
   * convert a Unicode String to ASCII-US
   * E.g., Unicode 16 returns "\u1234\u2234\u3334"
   *
   * @param unicodeString
   * @return asciiString
   * @author Wei Chin, Chong
   */
  public static String convertUnicodeStringToAsciiString(String unicodeString)
  {
    Assert.expect(unicodeString != null);

    String asciiString = "";
    Charset outCharset = Charset.forName("US-ASCII");
    CharsetEncoder encoder = outCharset.newEncoder();
    for (int i = 0; i < unicodeString.length(); ++i)
    {
      char ch = unicodeString.charAt(i);
      if (encoder.canEncode(ch))
      {
        asciiString += ch;
      }
      else
      {
        StringBuffer outBuffer = new StringBuffer(2048);
        if ((ch < 0x0020) || (ch > 0x007e))
        {
          outBuffer.append('\\');
          outBuffer.append('u');
          outBuffer.append(MathUtil.baseTenIntegerToHex((ch >> 12) & 0xF));
          outBuffer.append(MathUtil.baseTenIntegerToHex((ch >> 8) & 0xF));
          outBuffer.append(MathUtil.baseTenIntegerToHex((ch >> 4) & 0xF));
          outBuffer.append(MathUtil.baseTenIntegerToHex(ch & 0xF));
        }
        asciiString += outBuffer.toString();
      }
    }
    
    return asciiString;
  }

  /**
   * convert a ASCII-US back to Unicode String
   * E.g., convert String "\u1234\u2234\u3334" to Unicode 16
   *
   * @param asciiString
   * @return unicodeString
   * @author Wei Chin, Chong
   */
  public static String convertAcsiiStringToUnicodeString(String asciiString)
  {
    Assert.expect(asciiString != null);

    String unicodeTag = "\\u";
    StringBuffer unicodeString = new StringBuffer();
    int startIndex = 0;
    int endIndex = 0;
    int index = asciiString.indexOf(unicodeTag);
    int length = asciiString.length();

    boolean hasAsciiStringNeedToBeConvert = false;

    while(index >= 0 && endIndex + 6 <= length)
    {
      hasAsciiStringNeedToBeConvert = true;
      endIndex = startIndex + index;

      unicodeString.append(asciiString.substring(startIndex, endIndex));
      String asciiCodeToConvert = asciiString.substring(endIndex + 2, endIndex + 6);
      
      // if the ascii code cannot to be convert, ignore the conversion
      if(asciiCodeToConvert.matches(_hexRegularExpressionPattern) == false)
      {
        hasAsciiStringNeedToBeConvert = false;
        break;
      }
      
      char unicode = (char) Integer.parseInt(asciiCodeToConvert, 16);
      unicodeString.append(unicode);
      startIndex = endIndex + 6;
      index = asciiString.substring(startIndex,length).indexOf(unicodeTag);      
    }

    if(hasAsciiStringNeedToBeConvert == false)
      return asciiString;
    
    if(startIndex < length)
      unicodeString.append(asciiString.substring(startIndex));

    return unicodeString.toString();
  }

  /**
   * Convert each character (Unicode code point) in the specified string into
   * its hexadecimal code.  Generate a delimited string of the hexadecimal
   * codes.
   * E.g., codePoints of "Bob" returns "42<seperator>6f<seperator>62"
   *
   * @param codePoints the sequence of characters to convert.
   * @return a delimited string of the hexadecimal codes.
   * @author Chong, Wei Chin
   */
  public static String convertUnicodeStringIntoHexCodedString(String codePoints, char seperator, boolean isUpperCase)
  {
    Assert.expect(codePoints != null);
    StringBuilder stringBuf = new StringBuilder((ENCODING_EXPANSION * codePoints.length()) + MIN_BUFFER_SIZE);

    if (codePoints.length() > 0)
    {
      // Convert the 0 through Nth characters with leading (separating) delimiter.
      // If length of stringIn is 1 the loop is never excuted and no delimiter is output.
      for (int i = 0; i < codePoints.length(); ++i)
      {
        String appendString = Integer.toHexString((int)codePoints.charAt(i));
        if (appendString.length() == 1)
          stringBuf.append("0");
        if(i == (codePoints.length() -1))
          stringBuf.append(appendString);
        else
          stringBuf.append(appendString).append(seperator);
      }
    }
    else
      stringBuf.append(seperator);  // Ensure zero code points produces a non-empty string ",".

    if(isUpperCase)
      return stringBuf.toString().toUpperCase();
    
    return stringBuf.toString();
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  public static String replace(String originalString, String replacementString, int startIndex, int endIndex)
  {
    String output = originalString;
    if (originalString != null && replacementString != null)
    {
      String firstHalf = originalString.substring(0, startIndex);
      String secondHalf = originalString.substring(endIndex + 1);
      output = firstHalf + replacementString + secondHalf;
    }
    return output;
  }

  /**
   * Convert a date-time string to equivalent time in seconds
   * Expect format "HH-MM-SS"
   * HH = hour
   * MM = minutes
   * SS = seconds
   * @author Khaw Chek Hau
   */
  public static int convertStringToTimeInSeconds(String hourTime) throws BadFormatException
  {
    Assert.expect(hourTime != null);
    
    int timeInSeconds = 0;
    
    Matcher matcher = _hourTimePattern.matcher(hourTime);
    if (matcher.matches())
    {
      String hourStr = matcher.group(1);
      String minuteStr = matcher.group(2);
      String secondStr = matcher.group(3);
      int hour = Integer.valueOf(hourStr);
      int minute = Integer.valueOf(minuteStr);
      int second = Integer.valueOf(secondStr);
      
      timeInSeconds = second + (60 * minute) + (3600 * hour);
    }
    else
    {
      throw new BadFormatException(new LocalizedString("EX_STRING_TO_LONG_BAD_FORMAT_KEY", new Object[]{hourTime}));
    }

    return timeInSeconds;
  }
  
  /**
   * Convert time in seconds to a hour-time timestamp in
   * format "HH-MM-SS"
   * @author Khaw Chek Hau
   */
  public static String convertSecondsToString(int timeInSeconds)
  {
    Assert.expect(timeInSeconds >= 0,
                  "StringUtil.convertMilliSecondsToTimeAndDate() - Time cannot be negative.");

    int hour = timeInSeconds / 3600;
    int minute = (timeInSeconds % 3600) / 60;
    int second = timeInSeconds % 60;
    
    return String.format("%02d:%02d:%02d", hour, minute, second);
  }
}
