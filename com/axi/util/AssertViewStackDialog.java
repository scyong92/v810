
//Title:        AssertViewStackDialog
//Version:
//Copyright:    Copyright (c) 2000
//Author:       Steve Anonson
//Company:      Agilent Technologies
//Description:  Text dialog to view a stack trace.

package com.axi.util;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

/**
* This class provides a dialog window displaying a stack trace message.
*
* @author Steve Anonson
*/
public class AssertViewStackDialog extends JDialog
{
  private static String _title = "View Stack Trace";

  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelLayout = new BorderLayout();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _textPanel = new JPanel();
  private JScrollPane _textAreaScrollPane = new JScrollPane();
  private JTextArea _textArea = new JTextArea();
  private Border _textAreaBorder = null;
  private JButton _closeButton = new JButton();
  private BorderLayout borderLayout1 = new BorderLayout();

  /**
  * This method fills in the dialog data after the contents of the dialog
  * has been created.
  *
  * @author Steve Anonson
  */
  private void init(String stackTrace)
  {
    jbInit();
    if (stackTrace == null)
      _textArea.setText("no stack trace");
    else
      _textArea.setText(stackTrace);
    pack();
  }

  /**
  * This contructor takes the stack trace information and creates a
  * dialog with a Frame parent.
  *
  * @param frame - Frame parent of the dialog, can be null.
  * @param modal - flag to determine if the dialog is modal.
  * @param stackTrace - String containing the stack trace.
  * @author Steve Anonson
  */
  public AssertViewStackDialog(Frame frame, boolean modal, String stackTrace)
  {
    super(frame, _title, modal);
    init(stackTrace);
  }

  /**
  * This contructor takes the stack trace information and creates a
  * dialog with a Dialog parent.
  *
  * @param parent - dialog parent of the dialog, can be null.
  * @param modal - flag to determine if the dialog is modal.
  * @param stackTrace - String containing the stack trace.
  * @author Steve Anonson
  */
  public AssertViewStackDialog(Dialog parent, boolean modal, String stackTrace)
  {
    super(parent, _title, modal);
    init(stackTrace);
  }

  /*
  * This constructor is hidden.
  */
  private AssertViewStackDialog()
  {
    /* Don't use this constructor */
  }

  /**
  * This method creates the contents of the dialog.
  *
  * @author Steve Anonson
  */
  private void jbInit()
  {
    _textAreaBorder = BorderFactory.createEmptyBorder(5,5,5,5);
    _mainPanel.setLayout(_mainPanelLayout);
    _textArea.setColumns(60);
    _textArea.setRows(10);
    _textArea.setBorder(_textAreaBorder);
    _textArea.setText("stack trace");
    _textArea.setEditable(false);
    _textArea.setFont(new java.awt.Font("SansSerif", 0, 16));
    _textAreaScrollPane.setViewportBorder(BorderFactory.createEtchedBorder());
    _closeButton.setFont(new java.awt.Font("SansSerif", 0, 16));
    _closeButton.setMnemonic('C');
    _closeButton.setText("Close");
    _closeButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        closeButton_actionPerformed(e);
      }
    });
    _textPanel.setLayout(borderLayout1);
    getContentPane().add(_mainPanel);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
    _buttonPanel.add(_closeButton, null);
    _mainPanel.add(_textPanel, BorderLayout.CENTER);
    _textPanel.add(_textAreaScrollPane, BorderLayout.CENTER);
    _textAreaScrollPane.getViewport().add(_textArea, null);

    // Set up the default button.
    getRootPane().setDefaultButton(_closeButton);
  }

  /**
  * This is the method that gets called when the close button is pressed.
  *
  * @author Steve Anonson
  */
  private void closeButton_actionPerformed(ActionEvent e)
  {
    setVisible(false);
    dispose();
  }

  /*
  * Main function used only for testing.
  */
  public static void main( String [] args )
  {
    String msg = "Here is a message to put \nin the text area.";
    AssertViewStackDialog dlg = new AssertViewStackDialog((Frame)null, true, msg);
    // Add listener to exit if dialog is closed.
    dlg.addWindowListener( new WindowAdapter()
    {
      public void windowClosing( WindowEvent e )
      {
        System.exit(0);
      }
    } );
    dlg.setVisible(true);
  }
}

