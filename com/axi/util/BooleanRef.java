package com.axi.util;

import java.io.*;

/**
* BooleanRef allows booleans to be passed by reference.  A BooleanRef
* can be passed to a method and have its value changed by the method that
* it was passed into.
* This class is needed because the java.lang.Boolean class does not provide
* a set method.
*
* @author Bill Darbie
*/
public class BooleanRef implements Serializable
{
  public static final BooleanRef TRUE = new BooleanRef(true);
  public static final BooleanRef FALSE = new BooleanRef(false);

  private boolean _value;

  public BooleanRef()
  {
    _value = false;
  }

  /**
   * @author Bill Darbie
   */
  public BooleanRef(boolean value)
  {
    _value = value;
  }

  /**
   * @author Bill Darbie
   */
  public BooleanRef(BooleanRef booleanRef)
  {
    Assert.expect(booleanRef != null);

    _value = booleanRef._value;
  }

  /**
   * @author Bill Darbie
   */
  public void setValue(boolean value)
  {
    _value = value;
  }

  /**
   * @author Bill Darbie
   */
  public boolean getValue()
  {
    return _value;
  }

  /**
   * @author Bill Darbie
   */
  public boolean equals(Object rhs)
  {
    if (rhs == this)
      return true;

    if (rhs instanceof BooleanRef)
    {
      BooleanRef booleanRef = (BooleanRef)rhs;
      if (_value == booleanRef._value)
        return true;
    }

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public int hashCode()
  {
    if (_value == false)
      return 0;

    return 1;
  }
}
