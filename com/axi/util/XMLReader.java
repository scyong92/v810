package com.axi.util;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import java.util.*;
import javax.xml.parsers.*;

import org.xml.sax.*;
import org.xml.sax.helpers.*;

/**
 * Provide a way to read in an XML file that was written by the XMLWriter class.  Note that
 * any class read in by this Reader must provide a default constructor.  The Integer, Boolean,
 * Double, and Float classes are the only exceptions.
 *
 * @author Bill Darbie
 */
public class XMLReader extends DefaultHandler
{
  private SAXParser _parser = null;
  private Locator _locator;

  private Map<Integer, Object> _idToObjectMap = new HashMap<Integer, Object>();
  private Object _root = null;

  // current variables
  private Class _class;
  private Object _instance;
  private String _classTypeName;
  private String _elementName;
  // stacks of the current variables
  private List<Class> _classes = new ArrayList<Class>();
  private List<String> _elements = new ArrayList<String>();
  private List<Object> _instances = new ArrayList<Object>();

  private Integer _id = null;
  private int _numElements = 0;
  // maps class name to Class objects - it is faster to lookup in the Map then it is using reflection each tim
  private Map<String, Class> _classNameToClass = new HashMap<String, Class>();
  // maps class to a map of field name to field - quicker then using reflection each time
  private Map<Class, Map<Class, Field>> _classToFieldMaps = new HashMap<Class, Map<Class, Field>>();

  private Object _key = null;

  /**
   * @author Bill Darbie
   */
  public XMLReader()
  {
    SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
    try
    {
      _parser = saxParserFactory.newSAXParser();
    }
    catch(SAXException se)
    {
      se.printStackTrace();
    }
    catch(ParserConfigurationException pce)
    {
      pce.printStackTrace();
    }
  }

  /**
   * @author Bill Darbie
   */
  public Object readXML(Reader is) throws IOException, SAXException
  {
    InputSource inputSource = new InputSource(is);
    _parser.parse(inputSource, this);

    return _root;
  }

  /**
   * This method automatically gets called by the SAXParser at the start of the XML document parsing.
   *
   * @author Bill Darbie
   */
  public void startDocument() throws org.xml.sax.SAXException
  {
    cleanup();
  }

  /**
   * This method automatically gets called by the SAXParser at the end of the XML document parsing.
   *
   * @author Bill Darbie
   */
  public void endDocument() throws org.xml.sax.SAXException
  {
    _root = _instance;

    cleanup();
  }

  /**
   * @author Bill Darbie
   */
  private void cleanup()
  {
    _classes.clear();
    _elements.clear();
    _instances.clear();
    _classNameToClass.clear();
    _classToFieldMaps.clear();
  }

  /**
   * This method automatically gets called by the SAXParser to parse in the data between tags
   * [element] characters in here are what this method gets[/element]
   *
   * @author Bill Darbie
   */
  public void characters(char[] characters, int start, int length) throws org.xml.sax.SAXException
  {
    String str = new String(characters, start, length).trim();
    if (str.equals("") == false)
    {
      Object value = null;
      Class clazz = _class;
      Object instance = _instance;
      if (str.equals("null") == false)
      {
        if (_instance == null)
        {
          // if the instance is null then we have a variable that could not be instantiated.
          // find out which of the allowed cases it is
          if (_classTypeName.equals("int"))
            value = new Integer(str);
          else if (_classTypeName.equals("boolean"))
            value = new Boolean(str);
          else if (_classTypeName.equals("float"))
            value = new Float(str);
          else if (_classTypeName.equals("double"))
            value = new Double(str);
          else if (_classTypeName.equals("java.lang.String"))
            value = str;
          else if (_classTypeName.equals("java.lang.Integer"))
            value = new Integer(str);
          else if (_classTypeName.equals("java.lang.Float"))
            value = new Float(str);
          else if (_classTypeName.equals("java.lang.Double"))
            value = new Double(str);
          else if (_classTypeName.equals("java.lang.Boolean"))
            value = new Boolean(str);
          else
            Assert.expect(false);

          // replace the original instance with the correct one for this id
          if (_id != null)
            _idToObjectMap.put(_id, value);
        }
      }

      clazz = _classes.get(_classes.size() - 2);
      instance = _instances.get(_instances.size() - 2);

      // now that we know what the value of the field is, set it
      setField(clazz, instance, _elementName, value);
    }
  }

  /**
   * This method gets called automatically by the SAXParser to read in an element
   * [anElement]someData[/anElement]
   *
   * @author Bill Darbie
   */
  public void startElement(String uri, String localName, String qualifiedName, Attributes attributes) throws org.xml.sax.SAXException
  {
    _class = null;
    _instance = null;

    // figure out the name of the variable
    _elementName = localName;
    if (_elementName.equals(""))
      _elementName = qualifiedName;
    // put the current element on the elements stack
    _elements.add(_elementName);

    int length = attributes.getLength();
    for(int i = 0; i < length; ++i)
    {
      // get the name of the xml attribute - usually type or id
      String attName = attributes.getLocalName(i);
      if (attName.equals(""))
        attName = attributes.getQName(i);
      String attValue = attributes.getValue(i);

      _id = null;
      if (attName.equals(XMLParseUtil.TYPE))
      {
        // get the class name
        _classTypeName = XMLParseUtil.getClassNameFromXMLName(attValue);
      }
      else if (attName.equals(XMLParseUtil.NUMELEMENTS))
      {
        // numElements will specify the number of elements of the array that is being read in
        _numElements = Integer.parseInt(attValue);
      }
      else if (attName.equals(XMLParseUtil.ID))
      {
        // id is the unique integer value associated with every instance
        _id = new Integer(attValue);
        _instance = _idToObjectMap.get(_id);
        if (_instance == null)
        {
          // the object has not be instantiated yet, so create it
          try
          {
            _class = (Class)_classNameToClass.get(_classTypeName);
            if (_class == null)
            {
              // this is the first time this class has been instantiated, so use reflection
              // to find the Class object.  This lookup can be slow, which is why I use
              // a map to look up previous objects
              _class = Class.forName(_classTypeName);
              _classNameToClass.put(_classTypeName, _class);
            }
            _id = null;
            if (_class.isArray() == false)
            {
              // the class is not an array - try to create an instance using newInstance
              try
              {
                // do not create an instance of a String here since an empty string will be
                // created.  Let the characters method create the String with the correct
                // value in it
                if (_class != String.class)
                  _instance = _class.newInstance();
              }
              catch(InstantiationException e)
              {
                // wpd - this exception is thrown for classes that do not have a default constructor
                //       the instance will be created in the characters() method because that is where
                //       the value of the constructor is known for the built in types Integer, Boolean
                //       Double, Float
              }
            }
            else
            {
              // the class is an array - use Array.newInstance
              Class componentType = _class.getComponentType();
              _instance = Array.newInstance(componentType, _numElements);
              Class clazz = _classes.get(_classes.size() - 1);
              Object instance = _instances.get(_instances.size() - 1);
              setField(clazz, instance, _elementName, _instance);
            }

            if (_instance != null)
              _idToObjectMap.put(_id, _instance);
          }
          catch(Exception e)
          {
            e.printStackTrace();
            throw new SAXParseException("class not found: " + _classTypeName, _locator, e);
          }
        }
        else
        {
          // the object has already been created, just set the variable to the object already in memory
          Object instance = _instances.get(_instances.size() - 1);
          Class clazz = _classes.get(_classes.size() - 1);
          setField(clazz, instance, _elementName, _instance);
        }
      }
    }

    // put the current class and instance found on the stacks
    _classes.add(_class);
    _instances.add(_instance);
  }

  /**
   * Find the field object for a variable in a class.
   *
   * @param clazz is the class that the variable is in.
   * @param instance is the instance of the object that the variable is in
   * @param fieldName is the name of the variable we are trying to find
   *
   * @author Bill Darbie
   */
  private Field findField(Class clazz, Object instance, String fieldName) throws SAXParseException
  {
    Field field = null;
    // see if the fields of the class are already in the _classToFieldMap map
    Map<Class, Field> classToField = _classToFieldMaps.get(clazz);
    if (classToField != null)
    {
      // see if the field was already found before
      field = classToField.get(fieldName);
    }
    else
    {
      // create a new fieldNameToField map
      classToField = new HashMap<Class, Field>();
    }
    if (field == null)
    {
      // try to find the field for the first time - next time it will already be in the map
      // the map is much faster than using reflection
      while(clazz != Object.class)
      {
        try
        {
          field = clazz.getDeclaredField(fieldName);
        }
        catch(NoSuchFieldException nsfe)
        {
          // do nothing
        }
        // if field is null goto the super class and try again
        if (field == null)
          clazz = clazz.getSuperclass();
        else
          break;
      }
      if (field != null)
      {
        classToField.put(clazz, field);
        _classToFieldMaps.put(clazz, classToField);
      }
    }

    Assert.expect(field != null);
    return field;
  }

  /**
   * Set the variable of an instance to a particular value.
   *
   * @param clazz is the class that the variable is in.
   * @param instance is the instance of the object that the variable is in
   * @param fieldName is the name of the variable we are trying to find
   * @param value is the value that the fieldName should be set to
   *
   * @author Bill Darbie
   */
  private void setField(Class clazz, Object instance, String fieldName, Object value) throws SAXParseException
  {
    if (clazz.isArray())
    {
      // if the class is an Array we need to use Array.set
      int startIndex = XMLParseUtil.ELEMENT .length();
      String indexStr = fieldName.substring(startIndex, fieldName.length());
      int index = Integer.valueOf(indexStr).intValue();
      Array.set(instance, index, value);
    }
    else if (instance instanceof AbstractCollection)
    {
      // abstract collection classes use transient variables, so they must be handled with this
      // special case
      AbstractCollection collection = (AbstractCollection)instance;
      /** Warning "unchecked cast" approved for cast from Object types.*/
      collection.add(value);
    }
    else if (instance instanceof AbstractMap)
    {
      // abstract map classes use transient variables, so they must be handled with this
      // special case
      /** Warning "unchecked cast" approved for cast from Object types.*/
      AbstractMap<Object, Object> map = (AbstractMap<Object, Object>)instance;
      if (fieldName.equals(XMLParseUtil.KEY))
        _key = value;
      else if (fieldName.equals(XMLParseUtil.VALUE))
      {
        map.put(_key, value);
        _key = null;
      }
    }
    else
    {
      // the class is not an array so field.set will work
      Field field = findField(clazz, instance, fieldName);
      field.setAccessible(true);
      try
      {
        field.set(instance, value);
      }
      catch(IllegalAccessException iae)
      {
        iae.printStackTrace();
        throw new SAXParseException("could not find the field for variable" + fieldName, _locator, iae);
      }
    }
  }

  /**
   * This method gets called automatically by the SAXParser when then end element is parsed
   * [element]some data[/element]
   *
   * @author Bill Darbie
   */
  public void endElement(String uri, String localName, String qualifiedName) throws org.xml.sax.SAXException
  {
    String elemName = localName;
    if (elemName.equals(""))
      elemName = qualifiedName;

    // clear the stacks so the top data is valid for where we are in the XML file
    _classes.remove(_classes.size() - 1);
    if (_classes.size() > 0)
      _class = _classes.get(_classes.size() - 1);
    _instances.remove(_instances.size() - 1);
    if (_instances.size() > 0)
      _instance = _instances.get(_instances.size() - 1);
    _elements.remove(_elements.size() - 1);
    if (_elements.size() > 0)
      _elementName = _elements.get(_elements.size() - 1);
  }

  /**
   * This method is called by the parser to give access to information about where the parse
   * was in the file when any errors occur.
   *
   * @author Bill Darbie
   */
  public void setDocumentLocator(Locator locator)
  {
    _locator = locator;
  }


}
