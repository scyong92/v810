package com.axi.util;

import java.io.*;

/**
 * @author Cheah Lee Herng
 */
public class RuntimeExec 
{
  private final String _EMPTY_SPACE = " ";
  
  private static RuntimeExec _instance = null;
  
  private Runtime _runtime;
  
  /**
   * @author Cheah Lee Herng
   */
  private RuntimeExec()
  {
    _runtime = Runtime.getRuntime();    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static RuntimeExec getInstance()
  {
    if(_instance == null)
    {
      // default path
      _instance = new RuntimeExec();
    }
    return _instance;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public ExitCodeEnum execProcess(String processNameFullPath, String arguments) throws IOException
  {
    Assert.expect(processNameFullPath != null);
    Assert.expect(arguments != null);
    
    // Process path by adding quotes
    processNameFullPath = addQuote(processNameFullPath);
    arguments = addQuote(arguments);
    
    Process p = Runtime.getRuntime().exec(processNameFullPath + _EMPTY_SPACE + arguments);
    try
    {
      p.waitFor();
    }
    catch (InterruptedException ex)
    {
      // do nothing
    }
    return handleExitCode(p.exitValue());
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private ExitCodeEnum handleExitCode(int exitValue)
  {
    if(exitValue == 1)
    {
      return ExitCodeEnum.GENERAL_ERRORS;
    }
    else if(exitValue == 2)
    {
      return ExitCodeEnum.MISUSE_ERROR;
    }
    else if(exitValue == 126)
    {
      return ExitCodeEnum.CANNOT_EXECUTE_ERROR;
    }
    else if(exitValue == 127)
    {
      return ExitCodeEnum.NO_COMMAND_ERROR;
    }
    else if(exitValue == 128)
    {
      return ExitCodeEnum.INVALID_ARGUMENT_ERROR;
    }
    else if(exitValue > 128 && exitValue <= 165)
    {
      return ExitCodeEnum.FATAL_ERROR;
    }
    return ExitCodeEnum.NO_ERRORS;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private String addQuote(String originalString)
  {
    Assert.expect(originalString != null);

    originalString = "\"" + originalString + "\"";

    return originalString;
  }
}
