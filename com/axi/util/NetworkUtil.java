package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * @author George A. David
 */
public class NetworkUtil
{
  private static NetworkUtil _instance;

  // Chnee Khang Wah, 2011-12-12, Low Cost AXI
  static 
  {
    System.loadLibrary("nativeAxiUtil"); // load the JNI library
  }
  
  /**
   * @author George A. David
   */
  private NetworkUtil()
  {
    // do nothing
  }

  /**
   * @author George A. David
   */
  public static synchronized NetworkUtil getInstance()
  {
    if(_instance == null)
      _instance = new NetworkUtil();

    return _instance;
  }

  /**
   * @param isWireless
   * @return String MacAddress
   *
   * @author Chong, Wei Chin
   */
  public static String getMACAddress(boolean isWireless)
  {
    boolean isLAC = false;
    boolean isWNC = false;

    String address = "";

    String os = System.getProperty("os.name");
    if (os != null && os.startsWith("Windows"))
    {
      try
      {
        String command = "cmd.exe /c ipconfig /all";
        Process p = Runtime.getRuntime().exec(command);
        BufferedReader br =
                new BufferedReader(
                new InputStreamReader(p.getInputStream()));
        String line;
        while ((line = br.readLine()) != null)
        {
          if(line.indexOf("Local Area Connection") > 0)
          {
            isLAC = true;
          }

          if(line.indexOf("Wireless Network Connection") > 0)
          {
            isWNC = true;
          }

          if (line.indexOf("Media State") > 0)
          {
            isLAC = isWNC = false;
          }

          if (line.indexOf("Physical Address") > 0)
          {
            int index = line.indexOf(":");
            index += 2;
            if(isLAC & isWireless == false)
            {
              address = line.substring(index);
              break;
            }
            if(isWNC & isWireless)
            {
              address = line.substring(index);
              break;
            }
          }
        }
        br.close();
        return address.trim();
      }
      catch (IOException e)
      {
      }
    }
    return address;
  }

  /**
   * @return String MacAddress
   * @author Chong, Wei Chin
   */
/*
  public static List<String> getMACAddresses()
  {
    boolean isLAC = false;
    boolean isWNC = false;

    List <String> address = new ArrayList<String>();

    String os = System.getProperty("os.name");
    if (os != null && os.startsWith("Windows"))
    {
      try
      {
        String command = "cmd.exe /c ipconfig /all";
        Process p = Runtime.getRuntime().exec(command);
        BufferedReader br =
                new BufferedReader(
                new InputStreamReader(p.getInputStream()));
        String line;
        while ((line = br.readLine()) != null)
        {
          if(line.indexOf("Local Area Connection") > 0)
          {
            isLAC = true;
          }

          if(line.indexOf("Wireless Network Connection") > 0)
          {
            isWNC = true;
          }

          if (line.indexOf("Physical Address") > 0)
          {
            int index = line.indexOf(":");
            index += 2;
            if(isLAC || isWNC)
            {
              address.add(line.substring(index).trim());
              isLAC = isWNC = false;
            }
          }
        }
        br.close();
      }
      catch (IOException e)
      {
      }
    }
    return address;
  }
*/

  /**
   * @author George A. David
   */
  public boolean isNetworkShareAvailable(String serverName,
                                         String shareName,
                                         NetworkShareTypeEnum shareType)
  {
    Assert.expect(serverName != null);
    Assert.expect(shareName != null);

    return nativeIsNetworkShareAvailable(serverName, shareName, shareType.getId());
  }

  /**
   * @author George A. David
   */
  public boolean isPathSharedAs(String path,
                                String serverName,
                                String shareName,
                                NetworkShareTypeEnum shareType)
  {
    Assert.expect(path != null);
    Assert.expect(serverName != null);
    Assert.expect(shareName != null);

    boolean isPathSharedAs = false;
    if(isNetworkShareAvailable(serverName, shareName, shareType))
    {
      isPathSharedAs = nativeIsPathSharedAs(path, serverName, shareName);
    }
    return isPathSharedAs;
  }

  /**
   * @author George A. David
   */
  public void createNetworkShare(String path,
                                 String shareName,
                                 NetworkShareTypeEnum shareType,
                                 String description,
                                 int maxUses,
                                 boolean isReadOnly,
                                 String password)
  {
    Assert.expect(path != null);
    Assert.expect(shareName != null);
    Assert.expect(shareType != null);
    Assert.expect(description != null);
    Assert.expect(password != null);

    nativeCreateNetworkShare(path,
                             shareName,
                             shareType.getId(),
                             description,
                             maxUses,
                             isReadOnly,
                             password);
  }
  
	/**
   * 
   * @return
   * @author Wei Chin, Chong
   */
  public List<String> getMACAddresses()
  {
    List<String> macAddress = new ArrayList<String>();
    nativeGetListMacAddress(macAddress);
    return macAddress;
  }

  private native int nativeCreateNetworkShare(String path,
                                              String shareName,
                                              int shareType,
                                              String description,
                                              int maxUses,
                                              boolean isReadOnly,
                                              String password);

  private native boolean nativeIsNetworkShareAvailable(String serverName,
                                                       String shareName,
                                                       int shareType);

  private native boolean nativeIsPathSharedAs(String path,
                                             String serverName,
                                             String shareName);

	private native void nativeGetListMacAddress(List<String> macAddress);
}

