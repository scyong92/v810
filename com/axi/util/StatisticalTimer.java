package com.axi.util;

/**
 *
 * similar to TimerUtil, but keeps a running averaged and standard
 *  deviation of the length of time periods between calls to start()
 *  and stop() rather than adding them.
 *
 * durations are probably more of a Poisson distribution,
 *  but average and standard deviation are better known
 *
 * @author Greg Loring
 */
public class StatisticalTimer
{
  final String _activity;
  final long _maxSamples;

  TimerUtil _timer = new TimerUtil();
  double _sumOfValues;
  double _sumOfSquares;
  long _numSamples;
  double _average;
  double _standardDeviation;

  /**
   * @author Greg Loring
   */
  public StatisticalTimer(String activity, long maxSamples)
  {
    Assert.expect(activity != null);
    Assert.expect(maxSamples > 0);

    _activity = activity;
    _maxSamples = maxSamples;

    reset();
  }

  /**
   * @author Greg Loring
   */
  public StatisticalTimer(String activity)
  {
    this (activity, Long.MAX_VALUE);
  }

  /**
   * @author Greg Loring
   */
  public void reset()
  {
    _timer.reset();
    _sumOfValues = 0.;
    _sumOfSquares = 0.;
    _numSamples = 0;
    _average = 0.;
    _standardDeviation = 0.;
  }

  /**
   * @author Greg Loring
   */
  public void start()
  {
    _timer.reset();
    _timer.start();
  }

  public void stop()
  {
    _timer.stop();

    if (_numSamples >= _maxSamples)
    {
      Assert.expect(_numSamples == _maxSamples);
      _sumOfValues -= _average;
      _sumOfSquares -= _sumOfSquares / _numSamples;
      --_numSamples;
    }
    Assert.expect(_numSamples < _maxSamples);

    double value = _timer.getElapsedTimeInMillis();
    _sumOfValues += value;
    _sumOfSquares += value * value;
    ++_numSamples;
    Assert.expect(_numSamples > 0);
    _average = _sumOfValues / _numSamples;
    double variance = (_sumOfSquares / _numSamples) - (_average * _average);
    Assert.expect(variance >= 0.);
    _standardDeviation = Math.sqrt(variance);
  }

  /**
   * @author Greg Loring
   */
  public long getNumberOfSamples()
  {
    return _numSamples;
  }

  /**
   * @author Greg Loring
   */
  public long getAverageElapsedTimeInMillis()
  {
    return (long) Math.ceil(_average);
  }

  /**
   * @author Greg Loring
   */
  public long getStandardDeviationOfElapsedTimeInMillis()
  {
    return (long) Math.ceil(_standardDeviation);
  }

  /**
   * @author Greg Loring
   */
  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append(_activity);
    sb.append("(");
    sb.append(_numSamples);
    sb.append(")=");
    sb.append(_timer.getElapsedTimeInMillis());
    sb.append(",Mean=");
    sb.append(getAverageElapsedTimeInMillis());
    sb.append("+/-");
    sb.append(getStandardDeviationOfElapsedTimeInMillis());
    sb.append("ms");
    String result = sb.toString();
    return result;
  }

}
