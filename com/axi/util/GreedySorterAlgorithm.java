package com.axi.util;

import java.awt.*;
import java.util.*;

/**
 * This is a sorting algorithm that find the nearest distance from one location to another location.
 * @author Kok Chun, Tan
 */
public class GreedySorterAlgorithm
{
  private static GreedySorterAlgorithm _instance;
  private java.util.List<Point> _points = new ArrayList<Point>();
  private java.util.List<Point> _sortedPoints = new ArrayList<Point>();
  
  /**
   * @author Kok Chun, Tan
   */
  public GreedySorterAlgorithm()
  {
    //nothing
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static synchronized GreedySorterAlgorithm getInstance()
  {
    if (_instance == null)
      _instance = new GreedySorterAlgorithm();

    return _instance;
  }
  
  /**
   * Add a point.
   * @author Kok Chun, Tan
   */
  public void addPoint(int x, int y)
  {
    Point locationPoint = new Point(x, y);
    _points.add(locationPoint);
  }
  
  /**
   *  Add all points from a list of points.
   * @author Kok Chun, Tan
   */
  public void addPoints(java.util.List<Point> points)
  {
    _points.addAll(points);
  }
  
  /**
   * Find next nearest point.
   * @author Kok Chun, Tan
   */
  private Point findNextNearestPoint()
  {
    Point startPoint = _points.get(0);
    java.util.List<Double> distanceList = new ArrayList<Double>();
    java.util.List<Double> unsortdistanceList = new ArrayList<Double>();
    int index = 0;
    
    if (_points.size() != 1)
    {
      for (int i = 1; i < _points.size(); i++)
      {
        Point endPoint = _points.get(i);
        distanceList.add(startPoint.distance(endPoint));
        unsortdistanceList.add(startPoint.distance(endPoint));
      }

      Collections.sort(distanceList);
      double shortestDistance = distanceList.get(0);
      for (int i = 0; i < unsortdistanceList.size(); i++)
      {
        double testDistance = unsortdistanceList.get(i);
        if (shortestDistance==testDistance)
        {
          index = i + 1;
          _points.remove(0);
          Collections.swap(_points, 0, index - 1);
          break;
        }
      }
    }
    
    return _points.get(0);
  }
  
  /**
   * Sort and return GreedySorterAlgorithm location list.
   * @author Kok Chun, Tan
   */
  public java.util.List<Point> sort()
  {
    clear();

    _sortedPoints.add(_points.get(0));
    
    while(_points.size() != 1)
    {
      _sortedPoints.add(findNextNearestPoint());
    }
    
    return _sortedPoints;
  }
  
  /**
   * Sort and return GreedySorterAlgorithm location list by given a list.
   * @author Kok Chun, Tan
   */
  public java.util.List<Point> sort(java.util.List<Point> points)
  {
    Assert.expect(points != null);
    clear();
    
    addPoints(points);
    _sortedPoints.add(_points.get(0));
    
    while(_points.size() != 1)
    {
      _sortedPoints.add(findNextNearestPoint());
    }
    
    return _sortedPoints;
  }

  /**
   * Get total distance of the GreedySorterAlgorithm location list.
   * @author Kok Chun, Tan
   */
  public int getTotalDistance()
  {
    double totalDistance = 0.0;

    for (int point = 0; point < _sortedPoints.size(); point++)
    {
      Point fromPoint = _sortedPoints.get(point);

      Point toPoint;

      if (point + 1 < _sortedPoints.size())
      {
        toPoint = _sortedPoints.get(point+1);
        totalDistance += fromPoint.distance(toPoint);
      }
    }

    return (int)totalDistance;
  }
  
  /**
   * Clear location point list
   * @author Kok Chun, Tan
   */
  public void clear()
  {
    _points.clear();
    _sortedPoints.clear();
  }
}
