//#define UNICODE
#include <windows.h>
#include <Lm.h>

#include <iostream>

#include "JniUtil.h"
#include "util/src/sptAssert.h"
#include "util/src/WindowUtil.h"
#include "util/src/MacUtil.h"

#include "com_axi_util_NetworkUtil.h"

using namespace std;

unsigned int
getShareType(jint shareTypeId)
{
  unsigned int shareType = 0;
  if(shareTypeId == 0)
    shareType = STYPE_DISKTREE;
  else if (shareTypeId == 1)
    shareType = STYPE_PRINTQ;
  else if (shareTypeId == 2)
    shareType = STYPE_DEVICE;
  else if (shareTypeId == 3)
    shareType = STYPE_IPC;
  else if (shareTypeId == 4)
    shareType = STYPE_SPECIAL;
  else
    sptAssert(false);
  
  return shareType;
}

/*
 * @author George A. David
 */
JNIEXPORT jint JNICALL 
Java_com_axi_util_NetworkUtil_nativeCreateNetworkShare(JNIEnv* pEnv, 
                                                           jobject object, 
                                                           jstring path, 
                                                           jstring shareName, 
                                                           jint shareTypeId, 
                                                           jstring description,
                                                           jint maxUses, 
                                                           jboolean isReadOnly,
                                                           jstring password)
{
  try
  {
    unsigned int status =  WindowUtil::createNetworkShare(getSTLString(pEnv, path),
                                                          getSTLString(pEnv, shareName),
                                                          getShareType(shareTypeId),
                                                          getSTLString(pEnv, description),
                                                          maxUses,
                                                          isReadOnly,
                                                          getSTLString(pEnv, password));
    return status;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * @author George A. David
 */
JNIEXPORT jboolean JNICALL 
Java_com_axi_util_NetworkUtil_nativeIsNetworkShareAvailable(JNIEnv* pEnv, 
                                                                jobject object, 
                                                                jstring serverName, 
                                                                jstring shareName,
                                                                jint shareTypeId)
{
  try
  {
    return WindowUtil::isNetworkShareAvailable(getSTLString(pEnv, serverName),
                                               getSTLString(pEnv, shareName),
                                               getShareType(shareTypeId));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * @author George A. David
 */
JNIEXPORT jboolean JNICALL 
Java_com_axi_util_NetworkUtil_nativeIsPathSharedAs(JNIEnv* pEnv,
                                                       jobject object, 
                                                       jstring path,
                                                       jstring serverName, 
                                                       jstring shareName)
{
  try
  {
    return WindowUtil::isPathSharedAs(getSTLString(pEnv, path),
                                      getSTLString(pEnv, serverName),
                                      getSTLString(pEnv, shareName));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/*
 *  Get the macAddresses from Master Controller / TDW
 *  @author Wei Chin, Chong
 */
JNIEXPORT void JNICALL 
Java_com_axi_util_NetworkUtil_nativeGetListMacAddress( JNIEnv* pEnv,
                                                       	jobject object,
                                                       	jobject macAddress)
{
	try
	{
		list<string> cppMAcAddress = MacUtil::getMacAddress();
  	loadJavaStringArrayFromStlStringList(pEnv, macAddress, cppMAcAddress);  
  }  	
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}
