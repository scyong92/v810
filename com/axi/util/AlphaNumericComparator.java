package com.axi.util;

import java.io.*;
import java.math.*;
import java.util.*;

/**
 * This class will compare any two Strings.  It will take numbers into account to
 * determine ordering.  It is case insensitive.
 * For example it will order things like this:
 *
 * a1
 * A2
 * a3
 * a12
 * B94
 * B100
 *
 * @author Matt Wharton
 */
public class AlphaNumericComparator implements Comparator<Object>, Serializable
{
  boolean _ascending;

  /**
   * @author Andy Mechtenberg
   */
  public AlphaNumericComparator(boolean ascending)
  {
    _ascending = ascending;
  }

  /**
   * @author Andy Mechtenberg
   */
  public AlphaNumericComparator()
  {
    _ascending = true;
  }


  /**
   * Implementation of the 'compare' method in the Comparator interface.  This method assumes that the Strings
   * being compared follow the regular expression pattern of (<letter>*<digit>*)*
   *
   * The <letter> portions are compared alphabetically.  If they are the same, the numeric portions are compared as
   * integers.  The process repeats recursively until one of the comparisons is unequal.
   *
   * @param lhs first String being compared
   * @param rhs second String being compared
   * @return -1 if the first String is less than the second, 0 if the two Strings are equal, 1 if the first String is
   * greater than the second
   * @author Matt Wharton
   */
  public int compare( Object lhs, Object rhs )
  {
    Assert.expect( lhs != null );
    Assert.expect( rhs != null );

    String firstString = "";
    String secondString = "";

    if ( _ascending == true )
    {
      firstString = lhs.toString();
      secondString = rhs.toString();
    }
    else
    {
      firstString = rhs.toString();
      secondString = lhs.toString();
    }

    // Remove any leading or trailing spaces.
    firstString = firstString.trim();
    secondString = secondString.trim();

    // If both strings are integers, we should compare them numerically right away.
    if ( StringUtil.isInteger( firstString ) && StringUtil.isInteger( secondString ) )
    {
      BigInteger firstBigInteger = BigInteger.ZERO;
      BigInteger secondBigInteger = BigInteger.ZERO;

      try
      {
        firstBigInteger = StringUtil.convertStringToBigInteger( firstString );
        secondBigInteger = StringUtil.convertStringToBigInteger( secondString );
      }
      catch ( BadFormatException bfex )
      {
        // Shouldn't ever occur.
        System.out.println( "first string: " + firstString );
        System.out.println( "second string: " + secondString );
        Assert.logException(bfex);
      }

      // If the numbers are equal, compare them alphabetically.
      // Otherwise just return the numeric comparison result.
      // This will handle the '001' vs. '1' case.
      int numericCompareResult = firstBigInteger.compareTo( secondBigInteger );
      if ( numericCompareResult == 0 )
      {
        return firstString.compareTo( secondString );
      }
      else
      {
        return numericCompareResult;
      }
    }

    // We want this to be a case-insensitive comparison so make both Strings uppercase.
    firstString = firstString.toUpperCase();
    secondString = secondString.toUpperCase();

    int firstStringIndex = 0;
    int secondStringIndex = 0;
    int firstStringLength = firstString.length();
    int secondStringLength = secondString.length();

    StringBuilder firstStringAlphaPartBuffer = new StringBuilder( firstStringLength );
    StringBuilder secondStringAlphaPartBuffer = new StringBuilder( secondStringLength );
    StringBuilder firstStringNumericPartBuffer = new StringBuilder( firstStringLength );
    StringBuilder secondStringNumericPartBuffer = new StringBuilder( secondStringLength );
    BigInteger firstStringNumericPart = BigInteger.ZERO;
    BigInteger secondStringNumericPart = BigInteger.ZERO;

    // Get the alphabetical portion of the first string...leading zeros are treated alphabetically
    while ( firstStringIndex < firstStringLength && Character.isDigit( firstString.charAt( firstStringIndex ) ) == false )
    {
      firstStringAlphaPartBuffer.append( firstString.charAt( firstStringIndex ) );
      firstStringIndex++;
    }

    // Get the alphabetical portion of the second string...leading zeros are treated alphabetically.
    while ( secondStringIndex < secondStringLength && Character.isDigit( secondString.charAt( secondStringIndex ) ) == false )
    {
      secondStringAlphaPartBuffer.append( secondString.charAt( secondStringIndex ) );
      secondStringIndex++;
    }

    // If the alphabetical portions are not equal, then we're done
    String firstStringAlphaPart = firstStringAlphaPartBuffer.toString();
    String secondStringAlphaPart = secondStringAlphaPartBuffer.toString();

    if ( firstStringAlphaPart.equals( secondStringAlphaPart ) == false )
      return firstStringAlphaPart.compareTo( secondStringAlphaPart );

    // Get the numeric portion of the first string
    while ( firstStringIndex < firstStringLength && Character.isDigit( firstString.charAt(firstStringIndex) ) == true )
    {
      firstStringNumericPartBuffer.append( firstString.charAt( firstStringIndex ) );
      firstStringIndex++;
    }

    String firstStringNumericPartString = firstStringNumericPartBuffer.toString();
    if ( firstStringNumericPartString.length() > 0 )
      firstStringNumericPart = new BigInteger( firstStringNumericPartString );

    // Get the numeric portion of the second string
    while ( secondStringIndex < secondStringLength && Character.isDigit( secondString.charAt(secondStringIndex) ) == true )
    {
      secondStringNumericPartBuffer.append( secondString.charAt( secondStringIndex ) );
      secondStringIndex++;
    }

    String secondStringNumericPartString = secondStringNumericPartBuffer.toString();
    if ( secondStringNumericPartBuffer.length() > 0 )
      secondStringNumericPart = new BigInteger( secondStringNumericPartString );

    // If the numeric portions are not equal, then we're done.
    // We have to be careful in the comparison here though.
    // First we compare numerically.  If the substrings are not equal numerically, we're done.
    // If they're equal numerically, we then recheck them alphabetically.
    // If they differ alphabetically, we're done.
    // Otherwise, we keep scanning the strings.
    // This will differentiate between '001' and '1'.
    if ( firstStringNumericPart.equals( secondStringNumericPart ) == false )
    {
      int result = firstStringNumericPart.compareTo(secondStringNumericPart);
      return result;
    }
    else
    {
      int stringCompareResult = firstStringNumericPartString.compareTo( secondStringNumericPartString );
      if ( stringCompareResult != 0 )
      {
        return stringCompareResult;
      }
    }

    if ( firstStringIndex == firstStringLength && secondStringIndex == secondStringLength )
      return 0;
    else
    {
      int result = 0;
      //
      if (_ascending)
      {
        // call recursively to handle rest of string
        result = compare(firstString.substring(firstStringIndex), secondString.substring(secondStringIndex));
      }
      else
      {
        // the recursive call with descending with again swap first part and second part - not good.
        // reverse them in the call so they are correct after the next swap
        result = compare(secondString.substring(secondStringIndex), firstString.substring(firstStringIndex));
      }
      return result;
    }
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void setAscending(boolean ascending)
  {
    _ascending = ascending;
  }
}
