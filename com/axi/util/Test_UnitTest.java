package com.axi.util;

import java.io.*;

/**
 * @author Bill Darbie
 */
public class Test_UnitTest extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_UnitTest());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    Expect.expect(unitTesting());

    //System.out.println(getUnitTestRelativeDir());
    //System.out.println(getTestSourceDir());
    //System.out.println(getTestInputFileDir());
    //System.out.println(getTestDataDir());

    // try some simple cases
    Expect.expect(stripOffViewName("").equals(""));
    Expect.expect(stripOffViewName("just a test").equals("just a test"));

    // try some normal cases
    Expect.expect(stripOffViewName("m:\\aViewName\\axi\\cpp\\someDir\\unitTest\\src").equals("<viewName>\\cpp\\someDir\\unitTest\\src"));
    Expect.expect(stripOffViewName("m:\\aViewName\\axi\\java\\someDir\\autotest\\src").equals("<viewName>\\java\\someDir\\autotest\\src"));
    Expect.expect(stripOffViewName("d:\\staticDir\\aViewName\\axi\\cpp\\someDir\\unitTest\\src").equals("<viewName>\\cpp\\someDir\\unitTest\\src"));
    Expect.expect(stripOffViewName("d:\\staticDir\\aViewName\\axi\\java\\someDir\\autotest\\src").equals("<viewName>\\java\\someDir\\autotest\\src"));

    Expect.expect(stripOffViewName("d:\\staticDir\\aViewName\\axi\\java\\someDir\\axi\\someDir\\autotest\\src").equals("<viewName>\\java\\someDir\\axi\\someDir\\autotest\\src"), stripOffViewName("d:\\staticDir\\aViewName\\axi\\java\\someDir\\axi\\someDir\\autotest\\src"));

    Expect.expect(stripOffViewName("m:\\aViewName\\axi\\cpp\\someDir\\unitTest\\src\nnext line").equals("<viewName>\\cpp\\someDir\\unitTest\\src\nnext line"), stripOffViewName("m:\\aViewName\\axi\\cpp\\someDir\\unitTest\\src\nnext line"));
    Expect.expect(stripOffViewName("m:\\aViewName\\axi\\java\\someDir\\autotest\\src\nnext line").equals("<viewName>\\java\\someDir\\autotest\\src\nnext line"));
    Expect.expect(stripOffViewName("d:\\staticDir\\aViewName\\axi\\cpp\\someDir\\unitTest\\src\nnext line").equals("<viewName>\\cpp\\someDir\\unitTest\\src\nnext line"));
    Expect.expect(stripOffViewName("d:\\staticDir\\aViewName\\axi\\java\\someDir\\autotest\\src\nnext line").equals("<viewName>\\java\\someDir\\autotest\\src\nnext line"));

    // a little more complex
    Expect.expect(stripOffViewName(" extra text m:\\aViewName\\axi\\cpp\\someDir\\unitTest\\src").equals(" extra text <viewName>\\cpp\\someDir\\unitTest\\src"));
    Expect.expect(stripOffViewName(" extra text m:\\aViewName\\axi\\java\\someDir\\autotest\\src").equals(" extra text <viewName>\\java\\someDir\\autotest\\src"));
    Expect.expect(stripOffViewName(" extra text d:\\staticDir\\aViewName\\axi\\cpp\\someDir\\unitTest\\src").equals(" extra text <viewName>\\cpp\\someDir\\unitTest\\src"));
    Expect.expect(stripOffViewName(" extra text d:\\staticDir\\aViewName\\axi\\java\\someDir\\autotest\\src").equals(" extra text <viewName>\\java\\someDir\\autotest\\src"));

    // check that an extra : will not harm anything
    Expect.expect(stripOffViewName("ERROR: extra text m:\\aViewName\\axi\\cpp\\someDir\\unitTest\\src").equals("ERROR: extra text <viewName>\\cpp\\someDir\\unitTest\\src"));
    Expect.expect(stripOffViewName("ERROR: extra text m:\\aViewName\\axi\\java\\someDir\\autotest\\src").equals("ERROR: extra text <viewName>\\java\\someDir\\autotest\\src"));
    Expect.expect(stripOffViewName("ERROR: extra text d:\\staticDir\\aViewName\\axi\\cpp\\someDir\\unitTest\\src").equals("ERROR: extra text <viewName>\\cpp\\someDir\\unitTest\\src"));
    Expect.expect(stripOffViewName("ERROR: extra text d:\\staticDir\\aViewName\\axi\\java\\someDir\\autotest\\src").equals("ERROR: extra text <viewName>\\java\\someDir\\autotest\\src"));

    // check that a non drive letter does not throw things off
    Expect.expect(stripOffViewName("abcm:\\aViewName\\axi\\cpp\\someDir\\unitTest\\src").equals("abcm:\\aViewName\\axi\\cpp\\someDir\\unitTest\\src"), stripOffViewName("abcm:\\aViewName\\cpp\\someDir\\unitTest\\src"));
    Expect.expect(stripOffViewName("abcm:\\aViewName\\axi\\java\\someDir\\autotest\\src").equals("abcm:\\aViewName\\axi\\java\\someDir\\autotest\\src"));
    Expect.expect(stripOffViewName("abcd:\\staticDir\\aViewName\\axi\\cpp\\someDir\\unitTest\\src").equals("abcd:\\staticDir\\aViewName\\axi\\cpp\\someDir\\unitTest\\src"));
    Expect.expect(stripOffViewName("abcd:\\staticDir\\aViewName\\axi\\java\\someDir\\autotest\\src").equals("abcd:\\staticDir\\aViewName\\axi\\java\\someDir\\autotest\\src"));

    // check that more than one string replacement per line works properly
    Expect.expect(stripOffViewName("m:\\aViewName\\axi\\cpp\\someDir\\unitTest\\src m:\\aViewName\\axi\\cpp\\someDir\\unitTest\\src").equals("<viewName>\\cpp\\someDir\\unitTest\\src <viewName>\\cpp\\someDir\\unitTest\\src"), stripOffViewName("m:\\aViewName\\axi\\cpp\\someDir\\unitTest\\src m:\\aViewName\\axi\\cpp\\someDir\\unitTest\\src"));
    Expect.expect(stripOffViewName("m:\\aViewName\\axi\\java\\someDir\\autotest\\src m:\\aViewName\\axi\\java\\someDir\\autotest\\src").equals("<viewName>\\java\\someDir\\autotest\\src <viewName>\\java\\someDir\\autotest\\src"));
    Expect.expect(stripOffViewName("d:\\staticDir\\aViewName\\axi\\cpp\\someDir\\unitTest\\src d:\\staticDir\\aViewName\\axi\\cpp\\someDir\\unitTest\\src").equals("<viewName>\\cpp\\someDir\\unitTest\\src <viewName>\\cpp\\someDir\\unitTest\\src"));
    Expect.expect(stripOffViewName("d:\\staticDir\\aViewName\\axi\\java\\someDir\\autotest\\src d:\\staticDir\\aViewName\\axi\\java\\someDir\\autotest\\src").equals("<viewName>\\java\\someDir\\autotest\\src <viewName>\\java\\someDir\\autotest\\src"));

  }
}
