package com.axi.util;

import java.io.*;
import java.util.Random;

/**
 * Unit test for QuadraticSurfaceEstimator.
 *
 * @author John Heumann
 */
public class Test_QuadraticSurfaceEstimator extends UnitTest
{
  private Random _rng;

  private final double _x0[] = { 0.0, 1.0, 0.0, 1.0  };
  private final double _y0[] = { 0.0, 0.0, 1.0, 1.0 };

  private final double _tolerance = 0.1;

  /**
   * Constructor.
   *
   * @author John Heumann
   */
  private Test_QuadraticSurfaceEstimator()
  {
    _rng = new Random(0);
  }

  /**
   * @author John Heumann
   */
  private void testAsserts()
  {
    // Make sure the constructor asserts when passed insufficient data.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        final double x[] = {1, 2, 3};
        final double y[] = {1, 2, 3};
        final double z[] = {1, 2, 3};
        // minimum of 4 points are required for a bilinear fit
        new QuadraticSurfaceEstimator(x, y, z, QuadraticSurfaceEstimatorTypeEnum.BILINEAR);
      }
    });

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        final double x[] = {1, 2, 3, 4};
        final double y[] = {1, 2, 3, 4};
        final double z[] = {1, 2, 3, 4};
        // minimum of 5 points are required for a linear-x / quadratic-y fit
        new QuadraticSurfaceEstimator(x, y, z, QuadraticSurfaceEstimatorTypeEnum.LINEAR_X_QUADRATIC_Y);
      }
    });

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        final double x[] = {1, 2, 3, 4, 5};
        final double y[] = {1, 2, 3, 4, 5};
        final double z[] = {1, 2, 3, 4, 5};
        // minimum of 6 points are required for a quadratic fit
        new QuadraticSurfaceEstimator(x, y, z, QuadraticSurfaceEstimatorTypeEnum.QUADRATIC);
      }
    });

  }

  /**
   * @author John Heumann
   */
  private void initializeVectors(double x[], double y[], double z[], double cx,
                                 double cy, double cxy, double cx2, double cy2, double c)
  {
    Assert.expect(x != null);
    Assert.expect(y != null);
    Assert.expect(z != null);
    Assert.expect(x.length == y.length);
    Assert.expect(x.length == z.length);
    final int nPoints = x.length;
    Assert.expect(nPoints <= 9);

    final double xValues[] = {0.9, 1, 1.1, 1.9, 2, 2.1, 2.9, 3, 3.1};
    final double yValues[] = {1, 2, 3, 1.1, 2.1, 3.2, 1.2, 2.2, 3.2};

    for (int i = 0; i < nPoints; i++)
    {
      x[i] = xValues[i];
      y[i] = yValues[i];
      z[i] = c + cx * x[i] + cy * y[i] + cxy * x[i] * y[i] + cx2 * x[i] * x[i] + cy2 * y[i] * y[i]
             + 0.01 * _rng.nextGaussian();
    }
  }

  /**
   * @author John Heumann
   */
  private void testBilinearFitWithMinimalNumberOfPoints()
  {
    final int nPoints = 4;
    double x[] = new double[nPoints], y[] = new double[nPoints], z[] = new double[nPoints];
    final double c = 3.0;
    final double cx = 0.5;
    final double cy = 1.5;
    final double cxy = 0.6;
    final double cx2 = 0;
    final double cy2 = 0;

    initializeVectors(x, y, z, cx, cy, cxy, cx2, cy2, c);
    QuadraticSurfaceEstimator qse =
      new QuadraticSurfaceEstimator(x, y, z, QuadraticSurfaceEstimatorTypeEnum.BILINEAR);

    double zHat[] = qse.getEstimatedHeights();
    for (int i = 0; i < nPoints; i++)
      Assert.expect(Math.abs(z[i] - zHat[i]) < _tolerance);

    zHat = qse.getEstimatedHeights(_x0, _y0);
    // With this few input points, the fit is not reliable... don't bother checking results
  }

  /**
   * @author John Heumann
   */
  private void testLinearXQuadraticYFitWithMinimalNumberOfPoints()
  {
    final int nPoints = 5;
    double x[] = new double[nPoints], y[] = new double[nPoints], z[] = new double[nPoints];
    final double c = 2.0;
    final double cx = 0.5;
    final double cy = 1.5;
    final double cxy = 0.6;
    final double cx2 = 0;
    final double cy2 = 0.4;

    initializeVectors(x, y, z, cx, cy, cxy, cx2, cy2, c);
    QuadraticSurfaceEstimator qse =
      new QuadraticSurfaceEstimator(x, y, z, QuadraticSurfaceEstimatorTypeEnum.LINEAR_X_QUADRATIC_Y);

    double zHat[] = qse.getEstimatedHeights();
    for (int i = 0; i < nPoints; i++)
      Assert.expect(Math.abs(z[i] - zHat[i]) < _tolerance);

    zHat = qse.getEstimatedHeights(_x0, _y0);
    // With this few input points, the fit is not reliable... don't bother checking results
  }

  /**
   * @author John Heumann
   */
  private void testQuadraticFitWithMinimalNumberOfPoints()
  {
    final int nPoints = 6;
    double x[] = new double[nPoints], y[] = new double[nPoints], z[] = new double[nPoints];
    final double c = 5.0;
    final double cx = 0.5;
    final double cy = 1.5;
    final double cxy = 0.6;
    final double cx2 = 0.8;
    final double cy2 = 0.4;

    initializeVectors(x, y, z, cx, cy, cxy, cx2, cy2, c);
    QuadraticSurfaceEstimator qse =
      new QuadraticSurfaceEstimator(x, y, z, QuadraticSurfaceEstimatorTypeEnum.QUADRATIC);

    double zHat[] = qse.getEstimatedHeights();
    for (int i = 0; i < nPoints; i++)
      Assert.expect(Math.abs(z[i] - zHat[i]) < _tolerance);

    zHat = qse.getEstimatedHeights(_x0, _y0);
    // With this few input points, the fit is not reliable... don't bother checking results
  }

  /**
   * @author John Heumann
   */
  private void testBilinearFit()
  {
    final int nPoints = 9;
    double x[] = new double[nPoints], y[] = new double[nPoints], z[] = new double[nPoints];
    final double c = 3.0;
    final double cx = 0.5;
    final double cy = 1.5;
    final double cxy = 0.6;
    final double cx2 = 0;
    final double cy2 = 0;

    initializeVectors(x, y, z, cx, cy, cxy, cx2, cy2, c);
    QuadraticSurfaceEstimator qse =
      new QuadraticSurfaceEstimator(x, y, z, QuadraticSurfaceEstimatorTypeEnum.BILINEAR);

    double zHat[] = qse.getEstimatedHeights();
    for (int i = 0; i < nPoints; i++)
      Assert.expect(Math.abs(z[i] - zHat[i]) < _tolerance);

    zHat = qse.getEstimatedHeights(_x0, _y0);
    Assert.expect(Math.abs(zHat[0] - c) < _tolerance);
    Assert.expect(Math.abs(zHat[1] - (c + cx)) < _tolerance);
    Assert.expect(Math.abs(zHat[2] - (c + cy)) < _tolerance);
    Assert.expect(Math.abs(zHat[3] - (c + cx + cy + cxy)) < _tolerance);
  }

  /**
   * @author John Heumann
   */
  private void testLinearXQuadraticYFit()
  {
    final int nPoints = 9;
    double x[] = new double[nPoints], y[] = new double[nPoints], z[] = new double[nPoints];
    final double c = 2.0;
    final double cx = 0.5;
    final double cy = 1.5;
    final double cxy = 0.6;
    final double cx2 = 0;
    final double cy2 = 0.4;

    initializeVectors(x, y, z, cx, cy, cxy, cx2, cy2, c);
    QuadraticSurfaceEstimator qse =
      new QuadraticSurfaceEstimator(x, y, z, QuadraticSurfaceEstimatorTypeEnum.LINEAR_X_QUADRATIC_Y);

    double zHat[] = qse.getEstimatedHeights();
    for (int i = 0; i < nPoints; i++)
      Assert.expect(Math.abs(z[i] - zHat[i]) < _tolerance);

    zHat = qse.getEstimatedHeights(_x0, _y0);
    Assert.expect(Math.abs(zHat[0] - c) < _tolerance);
    Assert.expect(Math.abs(zHat[1] - (c + cx)) < _tolerance);
    Assert.expect(Math.abs(zHat[2] - (c + cy + cy2)) < _tolerance);
    Assert.expect(Math.abs(zHat[3] - (c + cx + cy + cxy + cy2)) < _tolerance);
  }

  /**
   * @author John Heumann
   */
  private void testQuadraticFit()
  {
    final int nPoints = 9;
    double x[] = new double[nPoints], y[] = new double[nPoints], z[] = new double[nPoints];
    final double c = 5.0;
    final double cx = 0.5;
    final double cy = 1.5;
    final double cxy = 0.6;
    final double cx2 = 0.8;
    final double cy2 = 0.4;

    initializeVectors(x, y, z, cx, cy, cxy, cx2, cy2, c);
    QuadraticSurfaceEstimator qse =
      new QuadraticSurfaceEstimator(x, y, z, QuadraticSurfaceEstimatorTypeEnum.QUADRATIC);

    double zHat[] = qse.getEstimatedHeights();
    for (int i = 0; i < nPoints; i++)
      Assert.expect(Math.abs(z[i] - zHat[i]) < _tolerance);

    zHat = qse.getEstimatedHeights(_x0, _y0);
    Assert.expect(Math.abs(zHat[0] - c) < _tolerance);
    Assert.expect(Math.abs(zHat[1] - (c + cx + cx2)) < _tolerance);
    Assert.expect(Math.abs(zHat[2] - (c + cy + cy2)) < _tolerance);
    Assert.expect(Math.abs(zHat[3] - (c + cx + cy + cxy + cx2 + cy2)) < _tolerance);
  }

  /**
   * Main unit test method.
   *
   * @author John Heumann
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testAsserts();
    testBilinearFitWithMinimalNumberOfPoints();
    testLinearXQuadraticYFitWithMinimalNumberOfPoints();
    testQuadraticFitWithMinimalNumberOfPoints();
    testBilinearFit();
    testLinearXQuadraticYFit();
    testQuadraticFit();
  }

  /**
   * Unit test entry point.
   *
   * @author John Heumann
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_QuadraticSurfaceEstimator());
  }
}
