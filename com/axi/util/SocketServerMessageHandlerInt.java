package com.axi.util;

import java.util.*;

/**
 * This interface provides the interface that any server using the
 * SocketServer class needs to implement.
 *
 * The SocketServer class provides all basic functionality for the
 * SocketServer and SocketClient code.  This interface provides a specific processInput
 * method that the user can define in any way they need to.  Because
 * it is impossible for the SocketServer class to know what messages
 * a particular implementation will need, it needs to have this method
 * provided to it.
 *
 * You should write code that handles all server messages, including some kind
 * of message telling the server that the client is going to disconnect.  This
 * call is needed if another client is to be allowed to connect later.  You
 * may also want to provide a server kill command if you want a client to have
 * the ability to shut the server down entirely.
 *
 * IMPORTANT: REMEMBER to call SocketServer.disconnectFromClient() in this method when
 *            you get a disconnect message.  If you are supporting a kill
 *            server message you need to call SocketServer.killServer().
 *
 ****NOTE: under com/axi/mtd/agt5dx/hardware/tini/, there is a copy of SocketServerMessageHandlerInt.
 *         Because SocketServerMessageHandlerInt will run on TINI,
 *         which has a different java api version from what Agilent uses,
 *         SocketServerMessageHandlerInt needs to be changed.
 *         IF ANYBODY FINDS ANY TYPE OF BUG IN THIS CODE AND FIXS IT,
 *         PLEASE MAKE SURE TO FIX THE BUG IN THE
 *         COM.AGILENT.MTD.AGT5DX.HARDWARE.TINI.SOCKETSERVERMESSAGEHANDLERINT AT THE SAME TIME.
 */
public interface SocketServerMessageHandlerInt
{
  /*
   * @param clientMessage is the message from the client that is sent to the server
   * @return a list of responses from the server.  Each response is an item
   *          in the list.  There may be 0 or more responses.
   * @author Eugene Kim-Leighton
   */
  public List<String> processMessage(String clientMessage);
}
