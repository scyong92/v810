package com.axi.util;

import java.rmi.*;
import java.util.*;

/**
* This is a helper class for the RemoteObservable class to work properly.  This class should
* not be used outside of this package.  This class converts between the RemoteObservable update()
* method and the ThreadSafeObservable update call.
*
* @author Bill Darbie
*/
class RemoteObserver implements Observer
{
  private RemoteObservable _remoteObservable;
  private RemoteObserverInt _remoteObserverInt;

  /**
  * @author Bill Darbie
  */
  RemoteObserver(RemoteObservable remoteObservable, RemoteObserverInt remoteObserverInt)
  {
    Assert.expect(remoteObservable != null);
    Assert.expect(remoteObserverInt != null);

    _remoteObservable = remoteObservable;
    _remoteObserverInt = remoteObserverInt;
  }

  /**
  * Take an update call from the ThreadSafeObservable class and turn it into an update call
  * for its corresponding RemoteObservable class.
  *
  * @author Bill Darbie
  */
  public synchronized void update(Observable observable, Object args)
  {
    try
    {
       _remoteObserverInt.update(_remoteObservable, args);
    }
    catch(RemoteException re)
    {
      // we could not connect to the client observer for some reason
      // print out the error so it goes to the log file
      // and remove it from the list of observers
      re.printStackTrace();
      _remoteObservable.deleteObserver(_remoteObserverInt);
    }
  }
}
