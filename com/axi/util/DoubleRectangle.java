package com.axi.util;

import java.awt.geom.*;
import java.io.*;

/**
 * This class is a representation of a rectangle.
 * Since this can be used for any type of rectangle, it is not necessarily
 * saved in nanometers. The variable name of an instance of this class should
 * be descriptive enough to determine what units are being used (i.e. pixels).
 * @author Keith Lee
 * @author Matt Wharton
 */
public class DoubleRectangle implements Serializable
{
  // This class really just wraps a java.awt.geom.Rectangle2D.Double.
  // NOTE: This IS a serializable class and the Rectangle2D is supposed to be transient.  This is handled
  // via the overridden readObject/writeObject methods below.
  private transient Rectangle2D.Double _rectangle = null;

  /**
   * @author Keith Lee
   * @author Matt Wharton
   */
  public DoubleRectangle(DoubleCoordinate topLeftCornerCoordinate, DoubleCoordinate bottomRightCornerCoordinate)
  {
    Assert.expect(topLeftCornerCoordinate != null);
    Assert.expect(bottomRightCornerCoordinate != null);

    _rectangle = new Rectangle2D.Double( topLeftCornerCoordinate.getX(),
                                         topLeftCornerCoordinate.getY(),
                                         bottomRightCornerCoordinate.getX() - topLeftCornerCoordinate.getX(),
                                         bottomRightCornerCoordinate.getY() - topLeftCornerCoordinate.getY() );
  }

  /**
   * @author Keith Lee
   * @author Matt Wharton
   */
  public void setTopLeftCornerCoordinate(DoubleCoordinate topLeftCornerCoordinate)
  {
    Assert.expect(topLeftCornerCoordinate != null);

    _rectangle.setRect( topLeftCornerCoordinate.getX(),
                        topLeftCornerCoordinate.getY(),
                        _rectangle.getMaxX() - topLeftCornerCoordinate.getX(),
                        _rectangle.getMaxY() - topLeftCornerCoordinate.getY() );
  }

  /**
   * @author Keith Lee
   * @author Matt Wharton
   */
  public DoubleCoordinate getTopLeftCornerCoordinate()
  {
    return new DoubleCoordinate( _rectangle.getX(), _rectangle.getY() );
  }

  /**
   * @author Matt Wharton
   * @author Keith Lee
   */
  public void setBottomRightCornerCoordinate( DoubleCoordinate bottomRightCoordinate )
  {
    Assert.expect( bottomRightCoordinate != null );

    _rectangle.setRect( _rectangle.getX(),
                        _rectangle.getY(),
                        bottomRightCoordinate.getX() - _rectangle.getX(),
                        bottomRightCoordinate.getY() - _rectangle.getY() );
  }

  /**
   * @author Matt Wharton
   * @author Keith Lee
   */
  public DoubleCoordinate getBottomRightCornerCoordinate()
  {
    return new DoubleCoordinate( _rectangle.getMaxX(), _rectangle.getMaxY() );
  }

  /**
   * @author Matt Wharton
   */
  protected void deserializeRectangle(ObjectInputStream is) throws IOException
  {
    Assert.expect(is != null);

    double tempX = is.readDouble();
    double tempY = is.readDouble();
    double tempWidth = is.readDouble();
    double tempHeight = is.readDouble();
    _rectangle = new Rectangle2D.Double(tempX, tempY, tempWidth, tempHeight);
  }

  /**
   * @author Matt Wharton
   */
  protected void serializeRectangle(ObjectOutputStream os) throws IOException
  {
    Assert.expect(os != null);
    Assert.expect(_rectangle != null);

    os.writeDouble(_rectangle.getX());
    os.writeDouble(_rectangle.getY());
    os.writeDouble(_rectangle.getWidth());
    os.writeDouble(_rectangle.getHeight());
  }

  /**
   * @author Matt Wharton
   */
  private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
  {
    is.defaultReadObject();
    deserializeRectangle(is);
  }

  /**
   * @author Matt Wharton
   */
  private void writeObject(ObjectOutputStream os) throws IOException
  {
    os.defaultWriteObject();
    serializeRectangle(os);
  }
}
