package com.axi.util;

/**
 * This class is thrown when parsing an empty list.
 *
 * @author Ying-Huan.Chu
 */
public class EmptyListException extends LocalizedException
{
  /**
   * @param localizedString - the Localized String for the exception.
   * @author Ying-Huan.Chu
   */
  public EmptyListException(LocalizedString localizedString)
  {
    super(localizedString);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public LocalizedString getLocalizedString()
  {
    return super.getLocalizedString();
  }
}
