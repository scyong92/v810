/**
 * Native implementation of NativeAssertFalse.
 * This is a simple helper function which does an sptAssert(false) for the 
 * purposes of a regression test.
 * 
 * @author Matt Wharton
 */

#include <jni.h>

#include "cpp/util/src/sptAssert.h"
#include "cpp/util/src/StringUtil.h"

#include "com_axi_util_NativeAssertFalse.h"

/**
 * Calls sptAssert(false).
 * 
 * @author Matt Wharton
 */
JNIEXPORT void JNICALL Java_com_axi_util_NativeAssertFalse_nativeAssertFalseSimple(JNIEnv* pEnv, jclass cls)
{
  try
  {
    sptAssert(false);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * Calls a function in another DLL which will invoke sptAssert(false).
 * 
 * @author Matt Wharton
 */
JNIEXPORT void JNICALL Java_com_axi_util_NativeAssertFalse_nativeAssertFalseNested(JNIEnv* pEnv, jclass cls)
{
  try
  {
    // Calling StringUtil::padHexString() with an empty string will cause an assert.
    StringUtil::padHexString("");
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}
