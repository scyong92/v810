package com.axi.util;

/**
 * @author George A. David
 */
public class NetworkShareTypeEnum extends Enum
{
  private static int _index = -1;

  public static NetworkShareTypeEnum DISK_DRIVE = new NetworkShareTypeEnum(++_index);
  public static NetworkShareTypeEnum PRINT_QUEUE = new NetworkShareTypeEnum(++_index);
  public static NetworkShareTypeEnum COMMUNICATIN_DEVICE = new NetworkShareTypeEnum(++_index);
  public static NetworkShareTypeEnum INTERPROCESS_COMMUNCATION = new NetworkShareTypeEnum(++_index);
  public static NetworkShareTypeEnum SPECIAL = new NetworkShareTypeEnum(++_index);

  /**
   * @author George A. David
   */
  private NetworkShareTypeEnum(int id)
  {
    super(id);
  }
}
