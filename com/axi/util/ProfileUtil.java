package com.axi.util;

import java.awt.Point;
import java.util.Arrays;

/**
 * A collection of routines for working with 1d float array profiles.
 * See also MathUtil, StatisticsUtil
 *
 * @author Patrick Lacz
 */
public class ProfileUtil
{
  public ProfileUtil()
  {
    // do nothing, static class
  }

  /**
   * Estimate the background by using the sides of the profile as background
   * samples and remove the background trend.  When this procedure is finished,
   * the profile should have a value of 255 on the left and the right ends of the
   * profile.
   *
   * @author Peter Esbensen
   */
  public static void removeBackgroundTrendFromProfile(float[] profile)
  {
    Assert.expect(profile != null);
    Assert.expect(profile.length > 0);

    float leftSideBackgroundEstimate = 0.0f;
    float rightSideBackgroundEstimate = 0.0f;
    if (profile.length == 1)
    {
      profile[0] = 255f;
      return;
    }
    leftSideBackgroundEstimate = (profile[0] + profile[1]) * 0.5f;
    rightSideBackgroundEstimate = (profile[profile.length - 1] + profile[profile.length - 2]) * 0.5f;

    float backgroundTrendFromLeftToRight = rightSideBackgroundEstimate - leftSideBackgroundEstimate;

    for (int i = 0; i < profile.length; ++i)
    {
      float percentDistanceFromLeftToRight = (float)i / (float)profile.length;

      float backgroundEstimate = leftSideBackgroundEstimate +
                                 (backgroundTrendFromLeftToRight * percentDistanceFromLeftToRight);

      float errorFromBackgroundEstimate = profile[i] - backgroundEstimate;

      profile[i] = 255 + errorFromBackgroundEstimate;
    }
  }

  /**
   * Estimate the background trend profile of the specified profile by using the sides
   * of the profile as background samples and interpolating across the profile.
   *
   * @author Matt Wharton
   */
  public static float[] getBackgroundTrendProfile(float[] profile)
  {
    Assert.expect(profile != null);
    Assert.expect(profile.length > 0);

    // Allocate space for the background trend profile.
    float[] backgroundTrendProfile = new float[profile.length];

    if (backgroundTrendProfile.length == 1)
    {
      backgroundTrendProfile[0] = 255f;
    }
    else
    {
      float leftSideBackgroundEstimate = (profile[0] + profile[1]) / 2f;
      float rightSideBackgroundEstimate = (profile[profile.length - 1] + profile[profile.length - 2]) / 2f;
      float backgroundTrendFromLeftToRight = rightSideBackgroundEstimate - leftSideBackgroundEstimate;

      for (int i = 0; i < profile.length; ++i)
      {
        float percentDistanceFromLeftToRight = (float)i / (float)profile.length;

        float backgroundEstimate = leftSideBackgroundEstimate +
                                   (backgroundTrendFromLeftToRight * percentDistanceFromLeftToRight);

        backgroundTrendProfile[i] = backgroundEstimate;
      }
    }

    return backgroundTrendProfile;
  }

  /**
   * @author Peter Esbensen
   */
  public static float findSubpixelEdgeLocationBasedOnExceedingThreshold(
      float[] profile,
      float threshold)
  {
    Assert.expect(profile != null);
    Assert.expect(threshold >= 0.0f);

    int i = 0;
    for (i = 0; i < profile.length; i++)
    {
      if (profile[i] > threshold)
      {
        break;
      }
    }
    float subpixelEdgeLocation = i;
    if ((i > 0) && (i < profile.length))
    {
      subpixelEdgeLocation = (float)i -
                             ((float)profile[i] - threshold) /
                             ((float)profile[i] - (float)profile[i - 1]);
    }
    return subpixelEdgeLocation;
  }

  /**
   * @todo make unit test
   * @author Peter Esbensen
   */
  public static float findSubpixelEdgeLocationSearchingLeftToRight(
      float[] profile,
      int startIndex,
      int endIndex,
      float threshold)
  {
    Assert.expect(profile != null);
    Assert.expect(profile.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < profile.length);
    Assert.expect(endIndex <= profile.length);
    Assert.expect(endIndex >= startIndex);
    Assert.expect(threshold >= 0.0f, "should be searching for positive thickness threshold, but it is: " + threshold);

    int i = startIndex;
    for (i = startIndex; i < endIndex; i++)
    {
      if (profile[i] > threshold)
      {
        break;
      }
    }
    float subpixelEdgeLocation = i;
    if ((i > startIndex) && (i < endIndex))
    {
      subpixelEdgeLocation = (float)i -
                             ((float)profile[i] - threshold) /
                             ((float)profile[i] - (float)profile[i - 1]);
    }
    return subpixelEdgeLocation;
  }

  /**
   * @todo make unit test
   * @author Peter Esbensen
   */
  public static float findSubpixelEdgeLocationSearchingRightToLeft(
      float[] profile,
      int startIndex,
      int endIndex,
      float threshold)
  {
    Assert.expect(profile != null);
    Assert.expect(profile.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < profile.length);
    Assert.expect(endIndex <= profile.length);
    Assert.expect(endIndex <= startIndex);
    Assert.expect(threshold >= 0.0f);

    int i = startIndex;
    for (i = startIndex; i > endIndex; --i)
    {
      if (profile[i] > threshold)
      {
        break;
      }
    }
    float subpixelEdgeLocation = i;
    if ((i < startIndex) && (i > endIndex))
    {
      subpixelEdgeLocation = (float)i -
                             ((float)profile[i] - threshold) /
                             ((float)profile[i] - (float)profile[i + 1]);
    }
    return subpixelEdgeLocation;
  }


  /**
   * @author Rick Gaudette
   */
  public static float findMinMaxRelativeGrayLevel(float[] profile, float edgeDetectionFraction)
  {
    Assert.expect(profile != null);
    Assert.expect(edgeDetectionFraction >= 0.0f);

    float maximum = ArrayUtil.max(profile);
    float minimum = ArrayUtil.min(profile);
    float edgeValue = maximum - ((maximum - minimum) * edgeDetectionFraction);
    // RJG debug
//    System.out.printf("profile: min: %f  max %f  fraction: %f  edge value %f\n",
//    minimum,
//    maximum,
//    edgeDetectionFraction,
//    edgeValue);
    return edgeValue;
  }


  /**
   * Find the edge location based on the specified gray level value in edgeGrayLevel.
   *
   * @author Rick Gaudette
   */
  public static float findSubpixelEdgeLocationBasedOnGrayLevel(float[] profile, float edgeGrayLevel)
  {
    Assert.expect(profile != null);
    Assert.expect(edgeGrayLevel >= 0.0f);

    int i;
    for (i = 0; i < profile.length; i++)
    {
      if (profile[i] < edgeGrayLevel)
      {
        break;
      }
    }
    float subpixelEdgeLocation = i;
    if ((i > 0) && (i < profile.length))
    {
      subpixelEdgeLocation = (float) i - (profile[i] - edgeGrayLevel) / (profile[i] - profile[i - 1]);
    }
    return subpixelEdgeLocation;
  }


  /**
   * @author Peter Esbensen
   * @author Rick Gaudette
   */
  public static float findSubpixelEdgeLocationBasedOnFractionOfRange(float[] profile,
      float edgeDetectionFraction)
  {
    float edgeValue = findMinMaxRelativeGrayLevel(profile, edgeDetectionFraction);

    int i = 0;
    for (i = 0; i < profile.length; i++)
    {
      if (profile[i] < edgeValue)
      {
        break;
      }
    }
    float subpixelEdgeLocation = i;
    if ((i > 0) && (i < profile.length))
    {
      subpixelEdgeLocation = (float)i -
                             ((float)profile[i] - edgeValue) /
                             ((float)profile[i] - (float)profile[i - 1]);
    }
    return subpixelEdgeLocation;
  }

  /**
   * @todo : Create Regression Tests.
   * @author Peter Esbensen
   */
  static public float[] createDerivativeProfile(float[] profile, int stepSize)
  {
    Assert.expect(profile != null);
    Assert.expect(stepSize > 0);

    float[] derivativeProfile = new float[profile.length];
    int offsetToLeft = 0 - stepSize / 2;
    int offsetToRight = offsetToLeft + stepSize;

    for (int i = 0; i < profile.length; ++i)
    {
      int profileIndexToLeft = Math.max(0, i + offsetToLeft);
      int profileIndexToRight = Math.min(profile.length - 1, i + offsetToRight);
      derivativeProfile[i] = profile[profileIndexToRight] -
                             profile[profileIndexToLeft];
    }
    return derivativeProfile;
  }


  /**
   * Find the index of the max absolute value within the array.
   * @param dataArray Array to be tested.
   * @return The index of the max absolute value.
   * @author Eddie Williamson
   */
  public static int findMaxAbsoluteValue(float[] dataArray)
  {
    Assert.expect(dataArray != null);

    int indexOfMax = 0;
    float max = 0.0F;

    for (int i = 0; i < dataArray.length; i++)
    {
      if (Math.abs(dataArray[i]) > max)
      {
        max = Math.abs(dataArray[i]);
        indexOfMax = i;
      }
    }
    return indexOfMax;
  }
  
   /**
   * Find the index of the max absolute value and second max absolute value within the array.
   * @param dataArray Array to be tested.
   * @return The index of the max absolute value.
   * @author Jack Hwee
   */
  public static java.awt.geom.Point2D findMaxAbsoluteValueForMask(float[] dataArray)
  {
    Assert.expect(dataArray != null);

    int indexOfMax = 0;
    int indexOfMax2 = 0;
    
    java.awt.geom.Point2D point =  new java.awt.geom.Point2D.Float(0, 0);
    
    float max = 0.0F;
    
    for (int i = 0; i < dataArray.length; i++)
    {
      if (Math.abs(dataArray[i]) > max)
      {
        max = Math.abs(dataArray[i]);
        indexOfMax = i;
      }
      else if (Math.abs(dataArray[i]) > 0)
      {
             indexOfMax2 = i;      
      }
    }
    point.setLocation(indexOfMax, indexOfMax2);
    return point;
  }
  
   /**
   * Find the index of the max absolute value within the array.
   * @param dataArray Array to be tested.
   * @return The index of the max absolute value.
   * @author Eddie Williamson
   */
  public static int findMaxValue(float[] dataArray)
  {
    Assert.expect(dataArray != null);

    int indexOfMax = 0;
    float max = 0.0F;

    for (int i = 0; i < dataArray.length; i++)
    {
      if ((dataArray[i]) > max)
      {
        max = (dataArray[i]);
        indexOfMax = i;
      }
    }
    return indexOfMax;
  }

  /**
   * @todo : Create Regression Tests.
   * @author Peter Esbensen
   */
  static public int getMinValueIndex(float[] profile)
  {
    Assert.expect(profile != null);
    int indexOfMin = 0;
    float min = Float.MAX_VALUE;
    for (int i = 0; i < profile.length; ++i)
    {
      if (profile[i] < min)
      {
        indexOfMin = i;
        min = profile[i];
      }
    }
    return indexOfMin;
  }

  /**
   * @author Peter Esbensen
   */
  static public void reverseProfile(float[] profile)
  {
    Assert.expect(profile != null);

    int profileLength = profile.length;
    for (int i = 0; i < (int)(profileLength * 0.5); ++i)
    {
      float temp = profile[profileLength - 1 - i];
      profile[profileLength - 1 - i] = profile[i];
      profile[i] = temp;
    }
  }

  /**
   * Smooth the given profile via a sliding averaging window of the specified length.
   *
   * For edge values where the smoothingKernel does not entirely fit, we will
   * extend the value of the first or last pixel "outwards" as necessary.
   *
   * @author Peter Esbensen
   */
  static public float[] getSmoothedProfile(float[] profile, int smoothingKernelLength)
  {
    Assert.expect(profile != null);
    Assert.expect(smoothingKernelLength > 0);

    float[] smoothedProfile = new float[profile.length];
    int offsetToLeft = 0 - smoothingKernelLength / 2;
    int offsetToRight = offsetToLeft + smoothingKernelLength;

    for (int i = 0; i < profile.length; ++i)
    {
      float sum = 0.0f;
      for (int j = offsetToLeft; j < offsetToRight; ++j)
      {
        int currentProfileIndex = i + j;
        if ((currentProfileIndex >= 0) && (currentProfileIndex < profile.length))
          sum += profile[currentProfileIndex];
        else if (currentProfileIndex < 0)
          sum += profile[0];
        else if (currentProfileIndex >= profile.length)
          sum += profile[profile.length - 1];
      }
      smoothedProfile[i] = sum / smoothingKernelLength;
    }
    return smoothedProfile;
  }

  static public int[] getSmoothedProfile(int[] profile, int smoothingKernelLength)
  {
    Assert.expect(profile != null);
    Assert.expect(smoothingKernelLength > 0);

    int[] smoothedProfile = new int[profile.length];
    int offsetToLeft = 0 - smoothingKernelLength / 2;
    int offsetToRight = offsetToLeft + smoothingKernelLength;

    for (int i = 0; i < profile.length; ++i)
    {
      int sum = 0;
      for (int j = offsetToLeft; j < offsetToRight; ++j)
      {
        int currentProfileIndex = i + j;
        if ((currentProfileIndex >= 0) && (currentProfileIndex < profile.length))
          sum += profile[currentProfileIndex];
        else if (currentProfileIndex < 0)
          sum += profile[0];
        else if (currentProfileIndex >= profile.length)
          sum += profile[profile.length - 1];
      }
      smoothedProfile[i] = sum / smoothingKernelLength;
    }
    return smoothedProfile;
  }


  /**
   * Calculate the weighted average index in the neighborhood around targetIndex.
   * @param dataArray Contains the weighted data.
   * @param targetIndex The center point of the average.
   * @param plusMinusSpan Average this many cells on either side of targetIndex.
   * @return The weighted average index.
   * @author Eddie Williamson
   */
  public static float calcWeightedAverageIndex(float[] dataArray, int targetIndex, int plusMinusSpan)
  {
    Assert.expect(dataArray != null);
    Assert.expect(targetIndex >= 0);
    Assert.expect(targetIndex < dataArray.length);
    Assert.expect(plusMinusSpan >= 0);

    float sumOfProducts = 0.0F;
    float sumOfWeights = 0.0F;

    // Truncate at the ends of the array. This has the effect of extending
    // the ends of the data array with zero weighting.
    int startIndex = Math.max(0, targetIndex - plusMinusSpan);
    int stopIndex = Math.min(dataArray.length - 1, targetIndex + plusMinusSpan);

    for (int i = startIndex; i <= stopIndex; i++)
    {
      sumOfProducts += dataArray[i] * i;
      sumOfWeights += dataArray[i];
    }
    float weightedAverage = sumOfProducts / sumOfWeights;

    return weightedAverage;
  }

  /**
   * @author Peter Esbensen
   */
  public static void normalizeProfile(float desiredMaxValue, float[] profile)
  {
    float maxValue = ArrayUtil.max(profile);
    float normalizationFactor = desiredMaxValue / maxValue;
    for (int i = 0; i < profile.length; ++i)
    {
      profile[i] *= normalizationFactor;
    }
  }

  /**
   * @author Peter Esbensen
   */
  public static void normalizeProfile(float desiredMaxValue, int[] profile)
  {
    int maxValue = ArrayUtil.max(profile);
    float normalizationFactor = desiredMaxValue / maxValue;
    for (int i = 0; i < profile.length; ++i)
    {
      profile[i] *= normalizationFactor;
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  public static void shiftProfile(int shiftValue, int[] profile)
  {
    int maxValue = ArrayUtil.max(profile);
    int minValue = ArrayUtil.min(profile);

    if(shiftValue > 0)
    {
      for (int i = 0; i < profile.length; ++i)
      {
        if (shiftValue + i >= profile.length)
        {
          profile[i] = maxValue;
        }
        else
        {
          profile[i] = profile[i + shiftValue];
        }
      }
    }
    else
    {
      for (int i = profile.length-1; i >= 0; --i)
      {
        if (shiftValue + i < 0)
        {
          profile[i] = minValue;
        }
        else
        {
          profile[i] = profile[i + shiftValue];
        }
      }
    }
  }

  /**
   * @author Peter Esbensen
   */
  public static float[] addProfiles(float[] profile1,
                                    float[] profile2)
  {
    Assert.expect(profile1 != null);
    Assert.expect(profile2 != null);
    Assert.expect(profile1.length == profile2.length);

    float[] sumProfile = new float[profile1.length];
    for (int i = 0; i < profile1.length; ++i)
    {
      sumProfile[i] = profile1[i] + profile2[i];
    }
    return sumProfile;
  }

  /**
   * @author Peter Esbensen
   */
  public static void multiplyProfileByConstant(float[] profile,
                                               float multiplier)
  {
    Assert.expect(profile != null);

    for (int i = 0; i < profile.length; ++i)
    {
      profile[i] = profile[i] * multiplier;
    }
  }

  /**
   * Sums the values in the specified profile from startIndex to endIndex.
   *
   * @author George Booth
   */
  public static float findAreaUnderProfileInterval(float[] profile, int startIndex, int endIndex)
  {
    Assert.expect(profile != null);
    Assert.expect(startIndex >= 0 && startIndex < profile.length);
    Assert.expect(endIndex >= 0 && endIndex < profile.length);
    Assert.expect(startIndex <= endIndex);

    // Sum up the profile values
    float sum = 0.0f;
    for (int i = startIndex; i <= endIndex; i++)
    {
      sum += profile[i];
    }

    return sum;
  }

  /**
   * Finds the index of the interpolated maximum value near a specificed profile index
   * This uses a 3-point quadratic interpolation derived from the Lagrange interpolation formula
   *
   * @author George Booth
   */
  public static float findInterpolatedMaximumIndex(float[] profile, int index)
  {
    Assert.expect(profile != null);
    Assert.expect(index >= 0 && index < profile.length);

    // can't refine estimate if peak is at either end
    if (index == 0 || index == profile.length - 1)
    {
//      System.out.println("peak is at either end, can't interpolate");
      return (float)index;
    }
    int z0 = index - 1;
    int z1 = index;
    int z2 = index + 1;
    float s0 = profile[z0];
    float s1 = profile[z1];
    float s2 = profile[z2];

    // make sure we have a proper curve to interpolate (index is < or > two outer points)
    if ((s1 > s0 && s1 > s2) || (s1 < s0 && s1 < s2))
    {
      // have local max or min, so use 3-point quadratic interpolation
      // This is derived from the Lagrange interpolation formula:
      // s = s0 * (z - z1)* (z - z2) / ((z0 - z1) * (z0 - z2)) +
      //     s1 * (z - z0)* (z - z2) / ((z1 - z0) * (z1 - z2)) +
      //     s2 * (z - z0)* (z - z1) / ((z2 - z0) * (z2 - z1)) +

      float denom =
          2.0f * ((z1 - z2) * s0 + (z2 - z0) * s1 + (z0 - z1) * s2);
      float z0Sq = z0 * z0;
      float z1Sq = z1 * z1;
      float z2Sq = z2 * z2;

      if (denom == 0.0f)
      {
        // can't divide by zero - return original position
//        System.out.println("denominator = 0; use z1");
        return z1;
      }
      else
      {
        float zMax = ((z1Sq - z2Sq) * s0 +
                      (z2Sq - z0Sq) * s1 +
                      (z0Sq - z1Sq) * s2) / denom;

        // make sure zMax is between two end points
        if (zMax > z0 && zMax < z2)
        {
          return zMax;
        }
        else
        {
          // return original position
//          System.out.println("zMax out of range - use z1; z0 = " + z0 + ", z2 = " + z2 + ". zMax - " + zMax);
          return z1;
        }
      }
    }
    else
    {
      // no local max, so just return original position;
      // note that z1 corresponds to original index
//      System.out.println("no local max; use z1");
      return z1;
    }
  }

  /**
   * @param profile
   * @param shiftPoint
   */
  public static void generateBezierProfile(int[] profile, int shiftPoint)
  {
    int points = 4;
    int length = profile.length;
    int stepSize = length / points + shiftPoint;
    Point[] bezierPoints = new Point[points];
    int currentPos = 0;

    for (int i = 0; i < points - 1; i++)
    {
      bezierPoints[i] = new Point();
      bezierPoints[i].x = currentPos;
      bezierPoints[i].y = profile[currentPos];
      currentPos += stepSize;
      if (currentPos > length)
      {
        currentPos = length;
      }
    }
    bezierPoints[3] = new Point();
    bezierPoints[3].x = length-1;
    bezierPoints[3].y = profile[length-1];

    double[] B = new double[4];

    for (int step = 0; step < length; step++)
    {
      int sumX = 0;
      int sumY = 0;
      double t = (double) step / (double) length;
      double at = 1 - t;
      B[0] = at * at * at;
      B[1] = 3 * t * at * at;
      B[2] = 3 * t * t * at;
      B[3] = t * t * t;

      for (int p = 0; p < 4; p++)
      {
        sumX += bezierPoints[p].x * B[p];
        sumY += bezierPoints[p].y * B[p];
      }
      if(sumX == step)
        profile[step] = sumY;
      else
        profile[sumX] = sumY;
    }
  }


  /**
   * @param normalizeProfile
   * @param shiftValue
   * @author Chong Wei Chin
   */
  public static void generateLagrangeProfile(int[] normalizeProfile, float shiftValue)
  {
    int max  = ArrayUtil.max(normalizeProfile) + 1;
    int length  = normalizeProfile.length;
    int stepSize = Math.round( max / 3.0f );
    Point[] lagrangePoints = new Point[4];
    
//    Assert.expect(max > stepSize * 3 + shiftValue);
//    Assert.expect(stepSize + shiftValue > 0);

    int currentPos = 0;
    stepSize += shiftValue;

    for (int i = 0; i < 3; i++)
    {
      lagrangePoints[i] = new Point();
      lagrangePoints[i].x = currentPos;
      lagrangePoints[i].y = normalizeProfile[currentPos];
      currentPos += stepSize;
    }
//    for (int i = 0; i < length; i++)
//    {
//      if(normalizeProfile[i] == currentPos)
//      {
//        lagrangePoints[j] = new Point();
//        lagrangePoints[j].x = i;
//        lagrangePoints[j].y = normalizeProfile[currentPos];
//        currentPos += stepSize;
//        j++;
//      }
//    }

    lagrangePoints[3] = new Point();
    lagrangePoints[3].x = length - 1;
    lagrangePoints[3].y = normalizeProfile[length-1];

    for(int i=0; i<normalizeProfile.length; i++)
    {
      float ly1 = lagrangePoints[0].y *
                  ( (i - lagrangePoints[1].x) / (lagrangePoints[0].x - lagrangePoints[1].x) *
                    (i - lagrangePoints[2].x) / (lagrangePoints[0].x - lagrangePoints[2].x) *
                    (i - lagrangePoints[3].x) / (lagrangePoints[0].x - lagrangePoints[3].x) );
//                    (i - lagrangePoints[4].x) / (lagrangePoints[0].x - lagrangePoints[4].x) );

      float ly2 = lagrangePoints[1].y *
                  ( (i - lagrangePoints[0].x) / (lagrangePoints[1].x - lagrangePoints[0].x) *
                    (i - lagrangePoints[2].x) / (lagrangePoints[1].x - lagrangePoints[2].x) *
                    (i - lagrangePoints[3].x) / (lagrangePoints[1].x - lagrangePoints[3].x) );
//                    (i - lagrangePoints[4].x) / (lagrangePoints[1].x - lagrangePoints[4].x) );

      float ly3 = lagrangePoints[2].y *
                  ( (i - lagrangePoints[0].x) / (lagrangePoints[2].x - lagrangePoints[0].x) *
                    (i - lagrangePoints[1].x) / (lagrangePoints[2].x - lagrangePoints[1].x) *
                    (i - lagrangePoints[3].x) / (lagrangePoints[2].x - lagrangePoints[3].x) );
//                    (i - lagrangePoints[4].x) / (lagrangePoints[2].x - lagrangePoints[4].x) );

      float ly4 = lagrangePoints[3].y *
                  ( (i - lagrangePoints[0].x) / (lagrangePoints[3].x - lagrangePoints[0].x) *
                    (i - lagrangePoints[1].x) / (lagrangePoints[3].x - lagrangePoints[1].x) *
                    (i - lagrangePoints[2].x) / (lagrangePoints[3].x - lagrangePoints[2].x) );
//                    (i - lagrangePoints[4].x) / (lagrangePoints[3].x - lagrangePoints[4].x) );

//      float ly5 = lagrangePoints[4].y *
//                  ( (i - lagrangePoints[0].x) / (lagrangePoints[4].x - lagrangePoints[0].x) *
//                    (i - lagrangePoints[1].x) / (lagrangePoints[4].x - lagrangePoints[1].x) *
//                    (i - lagrangePoints[2].x) / (lagrangePoints[4].x - lagrangePoints[2].x) *
//                    (i - lagrangePoints[3].x) / (lagrangePoints[4].x - lagrangePoints[3].x) );

      normalizeProfile[i] = Math.round( ly1 + ly2 + ly3 + ly4 );
    }
  }


  /**
   * @param profileData
   * @param kernelRadius
   * @author Anthony Fong
   */
  static public float[] getGaussainSmoothedProfile(float[] profileData, int kernelRadius)
  {
    return GaussianSmoothing.GaussianKernelFitering(profileData, kernelRadius);
  }
}


