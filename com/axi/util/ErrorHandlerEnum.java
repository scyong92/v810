package com.axi.util;

import java.util.*;
import java.io.*;

/**
 *
 * @author bee-hoon.goh
 */
public class ErrorHandlerEnum extends Enum
{
  private static int _index = -1;
  private String _name;
  private String _filepath;
  public static List <ErrorHandlerEnum> _errorHandlerFullList = new ArrayList<ErrorHandlerEnum>();
  private static String _ERROR_ENUM_FULL_LIST_FILE = "infoTagFullList.txt";

  public static final ErrorHandlerEnum HW_COMM_SET_PIP_HWL_CONFIRM_PIP_STUCKED = new ErrorHandlerEnum(++_index, "HW_COMM_SET_PIP_HWL_CONFIRM_PIP_STUCKED" ,"panelHandler");
  public static final ErrorHandlerEnum HW_COMM_SET_PIP_HWL_CONFIRM_PANEL_DROP = new ErrorHandlerEnum(++_index, "HW_COMM_SET_PIP_HWL_CONFIRM_PANEL_DROP" ,"panelHandler");
  public static final ErrorHandlerEnum HW_COMM_SET_PIP_HWL_CONFIRM_PANEL_INSIDE = new ErrorHandlerEnum(++_index, "HW_COMM_SET_PIP_HWL_CONFIRM_PANEL_INSIDE" ,"panelHandler");
  public static final ErrorHandlerEnum SW_COMM_SET_IRP_HWL_STARTUP_FAILED = new ErrorHandlerEnum(++_index, "SW_COMM_SET_IRP_HWL_STARTUP_FAILED", "Business_ImageAcquisition_ImageAcquisitionEngine_AcquireAlignmentImages__ReconstructedImagesProducer_WaitIfAbortInProgress"); 
  public static final ErrorHandlerEnum HW_COMM_SET_STG_HWL_HOME_EXCEPTION = new ErrorHandlerEnum(++_index, "HW_COMM_SET_STG_HWL_HOME_EXCEPTION", "Hardware_Home_Exceptions");
  public static final ErrorHandlerEnum HW_COMM_SET_DIO_HWL_DIGITAL_IO_NOTINITIALIZED = new ErrorHandlerEnum(++_index, "HW_COMM_SET_DIO_HWL_DIGITAL_IO_NOTINITIALIZED", "Hardware_DigitalIo");
  public static final ErrorHandlerEnum HW_COMM_CDA_XCAM_HWL_CAMERA_BLOCKED = new ErrorHandlerEnum(++_index, "HW_COMM_CDA_XCAM_HWL_CAMERA_BLOCKED", "Camera_Grayscale_Adjustment");
 
  /**
   * @author bee-hoon.goh
   */
  private ErrorHandlerEnum(int id, String name, String description)
  {
    super(id);
    _name = name.intern();
    _filepath = description.intern();
    _errorHandlerFullList.add(this);
  }

  /**
   * Returns a String representation of the ErrorHandlingEnum class
   * @return String representation of the instance of ErrorHandlingEnum
   * @author bee-hoon.goh
   */
  public String toString()
  {
    return _name;
  }
  
  public String toFilepathString()
  {
    return _filepath;
  }
  
  public static List <ErrorHandlerEnum> getFullEnumList()
  {
    return _errorHandlerFullList;
  }
  
  public static void saveFullEnumListToFile()
  {
    String filePath = com.axi.v810.datastore.Directory.getTempDir() + File.separator + _ERROR_ENUM_FULL_LIST_FILE;
    try
    {
      File outputfile = new File(filePath);
      if (outputfile.exists())
      {
        outputfile.delete();
      }
      outputfile.createNewFile();
      BufferedWriter out = new BufferedWriter(new FileWriter(filePath));
      for (ErrorHandlerEnum enumTag : _errorHandlerFullList)
      {
        out.write(enumTag.toString());
        out.newLine();
      }
      out.close();
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }

}
