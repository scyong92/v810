package com.axi.util;

import java.io.*;
import java.net.*;

/**
 * This class provides a way to launch a browser from Java.
 *
 * @author Bill Darbie
 */
public class BrowserLauncher
{
  public static void main(String[] args) throws IOException
  {
    // test code
    openURL("http://mtddarbie.lvld.axi.com/");
    openURL("http://mtddarbie.lvld.axi.com/");
    openURLInNewWindow("http://mtddarbie.lvld.axi.com/");

    openURL("file:/c:/program files/v810/x.xx/docs/index.html");
    openURL("file:/c:/program files/v810/x.xx/docs/index.html");
    openURLInNewWindow("file:/c:/program files/v810/x.xx/docs/index.html");
  }

  /**
   * Open up the browser to the url passed in.
   * @author Bill Darbie
   */
  public static void openURL(URL url) throws IOException
  {
    Assert.expect(url != null);
    String urlStr = url.toString();
    openURL(urlStr);
  }

  /**
   * Attempts to open the URL in the same browser as previous calls.  For some reason if your URL
   * has spaces in it, a new browser will come up instead.
   * @author Bill Darbie
   */
  public static void openURL(String urlStr) throws IOException
  {
    Assert.expect(urlStr != null);
    String command = "rundll32 url.dll,FileProtocolHandler \"" + urlStr + "\"";
    executeCommand(command);
  }

  /**
   * Open up a new browser window to the url passed in.
   * @author Bill Darbie
   */
  public static void openURLInNewWindow(URL url) throws IOException
  {
    Assert.expect(url != null);
    openURLInNewWindow(url.toString());
  }

  /**
   * Open up a new browser window to the url passed in.
   * @author Bill Darbie
   */
  public static void openURLInNewWindow(String urlStr) throws IOException
  {
    Assert.expect(urlStr != null);
    String command = "iexplore -new \"" + urlStr + "\"";
    executeCommand(command);
  }

  /**
   * @author Bill Darbie
   */
  private static void executeCommand(String command) throws IOException
  {
    Process process = Runtime.getRuntime().exec(command);
  }
}
