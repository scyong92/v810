package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * @author Greg Esparza
 */
public class Test_FileWriterUtil extends UnitTest
{
  /**
   * @author Greg Esparza
   */
  public Test_FileWriterUtil()
  {
    // do nothing
  }

  /**
   * @author Greg Esparza
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    boolean append = true;
    String fileNameFullPath = getTestDataDir() + File.separator +  "fileWriterUtil.txt";
    FileWriterUtil fileWriterUtil = new FileWriterUtil(fileNameFullPath, append);

    try
    {
      fileWriterUtil.open();
      fileWriterUtil.write("Test - line #1");
      fileWriterUtil.close();

      fileWriterUtil.open();
      fileWriterUtil.write("Test - line #2");
      fileWriterUtil.write("Test - line #3");
      fileWriterUtil.write("Test - line #4");
      fileWriterUtil.close();

      fileWriterUtil.open();
      fileWriterUtil.writeln("Test - line #5");
      String formattedStr = "\n" +
                            "A" + "B" +
                            "\n" +
                            "\n" +
                            "C" + "\tOneTab" +
                            "\n" +
                            "D" + "\t\tTwoTab";
      formattedStr = StringUtil.replace(formattedStr, "\n", "\r\n");
      fileWriterUtil.writeln(formattedStr);
      fileWriterUtil.close();

      // Test writing an exception
      fileWriterUtil.open();
      CouldNotCopyFileException exception = new CouldNotCopyFileException(fileNameFullPath, fileNameFullPath);
      fileWriterUtil.write(exception);
      fileWriterUtil.writeln(exception);
      fileWriterUtil.close();

      // Test appending
      fileWriterUtil.append(formattedStr);
      fileWriterUtil.append(exception);

      List<String> messages = new ArrayList<String>();
      String message = "";
      for (int i = 0; i < 10; ++i)
      {
        message = "message" + String.valueOf(i);
        messages.add(message);
      }
      fileWriterUtil.append(messages);
    }
    catch (CouldNotCreateFileException cncfe)
    {
      cncfe.printStackTrace(os);
      Expect.expect(false);
    }

    try
    {
      FileUtil.delete(fileNameFullPath);
    }
    catch (CouldNotDeleteFileException cndfe)
    {
      cndfe.printStackTrace(os);
      Expect.expect(false);
    }
  }

  /**
   * @author Greg Esparza
   */
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_FileWriterUtil());
  }
}
