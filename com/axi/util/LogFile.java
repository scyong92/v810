package com.axi.util;

import java.io.*;
import java.util.Date;

/**
  * The LogFile class provides a way to save messages to a file.
  * A LogFile instance is created by passing in either a file name, file name
  * and a append flag or directory path, file name and append flag.  Passing in
  * only a file name or a null directory path will cause the class to look in
  * the properties file for a logDirectory specification.  If none is found, the
  * file is created in the local directory.  The append flag determines how the
  * file is opened, new or append mode.  The file is not opened unless a message
  * is sent to the file by calling either logMessage() or logException().  When
  * the file is opened, the first line written is a separator line and the
  * current date.  The LogFile works silently.  To find out if something is
  * wrong, check the return value from the logMessage() or logException()
  * methods.
  * Don't open the same file with two or more LogFile instances or unusual
  * things may happen.  It may work but the class is not thread safe so
  * deadlock could occur.
  *
  * @author Steve Anonson
  */
public class LogFile
{
  private boolean _append = false;
  private String _logFileDirectory = null;  // the log file directory path
  private String _logFileName = null;  // the file name to log the messages to

  private FileOutputStream _fileStream = null;
  private PrintWriter _fileWriter = null;

  /**
    * This constructor takes a directory path, file name and append flag.
    * If the directory path is null, the default directory is looked up in the
    * properties file.
    *
    * @param logDirectory the directory path string
    * @param logFileName the file name string
    * @param append true if append mode is desired
    * @author Steve Anonson
    */
  public LogFile( String logDirectory, String logFileName, boolean append )
  {
    _append = append;  // save the append flag
    String logDir = logDirectory;  // get the directory where the log file should go
    if (logDir == null)
    {
      // If no directory given, look in the properties for a default directory.
      try
      {
        logDir = System.getProperty("assertLogDir", logDir);
      }
      catch (SecurityException se)
      {
        logDir = "";
      }
    }
    _logFileDirectory = logDir;
    _logFileName = logFileName;
  }

  /**
    * This constructor takes a file name and append flag.  The directory
    * path defaults to null before calling another constructor.
    *
    * @param logFileName - file name string
    * @param append - true if append mode is desired
    * @author Steve Anonson
    */
  public LogFile( String logFileName, boolean append )
  {
    this( null, logFileName, append );
  }

  /**
    * This constructor takes only a file name.  The directory path defaults to
    * null and append mode is assumed before calling another contructor.
    *
    * @param logFileName - file name string
    * @author Steve Anonson
    */
  public LogFile( String logFileName )
  {
    this( null, logFileName, true );  // default to append mode
  }

  /**
    * This method opens the log file using the directory path, file name and
    * append mode passed in through the constructor.  Once the file is opened,
    * a header string consisting of a separator line and date is written to
    * the file.
    *
    * @return true if file is opened without error
    * @throws LogFileException if file name is empty or file can't be opened
    * @author Steve Anonson
    */
  private boolean openLogFile() throws LogFileException
  {
    if (logFileOpen())
      return true;  // only open the file once

    if (_logFileName == null || _logFileName.equals(""))
      throw ( new LogFileException("Empty file name.") );

    String fileName = new String();
    if (_logFileDirectory == null || _logFileDirectory.equals(""))
      fileName = _logFileName;
    else
      fileName = _logFileDirectory + File.separator + _logFileName;

    try
    {
      _fileStream = new FileOutputStream(fileName, _append);
    }
    catch(IOException ioe)
    {
      _fileStream = null;
      
      LogFileException ex = new LogFileException("Could not open file: " + fileName);
      ex.initCause(ioe);
      throw ex;
    }
    catch(SecurityException se)
    {
      // must be in an applet - do not write to a file
      _fileStream = null;
      LogFileException ex = new LogFileException("Security violation on file: " + fileName);
      ex.initCause(se);
      throw ex;
    }

    if (_fileStream != null)
      _fileWriter = new PrintWriter(_fileStream, true);
    if (_fileWriter != null)
    {
      // For the first write to the log file write the header.
      Date date = new Date();
      String headerString = "##############################################################################\n" + date;
      _fileWriter.println(headerString);
    }
    return logFileOpen();
  }

  /**
    * Tests whether the log file has been opened.
    *
    * @return boolean - true if the file is open
    * @author Steve Anonson
    */
  private boolean logFileOpen()
  {
    return (_fileWriter != null);
  }

  /**
    * This method opens the log file and writes the message parameter to the file.
    *
    * @throws LogFileException passed through from openLogFile()
    * @param message - string to be written to the log file
    * @author Steve Anonson
    */
  public void logMessage( String message ) throws LogFileException
  {
    if (openLogFile())
      _fileWriter.println(message);  // send message to file
  }

  /**
    * This method writes an exception's information to the log file.
    *
    * @throws LogFileException passed through from openLogFile()
    * @param exception - exception whose information should be written to the file
    * @author Steve Anonson
    */
  public void logException(Exception exception) throws LogFileException
  {
    if (openLogFile())
    {
      exception.printStackTrace(_fileWriter);
      _fileWriter.println();
      _fileWriter = null;
    }
  }

  /**
    * This method closes the log file.
    *
    * @author Steve Anonson
    */
  public void closeLogFile()
  {
    if (logFileOpen())
    {
      _fileWriter.flush();
      _fileWriter.close();
    }
  }

  protected void finalize() throws Throwable
  {
    closeLogFile();
    super.finalize();
  }
}
