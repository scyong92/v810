package com.axi.util;

import java.nio.*;

/**
 * This class is implemented to provide read and write binary data compatible with a
 * 32 bit C or Pascal program for any of the java.nio.ByteBuffer classes.
 *
 * PE:  You should probably use java.nio.ByteBuffers instead . . .
 *
 * <pre>
 * The size of C types are:
 * bool          : 1 byte
 * char          : 1 byte
 * unsigned char : 1 byte
 * short         : 2 bytes
 * unsigned short: 2 bytes
 * int           : 4 bytes
 * unsigned int  : 4 bytes
 * long          : 4 bytes
 * unsigned long : 4 bytes
 * float         : 4 bytes
 * double        : 8 bytes
 * </pre>
 *
 * @author Bill Darbie
 */
public class CDataByteBufferUtil
{
  private ByteBuffer _buffer;

  /**
   * @author Bill Darbie
   */
  public CDataByteBufferUtil(ByteBuffer buffer)
  {
    Assert.expect(buffer != null);
    _buffer = buffer;
  }

  /**
   * @return an unsigned byte from the buffer.  Since Java does not have unsigned types
   *         the value will be returned as a char (2 bytes)
   * @author Bill Darbie
   */
  public char getUnsignedByte()
  {
    return (char)(0x00FF & _buffer.get());
  }

  /**
   * @param index the index to read from relative to the current location in the buffer
   * @return an unsigned byte from the buffer.  Since Java does not have unsigned types
   *         the value will be returned as a char (2 bytes)
   * @author Bill Darbie
   */
  public char getUnsignedByte(int index)
  {
    return (char)(0x00FF & _buffer.get(index));
  }

  /**
   * @return a bollean value from the buffer.
   * @author Bill Darbie
   */
  public boolean getBoolean()
  {
    int bool = _buffer.get();
    if (bool != 0)
      return true;

    return false;
  }

  /**
   * @param index is the index to get the value from
   * @return a bollean value from the buffer.
   * @author Bill Darbie
   */
  public boolean getBoolean(int index)
  {
    _buffer.position(index);
    return getBoolean();
  }

  /**
   * Put the boolean value into the buffer.
   * @param bool the value to put
   * @author Bill Darbie
   */
  public void putBoolean(boolean bool)
  {
    if (bool)
      _buffer.put((byte)1);
    else
      _buffer.put((byte)0);
  }

  /**
   * Put the boolean value into the buffer.
   * @param index is the index where the boolean will be written
   * @param bool the value to put
   * @author Bill Darbie
   */
  public void putBoolean(int index, boolean bool)
  {
    _buffer.position(index);
    putBoolean(bool);
  }

  /**
   * @return an unsigned char from the buffer.  Since Java does not have unsigned types
   *         the value will be returned as a char (2 bytes)
   * @author Bill Darbie
   */
  public short getUnsignedChar()
  {
    // read in a byte only since C/C++ has one byte per char where Java has two
    return (short)getUnsignedByte();
  }

  /**
   * @param index the index to read from relative to the current location in the buffer
   * @return an unsigned char from the buffer.  Since Java does not have unsigned types
   *         the value will be returned as a char (2 bytes)
   * @author Bill Darbie
   */
  public char getUnsignedChar(int index)
  {
    // read in a byte only since C/C++ has one byte per char where Java has two
    return getUnsignedByte(index);
  }

  /**
   * @return an unsigned short from the buffer.  Since Java does not have unsigned types
   *         the value will be returned as an int (4 bytes)
   * @author Bill Darbie
   */
  public int getUnsignedShort()
  {
    return (int)(0x0000FFFF &_buffer.getShort());
  }

  /**
   * @param index the index to read from relative to the current location in the buffer
   * @return an unsigned short from the buffer.  Since Java does not have unsigned types
   *         the value will be returned as an int (4 bytes)
   * @author Bill Darbie
   */
  public int getUnsignedShort(int index)
  {
    return (int)(0x0000FFFF & _buffer.getShort(index));
  }

  /**
   * @return an unsigned int from the buffer.  Since Java does not have unsigned types
   *         the value will be returned as a long (8 bytes)
   * @author Bill Darbie
   */
  public long getUnsignedInt()
  {
    return (long)(0x00000000FFFFFFFF & _buffer.getInt());
  }

  /**
   * @param index the index to read from relative to the current location in the buffer
   * @return an unsigned short from the buffer.  Since Java does not have unsigned types
   *         the value will be returned as a long (8 bytes)
   * @author Bill Darbie
   */
  public long getUnsignedInt(int index)
  {
    return (long)(0x00000000FFFFFFFF & _buffer.getInt(index));
  }

  /**
   * Get a 32 C/C++ long (4 bytes for C/C++ not 8 bytes like Java's long)
   * @return a long (4 bytes) from the buffer.
   * @author Bill Darbie
   */
  public int getLong()
  {
    return _buffer.getInt();
  }

  /**
   * Get a 32 C/C++ long (4 bytes for C/C++ not 8 bytes like Java's long)
   * @param index the index to read from relative to the current location in the buffer
   * @return a long (4 bytes) from the buffer.
   * @author Bill Darbie
   */
  public int getLong(int index)
  {
    return _buffer.getInt(index);
  }

  /**
   * Write a C/C++ 32 long (4 bytes) into the buffer.  Note that Java's long is 8 bytes
   * but this putLong will only write 4 bytes since it is a C/C++ long.
   * @author Bill Darbie
   */
  public void putLong(int value)
  {
    _buffer.putInt(value);
  }

  /**
   * Write a C/C++ 32 long (4 bytes) into the buffer.  Note that Java's long is 8 bytes
   * but this putLong will only write 4 bytes since it is a C/C++ long.
   * @param index the index to write from relative to the current location in the buffer
   * @author Bill Darbie
   */
  public void putLong(int index, int value)
  {
    _buffer.putInt(index, value);
  }

  /*
   * Read a number of characters from the current position.  The last charater must
   * be '\0'.  The '\0' charater is not returned as part of the string read in.
   *
   * @param length the number of characters to read from the buffer including the null terminator
   *
   * @return the string read in.
   * @author Bill Darbie
   */
  public String getString(int length)
  {
    return getString(length, length);
  }

  /*
   * Read a number of characters from the index.  The last charater must
   * be '\0'.  The '\0' charater is not returned as part of the string read in.
   *
   * @param index is the index to read the String from
   * @param length the number of characters to read from the buffer including the null terminator
   *
   * @return the string read in.
   * @author Bill Darbie
   */
  public String getString(int index, int length)
  {
    return getString(index, length, length);
  }

  /*
   * Read a number of characters from the current position.  The last charater must
   * be '\0'.  The '\0' charater is not returned as part of the string read in.
   *
   * @param index is the absolute index to read the String from
   * @param length the number of characters to read from the byte array including the null terminator.
   * @param limit the number of characters to put into the string.
   *
   * @return the string read in.
   * @author Bill Darbie
   */
  public String getString(int index, int length, int limit)
  {
    _buffer.position(index);
    return getString(length, limit);
  }

  /**
   * Write out the String into a C/C++ style char*
   * @param str the String to be written
   * @author Bill Darbie
   */
  public void putString(String str)
  {
    Assert.expect(str != null);

    byte[] bytes = str.getBytes();
    _buffer.put(bytes);
    _buffer.put((byte)'\0');
  }

  /**
   * Write out the String into a C/C++ style char*
   * @param index is the absolute index to write the String to.
   * @param str the String to be written
   * @author Bill Darbie
   */
  public void putString(int index, String str)
  {
    _buffer.position(index);
    putString(str);
  }

  /**
   * Read a number of characters from the current position and return it as a string.
   * The character string doesn't have to be null terminated.
   *
   * @param length the number of characters to read.
   * @return the String created from the characters.
   * @author Bill Darbie
   */
  public String getCharArrayString(int length)
  {
    return getCharArrayString(length, length);
  }

  /**
   * Read a number of characters from the current position.  The character string doesn't
   * have to be null terminated.
   *
   * @param length the number of characters to read.
   * @param limit the number of characters to put in the String.
   * @return the String created from the characters.
   * @author Bill Darbie
   */
  public String getCharArrayString(int length, int limit)
  {
    Assert.expect(length >= 0);
    Assert.expect(limit >= 0);
    Assert.expect(limit >= length);

    byte bytes[] = new byte[length];
    _buffer.get(bytes);

    return new String(bytes, 0, limit).intern();
  }

  /**
   * Read a number of characters from the current position.  The character string doesn't
   * have to be null terminated.
   *
   * @param length the number of characters to read.
   * @param limit the number of characters to put in the String.
   * @return the String created from the characters.
   * @author Bill Darbie
   */
  public String getCharArrayString(int index, int length, int limit)
  {
    _buffer.position(index);
    return getCharArrayString(length, limit);
  }

  /*
   * Write a number of characters from the current position.  The last charater does
   * not need to be '\0'.
   *
   * @author Bill Darbie
   */
  public void putCharArrayString(String str)
  {
    Assert.expect(str != null);

    _buffer.put(str.getBytes());
  }

  /*
   * Write a number of characters from the current position.  The last charater does
   * not need to be '\0'.
   *
   * @param index is the index to write the character array to.
   * @author Bill Darbie
   */
  public void putCharArrayString(int index, String str)
  {
    _buffer.position(index);
    putCharArrayString(str);
  }

  /**
   * Read a Pascal style string of characters from the current position.  The total
   * number of bytes to read is passed in through the length parameter.  This
   * includes the first byte which contains the number of valid characters in
   * the string.  The value in the first byte determines how many of the
   * characters are put into the String to be returned.
   *
   * @param length the maximum number of characters to read
   * @return the String created from the characters.
   * @author Bill Darbie
   */
  public String getPascalString(int length)
  {
    Assert.expect(length >= 0);

    byte[] characters = new byte[length];
    _buffer.get(characters);
    int strLength = (int)characters[0];

    return new String(characters, 1, strLength).intern();
  }

  /**
   * Read a Pascal style string of characters from the current position.  The total
   * number of bytes to read is passed in through the length parameter.  This
   * includes the first byte which contains the number of valid characters in
   * the string.  The value in the first byte determines how many of the
   * characters are put into the String to be returned.
   *
   * @param length the maximum number of characters to read
   * @return the String created from the characters.
   * @author Bill Darbie
   */
  public String getPascalString(int index, int length)
  {
    _buffer.position(index);

    return getPascalString(length);
  }

  /**
   * Read in a Pascal style string from the current position.
   *
   * @return the String created from pascal string read
   * @author Bill Darbie
   */
  public String getPascalString()
  {
    int numCharacters = ((int)_buffer.get());

    byte[] characters = new byte[numCharacters];
    _buffer.get(characters);

    return new String(characters, 0, numCharacters).intern();
  }

  /**
   * Read a number of characters from the current position.  The character string doesn't
   * have to be null terminated.
   *
   * @param length the number of characters to read.
   * @return the String created from the characters.
   * @author Bill Darbie
   */
  public String getPascalPackedArray(int length)
  {
    return getCharArrayString(length);
  }

  /**
   * Read a number of characters from the current position.  The character string doesn't
   * have to be null terminated.
   *
   * @param length the number of characters to read.
   * @param limit the number of characters to put in the String that will be returned.
   * @return the String created from the characters.
   * @author Bill Darbie
   */
  public String getPascalPackedArray(int length, int limit)
  {
    return getCharArrayString(length, limit);
  }

  /**
   * Read a number of characters from the current position.  The character string doesn't
   * have to be null terminated.
   *
   * @param index is the index to get the packed array from
   * @param length the number of characters to read.
   * @param limit the number of characters to put in the String that will be returned.
   * @return the String created from the characters.
   * @author Bill Darbie
   */
  public String getPascalPackedArray(int index, int length, int limit)
  {
    _buffer.position(index);

    return getCharArrayString(length, limit);
  }

  /**
   * Write a Pascal style string of characters
   * The value in the first byte determines how many of the characters are put
   * into the String to be returned.
   *
   * @param aString is the string to be written
   * @author Bill Darbie
   */
  public void putPascalString(String aString)
  {
    Assert.expect(aString != null);

    int length = aString.length();
    Assert.expect(length < 256);

    byte[] bytes = aString.getBytes();
    _buffer.put((byte)length);
    _buffer.put(bytes);
  }


  /**
   * Write a Pascal style string of characters
   * The value in the first byte determines how many of the characters are put
   * into the String to be returned.
   *
   * @param index is the index to write the string to
   * @param aString is the string to be written
   * @author Bill Darbie
   */
  public void putPascalString(int index, String aString)
  {
    _buffer.position(index);
    putPascalString(aString);
  }

  /**
   * Write a Pascal style packed array of characters.
   *
   * @param aString is the string to be written
   * @author Bill Darbie
   */
  public void putPascalPackedArray(String aString)
  {
    Assert.expect(aString != null);

    byte[] bytes = aString.getBytes();
    _buffer.put(bytes);
  }

  /**
   * Write a Pascal style packed array of characters.
   *
   * @param index is the index to write the string to
   * @param aString is the string to be written
   * @author Bill Darbie
   */
  public void putPascalPackedArray(int index, String aString)
  {
    _buffer.position(index);
    putPascalPackedArray(aString);
  }
}

