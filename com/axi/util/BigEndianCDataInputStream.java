package com.axi.util;

import java.io.*;

/**
 * This class reads in binary data in the big endian format.  This
 * is the format that HP, Sun, Motorolla, etc use.
 *
 * @author Bill Darbie
 *
 * @see CDataInputStream
 */
public class BigEndianCDataInputStream extends CDataInputStream
{
  /**
   * @author Bill Darbie
   */
  public BigEndianCDataInputStream(InputStream is)
  {
    super(is);
  }

  /**
   * Reads a short type from the input file.  Assumes that a C++ short
   * is 2 bytes.
   *
   * @return the short value of the bytes.
   * @exception EOFException if at the end of input stream before reading 2 bytes.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public short readShort() throws EOFException, IOException
  {
    int ch1 = read();  // read a byte from the stream
    int ch2 = read();  // read a byte from the stream
    if ((ch1 | ch2) < 0)
      throw new EOFException();

    int value = (ch1 << 8) + (ch2 << 0);

    return (short)value;
  }

  /**
   * Reads a unsigned short type from the input file.  Assumes that a C++ short
   * is 2 bytes.
   *
   * @return the non-negative value of the bytes.
   * @exception EOFException if at the end of input stream before reading 2 bytes.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public int readUnsignedShort() throws EOFException, IOException
  {
    int ch1 = read();  // read a byte from the stream
    int ch2 = read();  // read a byte from the stream
    if ((ch1 | ch2) < 0)
      throw new EOFException();

    int value = (ch1 << 8) + (ch2 << 0);

    return value;
  }

  /**
   * Reads a int type from the input file.  Assumes that a C++ int
   * is 4 bytes.
   *
   * @return the int value of the bytes.
   * @exception EOFException  if at the end of input stream before reading 4 bytes.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public int readInt() throws EOFException, IOException
  {
    int ch1 = read();  // read a byte from the stream
    int ch2 = read();  // read a byte from the stream
    int ch3 = read();  // read a byte from the stream
    int ch4 = read();  // read a byte from the stream
    if ((ch1 | ch2 | ch3 | ch4) < 0)
      throw new EOFException();

    int value = (ch1 << 24) + (ch2 << 16) + (ch3 << 8) + (ch4 << 0);

    return value;
  }

  /**
   * Reads a double type from the input file.  Assumes that a C++ double
   * is 8 bytes.  Least Significant Byte is first.
   *
   * @return the double value of the bytes.
   * @exception EOFException if at the end of input stream before reading eight bytes.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public double readDouble() throws EOFException, IOException
  {
    int int1 = readInt();
    int int2 = readInt();

    long value = ((long)int1 << 32) + ((long)int2 & 0xFFFFFFFFL);

    return Double.longBitsToDouble(value);
  }
}