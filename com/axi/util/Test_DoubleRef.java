package com.axi.util;

import java.io.*;

/**
* Selftest class
*
* @author Bill Darbie
*/
public class Test_DoubleRef extends UnitTest
{
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_DoubleRef());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    DoubleRef doubleRef = new DoubleRef();

    // check that the default value of DoubleRef is 0.0
    Expect.expect(doubleRef.getValue() == 0.0);

    // now test the constructor
    doubleRef = new DoubleRef(2.2);
    Expect.expect(doubleRef.getValue() == 2.2);

    // now test the setValue() method
    doubleRef.setValue(-10.0);
    Expect.expect(doubleRef.getValue() == -10.0);
  }  
}
