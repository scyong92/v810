package com.axi.util;

import java.io.*;

/**
 * @author Bill Darbie
 */
public class CouldNotCopyFileException extends IOException
{
  private String _fromFile;
  private String _toFile;

  /**
   * @author Bill Darbie
   */
  public CouldNotCopyFileException(String fromFile, String toFile)
  {
    super("ERROR: Could not copy file " + fromFile + " to " + toFile);
    Assert.expect(fromFile != null);
    Assert.expect(toFile != null);
    _fromFile = fromFile;
    _toFile = toFile;
  }

  /**
   * @author Bill Darbie
   */
  public String getFromFile()
  {
    return _fromFile;
  }

  /**
   * @author Bill Darbie
   */
  public String getToFile()
  {
    return _toFile;
  }
}
