package com.axi.util;

import java.io.*;

/**
* This class provides a way to pass both a localized String key and
* its cooresponding arguements.
*
* @author Bill Darbie
*/
public class LocalizedString implements Serializable
{
  private String _key;
  private Object[] _arguments;

  /**
  * Set both the key and and the arguements.
  *
  * @param key is the localized string key that will be used to look up the real string.
  * @param arguments an array of arguments that go with the key.  A null array
  *            can be passed in if no arguments are used.
  *
  * @author Bill Darbie
  */
  public LocalizedString(String key, Object[] arguments)
  {
    Assert.expect(key != null);
    // arguments can be null

    _key = key;
    _arguments = arguments;
  }

  /**
  * @return the message key.
  * @author Bill Darbie
  */
  public String getMessageKey()
  {
    Assert.expect(_key != null);

    return _key;
  }

  /**
  * @return the message key arguments.  If none exist null is returned.
  * @author Bill Darbie
  */
  public Object[] getArguments()
  {
    return _arguments;
  }

  /**
   * @author Bill Darbie
   */
  public void setArguments(Object[] args)
  {
    Assert.expect(args != null);
    _arguments = args;
  }

  /**
   * This method should not be used in production code - it only helps for debugging things.
   * @author Bill Darbie
   */
  public String toString()
  {
    String message = "key was: " + _key + "\n";
    if (_arguments != null)
    {
      for(int i = 0; i < _arguments.length; ++i)
        message = message + "arg #" + i + " was: " + _arguments[i] + "\n";
    }

    return message;
  }
}
