package com.axi.util;

import java.io.*;

/**
 * @author Vincent Wong
 */
public class BadFileFormatException extends IOException
{
  private String _fileName;

  /**
   * @author Vincent Wong
   */
  public BadFileFormatException(String fileName)
  {
    super("ERROR: Bad file format " + fileName);
    Assert.expect(fileName != null);
    _fileName = fileName;
  }

  /**
   * @author Vincent Wong
   */
  public String getFileName()
  {
    return _fileName;
  }
}
