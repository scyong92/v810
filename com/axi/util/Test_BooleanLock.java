package com.axi.util;

import java.io.*;

/**
* Selftest class.
*
* Besides the usual tests of the constructor and the getters and setters, I am going to
* create a separate thread that will wait on a BooleanLock controlled by the main thread.
*
* @author Peter Esbensen
*/
public class Test_BooleanLock extends UnitTest
{
  private BooleanLock _readyLock;

  static public void main(String[] args)
  {
    UnitTest.execute(new Test_BooleanLock());
  }

  private void runWork()
  {
    try
    {
      _readyLock.waitUntilTrue();
      System.out.println("readyLock is now true");
    }
    catch (InterruptedException ex)
    {
      System.out.println("interrupted while waiting for readyLock to become true");
    }
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    BooleanLock booleanLock = new BooleanLock();

    // check that the default value of BooleanLock is false
    Expect.expect(booleanLock.isFalse());

    // test the constructor with the initializer
    booleanLock = new BooleanLock(true);
    Expect.expect(booleanLock.isTrue());
    booleanLock = new BooleanLock(false);
    Expect.expect(booleanLock.isFalse());

    // test the setValue method
    booleanLock.setValue(true);
    Expect.expect(booleanLock.isTrue());
    booleanLock.setValue(false);
    Expect.expect(booleanLock.isFalse());

    System.out.println("creating BooleanLock instance");
    _readyLock = new BooleanLock(false);

    System.out.println("creating new thread");
    Runnable runnable = new Runnable()
    {
      public void run()
      {
        try
        {
          runWork();
        }
        catch (Exception ex)
        {
          // if we ever get here something is seriously wrong
          ex.printStackTrace();
        }
      }
    };
    Thread internalThread = new Thread(runnable, "internal");
    internalThread.setDaemon(false);
    internalThread.start();

    try
    {
      System.out.println("about to sleep for 3 seconds");
      Thread.sleep(3000);
    }
    catch (InterruptedException ex)
    {
      // If we ever get here something is seriously wrong
      ex.printStackTrace();
    }
    System.out.println("about to setValue to true");
    _readyLock.setValue(true);
    try
    {
      Thread.sleep(2000);
    }
    catch (InterruptedException ex)
    {
      // If we ever get here something is seriously wrong
      ex.printStackTrace();
    }
  }
}
