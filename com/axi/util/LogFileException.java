package com.axi.util;

/**
  * This class implements the exception thrown by the LogFile class if a file
  * can't be opened for some reason.
  *
  * @see Exception
  * @author Steve Anonson
  */
public class LogFileException extends Exception
{
  LogFileException()
  {
    super();
  }
  
  LogFileException(String s)
  {
    super(s);
  }
}
