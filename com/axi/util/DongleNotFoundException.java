package com.axi.util;

/**
 * @author Chong, Wei Chin
 */
public class DongleNotFoundException extends LocalizedException{

  /**
   * @author Chong, Wei Chin
   */
  public DongleNotFoundException()
  {
    super(new LocalizedString("BUS_SOFTWARE_LICENSE_NOT_FOUND_KEY",
                              null));
  }
}