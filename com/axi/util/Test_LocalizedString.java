package com.axi.util;

import java.io.*;

/**
* Selftest class.
* @author Bill Darbie
*/
public class Test_LocalizedString extends UnitTest
{
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_LocalizedString());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    LocalizedString ls = new LocalizedString("KEY", null);
    Expect.expect(ls.getMessageKey().equals("KEY"));
    Expect.expect(ls.getArguments() == null);
  }  
}
