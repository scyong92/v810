package com.axi.util;

import java.io.*;

/**
 * This class is implemented to read and write binary data in big endian format
 * compatible with a 32 bit C or Pascal program with random access.
 *
 * @see RandomAccessFile
 * @see CDataRandomAccessFile
 * @author Michael Martinez-Schiferl
 */
public class BigEndianCDataRandomAccessFile extends CDataRandomAccessFile
{
  /**
   * @author Michael Martinez-Schiferl
   */
  public BigEndianCDataRandomAccessFile(String name, String mode) throws IOException
  {
    super(name, mode);
  }

  /**
   * @author Michael Martinez-Schiferl
   */
  public BigEndianCDataRandomAccessFile(File file, String mode) throws IOException
  {
    super(file, mode);
  }

  /**
   * The method reads two bytes, starting at the current position.
   * If the two bytes read, in order,
   * are <code>b1</code> and <code>b2</code>, where each of the two values is
   * between <code>0</code> and <code>255</code>, inclusive, then the
   * result is equal to:
   * <blockquote><pre>
   *     (short)((b1 &lt;&lt; 8) | b2)
   * </pre></blockquote>
   * <p>
   * This method blocks until the two bytes are read, the end of the
   * stream is detected, or an exception is thrown.
   *
   * @return     the next two bytes of this file, interpreted as a signed
   *             16-bit number.
   * @exception  EOFException  if this file reaches the end before reading
   *               two bytes.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public short readShort() throws EOFException, IOException
  {
    int int1 = read();
    int int0 = read();
    if ((int0 | int1) < 0)
      throw new EOFException();

    return (short)((int1 << 8) + (int0 << 0));
  }

  /**
   * The method reads two bytes, starting at the current position.
   * If the bytes read, in order, are
   * <code>b1</code> and <code>b2</code>, where
   * <code>0&nbsp;&lt;=&nbsp;b1, b2&nbsp;&lt;=&nbsp;255</code>,
   * then the result is equal to:
   * <blockquote><pre>
   *     (b1 &lt;&lt; 8) | b2
   * </pre></blockquote>
   * <p>
   * This method blocks until the two bytes are read, the end of the
   * stream is detected, or an exception is thrown.
   *
   * @return     the next two bytes of this file, interpreted as an unsigned
   *             16-bit integer.
   * @exception  EOFException  if this file reaches the end before reading
   *               two bytes.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public int readUnsignedShort() throws EOFException, IOException
  {
    int int1 = read();
    int int0 = read();
    if ((int0 | int1) < 0)
      throw new EOFException();

    return (0x0000FFFF & (int1 << 8) + (int0 << 0));
  }

  /**
   * The method reads four bytes, starting at the current position.
   * If the bytes read, in order, are <code>b1</code>,
   * <code>b2</code>, <code>b3</code>, and <code>b4</code>, where
   * <code>0&nbsp;&lt;=&nbsp;b1, b2, b3, b4&nbsp;&lt;=&nbsp;255</code>,
   * then the result is equal to:
   * <blockquote><pre>
   *     (b1 &lt;&lt; 24) | (b2 &lt;&lt; 16) + (b3 &lt;&lt; 8) + b4
   * </pre></blockquote>
   * <p>
   * This method blocks until the four bytes are read, the end of the
   * stream is detected, or an exception is thrown.
   *
   * @return     the next four bytes of this file, interpreted as an
   *             <code>int</code>.
   * @exception  EOFException  if this file reaches the end before reading
   *               four bytes.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public int readInt() throws EOFException, IOException
  {
    int int3 = read();
    int int2 = read();
    int int1 = read();
    int int0 = read();
    if ((int0 | int1 | int2 | int3) < 0)
      throw new EOFException();

    return (int3 << 24) + (int2 << 16) + (int1 << 8) + (int0 << 0);
  }

  /**
   * Reads an unsigned integer from the current position of the byte array.
   *
   * @return the next 4 bytes from the byte array as an unsigned int
   * @author Bill Darbie
   */
  public long readUnsignedInt() throws EOFException, IOException
  {
    return (long)(0x00000000FFFFFFFF & readInt());
  }

  /**
   * The method reads eight bytes, starting at the current position.
   * If the four bytes read, in order,
   * by the <code>readInt</code> method
   * and then converts that <code>long</code> to a <code>double</code>
   * using the <code>longBitsToDouble</code> method in
   * class <code>Double</code>.
   * <p>
   * This method blocks until the eight bytes are read, the end of the
   * stream is detected, or an exception is thrown.
   *
   * @return     the next eight bytes of this file, interpreted as a
   *             <code>double</code>.
   * @exception  EOFException  if this file reaches the end before reading
   *             eight bytes.
   * @see        java.io.RandomAccessFile#readLong()
   * @see        java.lang.Double#longBitsToDouble(long)
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public double readDouble() throws EOFException, IOException
  {
    int int1 = readInt();
    int int0 = readInt();

    long value = ((long)int1 << 32) + ((long)int0 & 0xFFFFFFFFL);

    return Double.longBitsToDouble(value);
  }

  /**
   * Writes a <code>short</code> to the random access file as two bytes, high byte first.
   * The write starts at the current position.
   *
   * @param      anInt   a <code>short</code> to be written.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writeShort(int anInt) throws IOException
  {
    byte byte0 = ((byte)(0x000000FF & (anInt >> 0)));
    byte byte1 = ((byte)(0x000000FF & (anInt >> 8)));

    write(byte1);
    write(byte0);
  }

  /**
   * Writes an <code>int</code> to the random access file as four bytes, high byte first.
   * The write starts at the current position.
   *
   * @param      anInt   an <code>int</code> to be written.
   * @exception  IOException  if an I/O error occurs.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writeInt(int anInt) throws IOException
  {
    byte byte0 = ((byte)(0x000000FF & (anInt >> 0)));
    byte byte1 = ((byte)(0x000000FF & (anInt >> 8)));
    byte byte2 = ((byte)(0x000000FF & (anInt >> 16)));
    byte byte3 = ((byte)(0x000000FF & (anInt >> 24)));

    write(byte3);
    write(byte2);
    write(byte1);
    write(byte0);
  }

  /**
   * Writes a <code>long</code> to the random access file as four bytes.
   * This function discards the upper four bytes of the java long format
   * and only writes the lower four bites as it is in C.
   * The write starts at the current position.
   *
   * @param      aLong   a <code>long</code> to be written.
   * @exception  IOException  if an I/O error occurs.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writeLong(long aLong) throws IOException
  {
    byte byte0 = ((byte)(0x000000FF & (aLong >> 0)));
    byte byte1 = ((byte)(0x000000FF & (aLong >> 8)));
    byte byte2 = ((byte)(0x000000FF & (aLong >> 16)));
    byte byte3 = ((byte)(0x000000FF & (aLong >> 24)));

    write(byte3);
    write(byte2);
    write(byte1);
    write(byte0);
  }

  /**
   * Converts the double argument to a <code>long</code> using the
   * <code>doubleToLongBits</code> method in class <code>Double</code>,
   * and then writes that <code>long</code> value to the random access file as an
   * eight-byte quantity, high byte first. The write starts at the current
   * position.
   *
   * @param      aDouble   a <code>double</code> value to be written.
   * @see        java.lang.Double#doubleToLongBits(double)
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writeDouble(double aDouble) throws IOException
  {
    long bits = Double.doubleToRawLongBits(aDouble);
    int int0 = ((int)(0x00000000FFFFFFFFL & bits));
    int int1 = ((int)(bits >>> 32));

    writeInt(int1);
    writeInt(int0);
  }
}