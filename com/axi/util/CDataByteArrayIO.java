package com.axi.util;

import java.io.*;

/**
 * This class is implemented to read or write binary data to or from a byte array as
 * if the byte array were a file.  It assumes the data is binary 32 bit C or Pascal
 * data.  It provides method definitions where it can.  Abstract methods are provided
 * in cases where the method definition will be different between little endian and
 * big endian byte ordering.
 *
 * PE:  You should probably use java.nio.ByteBuffers instead . . .
 *
 * <pre>
 * The size of C types are:
 * bool          : 1 byte
 * char          : 1 byte
 * unsigned char : 1 byte
 * short         : 2 bytes
 * unsigned short: 2 bytes
 * int           : 4 bytes
 * unsigned int  : 4 bytes
 * long          : 4 bytes
 * unsigned long : 4 bytes
 * float         : 4 bytes
 * double        : 8 bytes
 * </pre>
 *
 * @author Michael Martinez-Schiferl
 * @author Bill Darbie
 */
public abstract class CDataByteArrayIO implements DataOutput, DataInput
{
  protected byte[] _byteArray; // the array of bytes to be read or written
  protected int _position;  // the current position in the array

  /**
   * Create a CDataByteArrayIO class with a corresponding byte array for it to work on
   * @param byteArray the array that will be used to read and/or write binary
   *
   * @author Michael Martinez-Schiferl
   */
  public CDataByteArrayIO(byte[] byteArray)
  {
    Assert.expect(byteArray != null);

    _byteArray = byteArray;
    _position = 0;
  }

  /**
   * Read enough bytes from the original byte array passed into this classes constructor
   * into the byte array passed in as a parameter to this method.  The parameter passed in
   * will be completely filled with data.
   *
   * @param byteArray the array that will be filled with data
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void readFully(byte[] byteArray)
  {
    Assert.expect(byteArray != null);

    readFully(byteArray, 0, byteArray.length);
  }

  /**
   * Reads the array of bytes passed into this classes constructor and puts them into the
   * byte array passed into this method as a parameter.
   *
   * @param byteArray the array to put the bytes into.
   * @param offset the starting position in the byte array parameter to put the data into
   * @param length number of bytes to read
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void readFully(byte[] byteArray, int offset, int length)
  {
    Assert.expect(byteArray != null);
    Assert.expect(offset >= 0);
    Assert.expect(length >= 0);
    Assert.expect(_position + offset + length <= _byteArray.length);

    for(int i = offset; i < offset + length; ++i)
      byteArray[i] = readByte();
  }

  /**
   * Reads a byte from the current position in the byte array and returns it as a byte.
   *
   * @return the next available byte from the byte array.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public byte readByte()
  {
    Assert.expect(_position < _byteArray.length);

    return _byteArray[_position++];
  }

  /**
   * Reads an unsigned byte from the current position in the byte array.
   *
   * @return the next available byte from the byte array as an unsigned byte
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public int readUnsignedByte()
  {
    Assert.expect(_position < _byteArray.length);

    return (int)(0x00FF & _byteArray[_position++]);
  }

  /**
   * Reads a boolean type from the current position of the byte array.
   *
   * @return the next available byte from the byte array as a boolean
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public boolean readBoolean()
  {
    Assert.expect(_position < _byteArray.length);

    int bool = readByte();
    if (bool != 0)
      return true;

    return false;
  }

  /**
   * Reads a character from the current position of the byte array.
   *
   * @return the next available byte from the byte array as a character
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public char readChar()
  {
    Assert.expect(_position < _byteArray.length);

    return (char)_byteArray[_position++];
  }

  /**
   * Reads an unsigned character from the current position of the byte array.
   *
   * @return the next available byte from the byte array as an unsigned character
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public short readUnsignedChar()
  {
    return (short)(0x00FF & readUnsignedByte());
  }

  /**
   * Reads a short from the current position of the byte array.
   *
   * @return the next 2 bytes from the byte array as a short
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public abstract short readShort();

  /**
   * Reads an unsigned short from the current position of the byte array.
   *
   * @return the next 2 bytes from the byte array as an unsigned short
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public abstract int readUnsignedShort();

  /**
   * Reads an integer from the current position.
   *
   * @return the next 4 bytes from the byte array as an integer
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public abstract int readInt();

  /**
   * Reads an unsigned integer from the current position of the byte array.
   *
   * @return the next 4 bytes from the byte array as an unsigned int
   * @author Bill Darbie
   */
  public abstract long readUnsignedInt();

  /**
   * Reads a long from the current position of the byte array.
   *
   * @return the next 4 bytes from the byte array as a long
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public long readLong()
  {
    return (long)readInt();
  }

  /**
   * Reads a float from the current position of the byte array.
   *
   * @return the next 4 bytes from the byte array as a float
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public float readFloat()
  {
    return Float.intBitsToFloat(readInt());
  }

  /**
   * Reads a double from the current position of the byte array.
   *
   * @return the next 8 bytes from the byte array as a double
   * @author Michael Martinez-Schiferl
   */
  public abstract double readDouble();

  /**
   * @deprecated
   * @author Bill Darbie
   */
  public String readLine()
  {
    Assert.expect(false, "CDataRandomAccessFile.readLine is deprecated");
    return null;
  }

  /**
   * @author Bill Darbie
   */
  public String readUTF()
  {
    Assert.expect(false, "Method writeUTF() not implemented, C/C++ do not support UTF.");
    return null;
  }

  /**
   * Read in a string from the byte array.  The string must end in a '\0'.
   *
   * @param stringLength the number of characters to read.
   * @return the string read in
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public String readString(int stringLength)
  {
    return readString(stringLength, stringLength);
  }

  /**
   * Read a number of characters from the current position.  The last charater must
   * be '\0'.  The '\0' charater is not returned as part of the string read in.
   *
   * @param numCharactersToRead the number of characters to read from the byte array.
   * @param numCharactersToUse the number of characters to put into the string.  Sometimes
   *        it is useful to read more characters in than you want to use.  To do this
   *        numCharactersToUse would be less than numCharactersToRead.
   * @return the string read in.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public String readString(int numCharactersToRead, int numCharactersToUse)
  {
    Assert.expect(numCharactersToRead >= 0);
    Assert.expect(numCharactersToUse >= 0);
    Assert.expect(numCharactersToUse <= numCharactersToRead);

    byte bytes[] = new byte[numCharactersToRead];
    readFully(bytes);

    if (numCharactersToRead > 0)
      Assert.expect(bytes[numCharactersToRead - 1] == '\0');

    // get rid of the '\0' before creating the string
    if ((numCharactersToUse == numCharactersToRead) && (numCharactersToUse > 0))
      numCharactersToUse--;

    return new String(bytes, 0, numCharactersToUse).intern();
  }

  /**
   * Read a number of characters from the current position and return it as a string.
   * The character string doesn't have to be null terminated.
   *
   * @param numCharactersToRead the number of characters to read.
   * @return the String created from the characters.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public String readCharArrayString(int numCharactersToRead)
  {
    return readCharArrayString(numCharactersToRead, numCharactersToRead);
  }

  /**
   * Read a number of characters from the current position.  The character string doesn't
   * have to be null terminated.
   *
   * @param numCharactersToRead the number of characters to read.
   * @param numCharactersToUse the number of characters to put in the String.  Sometimes
   *        it is useful to read in more characters than you need to use because of the
   *        format of the binary data.
   * @return the String created from the characters.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public String readCharArrayString(int numCharactersToRead, int numCharactersToUse)
  {
    Assert.expect(numCharactersToRead >= 0);
    Assert.expect(numCharactersToUse >= 0);
    Assert.expect(numCharactersToUse <= numCharactersToRead);

    byte[] myBytes = new byte[numCharactersToRead];
    readFully(myBytes);

    return new String (myBytes, 0, numCharactersToUse).intern();
  }

  /**
   * Read a Pascal style string of characters from the current position.  The total
   * number of bytes to read is passed in through the length prarameter.  This
   * includes the first byte which contains the number of valid characters in
   * the string.  The value in the first byte determines how many of the
   * characters are put into the String to be returned.
   *
   * @param numCharacters the maximum number of characters to read
   * @return the String created from the characters.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  private String readPascalString(int numCharacters)
  {
    Assert.expect(numCharacters >= 0);

    byte[] characters = new byte[numCharacters];
    readFully(characters);
    int strLength = (int)characters[0];

    return new String(characters, 1, strLength).intern();
  }

  /**
   * Read in a Pascal style string from the current position.
   *
   * @return the String created from pascal string read
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public String readPascalString()
  {
    int numCharacters = (int)readByte();

    byte[] characters = new byte[numCharacters];
    readFully(characters);

    return new String(characters, 0, numCharacters).intern();
  }

  /**
   * Read a number of characters from the current position.  The character string doesn't
   * have to be null terminated.
   *
   * @param length the number of characters to read.
   * @return the String created from the characters.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public String readPascalPackedArray(int length)
  {
    return readCharArrayString(length);
  }

  /**
   * Read a number of characters from the current position.  The character string doesn't
   * have to be null terminated.
   *
   * @param numCharactersToRead the number of characters to read.
   * @param numCharactersToUse the number of characters to put in the String that will be returned.
   * @return the String created from the characters.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public String readPascalPackedArray(int numCharactersToRead, int numCharactersToUse)
  {
    return readCharArrayString(numCharactersToRead, numCharactersToUse);
  }

  /**
   * Write the specified byte to the byte array.
   *
   * @param aByte is the byte to be written to the byte array
   * @author Bill Darbie
   */
  public void write(int aByte)
  {
    writeByte(aByte);
  }

  /**
   * Writes the data in byteArray to the internal byte array used by this class.
   *
   * @param byteArray the data to be written to the internal byte array.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void write(byte[] byteArray)
  {
    Assert.expect(byteArray != null);
    Assert.expect(_byteArray.length - _position > byteArray.length);

    for(int i = 0; i < byteArray.length; ++i)
      _byteArray[_position++] = byteArray[i];
  }

  /**
   * Writes <code>length</code> bytes from array
   * <code>byteArray</code>, in order,  to
   * the byte array.  If <code>length</code> is zero,
   * then no bytes are written. Otherwise, the
   * byte <code>byteArray[offset]</code> is written first,
   * then <code>btyeArray[offset+1]</code>, and so on; the
   * last byte written is <code>btyeArray[offset+length-1]</code>.
   *
   * @param byteArray the data.
   * @param offset the start offset in the data.
   * @param length the number of bytes to write.
   * @author Bill Darbie
   */
  public void write(byte[] byteArray, int offset, int length)
  {
    Assert.expect(byteArray != null);
    Assert.expect(offset >= 0);
    Assert.expect(length >= 0);
    Assert.expect(offset + length < byteArray.length);
    Assert.expect(_byteArray.length - _position > length);

    for(int i = offset; i < length; ++i)
      _byteArray[_position++] = byteArray[i];
  }

  /**
   * @author Bill Darbie
   */
  public void writeChar(int aChar)
  {
    Assert.expect(false, "Method writeChar() not implemented, C/C++ do not support 2 byte chars.");
  }

  /**
   * @author Bill Darbie
   */
  public void writeChars(String aString)
  {
    Assert.expect(false, "Method writeChars() not implemented, C/C++ do not support 2 byte chars.");
  }

  /**
   * @author Bill Darbie
   */
  public void writeUTF(String aString)
  {
    Assert.expect(false, "Method writeUTF() not implemented, C/C++ do not support UTF.");
  }

  /**
   * Writes a boolean to the byte array passed in this classes constructor.
   * If the argument <code>aBoolean</code>
   * is <code>true</code>, the value <code>(byte)1</code>
   * is written; if <code>value</code> is <code>false</code>,
   * the  value <code>(byte)0</code> is written.
   *
   * @param bool the boolean to be written to the byte array passed in this classes constructor.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writeBoolean(boolean bool)
  {
    if (bool)
      writeByte((byte)0x01);
    else
      writeByte((byte)0x00);
  }

  /**
   * Writes the byte parameter into the byte array passed into this classes constructor
   *
   * @param aByte the byte to be written to the byte array passed into this classes constructor
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writeByte(byte aByte)
  {
    Assert.expect(_position < _byteArray.length);
    _byteArray[_position++] = aByte;
  }

  /**
   * Writes to the byte array passed in through this classes constructor the eight low-
   * order bits of the argument <code>aByte</code>.
   * The 24 high-order bits of <code>aByte</code>
   * are ignored. (This means  that <code>writeByte</code>
   * does exactly the same thing as <code>write</code>
   * for an integer argument.)
   *
   * @param aByte the byte value to be written to the byte array passed in through
   *              this classes constructor
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writeByte(int aByte)
  {
    Assert.expect(_position < _byteArray.length);
    _byteArray[_position++] = ((byte)(aByte & 0x000000FF));
  }

  /**
   * Writes a short (two bytes) to the byte array passed in through this classes contructor
   * The byte values to be written, in the  order
   * shown, are for big endian: <p>
   * <pre><code>
   * (byte)(0xff &amp; (value &gt;&gt; 8))
   * (byte)(0xff &amp; value)
   * </code> </pre> <p>
   * for little endian:
   * <pre><code>
   * (byte)(0xff &amp; value)
   * (byte)(0xff &amp; (value &gt;&gt; 8))
   * </code> </pre> <p>
   *
   * @param aShort the <code>short</code> value to be written to the byte array passed in through this
   *               classes contructor
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public abstract void writeShort(int aShort);

  /**
   * Writes an integer value (4 bytes) to the byte array passed in through this classes contructor.
   * The byte values to be written, in the  order
   * shown, are for big endian:
   * <p><pre><code>
   * (byte)(0xff &amp; (value &gt;&gt; 24))
   * (byte)(0xff &amp; (value &gt;&gt; 16))
   * (byte)(0xff &amp; (value &gt;&gt; &#32; &#32;8))
   * (byte)(0xff &amp; value)
   * </code></pre><p>
   * for little endian:
   * <p><pre><code>
   * (byte)(0xff &amp; (value &gt;&gt; 24))
   * (byte)(0xff &amp; (value &gt;&gt; 16))
   * (byte)(0xff &amp; (value &gt;&gt; &#32; &#32;8))
   * (byte)(0xff &amp; value)
   * </code></pre><p>
   *
   * @param anInt the <code>int</code> value to be written.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public abstract void writeInt(int anInt);

  /**
   * Writes a long value (4 bytes) to the byte array passed in to this classes contructor.
   * The byte values to be written, in the  order
   * shown, are:
   * <p><pre><code>
   * (byte)(0xff &amp; (v &gt;&gt; 24))
   * (byte)(0xff &amp; (v &gt;&gt; 16))
   * (byte)(0xff &amp; (v &gt;&gt;  8))
   * (byte)(0xff &amp; v)
   * </code></pre><p>
   * The upper four bytes of the Java 8 byte long are discarded.
   *
   * @param aLong the <code>long</code> value to be written to the byte array passed in to
   *              this classes contructor.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public abstract void writeLong(long aLong);

  /**
   * Writes a float (4 bytes) to the byte array passed into this classes contructor.
   * It does this as if it first converts this
   * float value to an int in exactly the manner of the <code>Float.floatToIntBits</code>
   * method  and then writes the <code>int</code>
   * value in exactly the manner of the  <code>writeInt</code>
   * method.
   *
   * @param aFloat the <code>float</code> value to be written to the byte array passed in to
   *                this classes contructor.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writeFloat(float aFloat)
  {
    long bits = Float.floatToRawIntBits(aFloat);
    writeLong(bits);
  }

  /**
   * Writes a double value (8 bytes)to the byte array passed in to this classes contructor.
   * It does this as if it first converts this
   * <code>double</code> value to a <code>long</code>
   * in exactly the manner of the <code>Double.doubleToLongBits</code>
   * method  and then writes the <code>long</code>
   * value in exactly the manner of the  <code>writeLong</code>
   * method.
   *
   * @param aDouble the <code>double</code> value to be written to the byte array passed into
   *                this classes contructor
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public abstract void writeDouble(double aDouble);

  /**
   * Writes a string to the byte array passed into this classes constructor.
   * For every character in the string
   * <code>aString</code>,  taken in order, one byte
   * is written to byte array passed into this classes contructor.
   *
   * @param aString the string of bytes to be written.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writeBytes(String aString)
  {
    Assert.expect(aString != null);

    write(aString.getBytes());

    // add on a C/C++ style null terminator
    writeInt(0);
  }

  /**
   * Write a Pascal style string of characters to the byte array passed into this classes constructor.
   * The value in the first byte determines how many of the characters are put
   * into the String to be returned.
   *
   * @param aString is the string to be written
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writePascalString(String aString)
  {
    Assert.expect(aString != null);

    int length = aString.length();
    byte[] bytes = aString.getBytes();
    writeByte((byte)(0x000000FF & length));
    write(bytes);
  }

  /**
   * Write a Pascal style packed array of characters to the byte array passed into this classes constructor.
   *
   * @param aString is the string to be written
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writePascalPackedArray(String aString)
  {
    Assert.expect(aString != null);

    byte[] bytes = aString.getBytes();
    write(bytes);
  }

  /**
   * Set the current index into the byte array that was passed into this classes contructor.
   * All read and write methods in this class will operate on the byte array at that index.
   *
   * @author Michael Martinez-Schiferl
   */
  public void setPosition(int newPosition)
  {
    Assert.expect(newPosition >= 0);
    Assert.expect(newPosition < _byteArray.length);

    _position = newPosition;
  }

  /**
   * @return the current position in the byte array that was passed into this classes contructor
   * @author Michael Martinez-Schiferl
   */
  public int getPosition()
  {
    return _position;
  }

  /**
   * Attempts to skip over <code>n</code> bytes of input discarding the
   * skipped bytes.
   * <p>
   *
   * This method may skip over some smaller number of bytes, possibly zero.
   * This may result from any of a number of conditions; reaching end of
   * file before <code>n</code> bytes have been skipped is only one
   * possibility. This method never throws an <code>EOFException</code>.
   * The actual number of bytes skipped is returned.  If <code>n</code>
   * is negative, no bytes are skipped.
   *
   * @param      numberOfBytes the number of bytes to be skipped.
   * @return     the actual number of bytes skipped.
   * @exception  IOException  if an I/O error occurs.
   * @author Michael Martinez-Schiferl
   */
  public int skipBytes(int numberOfBytes)
  {
    int numBytes = 0;
    if (numberOfBytes > 0)
    {
      if (_position + numberOfBytes < _byteArray.length)
        numBytes = numberOfBytes;
      else
        numBytes = _byteArray.length - _position;

      _position = _position + numBytes;
    }

    return numBytes;
  }
}

