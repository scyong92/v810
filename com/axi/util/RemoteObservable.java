package com.axi.util;

import java.util.*;
import java.rmi.*;
import java.rmi.server.*;

/**
* RemoteObservable provides the same functionality as java.util.Observable
* except that it supports the Observer/Observable design pattern through
* RMI calls.  Extend any class that you want to be Observable with
* this class just as you would with java.util.Observable.  Your remote Observable
* RMI interface class will also need to implement the RemoteObservableInt so that
* the methods in this class can be called remotely.  Be aware that
* since this class uses RMI your program will have to deal with multiple
* threads.  Be sure to use synchronized methods where you need to.
*
* This class is written to handle multiple threads.  It provides the following multiple
* threading features.  The notifyObservers() call returns immediately.  The actual updating of
* observers happens on anther thread.  This mimimizes how much the code calling the notifyObservers()
* will be slowed down.  It also decreases the chance of a deadlock.  The update() method will
* be called in the same order that the notifyObservers() calls come in.
*
* Observers should ALWAYS spawn another thread to handle the update call.  This allows this
* class to notify all observers as quickly as possible.  It also prevents thread deadlock.
* Also the observers update() call should be synchonized since it is possible for more than
* one thread to call it.  If your update call is going to call any Swing methods, you must
* call SwingUtils.invokeLater() to get the GUI calls on the proper thread.
*
* @see RemoteObservableInt
* @see java.util.Observable
* @see javax.swing.SwingUtilities#invokeLater(Runnable)
* @author Bill Darbie
*/
public class RemoteObservable extends UnicastRemoteObject implements RemoteObservableInt
{
  private ThreadSafeObservable _threadSafeObservable;
  private Map<RemoteObserverInt, RemoteObserver> _observerMap;

  /**
  * Since this class extends UnicastRemoteObject the UnicastRemoteObjects
  * constructor will take care of exporting this class so rmi calls to
  * it can be made.
  *
  * @author Bill Darbie
  */
  public RemoteObservable() throws RemoteException
  {
    _threadSafeObservable = new ThreadSafeObservable("RemoteObservable thread");
    _observerMap = Collections.synchronizedMap(new HashMap<RemoteObserverInt, RemoteObserver>());
  }

  /**
  * Add a remote observer to the list of Observers that will get called
  * when a notifyObservers() call is made.
  *
  * @author Bill Darbie
  */
  // synchronize so _observerSet does not get accessed by more than one
  // thread at a time
  public void addObserver(RemoteObserverInt remoteObserver)
  {
    Assert.expect(remoteObserver != null);
    // do not add the same observer twice
    if (_observerMap.containsKey(remoteObserver) == false)
    {
      RemoteObserver ro = new RemoteObserver(this, remoteObserver);
      _observerMap.put(remoteObserver, ro);
      _threadSafeObservable.addObserver(ro);
    }
  }

  /**
  * Delete an Observer from the list of Observers that will get called
  * when a notifyObservers() call is made
  *
  * @author Bill Darbie
  */
  // synchronize so _observerSet does not get accessed by more than one
  // thread at a time
  public void deleteObserver(RemoteObserverInt remoteObserver)
  {
    RemoteObserver ro = (RemoteObserver)_observerMap.remove(remoteObserver);

    // if ro is null then the remoteObserver is not an observer of this class
    // maybe it was never added, or maybe it was thrown out because
    // a remote exception occurred
    if (ro != null)
      _threadSafeObservable.deleteObserver(ro);
  }

  /**
  * Delete the entire list of Observers.
  *
  * @author Bill Darbie
  */
  // synchronize so _observerSet does not get accessed by more than one
  // thread at a time
  public synchronized void deleteObservers()
  {
    _observerMap.clear();
    _threadSafeObservable.deleteObservers();
  }

  /**
  * Iterate over the list of observers and call their update() method to
  * tell them that some data that they are observing has changed.  Note that
  * the update method that this calls MUST be synchronized and must spawn off a new
  * thread to do any calls back into the server.  Failure to do this will cause
  * a potential deadlock situation or unpredictable program behavior.  If your
  * update() method is going to call Swing methods it must use SwingUtils.invokeLater()
  *
  * @see javax.swing.SwingUtilities#invokeLater(Runnable)
  * @author Bill Darbie
  */
  public void notifyObservers()
  {
    _threadSafeObservable.notifyObservers();
  }

  /**
  * Iterate over the list of observers and call their update() method to
  * tell them that some data that they are observing has changed.  Note that
  * the update method that this calls MUST be synchronized and must spawn off a new
  * thread to do any calls back into the server.  Failure to do this will cause
  * a potential deadlock situation or unpredictable program behavior.  If your
  * update() method is going to call Swing methods it must use SwingUtils.invokeLater()
  *
  * @see javax.swing.SwingUtilities#invokeLater(Runnable)
  * @author Bill Darbie
  */
  public void notifyObservers(Object arg)
  {
    _threadSafeObservable.notifyObservers(arg);
  }

  /**
  * Return a count of the number of Observers that will be notified if
  * notifyObservers is called.
  *
  * @author Bill Darbie
  */
  public int countObservers()
  {
    return _threadSafeObservable.countObservers();
  }

  /**
  * @return true if something in the Observable class has changed since the last notifyObservers()
  * call was made.
  *
  * @author Bill Darbie
  */
  public boolean hasChanged()
  {
    return _threadSafeObservable.hasChanged();
  }

  /**
  * Notifies this class that something has changed in the Observable data.
  * If notifyObservers() is called and setChanged was never called, then
  * observers update() methods will NOT be called since no data was changed.
  *
  * @author Bill Darbie
  */
  protected void setChanged()
  {
    _threadSafeObservable.setChanged();
  }

  /**
  * clears the flag that is set by setChanged()
  * @author Bill Darbie
  */
  protected void clearChanged()
  {
    _threadSafeObservable.clearChanged();
  }
}
