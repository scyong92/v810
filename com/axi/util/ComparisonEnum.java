package com.axi.util;

/**
 * @author Bill Darbie
 */
public class ComparisonEnum extends com.axi.util.Enum
{
  public static int _index = -1;
  public static ComparisonEnum EQUAL = new ComparisonEnum(++_index);
  public static ComparisonEnum LESS_THAN = new ComparisonEnum(++_index);
  public static ComparisonEnum GREATER_THAN = new ComparisonEnum(++_index);
  public static ComparisonEnum LESS_THAN_OR_EQUAL = new ComparisonEnum(++_index);
  public static ComparisonEnum GREATER_THAN_OR_EQUAL = new ComparisonEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private ComparisonEnum(int id)
  {
    super(id);
  }
}
