package com.axi.util;

/**
 * Enumeration of units for use with the com.axi.util.MathUtil class for
 * converting between units of measurement.
 *
 * @author Keith Lee
 */
public class MathUtilEnum extends Enum
{
  private static int _index = -1;
  private String _name;
  private boolean _isMetric;

  public static final MathUtilEnum MILS = new MathUtilEnum(++_index, "mils", false);
  public static final MathUtilEnum INCHES = new MathUtilEnum(++_index, "inches", false);
  public static final MathUtilEnum MILLIMETERS = new MathUtilEnum(++_index, "millimeters", true);
  public static final MathUtilEnum METERS = new MathUtilEnum(++_index, "meters", true);
  public static final MathUtilEnum NANOMETERS = new MathUtilEnum(++_index, "nanometers", true);
  public static final MathUtilEnum MICRONS = new MathUtilEnum(++_index, "microns", true);

  /**
   * @author Keith Lee
   */
  private MathUtilEnum(int id, String name, boolean isMetric)
  {
    super(id);
    _name = name.intern();
    _isMetric = isMetric;
  }

  /**
   * Returns a String representation of the MathUtilEnum class
   * @return String representaion of the instance of MathUtilEnum
   * @author Keith Lee
   */
  public String toString()
  {
    return _name;
  }

  /**
   * @author Patrick Lacz
   */
  public boolean isMetric()
  {
    return _isMetric;
  }
}
