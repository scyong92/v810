package com.axi.util;

import java.lang.ref.*;
import java.util.*;

/**
* ThreadSafeObservable provides the same functionality as java.util.Observable
* except that it supports the Observer/Observable design pattern in a mutli-threaded environment.
* Extend any class that you want to be Observable with this class just as you would with
* java.util.Observable.  Your Observer class will need to implement the Observer interface
* just like you would with the java.util.Observer class.
*
* This class is written to handle multiple threads.  It provides the following multiple
* threading features.  The notifyObservers() call returns immediately.  The actual updating of
* observers happens on another thread.  This mimimizes how much the code calling the notifyObservers()
* will be slowed down.  It also decreases the chance of a deadlock.  The update() method will
* be called in the same order that the notifyObservers() calls come in.
*
* Observers should ALWAYS spawn another thread to handle the update call.  This allows this
* class to notify all observers as quickly as possible.  It also prevents thread deadlock.
* Also the observers update() call should be synchonized since it is possible for more than
* one thread to call it.  If your update call is going to call any Swing methods, you must
* call SwingUtilites - invokeLater() to get the GUI calls on the proper thread.
*
* If you need an Observer/Observable that uses RMI see the RemoteObservable and RemoteObserverInt
* classes.
*
* @see java.util.Observable
* @see javax.swing.SwingUtilities - invokeLater(Runnable)
* @see RemoteObservable
* @see RemoteObserverInt
* @author Bill Darbie
*/
// This class has a PhantomReference to the ThreadSafeObservable class that is wrapping it.  That reference allows
// this class to know when there are no references to the ThreadSafeObservable anymore.
// When that happens this class will stop running its thread which frees up
// the thread resource and also allows the garbage collector to collect this class.
class WrappedThreadSafeObservable extends Observable implements Runnable
{
  // use the Set interface so the same observable cannot be in the set more than once
  private Set<Observer> _observerSet; // the set of all observers
  private volatile int _changed;     // something being observed has changed if > 0
  private List<Object> _args;        // parameter to be passed in the update() call

  // Lock objects allow me to lock the code on only the object that should not have two
  // or more threads accessing it at a time.  This is better than locking the entire class since
  // it allows things to run faster.
  private Object _changedLock = new Object();
  private Object _observerSetLock = new Object();

  // use a phantom reference to determine when ThreadSafeObservable is no longer being referenced
  // when that happens this classes run() method should exit so that it can be garbage collected
  // and the thread resource will be freed
  private PhantomReference<ThreadSafeObservable> _phantomReference;
  private ThreadSafeObservable _threadSafeObservable;

  /**
   * @author Bill Darbie
   */
  private WrappedThreadSafeObservable()
  {
    // do nothing
  }

  /**
   * @param threadName is the name that will show up in a debugger for the thread this Observable uses.
   * @author Bill Darbie
   */
  WrappedThreadSafeObservable(ThreadSafeObservable threadSafeObservable, String threadName)
  {
    Assert.expect(threadSafeObservable != null);
    Assert.expect(threadName != null);

    _threadSafeObservable = threadSafeObservable;
    _observerSet = new HashSet<Observer>();
    _args = Collections.synchronizedList(new LinkedList<Object>());
    _changed = 0;

    _phantomReference = new PhantomReference<ThreadSafeObservable>(_threadSafeObservable, new ReferenceQueue<ThreadSafeObservable>());

    // make this thread a daemon thread so it will not keep the application running by itself
    Thread thread = new Thread(this, threadName);
    thread.setDaemon(true);
    // now start the thread that handles calling the notifyObservers() method
    thread.start();
  }

  /**
   * This thread is always running ready to call notifyObserversInSeparateThread() whenever the
   * notifyObservers() method is called.  Running this in its own thread allows the notifyObservers()
   * call to return immediately rather than after all observers have had their update() method
   * called.  Note that this thread is automatically started by this classes constructor.
   *
   * @author Bill Darbie
   */
  public void run()
  {
    try
    {
      // stop this loop when there are no more references to WorkerThread
      // only after the thread stops running can this object be garbage collected and the
      // thread resource freed
      while (_phantomReference.isEnqueued() == false)
      {
        // while there are notifications to be made, call notifyObserversInSeparateThread()
        while (_args.isEmpty() == false)
          notifyObserversInSeparateThread();

        // since there are no notifications to be made, wait until another thread calls
        // notifyObservers (which will call notify to wake up this thread)
        synchronized (this)
        {
          try
          {
            // now wait until another notifyObserver is called
            // wait only 100ms then check agin on the _args size in case
            // a thread slipped in and added an element on the _args list
            // just before this wait
            wait(200);
          }
          catch(InterruptedException ie)
          {
            // do nothing
          }
        }
      }
    }
    catch(Throwable throwable)
    {
      Assert.logException(throwable);
    }
  }

  /**
   * Add a remote observer to the list of Observers that will get called
   * when a notifyObservers() call is made.
   *
   * @author Bill Darbie
   */
  // synchronize so _observerSet does not get accessed by more than one
  // thread at a time
  public void addObserver(Observer observer)
  {
    Assert.expect(observer != null);
    synchronized (_observerSetLock)
    {
      _observerSet.add(observer);
    }
  }

  /**
   * Delete an Observer from the list of Observers that will get called
   * when a notifyObservers() call is made
   *
   * @author Bill Darbie
   */
  // synchronize so _observerSet does not get accessed by more than one
  // thread at a time
  public void deleteObserver(Observer observer)
  {
    synchronized (_observerSetLock)
    {
      _observerSet.remove(observer);
    }
  }

  /**
   * Delete the entire list of Observers.
   *
   * @author Bill Darbie
   */
  // synchronize so _observerSet does not get accessed by more than one
  // thread at a time
  public synchronized void deleteObservers()
  {
    synchronized (_observerSetLock)
    {
      _observerSet.clear();
    }
  }

  /**
   * Iterate over the list of observers and call their update() method to
   * tell them that some data that they are observing has changed.  Note that
   * the update method that this calls MUST be synchronized and must spawn off a new
   * thread to do any calls back into the server.  Failure to do this will cause
   * a potential deadlock situation or unpredictable program behavior.  If your
   * update() method is going to call Swing methods it must use SwingUtils - invokeLater()
   *
   * @see javax.swing.SwingUtilities - invokeLater(Runnable)
   * @author Bill Darbie
   */
  public void notifyObservers()
  {
    notifyObservers(null);
  }

  /**
   * Iterate over the list of observers and call their update() method to
   * tell them that some data that they are observing has changed.  Note that
   * the update method that this calls MUST be synchronized and must spawn off a new
   * thread to do any calls back into the server.  Failure to do this will cause
   * a potential deadlock situation or unpredictable program behavior.  If your
   * update() method is going to call Swing methods it must use SwingUtils - invokeLater()
   *
   * @see javax.swing.SwingUtilities - invokeLater(Runnable)
   * @author Bill Darbie
   */
  public void notifyObservers(Object arg)
  {
    int changed = 0;
    synchronized (_changedLock)
    {
      changed = _changed;
    }

    if (changed > 0)
    {
      _args.add(arg);

      synchronized (this)
      {
        // wake up the ThreadSafeObservable thread that actually makes the update() calls
        // see the run method to find out where the wait() call is.
        notifyAll();
      }
    }
  }

  /**
   * This method does all the work of actually notifying the observers.  It is always called
   * by the ThreadSafeObservable thread rather than the thread that called notifyObservers().
   * This allows the notifyObservers() call to return immediately to maximize the speed of
   * the thread calling it.
   *
   * @author Bill Darbie
   */
  private void notifyObserversInSeparateThread()
  {
    Object localArg = null;
    // make sure arg does not get changed by another thread
    localArg = _args.remove(0);

    Set<Observer> observerSet = new HashSet<Observer>();
    synchronized (_observerSetLock)
    {
      // lock _observerSet while it is being copied
      // make a copy of the _observerSet
      observerSet.addAll(_observerSet);
    }

    // if a change has occurred - go through all observers and call update()
    // remember that if the update method will call GUI components it is
    // responsible for calling SwingUtilites - invokeLater() to get the
    // events on the Event thread queue
    for (Observer observer : observerSet)
    {
      observer.update(_threadSafeObservable, localArg);
    }

    clearChanged();
  }

  /**
   * Return a count of the number of Observers that will be notified if
   * notifyObservers is called.
   *
   * @author Bill Darbie
   */
  public int countObservers()
  {
    int count = 0;
    synchronized (_observerSetLock)
    {
      count = _observerSet.size();
    }
    return count;
  }

  /**
   * @return true if something in the Observable class has changed since the last notifyObservers()
   * call was made.
   *
   * @author Bill Darbie
   */
  public boolean hasChanged()
  {
    // increment changed rather then setting it to true or false.  This is required since m
    // multiple threads are being used.  This way if you get two hasChanged() calls then one
    // clearChanged() call, the hasChanged() method will still return true because one more
    // clearChanged() will still need to occur.
    int changed = 0;
    synchronized (_changedLock)
    {
      changed = _changed;
    }

    if (changed > 0)
      return true;

    return false;
  }

  /**
   * Notifies this class that something has changed in the Observable data.
   * If notifyObservers() is called and setChanged was never called, then
   * observers update() methods will NOT be called since no data was changed.
   *
   * @author Bill Darbie
   */
  protected void setChanged()
  {
    synchronized (_changedLock)
    {
      ++_changed;
    }
    Assert.expect(_changed >= 0);
  }

  /**
   * clears the flag that is set by setChanged()
   * @author Bill Darbie
   */
  protected void clearChanged()
  {
    synchronized (_changedLock)
    {
      --_changed;
    }
    Assert.expect(_changed >= 0);

    // clearChanged is called from within the notify method, after the observers have been notified.
    // we will call the outer Observable object so that any overrides it has made to this method are executed.
    // we do not call the outer Observable object for setChanged because it is invoked from outside of the object.
    _threadSafeObservable.clearChanged();
  }
}
