package com.axi.util;

import java.util.*;

import com.axi.util.*;
import java.lang.ref.*;

/**
* This class provides a way execute things on a worker thread.  It is handy when
* GUI code needs to get things off the Swing event queue.  It can also be used
* anytime you need to put tasks on another thread.  It works like the
* SwingUtilities.invokeLater() and invokeAndWait() calls except that it runs things
* on a non-swing event queue thread.  The order that you call invokeLater will be
* the order that things are run on this thread.  If you want things to run in
* parallel you can create more than one WorkerThread instance.
*
* At any time you can call the clearPendingTasks() method to remove all
* remaining tasks from the thread.
*
* @author Bill Darbie
*/
// This class has a PhantomReference to the WorkerThread class that is wrapping it.  That reference allows
// this class to know when there are no references to the WorkerThread anymore.
// When that happens this class will stop running its thread which frees up
// the thread resource and also allows the garbage collector to collect this class.
class WrappedWorkerThread extends Thread
{
  private List<Pair> _workerQueue = Collections.synchronizedList(new LinkedList<Pair>());
  private Object _workerQueueLock = new Object();
  private Object _newTaskLock = new Object();
  private Object _taskCompleteLock = new Object();
  private volatile boolean _taskComplete = false;
  private Exception _exception = null;
  private Thread _thisThread;

  // use a phantom reference to determine when WorkerThread is no longer being referenced
  // when that happens this classes run() method should exit so that it can be garbage collected
  // and the thread resource will be freed
  private PhantomReference<WorkerThread> _phantomReference;

  // nested class
  private class Pair
  {
    private Object _runnable;
    private boolean _waitForTaskToComplete;

    Pair(Object runnable, boolean waitForTaskToComplete)
    {
      Assert.expect(runnable != null);
      _runnable = runnable;
      _waitForTaskToComplete = waitForTaskToComplete;
    }

    Object getRunnable()
    {
      return _runnable;
    }

    boolean waitForTaskToComplete()
    {
      return _waitForTaskToComplete;
    }
  }

  /**
   * @author Bill Darbie
   */
  private WrappedWorkerThread()
  {
    // do nothing
  }

  /**
  * Create a new worker thread.
  *
  * @param threadName is the name of the thread.
  * @author Bill Darbie
  */
  WrappedWorkerThread(WorkerThread workerThread, String threadName)
  {
    Assert.expect(workerThread != null);
    Assert.expect(threadName != null);

    setName(threadName);
    // make sure this thread does not prevent an application from exiting
    // just because it is running.
    setDaemon(true);
    setPriority(Thread.NORM_PRIORITY);

    _phantomReference = new PhantomReference<WorkerThread>(workerThread, new ReferenceQueue<WorkerThread>());

    start();
  }

  /**
  * Pass in an object that implements the Runnable interface here and it will get
  * executed on this thread in the order it was received.  The code should look like
  * this
  * <pre>
  * WorkerThread workerThread = new WorkerThread("thread1");
  * workerThread.invokeLater(new Runnable()
  * {
  *   public void run()
  *   {
  *      // do whatever you want here - it will be run on the Worker thread
  *      // instead of the current thread that you were on
  *   }
  *  });
  *
  * </pre>
  *
  * @author Bill Darbie
  */
  synchronized void invokeLater(Runnable runnable)
  {
    Assert.expect(runnable != null);

    Pair pair = new Pair(runnable, false);
    _workerQueue.add(pair);

    synchronized(_newTaskLock)
    {
      _newTaskLock.notifyAll();
    }
  }

  /**
  * Pass in an object that implements the Runnable interface here and it will get
  * executed on this thread in the order it was received.  This call will not
  * return until the Runnable task is completed.  The code should look like
  * this
  * <pre>
  * WorkerThread workerThread = new WorkerThread("thread1");
  * workerThread.invokeAndWait(new Runnable()
  * {
  *   public void run()
  *   {
  *      // do whatever you want here - it will be run on the Worker thread
  *      // instead of the current thread that you were on
  *   }
  *  });
  *
  * </pre>
  *
  * @author Bill Darbie
  */
  void invokeAndWait(Runnable runnable)
  {
    Assert.expect(runnable != null);

    if (_thisThread == currentThread())
    {
      // if the thread calling this invokeAndWait method is already this workerThread
      // then we need to simply call the run method.
      // If we did not do this a dead lock would occur
      runnable.run();
    }
    else
    {
      synchronized(this)
      {
        Pair pair = new Pair(runnable, true);
        _workerQueue.add(pair);

        synchronized(_newTaskLock)
        {
          _newTaskLock.notifyAll();
        }

        synchronized(_taskCompleteLock)
        {
          while (_taskComplete == false)
          {
            try
            {
              _taskCompleteLock.wait(200);
            }
            catch(InterruptedException ex)
            {
              // do nothing
            }
          }
          _taskComplete = false;
        }
      }
    }
  }

  /**
  * Pass in an object that implements the RunnableWithExceptions interface here and
  * it will get executed on this thread in the order it was received.  This call will not
  * return until the Runnable task is completed.  Any exceptions thrown by the task
  * will be thrown from this call.  The code should look like
  * this
  * <pre>
  * try
  * {
  *   WorkerThread workerThread = new WorkerThread("thread1");
  *   workerThread.invokeAndWait(new Runnable()
  *   {
  *     public void run() throws Exception
  *     {
  *        // do whatever you want here - it will be run on the Worker thread
  *        // instead of the current thread that you were on
  *     }
  *    });
  * }
  * catch(Exception ex)
  * {
  *   // handle the exception here
  * }
  * </pre>
  *
  * @author Bill Darbie
  */
  void invokeAndWait(RunnableWithExceptions runnable) throws Exception
  {
    Assert.expect(runnable != null);

    if (_thisThread == currentThread())
    {
      // if the thread calling this invokeAndWait method is already this workerThread
      // then we need to simply call the run method.
      // If we did not do this a dead lock would occur
      runnable.run();
    }
    else
    {
      synchronized(this)
      {
        Pair pair = new Pair(runnable, true);
        _workerQueue.add(pair);

        synchronized(_newTaskLock)
        {
          _newTaskLock.notifyAll();
        }

        synchronized(_taskCompleteLock)
        {
          while (_taskComplete == false)
          {
            try
            {
              _taskCompleteLock.wait();
            }
            catch(InterruptedException ex)
            {
              // do nothing
            }
          }
          _taskComplete = false;
          if (_exception != null)
          {
            // do some work here to get the stack trace from the worker thread
            // combined with the stack trace from the calling thread
            Exception ex = _exception;
            StackTraceElement[] workerThreadStackTrace = ex.getStackTrace();
            ex.fillInStackTrace();
            StackTraceElement[] callingThreadStackTrace = ex.getStackTrace();

            int size = workerThreadStackTrace.length + callingThreadStackTrace.length;
            StackTraceElement[] stackTrace = new StackTraceElement[size];

            int i = 0;
            for(i = 0; i < workerThreadStackTrace.length; ++i)
              stackTrace[i] = workerThreadStackTrace[i];
            for(int j = i; j < size; ++j)
              stackTrace[j] = callingThreadStackTrace[j - i];

            ex.setStackTrace(stackTrace);

            _exception = null;
            throw ex;
          }
        }
      }
    }
  }

  /**
  * Do not call this method or the start() method.  This classes constructor will
  * start this up correctly.
  *
  * @author Bill Darbie
  */
  public void run()
  {
    _thisThread = currentThread();

    // stop this loop when there are no more references to WorkerThread
    // only after the thread stops running can this object be garbage collected and the
    // thread resource freed
    while (_phantomReference.isEnqueued() == false)
    {
      while(_workerQueue.size() > 0)
      {
        Pair pair = null;
        synchronized(_workerQueueLock)
        {
          if (_workerQueue.size() > 0)
            pair = (Pair)_workerQueue.remove(0);
        }
        if (pair != null)
        {
          Object object = pair.getRunnable();
          boolean waitForTaskToComplete = pair.waitForTaskToComplete();
          if (object instanceof Runnable)
          {
            Runnable runnable = (Runnable)object;
            try
            {
              runnable.run();
            }
            catch(Throwable throwable)
            {
              Assert.logException(throwable);
            }
          }
          else if (object instanceof RunnableWithExceptions)
          {
            RunnableWithExceptions runnable = (RunnableWithExceptions)object;
            try
            {
              runnable.run();
            }
            catch(Exception ex)
            {
              _exception = ex;
            }
            catch(Throwable throwable)
            {
              Assert.logException(throwable);
            }
          }
          else
            Assert.expect(false);

          if (waitForTaskToComplete)
          {
            synchronized(_taskCompleteLock)
            {
              _taskComplete = true;
              _taskCompleteLock.notifyAll();
            }
          }
        }
      }

      synchronized(_newTaskLock)
      {
        try
        {
          // now wait until another notifyAll is called
          _newTaskLock.wait(200);
        }
        catch(InterruptedException ie)
        {
          // do nothing
        }
      }
    }
  }

  /**
   * Clear out the existing list of tasks (if any exist) so they will never
   * be run.  The currently running job will still execute to completion.
   *
   * @author Bill Darbie
   */
  void clearPendingTasks()
  {
    synchronized(_workerQueueLock)
    {
      _workerQueue.clear();
    }
  }

  /**
   * @return the number of tasks waiting to be run.
   *
   * @author Peter Esbensen
   **/
  int getNumberOfPendingTasks()
  {
    int numPending = 0;
    synchronized(_workerQueueLock)
    {
      numPending = _workerQueue.size();
    }
    return numPending;
  }
}
