package com.axi.util;

import java.io.*;

/**
 * @author Bill Darbie
 */
public class Test_MemoryUtil extends UnitTest
{

  /**
   * @author Bill Darbie
   */
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_MemoryUtil());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    // use some memory
    int size = 100;
    int[] intArray = new int[size];
    for(int i = 0; i < size; ++i)
      intArray[i] = 10;

    // now null the reference so it can be garbage collected
    intArray = null;

    // now test if the garbageCollect() call will increase the amount of free memory
    Runtime runtime = Runtime.getRuntime();
    long freeMemBefore = runtime.freeMemory();
    MemoryUtil.garbageCollect();
    try
    {
      Thread.sleep(2000);
    }
    catch (InterruptedException ex)
    {
      // do nothing
    }
    long freeMemAfter = runtime.freeMemory();
    Expect.expect(freeMemBefore < freeMemAfter);
  }
}
