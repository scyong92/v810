package com.axi.util;

import java.io.*;

/**
 * @author Steve Anonson
 */
public class Test_LittleEndianCDataInputStream extends UnitTest
{
  /**
   * @author Steve Anonson
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_LittleEndianCDataInputStream());
  }

  /**
   * @author Steve Anonson
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      String fileName = is.readLine();

      if (fileName == null || fileName.length() == 0)
        System.out.println("ERROR: no filename was passed in");

      FileInputStream fis = new FileInputStream(fileName);
      CDataInputStream in = new LittleEndianCDataInputStream(fis);

      byte [] myBytes = null;
      String str = null;
//      in.skipBytes(13);
//      os.println("    Position: " + in.getPosition() + " - " + in.available());
      // readBoolean()
      /** @todo wpd - you should NOT use prints like this - use Assert.expect instead */
      os.println("readBoolean() " + in.readBoolean());
      // readBoolean()
      os.println("readBoolean() " + in.readBoolean());
      // readByte()
      os.println("readByte() " + in.readByte());
      // readChar()
      os.println("readChar() " + (byte)in.readChar());
      // readChar()
      os.println("readChar() " + (byte)in.readChar());
      // readDouble()
      os.println("readDouble() " + in.readDouble());
      // readFloat()
      os.println("readFloat() " + in.readFloat());
      // readFully(byte[])
      myBytes = new byte[7];
      in.readFully(myBytes);
      str = new String( myBytes );
      os.println("readFully(byte[]) " + str);
      // readFully(byte[], int, int)
      in.readFully(myBytes, 1, 5);
      str = new String( myBytes );
      os.println("readFully(byte[], int, int) " + str);
      // readInt()
      os.println("readInt() " + in.readInt());
      // readInt()
      os.println("readInt() " + in.readInt());
      // readLong()
      os.println("readLong() " + in.readLong());
      // readLong()
      os.println("readLong() " + in.readLong());
      // readByte()
      int lengthOfString = in.readByte();
      // readPascalString(int)
      str = in.readPascalString(lengthOfString + 1);
      os.println("readPascalString(int) " + str);
      // readShort()
      os.println("readShort() " + in.readShort());
      // readShort()
      os.println("readShort() " + in.readShort());
      // readDouble()
      os.println("readDouble() " + in.readDouble());
      // readFloat()
      os.println("readFloat() " + in.readFloat());
      // readUnsignedByte()
      os.println("readUnsignedByte() " + in.readUnsignedByte());
      // readUnsignedChar()
      os.println("readUnsignedChar() " + in.readUnsignedChar());
      // readUnsignedShort()
      os.println("readUnsignedShort() " + in.readUnsignedShort());
      // readString(int)
      str = in.readString(lengthOfString);
      os.println("readString(int) " + str);
      // readString(int, int)
      str = in.readString(lengthOfString, 15);
      os.println("readString(int, int) " + str);
      // readPascalPackedArray(int)
      str = in.readPascalPackedArray(6);
      os.println("readPascalPackedArray(int) and readCharArrayString(int) " + str);
      // readPascalPackedArray(int, int)
      str = in.readPascalPackedArray(12, 12);
      os.println("readPascalPackedArray(int, int) and readCharArrayString(int,int) " + str);
      // error conditions

      if (in != null)
        in.close();
    }
    catch (IOException e)
    {
      e.printStackTrace(os);
    }
  }

  /**
   * @author Steve Anonson
   */
  private void errorTest1( CDataInputStream in, PrintWriter os )
  {
    try
    {
      String str = in.readString(10);
    }
    catch (IOException e)
    {
      os.println("Got an exception reading data ");
      if (e.getMessage() != null)
        os.println(e.getMessage());
      e.printStackTrace();
    }
  }
}
