package com.axi.util;

import java.io.*;

public class Test_Assert extends UnitTest
{
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_Assert());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      Assert.setLogFile(Test_Assert.class.getClass().getName(), "1.00.00");

      Assert.expect(true, "this is true # 1");
      try
      {
        Assert.expect(false, "this is false # 1");
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }

      Assert.expect(true, "this is true # 2");
      try
      {
        Assert.expect(false, "this is false # 2");
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }

  }  
}
