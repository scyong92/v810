package com.axi.util;

import java.io.*;

/**
* FloatRef allows floats to be passed by reference.  A FloatRef
* can be passed to a method and have its value changed by the method that
* it was passed into.
* This class is needed because the java.lang.Float class does not provide
* a set method.
*
* @author Matt Wharton
*/
public class FloatRef implements Serializable
{
  private float _value;

  /**
   * @author Matt Wharton
   */
  public FloatRef()
  {
    _value = 0.0f;
  }

  /**
   * @author Matt Wharton
   */
  public FloatRef(float value)
  {
    _value = value;
  }

  /**
   * @author Matt Wharton
   */
  public FloatRef(FloatRef floatRef)
  {
    Assert.expect(floatRef != null);
    _value = floatRef._value;
  }

  /**
   * @author Matt Wharton
   */
  public void setValue(float value)
  {
    _value = value;
  }

  /**
   * @author Matt Wharton
   */
  public float getValue()
  {
    return _value;
  }

  /**
   * @author Matt Wharton
   */
  public boolean equals(Object rhs)
  {
    if (rhs == this)
      return true;

    if (rhs instanceof FloatRef)
    {
      FloatRef floatRef = (FloatRef)rhs;
      if (_value == floatRef._value)
        return true;
    }

    return false;
  }

  /**
   * @author Matt Wharton
   */
  public int hashCode()
  {
    Float flt = new Float(_value);

    return flt.hashCode();
  }

  /**
   * Returns true if the values are equal enough. This was ported from
   * util/src/Double.cpp where Bill Darbie had implemented this.
   * @author George A. David
   */
  public boolean fuzzyEquals(FloatRef rhs)
  {
    Assert.expect(rhs != null);
    return MathUtil.fuzzyEquals(_value, rhs.getValue());
  }
}
