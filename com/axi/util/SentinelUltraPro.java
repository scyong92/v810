/*****************************************************************************
* SentinelUltraPro.java:
* Java class which uses the JNI interface to access the UltraPro Client library.
*
* (C) Copyright 2005 SafeNet, Inc. All Rights Reserved.
*
****************************************************************************/
package com.axi.util;

public class SentinelUltraPro
{
  private final int DEVELOPER_ID = 0x9277;
  private final int DESIGNID = 0xCA57;

  private int _status = SP_ERR_SUCCESS;
  static final private int SP_APIPACKET_SZ = 4112; //APIPACKET size since it is in Byte
  private static byte[] _apiPacket = new byte[SP_APIPACKET_SZ];
  
  static private SentinelUltraPro _instance = null;
  static boolean _haventDecrement = true;

  static
  {
    try
    {
      System.loadLibrary("Uxjdk"); //load the JNI library
    }
    catch(UnsatisfiedLinkError e)
    {
        Assert.expect(false, e.getMessage());
    }
  }

  /**
   * @return SentinelUltraPro SentinelUltraPro instance
   * @author Chong, Wei Chin
   */
  public static SentinelUltraPro getInstance() throws DongleInitialiseException
  {
    if(_instance == null)
      _instance = new SentinelUltraPro();

    return _instance;
  }

  /**
   * @author Chong, Wei Chin
   */
  private SentinelUltraPro() throws DongleInitialiseException
  {
    init();
  }

  /**
   * @author Chong, Wei Chin
   */
  private void init() throws DongleInitialiseException
  {
    SFNTsntlCleanup();
    
    //Initialize the packet declared using the SFNTsntlInitialize API */
    _status = SFNTsntlInitialize(_apiPacket);
    if ((_status != SP_ERR_SUCCESS) && (_status != SP_ERR_PACKET_ALREADY_INITIALIZED))
    {
      //If the API packet is already initialized or already holds a valid license,
      //SFNTsntlInitialize will return an error.
      throw new DongleInitialiseException("Initialize Error \n Error Code: " + _status);
    }

    _status = SFNTsntlSetProtocol(_apiPacket, SP_TCP_PROTOCOL);
    if (_status != SP_ERR_SUCCESS)
    {
      // If a protocol other than the defined is requested or if NETAPI32.DLL is not available
      // when the requested protocol is NetBEUI, SFNTsntlSetProtocol will return an error.
      throw new DongleInitialiseException("Initialize TCP Protocol Error \n Error Code: " + _status);
    }

    _status = SFNTsntlSetContactServer(_apiPacket, SP_STANDALONE_MODE);
    if (_status != SP_ERR_SUCCESS)
    {
      //If the API packet already holds a valid license, SFNTsntlSetContactServer will return an error
      throw new DongleInitialiseException("Initialize Packet Error \n Error Code: " + _status);
    }

    // Acquire a license from the UltraPro key using the SFNTsntlGetLicense API function. It ensures
    // the license is acquired from a key containing the requested design ID by validating the design value
    //in the key. Refer to the design header file generated for this design. */
    _status = SFNTsntlGetLicense(_apiPacket, DEVELOPER_ID, DESIGNID, 0);
    if (_status != SP_ERR_SUCCESS)
    {
      // If a key with the requested developer ID and design ID is not found or a valid license is not
      // available, then SFNTsntlGetLicense will return an error.
      throw new DongleInitialiseException("License Invalid!\n Error Code: " + _status);
    }   
  }

  /**
   * @param toolkitCellAddress
   * @param dateInfo
   * @param daysLeft_lease
   * @throws DongleReadWriteException
   *
   * @author Wei Chin, Chong
   */
  public synchronized void readLease(long toolkitCellAddress, JDateInfo dateInfo, int [] daysLeft_lease) throws DongleReadWriteException
{
    Assert.expect(_apiPacket != null);
    
    /* Call the SFNTsntlReadLease API function to read the date up to which the lease is valid.
    This API will also calculate the number of days remaining, from the current date, for the lease to expire. */
    _status = SFNTsntlReadLease(_apiPacket,toolkitCellAddress, dateInfo, daysLeft_lease);
    if(_status != SP_ERR_SUCCESS)
    {
     /*
      If the Toolkit Cell Address value is invalid, an error will be returned.
     */
      throw new DongleReadWriteException("Error Code: " + _status);
    }
  }


  /**
   * @param toolkitCellAddress
   * @param readCounter_lease
   * @throws DongleReadWriteException
   *
   * @author Wei Chin, Chong
   */
  public void readValue(long toolkitCellAddress, long [] readCounter_lease) throws DongleReadWriteException
  {
    Assert.expect(_apiPacket != null);

    /* The SFNTsntlReadValue API function can read the boolean Value. It requires the
    Toolkit Cell Address corresponding to that element/value. Refer to the design header file
    generated for this design. */
    _status = SFNTsntlReadValue(_apiPacket, toolkitCellAddress, readCounter_lease);
    if(_status != SP_ERR_SUCCESS)
    {
      /*
       If the Toolkit Cell Address value is invalid, an error will be returned.
      */
      throw new DongleReadWriteException("Error Code: " + _status);
    }
  }

  /**
   * @param toolkitCellAddress
   * @param stringBuffer
   * @param maxStringLength
   * @throws DongleReadWriteException
   *
   * @author Wei Chin, Chong
   */
  public void readString(long toolkitCellAddress, StringBuffer stringBuffer,int maxStringLength) throws DongleReadWriteException
  {
    Assert.expect(_apiPacket != null);
    _status = SFNTsntlReadString(_apiPacket, toolkitCellAddress, stringBuffer, maxStringLength);
    if (_status != SP_ERR_SUCCESS)
    {
      throw new DongleReadWriteException("Error Code: " + _status);
    }
  }

  /**
   * @param toolkitCellAddress
   * @param writePwd
   * @param decrementAmounts
   * @throws DongleReadWriteException
   *
   * @author Wei Chin, Chong
   */
  public void decrementCounter(long toolkitCellAddress, int writePwd, int decrementAmounts) throws DongleReadWriteException
  {
    Assert.expect(_apiPacket != null);
    _status = SFNTsntlDecrementCounter(_apiPacket, toolkitCellAddress, writePwd, decrementAmounts);
    if (_status != SP_ERR_SUCCESS)
    {
      throw new DongleReadWriteException("Error Code: " + _status);
    }
  }

  /*
   * Below are the constants used in various UltraPro APIs.
   * For further details refer to the pdf file "UltraProJavaInterface.pdf".
   *
   */
  // Defines for Toolkit cell address
  static final public byte SP_INT8_TYPE = 1;
  static final public byte SP_INT32_TYPE = 2;
  static final public byte SP_STRING_TYPE = 3;
  static final public byte SP_BOOL_TYPE = 4;
  static final public byte SP_DATE_TYPE = 5;
  static final public byte SP_LEASE_TYPE = 6;
  static final public byte SP_ALGO_TYPE = 7;
  static final public byte SP_COUNTER_TYPE = 8;
  static final public byte SP_DATA_WORD_TYPE = 9;
  static final public byte SP_RESERVED_TYPE = 15;
  // Defines for SFNTsntlReadValue
  static final public long SP_SERIAL_NUMBER_INTEGER = ((SP_INT32_TYPE << 16) | (0 << 8) | 0);
  static final public long SP_DEVELOPER_ID_WORD = ((SP_DATA_WORD_TYPE << 16) | (0 << 8) | 1);
  static final public long SP_DESIGN_ID_WORD = ((SP_RESERVED_TYPE << 16) | 15);
  // Defines for SFNTsntlReadString
  static final public long SP_PART_NUMBER_STRING = ((SP_RESERVED_TYPE << 16) | 0x05);
  // The term "Part Number" has been renamed to "Model Number" from the release of UltraPro 1.1.
  static final public long SP_MODEL_NUMBER_STRING = SP_PART_NUMBER_STRING;
  // Sharing Flags
  static final public int SP_SHARE_USERNAME = 1;
  static final public int SP_SHARE_MAC_ADDRESS = 2;
  static final public int SP_SHARE_DEFAULT = 3;
  static final public int SP_DISABLE_USERLIMIT_SHARING = 0;
  static final public int SP_DISABLE_ALL_SHARING = 8;
  // Key Type Constants
  static final public int SP_KEY_FORM_FACTOR_PARALLEL = 0;
  static final public int SP_KEY_FORM_FACTOR_USB = 1;
  static final public int SP_SUPERPRO_FAMILY_KEY = 0;
  static final public int SP_ULTRAPRO_FAMILY_KEY = 1;
  // Protocol constants
  static final public int SP_TCP_PROTOCOL = 1;
  static final public int SP_IPX_PROTOCOL = 2;
  static final public int SP_NETBEUI_PROTOCOL = 4;
  static final public int SP_SAP_PROTOCOL = 8;
  // Heartbeat constants
  static final public long SP_MAX_HEARTBEAT = 2592000;
  static final public long SP_MIN_HEARTBEAT = 60;
  static final public long SP_INFINITE_HEARTBEAT = 0xFFFFFFFF; //==-1
  // Communication Modes
  static final public String SP_STANDALONE_MODE = "SP_STANDALONE_MODE";
  static final public String SP_DRIVER_MODE = "SP_DRIVER_MODE";
  static final public String SP_LOCAL_MODE = "SP_LOCAL_MODE";
  static final public String SP_BROADCAST_MODE = "SP_BROADCAST_MODE";
  static final public String SP_ALL_MODE = "SP_ALL_MODE";
  static final public String SP_SERVER_MODE = "SP_SERVER_MODE";
  static final public int SP_MAX_ADDR_LEN = 32;
  static final public int SP_MAX_NAME_LEN = 64;
  
  static final public int SP_MAX_STRING_LEN = 220;
  // Api Error Code
  static final public int SP_ERR_SUCCESS = 0;
  static final public int SP_ERR_INVALID_FUNCTION_CODE = 1;
  static final public int SP_ERR_INVALID_PACKET = 2;
  static final public int SP_ERR_UNIT_NOT_FOUND = 3;
  static final public int SP_ERR_ACCESS_DENIED = 4;
  static final public int SP_ERR_INVALID_MEMORY_ADDRESS = 5;
  static final public int SP_ERR_INVALID_ACCESS_CODE = 6;
  static final public int SP_ERR_PORT_IS_BUSY = 7;
  static final public int SP_ERR_WRITE_NOT_READY = 8;
  static final public int SP_ERR_NO_PORT_FOUND = 9;
  static final public int SP_ERR_ALREADY_ZERO = 10;
  static final public int SP_ERR_DRIVER_OPEN_ERROR = 11;
  static final public int SP_ERR_DRIVER_NOT_INSTALLED = 12;
  static final public int SP_ERR_IO_COMMUNICATIONS_ERROR = 13;
  static final public int SP_ERR_PACKET_TOO_SMALL = 15;
  static final public int SP_ERR_INVALID_PARAMETER = 16;
  static final public int SP_ERR_MEM_ACCESS_ERROR = 17;
  static final public int SP_ERR_VERSION_NOT_SUPPORTED = 18;
  static final public int SP_ERR_OS_NOT_SUPPORTED = 19;
  static final public int SP_ERR_QUERY_TOO_LONG = 20;
  static final public int SP_ERR_INVALID_COMMAND = 21;
  static final public int SP_ERR_MEM_ALIGNMENT_ERROR = 29;
  static final public int SP_ERR_DRIVER_IS_BUSY = 30;
  static final public int SP_ERR_PORT_ALLOCATION_FAILURE = 31;
  static final public int SP_ERR_PORT_RELEASE_FAILURE = 32;
  static final public int SP_ERR_ACQUIRE_PORT_TIMEOUT = 39;
  static final public int SP_ERR_SIGNAL_NOT_SUPPORTED = 42;
  static final public int SP_ERR_UNKNOWN_MACHINE = 44;
  static final public int SP_ERR_SYS_API_ERROR = 45;
  static final public int SP_ERR_UNIT_IS_BUSY = 46;
  static final public int SP_ERR_INVALID_PORT_TYPE = 47;
  static final public int SP_ERR_INVALID_MACH_TYPE = 48;
  static final public int SP_ERR_INVALID_IRQ_MASK = 49;
  static final public int SP_ERR_INVALID_CONT_METHOD = 50;
  static final public int SP_ERR_INVALID_PORT_FLAGS = 51;
  static final public int SP_ERR_INVALID_LOG_PORT_CFG = 52;
  static final public int SP_ERR_INVALID_OS_TYPE = 53;
  static final public int SP_ERR_INVALID_LOG_PORT_NUM = 54;
  static final public int SP_ERR_INVALID_ROUTER_FLGS = 56;
  static final public int SP_ERR_INIT_NOT_CALLED = 57;
  static final public int SP_ERR_DRVR_TYPE_NOT_SUPPORTED = 58;
  static final public int SP_ERR_FAIL_ON_DRIVER_COMM = 59;

  /* Networking Error Codes */
  static final public int SP_ERR_SERVER_PROBABLY_NOT_UP = 60;
  static final public int SP_ERR_UNKNOWN_HOST = 61;
  static final public int SP_ERR_SENDTO_FAILED = 62;
  static final public int SP_ERR_SOCKET_CREATION_FAILED = 63;
  static final public int SP_ERR_NORESOURCES = 64;
  static final public int SP_ERR_BROADCAST_NOT_SUPPORTED = 65;
  static final public int SP_ERR_BAD_SERVER_MESSAGE = 66;
  static final public int SP_ERR_NO_SERVER_RUNNING = 67;
  static final public int SP_ERR_NO_NETWORK = 68;
  static final public int SP_ERR_NO_SERVER_RESPONSE = 69;
  static final public int SP_ERR_NO_LICENSE_AVAILABLE = 70;
  static final public int SP_ERR_INVALID_LICENSE = 71;
  static final public int SP_ERR_INVALID_OPERATION = 72;
  static final public int SP_ERR_BUFFER_TOO_SMALL = 73;
  static final public int SP_ERR_INTERNAL_ERROR = 74;
  static final public int SP_ERR_PACKET_ALREADY_INITIALIZED = 75;
  static final public int SP_ERR_PROTOCOL_NOT_INSTALLED = 76;
  static final public int SP_ERR_NO_LEASE_FEATURE = 101;
  static final public int SP_ERR_LEASE_EXPIRED = 102;
  static final public int SP_ERR_COUNTER_LIMIT_REACHED = 103;
  static final public int SP_ERR_NO_DIGITAL_SIGNATURE = 104;
  static final public int SP_ERR_SYS_FILE_CORRUPTED = 105;
  static final public int SP_ERR_STRING_BUFFER_TOO_LONG = 106;

  /* Shell Error Codes */
  static final public int SH_BAD_ALGO = 128;
  static final public int SH_LONG_MSG = 129;
  static final public int SH_READ_ERROR = 130;
  static final public int SH_NOT_ENOUGH_MEMORY = 131;
  static final public int SH_CANNOT_OPEN = 132;
  static final public int SH_WRITE_ERROR = 133;
  static final public int SH_CANNOT_OVERWRITE = 134;
  static final public int SH_TOO_MANY_RELOCATION = 135;
  static final public int SH_BAD_RELOCATION = 136;
  static final public int SH_INVALID_HEADER = 137;
  static final public int SH_TMP_CREATE_ERROR = 138;
  static final public int SH_PRG_TOO_SMALL = 139;
  static final public int SH_PATH_NOT_THERE = 140;
  static final public int SH_OUT_OF_RANGE = 141;
  static final public int SH_NUMBER_EXPECTED = 142;
  static final public int SH_NOT_WINFILE = 143;
  static final public int SH_WIN3_OR_HIGHER = 144;
  static final public int SH_WIN_VERSION_WARNING = 145;
  static final public int SH_SELF_LOAD_FILE = 146;
  static final public int SH_WITHOUT_ENTRY = 147;
  static final public int SH_BAD_FILESPEC = 148;
  static final public int SH_SYNTAX = 149;
  static final public int SH_BAD_OPTION = 150;
  static final public int SH_TOO_MANY_FSPECS = 151;
  static final public int SH_TOO_MANY_SEEDS = 152;
  static final public int SH_NO_ENC_SEED = 153;
  static final public int SH_NO_HOOK_WARNING = 154;
  static final public int SH_UNSUPPORTED_EXE = 155;
  static final public int SH_TOOMANYFILES = 156;
  static final public int SH_BAD_FILE_INFO = 157;

  /* Win32 Specific Error Codes */
  static final public int SH_NOT_WIN32FILE = 158;
  static final public int SH_INVALID_MACHINE = 159;
  static final public int SH_INVALID_SECTION = 160;
  static final public int SH_INVALID_RELOC = 161;
  static final public int SH_NO_PESHELL = 162;

  /* Command-line Shell Error Codes */
  static final public int CLS_VERIFY_SIGN_ERROR = 163;
  static final public int CLS_PARAMETER_ERROR = 164;
  static final public int CLS_TOOL_ALREADY_RUNNING = 165;
  static final public int CLS_DESIGN_FILE_ERROR = 166;
  static final public int CLS_PARAMETER_MISSING = 167;
  static final public int CLS_PARAMETER_IDENTIFIER_MISSING = 168;
  static final public int CLS_KEY_NOT_FOUND = 170;
  static final public int CLS_PARAMETER_INVALID = 169;
  static final public int CLS_WRITE_PASSWORD_ERROR = 171;
  static final public int CLS_REGISTRY_ERROR = 172;
  static final public int CLS_LOAD_PROGRAM_ERROR = 173;
  static final public int CLS_MEMORY_ALLOCATE_ERROR = 174;
  static final public int CLS_MEMORY_ACCESS_ERROR = 175;
  static final public int CLS_BAD_IMPORT = 176;

    /* UltraPro APIs*/
  public native int SFNTsntlInitialize(byte[] apiPacket /*IN*/);

  public native int SFNTsntlActivateLicense(byte[] apiPacket /*IN*/,
          long toolkitCellAddress /*IN*/,
          int writePassword /*IN*/,
          int activatePassword1 /*IN*/,
          int activatePassword2 /*IN*/);

  public native int SFNTsntlGetFullStatus(byte[] apiPacket /*IN*/);

  public native int SFNTsntlGetVersion(byte[] apiPacket /*IN*/,
          byte[] majVer /*OUT*/,
          byte[] minVer /*OUT*/,
          byte[] rev /*OUT*/,
          byte[] osDrvrType /*OUT*/);

  public native int SFNTsntlSetContactServer(byte[] apiPacket /*IN*/,
          String serverName /*IN*/);

  public native int SFNTsntlGetContactServer(byte[] apiPacket /*IN*/,
          StringBuffer serverName /*OUT*/,
          int srvrNameLen /*IN*/);

  public native int SFNTsntlGetHardLimit(byte[] apiPacket /*IN*/,
          int[] hardLmt /*OUT*/);

  public native int SFNTsntlSetHeartBeat(byte[] apiPacket /*IN*/,
          int heartBeat /*IN*/);

  public native int SFNTsntlSetProtocol(byte[] apiPacket /*IN*/,
          int protocol /*IN*/);

  public native int SFNTsntlGetLicense(byte[] apiPacket /*IN*/,
          int devId /*IN*/,
          int designId /*IN*/,
          long toolkitCellAddress /*IN*/);

  public native int SFNTsntlQueryLicenseSimple(byte[] apiPacket /*IN*/,
          long toolkitCellAddress /*IN*/,
          byte[] queryData /*IN*/,
          byte[] response /*OUT*/,
          long[] response32 /*OUT*/,
          int length /*IN*/);

  public native int SFNTsntlQueryLicense(byte[] apiPacket /*IN*/,
          long toolkitCellAddress /*IN*/,
          int writePwd /*IN*/,
          byte[] queryData /*IN*/,
          byte[] response /*OUT*/,
          long[] response32 /*OUT*/,
          int length /*IN*/);

  public native int SFNTsntlQueryLicenseLease(byte[] apiPacket /*IN*/,
          long toolkitCellAddress /*IN*/,
          int writePwd /*IN*/,
          byte[] queryData /*IN*/,
          byte[] response /*OUT*/,
          long[] response32 /*OUT*/,
          int length /*IN*/);

  public native int SFNTsntlQueryLicenseDecrement(byte[] apiPacket /*IN*/,
          long toolkitCellAddress /*IN*/,
          int writePwd /*IN*/,
          int decAmount /*IN*/,
          byte[] queryData /*IN*/,
          byte[] response /*OUT*/,
          long[] response32 /*OUT*/,
          int length /*IN*/);

  public native int SFNTsntlDecrementCounter(byte[] apiPacket /*IN*/,
          long toolkitCellAddress /*IN*/,
          int writePwd /*IN*/,
          int decAmount /*IN*/);

  public native int SFNTsntlReadValue(byte[] apiPacket /*IN*/,
          long toolkitCellAddress /*IN*/,
          long[] value /*INOUT*/);

  public native int SFNTsntlWriteValue(byte[] apiPacket /*IN*/,
          long toolkitCellAddress /*IN*/,
          long value /*IN*/,
          byte flag /*IN*/,
          int writePwd /*IN*/);

  public native int SFNTsntlReadLease(byte[] apiPacket /*IN*/,
          long toolkitCellAddress /*IN*/,
          JDateInfo dateVal /*OUT*/,
          int[] daysLeft /*INOUT*/);

  public native int SFNTsntlReadString(byte[] apiPacket /*IN*/,
          long toolkitCellAddress /*IN*/,
          StringBuffer value /*OUT*/,
          int stringLength /*INOUT*/);

  public native int SFNTsntlWriteString(byte[] apiPacket /*IN*/,
          long toolkitCellAddress /*IN*/,
          String value /*IN*/,
          byte flag /*IN*/,
          int writePwd /*IN*/);

  public native int SFNTsntlLockData(byte[] apiPacket /*IN*/,
          long toolkitCellAddress /*IN*/,
          int writePwd /*IN*/);

  public native int SFNTsntlUnlockData(byte[] apiPacket /*IN*/,
          long toolkitCellAddress /*IN*/,
          int writePwd /*IN*/,
          int overwritePassword1 /*IN*/,
          int overwritePassword2 /*IN*/);

  public native int SFNTsntlGetKeyType(byte[] apiPacket /*IN*/,
          int[] keyFamily /*IN*/,
          int[] keyFormFactor /*OUT*/,
          int[] keyMemorySize /*INOUT*/);

  public native int SFNTsntlSetSharedLicense(byte[] apiPacket /*IN*/,
          int SharedMode /*OUT*/,
          char[] vendorStr /*IN*/);

  public native int SFNTsntlReleaseLicense(byte[] apiPacket /*IN*/,
          long toolkitCellAddress /*IN*/,
          int[] numUserLicense /*INOUT*/);

  public native void SFNTsntlCleanup();
}
