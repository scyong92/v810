package com.axi.util;


/**
 * The purpose of this class to abstract the various communication types that any hardware
 * might use such as TCPIP, USB, Firewire, etc.
 *
 * @author Greg Esparza
 */
public class CommunicationType
{
  private CommunicationTypeEnum _communicationType;
  private TcpipProperties _tcpipProperties;

  /**
   * @author Greg Esparza
   */
  public CommunicationType(TcpipProperties tcpipProperties)
  {
    Assert.expect(tcpipProperties != null);

    _communicationType = CommunicationTypeEnum.TCPIP;
    _tcpipProperties = tcpipProperties;
  }

  /**
   * @author Greg Esparza
   */
  public CommunicationTypeEnum getCommunicationType()
  {
    return _communicationType;
  }

  /**
   * @author Greg Esparza
   */
  public TcpipProperties getTcpipProperties()
  {
    Assert.expect(_communicationType.equals(CommunicationTypeEnum.TCPIP));
    return _tcpipProperties;
  }
}
