package com.axi.util;

import java.io.*;

/**
 * @author Bill Darbie
 */
public class CouldNotCreateDirectoryException extends IOException
{
  private String _dirName;

  /**
   * @author Bill Darbie
   */
  public CouldNotCreateDirectoryException(String dirName)
  {
    super("ERROR: Could not create directory " + dirName);
    Assert.expect(dirName != null);
    _dirName = dirName;
  }

  /**
   * @author Bill Darbie
   */
  public String getDirectoryName()
  {
    return _dirName;
  }
}
