#ifndef COM_AXI_UTIL_JNIUTIL_H
#define COM_AXI_UTIL_JNIUTIL_H

#include <jni.h>
#include <list>
#include <string>
#include <vector>

#ifdef NATIVE_AXI_UTIL_DLL
  #define DLL_DECL __declspec(dllexport)
#else
  #define DLL_DECL __declspec(dllimport)
#endif

/**
* Given a reference to a java doubleRef object, this method will call its
* setDouble method to set the doubleRefs value to whatever is passed in through
* doubleVal
*
* @param pEnv is a pointer to the JNI environment
* @param obj
* @param doubleRef is the reference to the Java DoubleRef object to be set by calling its setDouble() method
* @param doubleVal is the value to set doubleRef to
* @author Bill Darbie
*/
DLL_DECL void doubleRefSetValue(JNIEnv* pEnv,
                                jobject obj,
                                jobject doubleRef,
                                double  doubleVal);

/**
* Same as doubleRefSetValue except for Boolean.
* @author Bill Darbie
*/
DLL_DECL void booleanRefSetValue(JNIEnv* pEnv,
                                 jobject obj,
                                 jobject booleanRef,
                                 bool    booleanVal);

/**
* Same as doubleRefSetValue except for Boolean.
* @author Bill Darbie
*/
DLL_DECL void integerRefSetValue(JNIEnv* pEnv,
                                 jobject obj,
                                 jobject integerRef,
                                 int     integerVal);

/**
* Take a java string and return the equivalent STL string
* @author Bill Darbie
*/
DLL_DECL std::string getSTLString(JNIEnv* pEnv, jstring javaString);

/**
* Given a Java StringBuffer object, return an STL string from it.
* @author Bill Darbie
*/
DLL_DECL std::string getSTLStringFromJavaStringBuffer(JNIEnv* pEnv, jobject jStringBuffer);

/**
* Set a Java StringBuffer object to the contents of the passed in STL string.
* @author Bill Darbie
*/
DLL_DECL void setJavaStringBuffer(JNIEnv*      pEnv,
                                  jobject      jStringBuffer,
                                  std::string const& stlString);

/**
 * Create a java.util.ArrayList of String objects from and STL list of strings
 * @authro George A. David
 */
DLL_DECL jobject createJavaUtilArrayList(JNIEnv* pEnv, std::list<std::string> const& stlStringList);

/*
* Load a STL vector<int> from a jintArray
* @author Dave Ferguson
*/
DLL_DECL void loadSTLIntVectorFromJavaIntArray(JNIEnv* env, 
                                               jintArray arrayOfInts,
                                               std::vector<int>& vectorOfInts);

/*
* Load a STL vector<int> from a jdoubleArray
* @author Peter Esbensen
*/
DLL_DECL void loadSTLDoubleVectorFromJavaDoubleArray(JNIEnv* env, 
                                                     jdoubleArray arrayOfDoubles,
                                                     std::vector<double>& vectorOfDoubles);


//////////////////////////////////////////////////////////////////////////////
// Throw a C/C++ to Java JNI exception
//
// This function will allow a C/C++ JNI function to throw an exception back 
// up to the Java layer.
//
// Note: This function should rarely be used because virtually all Java 
//       exception handling will localize the message.  Only cases where,
//       for example, division-wide or company-wide Java utilities can be 
//       used, would this exception handler be necessary.  For example, 
//       com/axi/util/UsbUtil.java is a utility class that can be used
//       across company-wide software developers.  In this case, other software
//       architectures might not support localization so they would need the
//       "message" parameter for error messages, logging, etc.
//      
//
// @param pEnv is the environment pointer for active JNI call
// @param exceptionMessage is the message provided by the C/C++ client
// @param exceptionKey is the localization key that can be used by the Java
//                     layer to map to the localized message.
// @param exceptionParameters are the list of parameters that will be put
//        into the localized message if necessary.
// @param javaExceptionClassPackage defines which Java exception class to use.
//        For example, the user of this function would request the following
//        exception "com/axi/v810/hardware/PanelPositionerHardwareException"
// @author Greg Esparza
//////////////////////////////////////////////////////////////////////////////
DLL_DECL void throwJniException(JNIEnv* pEnv, 
                                std::string const& exceptionMessage, 
                                std::string const& exceptionKey, 
                                std::list<std::string> const& exceptionParameters, 
                                std::string javaExceptionClassPackage);


//////////////////////////////////////////////////////////////////////////////
// Throw a C/C++ to Java JNI exception
//
// This function will allow a C/C++ JNI function to throw an exception back 
// up to the Java layer.
//
// Note: This function is the preferred JNI exception handler for Java
//       architectures that support localization.  Only the localization
//       key and list of parameters are needed for the Java architecture
//       to generate a localized message.
//      
//
// @param pEnv is the environment pointer for active JNI call
// @param exceptionKey is the localization key that can be used by the Java
//                     layer to map to the localized message.
// @param exceptionParameters are the list of parameters that will be put
//        into the localized message if necessary.
// @param javaExceptionClassPackage defines which Java exception class to use.
//        For example, the user of this function would request the following
//        exception "com/axi/v810/hardware/PanelPositionerHardwareException"
// @author Greg Esparza
//////////////////////////////////////////////////////////////////////////////
DLL_DECL void throwJniException(JNIEnv* pEnv, 
                                std::string const& exceptionKey, 
                                std::list<std::string> const& exceptionParameters, 
                                std::string javaExceptionClassPackage);

/*
* Load an STL vector<jobject> from a jobjectArray
*
* @author Peter Esbensen
*/
DLL_DECL void loadSTLObjectVectorFromJavaObjectArray(JNIEnv* env, 
                                            jobjectArray arrayOfObjects,
                                            std::vector<jobject>& vectorOfJobjects);
  
/*
* Create a jintArray containing the integers in the given STL vector
*
* @author Peter Esbensen
*/
DLL_DECL jintArray createJavaIntArrayFromSTLVector(JNIEnv* env, 
                                                   std::vector< int > const& vectorOfInts);

/*
* Create a jfloatArray containing the integers in the given STL vector
*
* @author Peter Esbensen
*/
DLL_DECL jfloatArray createJavaFloatArrayFromSTLVector(JNIEnv* env, std::vector< float > const& vectorOfFloats);

/**
 * Checks to see if any exceptions have been thrown by java code.  This will simply re-throw the exception to your C++
 * code, so you better make sure that you have an appropriate catch block surrounding this.  One thing you may want to
 * do in your catch block is to simply clean up and then call env->Throw(javaException) to send the java exception back
 * to java.
 *
 * If you aren't calling this after making your c->java calls (or doing your own exception checking), you are headed for
 * trouble.  When an exception occurs in these situations (including Assertion failures), java will immediately return to your
 * C++ code without going to any java exception handlers.  It's up to you to handle the java exception properly.
 *
 * @author Peter Esbensen
 */
DLL_DECL void checkForJavaException(JNIEnv* env);


//////////////////////////////////////////////////////////////////////////////
// Load a Java String Array List from an STL list of strings
//
// Note: the Java String Array List must be allocated in Java, 
//       then passed down through the native interface where it will
//       be populated in this function.
//
// @author Greg Esparza
//////////////////////////////////////////////////////////////////////////////
DLL_DECL void loadJavaStringArrayFromStlStringList(JNIEnv* pEnv, jobject javaArrayListToLoadTo, std::list<std::string> const& stlListToLoadFrom);



#undef DLL_DECL

#endif
