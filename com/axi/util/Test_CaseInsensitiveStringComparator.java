package com.axi.util;

import java.io.*;

public class Test_CaseInsensitiveStringComparator extends UnitTest
{
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_CaseInsensitiveStringComparator());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      CaseInsensitiveStringComparator comp = new CaseInsensitiveStringComparator();

      try
      {
        comp.compare(null, "");
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }

      try
      {
        comp.compare("", null);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }

      Expect.expect(comp.compare("", "") == 0);
      Expect.expect(comp.compare("hello", "HELLO") == 0);
      Expect.expect(comp.compare("hello", "hello") == 0);
      Expect.expect(comp.compare("a", "b") < 0);
      Expect.expect(comp.compare("b", "a") > 0);
      Expect.expect(comp.equals(this) == false);
      Expect.expect(comp.equals(comp) == true);
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }

  }
}
