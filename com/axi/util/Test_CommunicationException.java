package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * Tests the CommunicationException class.
 * @author George A. David
 */
public class Test_CommunicationException extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_CommunicationException());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String message = "message";
    String key = "key";
    List<String> parameters = new ArrayList<String>();
    try
    {
      new CommunicationException(null, key, parameters);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }

    try
    {
      new CommunicationException(message, null, parameters);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }

    try
    {
      new CommunicationException(message, key, null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }

    CommunicationException cex = new CommunicationException(message, key, parameters);
    Expect.expect(key.equals(cex.getKey()));
    Expect.expect(message.equals(cex.getMessage()));
    Expect.expect(parameters == cex.getParameters());
  }
}
