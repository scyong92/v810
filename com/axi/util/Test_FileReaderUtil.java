package com.axi.util;

import java.io.*;


/**
 * @author George A. David
 */
public class Test_FileReaderUtil extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] arguments)
  {
    UnitTest.execute(new Test_FileReaderUtil());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      FileReaderUtil fileReaderUtil = new FileReaderUtil();
      try
      {
        fileReaderUtil.open(null);
        Expect.expect(false);
      }
      catch (AssertException exc)
      {
        //do nothing
      }

      try
      {
        fileReaderUtil.hasNextLine();
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      try
      {
        fileReaderUtil.readNextLine();
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      try
      {
        fileReaderUtil.getFilePath();
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      String badFilePath = getTestDataDir() + File.separator + "badFilePath";
      try
      {
        fileReaderUtil.open(badFilePath);
        Expect.expect(false);
      }
      catch (FileNotFoundException exc)
      {
        //do nothing
      }

      String existingFilePath = getTestDataDir() + File.separator + "Test_FileReaderUtil" + File.separator + "existingFile";

      fileReaderUtil.open(existingFilePath);
      Expect.expect(fileReaderUtil.getFilePath().equals(existingFilePath));
      Expect.expect(fileReaderUtil.hasNextLine());
      Expect.expect(fileReaderUtil.readNextLine().equals("this is an existing file"));
      Expect.expect(fileReaderUtil.hasNextLine() == false);
      try
      {
        fileReaderUtil.readNextLine();
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      fileReaderUtil.close();
    }
    catch(Exception exception)
    {
      exception.printStackTrace();
      Expect.expect(false);
    }
  }
}
