package com.axi.util;

import java.io.IOException;

/**
 *
 * @author bee-hoon.goh
 */
public class CameraConnectionStatusUnknownException extends IOException
{
  public CameraConnectionStatusUnknownException(String errorMessage)
  {
    super(errorMessage);
    Assert.expect(errorMessage != null);
  }
}
