package com.axi.util;

import java.io.*;

/**
 * This class is implemented to read or write binary data in little endian format
 * to or from a byte array as if the byte array were a file.  It assumes the data
 * is binary 32 bit C or Pascal data.
 *
 * @see CDataByteArrayIO
 * @author Michael Martinez-Schiferl
 */
public class LittleEndianCDataByteArrayIO extends CDataByteArrayIO
{
  /**
   * Create a CDataByteArrayIO class with a corresponding byte array for it to work on
   * @param byteArray the array that will be used to read and/or write binary
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public LittleEndianCDataByteArrayIO(byte[] byteArray)
  {
    super(byteArray);
  }

  /**
   * Reads a short from the current position of the byte array.
   *
   * @return the next 2 bytes from the byte array as a short
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public short readShort()
  {
    Assert.expect(_position + 2 <= _byteArray.length);

    byte byte0 = _byteArray[_position++];
    byte byte1 = _byteArray[_position++];

    int value = ((byte1 & 0xFF) << 8) + ((byte0 & 0xFF) << 0);

    return (short)value;
  }

  /**
   * Reads an unsigned short from the current position of the byte array.
   *
   * @return the next 2 bytes from the byte array as an unsigned short
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public int readUnsignedShort()
  {
    Assert.expect(_position + 2 <= _byteArray.length);

    byte byte0 = _byteArray[_position++];
    byte byte1 = _byteArray[_position++];

    int value = ((byte1 & 0xFF) << 8) + ((byte0 & 0xFF) << 0);

    return (0x0000FFFF & value);
  }

  /**
   * Reads an integer from the current position.
   *
   * @return the next 4 bytes from the byte array as an integer
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public int readInt()
  {
    Assert.expect(_position + 4 <= _byteArray.length);

    byte byte0 = _byteArray[_position++];
    byte byte1 = _byteArray[_position++];
    byte byte2 = _byteArray[_position++];
    byte byte3 = _byteArray[_position++];

    int anInt = ((byte3 & 0xFF) << 24) +
                ((byte2 & 0xFF) << 16) +
                ((byte1 & 0xFF) << 8) +
                ((byte0 & 0xFF) << 0);

    return anInt;
  }

  /**
   * Reads an unsigned integer from the current position of the byte array.
   *
   * @return the next 4 bytes from the byte array as an unsigned int
   * @author Bill Darbie
   */
  public long readUnsignedInt()
  {
    Assert.expect(_position + 4 <= _byteArray.length);

    byte byte0 = _byteArray[_position++];
    byte byte1 = _byteArray[_position++];
    byte byte2 = _byteArray[_position++];
    byte byte3 = _byteArray[_position++];

    int value = ((byte3 & 0xFF) << 24) +
                ((byte2 & 0xFF) << 16) +
                ((byte1 & 0xFF) << 8) +
                ((byte0 & 0xFF) << 0);

    return (0x00000000FFFFFFFF & value);
  }

  /**
   * Reads a double from the current position of the byte array.
   *
   * @return the next 8 bytes from the byte array as a double
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public double readDouble()
  {
    Assert.expect(_position + 8 <= _byteArray.length);

    int int0 = readInt();
    int int1 = readInt();
    long value = ((long)int1 << 32) + ((long)int0 & 0xFFFFFFFFL);

    return Double.longBitsToDouble(value);
  }

  /**
   * Writes a short (two bytes) to the byte array passed in through this classes contructor
   * The byte values to be written, in the order
   * shown are
   * <pre><code>
   * (byte)(0xff &amp; value)
   * (byte)(0xff &amp; (value &gt;&gt; 8))
   * </code> </pre> <p>
   *
   * @param aShort the <code>short</code> value to be written to the byte array passed in through this
   *               classes contructor
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writeShort(int aShort)
  {
    Assert.expect(_position + 2 <= _byteArray.length);

    byte byte0 = (byte)(0x000000FF & (aShort >> 0));
    byte byte1 = (byte)(0x000000FF & (aShort >> 8));

    _byteArray[_position++] = byte0;
    _byteArray[_position++] = byte1;
  }

  /**
   * Writes an integer value (4 bytes) to the byte array passed in through this classes contructor.
   * The byte values to be written, in the  order
   * shown, are
   * <p><pre><code>
   * (byte)(0xff &amp; (value &gt;&gt; 24))
   * (byte)(0xff &amp; (value &gt;&gt; 16))
   * (byte)(0xff &amp; (value &gt;&gt; &#32; &#32;8))
   * (byte)(0xff &amp; value)
   * </code></pre><p>
   *
   * @param anInt the <code>int</code> value to be written.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writeInt(int anInt)
  {
    Assert.expect(_position + 4 <= _byteArray.length);

    byte byte0 = (byte)(0x000000FF & (anInt >> 0));
    byte byte1 = (byte)(0x000000FF & (anInt >> 8));
    byte byte2 = (byte)(0x000000FF & (anInt >> 16));
    byte byte3 = (byte)(0x000000FF & (anInt >> 24));

    _byteArray[_position++] = byte0;
    _byteArray[_position++] = byte1;
    _byteArray[_position++] = byte2;
    _byteArray[_position++] = byte3;
  }

  /**
   * Writes a long value (4 bytes) to the byte array passed in to this classes contructor.
   * The byte values to be written, in the  order
   * shown, are:
   * <p><pre><code>
   * (byte)(0xff &amp; (v &gt;&gt; 24))
   * (byte)(0xff &amp; (v &gt;&gt; 16))
   * (byte)(0xff &amp; (v &gt;&gt;  8))
   * (byte)(0xff &amp; v)
   * </code></pre><p>
   * The upper four bytes of the Java 8 byte long are discarded.
   *
   * @param aLong the <code>long</code> value to be written to the byte array passed in to
   *              this classes contructor.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writeLong(long aLong)
  {
    Assert.expect(_position + 4 <= _byteArray.length);

    byte byte0 = (byte)(0x000000FF & (aLong >> 0));
    byte byte1 = (byte)(0x000000FF & (aLong >> 8));
    byte byte2 = (byte)(0x000000FF & (aLong >> 16));
    byte byte3 = (byte)(0x000000FF & (aLong >> 24));

    _byteArray[_position++] = byte0;
    _byteArray[_position++] = byte1;
    _byteArray[_position++] = byte2;
    _byteArray[_position++] = byte3;
  }

  /**
   * Writes a double value (8 bytes) to the byte array passed in to this classes contructor.
   * It does this as if it first converts this
   * <code>double</code> value to a <code>long</code>
   * in exactly the manner of the <code>Double.doubleToLongBits</code>
   * method  and then writes the <code>long</code>
   * value in exactly the manner of the  <code>writeLong</code>
   * method.
   *
   * @param aDouble the <code>double</code> value to be written to the byte array passed into
   *                this classes contructor
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writeDouble(double aDouble)
  {
    Assert.expect(_position + 8 <= _byteArray.length);

    long bits = Double.doubleToRawLongBits(aDouble);
    int int0 = ((int)(0x00000000FFFFFFFFL & bits));
    int int1 = ((int)(bits >>> 32)); // shift right with 0 extension

    writeInt(int0);
    writeInt(int1);
  }
}
