package com.axi.util;

/**
* This class provides additional functionality to get back class name.
*
* @author Matt Snook
*/
public class ClassNameUtil
{
  /**
  * Get the fully qualified class name of any object passed in.
  *
  * @param o object to find the class name of
  * @return fully qualified class name
  * @author Matt Snook
  */
  public static String getQualifiedName(Object o)
  {
    return o.getClass().getName();
  }

  /**
  * Get the (short) class name of any object passed in (without the package path).
  *
  * @param o object to find the short class name of
  * @return short class name
  * @author Matt Snook
  */
  public static String getName(Object o)
  {
    String fullClassName = getQualifiedName(o);

    int index = fullClassName.lastIndexOf(".") + 1;
    Assert.expect(index > 0);
    String className = fullClassName.substring(index);

    return className;
  }
}
