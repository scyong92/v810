package com.axi.util;

import java.util.*;

import Aladdin.*;
/**
 *
 * @author wei-chin.chong
 */
public class SentinelEMS
{
  private int _status = HaspStatus.HASP_STATUS_OK;
  
  private boolean _loginStatus = false;
  
  static private SentinelEMS _instance = null;
  
  private Hasp _hasp = null;
  private final long _FEATURE_V810 = 810;
  private final String _VENDOE_CODE = 
  "eq/PdeL/J3FFm04/zgxWgZJvjqAGFMFo29YdH0kHlwFQabDpRpVUGppo8IK48L+K5Oka7AIizlLli/v5" + 
  "DwCXhi4MKUoVoCF/Je/UuMjMY93nfqXdXUWsONR37Dd7zCF1HCX9688z4A4sIyyoOddeYYZu2UA0eGN4" + 
  "c6L0mGftcZ8r49pUisoTuRqX/OQGFF3tQhVxhOqTN+j+kjy/9a65hN9cwAuFjeztUJZmuziXaITjvy+c" + 
  "Y+gBDvj+Re16dh1/yBfgPc9dRq5uiSWQNnJKGlxSE231++wirdG4RYo4FfpJNGiHP39RyrUWjj/muiKd" + 
  "XJFlq5we3Iogkn7ETVhuQE8rZCSaCBOM3GwRCNJUCe4jYYiRy/eO/Dx4MyQWeCl3m5i+G9V5tcHCD+hn" + 
  "6YUl6LP3km+gdV5TmN7o3ru6pstsNadp7cUnyttmxpEO7B8QYB+ITOVFvQTkUeWGpAS46ZeZAM0H+C0f" + 
  "6IaagabAz/vJHKZqnmmTn0Q+uUHhsEs2HWFFotLFzu9H2j1/r3n3Y/fnfL779Hrocq2Fs05qRZvjMiOo" + 
  "111l4ZxSJNFD9B/ewodWbR9sTN2QFItHSjv7M8DcSnozkWVj/j3t7p0h+/b+cVCf+UxJTMexhidg0tTz" + 
  "psmD9k4k3Md+puWHUWmiO4VZtVZG9BPRmR+G9+PZGufyLYy6LKhbhnwAxmAodsl9WlTB63yy82zwMqNP" + 
  "JPUEpfihNRB7d1F1aczJ2pWAbhatUlGcbq7+2QPMo+HddS+/ufrArvoImtn297pvzGAUVSjTaiklQMoU" + 
  "VW0aYUiBn8WKVHlfXm3u1NjQSUiNFZUwg8ic9l1QbPqs3RxZKwXKYn7uKyqDRRrPKJpZBuYKftJb8rfp" + 
  "N7NG1LGZess4lbovfNlFY40Q18FrCL7NSjb3Gby4LwF0sms5bL5NMj/4i8WamxUqW4/hIYxiWOBmdSa2" + 
  "ES7ghZQGLFLLXog4g0gfPA==";
  
  private final String _SCOPE_1 = 
  "<haspscope>" + 
  "   <product id=\"8\"/>" + 
  "</haspscope>";
  
  public static final String _SCOPE_2 = new String(
    "<haspscope>\n"
    + " <license_manager hostname=\"localhost\" />\n"
    + "</haspscope>\n");

  public static final String _SCOPE_3 = new String(
    "<haspscope />\n");

  public static final String _VIEW = new String(
    "<haspformat root=\"my_custom_scope\">\n"
    + "  <hasp>\n"
    + "    <attribute name=\"id\" />\n"
    + "    <attribute name=\"type\" />\n"
    + "    <feature>\n"
    + "      <attribute name=\"id\" />\n"
    + "      <element name=\"concurrency\" />\n"
    + "      <element name=\"license\" />\n"
    + "      <session>\n"
    + "        <element name=\"username\" />\n"
    + "        <element name=\"hostname\" />\n"
    + "        <element name=\"ip\" />\n"
    + "        <element name=\"apiversion\" />\n"
    + "      </session>\n"
    + "    </feature>\n"
    + "  </hasp>\n"
    + "</haspformat>\n");

  private static HaspTime _datetime;
  
  /**
   * @return SentinelEMS instance
   * @author Chong, Wei Chin
   */
  public static SentinelEMS getInstance() throws DongleInitialiseException
  {
    if(_instance == null)
      _instance = new SentinelEMS();

    return _instance;
  }

  /**
   * @author Chong, Wei Chin
   */
  private SentinelEMS() throws DongleInitialiseException
  {
    init();
  }

  /**
   * @author Chong, Wei Chin
   */
  private void init() throws DongleInitialiseException
  {
    _hasp = new Hasp(_FEATURE_V810);
    
    HaspApiVersion version = _hasp.getVersion(_VENDOE_CODE);
    int status = version.getLastError();

    switch (status)
    {
      case HaspStatus.HASP_STATUS_OK:
        break;
      case HaspStatus.HASP_NO_API_DYLIB:
        throw new DongleInitialiseException("Initialize Error \n Error Code: " + _status + "\n Sentinel API dynamic library not found");
      case HaspStatus.HASP_INV_API_DYLIB:
        throw new DongleInitialiseException("Initialize Error \n Error Code: " + _status + "\n Sentinel API dynamic library is corrupt.");
      default:
        throw new DongleInitialiseException("Initialize Error \n Error Code: " + _status + "\n unexpected error");
    }

    login();
//    System.out.println("API Version: " + version.majorVersion() + "."
//      + version.minorVersion()
//      + "." + version.buildNumber() + "\n");   
  }

  /**
   * @throws DongleInitialiseException 
   * 
   * @author Wei Chin
   */
  public void login() throws DongleInitialiseException
  {
    _hasp.loginScope(_SCOPE_1, _VENDOE_CODE);

    int status = _hasp.getLastError();

    switch (status)
    {
      case HaspStatus.HASP_STATUS_OK:
        break;
      case HaspStatus.HASP_FEATURE_NOT_FOUND:
        throw new DongleInitialiseException("no Sentinel key found");
      case HaspStatus.HASP_HASP_NOT_FOUND:
        throw new DongleInitialiseException("Sentinel key not found");
      case HaspStatus.HASP_OLD_DRIVER:
        throw new DongleInitialiseException("outdated driver version or no driver installed");
      case HaspStatus.HASP_NO_DRIVER:
        throw new DongleInitialiseException("Sentinel key not found");
      case HaspStatus.HASP_INV_VCODE:
        throw new DongleInitialiseException("invalid vendor code");
      default:
        throw new DongleInitialiseException("login to default feature failed");
    }
    _loginStatus = true;
  }
  
  /**
   * @author Wei Chin, Chong
   */
  public String readValue(int offset, byte[] data) throws DongleReadWriteException
  {
    _hasp.read(Hasp.HASP_FILEID_RW, offset, data);
    int status = _hasp.getLastError();

    switch (status)
    {
      case HaspStatus.HASP_STATUS_OK:
        return HexadecimalUtil.convertByteArrayToString(data);
      case HaspStatus.HASP_INV_HND:
        throw new DongleReadWriteException("handle not active, Error Code: " + status);
      case HaspStatus.HASP_INV_FILEID:
        throw new DongleReadWriteException("invalid file id, Error Code: " + status);
      case HaspStatus.HASP_MEM_RANGE:
        throw new DongleReadWriteException("beyond memory range of attached Sentinel key, Error Code: " + status);
      case HaspStatus.HASP_HASP_NOT_FOUND:
        throw new DongleReadWriteException("hasp not found, Error Code: " + status);
      default:
        throw new DongleReadWriteException("read memory failed, Error Code: " + status);
    }
  } 

  /**
   * @author Wei Chin, Chong
   */
  public Boolean readBooleanValue(int offset, byte[] data) throws DongleReadWriteException, BadFormatException
  {
    try
    {
      if(hadLogin() == false)
        login();
    }
    catch(DongleInitialiseException ee)
    {
      // dothing 
    }
    
    _hasp.read(Hasp.HASP_FILEID_RW, offset, data);
    int status = _hasp.getLastError();

    switch (status)
    {
      case HaspStatus.HASP_STATUS_OK:
        return StringUtil.convertNumberStringToBoolean(HexadecimalUtil.convertByteArrayToString(data));
      case HaspStatus.HASP_INV_HND:
        throw new DongleReadWriteException("handle not active, Error Code: " + status);
      case HaspStatus.HASP_INV_FILEID:
        throw new DongleReadWriteException("invalid file id, Error Code: " + status);
      case HaspStatus.HASP_MEM_RANGE:
        throw new DongleReadWriteException("beyond memory range of attached Sentinel key, Error Code: " + status);
      case HaspStatus.HASP_HASP_NOT_FOUND:
        throw new DongleReadWriteException("hasp not found, Error Code: " + status);
      default:
        throw new DongleReadWriteException("read memory failed, Error Code: " + status);
    }
  }  
  
  /**
   * @return 
   * @author Wei Chin
   */
  public Date readCurrentTime()throws DongleReadWriteException
  {
    /**
     * ********************************************************************
     * hasp_get_rtc read current time from Sentinel Time key
     */
    _datetime = _hasp.getRealTimeClock();

    int status = _hasp.getLastError();
    Date date = new Date();

    switch (status)
    {
      case HaspStatus.HASP_STATUS_OK:
        date = new Date(_datetime.getYear(), _datetime.getMonth(), _datetime.getDay());
        return date;
      case HaspStatus.HASP_INV_TIME:
        throw new DongleReadWriteException("htime value outside supported range. Error Code: " + status);
      case HaspStatus.HASP_INV_HND:
        throw new DongleReadWriteException("handle not active. Error Code: " + status);
      case HaspStatus.HASP_NO_TIME:
        throw new DongleReadWriteException("no Sentinel Time connected. Error Code: " + status);
      default:
        throw new DongleReadWriteException("could not read time from Sentinel key. Error Code: " + status);
    }
  }
  
  /**
   * @author Wei Chin
   */
  public void logout()
  {
    _hasp.logout();
    int status = _hasp.getLastError();

    switch (status)
    {
      case HaspStatus.HASP_STATUS_OK:
//        do nothing
        break;
      case HaspStatus.HASP_INV_HND:
//        do nothing
        break;
      default:
//        do nothing
    }
    _loginStatus = false;    
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  public String getExpirationDate() throws DongleReadWriteException
  {
    String info;

    String format = 
    "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + 
    "<haspformat root=\"hasp_info\">" + 
    "    <feature>" + 
    "        <attribute name=\"id\" />" + 
    "        <element name=\"license\" />" + 
    "        <hasp>" + 
    "          <attribute name=\"id\" />" + 
    "          <attribute name=\"type\" />" + 
    "        </hasp>" + 
    "    </feature>" + 
    "</haspformat>" + 
    "";
    
    /**
     * Expected output:
     * <?xml version="1.0" encoding="UTF-8" ?>
        <hasp_info>
          <feature id="810">
            <license>
              <license_type>expiration</license_type>
              <exp_date>1454370900</exp_date>
            </license>
            <hasp id="1384230988" type="HASP-HL" />
          </feature>
        </hasp_info>
     */

    info = _hasp.getSessionInfo(format);

    int status = _hasp.getLastError();

    String expirationTag = "<exp_date>";
    switch (status)
    {
      case HaspStatus.HASP_STATUS_OK:
        int startIndexOfExpirationDate = info.indexOf(expirationTag) + expirationTag.length();
        int endIndexOfExpirationDate = info.indexOf("</exp_date>");        
        String expirationDate = info.substring(startIndexOfExpirationDate,endIndexOfExpirationDate);
        return expirationDate;
      case HaspStatus.HASP_INV_HND:
        throw new DongleReadWriteException("handle not active. Error Code: " + status);
      default:
        throw new DongleReadWriteException("unexpected Error. Error Code: " + status);
    }
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  public boolean hasExpirationDate() throws DongleReadWriteException
  {
    String info;

    String format = 
    "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + 
    "<haspformat root=\"hasp_info\">" + 
    "    <feature>" + 
    "        <attribute name=\"id\" />" + 
    "        <element name=\"license\" />" + 
    "        <hasp>" + 
    "          <attribute name=\"id\" />" + 
    "          <attribute name=\"type\" />" + 
    "        </hasp>" + 
    "    </feature>" + 
    "</haspformat>" + 
    "";

    info = _hasp.getSessionInfo(format);

    int status = _hasp.getLastError();

    switch (status)
    {
      case HaspStatus.HASP_STATUS_OK:
        return info.contains("expiration");
      case HaspStatus.HASP_INV_HND:
        throw new DongleReadWriteException("handle not active. Error Code: " + status);
      default:
        throw new DongleReadWriteException("unexpected Error. Error Code: " + status);
    }
  }

  /**
   * @return the _loginStatus
   * @author Wei Chin
   */
  public boolean hadLogin()
  {
    return _loginStatus;
  }

  /**
   * @param _loginStatus the _loginStatus to set
   * @author Wei Chin
   */
  public void setLoginStatus(boolean loginStatus)
  {
    _loginStatus = loginStatus;
  }
}
