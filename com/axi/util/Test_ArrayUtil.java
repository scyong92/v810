package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * Test class for ArrayUtil.

 * @author Matt Wharton
 */
public class Test_ArrayUtil extends UnitTest
{
  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  public Test_ArrayUtil()
  {
    // Do nothing...
  }

  /**
   * Main test entry point.
   *
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testArrayMinAndMax();
    testArrayMinIndexAndMaxIndex();
    testSubtractArrays();
    testArrayConversion();
    testCombineArrays();
    testInterpolatedMinAndMaxIndex();
    testResampleProfile();
    testSumArrays();
    testArrayCopy();
  }

  /**
   * @author Peter Esbensen
   */
  private void testSumArrays()
  {
    float[] floatArray = { 1f, 2f, 3f, 4f };
    float sum = ArrayUtil.sum(floatArray);
    Expect.expect( MathUtil.fuzzyEquals( sum, 10f ));

    float sliceSum = ArrayUtil.sum(floatArray, 0, 1);
    Expect.expect( MathUtil.fuzzyEquals( sliceSum, 1f ));

    sliceSum = ArrayUtil.sum(floatArray, 0, 2);
    Expect.expect( MathUtil.fuzzyEquals( sliceSum, 3f ));
  }

  /**
   * @author Peter Esbensen
   */
  private void testArrayCopy()
  {
    float[] floatArray = { 1f, 2f, 3f, 4f };
    float[] newArray = ArrayUtil.copy(floatArray, 0, 1);
    Expect.expect(Arrays.equals(newArray, ArrayUtil.makeArray(1f)));

    newArray = ArrayUtil.copy(floatArray, 1, 4);
    Expect.expect(Arrays.equals(newArray, ArrayUtil.makeArray(2f, 3f, 4f)));
  }

  /**
   * @author Patrick Lacz
   */
  private void testResampleProfile()
  {
    float[] profile5 = {0.f, 1.f, 2.f, 3.f, 4.f};
    float[] resultProfile = ArrayUtil.resampleProfile(profile5, 1);
    Expect.expect(resultProfile.length == 1);
    Expect.expect(resultProfile[0] == StatisticsUtil.mean(profile5) );

    resultProfile = ArrayUtil.resampleProfile(profile5, 5);
    Expect.expect(Arrays.equals(profile5, resultProfile));

    resultProfile = ArrayUtil.resampleProfile(profile5, 3);
    Expect.expect(Arrays.equals(resultProfile, ArrayUtil.makeArray(0.5f, 2.5f, 4f)));

    resultProfile = ArrayUtil.resampleProfile(profile5, 10);
    Expect.expect(Arrays.equals(resultProfile, ArrayUtil.makeArray(0f, 0f, 1f, 1f, 2f, 2f, 3f, 3f, 4f, 4f)));

    ArrayUtil.resampleProfile(profile5, 105);
  }

  /**
   * @author Peter Esbensen
   */
  private void testInterpolatedMinAndMaxIndex()
  {
    float[] floatArray1 = { 5.0f, 4.0f, 3.0f, 4.5f };
    float minInterpolatedIndex = ArrayUtil.interpolatedMinIndex(floatArray1);
    Expect.expect( MathUtil.fuzzyEquals( minInterpolatedIndex, 1.9f ));
    float minInterpolatedIndexInRange = ArrayUtil.interpolatedMinIndex(floatArray1, 1, 4);
    Expect.expect( MathUtil.fuzzyEquals( minInterpolatedIndexInRange, 1.9f ));

    float[] floatArray2 = { 0.0f, 1.0f, 2.0f, 1.5f };
    float maxInterpolatedIndex = ArrayUtil.interpolatedMaxIndex(floatArray2);
    Expect.expect( MathUtil.fuzzyEquals( maxInterpolatedIndex, 2.1666666667f ));
    float maxInterpolatedIndexInRange = ArrayUtil.interpolatedMaxIndex(floatArray2, 1, 4);
    Expect.expect( MathUtil.fuzzyEquals( maxInterpolatedIndexInRange, 2.16666666667f ));

    float[] tinyArray = { 0.0f };
    minInterpolatedIndex = ArrayUtil.interpolatedMinIndex(tinyArray);
    Expect.expect( MathUtil.fuzzyEquals( minInterpolatedIndex, 0.0f ));
    minInterpolatedIndexInRange = ArrayUtil.interpolatedMinIndex(tinyArray, 0, 1);
    Expect.expect( MathUtil.fuzzyEquals( minInterpolatedIndexInRange, 0.0f ));
    maxInterpolatedIndex = ArrayUtil.interpolatedMaxIndex(tinyArray);
    Expect.expect( MathUtil.fuzzyEquals( maxInterpolatedIndex, 0.0f ));
    maxInterpolatedIndexInRange = ArrayUtil.interpolatedMaxIndex(tinyArray, 0, 1);
    Expect.expect( MathUtil.fuzzyEquals( maxInterpolatedIndexInRange, 0.0f ));
  }

  /**
   * @author Peter Esbensen
   */
  private void testArrayMinIndexAndMaxIndex()
  {
    // Test the byte[] min/max methods.
    byte[] byteArray = new byte[50];
    for (byte i = 0; i < 50; ++i)
      byteArray[i] = i;
    int minByteIndex = ArrayUtil.minIndex(byteArray);
    int minByteIndexInRange = ArrayUtil.minIndex(byteArray, 10, 20);
    Expect.expect(minByteIndex == 0);
    Expect.expect(minByteIndexInRange == 10);
    int maxByteIndex = ArrayUtil.maxIndex(byteArray);
    int maxByteIndexInRange = ArrayUtil.maxIndex(byteArray, 10, 20);
    Expect.expect(maxByteIndex == 49);
    Expect.expect(maxByteIndexInRange == 19);

    // Test the short[] min/max methods.
    short[] shortArray = new short[50];
    for (short i = 0; i < 50; ++i)
      shortArray[i] = i;
    int minShortIndex = ArrayUtil.minIndex(shortArray);
    int minShortIndexInRange = ArrayUtil.minIndex(shortArray, 10, 20);
    Expect.expect(minShortIndex == 0);
    Expect.expect(minShortIndexInRange == 10);
    int maxShortIndex = ArrayUtil.maxIndex(shortArray);
    int maxShortIndexInRange = ArrayUtil.maxIndex(shortArray, 10, 20);
    Expect.expect(maxShortIndex == 49);
    Expect.expect(maxShortIndexInRange == 19);


    // Test the int[] min/max methods.
    int[] intArray = new int[50];
    for (int i = 0; i < 50; ++i)
      intArray[i] = i;
    int minIntIndex = ArrayUtil.minIndex(intArray);
    int minIntIndexInRange = ArrayUtil.minIndex(intArray, 10, 20);
    Expect.expect(minIntIndex == 0);
    Expect.expect(minIntIndexInRange == 10);
    int maxIntIndex = ArrayUtil.maxIndex(intArray);
    int maxIntIndexInRange = ArrayUtil.maxIndex(intArray, 10, 20);
    Expect.expect(maxIntIndex == 49);
    Expect.expect(maxIntIndexInRange == 19);


    // Test the long[] min/max methods.
    long[] longArray = new long[50];
    for (long i = 0; i < 50; ++i)
      longArray[(int)i] = i;
    int minLongIndex = ArrayUtil.minIndex(longArray);
    int minLongIndexInRange = ArrayUtil.minIndex(longArray, 10, 20);
    Expect.expect(minLongIndex == 0);
    Expect.expect(minLongIndexInRange == 10);
    int maxLongIndex = ArrayUtil.maxIndex(longArray);
    int maxLongIndexInRange = ArrayUtil.maxIndex(longArray, 10, 20);
    Expect.expect(maxLongIndex == 49);
    Expect.expect(maxLongIndexInRange == 19);


    // Test the float[] min/max methods.
    float[] floatArray = new float[50];
    for (float i = 0; i < 50; ++i)
      floatArray[(int)i] = i;
    int minFloatIndex = ArrayUtil.minIndex(floatArray);
    int minFloatIndexInRange = ArrayUtil.minIndex(floatArray, 10, 20);
    Expect.expect(minFloatIndex == 0);
    Expect.expect(minFloatIndexInRange == 10);
    int maxFloatIndex = ArrayUtil.maxIndex(floatArray);
    int maxFloatIndexInRange = ArrayUtil.maxIndex(floatArray, 10, 20);
    Expect.expect(maxFloatIndex == 49);
    Expect.expect(maxFloatIndexInRange == 19);

    // Test the double[] min/max methods.
    double[] doubleArray = new double[50];
    for (double i = 0; i < 50; ++i)
      doubleArray[(int)i] = i;
    int minDoubleIndex = ArrayUtil.minIndex(doubleArray);
    int minDoubleIndexInRange = ArrayUtil.minIndex(doubleArray, 10, 20);
    Expect.expect(minDoubleIndex == 0);
    Expect.expect(minDoubleIndexInRange == 10);
    int maxDoubleIndex = ArrayUtil.maxIndex(doubleArray);
    int maxDoubleIndexInRange = ArrayUtil.maxIndex(doubleArray, 10, 20);
    Expect.expect(maxDoubleIndex == 49);
    Expect.expect(maxDoubleIndexInRange == 19);
  }


  /**
   * @author Matt Wharton
   */
  private void testArrayMinAndMax()
  {
    // Test the byte[] min/max methods.
    List<Byte> byteList = new ArrayList<Byte>();
    for (byte i = 1; i <= 50; ++i)
    {
      byteList.add(i);
    }
    Collections.shuffle(byteList);
    byte[] byteArray = new byte[byteList.size()];
    for (int i =0; i < byteArray.length; ++i)
    {
      byteArray[i] = byteList.get(i);
    }
    byte minByte = ArrayUtil.min(byteArray);
    Expect.expect(minByte == 1);
    byte maxByte = ArrayUtil.max(byteArray);
    Expect.expect(maxByte == 50);

    // Test the short[] min/max methods.
    List<Short> shortList = new ArrayList<Short>();
    for (short i = 1; i <= 50; ++i)
    {
      shortList.add(i);
    }
    Collections.shuffle(shortList);
    short[] shortArray = new short[shortList.size()];
    for (int i =0; i < shortArray.length; ++i)
    {
      shortArray[i] = shortList.get(i);
    }
    short minShort = ArrayUtil.min(shortArray);
    Expect.expect(minShort == 1);
    short maxShort = ArrayUtil.max(shortArray);
    Expect.expect(maxShort == 50);

    // Test the int[] min/max methods.
    List<Integer> intList = new ArrayList<Integer>();
    for (int i = 1; i <= 50; ++i)
    {
      intList.add(i);
    }
    Collections.shuffle(intList);
    int[] intArray = new int[intList.size()];
    for (int i =0; i < intArray.length; ++i)
    {
      intArray[i] = intList.get(i);
    }
    int minInt = ArrayUtil.min(intArray);
    Expect.expect(minInt == 1);
    int maxInt = ArrayUtil.max(intArray);
    Expect.expect(maxInt == 50);

    // Test the long[] min/max methods.
    List<Long> longList = new ArrayList<Long>();
    for (long i = 1; i <= 50; ++i)
    {
      longList.add(i);
    }
    Collections.shuffle(longList);
    long[] longArray = new long[longList.size()];
    for (int i =0; i < longArray.length; ++i)
    {
      longArray[i] = longList.get(i);
    }
    long minLong = ArrayUtil.min(longArray);
    Expect.expect(minLong == 1);
    long maxLong = ArrayUtil.max(longArray);
    Expect.expect(maxLong == 50);

    // Test the float[] min/max methods.
    List<Float> floatList = new ArrayList<Float>();
    for (float i = 1; i <= 50; ++i)
    {
      floatList.add(i);
    }
    Collections.shuffle(floatList);
    float[] floatArray = new float[floatList.size()];
    for (int i =0; i < floatArray.length; ++i)
    {
      floatArray[i] = floatList.get(i);
    }
    float minFloat = ArrayUtil.min(floatArray);
    Expect.expect(minFloat == 1);
    float maxFloat = ArrayUtil.max(floatArray);
    Expect.expect(maxFloat == 50);

    // Test the double[] min/max methods.
    List<Double> doubleList = new ArrayList<Double>();
    for (double i = 1; i <= 50; ++i)
    {
      doubleList.add(i);
    }
    Collections.shuffle(doubleList);
    double[] doubleArray = new double[doubleList.size()];
    for (int i =0; i < doubleArray.length; ++i)
    {
      doubleArray[i] = doubleList.get(i);
    }
    double minDouble = ArrayUtil.min(doubleArray);
    Expect.expect(minDouble == 1);
    double maxDouble = ArrayUtil.max(doubleArray);
    Expect.expect(maxDouble == 50);
  }

  /**
   * @author Matt Wharton
   */
  private void testSubtractArrays()
  {
    // Test that subtractArrays(int[], int[]) asserts with null params.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.subtractArrays((int[])null, new int[]{ 1 });
      }
    });
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.subtractArrays(new int[]{ 1 }, (int[])null);
      }
    });

    // Test that subtractArrays(int[], int[]) asserts with arrays of differing lengths.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.subtractArrays(new int[]{ 1, 2 }, new int[]{ 3 });
      }
    });

    // Test the int[] subtraction.
    final int[] firstIntArray = { 2, 4, 6 };
    final int[] secondIntArray = { 1, 2, 3 };
    final int[] differenceIntArray = ArrayUtil.subtractArrays(firstIntArray, secondIntArray);
    Expect.expect(Arrays.equals(differenceIntArray, new int[]{ 1, 2, 3 }));

    // Test that subtractArrays(long[], long[]) asserts with null params.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.subtractArrays((long[])null, new long[]{ 1 });
      }
    });
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.subtractArrays(new long[]{ 1 }, (long[])null);
      }
    });

    // Test that subtractArrays(long[], long[]) asserts with arrays of differing lengths.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.subtractArrays(new long[]{ 1, 2 }, new long[]{ 3 });
      }
    });

    // Test the long[] subtraction.
    final long[] firstLongArray = { 200, 400, 600 };
    final long[] secondLongArray = { 100, 200, 300 };
    final long[] differenceLongArray = ArrayUtil.subtractArrays(firstLongArray, secondLongArray);
    Expect.expect(Arrays.equals(differenceLongArray, new long[]{ 100, 200, 300 }));

    // Test that subtractArrays(float[], float[]) asserts with null params.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.subtractArrays((float[])null, new float[]{ 1f });
      }
    });
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.subtractArrays(new float[]{ 1f }, (float[])null);
      }
    });

    // Test that subtractArrays(float[], float[]) asserts with arrays of differing lengths.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.subtractArrays(new float[]{ 1f, 2f }, new float[]{ 3f });
      }
    });

    // Test the float[] subtraction.
    final float[] firstFloatArray = { 2.5f, 5.5f, 7.5f };
    final float[] secondFloatArray = { 1.5f, 3.5f, 4.f };
    final float[] differenceFloatArray = ArrayUtil.subtractArrays(firstFloatArray, secondFloatArray);
    Expect.expect(Arrays.equals(differenceFloatArray, new float[]{ 1f, 2f, 3.5f }));

    // Test that subtractArrays(double[], double[]) asserts with null params.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.subtractArrays((double[])null, new double[]{ 1d });
      }
    });
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.subtractArrays(new double[]{ 1d }, (double[])null);
      }
    });

    // Test that subtractArrays(double[], double[]) asserts with arrays of differing lengths.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.subtractArrays(new double[]{ 1d, 2d }, new double[]{ 3d });
      }
    });

    // Test the double[] subtraction.
    final double[] firstDoubleArray = { 2.5d, 5.5d, 7.5d };
    final double[] secondDoubleArray = { 1.5d, 3.5d, 4.d };
    final double[] differenceDoubleArray = ArrayUtil.subtractArrays(firstDoubleArray, secondDoubleArray);
    Expect.expect(Arrays.equals(differenceDoubleArray, new double[]{ 1d, 2d, 3.5d }));
  }

  /**
   * @author Matt Wharton
   */
  private void testArrayConversion()
  {
    // Test conversion from float[] to byte[] (big endian).
    float[] floatArray1 = { 1f, 2f, 3f, 4f, 5f };
    byte[] byteArray1 = ArrayUtil.convertFloatArrayToByteArray(floatArray1);
    byte[] expectedByteArray1 = { 63, -128, 0, 0, 64, 0, 0, 0, 64, 64, 0, 0, 64, -128, 0, 0, 64, -96, 0, 0 };
    Expect.expect(Arrays.equals(byteArray1, expectedByteArray1));

    // Test conversion from byte[] to float[] (big endian).
    byte[] byteArray2 = new byte[] { 64, -96, 0, 0, 64, -128, 0, 0, 64, 64, 0, 0, 64, 0, 0, 0, 63, -128, 0, 0 };
    float[] floatArray2 = ArrayUtil.convertByteArrayToFloatArray(byteArray2);
    float[] expectedFloatArray2 = { 5f, 4f, 3f, 2f, 1f };
    Expect.expect(Arrays.equals(floatArray2, expectedFloatArray2));
  }

  /**
   * @author Matt Wharton
   */
  private void testCombineArrays()
  {
    // Test asserts for byte[][] version.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.combineArrays((byte[][])null);
      }
    });
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.combineArrays(new byte[0][0]);
      }
    });

    // Test 'happy path' for byte[][] version.
    byte[] byteArray1 = new byte[] { 1, 2, 3 };
    byte[] byteArray2 = new byte[] { 4, 5, 6 };
    byte[][] byteArrays = new byte[][] { byteArray1, byteArray2 };
    byte[] combinedByteArray = ArrayUtil.combineArrays(byteArrays);
    byte[] expectedByteArray = new byte[] { 1, 2, 3, 4, 5, 6 };
    Expect.expect(Arrays.equals(combinedByteArray, expectedByteArray));

    // Test asserts for short[][] version.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.combineArrays((short[][])null);
      }
    });
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.combineArrays(new short[0][0]);
      }
    });

    // Test 'happy path' for short[][] version.
    short[] shortArray1 = new short[] { 1, 2, 3 };
    short[] shortArray2 = new short[] { 4, 5, 6 };
    short[][] shortArrays = new short[][] { shortArray1, shortArray2 };
    short[] combinedShortArray = ArrayUtil.combineArrays(shortArrays);
    short[] expectedShortArray = new short[] { 1, 2, 3, 4, 5, 6 };
    Expect.expect(Arrays.equals(combinedShortArray, expectedShortArray));

    // Test asserts for int[][] version.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.combineArrays((int[][])null);
      }
    });
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.combineArrays(new int[0][0]);
      }
    });

    // Test 'happy path' for int[][] version.
    int[] intArray1 = new int[] { 1, 2, 3 };
    int[] intArray2 = new int[] { 4, 5, 6 };
    int[][] intArrays = new int[][] { intArray1, intArray2 };
    int[] combinedIntArray = ArrayUtil.combineArrays(intArrays);
    int[] expectedIntArray = new int[] { 1, 2, 3, 4, 5, 6 };
    Expect.expect(Arrays.equals(combinedIntArray, expectedIntArray));

    // Test asserts for long[][] version.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.combineArrays((long[][])null);
      }
    });
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.combineArrays(new long[0][0]);
      }
    });

    // Test 'happy path' for long[][] version.
    long[] longArray1 = new long[] { 1, 2, 3 };
    long[] longArray2 = new long[] { 4, 5, 6 };
    long[][] longArrays = new long[][] { longArray1, longArray2 };
    long[] combinedLongArray = ArrayUtil.combineArrays(longArrays);
    long[] expectedLongArray = new long[] { 1, 2, 3, 4, 5, 6 };
    Expect.expect(Arrays.equals(combinedLongArray, expectedLongArray));

    // Test asserts for float[][] version.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.combineArrays((float[][])null);
      }
    });
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.combineArrays(new float[0][0]);
      }
    });

    // Test 'happy path' for float[][] version.
    float[] floatArray1 = new float[] { 1f, 2f, 3f };
    float[] floatArray2 = new float[] { 4f, 5f, 6f };
    float[][] floatArrays = new float[][] { floatArray1, floatArray2 };
    float[] combinedFloatArray = ArrayUtil.combineArrays(floatArrays);
    float[] expectedFloatArray = new float[] { 1f, 2f, 3f, 4f, 5f, 6f };
    Expect.expect(Arrays.equals(combinedFloatArray, expectedFloatArray));

    // Test asserts for double[][] version.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.combineArrays((double[][])null);
      }
    });
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        ArrayUtil.combineArrays(new double[0][0]);
      }
    });

    // Test 'happy path' for double[][] version.
    double[] doubleArray1 = new double[] { 1d, 2d, 3d };
    double[] doubleArray2 = new double[] { 4d, 5d, 6d };
    double[][] doubleArrays = new double[][] { doubleArray1, doubleArray2 };
    double[] combinedDoubleArray = ArrayUtil.combineArrays(doubleArrays);
    double[] expectedDoubleArray = new double[] { 1d, 2d, 3d, 4d, 5d, 6d };
    Expect.expect(Arrays.equals(combinedDoubleArray, expectedDoubleArray));
  }

  /**
   * Main method.
   *
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ArrayUtil());
  }
}
