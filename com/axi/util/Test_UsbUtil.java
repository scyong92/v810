package com.axi.util;

import java.io.*;
import java.util.*;

public class Test_UsbUtil extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_UsbUtil());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    System.loadLibrary("nativeAxiUtil");

    try
    {
      UsbUtil.getInstance(null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }


    UsbUtil usbUtil = UsbUtil.getInstance(UsbDriverTypeEnum.FTDI);
    Expect.expect(usbUtil == UsbUtil.getInstance(UsbDriverTypeEnum.FTDI));

    try
    {
      usbUtil.connect(null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    catch(CommunicationException cex)
    {
      Expect.expect(false);
      cex.printStackTrace();
    }


    String deviceDescription = "non-existant device";
    try
    {
      usbUtil.connect(deviceDescription);
      Expect.expect(false);
    }
    catch (CommunicationException cex)
    {
      Expect.expect(cex.getKey().equals("HW_USB_DEVICE_NOT_FOUND_KEY"),
                    "expected the key HW_USB_DEVICE_NOT_FOUND_KEY but got " + cex.getKey());
    }

    deviceDescription = "X-ray Source";
    try
    {
      usbUtil.connect(deviceDescription);
    }
    catch(CommunicationException cex)
    {
      System.out.println("CommunicationException");
      System.out.println("key: " + cex.getKey());
      System.out.println("message: " + cex.getMessage());
      List<String> parameters = cex.getParameters();
      Iterator<String> it = parameters.iterator();
      while(it.hasNext())
      {
        System.out.println("Parameter: " + it.next());
      }
    }

    try
    {
      usbUtil.disconnect(null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    catch(CommunicationException cex)
    {
      Expect.expect(false);
      cex.printStackTrace();
    }

    String message = "message";
    try
    {
      usbUtil.sendMessage(null, message);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    catch(CommunicationException cex)
    {
      Expect.expect(false);
      cex.printStackTrace();
    }

    try
    {
      usbUtil.sendMessage(deviceDescription, null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    catch(CommunicationException cex)
    {
      Expect.expect(false);
      cex.printStackTrace();
    }

    try
    {
      usbUtil.getReplies(null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    catch(CommunicationException cex)
    {
      Expect.expect(false);
      cex.printStackTrace();
    }

    try
    {
      usbUtil.sendMessageAndGetReplies(null, message);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    catch(CommunicationException cex)
    {
      Expect.expect(false);
      cex.printStackTrace();
    }

    try
    {
      usbUtil.sendMessageAndGetReplies(deviceDescription, null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    catch(CommunicationException cex)
    {
      Expect.expect(false);
      cex.printStackTrace();
    }

//    try
//    {
//      usbUtil.sendMessage(deviceDescription, "message");
//    }
//    catch (CommunicationException ex)
//    {
//      System.out.println("key: " + ex.getKey());
//      System.out.println("message: " + ex.getMessage());
//      List parameters = ex.getParameters();
//      Iterator it = parameters.iterator();
//      while(it.hasNext())
//      {
//        System.out.println("Parameter: " + it.next());
//      }
//      ex.printStackTrace();
//    }
//    try
//    {
//      usbUtil.getReplies(deviceDescription);
//    }
//    catch (CommunicationException ex)
//    {
//      System.out.println("key: " + ex.getKey());
//      System.out.println("message: " + ex.getMessage());
//      List parameters = ex.getParameters();
//      Iterator it = parameters.iterator();
//      while(it.hasNext())
//      {
//        System.out.println("Parameter: " + it.next());
//      }
//      ex.printStackTrace();
//    }
//    try
//    {
//      usbUtil.sendMessageAndGetReplies(deviceDescription, "message");
//    }
//    catch (CommunicationException ex)
//    {
//      System.out.println("key: " + ex.getKey());
//      System.out.println("message: " + ex.getMessage());
//      List parameters = ex.getParameters();
//      Iterator it = parameters.iterator();
//      while(it.hasNext())
//      {
//        System.out.println("Parameter: " + it.next());
//      }
//      ex.printStackTrace();
//    }
//    try
//    {
//      usbUtil.disconnect(deviceDescription);
//    }
//    catch (CommunicationException ex)
//    {
//      System.out.println("key: " + ex.getKey());
//      System.out.println("message: " + ex.getMessage());
//      List parameters = ex.getParameters();
//      Iterator it = parameters.iterator();
//      while(it.hasNext())
//      {
//        System.out.println("Parameter: " + it.next());
//      }
//      ex.printStackTrace();
//    }
//    usbUtil.finalize();
  }
}
