package com.axi.util;

import java.io.*;

/**
 * @author Bill Darbie
 */
public class CouldNotWriteFileException extends IOException
{
  private String _fileName;

  /**
   * @author Bill Darbie
   */
  public CouldNotWriteFileException(String fileName)
  {
    super("ERROR: Could not write file or directory " + fileName);
    Assert.expect(fileName != null);
    _fileName = fileName;
  }

  /**
   * @author Bill Darbie
   */
  public String getFileName()
  {
    return _fileName;
  }
}
