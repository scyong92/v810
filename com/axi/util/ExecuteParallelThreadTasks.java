package com.axi.util;

import java.util.*;
import java.util.concurrent.*;

/**
 * This class allows you to run many tasks in parallel.
 * The waitForAllTasksToComplete() method will return only
 * when all tasks are complete.  If one or more tasks
 * threw an Exception, all tasks will still complete, but
 * the waitForAllTasksToComplete() method will return the
 * first exception thrown.  You can query the results of
 * each task using the Future<> object that is returned
 * when the tasks are submitted.
 *
 * @see ThreadTask
 * @author Bill Darbie
 */
public class ExecuteParallelThreadTasks<T>
{
  private ExecutorService _executor;
  private List<Future<T>> _futures = new ArrayList<Future<T>>();
  private List<ThreadTask<T>> _threadTasks = new ArrayList<ThreadTask<T>>();
  private Throwable _firstExceptionThrown;
  private boolean _cancel = false;

  /**
   * @author Bill Darbie
   */
  public ExecuteParallelThreadTasks()
  {
    _executor = Executors.newCachedThreadPool();
  }

  /**
   * Submit a ThreadTask to be run immediately.
   * @author Bill Darbie
   */
  public synchronized Future<T> submitThreadTask(ThreadTask<T> threadTask)
  {
    Assert.expect(threadTask != null);

    // clear any abort that may have been set
    _cancel = false;

    threadTask.clearCancel();
    threadTask.setExecuteParallelThreadTask(this);
    Future<T> future = _executor.submit(threadTask);
    _futures.add(future);
    _threadTasks.add(threadTask);

    return future;
  }

  /**
   * Submit a List of ThreadTasks to be run immediately.
   * @author Bill Darbie
   */
  public synchronized List<Future<T>> submitThreadTasks(List<? extends ThreadTask<T>> threadTasks)
  {
    Assert.expect(threadTasks != null);

    // clear any abort that may have been set
    _cancel = false;

    List<Future<T>> futures = new ArrayList<Future<T>>();
    for (ThreadTask<T> threadTask : threadTasks)
    {
      threadTask.clearCancel();
      threadTask.setExecuteParallelThreadTask(this);
      Future<T> future = _executor.submit(threadTask);
      futures.add(future);
      _threadTasks.add(threadTask);
    }

    return futures;
  }

  /**
   * Wait for all ThreadTasks to finish executing.
   * If one or more tasks throw an exception, the
   * first exception to be thrown from a ThreadTask
   * will be thrown from this method after all tasks
   * have finished running.
   *
   * @author Bill Darbie
   */
  public synchronized void waitForTasksToComplete() throws Exception
  {
    // wait for all threads to finish
    for (Future<T> future : _futures)
    {
      try
      {
        if (_cancel == false)
          future.get();
      }
      catch (InterruptedException ie)
      {
        // do nothing
      }
      catch (ExecutionException ex)
      {
        // do nothing
      }
    }

    if (_firstExceptionThrown != null)
    {
      //Local reference to the throwable so we can null out the member reference
      //so we don't see the same exception the next time it runs
      Throwable throwable = _firstExceptionThrown;
      _firstExceptionThrown = null;

      if (throwable instanceof Error)
        throw (Error)throwable;
      else if (throwable instanceof Exception)
        throw (Exception)throwable;
      else
        Assert.logException(throwable);
    }

    // purge the list of futures when done
    _futures.clear();
    _threadTasks.clear();
  }

  /**
   * Cancel any currently running tasks by calling their cancel() method.
   *
   * @author Bill Darbie
   */
  public void cancel() throws Exception
  {
    _cancel = true;

    // cancel all tasks
    if (_cancel)
    {
      for (ThreadTask<T> threadTask : _threadTasks)
        threadTask.cancel();
    }
  }

  /**
   * @author Bill Darbie
   */
  public boolean isCancelled()
  {
    return _cancel;
  }

  /**
   * Capture the first exception thrown.
   * This method should only be used by ThreadTask.
   * @author Bill Darbie
   */
  void setException(Throwable ex)
  {
    Assert.expect(ex != null);
    if (_firstExceptionThrown == null)
      _firstExceptionThrown = ex;
  }
}
