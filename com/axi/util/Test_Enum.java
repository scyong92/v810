package com.axi.util;

import java.io.*;

/**
 * @author Bill Darbie
 */
public class Test_Enum extends UnitTest
{
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Enum());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    Expect.expect(EnumA.ONE.equals(EnumA.ONE));
    Expect.expect(EnumA.TWO.equals(EnumA.TWO));
    Expect.expect(EnumB.ONE.equals(EnumB.ONE));
    Expect.expect(EnumB.TWO.equals(EnumB.TWO));
    Expect.expect(EnumA.ONE.equals(EnumB.ONE) == false);
    Expect.expect(EnumA.TWO.equals(EnumB.TWO) == false);
    Integer anInt = new Integer(100);
    Expect.expect(EnumA.ONE.equals(anInt) == false);
    // a new value for EnumB should be OK
    new EnumB(10);
    boolean exception = false;
    try
    {
      // instantiating the same id for EnumB should cause an AssertException
      new EnumB(0);
    }
    catch(AssertException ae)
    {
      exception = true;
    }
    Assert.expect(exception);
  }

}

class EnumA extends Enum
{
  private static int _index = -1;
  public static EnumA ONE = new EnumA(++_index);
  public static EnumA TWO = new EnumA(++_index);

  private EnumA(int id)
  {
    super(id);
  }
}

class EnumB extends Enum
{
  private static int _index = -1;
  public static EnumB ONE = new EnumB(++_index);
  public static EnumB TWO = new EnumB(++_index);

  EnumB(int id)
  {
    super(id);
  }
}