package com.axi.util;

import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.channels.*;
import java.util.*;

/**
 * This is a generic socket client program.
 *
 * SocketClient will make a connection to any desired server via SocketServer
 * whose role is to set up and to maintain a network connection.
 *
 * SocketClient provides the funcitonalities for connecting to server, disconnecting from server,
 * and send/receive message to/from server.
 *
 * @author Eugene Kim-Leighton
 * @author Tony Turner (added nio support)
 * @author Reid Hayhow (adding read/write checks)
 */
public class SocketClient
{
  private static final String _DEFAULT_DELIMITERS = " \t\n\r\f";

  private final String _READ_FAILED_MESSAGE = "SocketClient read failed. The socket connection was unexpectedly closed";
  private final String _WRITE_FAILED_MESSAGE = "SocketClient write failed. The socket connection was unexpectedly closed";

  // The ip address and port are accessible through InetSocketAddress.
  // You can get ip address by _inetSocketAddress.getAddress().getHostAddress()
  private InetSocketAddress _inetSocketAddress;

  // Timeout related members
  // The input and output streams from the underlying socket are necessary
  // in order to get timeouts to work
  private InputStream _socketInputStream;

  // The default timeout is NO timeout, ie. 0
  private int _socketTimeoutInMilliSeconds = 0;

  private SocketChannel _socket;
  private ByteOrder _defaultByteOrder = ByteOrder.BIG_ENDIAN;

  // to emulate the inputStream/OutputStream's ability to buffer data, we'll use
  // the following buffer size information, which is the same as the stream's default
  // sizes.
  private final int _DEFAULT_BUFFER_SIZE = 8192;

  private static int _WAIT_FOREVER = 0;
  private static InetAddress _NO_NEED_TO_BIND_TO_SPECIFIC_INET_ADDRESS = null;
  private static int _USE_ANY_FREE_PORT = 0;

  /**
   * This class allows the client to make a connection to a server over a socket and send requests
   * to the server.
   *
   * @param serverIPAddress is the address of the server in one of the following forms:
   *                        mtddarbi.lvld.axi.com or X.X.X.X where X is 0 - 255
   * @param serverPortNumber is the port number on the server to connect to
   *
   * @author Eugene Kim-Leighton
   * @author Rex Shang
   */
  public SocketClient(String serverIPAddress, int serverPortNumber)
  {
    // check see if the IP address that client is attempting to connect to is valid
    this(new InetSocketAddress(serverIPAddress, serverPortNumber));
  }

  /**
   * @author George A. David
   */
  public SocketClient(InetSocketAddress inetSocketAddress)
  {
    Assert.expect(inetSocketAddress != null);

    _inetSocketAddress = inetSocketAddress;
  }

  /**
   * Returns the address to which the socket is connected.
   * @todo RMS We should be able to remove this.
   * @author Rex Shang
   */
  public InetAddress getInetAddress()
  {
    return _inetSocketAddress.getAddress();
  }

  /**
   * @todo RMS We should be able to remove this.
   * @author Rex Shang
   */
  public int getPort()
  {
    return _inetSocketAddress.getPort();
  }

  /**
   * @todo RMS We should be able to remove this.
   * @author Rex Shang
   */
  public InetAddress getLocalAddress()
  {
    if (isConnected() == false || _socket.socket() == null)
      return null;
    else
      return _socket.socket().getLocalAddress();
  }

  /**
   * This function is to initiate the connection to the server.
   * @throws exception if there is a failure in connecting to a server.
   * @author Rex Shang
   */
  public void connect() throws SocketConnectionFailedException
  {
    // Connect in blocking mode.  No need to bind to a specific InetAddress.
    connect(_NO_NEED_TO_BIND_TO_SPECIFIC_INET_ADDRESS, _WAIT_FOREVER);
  }

  /**
   * This function is to initiate the connection to the server.
   * @throws exception if there is a failure in connecting to a server.
   * @author Rex Shang
   */
  public void connect(int timeoutInMilliSeconds) throws SocketConnectionFailedException
  {
    // Connect in NON blocking mode.  No need to bind to a specific InetAddress.
    connect(_NO_NEED_TO_BIND_TO_SPECIFIC_INET_ADDRESS, timeoutInMilliSeconds);
  }

  /**
   * This function is to initiate the connection to the server bound to a specific local interface.
   * @throws exception if there is a failure in connecting to a server.
   * @author Rex Shang
   */
  public void connect(InetAddress localInetAddress) throws SocketConnectionFailedException
  {
    // Connect in blocking mode.
    connect(localInetAddress, _WAIT_FOREVER);
  }

  /**
   * This function is to initiate the connection to the server bound to a specific local interface.
   * @throws exception if there is a failure in connecting to a server.
   *
   * @author Eugene Kim-Leighton
   * @author Reid Hayhow
   * @author Rex Shang
   */
  public void connect(InetAddress localInetAddress, int timeoutInMilliSeconds) throws SocketConnectionFailedException
  {
    _socketTimeoutInMilliSeconds = timeoutInMilliSeconds;

    // create a socket to connect to the specified IP address at the specified port
    try
    {
      if(_socket != null)
        close();
      _socket = SocketChannel.open();
      Assert.expect(_socket != null, "Failed to open socketChannel: " +
                    _inetSocketAddress.getAddress() +
                    ": " +
                    _inetSocketAddress.getPort());

      if (localInetAddress != _NO_NEED_TO_BIND_TO_SPECIFIC_INET_ADDRESS)
      {
        InetSocketAddress localSocketAddress = new InetSocketAddress(localInetAddress, _USE_ANY_FREE_PORT);
        _socket.socket().bind(localSocketAddress);
      }

      _socket.socket().connect(_inetSocketAddress, _socketTimeoutInMilliSeconds);

      // Set the timeout for this socket and get the input streams
      // because the timeout only works on the streams
      _socket.socket().setSoTimeout(_socketTimeoutInMilliSeconds);
      _socketInputStream = _socket.socket().getInputStream();
    }
    catch(Exception e)
    {
      SocketConnectionFailedException ex = new SocketConnectionFailedException(_inetSocketAddress.getAddress().getHostAddress(),
                                                                               _inetSocketAddress.getPort(),
                                                                               e.getMessage());
      ex.initCause(e);
      throw ex;
    }
  }

  /**
   * The server needs to be told that the client is disconnecting, so it can close the connection on its end.
   * If your client and server program simply exchange messages with handshaking, use this this function.
   * If you are expecting to receive prompt or specific string from the server, then you should use
   * disconnectFromServer(String, String, Boolean, Boolean).
   *
   * @param disconnectMessage is the message for the server to disconnect the connection.
   * @throws IOException when there is an error sending out the message for disconnecting.
   *
   * @author Eugene Kim-Leighton
   */
  public void disconnectFromServer(String disconnectMessage) throws IOException
  {
    Assert.expect(disconnectMessage != null);
    sendRequestAndReceiveReplies(disconnectMessage, "\n");
    if(_socket != null)
      close();
  }

  /**
   * The server needs to be told that the client is disconnecting, so it can close the connection on its end.
   *
   * @param disconnectMessage is the message for the server to disconnect the connection.
   * @param endOfRepliesIndicator is a string that the client is expecting to receive at the end of each server's reply.
   *        This allows the client know when to stop receiving the response from the server.
   * @param matchEndOfRepliesIndicatorExactly is the flag indicating whether the client should ensure that "endOfReplyIndicator"
   *        is received with an exact match.  Otherwise, the client will only look for "endOfReplyIndicator" and ignore
   *        the rest of the server's reply.
   * @throws IOException when there is an error sending out the message for disconnecting.
   *
   * @author Eugene Kim-Leighton
   */
  public void disconnectFromServer(String disconnectMessage,
                                   String endOfRepliesIndicator,
                                   boolean matchEndOfRepliesIndicatorExactly,
                                   boolean hideEndOfRepliesIndicatorFromReplies) throws IOException
  {
    Assert.expect(disconnectMessage != null);
    Assert.expect(endOfRepliesIndicator != null);

    sendRequestAndReceiveReplies(disconnectMessage,
                                 endOfRepliesIndicator,
                                 matchEndOfRepliesIndicatorExactly,
                                 hideEndOfRepliesIndicatorFromReplies);
    if(_socket != null)
      close();
  }

  /**
   * This function sends the client's request to the server.  The server is
   * expected to send back a "\n" to acknowledge that the action is possible OR
   * an error message if it is not.
   * <p>
   * If all is not well (i.e. an error has occured on the server), a
   * SocketMessageFailedException be throw containing the error message retrieved
   * from the server.  This indicates the server is still alive and can continue
   * the communication.
   * <p>
   * If all is not well (i.e. an IOException has occured on the socket), it will
   * be simply thrown out and the caller can decide its fate.  This typically
   * indicates the communication socket is suffering and cannot continue.
   * <p>
   * If all is well, the numberOfBytesToExpect will be pulled from the server and
   * placed into a ByteBuffer for return.
   *
   *
   * @param readAckCharAndReportErrors boolean passed in to maintain backward protocol compatiblity with old cameras.
   *
   * @author Reid Hayhow
   * @author Roy Williams
   */
  public ByteBuffer sendRequestAndReturnRawByteBuffer(String request,
                                                      int numberOfBytesToExpect,
                                                      boolean readAckCharAndReportErrors) throws IOException, SocketMessageFailedException
  {
    Assert.expect(request != null);
    Assert.expect(numberOfBytesToExpect >= 0);

    // request data
    sendRequest(request);

    if (readAckCharAndReportErrors)
    {
      ByteBuffer acknowledgeBuffer = receiveBuffer(1);
      byte acknowledgeChar = acknowledgeBuffer.get();
      if (acknowledgeChar != '\n')
      {
        // Bad news!   The camera is having problems.
        // We have read the first character from the stream and now we
        // must read the remainder and throw an exception with the new message
        // attached.
        StringBuffer buf = new StringBuffer(acknowledgeChar);
        List<String> replies = receiveReplies("\n");
        for (String replyLine : replies)
        {
          buf.append(replyLine);
        }
        throw new SocketMessageFailedException(buf.toString());
      }
    }
    // populate a ByteBuffer with the raw received data
    return receiveBuffer(numberOfBytesToExpect);
  }

  /**
   * @author George A. David
   */
  public ByteBuffer sendRequestAndReceiveReplies(ByteBuffer buffer, int sizeOfReplyInBytes) throws IOException
  {
    Assert.expect(buffer.order().equals(_defaultByteOrder));
    sendRequest(buffer);

    return receiveBuffer(sizeOfReplyInBytes);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public ByteBuffer sendRequestAndReceiveReplies(ByteBuffer buffer, int sizeOfReplyInBytes, boolean bypassIgnoreByteChecking, byte byteToIgnore) throws IOException
  {
    Assert.expect(buffer.order().equals(_defaultByteOrder));
    sendRequest(buffer);

    return receiveBuffer(sizeOfReplyInBytes, bypassIgnoreByteChecking, byteToIgnore);
  }

  /**
   * This function sends the client's request to the server and receives the response from the server.
   *
   * This function will be used in a normal situation of communication between client and server which a client sends a
   * request and receive replies from a server.  However, if the client doesn't have to receive any response from the
   * server when sending message to the server, then sendMessageToServer(String request) should be used.  Also, if the
   * client needs to receive the server's reply without sending request, then receiveReply or receiveReplies() should be
   * used.
   *
   * If you are expecting to receive prompt from the server, then use sendRequestAndReciveReplies(String, String, Boolean, Boolean).
   * However, if the client doesn't require to receive any response from the server when sending message to the server,
   * then sendMessageToServer(String request) should be used.
   * Also, if the client needs to receive the server's reply before sending request,
   * then receiveResponseFromServer(String endOfReplyIndicator, boolean matchEndOfRepliesIndicatorExactly) should be used.
   *
   * @param request is a client's request to the server. It cannot be a null.
   * @param endOfRepliesIndicator is a string that the client is expecting to receive at the end of the server's reply.
   *                            This allows the client to know when to stop receiving the response from the server.
   * @return List of server's responses per request
   * @throws IOException when sending a request to the server and receiving replies from the server
   *
   * @author Eugene Kim-Leighton
   */
  public List<String> sendRequestAndReceiveReplies(String request, String endOfRepliesIndicator) throws IOException
  {
    Assert.expect(request != null);
    Assert.expect(endOfRepliesIndicator != null);

    sendRequest(request);
    List<String> replies = new ArrayList<String>();
    replies = receiveReplies(endOfRepliesIndicator);
    return replies;
  }

  /**
   * This function sends the client's request to the server and receives the response from the server.
   *
   * This function will be used in a normal situation of communication between client and server which a client sends a
   * request and receive replies from a server.  However, if the client doesn't have to receive any response from the
   * server when sending message to the server, then sendMessageToServer(String request) should be used.  If the
   * client needs to receive the server's reply without sending request, then receiveReply or receiveReplies() should be
   * used.  Also, if you are expecting to receive a string of reply after sending a request, then use
   * sendRequestAndReceiveReply() instead of this function.
   *
   * @param request is a client's request to the server. It cannot be a null.
   * @param endOfReplyIndicator is a string that the client is expecting to receive at the end of the server's reply.
   *                            This allows the client to know when to stop receiving the response from the server.
   * @param matchEndOfRepliesIndicatorExactly is the flag indicating whether the client should ensure that
   *                                          "endOfReplyIndicator" is received with an exact match.  Otherwise, the
   *                                          client will only look for "endOfReplyIndicator" and ignore the rest of the
   *                                          server's reply.
   * @param hideEndOfRepliesIndicatorFromReplies is a flag indicating whether or not a client wnat to keep the
   *                                             "endOfReplyIndicator" as a server's reply.
   * @return a List of server's responses per request
   * @throws IOException when sending a request to the server and receiving replies from the server
   *
   * @author Eugene Kim-Leighton
   */
  public List<String> sendRequestAndReceiveReplies(String request,
                                                   String endOfReplyIndicator,
                                                   boolean matchEndOfRepliesIndicatorExactly,
                                                   boolean hideEndOfRepliesIndicatorFromReplies) throws IOException
  {
    Assert.expect(request != null);
    Assert.expect(endOfReplyIndicator != null && endOfReplyIndicator.equals("") == false);

    List<String> replies = new ArrayList<String>();
    sendRequest(request);
    replies = receiveReplies(endOfReplyIndicator,
                             matchEndOfRepliesIndicatorExactly,
                             hideEndOfRepliesIndicatorFromReplies);
    return replies;
  }

  /**
   * This function just sends a request to the server.
   *
   * If you are expecting to receive a (list of) server's reply, after sending a request to the server, use either
   * sendRequestAndReceiveReply() or sendRequestAndReceiveReplies() instead of using this function, and receiveReply()
   * or receiveReplies() directly.  If you just need to send a request to the server and don't have to receive anything
   * from the server, then use this function.
   *
   * @param request is a string that a client wants a server to get
   *
   * @author Eugenen Kim-Leighton
   * @author Reid Hayhow
   */
  public void sendRequest(String request) throws IOException
  {
    // send request
    // assert that message to send to server is not null and has some length
    Assert.expect(request != null);
    Assert.expect(request.length() > 0);

    // place request string in a ByteBuffer and send it.
    ByteBuffer byteBuffer = getByteBuffer(request.length());
    byteBuffer.put(request.getBytes());
    byteBuffer.flip();

    long bytesWritten = _socket.write(byteBuffer);

    // If the write didn't work, throw an exception. The socket was closed or some other problem was encountered
    if(_socket.isBlocking() && bytesWritten != request.length())
    {
      throw new IOException(_WRITE_FAILED_MESSAGE);
    }
  }

  /**
   * @author George A. David
   * @author Reid Hayhow
   */
  public void sendRequest(ByteBuffer byteBuffer) throws IOException
  {
    Assert.expect(byteBuffer != null);
    int bytesToSend = byteBuffer.remaining();
    long bytesWritten = _socket.write(byteBuffer);

    // If the write didn't work, throw an exception. The socket was closed or some other problem was encountered
    if(_socket.isBlocking() &&
       bytesWritten != bytesToSend &&
       byteBuffer.remaining() == 0)
    {
      throw new IOException(_WRITE_FAILED_MESSAGE);
    }
  }

  /**
   * Set the default byteOrder to be used.
   *
   * @author Roy Williams
   */
  public void setDefaultByteOrderForSendAndRecieveBuffers(ByteOrder byteOrder)
  {
    Assert.expect(byteOrder != null);
    _defaultByteOrder = byteOrder;
  }

  /**
   * Creates a new ByteBuffer of the specified size and sets the byteOrder to
   * whatever is specified as the default ByteOrder.   Unless it has been changed,
   * by setDefaultByteOrderForSendAndRecieveBuffers, that would be BIG_ENDIAN.
   *
   * @author Roy Williams
   */
  private ByteBuffer getByteBuffer(int size)
  {
    // set up the response buffer for receiving input
    ByteBuffer byteBuffer = ByteBuffer.allocate(size);
    byteBuffer.order(_defaultByteOrder);
    byteBuffer.clear();
    return byteBuffer;
  }

  /**
   * This function returns a byte array with content read directly from the camera.
   *
   * @author Roy Williams
   * @author Reid Hayhow
   */
  public ByteBuffer receiveBuffer(int bytesToReceive) throws IOException
  {
    Assert.expect(bytesToReceive > 0);

    // This receive buffer method assumes blocking mode, a new method will be needed
    // to handle non blocking receive
    Assert.expect(_socket.isBlocking());

    int totalBytesRead = 0;
    int bytesReadThisTime = 0;

    // Create a byte array and byte buffer with the same capacity
    byte byteArray[] = new byte[bytesToReceive];
    ByteBuffer rawByteBuffer = getByteBuffer(bytesToReceive);

    while(bytesReadThisTime >= 0 && (totalBytesRead < bytesToReceive))
    {
      // The read blocks (unless the the _socketTimeout is set) until something is returned.
      bytesReadThisTime = _socketInputStream.read(byteArray);

      // If the read returns -1, it didn't work, throw an exception.
      // The socket was closed or some other problem was encountered
      if(bytesReadThisTime == -1)
      {
        throw new IOException(_READ_FAILED_MESSAGE);
      }

      // Transfer the bytes we read this time from the byte array to the byte buffer
      // with no starting offset (they byteBuffer tracks this for us).
      rawByteBuffer.put(byteArray, 0, bytesReadThisTime);

      // Increment the total number of bytes read by the number of bytes read this time
      totalBytesRead += bytesReadThisTime;
    }

    Assert.expect(totalBytesRead == bytesToReceive);

    // flip the buffer so it is ready to use by the client!!!
    rawByteBuffer.flip();
    return rawByteBuffer;
  }
  
  /**
   * This function reads byte buffer from input socket while ignoring the input byte.
   * It will keep reading the input stream from SocketChannel until it gets all the bytes
   * specified by bytesToReceive parameter.
   * 
   * @author Cheah Lee Herng
   */
  public ByteBuffer receiveBuffer(int bytesToReceive, boolean bypassIgnoreByteChecking, byte byteToIgnore) throws IOException
  {
    Assert.expect(bytesToReceive > 0);

    // This receive buffer method assumes blocking mode, a new method will be needed
    // to handle non blocking receive
    Assert.expect(_socket.isBlocking());

    int totalBytesRead = 0;
    int bytesReadThisTime = 0;
    long totalElapsedTimeInMilis = 0;

    // Create a byte array and byte buffer with the same capacity
    byte byteArray[] = new byte[bytesToReceive];
    ByteBuffer rawByteBuffer = getByteBuffer(bytesToReceive);
    
    // XCR1687 Cheah Lee Herng 26 July 2013 - Surface Map Hang
    // Need a timer to keep track the elapsed time so that it will timeout and
    // and not hang forever
    TimerUtil timer = new TimerUtil();
    timer.start();
    
    while(totalBytesRead < bytesToReceive)
    {
      // The read blocks (unless the the _socketTimeout is set) until something is returned.
      bytesReadThisTime = _socketInputStream.read(byteArray);

      // If the read returns -1, it didn't work, throw an exception.
      // The socket was closed or some other problem was encountered
      if(bytesReadThisTime == -1)
      {
        throw new IOException(_READ_FAILED_MESSAGE);
      }
      
      // Before putting the read bytes into byte buffer, we analyze if it contains the ignore bytes
      ByteBuffer filtereByteBuffer = getByteBuffer(bytesToReceive);
      if (bytesReadThisTime > 0)
      {
        for(int counter=0; counter < byteArray.length; counter++)
        {         
          if ((byteArray[counter] != byteToIgnore) || bypassIgnoreByteChecking)
          {
            filtereByteBuffer.put(byteArray[counter]);
          }
          else
            bytesReadThisTime--;
        }
      }
      
      if (bytesReadThisTime > 0)
      {
        byte[] filteredByteArray = filtereByteBuffer.array();
      
        // Transfer the bytes we read this time from the byte array to the byte buffer
        // with no starting offset (they byteBuffer tracks this for us).
        rawByteBuffer.put(filteredByteArray, 0, bytesReadThisTime);

        // Increment the total number of bytes read by the number of bytes read this time
        totalBytesRead += bytesReadThisTime;
      }
      else
      {
        // We detect ignore bytes. If elapsed time exceeds defined timeout value,
        // throw IOException error instead.
        timer.stop();
        totalElapsedTimeInMilis += timer.getElapsedTimeInMillis();
        if (totalElapsedTimeInMilis > getTimeoutInMilliSeconds())
          throw new IOException(_READ_FAILED_MESSAGE);
        else
          timer.restart();
      }
      
      // Clear byte array
      for(int i=0; i < bytesToReceive; i++)
        byteArray[i] = 0;
    }
    
    Assert.expect(totalBytesRead == bytesToReceive);

    // flip the buffer so it is ready to use by the client!!!
    rawByteBuffer.flip();
    return rawByteBuffer;
  }

  /**

   * This function just receives a list of replies from the server.
   *
   * You want to use sendRequestAndReceiveReples(String request) for sending a request to and receiving a list of
   * replies from the server instead of using this function and sendRequest(Stirng request) direclty.  However, if you
   * need to recieve a reply from the server without sending any request, you would wnat to use this function.
   *
   * @param endOfReplyIndicator is a string that client is expecting to receive at the end of each server's reply.
   *                            This allows the client know when to stop receiving the respose from server.
   * @return a List of a server's responses.
   * @throws IOException if there is an error receiving reply from the server.
   *
   * @author Eugene Kim-Leighton
   * @author Reid Hayhow
   */
  public List<String> receiveReplies(String endOfReplyIndicator) throws IOException
  {
    Assert.expect(endOfReplyIndicator != null && endOfReplyIndicator.equals("") == false);

    // This receive buffer method assumes blocking mode, a new method will be needed
    // to handle non blocking receive
    Assert.expect(_socket.isBlocking());

    List<String> replies = new ArrayList<String>();
    final char LINEFEED = '\n';

    boolean handshakeReceived = false;
    StringBuffer msgReceived = new StringBuffer();
    int bytesReadThisTime = 0;

    // Create the byteBuffer and byte array to use for data transfer
    ByteBuffer responseBuffer = getByteBuffer(_DEFAULT_BUFFER_SIZE);
    byte[] byteArray = new byte[_DEFAULT_BUFFER_SIZE];

    // Read the next chunk of bytes if we have not received the handshake string
    while (handshakeReceived == false)
    {
      bytesReadThisTime = _socketInputStream.read(byteArray);

      // If the read returns -1, it didn't work, throw an exception.
      // The socket was closed or some other problem was encountered
      if(bytesReadThisTime == -1)
      {
        throw new IOException(_READ_FAILED_MESSAGE);
      }

      // Since the buffer may be reused, clear it before writing new data into it
      responseBuffer.clear();

      // Transfer the bytes we read this time from the byte array to the byte buffer
      // with no starting offset (they byteBuffer tracks this for us).
      responseBuffer.put(byteArray, 0, bytesReadThisTime);

      // Prepare the buffer to be read from instead of written to
      responseBuffer.flip();

      // Read all bytes received, so far.
      for (int i = 0; i < bytesReadThisTime; ++i)
      {
        char nextChar = (char) responseBuffer.get(i);

        // Check if this is the end of a message (indicated by LINEFEED)
        if (nextChar == LINEFEED)
        {
          // End of a message.  If this is the endOfReplyIndicator then bail
          // otherwise add the message to the List and wait for more.
          if (msgReceived.toString().equals(endOfReplyIndicator))
          {
            /**@todo rfh - consider peek instead of read so data is left in TCP buffer*/
            handshakeReceived = true;
            break;
          }
          else
          {
            // Add this message to the List of replies
            replies.add(msgReceived.toString());

            // Clear out the msgRecieved buffer for the next message
            msgReceived = new StringBuffer();
          }
        }
        else
        {
          // Not the end of a message, just add another char to the
          // buffer.
          msgReceived.append(nextChar);
        }
      }
      // If the message buffer we received is NOT empty and we got the EOM
      // indicator then something went wrong.
      Assert.expect(msgReceived.length() == 0);
    }

    return replies;
  }


  /**
   * It receives a (list of) reply from the server.
   *
   * @param endOfReplyIndicator is a string that client is expecting to receive
   *   at the end of each server's reply. This allows the client know when to
   *   stop receiving the respose from server.
   * @param matchEndOfRepliesIndicatorExactly is the flag indicating whether
   *   the client should ensure that "endOfReplyIndicator" is received with an
   *   exact match. Otherwise, the client will only look for
   *   "endOfReplyIndicator" and ignore the rest of the server's reply.
   * @param hideEndOfRepliesIndicatorFromReplies boolean
   * @return List of server's responses per request.
   * @throws IOException if there is an error receiving reponses from the
   *   server.
   * @author Eugene Kim-Leighton
   * @author Reid Hayhow
   */
  public List<String> receiveReplies(String endOfReplyIndicator,
                                     boolean matchEndOfRepliesIndicatorExactly,
                                     boolean hideEndOfRepliesIndicatorFromReplies) throws IOException
  {
    Assert.expect(endOfReplyIndicator != null && endOfReplyIndicator != "");
    Assert.expect(_socket.isBlocking());

    List<String> replies = new ArrayList<String>();

    int length = endOfReplyIndicator.length();
    int endCharacter= (int)endOfReplyIndicator.charAt(length - 1);
    String tempString = "";
    StringBuffer stringBuffer = new StringBuffer();

    int read = -1;
    boolean sawEndOfBufferIndicator = false;

    int stringIndex = 0;

    // Check if the current string buffer contains the endOfReplyIndicator
    // according to the boolean value of matchEndOfRepliesIndicatorExactly.

    // Adding an outer loop to the socket client. This is necessary because it
    // is possible that the endOfReplyIndicator will not be found in the first
    // read from the socket. For example, if the socket data is larger than the buffer
    // it will be nessary to allow for at least one more read from the socket.

    // allocate a standard sized byte buffer and byte array for receiving data
    ByteBuffer byteBuffer = getByteBuffer(_DEFAULT_BUFFER_SIZE);
    byte byteArray[] = new byte[_DEFAULT_BUFFER_SIZE];

    while (sawEndOfBufferIndicator == false)
    {
      // Read the data from the input stream
      int bytesRead = _socketInputStream.read(byteArray);

      // If the read returns -1, it didn't work, throw an exception.
      // The socket was closed or some other problem was encountered
      if (bytesRead == -1)
      {
        throw new IOException(_READ_FAILED_MESSAGE);
      }

      // The data from the last read has been added to the List<String>, so clear the buffer
      // before adding new data to it
      byteBuffer.clear();

      // Transfer the data over from the byte array to the byte buffer
      byteBuffer.put(byteArray, 0, bytesRead);

      //Prepare the byte buffer to be read from
      byteBuffer.flip();

      // In order to prevent a buffer underflow,  check to see that there is
      // information remaining in the _responseBuffer *before* reading from it
      while (byteBuffer.remaining() > 0)
      {
        read = byteBuffer.get();

        stringBuffer.append((char)read);
        stringIndex++;

        if(read == endCharacter)
        {
          tempString = stringBuffer.substring(0, stringIndex);
          if(matchEndOfRepliesIndicatorExactly == true)
          {
            if(tempString.equals(endOfReplyIndicator))
            {
              // Set the boolean to indicate that we have seen the
              // endOfReplyIndicator
              sawEndOfBufferIndicator = true;

              if(hideEndOfRepliesIndicatorFromReplies == false)
              {
                if(endCharacter == '\n')
                  addToList(stringBuffer, stringIndex, replies);
                else
                  replies.add(tempString);
              }
              break;
            }
          }
          else
          {
            if(tempString.endsWith(endOfReplyIndicator))
            {
              sawEndOfBufferIndicator = true;

              if(hideEndOfRepliesIndicatorFromReplies)
              {
                String reply = tempString.substring(0, tempString.length() - endOfReplyIndicator.length());
                if(reply.length() > 0)
                  replies.add(reply);
              }
              else
                replies.add(tempString);

              break;
            }
            else
            {
              StringTokenizer tokens = new StringTokenizer(tempString);

              while (tokens.hasMoreTokens())
              {
                String token = tokens.nextToken();

                if (token.equals(endOfReplyIndicator) || tempString.endsWith(endOfReplyIndicator))
                {
                  // Set the boolean to indicate that we have seen the
                  // endOfReplyIndicator
                  sawEndOfBufferIndicator = true;

                  if (hideEndOfRepliesIndicatorFromReplies == false)
                  {
                    if (endCharacter == '\n')
                    {
                      addToList(stringBuffer, stringIndex, replies);
                    }
                    else
                      replies.add(tempString);
                  }
                  break;
                }
              }
              break;
            }
          }
        }

        if(stringIndex > 0 && stringBuffer.charAt(stringIndex-1) == '\n')
        {
          addToList(stringBuffer, stringIndex, replies);
          stringBuffer.delete(0, stringIndex);
          stringIndex = 0;
        }
      }
    }
    return replies;
  }

  /**
   * @param stringBuffer
   * @param stringIndex
   * @param replies
   * @author Eugene Kim-Leighton
   */
  private void addToList(StringBuffer stringBuffer, int stringIndex, List<String> replies)
  {
    String tempString = "";
    if(stringIndex > 1 && stringBuffer.charAt(stringIndex-2) == '\r')
    {
      tempString =  stringBuffer.substring(0, stringIndex-2);
    }
    else
    {
      tempString = stringBuffer.substring(0, stringIndex-1);
    }
    if(tempString.length() > 0)
    {
      replies.add(tempString);
    }
  }

  /**
   * @author George A. David
   */
  public void close() throws IOException
  {
    if(_socket != null)
    {
      _socket.close();
      _socket = null;
    }
  }

  /**
   * @author Rex Shang
   */
  public boolean isConnected()
  {
    if (_socket == null)
      return false;
    else
      return _socket.isConnected();
  }

  /**
   * Get the timeout value for the underlying socket for this socket client
   *
   * @author Reid Hayhow
   */
  public int getTimeoutInMilliSeconds() throws SocketException
  {
    return _socketTimeoutInMilliSeconds;
  }
}
