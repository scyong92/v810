package com.axi.util.image;

import java.nio.*;
import java.util.*;

import com.axi.util.*;

/**
 * Statistics class
 *
 * This class holds functions to gather common statistics on a region of
 * interest in the image.
 *
 * @author Patrick Lacz
 */
public class Statistics
{
  /**
   * @author Patrick Lacz
   */
  public Statistics()
  {
    // do nothing, methods in Statistics are staticly defined.
  }

  /**
   * @author Patrick Lacz
   */
  static {
    System.loadLibrary("nativeImageUtil");
  }

  static private List<Pair<Image, Integer>> _cachedCircleKernels = Collections.synchronizedList(new LinkedList<Pair<Image, Integer>>());

  /**
   * Computes the mean value in an Image.
   * @author Patrick Lacz
   */
  public static float mean(Image image)
  {
    Assert.expect(image != null);

    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder( 0, 0, image.getWidth() - 1, image.getHeight() - 1,
         0, RegionShapeEnum.RECTANGULAR );
    return mean(image, roi);
  }

  /**
   * Computes the mean value within a region of interest.
   * The shape (rectangle vs. obround) determines the area that is actually averaged.
   * @author Patrick Lacz
   */
  public static float mean(Image image, RegionOfInterest regionOfInterest)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(image));
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    float mean = 0.0f;

    int minDimension = Math.min(regionOfInterest.getWidth(), regionOfInterest.getHeight());
    Assert.expect(minDimension > 0);

    if (regionOfInterest.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR) || minDimension <= 2)
      mean = rectangularMean(image, regionOfInterest);
    else
      // circular/obround
      //if (Math.abs(regionOfInterest.getWidth() - regionOfInterest.getHeight()) <= 1)
      if (regionOfInterest.getWidth() == regionOfInterest.getHeight())
      {
        mean = circularMean(image, regionOfInterest);
      } else {
        if (regionOfInterest.getWidth() > regionOfInterest.getHeight())
          mean = horizontalObroundMean(image, regionOfInterest);
        else
          mean = verticalObroundMean(image, regionOfInterest);
      }
    return mean;
  }

  /**
   * Based off the cached semi-circle kernels in ImageFeatureExtraction
   * @author Patrick Lacz
   */
  static private Pair<Image, Integer> getCachedCircleKernelIfItExists(int diameter)
  {
    Assert.expect(diameter >= 0);

    Assert.expect(_cachedCircleKernels != null);
    synchronized (_cachedCircleKernels)
    {
      for (Pair<Image, Integer> imageAndPixelCount : _cachedCircleKernels)
      {
        Image image = imageAndPixelCount.getFirst();
        if (Math.max(image.getWidth(), image.getHeight()) == diameter)
        {
          _cachedCircleKernels.remove(imageAndPixelCount);
          _cachedCircleKernels.add(0, imageAndPixelCount);
          return imageAndPixelCount;
        }
      }
    }
    return null; // return null if the image wasn't in the list
  }

  /**
   * Based off the cached semi-circle kernels in ImageFeatureExtraction
   * @author Patrick Lacz
   */
  static private void cacheCircleKernel(Image image, int pixelCount)
  {
    Assert.expect(image != null);

    synchronized (_cachedCircleKernels)
    {
      int listSize = _cachedCircleKernels.size();
      final int MAX_LIST_SIZE = 10;
      if (listSize < MAX_LIST_SIZE)
      {
        image.incrementReferenceCount();
        _cachedCircleKernels.add(new Pair<Image, Integer>(image, pixelCount)); // just append to list
      }
      else
      {
        Image oldImage = _cachedCircleKernels.get(MAX_LIST_SIZE - 1).getFirst();
        oldImage.decrementReferenceCount();
        image.incrementReferenceCount();
        _cachedCircleKernels.set(MAX_LIST_SIZE - 1, new Pair<Image, Integer>(image, pixelCount)); // replace the last image on the list
      }
    }
  }

  /**
   * @author Patrick Lacz
   */
  private static float circularMean(Image image, RegionOfInterest regionOfInterest)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    int width = regionOfInterest.getWidth();
    int height = regionOfInterest.getHeight();
    Assert.expect(Math.abs(width-height) <= 1);

    int diameter = Math.min(width,height);
    Pair<Image, Integer> cacheEntry = getCachedCircleKernelIfItExists(diameter);
    Image circleImage = null;
    int pixelCount = 0;
    if (cacheEntry == null)
    {
      circleImage = new Image(diameter, diameter);
      Paint.fillImage(circleImage, 0.f);
      pixelCount = Paint.fillCircle(circleImage, RegionOfInterest.createRegionFromImage(circleImage),
                                            new DoubleCoordinate((double)diameter / 2.0, (double)diameter / 2.0), (float)diameter / 2.0f, 1.0f);
      cacheCircleKernel(circleImage, pixelCount);
    }
    else
    {
      circleImage = cacheEntry.getFirst();
      pixelCount = cacheEntry.getSecond();
      circleImage.incrementReferenceCount();
    }
    Assert.expect(pixelCount > 0);
    Image kernelResultImage = Filter.convolveKernel(image,
                                                    regionOfInterest,
                                                    circleImage,
                                                    new ImageCoordinate(0, 0), // anchor
                                                    BorderSamplingModeEnum.CONSTRAIN_TO_REGION);
    float circleRegionSum = kernelResultImage.getPixelValue(0, 0);

    kernelResultImage.decrementReferenceCount();
    circleImage.decrementReferenceCount();

    return circleRegionSum/pixelCount;
  }

  /**
   * @author Patrick Lacz
   */
  private static float horizontalObroundMean(Image image, RegionOfInterest regionOfInterest)
  {
    /**
     * The regions are set up to look like:
     *    _ _______ _
     *   / |       | \
     *  ( A|   B   |C )
     *   \_|_______|_/
     *
     * A = leftCircleRoi, B = rectRegion, C = rightCircleRoi
     */
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);

    int width = regionOfInterest.getWidth();
    int height = regionOfInterest.getHeight();

    // we're setting the circle to not include the column that would include the full circle diameter. The rect region
    // will handle this region.
    int halfCircleWidth = (int)Math.ceil(height/2.0)-1;

    // there could be a problem if the width and height only vary by one
    Assert.expect(Math.abs(width-height) > 0);

    RegionOfInterest rectRegion = new RegionOfInterest(regionOfInterest);
    rectRegion.setWidthKeepingSameCenter(width - 2*halfCircleWidth); // minus two height/2 semicircles

    RegionOfInterest leftCircleRoi = new RegionOfInterest(regionOfInterest);
    leftCircleRoi.setWidthKeepingSameMinX(halfCircleWidth);


    RegionOfInterest rightCircleRoi = new RegionOfInterest(leftCircleRoi);
    rightCircleRoi.translateXY(halfCircleWidth+rectRegion.getWidth(), 0);


    // create a summing filter
    Image halfCircleImage = new Image(halfCircleWidth, height);
    Paint.fillImage(halfCircleImage, 0.f);
    int pixelsInHalfCircle = Paint.fillCircle(halfCircleImage, RegionOfInterest.createRegionFromImage(halfCircleImage),
                             new DoubleCoordinate((double)height/2.0, (double)height/2.0), (float)height/2.0f, 1.0f);

    // We 'convolve' this kernel, which should return a 1x1 image containing the sum we are interested in.
    Image kernelResultImage = Filter.convolveKernel(image,
                                                    leftCircleRoi,
                                                    halfCircleImage,
                                                    new ImageCoordinate(0, 0),
                                                    BorderSamplingModeEnum.CONSTRAIN_TO_REGION);

    float leftCircleRegionSum = kernelResultImage.getPixelValue(0, 0);
    kernelResultImage.decrementReferenceCount();

    // We 'convolve' this kernel a second time.
    Transform.flipX(halfCircleImage);
    kernelResultImage = Filter.convolveKernel(image,
                                              rightCircleRoi,
                                              halfCircleImage,
                                              new ImageCoordinate(0, 0),
                                              BorderSamplingModeEnum.CONSTRAIN_TO_REGION);

    float rightCircleRegionSum = kernelResultImage.getPixelValue(0, 0);
    kernelResultImage.decrementReferenceCount();
    halfCircleImage.decrementReferenceCount();

    int numberOfPixelsInRectRegion = (rectRegion.getWidth()*rectRegion.getHeight());

    double rectRegionSum = 0.0;
    if (numberOfPixelsInRectRegion > 0)
    {
      // find the sum of the pixels in the center rectangle
      rectRegionSum = rectangularMean(image, rectRegion) * (rectRegion.getWidth() * rectRegion.getHeight());
    }

    return (float)((rectRegionSum + leftCircleRegionSum + rightCircleRegionSum) /
                   (numberOfPixelsInRectRegion + 2*pixelsInHalfCircle));
  }



  /**
   * @author Patrick Lacz
   */
  private static float verticalObroundMean(Image image, RegionOfInterest regionOfInterest)
  {
    /**
     * The regions are set up to look like:
     *     ___
     *    / A \   = topCircleRoi
     *    |___|
     *    | B |   = rectRegion
     *    |___|
     *    | C |   = bottomCircleRoi
     *    \___/
     */
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);

    int width = regionOfInterest.getWidth();
    int height = regionOfInterest.getHeight();

    // we're setting the circle to not include the row that would include the full circle diameter. The rect region
    // will handle this region.
    int halfCircleHeight = (int)Math.ceil(width/2.0) - 1;

    Assert.expect(Math.abs(height-width) > 0);

    RegionOfInterest rectRegion = new RegionOfInterest(regionOfInterest);
    rectRegion.setHeightKeepingSameCenter(height - 2*halfCircleHeight); // minus two width/2 semicircles

    RegionOfInterest topCircleRoi = new RegionOfInterest(regionOfInterest);
    topCircleRoi.setHeightKeepingSameMinY(halfCircleHeight);

    RegionOfInterest bottomCircleRoi = new RegionOfInterest(topCircleRoi);
    bottomCircleRoi.translateXY(0, halfCircleHeight+rectRegion.getHeight());

    // create a summing filter
    Image halfCircleImage = new Image(width, halfCircleHeight);
    Paint.fillImage(halfCircleImage, 0.f);
    int pixelsInHalfCircle = Paint.fillCircle(halfCircleImage, RegionOfInterest.createRegionFromImage(halfCircleImage),
                             new DoubleCoordinate((double)width/2.0, (double)width/2.0), (float)width/2.0f, 1.0f);

    // We 'convolve' this kernel, which should return a 1x1 image containing the sum we are interested in.
    Image kernelResultImage = Filter.convolveKernel(image,
                                                    topCircleRoi,
                                                    halfCircleImage,
                                                    new ImageCoordinate(0, 0),
                                                    BorderSamplingModeEnum.CONSTRAIN_TO_REGION);
    float topCircleRegionSum = kernelResultImage.getPixelValue(0, 0);
    kernelResultImage.decrementReferenceCount();

    // We 'convolve' this kernel a second time.
    Transform.flipX(halfCircleImage);
    kernelResultImage = Filter.convolveKernel(image,
                                              bottomCircleRoi,
                                              halfCircleImage,
                                              new ImageCoordinate(0, 0),
                                              BorderSamplingModeEnum.CONSTRAIN_TO_REGION);
    float bottomCircleRegionSum = kernelResultImage.getPixelValue(0, 0);
    kernelResultImage.decrementReferenceCount();
    halfCircleImage.decrementReferenceCount();

    // find the sum of the pixels in the center rectangle
    double rectRegionSum = rectangularMean(image, rectRegion) * (rectRegion.getWidth()*rectRegion.getHeight());

    return (float)((rectRegionSum + topCircleRegionSum + bottomCircleRegionSum) /
                   ((rectRegion.getWidth()*rectRegion.getHeight()) + 2*pixelsInHalfCircle));
  }


  /**
   * @author Patrick Lacz
   */
  public static float rectangularMean(Image image, RegionOfInterest regionOfInterest)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(image));
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    return nativeMean(image.getNativeDefinition(),
                      regionOfInterest.getMinX(),
                      regionOfInterest.getMinY(),
                      regionOfInterest.getWidth(),
                      regionOfInterest.getHeight());
  }


  /**
   * Computes the mean in the area surrounding <pre>innerRegion</pre> in <pre>outerRegion</pre>.
   * All means are computed from rectangular regions, so the inner ROI will be treated as rectangular
   * regardless of the actual RegionShapeEnum. The outer region must be rectangular.
   *
   *
   * @author Patrick Lacz
   */
  public static float meanAroundRegion(Image image, RegionOfInterest outerRegion, RegionOfInterest innerRegion)
  {
    Assert.expect(image != null);
    Assert.expect(outerRegion != null);
    Assert.expect(innerRegion != null);
    Assert.expect(outerRegion.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR));
    Assert.expect(innerRegion.fitsWithinRegionOfInterest(outerRegion));

    // We may constrict the outer region to include only the section actually inside the image.
    RegionOfInterest outerRegionToUse = outerRegion;
    if (outerRegion.fitsWithinImage(image) == false)
    {
      outerRegionToUse = RegionOfInterest.createRegionFromIntersection(outerRegion, RegionOfInterest.createRegionFromImage(image));
    }
    // We also may constrict the inner region to include only the section actually inside the image.
    RegionOfInterest innerRegionToUse = innerRegion;
    if (outerRegion.fitsWithinImage(image) == false)
    {
      innerRegionToUse = RegionOfInterest.createRegionFromIntersection(innerRegion, RegionOfInterest.createRegionFromImage(image));
    }

    float sum = 0.0f;
    int numPixels = 0;

    RegionOfInterest topRegion = new RegionOfInterest(outerRegionToUse);
    topRegion.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);
    int newHeight = innerRegionToUse.getMinY() - outerRegionToUse.getMinY();
    if (newHeight  > 0)
    {
      topRegion.setHeightKeepingSameMinY(newHeight);
      sum += sumOfPixels(image, topRegion);
      numPixels += topRegion.getWidth()*topRegion.getHeight();
    }

    RegionOfInterest leftRegion = new RegionOfInterest(topRegion);
    leftRegion.translateXY(0, newHeight);
    leftRegion.setHeightKeepingSameMinY(innerRegionToUse.getHeight());
    int newWidth = innerRegionToUse.getMinX() - outerRegionToUse.getMinX();
    if (newWidth > 0)
    {
      leftRegion.setWidthKeepingSameMinX(newWidth);
      sum += sumOfPixels(image, leftRegion);
      numPixels += leftRegion.getWidth()*leftRegion.getHeight();
    }

    RegionOfInterest rightRegion = new RegionOfInterest(leftRegion);
    rightRegion.translateXY(innerRegionToUse.getMaxX() - leftRegion.getMinX() + 1, 0);
    newWidth = outerRegionToUse.getMaxX() - rightRegion.getMinX() + 1;
    if (newWidth > 0)
    {
      rightRegion.setWidthKeepingSameMinX(outerRegionToUse.getMaxX() - rightRegion.getMinX() + 1);
      sum += sumOfPixels(image, rightRegion);
      numPixels += rightRegion.getWidth()*rightRegion.getHeight();
    }

    RegionOfInterest bottomRegion = new RegionOfInterest(topRegion);
    bottomRegion.translateXY(0, innerRegionToUse.getMaxY() - outerRegionToUse.getMinY() + 1);
    newHeight = outerRegionToUse.getMaxY() - innerRegionToUse.getMaxY();
    if (newHeight > 0)
    {
      bottomRegion.setHeightKeepingSameMinY(newHeight);
      sum += sumOfPixels(image, bottomRegion);
      numPixels += bottomRegion.getWidth()*bottomRegion.getHeight();
    }
    if (numPixels > 0)
      return sum / numPixels;
    return 0.0f;
  }

  /**
   * Computes the median in the area surrounding <pre>innerRegion</pre> in <pre>outerRegion</pre>.
   * The median is computed from rectangular regions, so the inner ROI will be treated as rectangular
   * regardless of the actual RegionShapeEnum. The outer region must be rectangular.
   * @author Rex Shang
   */
  public static float medianAroundRegion(Image image, RegionOfInterest outerRegion, RegionOfInterest innerRegion)
  {
    Assert.expect(image != null);
    Assert.expect(outerRegion != null);
    Assert.expect(innerRegion != null);
    Assert.expect(outerRegion.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR));
    Assert.expect(innerRegion.fitsWithinRegionOfInterest(outerRegion));

    // We may constrict the outer region to include only the section actually inside the image.
    RegionOfInterest outerRegionToUse = outerRegion;
    if (outerRegion.fitsWithinImage(image) == false)
      outerRegionToUse = RegionOfInterest.createRegionFromIntersection(outerRegion, RegionOfInterest.createRegionFromImage(image));

    // We also may constrict the inner region to include only the section actually inside the image.
    // this region is modified, so we make a copy of it.
    RegionOfInterest innerRegionToUse = new RegionOfInterest(innerRegion);
    if (innerRegion.fitsWithinImage(image) == false)
      innerRegionToUse = RegionOfInterest.createRegionFromIntersection(innerRegion, RegionOfInterest.createRegionFromImage(image));

    // To exclude the values of the inner region from median calculation, we are filling the inner
    // region with maximum value that would push the median to be a smaller percentile.
    // So the percentile = (Outer - Inner) / Outer / 2
    int outerRegionPixelCount = outerRegionToUse.getWidth() * outerRegionToUse.getHeight();
    int innerRegionPixelCount = innerRegionToUse.getWidth() * innerRegionToUse.getHeight();
    Assert.expect(innerRegionPixelCount < outerRegionPixelCount, "Inner region has to be smaller than outer region.");
    Assert.expect(outerRegionPixelCount > 1, "Outer region has to have more than 1 pixel.");
    float maxValue = Float.MAX_VALUE;
    float percentileWithMaxFill = (float) (outerRegionPixelCount - innerRegionPixelCount - 1)
                                  / (float) (outerRegionPixelCount - 1) / 2.0f;
    Image imageCopy = Image.createContiguousFloatImage(outerRegionToUse.getWidth(), outerRegionToUse.getHeight());
    Transform.copyImageIntoImage(image, outerRegionToUse, imageCopy, RegionOfInterest.createRegionFromImage(imageCopy));

    innerRegionToUse.translateXY(-outerRegionToUse.getMinX(), -outerRegionToUse.getMinY());

    Paint.fillRegionOfInterest(imageCopy, innerRegionToUse, maxValue);
    float median = getPercentileInPlace(imageCopy, percentileWithMaxFill);

    // Clean up the image buffer.
    imageCopy.decrementReferenceCount();

    return median;
  }

  /**
   * @author Patrick Lacz
   */
  public static float circularMedian(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));

    float radius = Math.min(roi.getWidth(), roi.getHeight()) / 2.f;

    Image circleImage = new Image(roi.getWidth(), roi.getHeight());
    Paint.fillImage(circleImage, Float.POSITIVE_INFINITY);

    RegionOfInterest subImageRoi = RegionOfInterest.createRegionFromImage(circleImage);
    DoubleCoordinate centerCoordinate = new DoubleCoordinate(subImageRoi.getCenterX(),
                                                             subImageRoi.getCenterY());

    int numberOfPixelsInsideCircle = Paint.fillCircle(circleImage, subImageRoi, centerCoordinate, radius, 0.0f);

    Image copyOfCircleArea = Image.createContiguousFloatImage(roi.getWidth(), roi.getHeight());
    Transform.copyImageIntoImage(image, roi, copyOfCircleArea, subImageRoi);

    Arithmetic.addImages(copyOfCircleArea, subImageRoi, circleImage, subImageRoi, copyOfCircleArea, subImageRoi);

    circleImage.decrementReferenceCount();

    Statistics.sortAscendingInPlace(copyOfCircleArea);

    int indexOfMedian = (numberOfPixelsInsideCircle - 1) / 2;
    int imageWidth = roi.getWidth();
    float median = copyOfCircleArea.getPixelValue(indexOfMedian % imageWidth, indexOfMedian / imageWidth);
    if (numberOfPixelsInsideCircle % 2 == 0)
    {
      float otherMedianPixel = copyOfCircleArea.getPixelValue((indexOfMedian + 1) % imageWidth,
                                                              (indexOfMedian + 1) / imageWidth);
      median = (median + otherMedianPixel)/ 2.f;
    }
    copyOfCircleArea.decrementReferenceCount();

    return median;
  }

  /**
   * Computes the mean value and standard deviation in an Image.
   * @author Patrick Lacz
   */
  public static void meanStdDev(Image image, DoubleRef meanRef, DoubleRef stdDevRef)
  {
    Assert.expect(image != null);

    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder(0, 0, image.getWidth() - 1,
        image.getHeight() - 1,
        0, RegionShapeEnum.RECTANGULAR);
    meanStdDev(image, roi, meanRef, stdDevRef);
  }

  /**
   * Computes the mean value and standard deviation within a region of interest.
   * @author Patrick Lacz
   */
   public static void meanStdDev(Image image, RegionOfInterest regionOfInterest,
                                DoubleRef meanRef, DoubleRef stdDevRef)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR));
    Assert.expect(regionOfInterest.fitsWithinImage(image));
    Assert.expect(meanRef != null);
    Assert.expect(stdDevRef != null);
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    nativeMeanStdDev(image.getNativeDefinition(),
                     regionOfInterest.getMinX(),
                     regionOfInterest.getMinY(),
                     regionOfInterest.getWidth(),
                     regionOfInterest.getHeight(),
                     meanRef,
                     stdDevRef);
  }

  /**
   * @todo : Create Regression Tests
   * @author Patrick Lacz
   */
  public static double sumOfPixels(Image image)
  {
    Assert.expect(image != null);

    return sumOfPixels(image, RegionOfInterest.createRegionFromImage(image));
  }

  /**
   * @todo : Create Explicit Regression Tests (Implicitly tested via meanAroundRegion)
   * @author Patrick Lacz
   */
  public static double sumOfPixels(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR));
    Assert.expect(roi.fitsWithinImage(image));
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    return nativeSumOfPixels(image.getNativeDefinition(), roi.getMinX(), roi.getMinY(),
                             roi.getWidth(), roi.getHeight());
  }


  /**
   * Computes the maximum value for the entire image.
   * @author Patrick Lacz
   */
  public static float maxValue(Image image)
  {
    Assert.expect(image != null);

    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder(0, 0, image.getWidth() - 1,
      image.getHeight() - 1,
      0, RegionShapeEnum.RECTANGULAR);
    return maxValue(image, roi);
  }

  /**
   * Cimputes the maximum value within a region of interest.
   * @author Patrick Lacz
   */
  public static float maxValue(Image image, RegionOfInterest regionOfInterest)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(image));
    Assert.expect(regionOfInterest.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR));
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    int maxCoords[] = nativeMax(image.getNativeDefinition(),
                      regionOfInterest.getMinX(),
                      regionOfInterest.getMinY(),
                      regionOfInterest.getWidth(),
                      regionOfInterest.getHeight());

    float maxValue = image.getPixelValue(maxCoords[0], maxCoords[1]);
    return maxValue;
  }

  /**
   * Computes the location of the maximum value in the image.
   * @author Patrick Lacz
   */
  public static ImageCoordinate maxPixel(Image image)
  {
    Assert.expect(image != null);

    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder(0, 0, image.getWidth() - 1,
        image.getHeight() - 1,
        0, RegionShapeEnum.RECTANGULAR);
    return maxPixel(image, roi);
  }

  /**
   * Computes the location of the maximum value within a region of interest.
   * @author Patrick Lacz
   */
  public static ImageCoordinate maxPixel(Image image, RegionOfInterest regionOfInterest)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(image));
    Assert.expect(regionOfInterest.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR));
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    int maxCoords[] = nativeMax(image.getNativeDefinition(),
                                regionOfInterest.getMinX(),
                                regionOfInterest.getMinY(),
                                regionOfInterest.getWidth(),
                                regionOfInterest.getHeight());

    return new ImageCoordinate(maxCoords[0], maxCoords[1]);
  }

  /**
   * Computes the minimum value for the entire image.
   * @author Patrick Lacz
   */
  public static float minValue(Image image)
  {
    Assert.expect(image != null);

    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder(0, 0, image.getWidth() - 1,
        image.getHeight() - 1,
        0, RegionShapeEnum.RECTANGULAR);
    return minValue(image, roi);
  }

  /**
   * Computes the minimum value within a region of interest.
   * @author Patrick Lacz
   */
  public static float minValue(Image image, RegionOfInterest regionOfInterest)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(image));
    Assert.expect(regionOfInterest.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR));
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    int minCoords[] = nativeMin(image.getNativeDefinition(),
                      regionOfInterest.getMinX(),
                      regionOfInterest.getMinY(),
                      regionOfInterest.getWidth(),
                      regionOfInterest.getHeight());

    float minValue = image.getPixelValue(minCoords[0], minCoords[1]);
    return minValue;
  }

  /**
   * Computes the location of the minimum value in the image.
   * @author Patrick Lacz
   */
  public static ImageCoordinate minPixel(Image image)
  {
    Assert.expect(image != null);

    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder(0, 0, image.getWidth() - 1,
        image.getHeight() - 1,
        0, RegionShapeEnum.RECTANGULAR);
    return minPixel(image, roi);
  }

  /**
   * Computes the location of the minimum value within a region of interest.
   * @author Patrick Lacz
   */
  public static ImageCoordinate minPixel(Image image, RegionOfInterest regionOfInterest)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(image));
    Assert.expect(regionOfInterest.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR));
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    int minCoords[] = nativeMin(image.getNativeDefinition(),
                                regionOfInterest.getMinX(),
                                regionOfInterest.getMinY(),
                                regionOfInterest.getWidth(),
                                regionOfInterest.getHeight());

    return new ImageCoordinate(minCoords[0], minCoords[1]);
  }


  /**
   * @author Patrick Lacz
   */
  public static float median(Image image)
  {
    return getPercentile(image, RegionOfInterest.createRegionFromImage(image), 0.5f);
  }

  /**
   * @author Patrick Lacz
   */
  public static float median(Image image, RegionOfInterest roi)
  {
    return getPercentile(image, roi, 0.5f);
  }

  /**
   * @author Patrick Lacz
   */
  public static float getPercentile(Image image, RegionOfInterest roi, float percentile)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));
    Assert.expect(percentile >= 0.f && percentile <= 1.f);

    int regionWidth = roi.getWidth();
    int regionHeight = roi.getHeight();
    Image contiguousImage = Image.createContiguousFloatImage(regionWidth, regionHeight);
    Transform.copyImageIntoImage(image, roi, contiguousImage, RegionOfInterest.createRegionFromImage(contiguousImage));

    sortAscendingInPlace(contiguousImage);

    int numberOfPixels = regionWidth * regionHeight;
    double indexVal = percentile * (numberOfPixels - 1);
    double indexFloor = Math.floor(indexVal + 0.00001);
    int index = (int)indexFloor;
    float result = contiguousImage.getPixelValue(index % regionWidth, index / regionWidth);
    float indexOffset = (float) indexVal - (float) indexFloor;
    // Check to see if our index is off from an integer too much.  If so, averaging is needed.
    if (indexOffset > 0.00002f)
    {
      int indexB = index + 1;
      float valueB = contiguousImage.getPixelValue(indexB % regionWidth, indexB / regionWidth);
      // Use weighted average to find our percentile.
      result = (1.f - indexOffset) * result + indexOffset * valueB;
    }
    contiguousImage.decrementReferenceCount();

    return result;
  }

  /**
   * @author Patrick Lacz
   */
  public static float getPercentileInPlace(Image image, float percentile)
  {
    Assert.expect(image != null);
    Assert.expect(percentile >= 0.f && percentile <= 1.f);

    int regionWidth = image.getWidth();
    int regionHeight = image.getHeight();

    sortAscendingInPlace(image);

    int numberOfPixels = regionWidth * regionHeight;
    double indexVal = percentile * (numberOfPixels - 1);
    double indexFloor = Math.floor(indexVal + 0.00001);
    int index = (int)indexFloor;
    float result = image.getPixelValue(index % regionWidth, index / regionWidth);
    float indexOffset = (float)indexVal - (float)indexFloor;
    // Check to see if our index is off from an integer too much.  If so, averaging is needed.
    if (indexOffset > 0.00002f)
    {
      int indexB = index + 1;
      float valueB = image.getPixelValue(indexB % regionWidth, indexB / regionWidth);
      // Use weighted average to find our percentile.
      result = (1.f - indexOffset) * result + indexOffset * valueB;
    }

    return result;
  }

  /**
   * @author Patrick Lacz
   */
  public static float[] getPercentiles(Image image, RegionOfInterest roi, float... percentiles)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));
    Assert.expect(percentiles != null);
    Assert.expect(roi.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR));

    int regionWidth = roi.getWidth();
    int regionHeight = roi.getHeight();
    Image contiguousImage = Image.createContiguousFloatImage(regionWidth, regionHeight);
    Transform.copyImageIntoImage(image, roi, contiguousImage, RegionOfInterest.createRegionFromImage(contiguousImage));


    sortAscendingInPlace(contiguousImage);

    float valuesAtPercentiles[] = new float[percentiles.length];

    int numberOfPixels = regionWidth * regionHeight;

    int numberOfPercentilesFound = 0;
    for (float percentile : percentiles)
    {
      Assert.expect(percentile >= 0.f && percentile <= 1.f);

      double indexVal = percentile * (numberOfPixels - 1);
      double indexFloor = Math.floor(indexVal + 0.00001);
      int index = (int)indexFloor;
      float valueA = contiguousImage.getPixelValue(index % regionWidth, index / regionWidth);
      valuesAtPercentiles[numberOfPercentilesFound] = valueA;
      if ((indexVal - indexFloor) > 0.00002)
      {
        int indexB = index + 1;
        float valueB = contiguousImage.getPixelValue(indexB % regionWidth, indexB / regionWidth);
        valuesAtPercentiles[numberOfPercentilesFound] = (1.f - percentile) * valueA + percentile * valueB;
        numberOfPercentilesFound++;
      }
    }
    contiguousImage.decrementReferenceCount();
    return valuesAtPercentiles;
  }

  /**
   * Sorts the pixel values stored in <pre>image</pre>.
   *
   * @author Patrick Lacz
   */
  public static void sortAscendingInPlace(Image image)
  {
    Assert.expect(image != null);
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    nativeSortAscending(image.getNativeDefinition());
  }

  /**
   * @author Patrick Lacz
   */
  public static Map<IntCoordinate, Double> computeNormalizedCentralMoments(Image image, RegionOfInterest roi, int maximumOrder)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    double momentArray[] = nativeNormalizedCentralMoments(image.getNativeDefinition(),
                                         roi.getMinX(), roi.getMinY(),
                                         roi.getWidth(), roi.getHeight(),
                                         maximumOrder,
                                         2 // 1 = fast, 2 = accurate
                         );
    Assert.expect(momentArray != null);

    Map<IntCoordinate, Double> momentMap = new HashMap<IntCoordinate,Double>();

    int order = 0;
    int orderBase = 0;
    int nextOrder = 1;
    for (int i = 0 ; i < momentArray.length ; ++i)
    {
      if (i == nextOrder)
      {
        ++order;
        orderBase = nextOrder;
        // There are order + 1 entries in the array for each order. (0 = {00}, 1 = {10, 01}, 2 = {20, 11, 02}, etc.)
        nextOrder += order + 1;

      }

      int mIndex = i - orderBase;
      int nIndex = order - mIndex;

      Assert.expect(nIndex + mIndex == order);

      momentMap.put(new IntCoordinate(nIndex, mIndex), momentArray[i]);
    }

    return momentMap;
  }

  /**
   * @author Patrick Lacz
   */
  public static double[] computeHuMoments(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    double huMoments[] = nativeHuMoments(image.getNativeDefinition(),
                                         roi.getMinX(), roi.getMinY(),
                                         roi.getWidth(), roi.getHeight(),
                                         2 // 1 = fast, 2 = accurate
                         );
    Assert.expect(huMoments != null);
    return huMoments;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static void clear()
  {
    if (_cachedCircleKernels != null)
    {
      for(Pair<Image, Integer> cacheImage : _cachedCircleKernels)
      {
        cacheImage.getFirst().decrementReferenceCount();
      }
      _cachedCircleKernels.clear();
    }
  }

  /**
    * Computes the seven Hu Moments from a Region Of Interest.
    *
    * @param algHint A hint to the algorithm to run fast (1) or accurate (2) or no preference (0).
    * @author Patrick Lacz
    */
   static native private double[] nativeHuMoments(int sourceImage[], int offsetX, int offsetY, int roiWidth, int roiHeight, int algHint);

   /**
    * Computes Central Moments from a Region Of Interest.
    *
    * The return value array is in the following order:
    *   each order, ascending,
    *   within each order, for u_mn, m starts at its maximum and decreases.  ie. u_20, u_11, u_02.
    *
    * @param order The highest order of moment to retrieve. For example, u_00 is order 0, while u_11 is order 2.
    * @param algHint A hint to the algorithm to run fast (1) or accurate (2) or no preference (0).
    * @author Patrick Lacz
    */
   static native private double[] nativeNormalizedCentralMoments(int sourceImage[], int offsetX, int offsetY, int roiWidth, int roiHeight, int order, int algHint);

   /**
    * @author Patrick Lacz
    */
   static native private float nativeMean(int sourceImage[], int roiMinX, int roiMinY, int roiMaxX, int roiMaxY);

   /**
    * @author Patrick Lacz
    */
   private native static double nativeSumOfPixels(int sourceImage[], int roiMinX, int roiMinY, int roiWidth, int roiHeight);

   /**
    * @author Patrick Lacz
    */
   static native private int[] nativeMax(int sourceImage[], int roiMinX, int roiMinY, int roiWidth, int roiHeight);

   /**
    * @author Patrick Lacz
    */
   static native private int[] nativeMin(int sourceImage[], int roiMinX, int roiMinY, int roiWidth, int roiHeight);

   /**
    * @author Patrick Lacz
    */
   static native private void nativeMeanStdDev(int sourceImage[],
       int roiMinX, int roiMinY, int roiMaxX, int roiMaxY, DoubleRef mean, DoubleRef stdDev);

   /**
    * Cannot use a region of interest: use a contiguous copy, if possible.
    *
    * @author Patrick Lacz
    */
  static native private void nativeSortAscending(int sourceImage[]);
}
