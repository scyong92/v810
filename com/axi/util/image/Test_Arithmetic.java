package com.axi.util.image;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Patrick Lacz
 */
class Test_Arithmetic extends UnitTest
{
  /**
   * @author Patrick Lacz
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Arithmetic());
  }

  /**
   * @author Patrick Lacz
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Image image = loadTestImage(getTestDataDir(), "test1.png");

    /* test1.png is an image that looks like:

      0    0    0   127 127 127
      0    0    0   127 127 127
      0    0    0   127 127 127
      0    0    0   255 255 255
      0    0    0   255 255 255
      0    0    0   255 255 255
     */
    testBadInputs(image);

    testMultiply(image);

    testMaximum(image);

    image.decrementReferenceCount();
    // uncomment to perform timing tests
    //testTiming();
  }

  /**
   * @author Patrick Lacz
   */
  private void testBadInputs(Image image)
  {
    Assert.expect(image != null);

    RegionOfInterest goodRoi = RegionOfInterest.createRegionFromImage(image);

    // test illegal regions of interest
    Collection<RegionOfInterest> regions = ImageUnitTestUtil.generateBadRegionsOfInterest(image);
    for (RegionOfInterest badRoi : regions)
    {
      // multiplyByConstant
      try
      {
        Arithmetic.multiplyByConstantInPlace(image, badRoi, 1.f);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }

      // add
      try
      {
        Arithmetic.addImages(image, badRoi, image, badRoi);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
      try
      {
        Arithmetic.addImages(image, goodRoi, image, badRoi);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
      try
      {
        Arithmetic.addImages(image, badRoi, image, goodRoi);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
      try
      {
        RegionOfInterest differentGoodRoi = RegionOfInterest.createRegionFromImage(image);
        differentGoodRoi.setWidthKeepingSameCenter(3);

        Arithmetic.addImages(image, badRoi, image, differentGoodRoi);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }

      // multiplyByConstant
      try
      {
        Arithmetic.multiplyImages(image, badRoi, image, badRoi);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }      // multiplyByConstant
      try
      {
        Arithmetic.multiplyImages(image, goodRoi, image, badRoi);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }      // multiplyByConstant
      try
      {
        Arithmetic.multiplyImages(image, badRoi, image, goodRoi);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
      try
      {
        RegionOfInterest differentGoodRoi = RegionOfInterest.createRegionFromImage(image);
        differentGoodRoi.setWidthKeepingSameCenter(3);

        Arithmetic.multiplyImages(image, badRoi, image, differentGoodRoi);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }

    }

  }

  /**
   * @author Patrick Lacz
   */
  private void testMultiply(Image image)
  {
    Assert.expect(image != null);

    image = new Image(image);

    // Whole Image.
    Arithmetic.multiplyByConstantInPlace(image, 3.f);
    float minResult = Statistics.minValue(image);
    Expect.expect(Math.abs(minResult - 0.f) < 0.00001f);
    float maxResult = Statistics.maxValue(image);
    Expect.expect(Math.abs(maxResult - 3.f * 255.f) < 0.00001f);
    float meanResult = Statistics.mean(image);
    Expect.expect(Math.abs(meanResult - 3.f * 95.5f) < 0.00001f);

    // With Region of Interest
    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder(1, 1, 2, 2, 0, RegionShapeEnum.RECTANGULAR);
    Arithmetic.multiplyByConstantInPlace(image, roi, -0001.f);
    minResult = Statistics.minValue(image);
    Expect.expect(Math.abs(minResult - 0.f) < 0.00001f);
    maxResult = Statistics.maxValue(image);
    Expect.expect(Math.abs(maxResult - 3.f * 255.f) < 0.00001f);
    meanResult = Statistics.mean(image);
    Expect.expect(Math.abs(meanResult - 3.f * 95.5f) < 0.00001f);

    image.decrementReferenceCount();
    /**
     * @todo more test cases
     */
  }


  /**
   * @author Patrick Lacz
   */
  private void testMaximum(Image image)
  {
    Assert.expect(image != null);

    Image xFlippedImage = new Image(image);
    Transform.flipX(xFlippedImage);
    Image yFlippedImage = new Image(image);
    Transform.flipY(yFlippedImage);

    Image resultImage = Arithmetic.maximumOfImages(image, image);

    boolean compareResult = resultImage.equals(image);
    if (compareResult == false)
    {
      resultImage.printImageCSV("resultImage");
    }
    Assert.expect(compareResult);
    resultImage.decrementReferenceCount();

    resultImage = Arithmetic.maximumOfImages(image, xFlippedImage);
    float minValue = Statistics.minValue(resultImage);
    Assert.expect(minValue == 127.f);
    Assert.expect(resultImage.getPixelValue(0,0) == 127.f);
    Assert.expect(resultImage.getPixelValue(1,1) == 127.f);
    Assert.expect(resultImage.getPixelValue(0,3) == 255.f);
    Assert.expect(resultImage.getPixelValue(1,3) == 255.f);
    resultImage.decrementReferenceCount();

    resultImage = Arithmetic.maximumOfImages(image, yFlippedImage);
    Assert.expect(resultImage.getPixelValue(0, 0) == 0.0f);
    Assert.expect(resultImage.getPixelValue(1, 1) == 0.0f);
    Assert.expect(resultImage.getPixelValue(3, 0) == 255.f);
    Assert.expect(resultImage.getPixelValue(3, 1) == 255.f);
    resultImage.decrementReferenceCount();

    xFlippedImage.decrementReferenceCount();
    yFlippedImage.decrementReferenceCount();

    /**
     * @todo more test cases
     */
  }


  /**
   * Implemented only to compare techniques of implementing the algorithm. Kept for future reference.
   * @author Patrick Lacz
   */
  private void testTiming()
  {
    Image image = loadTestImage(getTestDataDir(), "testimage_png1.png");

    Assert.expect(image != null);

    System.out.println("Image Dimensions: " + image.getWidth() + " x " + image.getHeight());

    int tests = 1000;

    long nanoStart = System.nanoTime();
    for (int i = 0 ; i < tests ; ++i)
    {
      Arithmetic.multiplyByConstantInPlace(image, 1.f);
    }
    long nanoEnd = System.nanoTime();

    double duration = (nanoEnd - nanoStart)/tests;
    System.out.println("Multiply by Constant invokation took on average " + duration/100000.0 + " milliseconds.");
    image.decrementReferenceCount();
  }

  /**
   * Copied from Test_ImageFeatureExtraction
   * @author Peter Esbensen
   */
  private Image loadTestImage(String directoryPath, String filename)
  {
    Assert.expect(directoryPath != null);
    Assert.expect(directoryPath.length() > 0);
    Assert.expect(filename != null);
    Assert.expect(filename.length() > 0);

    Image resultImage = null;
    try
    {
      resultImage = ImageIoUtil.loadPngImage(directoryPath + File.separator + filename);
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "Exception occurred during PNG Image Load: " + exc.getMessage());
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(false, "FileDoesNotExistException:" + exc.getMessage());
    }
    Assert.expect(resultImage != null);
    return resultImage;
  }
}
