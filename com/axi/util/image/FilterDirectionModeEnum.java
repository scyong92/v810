package com.axi.util.image;

/**
 *
 * @author Lim, Lay Ngor
 */
public class FilterDirectionModeEnum extends com.axi.util.Enum
{  
  //Modify to fix number so that the value is tally with third party enum file at:
  //thirdParty\VitroxImageEnhancer\Include/VfImageEnhancer.hpp, enum VxImageEnhancer::FilterDirectionMode
  //This is to ensure the enum number we pass into the third party function is tally.  
  //Only list out supported enum that being use by JNI function here
  public static final FilterDirectionModeEnum None = new FilterDirectionModeEnum(0);
  public static final FilterDirectionModeEnum Horizontal = new FilterDirectionModeEnum(1);
  public static final FilterDirectionModeEnum Vertical = new FilterDirectionModeEnum(2);
  public static final FilterDirectionModeEnum Both = new FilterDirectionModeEnum(3);

  /**
   * @author Lim, Lay Ngor
   */
  public FilterDirectionModeEnum(int id)
  {
    super(id);
  }
  
  /**
   * @author Lim, Lay Ngor
   */
  public String getString(FilterDirectionModeEnum mode)
  {
    if(mode == FilterDirectionModeEnum.Horizontal)
      return "Horizontal";
    else if(mode == FilterDirectionModeEnum.Vertical)
      return "Vertical";
    else
      return "None";
  }
}
