package com.axi.util.image;

import com.axi.util.Assert;
import com.axi.util.image.Image;

/**
 * ImageEnhancer class
 *
 * This class holds functions to perform resize operations on Image data.
 *
 * @author Bee Hoon
 */

public class ImageEnhancer
{
  public ImageEnhancer()
  {
    // do nothing
  }

  static
  {
//    System.loadLibrary("nativeImageUtil");
    System.loadLibrary("nativeImageEnhancer");
  }

 /**
   * Perform resize operations on Image (32-bits Image; Interpolation Mode = Linear).
   * 
   * @author Bee Hoon
   */
  public static Image resizeLinear32bitsImage(Image destImage, Image srcImage, double scaleX, double scaleY)
  {
    Assert.expect(srcImage != null);
    
    nativeResizeLinear32bitsImage(destImage.getNativeDefinition(), srcImage.getNativeDefinition(), scaleX, scaleY); 
  
    return destImage;
  }

  /**
   * Perform resize operations on Image (32-bits Image; Interpolation Mode = Cubic).
   * 
   * @author Bee Hoon
   */
  public static Image resizeCubic32bitsImage(Image destImage, Image srcImage, double scaleX, double scaleY)
  {
    Assert.expect(srcImage != null);

    nativeResizeCubic32bitsImage(destImage.getNativeDefinition(), srcImage.getNativeDefinition(), scaleX, scaleY);

    return destImage;
  }

  /**
   * Perform CLAHE on image
   *
   * @author Bee Hoon
   */
  public static Image contrastLimitAdapHisEqual(Image destImage, Image srcImage, int blockRadius, int bins, float slope, boolean isFast)
  {
    Assert.expect(srcImage != null);

    nativeCLAHE(destImage.getNativeDefinition(), srcImage.getNativeDefinition(), blockRadius, bins, slope, isFast);

    return destImage;
  }

  /**
   * Perform RemoveArtifactByBoxFilter on image
   *
   * @author Bee Hoon
   */
  public static Image removeArtifactByBoxFilter(Image destImage, Image srcImage, int maskWidth, int maskHeight, int iteration)
  {
    Assert.expect(srcImage != null);

    nativeRemoveArtifactByBoxFilter(destImage.getNativeDefinition(), srcImage.getNativeDefinition(), maskWidth, maskHeight, iteration);

    return destImage;
  }

  /**
   * Perform EnumBoxFilter on image
   *
   * @author Bee Hoon
   */
  public static Image boxFilter(Image destImage, Image srcImage, int maskWidth, int maskHeight, int iteration)
  {
    Assert.expect(srcImage != null);

    nativeBoxFilter(destImage.getNativeDefinition(), srcImage.getNativeDefinition(), maskWidth, maskHeight, iteration);

    return destImage;
  }

  /**
   * Perform Subtract operation on two images
   *
   * @author Bee Hoon
   */
  public static Image subtractImages(Image destImage, Image src1Image, Image src2Image, int width, int height)
  {
    Assert.expect(src1Image != null);
    Assert.expect(src2Image != null);

    nativeSubtract(destImage.getNativeDefinition(), src1Image.getNativeDefinition(), src2Image.getNativeDefinition(), width, height);

    return destImage;
  }

  /**
   * Perform Invert on image
   *
   * @author Ying-Huan.Chu
   */
  public static Image invertImage(Image destImage, Image srcImage)
  {
    Assert.expect(srcImage != null);

    nativeInvertImage(destImage.getNativeDefinition(), srcImage.getNativeDefinition());

    return destImage;
  }

  /**
   * Perform Colour Twist on image using ColourType enum
   *
   * @author Ying-Huan.Chu
   */
  public static Image colourTwistWithEnum(Image destImage, Image srcImage, ColourTypeEnum colour)
  {
    Assert.expect(srcImage != null);

    int colourTypeEnumId = colour.getId();
    nativeColourTwistWithEnum(destImage.getNativeDefinition(), srcImage.getNativeDefinition(), colourTypeEnumId);

    return destImage;
  }

  /**
   * Perform Colour Twist on image using array
   *
   * @author Ying-Huan.Chu
   */
  public static Image colourTwistWithArray(Image destImage, Image srcImage, float[][] colourTwistArray)
  {
    Assert.expect(srcImage != null);

    nativeColourTwistWithArray(destImage.getNativeDefinition(), srcImage.getNativeDefinition(), colourTwistArray);

    return destImage;
  }

  /**
   * Perform Colour Map on image
   *
   * @author Ying-Huan.Chu
   */
  public static void colourMap(String destImageFilePath, String srcImageFilePath, ColourMapTypeEnum colourMap)
  {
    int colourMapTypeEnumId = colourMap.getId();
    nativeColourMap(destImageFilePath, srcImageFilePath, colourMapTypeEnumId);
  }

  /**
   * Perform FFT Band Pass Filter on image
   *
   * @author Lim, Lay Ngor
   */
  public static Image FFTBandPassFilter(Image destImage, Image srcImage, int lowPassLargeDownTo, int hiPassSmallUpTo, double toleranceOfDirection, FilterDirectionModeEnum suppressStripesMode, boolean enhanceContrast, int saturateValue, InterpolationModeEnum interpolationMode)
  {
    Assert.expect(srcImage != null);

    int suppressStripesModeEnumId = suppressStripesMode.getId();
    int interpolationModeEnumId = interpolationMode.getId();
    nativeFFTBandPassFilter(destImage.getNativeDefinition(), srcImage.getNativeDefinition(),
      lowPassLargeDownTo, hiPassSmallUpTo, toleranceOfDirection,
      suppressStripesModeEnumId, enhanceContrast, saturateValue,
      interpolationModeEnumId);

    return destImage;
  }
  
  /**
   * Perform Retinex on image
   * @author Siew Yeng
   */
  public static Image RFilter(Image destImage, Image srcImage)
  {
    Assert.expect(srcImage != null);
    
    nativeRFilter(destImage.getNativeDefinition(), 
                  srcImage.getNativeDefinition(),
                  3.f, 
                  1.f, 
                  10);

    return destImage;
  }

  /**
   * @author Kok Chun, Tan
   */
  public static Image MotionBlur(Image destImage, Image srcImage, int scale, int offset, FilterDirectionModeEnum direction, int iteration)
  {
    Assert.expect(srcImage != null);
    Assert.expect(direction != null);
    Assert.expect(iteration >= 0);

    nativeMotionBlur(destImage.getNativeDefinition(), 
                     srcImage.getNativeDefinition(),
                     scale, 
                     offset,
                     direction.getId(),
                     iteration);

    return destImage;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static Image ShadingRemoval(Image destImage, Image srcImage, int blurDistance, int keepOutDistance, FilterDirectionModeEnum direction)
  {
    Assert.expect(srcImage != null);
    Assert.expect(direction != null);

    nativeShadingRemoval(destImage.getNativeDefinition(), 
                         srcImage.getNativeDefinition(),
                         blurDistance, 
                         keepOutDistance,
                         direction.getId());

    return destImage;
  }
  
  private static native void nativeResizeLinear32bitsImage(int[] dImage, int[] sImage, double scaleX, double scaleY);

  private static native void nativeResizeCubic32bitsImage(int[] dImage, int[] sImage, double scaleX, double scaleY);

  private static native void nativeCLAHE(int[] dImage, int[] sImage, int nBlockRadius, int nBins, float nSlope, boolean nIsFast);

  private static native void nativeRemoveArtifactByBoxFilter(int[] dImage, int[] sImage, int nMaskWidth, int nMaskHeight, int nIteration);

  private static native void nativeBoxFilter(int[] dImage, int[] sImage, int nMaskWidth, int nMaskHeight, int nIteration);

  private static native void nativeSubtract(int[] dImage, int[] s1Image, int[] s2Image, int width, int height);

  private static native void nativeInvertImage(int[] dImage, int[] sImage);

  private static native void nativeColourTwistWithEnum(int[] dImage, int[] sImage, int colourTypeEnumId);

  private static native void nativeColourTwistWithArray(int[] dImage, int[] sImage, float[][] colourTwistArray);

  private static native void nativeColourMap(String dImageFilePath, String sImageFilePath, int colourMapTypeEnumId);

  private static native void nativeFFTBandPassFilter(int[] dImage, int[] sImage, int lowPassLargeDownTo, int hiPassSmallUpTo, double toleranceOfDirection, int suppressStripesModeEnumId, boolean enhanceContrast, int saturateValue, int interpolationModeEnumId);
  
  private static native void nativeRFilter(int[] dImage, int[] sImage, float alpha, float beta, int scale);
  
  private static native void nativeMotionBlur(int[] dImage, int[] sImage, int scale, int offset, int direction, int iteration);
  
  private static native void nativeShadingRemoval(int[] dImage, int[] sImage, int blurDistance, int keepOutDistance, int direction);
}
