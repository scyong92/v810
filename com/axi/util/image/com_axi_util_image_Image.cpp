#include <jni.h>
#include "com_axi_util_image_Image.h"

#include "jniImageUtil.h"

#include "ippcore.h"
#include "ippi.h"
#include "ipps.h"

#include "sptAssert.h"

/**
 * nativeIntializeJavaDefinition
 *
 * Intialize the JNIImageData for an image that isn't aligned (allocated via Java, probably. This may change to an ipps-allocated memory block)
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Image_nativeIntializeJavaDefinition(JNIEnv *jEnv, 
                                                                                       jclass jContext, 
                                                                                       jintArray jImageDef, 
                                                                                       jobject jImageBuffer)
{
  try
  {
    JNIImageData *pImageDef = getImageDataFromJava(jEnv, jImageDef);
    Ipp32f *pImageBuffer = reinterpret_cast<Ipp32f*>(jEnv->GetDirectBufferAddress(jImageBuffer));
    
    sptAssert(pImageDef->width > 0);
    sptAssert(pImageDef->height > 0);
    
    pImageDef->address = pImageBuffer;
    pImageDef->stride = pImageDef->width * sizeof(Ipp32f);
    pImageDef->allocation = JAVA_ALLOCATED;
    releaseImageDataFromJavaModified(jEnv, jImageDef, pImageDef);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeGetByteBuffer
 * @author Patrick Lacz
 */
JNIEXPORT jobject JNICALL Java_com_axi_util_image_Image_nativeGetByteBuffer(JNIEnv *jEnv, 
		jclass jContext, jintArray jImageDef)
{
  try
  {
    JNIImageData *pImageDef = getImageDataFromJava(jEnv, jImageDef);
    
    jobject byteBuffer = jEnv->NewDirectByteBuffer(reinterpret_cast<void*>(pImageDef->address), pImageDef->height * pImageDef->stride);

    releaseImageDataFromJavaUnmodified(jEnv, jImageDef, pImageDef);
    return byteBuffer;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return NULL;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return NULL;
  }
}

/**
 * nativeGetSizeOfNativeDefinitionInInts
 * Returns the size of the native image definition structure. This is how large of an int array we should allocate
 * in Java to hold this data.
 * 
 * @author Patrick Lacz
 */
JNIEXPORT jint JNICALL Java_com_axi_util_image_Image_nativeGetSizeOfNativeDefinitionInInts(JNIEnv *jEnv, jclass jContext)
{
  try
  {
    int sizeOfJNIImageData = sizeof(JNIImageData);
    int sizeOfJavaInt = sizeof(jint);
    sptAssert(sizeOfJNIImageData % sizeOfJavaInt == 0);
    return sizeOfJNIImageData / sizeOfJavaInt;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * nativeSetImageFromArray
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Image_nativeSetImageFromArray(JNIEnv *jEnv, 
                                                                                 jclass jContext, 
                                                                                 jintArray jImageDef, 
                                                                                 jfloatArray floatArray)
{
  try
  {
    JNIImageData *pImageDef = getImageDataFromJava(jEnv, jImageDef);
    Ipp32f *pArraySource = reinterpret_cast<Ipp32f*>(jEnv->GetPrimitiveArrayCritical(floatArray, 0));
    
    sptAssert(jEnv->GetArrayLength(floatArray) == pImageDef->width * pImageDef->height);
    
    IppStatus ippResult = ippiCopy_32f_C1R(pArraySource, 
                                           pImageDef->width * sizeof(Ipp32f), 
                                           pImageDef->address, pImageDef->stride,
                                           pImageDef->getSize());
    
    // must 'release' the pResult pointer back Java for the results to be stored.
    jEnv->ReleasePrimitiveArrayCritical(floatArray, reinterpret_cast<void *>(pArraySource), JNI_ABORT);
    releaseImageDataFromJavaUnmodified(jEnv, jImageDef, pImageDef);
  
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeSetImageFromByteArray
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Image_nativeSetImageFromByteArray(JNIEnv *jEnv, 
                                                                                     jclass jContext, 
                                                                                     jintArray jImageDef, 
                                                                                     jbyteArray byteArray)
{
  try
  {
    JNIImageData *pImageDef = getImageDataFromJava(jEnv, jImageDef);
    Ipp8u *pArraySource = reinterpret_cast<Ipp8u*>(jEnv->GetPrimitiveArrayCritical(byteArray, 0));
    
    sptAssert(jEnv->GetArrayLength(byteArray) == pImageDef->width * pImageDef->height);
    
    IppStatus ippResult = ippiConvert_8u32f_C1R(pArraySource, 
                                                pImageDef->width * sizeof(Ipp8u), 
                                                pImageDef->address, 
                                                pImageDef->stride,
                                                pImageDef->getSize());
    
    // must 'release' the pResult pointer back to Java for the results to be stored.
    jEnv->ReleasePrimitiveArrayCritical(byteArray, reinterpret_cast<void *>(pArraySource), JNI_ABORT);
    releaseImageDataFromJavaUnmodified(jEnv, jImageDef, pImageDef);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * nativeCreateFloatArrayFromImage
 *
 * @author Patrick Lacz
 */
JNIEXPORT jfloatArray JNICALL Java_com_axi_util_image_Image_nativeCreateFloatArrayFromImage(JNIEnv *jEnv, 
                                                                                                jclass jContext, 
                                                                                                jintArray jImageDef, 
                                                                                                jint roiMinX, 
                                                                                                jint roiMinY, 
                                                                                                jint roiWidth, 
                                                                                                jint roiHeight)
{
  try
  {
    JNIImageData *pImageDef = getImageDataFromJava(jEnv, jImageDef);
    
    // the result array is the size of the ROI.
    jfloatArray resultArray = jEnv->NewFloatArray(roiWidth * roiHeight);
    Ipp32f *pResult = reinterpret_cast<Ipp32f*>(jEnv->GetPrimitiveArrayCritical(resultArray, 0));
    
    IppiSize resultSize = { roiWidth, roiHeight };
    
    IppStatus ippResult = ippiCopy_32f_C1R(pImageDef->addressAt(roiMinX, roiMinY), 
                                           pImageDef->stride,
                                           pResult, 
                                           roiWidth * sizeof(Ipp32f), 
                                           resultSize);
    
    // must 'release' the pResult pointer back Java for the results to be stored.
    jEnv->ReleasePrimitiveArrayCritical(resultArray, reinterpret_cast<void*>(pResult), 0);
    releaseImageDataFromJavaUnmodified(jEnv, jImageDef, pImageDef);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
    return resultArray;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return NULL;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return NULL;
  }
}


/**
 * nativeCreateByteArrayFromImage
 *
 * @author Patrick Lacz
 */
JNIEXPORT jbyteArray JNICALL Java_com_axi_util_image_Image_nativeCreateByteArrayFromImage(JNIEnv *jEnv, 
                                                                                              jclass jContext, 
                                                                                              jintArray jImageDef, 
                                                                                              jint roiMinX, 
                                                                                              jint roiMinY, 
                                                                                              jint roiWidth, 
                                                                                              jint roiHeight)
{
  try
  {
    JNIImageData *pImageDef = getImageDataFromJava(jEnv, jImageDef);
    
    // the result array is the size of the ROI.
    jbyteArray resultArray = jEnv->NewByteArray(roiWidth * roiHeight);
    Ipp8u *pResult = reinterpret_cast<Ipp8u*>(jEnv->GetPrimitiveArrayCritical(resultArray, 0));
    
    IppiSize resultSize = { roiWidth, roiHeight };
  
    IppStatus ippResult = ippiConvert_32f8u_C1R(pImageDef->addressAt(roiMinX, roiMinY), 
                                                pImageDef->stride,
                                                pResult,
                                                roiWidth*sizeof(Ipp8u),
                                                resultSize,
                                                ippRndNear);
    
    // must 'release' the pResult pointer back Java for the results to be stored.
    jEnv->ReleasePrimitiveArrayCritical(resultArray, reinterpret_cast<void*>(pResult), 0);
    releaseImageDataFromJavaUnmodified(jEnv, jImageDef, pImageDef);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
    return resultArray;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return NULL;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return NULL;
  }
}


/**
 * nativeConvertByteToFloatBuffer
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Image_nativeConvertByteToFloatImage(JNIEnv *jEnv, 
                                                                                       jclass jContext, 
                                                                                       jobject jByteBuffer, 
                                                                                       jint bufferWidth, 
                                                                                       jint roiOffset, 
                                                                                       jintArray jDestImage)
{
  try
  {
    // Get direct access to the image data from the Image buffer.
    Ipp8u *pByteImage = reinterpret_cast<Ipp8u*>(jEnv->GetDirectBufferAddress(jByteBuffer));
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    
    sptAssert(pByteImage != NULL);
    
    IppStatus ippResult = ippiConvert_8u32f_C1R(pByteImage + roiOffset, 
                                                bufferWidth*sizeof(Ipp8u),
                                                pDestImage->address, 
                                                pDestImage->stride,
                                                pDestImage->getSize());
    
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}
