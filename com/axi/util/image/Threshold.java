package com.axi.util.image;


import java.util.*;

import com.axi.util.*;
import java.awt.Point;

/**
 * Threshold class
 *
 * This class holds functions to perform various threshold-related functions on
 * images. It provides a variety of flavors of threshold and clamping operations,
 * as well as histogram and counting methods. <p>
 *
 * Any operation that is primarily based on classifying pixels into bins based
 * on intensity should be placed here.
 *
 * @author Patrick Lacz
 */
public class Threshold
{
  private static int WHITE_PIXEL_THRESHOLD = 128;

  private static int VOID_PIXEL_THRESHOLD = 175;  //55
  
  private static TimerUtil _timerUtil = new TimerUtil();
  private static boolean _logTimestamp = false;

  /**
   * @author Patrick Lacz
   */
  public Threshold()
  {
  }

  /**
   * The Intel IPP is used to implement the methods in this class.
   *
   * @author Patrick Lacz
   */
  static
  {
    System.loadLibrary("nativeImageUtil");
  }

  /**
   * Assigns every pixel in the image lower than the lowerBound to the lowerBound, and pixels
   * higher than the upperBound to the upperBound.  Commonly, lowerBound is zero and upperBound is one or 255.
   *
   * @todo : Create Regression Tests
   * @author Patrick Lacz
   */
  public static void clamp(Image image, float lowerBound, float upperBound)
  {
    threshold(image, RegionOfInterest.createRegionFromImage(image), lowerBound, lowerBound, upperBound, upperBound);
  }

  /**
   * Assigns every pixel in the region of interest lower than the lowerBound to the lowerBound, and pixels
   * higher than the upperBound to the upperBound.  Commonly, lowerBound is zero and upperBound is one or 255.
   *
   * @todo : Create Regression Tests
   * @author Patrick Lacz
   */
  public static void clamp(Image image, RegionOfInterest roi, float lowerBound, float upperBound)
  {
    threshold(image, roi, lowerBound, lowerBound, upperBound, upperBound);
  }

  /**
   * Applies a thresholding operation to every pixel in the image. If the pixel value is less
   * than thresholdLessThan, the pixel is assigned to valueLessThan. Likewise if the pixel value
   * is greater than thresholdGreaterThan, the pixel is assigned to valueGreaterThan.
   *
   * @author Patrick Lacz
   */
  public static void threshold(Image image, float thresholdLessThan, float valueLessThan,
                               float thresholdGreaterThan, float valueGreaterThan)
  {
    threshold(image, RegionOfInterest.createRegionFromImage(image), thresholdLessThan, valueLessThan,
              thresholdGreaterThan, valueGreaterThan);
  }

  /**
   * Applies a thresholding operation to every pixel in the region of interest. If the pixel value is less
   * than thresholdLessThan, the pixel is assigned to valueLessThan. Likewise if the pixel value
   * is greater than thresholdGreaterThan, the pixel is assigned to valueGreaterThan.
   *
   * @author Patrick Lacz
   */
  public static void threshold(Image image, RegionOfInterest roi, float thresholdLessThan,
                               float valueLessThan, float thresholdGreaterThan,
                               float valueGreaterThan)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));
    Assert.expect(thresholdLessThan <= thresholdGreaterThan);
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    nativeThresholdInPlace(image.getNativeDefinition(), roi.getMinX(), roi.getMinY(),
                    roi.getWidth(), roi.getHeight(), thresholdLessThan, valueLessThan, thresholdGreaterThan,
                    valueGreaterThan);
  }

  /**
   * Return a threshold to divide the data in the histogram into two groups.  This is based on the "isodata" technique
   * used in ImageJ (Ridler and Calvard, 1978).  This code was leveraged from the ImageJ software (free, no licensing required).
   *
   * @author Peter Esbensen
   */
  public static int getAutoThreshold(int[] histogram)
  {
    Assert.expect(histogram != null);
    Assert.expect(histogram.length > 0);

    int level;
    int maxValue = histogram.length - 1;
    double result, sum1, sum2, sum3, sum4;

    // find the min and max bins of the histogram
    int count0 = histogram[0];
    histogram[0] = 0; //set to zero so erased areas aren't included
    int countMax = histogram[maxValue];
    histogram[maxValue] = 0;
    int min = 0;
    while ((histogram[min] == 0) && (min < maxValue))
    {
      min++;
    }
    int max = maxValue;
    while ((histogram[max] == 0) && (max > 0))
    {
      max--;
    }

    // if this is a weird histogram, like a totally flat one, just return the midpoint
    if (min >= max)
    {
      histogram[0] = count0;
      histogram[maxValue] = countMax;
      level = histogram.length / 2;
      return level;
    }

    // now search for the best threshold
    int movingIndex = min;
    //int inc = Math.min(max/40, 1);

    // search for the index that puts the threshold at the midpoint between the means of the two classes
    do
    {
      sum1 = sum2 = sum3 = sum4 = 0.0;
      for (int i = min; i <= movingIndex; i++)
      {
        sum1 += i * histogram[i];
        sum2 += histogram[i];
      }
      for (int i = (movingIndex + 1); i <= max; i++)
      {
        sum3 += i * histogram[i];
        sum4 += histogram[i];
      }
      result = (sum1 / sum2 + sum3 / sum4) / 2.0;
      movingIndex++;
    } while ((movingIndex+1)<=result && movingIndex<max-1);

    histogram[0] = count0;
    histogram[maxValue] = countMax;
    level = (int) Math.round(result);
    return level;
  }

  /**
   * Compute a histogram using the given number of evenly sized bins.
   * The first bin is assumed to have a lower threshold of 0.0 and the
   * last bin is assumed to have a high threshold of 255.0.
   *
   * @author Patrick Lacz
   */
  public static int[] histogram(Image image, RegionOfInterest roi, int bins)
  {
    return histogram(image, roi, bins, 0.0f, 255.0f);
  }

  /**
   * Computes a histogram of a the "donut ring" of the specified outer and inner
   * ROIs.
   * The first bin is assumed to have a lower threshold of 0.0 and the
   * last bin is assumed to have a high threshold of 255.0.
   *
   * @todo : Create Regression Tests
   * @author Matt Wharton
   */
  public static int[] histogram(Image image, RegionOfInterest outerRoi, RegionOfInterest innerRoi, int bins)
  {
    Assert.expect(image != null);
    Assert.expect(outerRoi != null);
    Assert.expect(innerRoi != null);
    Assert.expect(outerRoi.contains(innerRoi));

    // We need to make a histogram of the outer region and the inner region and then take the
    // difference of the two.  This will represent a histogram of the area that's contained by the
    // outer ROI but not the inner.
    int[] outerHistogramBins = histogram(image, outerRoi, 255);
    int[] innerHistogramBins = histogram(image, innerRoi, 255);
    Assert.expect(outerHistogramBins.length == 255);
    Assert.expect(innerHistogramBins.length == 255);

    int[] ringHistogramBins = new int[outerHistogramBins.length];
    for (int i = 0; i < ringHistogramBins.length; ++i)
    {
      ringHistogramBins[i] = Math.max(0, outerHistogramBins[i] - innerHistogramBins[i]);
    }

    return ringHistogramBins;
  }

  /**
   * Compute a histogram using an indicated number of evenly sized bins.
   * The lower and upper bounds of the range can be set; greylevels outside
   * of this range will not be added to any bin.
   *
   * @todo : Create Explicit Regression Tests (Implicitly tested via histogram(Image, ROI, int)
   * @author Patrick Lacz
   */
  public static int[] histogram(Image image, RegionOfInterest roi, int bins, float floor, float ceiling)
  {
    Assert.expect(ceiling > floor);
    Assert.expect(bins > 1);

    float binThresholds[] = new float[bins+1];
    float difference = ceiling-floor;
    float step = difference/bins;
    binThresholds[0] = floor;
    for (int i = 1 ; i <= bins ; ++i)
    {
      binThresholds[i] = floor + step*i;
    }

    return histogram(image, roi, binThresholds);
  }


  /**
   * Compute a histogram with explicitly sized bins.
   *
   * The bins are defined by setting thresholds in the bin thresholds array.
   * The first value is the lower bound for the first bin, and the last value
   * is the upper bound of the last bin. Otherwise the ith value is a ceiling
   * for the (i-1)th bin and a floor for the ith bin.
   *
   * @todo : Create Regression Tests
   * @author Patrick Lacz
   * @param binThresholds float[] The array holding the greylevel thresholds.
   * @return int[] The totals for each bin.
   */
  public static int[] histogram(Image image, RegionOfInterest roi, float[] binThresholds)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(binThresholds != null);
    Assert.expect(binThresholds.length > 2);
    Assert.expect(roi.fitsWithinImage(image));
    Assert.expect(roi.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR));
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    // add a little bit to the last threshold -- the ceiling is exclusive.
    binThresholds[binThresholds.length-1] += 0.00001;

    return nativeHistogramRange(image.getNativeDefinition(),
                                roi.getMinX(),
                                roi.getMinY(),
                                roi.getWidth(),
                                roi.getHeight(),
                                binThresholds);
  }

  /**
   * Returns the total number of pixel values contained in the specified histogram.
   *
   * @todo : Create Regression Tests
   * @author Matt Wharton
   */
  public static int getHistogramPixelCount(int[] histogramBins)
  {
    Assert.expect(histogramBins != null);

    int pixelCount = 0;
    for (int bin : histogramBins)
    {
      pixelCount += bin;
    }

    return pixelCount;
  }

  /**
   * @author Patrick Lacz
   */
  public static float getPixelValueAtPercentileUsingHistogram(int[] histogramBins, float percentileValue, float minValue, float maxValue)
  {
    Assert.expect(histogramBins != null);
    Assert.expect(percentileValue >= 0.f && percentileValue <= 100.0f);
    Assert.expect(maxValue > minValue);

    int pixelCount = getHistogramPixelCount(histogramBins);

    if (pixelCount <= 1)
      return (minValue + maxValue)/2;

    float exactThreshold = (percentileValue / 100.f) * (pixelCount - 1) + 1;
    float floorThreshold = (float)Math.floor(exactThreshold);

    float distanceBetweenBins = (maxValue - minValue) / (histogramBins.length - 1);

    int currentBin = 0;
    int nextBin = 0;
    float pixelsSeen = 0;
    do
    {
      currentBin = nextBin++;
      int numberOfPixelsInCurrentBin = histogramBins[currentBin];
      pixelsSeen += numberOfPixelsInCurrentBin;
    }
    while (pixelsSeen < floorThreshold && nextBin < histogramBins.length);

    // for some reason we have ran over. If percent <= 100, this should never occur
    if (nextBin == histogramBins.length && pixelsSeen <= floorThreshold)
    {
      return maxValue;
    }

    // the simple case - the percentile lies within the current bin
    float currentBinValue = currentBin * distanceBetweenBins + minValue;
    if (pixelsSeen >= exactThreshold)
    {
      return currentBinValue;
    }

    // the complex case - the percentile lies between two bins

    // find the next bin with a sample in it.
    int binOfNextSample = currentBin + 1;

    while (histogramBins[binOfNextSample] == 0)
    {
      binOfNextSample++;
    }

    float nextBinValue = binOfNextSample * distanceBetweenBins + minValue;

    return (exactThreshold - floorThreshold) * (nextBinValue - currentBinValue) + currentBinValue;
  }

  /**
   * Estimates the pixel value at the specified percentile in the given histogram.
   * Assumes a histogram with uniformly sized bins ranging from 0 to 255 (inclusive).
   *
   * NOTE: This is largely adapted from the old FindHistogramBin routine in the old 5DX code.
   *
   * @author Matt Wharton
   */
  public static float estimatePixelValueAtPercentileUsingHistogram(int[] histogramBins, float percentileValue)
  {
    Assert.expect(histogramBins != null);
    Assert.expect(histogramBins.length == 256);
    Assert.expect((percentileValue >= 0f) && (percentileValue <= 1f));

    return estimatePixelValueAtPercentileUsingHistogram(histogramBins, percentileValue, 0, 255);
  }

  /**
   * Estimates the pixel value at the specified percentile in the given histogram.
   * Assumes a histogram with uniformly sized bins ranging from the specified minimum and
   * maximum values (inclusive).
   *
   * NOTE: This is largely adapted from the old FindHistogramBin routine in the old 5DX code.
   *
   * @author Matt Wharton
   */
  public static float estimatePixelValueAtPercentileUsingHistogram(
    int[] histogramBins,
    float percentileValue,
    int minPixelValue,
    int maxPixelValue)
  {
    Assert.expect(histogramBins != null);
    Assert.expect(histogramBins.length >= 1);
    Assert.expect((percentileValue >= 0f) && (percentileValue <= 1f));
    Assert.expect(minPixelValue >= 0);
    Assert.expect(maxPixelValue >= 0);
    Assert.expect(minPixelValue <= maxPixelValue);

    float binSize = (float) (maxPixelValue - minPixelValue) / (histogramBins.length - 1);
    int pixelCount = getHistogramPixelCount(histogramBins);
    float targetPixelSum = percentileValue * pixelCount;
    int runningPixelSum = 0;
    int bin = 0;
    float pixelValueForCurrentBin = 0f;
    for (bin = 0; bin < histogramBins.length; ++bin)
    {
      pixelValueForCurrentBin = minPixelValue + (bin * binSize);
      Assert.expect(pixelValueForCurrentBin <= maxPixelValue);

      runningPixelSum += histogramBins[bin];

      if (runningPixelSum > targetPixelSum)
      {
        break;
      }
    }

    Assert.expect((pixelValueForCurrentBin < maxPixelValue) && (runningPixelSum > targetPixelSum));

    int previousPixelSum = 0;
    if (pixelValueForCurrentBin > minPixelValue)
    {
      previousPixelSum = runningPixelSum - histogramBins[bin];
    }

    float pixelValueAtPercentile = 0f;
    if (runningPixelSum == previousPixelSum)
    {
      pixelValueAtPercentile = pixelValueForCurrentBin;
    }
    else
    {
      float deltaGray = ((targetPixelSum - previousPixelSum) / (runningPixelSum - previousPixelSum)) * binSize;
      pixelValueAtPercentile = pixelValueForCurrentBin + deltaGray;
    }

    return pixelValueAtPercentile;
  }

  /**
   * Counts the number of pixels with a value between (inclusive) the lower and upper bounds.
   *
   * @author Patrick Lacz
   */
  public static int countPixelsInRange(Image image, float lowerBound, float upperBound)
  {
    return countPixelsInRange(image, RegionOfInterest.createRegionFromImage(image), lowerBound, upperBound);
  }

  /**
   * Counts the number of pixels with a value between (inclusive) the lower and upper bounds within the region of interest.
   *
   * @author Patrick Lacz
   */
  public static int countPixelsInRange(Image image, RegionOfInterest roi, float lowerBound, float upperBound)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));
    Assert.expect(roi.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR));
    Assert.expect(lowerBound < upperBound);
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    return nativeCountPixelsInRange(image.getNativeDefinition(), roi.getMinX(), roi.getMinY(),
                                    roi.getWidth(), roi.getHeight(), lowerBound, upperBound);
  }


  /**
   * Creates a set of pixels that have their intensity greater than the given value.
   * This is useful in conjunction with edge detection or other feature isolation schemes.
   *
   * @author Patrick Lacz
   */
  public static List<ImageCoordinate> createSetOfPixelsWithinRange(Image image, RegionOfInterest regionOfInterest, float minimumIntensity, float maximumIntensity)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(image));
    Assert.expect(regionOfInterest.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR));

    List<ImageCoordinate> setOfOnPixels = new ArrayList<ImageCoordinate>();

    int maxX = regionOfInterest.getMaxX();
    int maxY = regionOfInterest.getMaxY();
    for (int y = regionOfInterest.getMinX() ; y < maxY ; ++y)
    {
      for (int x = regionOfInterest.getMinX() ; x < maxX ; ++x)
      {
        float intensity = image.getPixelValue(x, y);
        if (intensity >= minimumIntensity && intensity <= maximumIntensity)
          setOfOnPixels.add(new ImageCoordinate(x, y));
      }
    }

    return setOfOnPixels;
  }
  
  /**
   * Creates an float array with pixels that have their intensity greater than the given value.
   * @author Siew Yeng
   */
  public static float[] createArrayOfPixelsWithinRange(Image image, RegionOfInterest regionOfInterest, float minimumIntensity, float maximumIntensity)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(image));
    Assert.expect(regionOfInterest.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR));

    float[] arrayOfPixels = new float[regionOfInterest.getWidth() * regionOfInterest.getHeight()];

    int maxX = regionOfInterest.getMaxX();
    int maxY = regionOfInterest.getMaxY();
    for (int y = regionOfInterest.getMinX() ; y < maxY ; ++y)
    {
      for (int x = regionOfInterest.getMinX() ; x < maxX ; ++x)
      {
        float intensity = image.getPixelValue(x, y);
        if (intensity >= minimumIntensity && intensity <= maximumIntensity)
          arrayOfPixels[y * regionOfInterest.getWidth() + x] = 255;
        else
          arrayOfPixels[y * regionOfInterest.getWidth() + x] = 0;
      }
    }

    return arrayOfPixels;
  }

  /**
   * Returns <pre>null</pre> if there are no matches.
   * @author Patrick Lacz
   */
  public static Pair<ImageCoordinate, Float> searchForPixelInRange(Image image, RegionOfInterest regionOfInterest, float minimumIntensity, float maximumIntensity)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);

    int resultLocation[] = new int[2];

    float resultIntensity = nativeSearchForFirstPixelInRange(image.getNativeDefinition(),
                                                    regionOfInterest.getMinX(), regionOfInterest.getMinY(),
                                                    regionOfInterest.getWidth(), regionOfInterest.getHeight(),
                                                    regionOfInterest.getMinX(), regionOfInterest.getMinY(),
                                                    minimumIntensity, maximumIntensity, resultLocation);
    if (resultLocation[0] == -1 || resultLocation[1] == -1)
      return null;

    Pair<ImageCoordinate, Float> result = new Pair<ImageCoordinate, Float>(new ImageCoordinate(resultLocation[0], resultLocation[1]), resultIntensity);
    return result;
  }

  /**
   * Best used in combination with the previous method. If you use only this method, you will never search the pixel you start on.
   * Returns <pre>null</pre> if there are no matches.
   * @author Patrick Lacz
   */
  public static Pair<ImageCoordinate, Float> searchForNextPixelInRange(Image image, RegionOfInterest regionOfInterest, float minimumIntensity, float maximumIntensity, ImageCoordinate startAfterCoordinate)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);

    int resultLocation[] = new int[2];

    int startX = startAfterCoordinate.getX();
    int startY = startAfterCoordinate.getY();

    if (regionOfInterest.containsPixel(startAfterCoordinate) == false)
    {
      if (startY <= regionOfInterest.getMaxY())
      {
        ++startY;
        startX = regionOfInterest.getMinX();
      }
      else
      {
        return null;
      }
    }

    float resultIntensity = nativeSearchForFirstPixelInRange(image.getNativeDefinition(),
                                                    regionOfInterest.getMinX(), regionOfInterest.getMinY(),
                                                    regionOfInterest.getWidth(), regionOfInterest.getHeight(),
                                                    startX, startY,
                                                    minimumIntensity, maximumIntensity, resultLocation);
    if (resultLocation[0] == -1 || resultLocation[1] == -1)
      return null;

    Pair<ImageCoordinate, Float> result = new Pair<ImageCoordinate, Float>(new ImageCoordinate(resultLocation[0], resultLocation[1]), resultIntensity);
    return result;
  }

  /**
   * @author Patrick Lacz
   */
  private static native int[] nativeHistogramRange(
      int[] sourceImage, int roiMinX, int roiMinY,
      int roiWidth, int roiHeight,
      float[] binThresholds);

  /**
   * @author Patrick Lacz
   */
  private static native void nativeThresholdInPlace(
      int[] sourceImage, int roiMinX, int roiMinY,
      int roiWidth, int roiHeight,
      float thresholdLT, float valueLT,
      float thresholdGT, float valueGT);

  /**
   * @author Patrick Lacz
   */
  private native static int nativeCountPixelsInRange(int[] sourceImage, int roiMinX, int roiMinY, int roiWidth, int roiHeight, float lowerBound, float upperBound);

  /**
   * @author Patrick Lacz
   */
  private native static float nativeSearchForFirstPixelInRange(int[] sourceImage, int roiMinX, int roiMinY, int roiWidth, int roiHeight,
                                                               int startX, int startY,
                                                               float thresholdMin, float thresholdMax, int[] jReturnXY);


   /**
   * @author Jack Hwee
   */
  private static boolean isWhite(Image image, int posX, int posY)
  {
   Assert.expect(image != null);

    int brightness = (int)image.getPixelValue(posX, posY);

    return (brightness > 0) && (brightness < VOID_PIXEL_THRESHOLD + 0.5f);
  }

    /**
   * @author Jack Hwee
   */
  private static boolean isWhiteFloodFill(Image image, int posX, int posY)
  {
   Assert.expect(image != null);

    int brightness = (int)image.getPixelValue(posX, posY);

    return brightness > WHITE_PIXEL_THRESHOLD;
  }

   /**
   * Count the pixels for individual void
   * @author Jack Hwee
   */
  public static int individualVoid(Image bimg, float individualVoidAreaFailThreshold, float numberOfPixelsTested)
  {
    Assert.expect(bimg != null);
    Assert.expect(individualVoidAreaFailThreshold >= 0);
    Assert.expect(numberOfPixelsTested > 0);

    boolean[][] painted = new boolean[bimg.getHeight()][bimg.getWidth()];
 
    int max = 0;

    for (int i = 0; i < bimg.getHeight(); i++)
    {
      for (int j = 0; j < bimg.getWidth(); j++)
      {

        if (isWhite(bimg, j, i) && painted[i][j] == false)
        {
          Queue<Point> queue = new LinkedList<Point>();

          queue.add(new Point(j, i));

          int pixelCount = 0;

          while (queue.isEmpty() == false)
          {

            Point p = queue.remove();

            if ((p.x >= 0) && (p.x < bimg.getWidth() && (p.y >= 0) && (p.y < bimg.getHeight())))
            {
              if (painted[p.y][p.x] == false && isWhite(bimg, p.x, p.y))
              {

                painted[p.y][p.x] = true;

                pixelCount++;

                queue.add(new Point(p.x + 1, p.y));

                queue.add(new Point(p.x - 1, p.y));

                queue.add(new Point(p.x, p.y + 1));

                queue.add(new Point(p.x, p.y - 1));

                queue.add(new Point(p.x + 1, p.y + 1));

                queue.add(new Point(p.x - 1, p.y + 1));

                queue.add(new Point(p.x + 1, p.y - 1));

                queue.add(new Point(p.x - 1, p.y - 1));

              }
            }
          }

       
      //    if (checkPixel(pixelCount, individualVoidAreaFailThreshold, numberOfPixelsTested))
      //    {
            if (pixelCount > max)
            {
              max = pixelCount;
            }
      //    }
          queue.clear();
          queue = null;
        }
      }
    }
    painted = null;
    
    return max;
  }

   /**
   * Count the pixels for floodFill individual void
   * @author Jack Hwee
   */
  public static int floodFillIndividualVoid(Image bimg, float individualVoidAreaFailThreshold, float numberOfPixelsTested)
  {
    Assert.expect(bimg != null);
    Assert.expect(individualVoidAreaFailThreshold >= 0);
    Assert.expect(numberOfPixelsTested > 0);

    boolean[][] painted = new boolean[bimg.getHeight()][bimg.getWidth()];
   
    int max = 0;

    for (int i = 0; i < bimg.getHeight(); i++)
    {
      for (int j = 0; j < bimg.getWidth(); j++)
      {
        if (isWhiteFloodFill(bimg, j, i) && painted[i][j] == false)
        {
          Queue<Point> queue = new LinkedList<Point>();

          queue.add(new Point(j, i));
            
          int pixelCount = 0;
   
          while (queue.isEmpty() == false)
          {
            Point p = queue.remove();
            
            if ((p.x >= 0) && (p.x < bimg.getWidth() && (p.y >= 0) && (p.y < bimg.getHeight())))
            {                       
              if (painted[p.y][p.x] == false && isWhiteFloodFill(bimg, p.x, p.y))
              {
                painted[p.y][p.x] = true;

                pixelCount++;

                queue.add(new Point(p.x + 1, p.y));

                queue.add(new Point(p.x - 1, p.y));

                queue.add(new Point(p.x, p.y + 1));

                queue.add(new Point(p.x, p.y - 1));

                queue.add(new Point(p.x + 1, p.y + 1));

                queue.add(new Point(p.x - 1, p.y + 1));

                queue.add(new Point(p.x + 1, p.y - 1));

                queue.add(new Point(p.x - 1, p.y - 1));                               
                          
              }            
            }
          }          
    
          if (pixelCount > max)
          {
            max = pixelCount;       
          }

          queue.clear();
          queue = null;
        }
      }
    }
    painted = null;

    return max;
  }
  
   /**
   * Count the pixels for largest floodFill individual void
   * @author Jack Hwee
   */
  public static float[] floodFillLargestIndividualVoid(Image bimg)
  {
    Assert.expect(bimg != null);
   
    boolean[][] painted = new boolean[bimg.getHeight()][bimg.getWidth()];
    
    int arraySize = bimg.getWidth() * bimg.getHeight();
    
    float [] imageArray;// = new float[bimg.getWidth() * bimg.getHeight()];
    
    float [] largestVoidImageArray = new float[bimg.getWidth() * bimg.getHeight()];
   
    int max = 0;

    for (int i = 0; i < bimg.getHeight(); i++)
    {
      for (int j = 0; j < bimg.getWidth(); j++)
      {
        if (isWhiteFloodFill(bimg, j, i) && painted[i][j] == false)
        {
          Queue<Point> queue = new LinkedList<Point>();

          queue.add(new Point(j, i));
            
          int pixelCount = 0;
   
          imageArray = new float[bimg.getWidth() * bimg.getHeight()];
    
          while (queue.isEmpty() == false)
          {
            Point p = queue.remove();
            
            if (((p.x >= 0) && p.x < bimg.getWidth()) && ((p.y >= 0) && (p.y < bimg.getHeight())))
            {         
              int arrayPosition = (p.y * bimg.getWidth()) + p.x;
              
              if (painted[p.y][p.x] == false && isWhiteFloodFill(bimg, p.x, p.y))
              {
                painted[p.y][p.x] = true;

                pixelCount++;

                queue.add(new Point(p.x + 1, p.y));

                queue.add(new Point(p.x - 1, p.y));

                queue.add(new Point(p.x, p.y + 1));

                queue.add(new Point(p.x, p.y - 1));

                queue.add(new Point(p.x + 1, p.y + 1));

                queue.add(new Point(p.x - 1, p.y + 1));

                queue.add(new Point(p.x + 1, p.y - 1));

                queue.add(new Point(p.x - 1, p.y - 1));                               
                
                imageArray[arrayPosition] = 255;                 
              }            
            }
          }          
    
          if (pixelCount > max)
          {
            max = pixelCount;       
          
            largestVoidImageArray = ArrayUtil.copy(imageArray, 0, arraySize);
          }

          imageArray = null;
          queue.clear();
          queue = null;
        }
      }
    }
    painted = null;

    return largestVoidImageArray;
  }
  
  /**
   * Count the pixels for largest floodFill individual void within the neighbour size
   * @edited by Siew Yeng
   */
  public static float[] floodFillLargestIndividualVoid(Image bimg, int neighbourSize)
  {
    Assert.expect(bimg != null);
   
    if(_logTimestamp)
    {
      _timerUtil.reset();
      _timerUtil.start();
    }
    
    boolean[][] painted = new boolean[bimg.getHeight()][bimg.getWidth()];
    
    int arraySize = bimg.getWidth() * bimg.getHeight();
    
    float [] imageArray;// = new float[bimg.getWidth() * bimg.getHeight()];
    
    float [] largestVoidImageArray = new float[bimg.getWidth() * bimg.getHeight()];
   
    int max = 0;

    Point[] pointArray = new Point[bimg.getWidth()*bimg.getHeight()];
    
    //Siew Yeng - create all points for each pixel
    int w = 0;
    int h = 0;
    while(w < bimg.getWidth() && h < bimg.getHeight())
    {
      pointArray[h*bimg.getWidth()+w] = new Point(w,h);
      w++;
      
      if(w == bimg.getWidth())
      {
        w = 0;
        h++;
      }
    }

    for (int i = 0; i < bimg.getHeight(); i++)
    {
      for (int j = 0; j < bimg.getWidth(); j++)
      {
        if (isWhiteFloodFill(bimg, j, i) && painted[i][j] == false)
        {
          //Siew Yeng - XCR-2560 - fix production hang when gap distance is set to 30mils
          // change from LinkedList to LinkedHashSet to make sure the points in the queue
          // are always unique. This is to avoid adding too many points(duplicate points) 
          // into the list that will cause hang.
          //Queue<Point> queue = new LinkedList<Point>();
          LinkedHashSet<Point> queue = new LinkedHashSet<Point>();

          queue.add(pointArray[i*bimg.getWidth()+j]);
            
          int pixelCount = 0;
   
          imageArray = new float[bimg.getWidth() * bimg.getHeight()];
    
          while (queue.isEmpty() == false)
          {
            //Siew Yeng - LinkedHashSet is not a Queue therefore need to use iterator to get the 1st elements in the list.
            Point p = queue.iterator().next();
            queue.remove(p);
            
            if (((p.x >= 0) && p.x < bimg.getWidth()) && ((p.y >= 0) && (p.y < bimg.getHeight())))
            {         
              int arrayPosition = (p.y * bimg.getWidth()) + p.x;
              
              //Siew Yeng - XCR-2559
              boolean isWhiteFloodfill = isWhiteFloodFill(bimg, p.x, p.y);
              if (painted[p.y][p.x] == false && isWhiteFloodfill)
              {
                painted[p.y][p.x] = true;

                pixelCount++;

                //Siew Yeng - XCR-2210 Combine two voids if the two voids are within an acceptable gap distance
                int n = p.y + neighbourSize;
                int m = p.x + neighbourSize;
                int mMinimum = p.x - neighbourSize;
                int nMinimum = p.y - neighbourSize;
                
                while(m >= mMinimum && n >= nMinimum)
                {                  
                  if((m >=0 && m < bimg.getWidth()) && (n >= 0 && n < bimg.getHeight()))
                  {
                    int pointPosition = n * bimg.getWidth() + m;

                    if((m != p.x && n != p.y) &&
                       //queue.contains(pointArray[pointPosition]) == false && //Siew Yeng - XCR-2559 - remove this as this will cause hang
                       painted[n][m] == false &&
                       Math.round(Math.ceil(p.distance(m, n))) <= neighbourSize)
                      queue.add(pointArray[pointPosition]);
                  }
                  
                  m--;
                  if(m < mMinimum)
                  {
                    m = p.x + neighbourSize;
                    n--;
                  }
                }                
                imageArray[arrayPosition] = 255;                 
              } 
              else if(painted[p.y][p.x] == false && isWhiteFloodfill == false) //Siew Yeng - XCR-2559
              {
                painted[p.y][p.x] = true;
              }
            }
          }
          
          if (pixelCount > max)
          {
            max = pixelCount;       
          
            largestVoidImageArray = ArrayUtil.copy(imageArray, 0, arraySize);
          }

          imageArray = null;
          queue.clear();
          queue = null;
        }
      }
    }
    
    painted = null;
    pointArray = null;

    if(_logTimestamp)
    {
      _timerUtil.stop();
      System.out.println("Timestamp for FloodFill Largest Void with gap distance : " + _timerUtil.getElapsedTimeInMillis());
    }
     
    return largestVoidImageArray;
  }

   /**
   * @author Jack Hwee
   */
  public static boolean checkPixel(int pixelCount, float individualVoidAreaFailThreshold, float numberOfPixelsTested)
  {
    Assert.expect(pixelCount >= 0);
    Assert.expect(individualVoidAreaFailThreshold >= 0);
    Assert.expect(numberOfPixelsTested > 0);

    float individualVoidPercent = 100.f * (float) pixelCount / numberOfPixelsTested;

    return individualVoidPercent > individualVoidAreaFailThreshold;
  }
  
    /**
   * Count the pixels for insufficient area
   * @author Jack Hwee
   */
  public static float[] floodFillInsufficient(Image bimg)
  {
    Assert.expect(bimg != null);
   
    boolean[][] painted = new boolean[bimg.getHeight()][bimg.getWidth()];
    
    int arraySize = bimg.getWidth() * bimg.getHeight();
    
    float [] imageArray = new float[bimg.getWidth() * bimg.getHeight()];
    
    float [] largestVoidImageArray = new float[bimg.getWidth() * bimg.getHeight()];
   
    for (int i = 0; i < bimg.getHeight(); i++)
    {
      for (int j = 0; j < bimg.getWidth(); j++)
      {
        if (isWhiteFloodFill(bimg, j, i) && painted[i][j] == false)
        {
          Queue<Point> queue = new LinkedList<Point>();

          queue.add(new Point(j, i));
            
          int pixelCount = 0;
   
          //imageArray = new float[bimg.getWidth() * bimg.getHeight()];
          
          boolean isInsufficientArea = false;
        
          while (queue.isEmpty() == false)
          {
            Point p = queue.remove();
            
            if ((p.x >= 0) && (p.x < bimg.getWidth() && (p.y >= 0) && (p.y < bimg.getHeight())))
            {         
              int arrayPosition = (p.y * bimg.getWidth()) + p.x;
              
              if (painted[p.y][p.x] == false && isWhiteFloodFill(bimg, p.x, p.y))
              {
                painted[p.y][p.x] = true;

                pixelCount++;

                queue.add(new Point(p.x + 1, p.y));

                queue.add(new Point(p.x - 1, p.y));

                queue.add(new Point(p.x, p.y + 1));

                queue.add(new Point(p.x, p.y - 1));

                queue.add(new Point(p.x + 1, p.y + 1));

                queue.add(new Point(p.x - 1, p.y + 1));

                queue.add(new Point(p.x + 1, p.y - 1));

                queue.add(new Point(p.x - 1, p.y - 1));                               
                
                imageArray[arrayPosition] = 255; 
                
//                if (arrayPosition == 0 || arrayPosition == bimg.getWidth() - 1 
//                   || arrayPosition == (bimg.getWidth() * bimg.getHeight()) - bimg.getWidth() 
//                   || arrayPosition == (bimg.getWidth() * bimg.getHeight()) - 1)
//                  isInsufficientArea = true;  
                
                if (p.x == 0 || p.y == 0 || p.x == bimg.getWidth() - 1 || p.y == bimg.getHeight() - 1)
                     isInsufficientArea = true; 
              }     
            }
          }          
     
          if (isInsufficientArea == true)
            largestVoidImageArray = ArrayUtil.copy(imageArray, 0, arraySize);
          else
          {
            imageArray = null;
            imageArray = new float[bimg.getWidth() * bimg.getHeight()];
            imageArray = ArrayUtil.copy(largestVoidImageArray, 0, arraySize);
          }
      
          isInsufficientArea = false;

          queue.clear();
          queue = null;
        }
      }
    }

    return largestVoidImageArray;
  }
  
    /**
   * @author Jack Hwee
   */
  private static boolean isBlackFloodFill(Image image, int posX, int posY)
  {
   Assert.expect(image != null);

    int brightness = (int)image.getPixelValue(posX, posY);

    return brightness < WHITE_PIXEL_THRESHOLD;
  }
  
     /**
   * Count the pixels for solder pad area
   * @author Jack Hwee
   */
  public static float[] floodFillPadSolderArea(Image bimg)
  {
    Assert.expect(bimg != null);
   
    boolean[][] painted = new boolean[bimg.getHeight()][bimg.getWidth()];
    
    int arraySize = bimg.getWidth() * bimg.getHeight();
    
    float [] imageArray = new float[bimg.getWidth() * bimg.getHeight()];
    
    float [] largestPadImageArray = new float[bimg.getWidth() * bimg.getHeight()];
   
    int max = 0;

    for (int i = 0; i < bimg.getHeight(); i++)
    {
      for (int j = 0; j < bimg.getWidth(); j++)
      {
        if (isBlackFloodFill(bimg, j, i) && painted[i][j] == false)
        {
          Queue<Point> queue = new LinkedList<Point>();

          queue.add(new Point(j, i));
            
          int pixelCount = 0;
         
          imageArray = new float[bimg.getWidth() * bimg.getHeight()];
    
          while (queue.isEmpty() == false)
          {
            Point p = queue.remove();
            
            if ((p.x >= 0) && (p.x < bimg.getWidth() && (p.y >= 0) && (p.y < bimg.getHeight())))
            {         
              int arrayPosition = (p.y * bimg.getWidth()) + p.x;
              
              if (painted[p.y][p.x] == false && isBlackFloodFill(bimg, p.x, p.y))
              {
                painted[p.y][p.x] = true;

                pixelCount++;

                queue.add(new Point(p.x + 1, p.y));

                queue.add(new Point(p.x - 1, p.y));

                queue.add(new Point(p.x, p.y + 1));

                queue.add(new Point(p.x, p.y - 1));

                queue.add(new Point(p.x + 1, p.y + 1));

                queue.add(new Point(p.x - 1, p.y + 1));

                queue.add(new Point(p.x + 1, p.y - 1));

                queue.add(new Point(p.x - 1, p.y - 1));                               
                
                imageArray[arrayPosition] = 255;  
              }       
            }
          }       
    
          if (pixelCount > max)
          {
            max = pixelCount;       
          
            largestPadImageArray = ArrayUtil.copy(imageArray, 0, arraySize);
          }

          imageArray = null;
          queue.clear();
          queue = null;
        }
      }
    }

    return largestPadImageArray;
  }

}
