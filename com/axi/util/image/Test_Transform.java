package com.axi.util.image;

import java.io.*;

import com.axi.util.*;
import java.util.Arrays;
import java.util.Collection;
import java.awt.image.BufferedImage;

/**
 * @author Peter Esbensen
 */
public class Test_Transform extends UnitTest
{
  /**
   * @author Peter Esbensen
   */
  public Test_Transform()
  {
  }

  /**
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Transform());
  }

  /**
   * @author Peter Esbensen
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testRotate();
  }

  /* test1.png is an image that looks like:

        0    0    0   127 127 127
        0    0    0   127 127 127
        0    0    0   127 127 127
        0    0    0   255 255 255
        0    0    0   255 255 255
        0    0    0   255 255 255
    */

  /**
   * @author Peter Esbensen
   */
  private void testRotate()
  {

    Image inputImage = new Image(5, 5);
    Paint.fillImage(inputImage, 0f);
    inputImage.setPixelValue(2, 0, 1f);
    inputImage.setPixelValue(4, 2, 1f);

    // Input image:

    // 0 0 1 0 0
    // 0 0 0 0 0
    // 0 0 0 0 1
    // 0 0 0 0 0
    // 0 0 0 0 0

    Image rotatedImage = Transform.rotate(inputImage, 2f, 90);

    // Expected image after 90 degree rotation:

    // 0 0 0 0 0
    // 0 0 0 0 0
    // 0 0 0 0 1
    // 0 0 0 0 0
    // 0 0 1 0 0

    for (int row = 0; row < rotatedImage.getHeight(); ++row)
    {
      for (int col = 0; col < rotatedImage.getWidth(); ++col)
      {
        if ((row == 2 && col == 4) ||
            (row == 4 && col == 2))
        {
          Expect.expect(MathUtil.fuzzyEquals(rotatedImage.getPixelValue(col, row), 1f));
        }
        else
        {
          Expect.expect(MathUtil.fuzzyEquals(rotatedImage.getPixelValue(col, row), 0f));
        }
      }
    }

    rotatedImage = Transform.rotate(inputImage, 2f, -180);

    // Expected image after -180 degree rotation:

    // 0 0 0 0 0
    // 0 0 0 0 0
    // 1 0 0 0 0
    // 0 0 0 0 0
    // 0 0 1 0 0

    for (int row = 0; row < rotatedImage.getHeight(); ++row)
    {
      for (int col = 0; col < rotatedImage.getWidth(); ++col)
      {
        if ((row == 2 && col == 0) ||
            (row == 4 && col == 2))
        {
          Expect.expect(MathUtil.fuzzyEquals(rotatedImage.getPixelValue(col, row), 1f));
        }
        else
        {
          Expect.expect(MathUtil.fuzzyEquals(rotatedImage.getPixelValue(col, row), 0f));
        }
      }
    }

  }

  /**
   * Copied from Test_ImageFeatureExtraction
   * @author Peter Esbensen
   */
  private Image loadTestImage(String directoryPath, String filename)
  {
    Assert.expect(directoryPath != null);
    Assert.expect(directoryPath.length() > 0);
    Assert.expect(filename != null);
    Assert.expect(filename.length() > 0);

    Image resultImage = null;
    try
    {
      resultImage = ImageIoUtil.loadPngImage(directoryPath + File.separator + filename);
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "Exception occurred during PNG Image Load: " + exc.getMessage());
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(false, "FileDoesNotExistException:" + exc.getMessage());
    }
    Assert.expect(resultImage != null);
    return resultImage;
  }
 }
