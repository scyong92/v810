package com.axi.util.image;

import java.awt.image.*;
import java.awt.Graphics2D;
import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Patrick Lacz
 */
class Test_Filter extends UnitTest
{
  /**
   * @author Patrick Lacz
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Filter());
  }

  /**
   * @author Patrick Lacz
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Image image = loadTestImage(getTestDataDir(), "test1.png");

    /* test1.png is an image that looks like:

      0    0    0   127 127 127
      0    0    0   127 127 127
      0    0    0   127 127 127
      0    0    0   255 255 255
      0    0    0   255 255 255
      0    0    0   255 255 255
     */

    testMakeKernel();

    testConvolveRows(image);
    testConvolveColumns(image);

    testSobel(image);
    testLowpass(image);

    testTemplateMatching();

    // uncomment to determine running times
    //testTiming();
  }

  /**
   * @author Patrick Lacz
   */
  private void testMakeKernel()
  {
    try {
      float kernel[] = Filter.createUniform1DFilterKernel(0.123f, -2);
      Expect.expect(false);
    } catch (AssertException e) {
      // expected, do nothing
    }

    float kernel[] = Filter.createUniform1DFilterKernel(0.123f, 7);

    Expect.expect(kernel.length == 7);
    for (int i = 0 ; i < kernel.length ; ++i)
      Expect.expect(Math.abs(kernel[i] - 0.123f) < 0.00001);

    // Circle Kernels
    Image circleKernelImage = new Image(10, 10);
    Paint.fillImage(circleKernelImage, 0.0f);

    /** @todo This should be tested in Test_Paint */
    int pixelsActivated = Paint.fillCircle(circleKernelImage, RegionOfInterest.createRegionFromImage(circleKernelImage),
                                new DoubleCoordinate(0.0, 0.0), 3.0f, 100.0f);
    // definitely inside the circle
    Expect.expect(MathUtil.fuzzyEquals(circleKernelImage.getPixelValue(0,0), 100.0));
    // definitely outside the circle
    Expect.expect(MathUtil.fuzzyEquals(circleKernelImage.getPixelValue(9,9), 0.0));
    // border-line pixels
    Expect.expect(MathUtil.fuzzyEquals(circleKernelImage.getPixelValue(0,2), 100.0));
    Expect.expect(MathUtil.fuzzyEquals(circleKernelImage.getPixelValue(0,3), 0.0));
    Expect.expect(MathUtil.fuzzyEquals(circleKernelImage.getPixelValue(1,2), 100.0));
    Expect.expect(MathUtil.fuzzyEquals(circleKernelImage.getPixelValue(1,3), 0.0));
    Expect.expect(MathUtil.fuzzyEquals(circleKernelImage.getPixelValue(2,1), 100.0));
    Expect.expect(MathUtil.fuzzyEquals(circleKernelImage.getPixelValue(2,2), 0.0));
    Expect.expect(MathUtil.fuzzyEquals(circleKernelImage.getPixelValue(3,0), 0.0));
    Expect.expect(MathUtil.fuzzyEquals(circleKernelImage.getPixelValue(3,1), 0.0));
    Expect.expect(pixelsActivated == 8);
    circleKernelImage.decrementReferenceCount();
  }

  /**
   * @author Patrick Lacz
   */
  private void testConvolveRows(Image image)
  {
    Assert.expect(image != null);

    float kernel[] = Filter.createUniform1DFilterKernel(1.f/3.f, 3);
    Image resultImage;

    // test illegal regions of interest
    Collection<RegionOfInterest> regions = ImageUnitTestUtil.generateBadRegionsOfInterest(image);
    for (RegionOfInterest regionOfInterest : regions)
    {
      try
      {
        resultImage = Filter.convolveRows(image, regionOfInterest, kernel);
        Expect.expect( false ); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
      try
      {
        resultImage = Filter.convolveRowsUniformKernel(image, regionOfInterest, 3, 1.f, 0, BorderSamplingModeEnum.CONSTRAIN_TO_REGION);
        Expect.expect( false ); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
    }

    RegionOfInterest badRoi = RegionOfInterest.createRegionFromRegionBorder(2,2,5,5, 0, RegionShapeEnum.RECTANGULAR);
    try {
      resultImage = Filter.convolveRows(image, badRoi, kernel);
      Expect.expect( false );
    } catch (AssertException ae) {
      // do nothing, expected
    }

     // Whole Image.
     resultImage = Filter.convolveRows(image, kernel);
     Expect.expect(resultImage.getWidth() == image.getWidth()-kernel.length+1);
     Expect.expect(resultImage.getHeight() == image.getHeight());

     float result0 = resultImage.getPixelValue(0, 0);
     float result1 = resultImage.getPixelValue(1, 0);
     float result2 = resultImage.getPixelValue(2, 0);
     float result3 = resultImage.getPixelValue(3, 0);
     Expect.expect(Math.abs(result0) < 0.000001f );
     Expect.expect(Math.abs(result1 - 42.3333333f) < 0.00001f );
     Expect.expect(Math.abs(result2 - 84.6666666f) < 0.00001f );
     Expect.expect(Math.abs(result3 - 127.f) < 0.00001f );

     resultImage.decrementReferenceCount();

     // Whole Image.
     resultImage = Filter.convolveRowsUniformKernel(image, RegionOfInterest.createRegionFromImage(image), 3, 1f / 3f, 0,
                                                    BorderSamplingModeEnum.CONSTRAIN_TO_REGION);
     Expect.expect(resultImage.getWidth() == image.getWidth()-kernel.length+1);
     Expect.expect(resultImage.getHeight() == image.getHeight());

     result0 = resultImage.getPixelValue(0, 0);
     result1 = resultImage.getPixelValue(1, 0);
     result2 = resultImage.getPixelValue(2, 0);
     result3 = resultImage.getPixelValue(3, 0);
     Expect.expect(Math.abs(result0) < 0.000001f);
     Expect.expect(Math.abs(result1 - 42.3333333f) < 0.00001f);
     Expect.expect(Math.abs(result2 - 84.6666666f) < 0.00001f);
     Expect.expect(Math.abs(result3 - 127.f) < 0.00001f);

     resultImage.decrementReferenceCount();
    /**
     * @todo more test cases
     */
  }

  /**
   * @author Patrick Lacz
   */
  private void testConvolveColumns(Image image)
  {
    Assert.expect(image != null);

    float kernel[] = Filter.createUniform1DFilterKernel(1.f / 3.f, 3);
    Image resultImage;

    // test illegal regions of interest
    Collection<RegionOfInterest> regions = ImageUnitTestUtil.generateBadRegionsOfInterest(image);
    for (RegionOfInterest regionOfInterest : regions)
    {
      try
      {
        resultImage = Filter.convolveColumns(image, regionOfInterest, kernel);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
      try
      {
        resultImage = Filter.convolveColumnsUniformKernel(image, regionOfInterest, 3, 1.f, 0, BorderSamplingModeEnum.CONSTRAIN_TO_REGION);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
    }

    RegionOfInterest badRoi = RegionOfInterest.createRegionFromRegionBorder(2, 2, 5, 5, 0, RegionShapeEnum.RECTANGULAR);
    try
    {
      resultImage = Filter.convolveColumns(image, badRoi, kernel);
      Expect.expect(false);
    }
    catch (AssertException ae)
    {
      // do nothing, expected
    }

    // Whole Image.
    resultImage = Filter.convolveColumns(image, kernel);
    Expect.expect(resultImage.getWidth() == image.getWidth());
    Expect.expect(resultImage.getHeight() == image.getHeight() - kernel.length + 1);

    float result0 = resultImage.getPixelValue(0, 0);
    float result1 = resultImage.getPixelValue(1, 0);
    float result2 = resultImage.getPixelValue(2, 0);
    float result3 = resultImage.getPixelValue(3, 0);
    float resultX = resultImage.getPixelValue(3, 1);
    Expect.expect(Math.abs(result0) < 0.000001f);
    Expect.expect(Math.abs(result1) < 0.000001f);
    Expect.expect(Math.abs(result2) < 0.000001f);
    Expect.expect(Math.abs(result3 - 127.f) < 0.00001f);
    Expect.expect(Math.abs(resultX - 169.666666f) < 0.00001f);

     resultImage.decrementReferenceCount();
    resultImage = Filter.convolveColumnsUniformKernel(image, RegionOfInterest.createRegionFromImage(image), 3, 1.f/3f, 0,
        BorderSamplingModeEnum.CONSTRAIN_TO_REGION);
    Expect.expect(resultImage.getWidth() == image.getWidth());
    Expect.expect(resultImage.getHeight() == image.getHeight() - kernel.length + 1);


    result0 = resultImage.getPixelValue(0, 0);
    result1 = resultImage.getPixelValue(1, 0);
    result2 = resultImage.getPixelValue(2, 0);
    result3 = resultImage.getPixelValue(3, 0);
    resultX = resultImage.getPixelValue(3, 1);
    Expect.expect(Math.abs(result0) < 0.000001f);
    Expect.expect(Math.abs(result1) < 0.000001f);
    Expect.expect(Math.abs(result2) < 0.000001f);
    Expect.expect(Math.abs(result3 - 127.f) < 0.00001f);
    Expect.expect(Math.abs(resultX - 169.666666f) < 0.00001f);

     resultImage.decrementReferenceCount();
    /**
     * @todo more test cases
     */
  }

  /**
   * @author Patrick Lacz
   */
  private void testTemplateMatching()
  {
    BufferedImage sourceBuffer = new BufferedImage(400, 400, BufferedImage.TYPE_BYTE_GRAY);
    Graphics2D gfx = sourceBuffer.createGraphics();

    int sizeX = 51;
    int sizeY = 62;
    int borderWidth = 10;
    int borderHeight = 10;

    // build an image to search with a few distracting rectangles.
    gfx.setBackground(java.awt.Color.WHITE);
    gfx.fillRect(0, 0, sourceBuffer.getWidth(), sourceBuffer.getHeight());
    gfx.setColor(java.awt.Color.BLACK);
    gfx.fillRect(30, 62, 72, 15);
    gfx.fillRect(302, 123, 23, 70);

    gfx.fillRect(142, 276, 61, 62);

    // add the triangle to find.
    int xCoords[] = {127, 127+sizeX, 127+sizeX};
    int yCoords[] = {99, 99, 99+sizeY};
    gfx.fillPolygon(xCoords, yCoords, 3);

    int xCoords2[] = {300, 300+sizeX, 300};
    int yCoords2[] = {300, 300, 300+sizeY};
    gfx.fillPolygon(xCoords2, yCoords2, 3);

    Image searchImage = Image.createFloatImageFromBufferedImage(sourceBuffer);

    // Create the template
    BufferedImage templateBuffer = new BufferedImage(sizeX + 2*borderWidth, sizeY+2*borderHeight, BufferedImage.TYPE_BYTE_GRAY);
    gfx = templateBuffer.createGraphics();
    gfx.setBackground(java.awt.Color.WHITE);
    gfx.fillRect(0, 0, templateBuffer.getWidth(), templateBuffer.getHeight());
    gfx.setColor(java.awt.Color.BLACK);
    int templateXCoords[] = {borderWidth, borderWidth+sizeX, borderWidth+sizeX};
    int templateYCoords[] = {borderHeight, borderHeight, borderHeight+sizeY};
    gfx.fillPolygon(templateXCoords, templateYCoords, 3);

    Image templateImage = Image.createFloatImageFromBufferedImage(templateBuffer);

    DoubleRef matchQuality = new DoubleRef();

    // test illegal regions of interest
    Collection<RegionOfInterest> regions = ImageUnitTestUtil.generateBadRegionsOfInterest(searchImage);
    for (RegionOfInterest regionOfInterest : regions)
    {
      try
      {
        ImageCoordinate coord = Filter.matchTemplate(searchImage,
                                                     templateImage,
                                                     regionOfInterest,
                                                     MatchTemplateTechniqueEnum.CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING,
                                                     matchQuality);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
    }


    ImageCoordinate coord = Filter.matchTemplate(searchImage,
                                                 templateImage,
                                                 MatchTemplateTechniqueEnum.CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING,
                                                 matchQuality);

    Expect.expect(coord.getX() + borderWidth == 127);
    Expect.expect(coord.getY() + borderHeight == 99);

    Transform.flipX(templateImage);
    coord = Filter.matchTemplate(searchImage,
                                 templateImage,
                                 MatchTemplateTechniqueEnum.CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING,
                                 matchQuality);

    Expect.expect(coord.getX() + borderWidth == 300);
    Expect.expect(coord.getY() + borderHeight == 300);

    searchImage.decrementReferenceCount();
    templateImage.decrementReferenceCount();

    /**
     * @todo more test cases for matchTemplate
     */
    }

    /**
     * @author Patrick Lacz
     */
    public void testLocateRectangle()
    {
      BufferedImage sourceBuffer = new BufferedImage(400, 400, BufferedImage.TYPE_BYTE_GRAY);
      Graphics2D gfx = sourceBuffer.createGraphics();

      int sizeX = 23;
      int sizeY = 40;
      int borderWidth = 10;
      int borderHeight = 10;

      gfx.setBackground(java.awt.Color.WHITE);
      gfx.fillRect(0, 0, sourceBuffer.getWidth(), sourceBuffer.getHeight());
      gfx.setColor(java.awt.Color.BLACK);
      gfx.fillRect(30, 62, 72, 15);
      gfx.fillRect(302, 123, 23, 70);
      gfx.fillRect(142, 276, 61, 62);
      gfx.fillRect(127, 99, sizeX, sizeY);

      Image searchImage = Image.createFloatImageFromBufferedImage(sourceBuffer);

      RegionOfInterest roi = RegionOfInterest.createRegionFromImage(searchImage);

      ImageCoordinate coord = Filter.locateRectangle(searchImage, roi, sizeX, sizeY, borderWidth,
          borderHeight, true);

      searchImage.decrementReferenceCount();
      // This test checks that the locate rectangle routine is not confused by rectangles of other sizes.
      Expect.expect(coord.getX() == 127);
      Expect.expect(coord.getY() == 99);
    }

  /**
   * Tests:
   *  convolveSobelHorizontal
   *  convolveSobelVertical
   *
   * @author Patrick Lacz
   */
    private void testSobel(Image testImage)
  {
    Assert.expect(testImage != null);

    // test illegal regions of interest
    Collection<RegionOfInterest> regions = ImageUnitTestUtil.generateBadRegionsOfInterest(testImage);
    for (RegionOfInterest regionOfInterest : regions)
    {
      try
      {
        Filter.convolveSobelHorizontal(testImage, regionOfInterest);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
    }
    for (RegionOfInterest regionOfInterest : regions)
    {
      try
      {
        Filter.convolveSobelVertical(testImage, regionOfInterest);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
    }

    Image verticalEdgeImage = Filter.convolveSobelVertical(testImage);
    Expect.expect(MathUtil.fuzzyEquals(verticalEdgeImage.getPixelValue(0,0), 0.f));
    Expect.expect(MathUtil.fuzzyEquals(verticalEdgeImage.getPixelValue(2,0), -4*127.f));
    Expect.expect(MathUtil.fuzzyEquals(verticalEdgeImage.getPixelValue(3,0), -4*127.f));
    Expect.expect(MathUtil.fuzzyEquals(verticalEdgeImage.getPixelValue(4,0), 0.f));
    Expect.expect(MathUtil.fuzzyEquals(verticalEdgeImage.getPixelValue(2,3), -127.f-3*255.f));
    Expect.expect(MathUtil.fuzzyEquals(verticalEdgeImage.getPixelValue(3,3), -127.f-3*255.f));
    Expect.expect(MathUtil.fuzzyEquals(verticalEdgeImage.getPixelValue(4,3), 0.f));
    Expect.expect(MathUtil.fuzzyEquals(verticalEdgeImage.getPixelValue(3,4), -4*255.f));
    Expect.expect(MathUtil.fuzzyEquals(verticalEdgeImage.getPixelValue(4,4), 0.f));
    Expect.expect(MathUtil.fuzzyEquals(verticalEdgeImage.getPixelValue(5,5), 0.f));
    verticalEdgeImage.decrementReferenceCount();

    Image horizontalEdgeImage = Filter.convolveSobelHorizontal(testImage);
    Expect.expect(MathUtil.fuzzyEquals(horizontalEdgeImage.getPixelValue(0,0), 0.f));
    Expect.expect(MathUtil.fuzzyEquals(horizontalEdgeImage.getPixelValue(2,0), 0.f));
    Expect.expect(MathUtil.fuzzyEquals(horizontalEdgeImage.getPixelValue(3,0), 0.f));
    Expect.expect(MathUtil.fuzzyEquals(horizontalEdgeImage.getPixelValue(4,0), 0.f));
    Expect.expect(MathUtil.fuzzyEquals(horizontalEdgeImage.getPixelValue(2,3), 128.f));
    Expect.expect(MathUtil.fuzzyEquals(horizontalEdgeImage.getPixelValue(3,3), 3*128.f));
    Expect.expect(MathUtil.fuzzyEquals(horizontalEdgeImage.getPixelValue(4,3), 4*128.f));
    Expect.expect(MathUtil.fuzzyEquals(horizontalEdgeImage.getPixelValue(5,5), 0.f));
    horizontalEdgeImage.decrementReferenceCount();
  }

 /**
   * Tests: convolveLowpass
   *
   * @author Patrick Lacz
   */
    private void testLowpass(Image testImage)
  {
    Assert.expect(testImage != null);

    // test illegal regions of interest
    Collection<RegionOfInterest> regions = ImageUnitTestUtil.generateBadRegionsOfInterest(testImage);
    for (RegionOfInterest regionOfInterest : regions)
    {
      try
      {
        Filter.convolveLowpass(testImage, regionOfInterest);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
    }

    Image blurredImage = Filter.convolveLowpass(testImage);
    Expect.expect(MathUtil.fuzzyEquals(blurredImage.getPixelValue(0,0), 0.f));
    Expect.expect(MathUtil.fuzzyEquals(blurredImage.getPixelValue(2,0), 3.f/9.f*127.f));
    Expect.expect(MathUtil.fuzzyEquals(blurredImage.getPixelValue(3,0), 6.f/9.f*127.f));
    Expect.expect(MathUtil.fuzzyEquals(blurredImage.getPixelValue(4,0), 127.f));
    Expect.expect(MathUtil.fuzzyEquals(blurredImage.getPixelValue(2,3), 127.f/9.f+2.f/9.f*255.f));
    Expect.expect(MathUtil.fuzzyEquals(blurredImage.getPixelValue(3,3), 2.f/9.f*127.f+4.f/9.f*255.f));
    Expect.expect(MathUtil.fuzzyEquals(blurredImage.getPixelValue(4,3), 3.f/9.f*127.f+6.f/9.f*255.f));
    Expect.expect(MathUtil.fuzzyEquals(blurredImage.getPixelValue(3,4), 6.f/9.f*255.f));
    Expect.expect(MathUtil.fuzzyEquals(blurredImage.getPixelValue(4,4), 255.f));
    Expect.expect(MathUtil.fuzzyEquals(blurredImage.getPixelValue(5,5), 255.f));
    blurredImage.decrementReferenceCount();
  }

  /**
   * Implemented only to compare techniques of implementing the algorithm. Kept for future reference.
   * @author Patrick Lacz
   */
  private void testTiming()
  {
    Image image = loadTestImage(getTestDataDir(), "testimage_png1.png");

    Assert.expect(image != null);

    System.out.println("Image Dimensions: " + image.getWidth() + " x " + image.getHeight());

    int tests = 100;

    float kernel[] = Filter.createUniform1DFilterKernel(1.f,10);

    long nanoStart = System.nanoTime();
    for (int i = 0 ; i < tests ; ++i)
    {
      Filter.convolveRows(image, kernel);
    }
    long nanoEnd = System.nanoTime();

    double duration = (nanoEnd - nanoStart)/tests;
    System.out.println("Convolve Rows invokation with a kernel size of 10 took on average " + duration/100000.0 + " milliseconds.");


    kernel = Filter.createUniform1DFilterKernel(1.f,5);

    nanoStart = System.nanoTime();
    for (int i = 0 ; i < tests ; ++i)
    {
      Filter.convolveRows(image, kernel);
    }
    nanoEnd = System.nanoTime();
    duration = (nanoEnd - nanoStart)/tests;
    System.out.println("Convolve Rows invokation with a kernel size of 5 took on average " + duration/100000.0 + " milliseconds.");


     nanoStart = System.nanoTime();
    for (int i = 0 ; i < tests ; ++i)
    {
      Filter.convolveColumns(image, kernel);
    }
    nanoEnd = System.nanoTime();

    duration = (nanoEnd - nanoStart)/tests;
    System.out.println("Convolve Columns invokation with a kernel size of 10 took on average " + duration/100000.0 + " milliseconds.");


    kernel = Filter.createUniform1DFilterKernel(1.f,5);

    nanoStart = System.nanoTime();
    for (int i = 0 ; i < tests ; ++i)
    {
      Filter.convolveColumns(image, kernel);
    }
    nanoEnd = System.nanoTime();
    duration = (nanoEnd - nanoStart)/tests;
    System.out.println("Convolve Columns invokation with a kernel size of 5 took on average " + duration/100000.0 + " milliseconds.");

    image.decrementReferenceCount();
  }

  /**
   * Copied from Test_ImageFeatureExtraction
   * @author Peter Esbensen
   */
  private Image loadTestImage(String directoryPath, String filename)
  {
    Assert.expect(directoryPath != null);
    Assert.expect(directoryPath.length() > 0);
    Assert.expect(filename != null);
    Assert.expect(filename.length() > 0);

    Image resultImage = null;
    try
    {
      resultImage = ImageIoUtil.loadPngImage(directoryPath + File.separator + filename);
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "Exception occurred during PNG Image Load: " + exc.getMessage());
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(false, "FileDoesNotExistException:" + exc.getMessage());
    }
    Assert.expect(resultImage != null);
    return resultImage;
  }
}
