#include <jni.h>
#include "com_axi_util_image_NativeMemoryMonitor.h"

#include "jniImageUtil.h"

#include "ippcore.h"
#include "ippi.h"
#include "ipps.h"

#include "sptAssert.h"

using namespace std;


/*
 * nativeAllocateAligned
 * Signature: ([I)Z
 */
JNIEXPORT jboolean JNICALL Java_com_axi_util_image_NativeMemoryMonitor_nativeAllocateAlignedIfPossible(JNIEnv *jEnv, 
                                                                                                           jclass jContext, 
                                                                                                           jintArray jImageDef)
{
  jboolean wasAllocated = false;
  try
  {
    JNIImageData *pImageDef = getImageDataFromJava(jEnv, jImageDef);
    
    pImageDef->address = ippiMalloc_32f_C1(pImageDef->width, pImageDef->height, &pImageDef->stride);
    if(pImageDef->address == NULL)
      wasAllocated = false;
    else
    {
      wasAllocated = true;
      pImageDef->allocation = IPP_ALIGNED_ALLOCATED;
    }
    releaseImageDataFromJavaModified(jEnv, jImageDef, pImageDef);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }

  return wasAllocated;
}

/**
 * nativeAllocateAligned
 *
 * @returns a ByteBuffer referencing IPP-allocated memory
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_NativeMemoryMonitor_nativeAllocateAligned(JNIEnv *jEnv, 
                                                                                             jclass jContext, 
                                                                                             jintArray jImageDef)
{
  try
  {
    JNIImageData *pImageDef = getImageDataFromJava(jEnv, jImageDef);
    
    int count = 0;
    do
    {
      if(count > 0)
        _sleep(1000);
      pImageDef->address = ippiMalloc_32f_C1(pImageDef->width, pImageDef->height, &pImageDef->stride);
      ++count;
    }while(pImageDef->address == NULL && count < 5);
    
//    sptAssertWithMessage(pImageDef->address != NULL, "Unable to allocate image memory!");
    ostringstream oss;
    oss << "Unable to allocate image memory! width: " << pImageDef->width << " height: " << pImageDef->height << " stride: " << pImageDef->stride;
    sptAssertWithMessage(pImageDef->address != NULL, oss.str().c_str());
    
    pImageDef->allocation = IPP_ALIGNED_ALLOCATED;
    releaseImageDataFromJavaModified(jEnv, jImageDef, pImageDef);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * nativeAllocateContiguousIfPoosible
 * @author George A. David
 */
JNIEXPORT jboolean JNICALL Java_com_axi_util_image_NativeMemoryMonitor_nativeAllocateContiguousIfPossible(JNIEnv *jEnv, 
                                                                                                              jclass jContext, 
                                                                                                              jintArray jImageDef)
{
  jboolean wasAllocated = false;
  try
  {
    JNIImageData *pImageDef = getImageDataFromJava(jEnv, jImageDef);
    
    pImageDef->address = ippsMalloc_32f(pImageDef->width * pImageDef->height);
    if(pImageDef->address == NULL)
      wasAllocated = false;
    else
    {
      wasAllocated = true;
      pImageDef->stride = pImageDef->width * sizeof(Ipp32f);
      pImageDef->allocation = IPP_CONTIGUOUS_ALLOCATED;
    }

    releaseImageDataFromJavaModified(jEnv, jImageDef, pImageDef);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }

  return wasAllocated;
}

/**
 * nativeAllocateContiguous
 *
 * @returns a ByteBuffer referencing IPP-allocated memory
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_NativeMemoryMonitor_nativeAllocateContiguous
(JNIEnv *jEnv, jclass jContext, jintArray jImageDef)
{
  try
  {
    JNIImageData *pImageDef = getImageDataFromJava(jEnv, jImageDef);
    
    int count = 0;
    do
    {
      if(count > 0)
        _sleep(1000);
      pImageDef->address = ippsMalloc_32f(pImageDef->width * pImageDef->height);
      ++count;
    }while(pImageDef->address == NULL && count < 5);
    
    pImageDef->stride = pImageDef->width * sizeof(Ipp32f);
    ostringstream oss;
    oss << "Unable to allocate image memory! width: " << pImageDef->width << " height: " << pImageDef->height << " stride: " << pImageDef->stride;
    sptAssertWithMessage(pImageDef->address != NULL, oss.str().c_str());
//    sptAssertWithMessage(pImageDef->address != NULL, "Unable to allocate image memory!");
    
    pImageDef->allocation = IPP_CONTIGUOUS_ALLOCATED;
    releaseImageDataFromJavaModified(jEnv, jImageDef, pImageDef);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeAllocateCPPContiguous
 *
 * @returns a ByteBuffer referencing CPP-allocated memory
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_NativeMemoryMonitor_nativeAllocateCPPContiguous(JNIEnv *jEnv, 
                                                                                                   jclass jContext, 
                                                                                                   jintArray jImageDef)
{
  try
  {
    JNIImageData *pImageDef = getImageDataFromJava(jEnv, jImageDef);
    
    int count = 0;
    do
    {
      if(count > 0)
        _sleep(1000);
      pImageDef->address = new Ipp32f[pImageDef->width*pImageDef->height];
      ++count;
    }while(pImageDef->address == NULL && count < 5);

    pImageDef->stride = pImageDef->width * sizeof(Ipp32f);
    ostringstream oss;
    oss << "Unable to allocate image memory! width: " << pImageDef->width << " height: " << pImageDef->height << " stride: " << pImageDef->stride;
    sptAssertWithMessage(pImageDef->address != NULL, oss.str().c_str());
//    sptAssertWithMessage(pImageDef->address != NULL, "Unable to allocate image memory!");
    
    pImageDef->allocation = CPP_NEW_ALLOCATED;
    releaseImageDataFromJavaModified(jEnv, jImageDef, pImageDef);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeGetByteBuffer
 * @author Patrick Lacz
 */
JNIEXPORT jobject JNICALL Java_com_axi_util_image_NativeMemoryMonitor_nativeGetByteBuffer(JNIEnv *jEnv, 
                                                                                              jclass jContext, jintArray jImageDef)
{
  try
  {
    JNIImageData *pImageDef = getImageDataFromJava(jEnv, jImageDef);
    
    jobject byteBuffer = jEnv->NewDirectByteBuffer(reinterpret_cast<void*>(pImageDef->address), pImageDef->height * pImageDef->stride);
    
    releaseImageDataFromJavaUnmodified(jEnv, jImageDef, pImageDef);
    return byteBuffer;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return NULL;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return NULL;
  }
}

/**
 * nativeFreeImage
 *
 * Frees memory based on the allocation technique (stored in the image definition).
 * Returns the size of the memory to remove from the manager.
 * @author Patrick Lacz
 */
JNIEXPORT jint JNICALL Java_com_axi_util_image_NativeMemoryMonitor_nativeFree(JNIEnv *jEnv, 
                                                                                  jclass jContext, 
                                                                                  jintArray jImageDef)
{
  try
  {
    JNIImageData *pImageDef = getImageDataFromJava(jEnv, jImageDef);
    
    int imageSize = 0;
    switch (pImageDef->allocation)
    {
    case UNALLOCATED:
      sptAssert(pImageDef->allocation != UNALLOCATED);
      break;
    case JAVA_ALLOCATED:
      // do nothing
      break;
    case CPP_NEW_ALLOCATED:
      delete [] pImageDef->address;
      imageSize = pImageDef->stride * pImageDef->height;
      break;
    case C_MALLOC_ALLOCATED:
      free(pImageDef->address);
      imageSize = pImageDef->stride * pImageDef->height;
      break;
    case IPP_CONTIGUOUS_ALLOCATED:
      ippsFree(pImageDef->address);
      imageSize = pImageDef->stride * pImageDef->height;
      break;
    case IPP_ALIGNED_ALLOCATED:
      ippiFree(pImageDef->address);
      imageSize = pImageDef->stride * pImageDef->height;
      break;
    default:
      sptAssert(false);
    }
    
    pImageDef->address = NULL;
    pImageDef->width = 0;
    pImageDef->height = 0;
    pImageDef->stride = 0;
    pImageDef->allocation = UNALLOCATED;
    releaseImageDataFromJavaModified(jEnv, jImageDef, pImageDef);
    return imageSize;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * nativeIntializeJavaDefinition
 *
 * Intialize the JNIImageData for an image that isn't aligned (allocated via Java, probably. This may change to an ipps-allocated memory block)
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_NativeMemoryMonitor_nativeIntializeJavaDefinition(JNIEnv *jEnv, 
                                                                                                     jclass jContext, 
                                                                                                     jintArray jImageDef, 
                                                                                                     jobject jImageBuffer)
{
  try
  {
    JNIImageData *pImageDef = getImageDataFromJava(jEnv, jImageDef);
    Ipp32f *pImageBuffer = reinterpret_cast<Ipp32f*>(jEnv->GetDirectBufferAddress(jImageBuffer));
    
    sptAssert(pImageDef->width > 0);
    sptAssert(pImageDef->height > 0);
    
    pImageDef->address = pImageBuffer;
    pImageDef->stride = pImageDef->width * sizeof(Ipp32f);
    pImageDef->allocation = JAVA_ALLOCATED;
    releaseImageDataFromJavaModified(jEnv, jImageDef, pImageDef);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}


