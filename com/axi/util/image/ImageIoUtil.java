package com.axi.util.image;

import java.awt.image.*;
import java.util.*;
import java.io.*;
import javax.imageio.*;
import javax.imageio.metadata.*;
import javax.imageio.stream.*;
import javax.imageio.event.*;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;

import com.sun.media.imageio.plugins.tiff.*;

import com.axi.util.*;

/**
 * Shared utility functions for image I/O.
 * @author Kay Lannen
 */
public class ImageIoUtil implements IIOWriteProgressListener
{
  static private final String _JPEG_IMAGE_WRITER_CLASS = "com.sun.imageio.plugins.jpeg.JPEGImageWriter";
  static private final String _JPEG_IMAGE_READER_CLASS = "com.sun.imageio.plugins.jpeg.JPEGImageReader";

  static private final String _JPEG_FORMAT = "JPEG";
  static private final float _JPEG_COMPRESSION_QUALITY = 0.75F;
  static private final String _JPEG_METADATA_FORMAT = "javax_imageio_jpeg_image_1.0";

  static private final String _PNG_IMAGE_META_WRITER_CLASS = "com.sun.imageio.plugins.png.PNGImageWriter";
  static private final String _PNG_IMAGE_WRITER_CLASS = "com.sun.media.imageioimpl.plugins.png.CLibPNGImageWriter";
  static private final String _PNG_IMAGE_READER_CLASS = "com.sun.imageio.plugins.png.PNGImageReader";
  static private final float _PNG_COMPRESSION_QUALITY = 0.11111111F;
  static private final String _PNG_FORMAT = "PNG";
  
  static private final String _TIFF_FORMAT = "TIFF";
  static private final String _TIFF_IMAGE_WRITER_CLASS = "com.sun.media.imageioimpl.plugins.tiff.TIFFImageWriter";
  static private final String _TIFF_IMAGE_READER_CLASS = "com.sun.media.imageioimpl.plugins.tiff.TIFFImageReader";
  static private final String _TIFF_METADATA_FORMAT = "com_sun_media_imageio_plugins_tiff_image_1.0";

  static private final String _STD_METADATA_FORMAT = "javax_imageio_1.0";

  static private ImageIoUtil  _instance = new ImageIoUtil();
  private boolean             _writeCompleted = true;

  /**
   * @author Patrick Lacz
   */
  static
  {
    System.loadLibrary("nativeImageUtil");
  }

  /**
   * Creates the directory and its parent directories if they do not already exist.
   * @param pathName the directory path to be created

   * @author Kay Lannen
   */
  static public void createImageDirectoryIfNeeded(String pathName) throws FileFoundWhereDirectoryWasExpectedException,
      CouldNotCreateFileException
  {
    Assert.expect(pathName != null);

    if (FileUtil.exists(pathName) == false)
    {
      FileUtil.mkdirs(pathName);
    }
  }

  /**
   * Under the 1.5.0 release of JDK we may only write to one file at a time.
   * And, we must wait around for the write to finish.  Otherwise, the file
   * you are expecting will/may never appear.  It is a race condition and you'll
   * be taking your chances.
   *
   * @author Roy Williams
   */
  private synchronized void writeImageAndWaitForWriterToComplete(ImageWriter imageWriter,
                                                                IIOImage image, ImageWriteParam param) throws IOException
  {
    Assert.expect(imageWriter != null);
    Assert.expect(image != null);

    _writeCompleted = false;
    imageWriter.addIIOWriteProgressListener(this);
    imageWriter.write(null, image, param);

    while (_writeCompleted == false)
    {
      try
      {
        wait(200);
      }
      catch (InterruptedException e)
      {
        // do nothing.
      }
    }
  }

  /**
   * @author Roy Williams
   */
  public synchronized void writeAborted(ImageWriter source)
  {
    _writeCompleted = true;
    notifyAll();
  }

  /**
   * @author Roy Williams
   */
  public synchronized void imageComplete(ImageWriter source)
  {
    _writeCompleted = true;
    notifyAll();
  }

  /**
   * @author Roy Williams
   */
  public void imageProgress(ImageWriter source, float percentageDone)
  {
    // do nothing
  }

  /**
   * @author Roy Williams
   */
  public void imageStarted(ImageWriter source, int imageIndex)
  {
    // do nothing
  }

  /**
   * @author Roy Williams
   */
  public void thumbnailComplete(ImageWriter source)
  {
    // do nothing
  }

  /**
   * @author Roy Williams
   */
  public void thumbnailProgress(ImageWriter source, float percentageDone)
  {
    // do nothing
  }

  /**
   * @author Roy Williams
   */
  public void thumbnailStarted(ImageWriter source, int imageIndex, int thumbnailIndex)
  {
    // do nothing
  }


  /**
   * Creates metadata from the image's ImageDescription.
   * @author Patrick Lacz
   */
  static public void saveJpegImage(Image image, String fullPathName) throws CouldNotCreateFileException
  {
    Assert.expect(image != null);
    Assert.expect(fullPathName != null);

    java.awt.image.BufferedImage bufferedImage = image.getBufferedImage();
    if (image.hasImageDescription())
    {
      String propertyString = image.getImageDescription().createStringDescription();
      saveJpegImage(bufferedImage, fullPathName, propertyString);
    }
    else
    {
      saveJpegImage(bufferedImage, fullPathName, "");
    }
  }


  /**
   * Saves a buffered image to disk as a Jpeg image with embedded text metadata.
   * @param buffImg buffered image to be saved
   * @param filePath filename of the image
   * @param metaData metadata string to be stored within the image header

   * @author Kay Lannen
   * @author Matt Wharton
   */
  static public void saveJpegImage(java.awt.image.BufferedImage buffImg,
                                   String filePath,
                                   String metaData) throws CouldNotCreateFileException
  {
    Assert.expect(buffImg != null);
    Assert.expect(filePath != null);
    Assert.expect(metaData != null);

    ImageWriter imgWriter = null;
    try
    {
      // Create the directory if needed.
      String dir = FileUtil.getAbsoluteParent(filePath);
      createImageDirectoryIfNeeded(dir);

      File outputFile = new File(filePath);
      if (!outputFile.canWrite())
      {
        if (!outputFile.createNewFile())
        {
          throw new CouldNotCreateFileException(filePath);
        }
      }

      imgWriter = getSpecificImageWriter(_JPEG_IMAGE_WRITER_CLASS, _JPEG_FORMAT);
      ImageOutputStream ios = ImageIO.createImageOutputStream(outputFile);
      imgWriter.setOutput(ios);
      IIOMetadata meta = null;
      ImageWriteParam param = imgWriter.getDefaultWriteParam();
      param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
      param.setCompressionQuality(_JPEG_COMPRESSION_QUALITY);
      if(metaData.length() > 0)
      {
        ImageTypeSpecifier imgTypeSpec = ImageTypeSpecifier.createFromBufferedImageType(buffImg.getType());
        meta = imgWriter.getDefaultImageMetadata(imgTypeSpec, param);
        addStringToJpegMetadata(meta, metaData);
      }
      IIOImage image = new IIOImage(buffImg, null, meta);
      imgWriter.write(meta, image, param);

      ios.close();
      imgWriter.dispose();
    }
    catch (IOException exc)
    {
      if (imgWriter != null)
      {
        imgWriter.dispose();
      }
      throw new CouldNotCreateFileException(filePath);
    }
  }

  /**
   * @author George A. David
   */
  static public void saveJpegImage(java.awt.image.BufferedImage buffImg,
                                   String filePath) throws CouldNotCreateFileException
  {
    Assert.expect(buffImg != null);
    Assert.expect(filePath != null);

    saveJpegImage(buffImg, filePath, "");
  }

  /**
   * Creates metadata from the image's ImageDescription.
   * @author Patrick Lacz
   */
  static public void savePngImage(Image image, String fullPathName) throws CouldNotCreateFileException
  {
    Assert.expect(image != null);
    Assert.expect(fullPathName != null);

    //Ngie Xing, added checking whether directory exist, else create it
    try
    {
      // Create the directory if needed.
      String dir = FileUtil.getAbsoluteParent(fullPathName);
      createImageDirectoryIfNeeded(dir);
      
      if (image.hasImageDescription())
      {
        String propertyString = image.getImageDescription().createStringDescription();
        nativeSavePng(image.getNativeDefinition(), fullPathName, propertyString);
      }
      else
      {
        nativeSavePng(image.getNativeDefinition(), fullPathName, "");
      }
    }
    catch ( IOException exception)
    {
      throw new CouldNotCreateFileException(fullPathName);
    }
  }

  /**
   * Saves a buffered image to disk as a PNG (Portable Network Graphics) image with embedded text metadata.
   * @param buffImg buffered image to be saved
   * @param fullPathName Fully qualified filename of the image
   * @param metaData metadata string to be stored within the image header

   * @author Kay Lannen
   * @author Matt Wharton
   */
  static public void savePngImage(java.awt.image.BufferedImage buffImg, String fullPathName, String metaData) throws CouldNotCreateFileException
  {
    Assert.expect(buffImg != null);
    Assert.expect(fullPathName != null);
    Assert.expect(metaData != null);

    ImageWriter imgWriter = null;
    try
    {
      // Crate the directory if needed.
      String dir = FileUtil.getAbsoluteParent(fullPathName);
      createImageDirectoryIfNeeded(dir);

      File outputFile = new File(fullPathName);
      if (!outputFile.canWrite())
      {
        if (!outputFile.createNewFile())
        {
          throw new CouldNotCreateFileException(fullPathName);
        }
      }

      imgWriter = getSpecificImageWriter(_PNG_IMAGE_META_WRITER_CLASS, _PNG_FORMAT);
      ImageOutputStream ios = ImageIO.createImageOutputStream(outputFile);
      imgWriter.setOutput(ios);
      ImageTypeSpecifier imgTypeSpec = ImageTypeSpecifier.createFromBufferedImageType(buffImg.getType());
      ImageWriteParam param = imgWriter.getDefaultWriteParam();
      IIOMetadata meta = imgWriter.getDefaultImageMetadata(imgTypeSpec, param);
      addStringToStandardMetadata(meta, metaData);
      IIOImage image = new IIOImage(buffImg, null, meta);
      _instance.writeImageAndWaitForWriterToComplete(imgWriter, image, param);
      ios.close();
      imgWriter.dispose();
    }
    catch (IOException exc)
    {
      if (imgWriter != null)
      {
        imgWriter.dispose();
      }
      throw new CouldNotCreateFileException(fullPathName);
    }
  }

  /**
   * @author Kay Lannen
   * @author Matt Wharton
   * @author George A. David
   */
  static public void savePngImage(java.awt.image.BufferedImage buffImg, String fullPathName) throws CouldNotCreateFileException
  {
    Assert.expect(buffImg != null);
    Assert.expect(fullPathName != null);
    savePngImage(buffImg, fullPathName, "");
//    Assert.expect(buffImg != null);
//    Assert.expect(fullPathName != null);
//
//    ImageWriter imgWriter = null;
//    try
//    {
//      // Crate the directory if needed.
//      String dir = FileUtil.getAbsoluteParent(fullPathName);
//      createImageDirectoryIfNeeded(dir);
//
//      File outputFile = new File(fullPathName);
//      if (!outputFile.canWrite())
//      {
//        if (!outputFile.createNewFile())
//        {
//          throw new CouldNotCreateFileException(fullPathName);
//        }
//      }
//
//      imgWriter = getSpecificImageWriter(_PNG_IMAGE_WRITER_CLASS, _PNG_FORMAT);
//      ImageOutputStream ios = ImageIO.createImageOutputStream(outputFile);
//      imgWriter.setOutput(ios);
//      ImageWriteParam param = imgWriter.getDefaultWriteParam();
//      param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
//      param.setCompressionQuality(_PNG_COMPRESSION_QUALITY);
//      IIOImage image = new IIOImage(buffImg, null, null);
//      imgWriter.write(null, image, param);
//      ios.close();
//      imgWriter.dispose();
//    }
//    catch (IOException exc)
//    {
//      if (imgWriter != null)
//      {
//        imgWriter.dispose();
//      }
//      throw new CouldNotCreateFileException(fullPathName);
//    }
  }

  /**
   * Saves an image to a specified file.
   * This calls the appropriate save routine based on the file extension.
   * The file name and directory name are set in the ImageDescription of the Image.
   * @param image the image

   * @author Kay Lannen
   */
  static public void saveImage(Image image, String filePath) throws CouldNotCreateFileException
  {
    Assert.expect(image != null);
    Assert.expect(filePath != null);

    String extension = FileUtil.getExtension(filePath);

    if ((extension.equalsIgnoreCase(".jpg")) || (extension.equalsIgnoreCase(".jpeg")))
    {
      saveJpegImage(image, filePath);
    }
    else if (extension.equalsIgnoreCase(".png"))
    {
      savePngImage(image, filePath);
    }
    else if (extension.equalsIgnoreCase(".tiff"))
    {
      saveTiffImage(image, filePath);
    }
    else
    {
      throw new CouldNotCreateFileException(filePath);
    }
  }

  /**
   * Loads a Jpeg image from the specified file.
   * The Image includes an ImageDescription and BufferedImage.
   * If only the BufferedImage is needed, use loadJpegBufferedImage.
   * @param filePath the file name of the JPEG image
   * @return Image the loaded image

   * @author Kay Lannen
   */
  static public Image loadJpegImage(String filePath) throws FileDoesNotExistException,
      CouldNotReadFileException
  {
    Assert.expect(filePath != null);

    ImageReader imgReader = initializeJpegImageReader(filePath);
    String metadata = readTextMetadata(imgReader, filePath);
    ImageDescription imgDesc = new ImageDescription(metadata);
    java.awt.image.BufferedImage buffImg = readBufferedImage(imgReader, filePath);
    cleanupImageReader(imgReader, filePath);

    Image newImage = Image.createFloatImageFromBufferedImage(buffImg);
    newImage.setImageDescription(imgDesc);
    return newImage;
  }

  /**
   * Loads a Jpeg image from the specified file.
   * If the ImageDescription is needed, use loadJpegImage instead.
   * @param filePath the file name of the JPEG image
   * @return BufferedImage the loaded image

   * @author Kay Lannen
   */
  static public java.awt.image.BufferedImage loadJpegBufferedImage(String filePath) throws
      FileDoesNotExistException, CouldNotReadFileException
  {
    Assert.expect(filePath != null);

    ImageReader imgReader = initializeJpegImageReader(filePath);
    java.awt.image.BufferedImage buffImg = readBufferedImage(imgReader, filePath);
    cleanupImageReader(imgReader, filePath);
    return buffImg;
  }

  /**
   * Loads a Png image from the specified file.
   * The Image includes an ImageDescription and BufferedImage.
   * If only the BufferedImage is needed, use loadPngBufferedImage.
   * @param filePath the file name of the JPEG image
   * @return Image the loaded image

   * @author Kay Lannen
   */
  static public Image loadPngImage(String filePath) throws FileDoesNotExistException, CouldNotReadFileException
  {
    Assert.expect(filePath != null);

    ImageDescription imgDesc = null;
    java.awt.image.BufferedImage buffImg = null;

    ImageReader imgReader = initializePngImageReader(filePath);
    try
    {
      String metaData = readTextMetadata(imgReader, filePath);
      imgDesc = new ImageDescription(metaData);
      buffImg = readBufferedImage(imgReader, filePath);
    }
    finally
    {
      // should be entered by normal execution or an exception.
      cleanupImageReader(imgReader, filePath);
    }

    Assert.expect(buffImg != null);

    Image newImage = Image.createFloatImageFromBufferedImage(buffImg);
    newImage.setImageDescription(imgDesc);
    return newImage;
  }

  /**
   * Loads a Png image from the specified file.
   * If the ImageDescription is needed, use loadJpegImage instead.
   * @param filePath the file name of the JPEG image
   * @return Image the loaded image

   * @author Kay Lannen
   */
  static public java.awt.image.BufferedImage loadPngBufferedImage(String filePath) throws
      FileDoesNotExistException, CouldNotReadFileException
  {
    Assert.expect(filePath != null);

    ImageReader imgReader = initializePngImageReader(filePath);
    java.awt.image.BufferedImage buffImg = readBufferedImage(imgReader, filePath);
    cleanupImageReader(imgReader, filePath);
    return buffImg;
  }

  /**
   * Opens the image input stream for the JPEG file and initializes an ImageReader
   * to be ready to read the image.
   * @param filePath file name for the image
   * @return an initialized ImageReader
   *
   * @author Kay Lannen
   */
  static private ImageReader initializeJpegImageReader(String filePath) throws
      FileDoesNotExistException, CouldNotReadFileException
  {
    Assert.expect(filePath != null);

    ImageReader imgReader = null;
    File file = new File(filePath);
    if (!file.exists())
    {
      throw new FileDoesNotExistException(filePath);
    }

    try
    {
      ImageInputStream iis = ImageIO.createImageInputStream(file);
      imgReader = getSpecificImageReader(_JPEG_IMAGE_READER_CLASS, _JPEG_FORMAT);
      imgReader.setInput(iis);
      return imgReader;
    }
    catch (IOException exc)
    {
      if (imgReader != null)
      {
        imgReader.dispose();
      }
      throw new CouldNotReadFileException(filePath);
    }
  }

  /**
   * Loads an image from the specified file.
   * The Image includes an ImageDescription and BufferedImage.
   * This calls the appropriate load routine based on the file extension.
   * If only the BufferedImage is needed, use loadPngBufferedImage or loadJpegBufferedImage.
   * @param filePath the file name of the JPEG image
   * @return Image the loaded image

   * @author Kay Lannen
   */
  static public Image loadImage(String filePath) throws FileDoesNotExistException,
      CouldNotReadFileException
  {
    Assert.expect(filePath != null);

    Image image = null;
    String extension = FileUtil.getExtension(filePath);

    if ((extension.equalsIgnoreCase(".jpg")) || (extension.equalsIgnoreCase(".jpeg")))
    {
      image = loadJpegImage(filePath);
    }
    else if (extension.equalsIgnoreCase(".png"))
    {
      image = loadPngImage(filePath);
    }
    else
    {
      throw new CouldNotReadFileException(filePath);
    }

    return image;
  }

  /**
   * Opens the image input stream for the PNG file and initializes an ImageReader
   * to be ready to read the image.
   * @param filePath file name for the image
   * @return an initialized ImageReader
   *
   * @author Kay Lannen
   */
  static private ImageReader initializePngImageReader(String filePath) throws
      FileDoesNotExistException, CouldNotReadFileException
  {
    Assert.expect(filePath != null);

    ImageReader imgReader = null;
    File file = new File(filePath);
    if (!file.exists())
    {
      throw new FileDoesNotExistException(filePath);
    }

    try
    {
      ImageInputStream iis = ImageIO.createImageInputStream(file);
      imgReader = getSpecificImageReader(_PNG_IMAGE_READER_CLASS, _PNG_FORMAT);
      imgReader.setInput(iis);
      return imgReader;
    }
    catch (IOException exc)
    {
      if (imgReader != null)
      {
        imgReader.dispose();
      }
      throw new CouldNotReadFileException(filePath);
    }
  }

  /**
   * Returns the string metadata which is stored within the image header.
   * @param imgReader an initialized ImageReader
   * @param filePath the file name for the image
   * @return String containing the text metadata stored within the image
   *
   * @author Kay Lannen
   */
  static private String readTextMetadata(ImageReader imgReader, String filePath) throws
      CouldNotReadFileException
  {
    Assert.expect(imgReader != null);
    Assert.expect(filePath != null);

    try
    {
      IIOMetadata meta = imgReader.getImageMetadata(0);
      IIOMetadataNode root = (IIOMetadataNode)meta.getAsTree(_STD_METADATA_FORMAT);
      NodeList textEntryNodes = root.getElementsByTagName("TextEntry");
      String metadata = "";
      if (textEntryNodes.getLength() > 0)
      {
        for (int i = 0; i < textEntryNodes.getLength(); ++i)
        {
          Node teNode = textEntryNodes.item(i);
          NamedNodeMap attrs = teNode.getAttributes();
          if (attrs == null)
          {
            break;
          }

          Node valueNode = attrs.getNamedItem("value");
          if (valueNode == null)
          {
            break;
          }

          metadata = metadata + valueNode.getNodeValue();
        }
      }
      return metadata;
    }
    catch (IOException exc)
    {
      if (imgReader != null)
      {
        imgReader.dispose();
      }
      throw new CouldNotReadFileException(filePath);
    }
  }

  /**
   * Reads the buffered image from the file.
   * @param imgReader an initialized ImageReader
   * @param filePath the file name for the image
   * @return a java.awt.image.BufferedImage containing the image data
   *
   * @author Kay Lannen
   */
  static private java.awt.image.BufferedImage readBufferedImage(ImageReader imgReader, String filePath) throws
      CouldNotReadFileException
  {
    Assert.expect(imgReader != null);
    Assert.expect(filePath != null);

    java.awt.image.BufferedImage buffImg;
    try
    {
      buffImg = imgReader.read(0);
    }
    catch (IOException exc)
    {
      if (imgReader != null)
      {
        imgReader.dispose();
      }
      throw new CouldNotReadFileException(filePath);
    }
    return buffImg;
  }

  /**
   * Returns an instance of the requested ImageWriter.
   *
   * This function is unfortunately necessary with the Java Image I/O package.  The
   * standard access functions in javax.imageio return a list of image writers
   * which are not guaranteed to be in any particular order and which work
   * differently.  For instance, some of the writers do not support metadata
   * at all.  Use of this function will ensure that the same writer is loaded
   * every time.
   * @param className the full class name of the desired ImageWriter, such as "com.sun.imageio.plugins.jpeg.JPEGImageWriter"
   * @param formatName the name of the desired image format, such as "JPEG"
   * @return an instance of the requested image writer (null if none is found)
   * @author Kay Lannen
   */
  static private ImageWriter getSpecificImageWriter(String className, String formatName)
  {
    Assert.expect(className != null);
    Assert.expect(formatName != null);

    ImageWriter imgWriter = null;
    Iterator<ImageWriter> it = ImageIO.getImageWritersByFormatName(formatName);
    while (it.hasNext())
    {
      imgWriter = it.next();
      if (imgWriter.getClass().getName().compareTo(className) == 0)
      {
        break;
      }
      else
      {
        imgWriter = null;
      }
    }
    Assert.expect(imgWriter != null);
    return imgWriter;
  }

  /**
   * Returns an instance of the requested ImageWriter.
   *
   * This function is unfortunately necessary with the Java Image I/O package.  The
   * standard access functions in javax.imageio return a list of image writers
   * which are not guaranteed to be in any particular order and which work
   * differently.  For instance, some of the writers do not support metadata
   * at all.  Use of this function will ensure that the same writer is loaded
   * every time.
   * @param className the full class name of the desired ImageWriter, such as "com.sun.imageio.plugins.jpeg.JPEGImageWriter"
   * @param formatName the name of the desired image format, such as "JPEG"
   * @return an instance of the requested image writer (null if none is found)
   * @author Kay Lannen
   */
  static private ImageWriter getSpecificImageWriter(String formatName)
  {
    Assert.expect(formatName != null);

    ImageWriter imgWriter = null;
    Iterator<ImageWriter> it = ImageIO.getImageWritersByFormatName(formatName);
    if(it.hasNext())
    {
      imgWriter = (ImageWriter) it.next();
    }
    return imgWriter;
  }

//  /**
//   * @author George A. David
//   */
//  static public ImageWriter getImageWriter(String className, String formatName)
//  {
//    Assert.expect(className != null);
//    Assert.expect(formatName != null);
//
//    ImageWriter imgWriter = null;
//    Iterator<ImageWriter> it = ImageIO.getImageWritersByFormatName(formatName);
//    while (it.hasNext())
//    {
//      imgWriter = it.next();
//      ImageWriteParam param = imgWriter.getDefaultWriteParam();
//      if(param.canWriteCompressed())
//      {
//        System.out.println(imgWriter.getClass().getName());
//        param.setCompressionMode(param.MODE_EXPLICIT);
//        for (float quality : param.getCompressionQualityValues())
//          System.out.print(quality + ", ");
//        System.out.println();
//        for (String desc : param.getCompressionQualityDescriptions())
//          System.out.print(desc + ", ");
//        System.out.println();
//      }
//    }
//    return imgWriter;
//  }

  /**
   * Returns an instance of the requested ImageReader.
   *
   * This function is unfortunately necessary with the Java Image I/O package.  The
   * standard access functions in javax.imageio return a list of image readers
   * which are not guaranteed to be in any particular order and which work
   * differently.  For instance, some of the readers do not support metadata
   * at all.    Use of this function will ensure that the same reader is loaded
   * every time.
   * @param className the full class name of the desired ImageReader, such as "com.sun.imageio.plugins.jpeg.JPEGImageReader"
   * @param formatName the name of the desired image format, such as "JPEG"
   * @return an instance of the requested image reader (null if none is found)
   * @author Kay Lannen
   */
  static private ImageReader getSpecificImageReader(String className, String formatName)
  {
    Assert.expect(className != null);
    Assert.expect(formatName != null);

    ImageReader imgReader = null;
    Iterator<ImageReader> it = ImageIO.getImageReadersByFormatName(formatName);
    while (it.hasNext())
    {
      imgReader = it.next();
      if (imgReader.getClass().getName().compareTo(className) == 0)
      {
        break;
      }
      else
      {
        imgReader = null;
      }
    }
    return imgReader;
  }


  /**
   * Inserts a string into the metadata for a Image IO image.
   * @param meta the IIOMetadata object containing the current metadata
   * @param str the new text string to be inserted within the metadata
   * @author Kay Lannen
   */
  static private void addStringToStandardMetadata(IIOMetadata meta, String str)
  {
    Assert.expect(meta != null);
    Assert.expect(str != null);

    // Note:  There is a bug in Java Image I/O 1.0 which prevents this from
    // working with JPEG images, but it does work with PNG images.  The
    // workaround for JPEG images is to use the JPEG native metadata format.
    Assert.expect(meta != null);
    IIOMetadataNode metadataRootNode = (IIOMetadataNode)meta.getAsTree(_STD_METADATA_FORMAT);
    IIOMetadataNode textNode = new IIOMetadataNode("Text");
    IIOMetadataNode textEntryNode = new IIOMetadataNode("TextEntry");

    textEntryNode.setAttribute("keyword", "comment");
    textEntryNode.setAttribute("value", str);
    textEntryNode.setAttribute("encoding", "ISO-8859-1");
    textEntryNode.setAttribute("language", "en");
    textEntryNode.setAttribute("compression", "none");

    textNode.appendChild(textEntryNode);
    metadataRootNode.appendChild(textNode);

    try
    {
      meta.setFromTree(_STD_METADATA_FORMAT, metadataRootNode);
    }
    catch (IIOInvalidTreeException exc)
    {
      Assert.expect(false, "Could not insert text into standard metadata");
    }
  }

  /**
   * Inserts a string into the metadata for a Jpeg image.
   * @param meta the IIOMetadata object containing the current metadata
   * @param str the new text string to be inserted within the metadata
   * @author Kay Lannen
   */
  static private void addStringToJpegMetadata(IIOMetadata meta, String str)
  {
    Assert.expect(meta != null);
    Assert.expect(str != null);

    IIOMetadataNode metadataRootNode = (IIOMetadataNode)meta.getAsTree(_JPEG_METADATA_FORMAT);
    IIOMetadataNode commentNode = new IIOMetadataNode("com");
    commentNode.setAttribute("comment", str);
    NodeList markerSeqList = metadataRootNode.getElementsByTagName("markerSequence");
    if (markerSeqList.getLength() > 0)
    {
      Node markerSeqNode = markerSeqList.item(0);
      Node markerSeqFirstChild = markerSeqNode.getFirstChild();
      if (markerSeqFirstChild != null)
      {
        markerSeqNode.insertBefore(commentNode, markerSeqFirstChild);
      }
      else
      {
        markerSeqNode.appendChild(commentNode);
      }
    }

    try
    {
      meta.setFromTree(_JPEG_METADATA_FORMAT, metadataRootNode);
    }
    catch (IIOInvalidTreeException exc)
    {
      // We should never end up here.
      Assert.expect(false, "Problem adding image information to Jpeg Metadata");
    }
  }

  /**
   * Closes the input streams associated with the image reader and frees memory
   * used by the image reader.
   * @param imgReader ImageReader to be closed
   * @param filePath String name of the file associated with the image reader
   *
   * @author Kay Lannen
   */
  private static void cleanupImageReader(ImageReader imgReader, String filePath) throws
      CouldNotReadFileException
  {
    Assert.expect(imgReader != null);
    Assert.expect(filePath != null);

    try
    {
      ImageInputStream iis = (ImageInputStream)imgReader.getInput();
      iis.close();
      imgReader.dispose(); // Important to do this to avoid memory leaks!
    }
    catch (IOException ex)
    {
      throw new CouldNotReadFileException(filePath);
    }
  }

  /**
   * FOR DEBUGGING USE ONLY!!!
   * Loads a full-precision image from the specified file.
   * If the ImageDescription is needed, use loadFullPrecisionImage instead.
   * @param filePath the file name of the full-precision image
   * @return Image the loaded image
   *
   * @author John Heumann
   */
  static public java.awt.image.BufferedImage loadFullPrecisionBufferedImage(String filePath) throws
      FileDoesNotExistException, CouldNotReadFileException
  {
    Assert.expect(filePath != null);

    Image image = null;
    try
    {
      image = loadFullPrecisionImage(filePath);
    }
    catch (IOException exc)
    {
      throw new CouldNotReadFileException(filePath);
    }

    Assert.expect(image != null);
    java.awt.image.BufferedImage buffImg = image.getBufferedImage();
    return buffImg;
  }

  /**
   * FOR DEBUGGING USE ONLY!!!
   * Loads a full-precision image from the specified file.
   * The Image includes an empty ImageDescription and BufferedImage.
   * If only the BufferedImage is needed, use loadFullPrecisionBufferedImage.
   * @param filePath the file name of the full-precision image
   * @return Image the loaded image
   *
   * @author Kay Lannen
   */
  static public Image loadFullPrecisionImage(String filePath) throws FileDoesNotExistException, CouldNotReadFileException
  {
    Assert.expect(filePath != null);

    DataInputStream in = null;
    Image image = null;
    try
    {
      in = new DataInputStream(new BufferedInputStream(new FileInputStream(filePath)));
      int nRows = in.readInt();
      int nCols = in.readInt();
      image = new Image(nCols, nRows);
      for (int i = 0; i < nCols; i++)
      {
        for (int j = 0; j < nRows; j++)
        {
          image.setPixelValue(i, j, in.readFloat());
        }
      }
    }
    catch (IOException exc)
    {
      throw new CouldNotReadFileException(filePath);
    }
    finally {
      try
      {
        if (in != null)
          in.close();
      }
      catch (IOException exc)
      {
        throw new CouldNotReadFileException(filePath);
      }
    }

    ImageDescription imageDescription = new ImageDescription("");
    image.setImageDescription(imageDescription);
    return image;
  }

  /**
   * FOR DEBUGGING USE ONLY!!!
   * @author John Heumann
   */
  static public void saveFullPrecisionImage(java.awt.image.BufferedImage buffImg, String fullPathName)
      throws CouldNotCreateFileException
  {
    Image image = Image.createFloatImageFromBufferedImage(buffImg);
    saveFullPrecisionImage(image, fullPathName);
  }

  /**
   * FOR DEBUGGING USE ONLY!!!
   * Write an image maintaining full floating point precision
   * @author John Heumann
   */
  static public void saveFullPrecisionImage(Image image, String fullPathName) throws CouldNotCreateFileException
  {
    Assert.expect(image != null);

    DataOutputStream out = null;
    try
    {
      // Crate the directory if needed.
      String dir = FileUtil.getAbsoluteParent(fullPathName);
      createImageDirectoryIfNeeded(dir);

      out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(fullPathName)));
      final int nRows = image.getHeight();
      final int nCols = image.getWidth();
      out.writeInt(nRows);
      out.writeInt(nCols);
      for (int i = 0; i < nCols; i++)
      {
        for (int j = 0; j < nRows; j++)
        {
          out.writeFloat(image.getPixelValue(i, j));
        }
      }
    }
    catch (IOException exc)
    {
      throw new CouldNotCreateFileException(fullPathName);
    }
    finally
    {
      try
      {
        if (out != null)
          out.close();
      }
      catch (IOException exc)
      {
        throw new CouldNotCreateFileException(fullPathName);
      }
    }
  }

  /**
   * Opens the image input stream for the TIFF file and initializes an ImageReader
   * to be ready to read the image.
   * @param filePath file name for the image
   * @return an initialized ImageReader
   *
   * @author Chong, Wei Chin
   */
  static private ImageReader initializeTiffImageReader(String filePath) throws
      FileDoesNotExistException, CouldNotReadFileException
  {
    Assert.expect(filePath != null);

    ImageReader imgReader = null;
    File file = new File(filePath);
    if (!file.exists())
    {
      throw new FileDoesNotExistException(filePath);
    }

    try
    {
      ImageInputStream iis = ImageIO.createImageInputStream(file);
      imgReader = getSpecificImageReader(_TIFF_IMAGE_READER_CLASS, _TIFF_FORMAT);
      imgReader.setInput(iis);
      return imgReader;
    }
    catch (IOException exc)
    {
      if (imgReader != null)
      {
        imgReader.dispose();
      }
      throw new CouldNotReadFileException(filePath);
    }
  }  

  /**
   * Loads a Tiff image from the specified file.
   * The Image includes an ImageDescription and BufferedImage.
   * If only the BufferedImage is needed, use loadPngBufferedImage.
   * @param filePath the file name of the TIFF image
   * @return Image the loaded image

   * @author Chong, Wei Chin
   */
  static public Image loadTiffImage(String filePath) throws FileDoesNotExistException, CouldNotReadFileException
  {
    Assert.expect(filePath != null);

    ImageDescription imgDesc = null;
    java.awt.image.BufferedImage buffImg = null;

    ImageReader imgReader = initializeTiffImageReader(filePath);
    try
    {
      String metaData = readTextMetadata(imgReader, filePath);
      imgDesc = new ImageDescription(metaData);
      buffImg = readBufferedImage(imgReader, filePath);
    }
    finally
    {
      // should be entered by normal execution or an exception.
      cleanupImageReader(imgReader, filePath);
    }

    Assert.expect(buffImg != null);

    Image newImage = Image.createFloatImageFromBufferedImage(buffImg);
    newImage.setImageDescription(imgDesc);
    return newImage;
  }

  /**
   * Creates metadata from the image's ImageDescription.
   * @author Chong, Wei Chin
   */
  static public void saveTiffImage(Image image, String fullPathName) throws CouldNotCreateFileException
  {
    Assert.expect(image != null);
    Assert.expect(fullPathName != null);

    java.awt.image.BufferedImage bufferedImage = image.get32BitBufferedImage();
    if (image.hasImageDescription())
    {
      String propertyString = image.getImageDescription().createStringDescription();
      saveTiffImage(bufferedImage, fullPathName, propertyString);
    }
    else
    {
      saveTiffImage(bufferedImage, fullPathName, "");
    }
  }

  /**
   * Saves a buffered image to disk as a TIFF image with embedded text metadata.
   * @param buffImg buffered image to be saved
   * @param filePath filename of the image
   * @param metaData metadata string to be stored within the image header

   * @author Chong, Wei Chin
   */
  static public void saveTiffImage(java.awt.image.BufferedImage buffImg,
                                   String filePath,
                                   String metaData) throws CouldNotCreateFileException
  {
    Assert.expect(buffImg != null);
    Assert.expect(filePath != null);
    Assert.expect(metaData != null);

    ImageWriter imgWriter = null;
    try
    {
      // Create the directory if needed.
      String dir = FileUtil.getAbsoluteParent(filePath);
      createImageDirectoryIfNeeded(dir);

      File outputFile = new File(filePath);
      if (!outputFile.canWrite())
      {
        if (!outputFile.createNewFile())
        {
          throw new CouldNotCreateFileException(filePath);
        }
      }

      imgWriter = getSpecificImageWriter(_TIFF_IMAGE_WRITER_CLASS, _TIFF_FORMAT);
      ImageOutputStream ios = ImageIO.createImageOutputStream(outputFile);
      imgWriter.setOutput(ios);
      IIOMetadata meta = null;
      ImageWriteParam param = imgWriter.getDefaultWriteParam();
      param.setCompressionMode(ImageWriteParam.MODE_DEFAULT);
      if(metaData.length() > 0)
      {
        ImageTypeSpecifier imgTypeSpec = new ImageTypeSpecifier(buffImg.getColorModel(), buffImg.getSampleModel());
        meta = imgWriter.getDefaultImageMetadata(imgTypeSpec, param);
        addStringToTiffMetadata(meta, metaData);
      }
      IIOImage image = new IIOImage(buffImg, null, meta);
      imgWriter.write(meta, image, param);

      ios.close();
      imgWriter.dispose();
    }
    catch (IOException exc)
    {
      if (imgWriter != null)
      {
        imgWriter.dispose();
      }
      throw new CouldNotCreateFileException(filePath);
    }
  }

  /**
   * Inserts a string into the metadata for a Tiff image.
   * @param meta the IIOMetadata object containing the current metadata
   * @param str the new text string to be inserted within the metadata
   * @author Chong, Wei Chin
   */
  static private void addStringToTiffMetadata(IIOMetadata meta, String str)
  {
    Assert.expect(meta != null);
    Assert.expect(str != null);

    IIOMetadataNode metadataRootNode = (IIOMetadataNode)meta.getAsTree(_TIFF_METADATA_FORMAT);

    Node markerSeqNode = metadataRootNode.getChildNodes().item(0);
    TIFFTagSet set = BaselineTIFFTagSet.getInstance();

    String[] value = { str };
    TIFFTag tag = set.getTag(BaselineTIFFTagSet.TAG_IMAGE_DESCRIPTION);
    TIFFField field = new TIFFField(tag, TIFFTag.TIFF_ASCII, 1, value);
    markerSeqNode.appendChild(field.getAsNativeNode());

    try
    {
      meta.mergeTree(_TIFF_METADATA_FORMAT, metadataRootNode);
    }
    catch (IIOInvalidTreeException exc)
    {
      // We should never end up here.
      Assert.expect(false, "Problem adding image information to Tiff Metadata");
    }
  }

  /**
   * @author sham
   */
  static public void saveTiffImage(BufferedImage image, String fullPathName) throws CouldNotCreateFileException
  {
    Assert.expect(image != null);
    Assert.expect(fullPathName != null);

    java.awt.image.BufferedImage bufferedImage = image;
    saveTiffImage(bufferedImage, fullPathName, "");
  }
  
  /**
   * @author Patrick Lacz
   */
  private static native void nativeSavePng(int imageDefinition[], String fullFilePath, String commentString);
}
