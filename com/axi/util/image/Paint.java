package com.axi.util.image;

import java.nio.*;

import com.axi.util.*;
import java.awt.Rectangle;

/**
 * Paint class
 *
 * This class holds functions to draw and fill into images.
 * Many of these methods return the number of pixels modified.
 *
 * @author Patrick Lacz
 */
public class Paint
{
  /**
   * @author Patrick Lacz
   */
  private Paint()
  {
    // do nothing
    // methods in Paint are staticly defined.
  }

  /**
   * @author Patrick Lacz
   */
  static
  {
    System.loadLibrary("nativeImageUtil");
  }

  /**
   * @todo This should probably not be in the 'transform' object.
   * @todo Create Regression Tests
   * @author Patrick Lacz
   */
  public static int fillImage(Image image, float value)
  {
    Assert.expect(image != null);

    return fillRegionOfInterest(image, RegionOfInterest.createRegionFromImage(image), value);
  }

  /**
   * Fills a rectangular region of interest with the given value.
   * @todo This should wrap a 'fill rectangle' and a modification of the 'fill circle' call.
   * @todo Create Regression Tests
   * @author Patrick Lacz
   */
  public static int fillRegionOfInterest(Image image, RegionOfInterest roi, float value)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    nativeFill(image.getNativeDefinition(), roi.getMinX(), roi.getMinY(), roi.getWidth(), roi.getHeight(), value);

    // return the number of pixels modified.
    return roi.getWidth()*roi.getHeight();
  }
  
  /**
   * Fills a rectangular shape with the given value.
   * @todo This should wrap a 'fill rectangle' and a modification of the 'fill circle' call.
   * @todo Create Regression Tests
   * @author Patrick Lacz
   * @author sham
   */
  public static int fillRegion(Image image, Rectangle rec, float value)
  {
    Assert.expect(image != null);
    Assert.expect(rec != null);
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    nativeFill(image.getNativeDefinition(), (int)rec.getMinX(), (int)rec.getMinY(), (int)rec.getWidth(), (int)rec.getHeight(), value);

    // return the number of pixels modified.
    return (int)(rec.getWidth()*rec.getHeight());
  }
  /**
   * @author Patrick Lacz
   */
  public static int fillGradientXRegionOfInterest(Image image, RegionOfInterest roi, float westPixelValue,
      float eastPixelValue)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    float slope = (eastPixelValue - westPixelValue) / (roi.getWidth() - 1);
    nativeGradient(image.getNativeDefinition(),
                   roi.getMinX(), roi.getMinY(),
                   roi.getWidth(), roi.getHeight(),
                   westPixelValue, slope, true);

    // return the number of pixels modified.
    return roi.getWidth()*roi.getHeight();

  }

  /**
   * @author Patrick Lacz
   */
  public static int fillGradientYRegionOfInterest(Image image, RegionOfInterest roi, float northPixelValue,
      float southPixelValue)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    float slope = (southPixelValue - northPixelValue) / (roi.getHeight() - 1);
    nativeGradient(image.getNativeDefinition(),
                   roi.getMinX(), roi.getMinY(),
                   roi.getWidth(), roi.getHeight(),
                   northPixelValue, slope, false);

    // return the number of pixels modified.
    return roi.getWidth()*roi.getHeight();
  }

  /**
  * Create an obround kernel image.
  * The background is un-initialized. Use Transform.fillImage to initalize the background pixels before
  * using this method.
  * This function just draw a rectangle at center with 2 circle on each side.
  *
  * @param image The image to write the circle into.
  * @param roi Drawing is limited to this region; if the obround is entirely outside of this roi, nothing will be drawn.
  * @param obroundCenter The coordinate to draw the obround.
  * @param color The float value to draw the circle in.
  * @return The number of pixels in the circle.
  *
  * @author Seng-Yew Lim
  */
  public static int fillObround(Image image, RegionOfInterest roi,  DoubleCoordinate obroundCenter, float color)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(obroundCenter != null);
    Assert.expect(roi.fitsWithinImage(image));

    if (roi.getWidth()==roi.getHeight())
    {
      return fillCircle(image, roi, obroundCenter, roi.getWidth()/2, color);
    }
    int pixelsModified = 0;
    int circleDiameter = Math.min(roi.getWidth(),roi.getHeight());
    int circleRadius = circleDiameter/2;

    int circleCenterX1 = 0;
    int circleCenterY1 = 0;
    int circleCenterX2 = 0;
    int circleCenterY2 = 0;
    if (roi.getHeight() > roi.getWidth())
    {
      circleCenterX1 = (int)roi.getCenterX();
      circleCenterY1 = (int)(roi.getCenterY())+(roi.getHeight()-roi.getWidth())/2;
      circleCenterX2 = (int)roi.getCenterX();
      circleCenterY2 = (int)(roi.getCenterY())-(roi.getHeight()-roi.getWidth())/2;
    }
    else
    {
      circleCenterX1 = (int)(roi.getCenterX())+(roi.getWidth()-roi.getHeight())/2;
      circleCenterY1 = (int)roi.getCenterY();
      circleCenterX2 = (int)(roi.getCenterX())-(roi.getWidth()-roi.getHeight())/2;
      circleCenterY2 = (int)roi.getCenterY();
    }

    RegionOfInterest circleROI1 = RegionOfInterest.createRegionFromRegionCenter(circleCenterX1,circleCenterY1,circleDiameter,circleDiameter,roi.getOrientationInDegrees(),RegionShapeEnum.OBROUND);
    RegionOfInterest circleROI2 = RegionOfInterest.createRegionFromRegionCenter(circleCenterX2,circleCenterY2,circleDiameter,circleDiameter,roi.getOrientationInDegrees(),RegionShapeEnum.OBROUND);
    pixelsModified += fillCircle(image,circleROI1,new DoubleCoordinate(circleCenterX1,circleCenterY1),circleRadius,color);
    pixelsModified += fillCircle(image,circleROI2,new DoubleCoordinate(circleCenterX2,circleCenterY2),circleRadius,color);

    int rectangleCenterX = (int)roi.getCenterX();
    int rectangleCenterY = (int)roi.getCenterY();
    int rectangleWidth = 0;
    int rectangleHeight = 0;
    if (roi.getHeight() > roi.getWidth())
    {
      rectangleWidth = roi.getWidth();
      rectangleHeight = roi.getHeight()-roi.getWidth();
    }
    else
    {
      rectangleWidth = roi.getWidth()-roi.getHeight();
      rectangleHeight = roi.getHeight();
    }

    RegionOfInterest rectangleROI = RegionOfInterest.createRegionFromRegionCenter(rectangleCenterX,rectangleCenterY,rectangleWidth,rectangleHeight,0,RegionShapeEnum.RECTANGULAR);
    pixelsModified += fillRegionOfInterest(image, rectangleROI, color);

    return pixelsModified;
  }

  /**
   * Create a circular (or semi-circular) kernel image.
   * The background is un-initialized. Use Transform.fillImage to initalize the background pixels before
   * using this method.
   *
   * @param roi Drawing is limited to this region; if the circle is entirely outside of this roi, nothing will be drawn.
   * @param image The image to write the circle into.
   * @param color The float value to draw the circle in.
   * @return The number of pixels in the circle.
   *
   * @author Patrick Lacz
   */
  public static int fillCircle(Image image, RegionOfInterest roi,  DoubleCoordinate circleCenter, double radius, float color)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(circleCenter != null);
    Assert.expect(roi.fitsWithinImage(image));
    int pixelsModified = 0;

    int minY = roi.getMinY();
    int maxY = roi.getMaxY();
    int minX = roi.getMinX();
    int maxX = roi.getMaxX();

    double x0Squared = circleCenter.getX() * circleCenter.getX();
    double radiusSquared = radius * radius;

    for (int y = minY ; y <= maxY ; ++y)
    {
      // we use the implict formula for a circle
      //   0 = (x-x0)^2 + (y-y0)^2 - R^2
      // solve the quadratic equation for this row
      // A = 1
      // B = -2*x0
      // C = x0^2 + (y-y0)^2-R^2
      double A = 1.0;
      double B = -2.0*circleCenter.getX();
      double differenceInY = (y+0.5-circleCenter.getY());
      double C = x0Squared + differenceInY * differenceInY - radiusSquared;

      double underSqrt = B*B - 4*A*C;
      if (underSqrt < 0.0) // imaginary roots -- no circle intersection
        continue;

      double result0 = (-B - Math.sqrt(underSqrt))/(2*A);
      double result1 = (-B + Math.sqrt(underSqrt))/(2*A);

      // bias rounding down
      int beginX = Math.max((int)Math.round(result0 - 0.0001), minX);
      // bias rounding up
      int endX = Math.min((int)Math.round(result1 + 0.0001) - 1, maxX);

      if (beginX <= endX)
      {
        image.setPixelRowValues(beginX, endX, y, color);
        pixelsModified += endX - beginX + 1;
      }
    }
    return pixelsModified;
  }

  /**
   * @author Patrick Lacz
   */
  public static int floodFillExact(Image image, RegionOfInterest roi, int seedX, int seedY, float fillValue)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));

    int numberOfPixelsModified = nativeFloodFillExact(image.getNativeDefinition(),
                                                      roi.getMinX(), roi.getMinY(), roi.getWidth(), roi.getHeight(),
                                                      seedX, seedY, fillValue);

    return numberOfPixelsModified;
  }

  /**
   * @author Patrick Lacz
   */
  public static int floodFillRange(Image image, RegionOfInterest roi, int seedX, int seedY, float fillValue, float minBelowSeed, float maxAboveSeed)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));

    int numberOfPixelsModified = nativeFloodFillRange(image.getNativeDefinition(),
                                                      roi.getMinX(), roi.getMinY(), roi.getWidth(), roi.getHeight(),
                                                      seedX, seedY, fillValue, minBelowSeed, maxAboveSeed);

    return numberOfPixelsModified;
  }

  /**
   * @author Patrick Lacz
   */
  public static int floodFillGradient(Image image, RegionOfInterest roi, int seedX, int seedY, float fillValue, float minBelowNeighbor, float maxAboveNeighbor)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));

    int numberOfPixelsModified = nativeFloodFillGradient(image.getNativeDefinition(),
                                                      roi.getMinX(), roi.getMinY(), roi.getWidth(), roi.getHeight(),
                                                      seedX, seedY, fillValue, minBelowNeighbor, maxAboveNeighbor);

    return numberOfPixelsModified;
  }

  /**
   * @author Patrick Lacz
   */
  private static native void nativeFill(int[] sourceImage,
                                        int roiMinX, int roiMinY, int roiWidth, int roiHeight,
                                        float value);

  /**
   * @author Patrick Lacz
   */
  private static native void nativeGradient(int[] sourceImage,
                                            int roiMinX, int roiMinY, int roiWidth, int roiHeight,
                                            float startValue, float slope, boolean isHorizontal);

  /**
   * Returns the number of pixels filled.
   * @author Patrick Lacz
   */
  private static native int nativeFloodFillExact(int[] sourceImage, int roiMinX, int roiMinY, int roiWidth, int roiHeight,
                                                  int seedX, int seedY, float newValue);

  /**
   * Returns the number of pixels filled.
   * @author Patrick Lacz
   */
  private static native int nativeFloodFillRange(int[] sourceImage, int roiMinX, int roiMinY, int roiWidth, int roiHeight,
                                                  int seedX, int seedY, float newValue, float minBelowSeed, float maxAboveSeed);

  /**
   * Returns the number of pixels filled.
   * @author Patrick Lacz
   */
  private static native int nativeFloodFillGradient(int[] sourceImage, int roiMinX, int roiMinY, int roiWidth, int roiHeight,
                                                  int seedX, int seedY, float newValue, float minBelowNeighbor, float maxAboveNeighbor);


}
