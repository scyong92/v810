package com.axi.util.image;

import java.io.*;
import java.awt.*;
import java.awt.image.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Kay Lannen
 * @author Patrick Lacz
 */

public class Test_ImageIoUtil extends UnitTest
{
  // Setup data for test
  String _dirPath = "test";
  BufferedImage _buffImg = getGradientImage();
  String _jpgFileName1 = "testimage_jpg1.jpg";
  String _jpgFileName2 = "testimage_jpg2.jpg";
  String _pngFileName1 = "testimage_png1.png";
  String _pngFileName2 = "testimage_png2.png";
  String _strMetadata = "testMetadata=10\n";

  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ImageIoUtil());
  }

  public void test(BufferedReader is, PrintWriter os)
  {

    // Run the tests once to verify that all methods are working.
    testAllPublicMethods();

    // Test for memory leaks in any ImageIoUtil routines
    long memoryBound = getMemoryUsage();
    int loopCount = 0;
    int memoryBoundIndex = 0;
    int minLoops = 10; //Check memory usage for 10 loops after a new bound is set
    int maxLoops = 50; //Test fails if we have to check more than 50 loops total
    while ((loopCount < maxLoops) && (loopCount < (memoryBoundIndex + minLoops)))
    {
      testAllPublicMethods();

      long usedMemory = getMemoryUsage();
      if (usedMemory > memoryBound)
      {
        // If the memory usage has increased, we have a new bound.
        memoryBound = usedMemory;
        memoryBoundIndex = loopCount;
      }
      loopCount++;
    }
    Expect.expect(loopCount < maxLoops); // If this test fails, a memory leak is likely.
  }

  private void testAllPublicMethods()
  {

    // Test ImageIoUtil routines
    testDirectoryCreation(_dirPath);
    testJpegImageSave(_buffImg, _dirPath, _jpgFileName1, _strMetadata);
    testPngImageSave(_buffImg, _dirPath, _pngFileName1, _strMetadata);
    testJpegImageLoad(_dirPath + File.separator + _jpgFileName1, _strMetadata, _buffImg);
    testPngImageLoad(_dirPath + File.separator + _pngFileName1, _strMetadata, _buffImg);
    testSaveAndLoadImage(_buffImg, _dirPath + File.separator + _jpgFileName2);
    testSaveAndLoadImage(_buffImg, _dirPath + File.separator + _pngFileName2);
    testExceptions();
  }

  private void testDirectoryCreation(String dirPath)
  {
    try
    {
      ImageIoUtil.createImageDirectoryIfNeeded(dirPath);
      ImageIoUtil.createImageDirectoryIfNeeded(dirPath);
    }
    catch (Exception exc)
    {
      Expect.expect(false, "Exception occurred during image directory creation: " + exc.getMessage());
    }
  }

  private void testJpegImageSave(BufferedImage buffImg, String dirPath, String fileName,
                                 String strMetadata)
  {
    try
    {
      ImageIoUtil.saveJpegImage(buffImg, dirPath + File.separator + fileName, strMetadata);
    }
    catch (CouldNotCreateFileException exc)
    {
      Expect.expect(false, "Exception occurred during JPEG file save: " + exc.getMessage());
    }
  }

  private void testPngImageSave(BufferedImage buffImg, String dirPath, String fileName,
                                String strMetadata)
  {
    try
    {
      ImageIoUtil.savePngImage(buffImg, dirPath + File.separator + fileName, strMetadata);
    }
    catch (CouldNotCreateFileException exc)
    {
      Expect.expect(false, "Exception occurred during PNG file save: " + exc.getMessage());
    }
  }

  private void testSaveAndLoadImage(BufferedImage buffImg, String filePath)
  {
    ImageDescription imageDesc = new ImageDescription("");
    imageDesc.setParameter("testNumber", 123.456);
    imageDesc.setParameter("testString", "Some strange string");

    Image imageToSave = Image.createFloatImageFromBufferedImage(buffImg);
    imageToSave.setImageDescription(imageDesc);

    try
    {
      ImageIoUtil.saveImage(imageToSave, filePath);
    }
    catch (CouldNotCreateFileException exc)
    {
      Expect.expect(false, "Exception occurred during file save: " + exc.getMessage());
    }

    Image imageLoaded;
    try
    {
      imageLoaded = ImageIoUtil.loadImage(filePath);
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(false, "ImageDoesNotExistsException occurred during file load: " + exc.getMessage());
      return;
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "CouldNotReadFileException occured during file load: " + exc.getMessage());
      return;
    }

    ImageDescription imageDesc2 = imageLoaded.getImageDescription();
    Expect.expect(imageDesc2.hasParameter("testNumber"));
    Expect.expect(imageDesc2.getParameter("testNumber") instanceof Number);
    Expect.expect(MathUtil.fuzzyEquals(((Number)imageDesc2.getParameter("testNumber")).doubleValue(), 123.456));
    Expect.expect(imageDesc2.hasParameter("testString"));
    Expect.expect(imageDesc2.getParameter("testString") instanceof String);
    Expect.expect(((String)imageDesc2.getParameter("testString")).compareTo("Some strange string") == 0);
    imageLoaded.decrementReferenceCount();
    imageToSave.decrementReferenceCount();
  }

  private void testJpegImageLoad(String filePath, String expectMetadata,
                                 BufferedImage expectBuffImg)
  {
    try
    {
      Image image = ImageIoUtil.loadJpegImage(filePath);
      ImageDescription imageDesc = image.getImageDescription();
      String resultMetadata = imageDesc.createStringDescription();
      Expect.expect(resultMetadata.equals(expectMetadata));
      BufferedImage resultBuffImg = image.getBufferedImage();
      // Jpeg image loaded should not match original because of lossy compression.
      Expect.expect(compareBufferedImages(expectBuffImg, resultBuffImg) == false);

      resultBuffImg = ImageIoUtil.loadJpegBufferedImage(filePath);
      // Jpeg image loaded should not match original because of lossy compression.
      Expect.expect(compareBufferedImages(expectBuffImg, resultBuffImg) == false);

      image.decrementReferenceCount();
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "CouldNotReadFileException:" + exc.getMessage());
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(false, "FileDoesNotExistException:" + exc.getMessage());
    }
  }

  private void testPngImageLoad(String filePath, String expectMetadata,
                                BufferedImage expectBuffImg)
  {
    try
    {
      Image image = ImageIoUtil.loadPngImage(filePath);
      ImageDescription imageDesc = image.getImageDescription();
      String resultMetadata = imageDesc.createStringDescription();
      Expect.expect(resultMetadata.equals(expectMetadata));
      BufferedImage resultBuffImg = image.getBufferedImage();
      // Png image loaded should match original image (lossless storage).
      Expect.expect(compareBufferedImages(expectBuffImg, resultBuffImg) == true);

      resultBuffImg = ImageIoUtil.loadPngBufferedImage(filePath);
      // Png image loaded should match original image (lossless storage).
      Expect.expect(compareBufferedImages(expectBuffImg, resultBuffImg) == true);
      image.decrementReferenceCount();
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "Exception occurred during PNG Image Load: " + exc.getMessage());
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(false, "FileDoesNotExistException:" + exc.getMessage());
    }
  }


  private void testExceptions()
  {
    // Setup data for exception testing.
    String bogusDirPath = "QXZY:\\bogus:\\thisimagedirectorycannotbecreated";
    String goodDirPath = "test";
    String nonexistentJpgFileName = "nonexistent.jpg";
    String nonexistentPngFileName = "nonexistent.png";
    String badJpgFileName = "badimage.jpg";
    String badPngFileName = "badimage.png";
    BufferedImage buffImg = getGradientImage();
    Image gradImage = Image.createFloatImageFromBufferedImage(buffImg);
    String strMetadata = "some real text";
    createTextFile(goodDirPath, badJpgFileName, "This is not really a JPEG image file!");
    createTextFile(goodDirPath, badPngFileName, "This is not really a PNG image file!");
    Image resultImage = null;
    BufferedImage resultBufferedImage = null;

    // createImageDirectory
    try
    {
      ImageIoUtil.createImageDirectoryIfNeeded(bogusDirPath);
      Expect.expect(false, "CannotCreateImageDirectoryException was expected");
    }
    catch (Exception exc)
    {
    }

    // saveJpegImage
    try
    {
      ImageIoUtil.saveJpegImage(buffImg, bogusDirPath + File.separator + nonexistentJpgFileName, strMetadata);
      Expect.expect(false, "CouldNotCreateFileException was expected");
    }
    catch (CouldNotCreateFileException exc)
    {
    }

    // saveJpegImage
    try
    {
      ImageIoUtil.saveJpegImage(gradImage, bogusDirPath + File.separator + nonexistentJpgFileName);
      Expect.expect(false, "CouldNotCreateFileException was expected");
    }
    catch (CouldNotCreateFileException exc)
    {
    }


    // savePngImage
    try
    {
      ImageIoUtil.savePngImage(buffImg, bogusDirPath + File.separator + nonexistentPngFileName, strMetadata);
      Expect.expect(false, "CouldNotCreateFileException was expected");
    }
    catch (CouldNotCreateFileException exc)
    {
    }

    // savePngImage
    try
    {
      ImageIoUtil.savePngImage(gradImage, bogusDirPath + File.separator + nonexistentPngFileName);
      Expect.expect(false, "CouldNotCreateFileException was expected");
    }
    catch (CouldNotCreateFileException exc)
    {
    }

    // loadJpegImage
    try
    {
      resultImage = null;
      resultImage = ImageIoUtil.loadJpegImage(goodDirPath + File.separator + nonexistentJpgFileName);
      Expect.expect(false, "FileDoesNotExistException was expected");
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "FileDoesNotExistException was expected");
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(resultImage == null);
    }

    try
    {
      resultImage = null;
      resultImage = ImageIoUtil.loadJpegImage(goodDirPath + File.separator + badJpgFileName);
      Expect.expect(false, "CouldNotReadFileException was expected");
    }
    catch (CouldNotReadFileException ex)
    {
      Expect.expect(resultImage == null);
    }
    catch (FileDoesNotExistException ex)
    {
      Expect.expect(false, "CouldNotReadFileException was expected");
    }

    // loadJpegBufferedImage
    try
    {
      resultBufferedImage = null;
      resultBufferedImage = ImageIoUtil.loadJpegBufferedImage(goodDirPath + File.separator + nonexistentJpgFileName);
      Expect.expect(false, "FileDoesNotExistException was expected");
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "FileDoesNotExistException was expected");
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(resultBufferedImage == null);
    }

    try
    {
      resultBufferedImage = null;
      resultBufferedImage = ImageIoUtil.loadJpegBufferedImage(goodDirPath + File.separator + badJpgFileName);
      Expect.expect(false, "CouldNotReadFileException was expected");
    }
    catch (CouldNotReadFileException ex)
    {
      Expect.expect(resultBufferedImage == null);
    }
    catch (FileDoesNotExistException ex)
    {
      Expect.expect(false, "CouldNotReadFileException was expected");
    }

    // loadPngImage
    try
    {
      resultImage = null;
      resultImage = ImageIoUtil.loadPngImage(goodDirPath + File.separator + nonexistentPngFileName);
      Expect.expect(false, "FileDoesNotExistException was expected");
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "FileDoesNotExistException was expected");
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(resultImage == null);
    }

    try
    {
      resultImage = null;
      resultImage = ImageIoUtil.loadPngImage(goodDirPath + File.separator + badPngFileName);
      Expect.expect(false, "CouldNotReadFileException was expected");
    }
    catch (CouldNotReadFileException ex)
    {
      Expect.expect(resultImage == null);
    }
    catch (FileDoesNotExistException ex)
    {
      Expect.expect(false, "CouldNotReadFileException was expected");
    }

    // loadPngBufferedImage
    try
    {
      resultBufferedImage = null;
      resultBufferedImage = ImageIoUtil.loadPngBufferedImage(goodDirPath + File.separator + nonexistentPngFileName);
      Expect.expect(false, "FileDoesNotExistException was expected");
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "FileDoesNotExistException was expected");
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(resultBufferedImage == null);
    }

    try
    {
      resultBufferedImage = null;
      resultBufferedImage = ImageIoUtil.loadPngBufferedImage(goodDirPath + File.separator + badJpgFileName);
      Expect.expect(false, "CouldNotReadFileException was expected");
    }
    catch (CouldNotReadFileException ex)
    {
      Expect.expect(resultBufferedImage == null);
    }
    catch (FileDoesNotExistException ex)
    {
      Expect.expect(false, "CouldNotReadFileException was expected");
    }
    gradImage.decrementReferenceCount();
  }

  /**
   * Returns an image that contains all grey levels for an 8-big image (0-255).
   * This is used for testing lossless and lossy image storage.
   * @return BufferedImage containing all grey levels
   */
  static public BufferedImage getGradientImage()
  {
    BufferedImage buffImg = new BufferedImage(256, 50, BufferedImage.TYPE_BYTE_GRAY);
    for (int i = 0; i < 256; ++i)
    {
      for (int j = 0; j < 50; ++j)
      {
        int[] iArray = new int[3];
        for (int x = 0; x < iArray.length; ++x)
        {
          iArray[x] = i;
        }
        buffImg.getRaster().setPixel(i, j, iArray);
      }
    }
    return buffImg;
  }

  /**
   * Compares two buffered image pixel by pixel.
   * Assumes 3 integers per pixel.
   * @param buffImg1 the first buffered image
   * @param buffImg2 the second buffered image
   * @return true if they are the same, false if they are different
   */
  static public boolean compareBufferedImages(BufferedImage buffImg1, BufferedImage buffImg2)
  {
    if ((buffImg1.getWidth() != buffImg2.getWidth()) ||
        (buffImg1.getHeight() != buffImg2.getHeight()))
    {
      return false;
    }

    int[] iArray1 = new int[3];
    int[] iArray2 = new int[3];
    for (int i = 0; i < buffImg1.getWidth(); ++i)
    {
      for (int j = 0; j < buffImg1.getHeight(); ++j)
      {
        buffImg1.getRaster().getPixel(i, j, iArray1);
        buffImg2.getRaster().getPixel(i, j, iArray2);

        if ((iArray1[0] != iArray2[0]) ||
            (iArray1[1] != iArray2[1]) ||
            (iArray1[2] != iArray2[2]))
        {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Create a text file with the specified contents.
   * @author Kay Lannen
   */
  private void createTextFile(String dirName, String fileName, String contents)
  {
    try
    {
      PrintWriter pw = new PrintWriter(new FileWriter(new File(dirName, fileName)));
      pw.println(contents);
      pw.close();
    }
    catch (IOException exc)
    {
      Expect.expect(false, "IOException: " + exc.getMessage());
    }
  }

  /**
   * @return the current memory usage after attempting garbage collection
   * @author Kay Lannen
   */
  private long getMemoryUsage()
  {
    Runtime rt = Runtime.getRuntime();
    long totalMemory = rt.totalMemory();
    rt.gc();
    long freeMemory = rt.freeMemory();
    long usedMemory = totalMemory - freeMemory;
    return usedMemory;
  }

  /**
   * @author Patrick Lacz
   */
//  public void testImageLoadTimes()
//  {
//    String imageDirectory = "F:\\projects\\cygnus_asap_83\\images\\inspection\\2006-11-10_12-47-08-200";
//    String saveTarget = "C:\\testImage.png";
//
//    Collection<String> files = FileUtil.listAllFilesInDirectory(imageDirectory);
//
//    TimerUtil loadTimer = new TimerUtil();
//    TimerUtil saveTimer = new TimerUtil();
//    long saveSize = 0;
//    long loadSize = 0;
//    int imageCount = 0;
//    for (String file : files)
//    {
//      if (file.endsWith("png"))
//      {
//        String fileNameWithPath = FileUtil.concatFileNameToPath(imageDirectory, file);
//        try
//        {
//          loadTimer.start();
//          Image image = ImageIoUtil.loadImage(fileNameWithPath);
//          loadTimer.stop();
//          loadSize += FileUtil.getFileSizeInBytes(fileNameWithPath);
//
//          saveTimer.start();
//          ImageIoUtil.saveImage(image, saveTarget);
//          saveTimer.stop();
//          saveSize += FileUtil.getFileSizeInBytes(saveTarget);
//
//          image.decrementReferenceCount();
//          imageCount++;
//        }
//        catch (CouldNotReadFileException ex)
//        {
//          Expect.expect(false);
//        }
//        catch (FileDoesNotExistException ex)
//        {
//          Expect.expect(false);
//        }
//        catch (CouldNotCreateFileException ex1)
//        {
//          Expect.expect(false);
//        }
//      }
//    }
//    long elapsedLoadTime = loadTimer.getElapsedTimeInMillis();
//    long elapsedSaveTime = saveTimer.getElapsedTimeInMillis();
//
//    System.out.println("Elapsed time to load " + imageCount + " images: " + elapsedLoadTime + " ms (" + loadSize + " bytes)");
//    System.out.println("Elapsed time to save" + imageCount + " images: " + elapsedSaveTime + " ms(" + saveSize + " bytes)");
//  }
}
