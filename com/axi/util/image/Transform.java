package com.axi.util.image;

import java.awt.geom.*;

import com.axi.util.*;

/**
 * Functions to modify the orientation, position, and size of Images.
 *
 * @author Patrick Lacz
 */
public class Transform
{
  /**
   * @author Patrick Lacz
   */
  public Transform()
  {
    // do nothing
    // this object has bunch of static methods
  }

  /**
   * @author Patrick Lacz
   */
  static
  {
    System.loadLibrary("nativeImageUtil");
  }

  /**
   * @todo : Create Regression Tests
   * @author Patrick Lacz
   */
  public static void flipX(Image image)
  {
    Assert.expect(image != null);
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    nativeFlip(image.getNativeDefinition(), 0, 0, image.getWidth(), image.getHeight(), 1);
  }

  /**
   * @todo : Create Regression Tests
   * @author Patrick Lacz
   */
  public static void flipY(Image image)
  {
    Assert.expect(image != null);
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    nativeFlip(image.getNativeDefinition(), 0, 0, image.getWidth(), image.getHeight(), 0);
  }

  /**
   * @todo : Create Regression Tests
   * @author Patrick Lacz
   */
  public static void flipXY(Image image)
  {
    Assert.expect(image != null);
    Assert.expect(image.getBytesPerPixel() == 4); // float format

    nativeFlip(image.getNativeDefinition(), 0, 0, image.getWidth(), image.getHeight(), 2);
  }

  /**
   * Operates only within the region of interest.
   * @todo : Create Regression Tests
   * @author Patrick Lacz
   */
  public static void flipX(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));

    nativeFlip(image.getNativeDefinition(),
               roi.getMinX(), roi.getMinY(),
               roi.getWidth(), roi.getHeight(), 1);
  }

  /**
   * Operates only within the region of interest.
   * @todo : Create Regression Tests
   * @author Patrick Lacz
   */
  public static void flipY(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));

    nativeFlip(image.getNativeDefinition(),
               roi.getMinX(), roi.getMinY(),
               roi.getWidth(), roi.getHeight(), 0);
  }

  /**
   * Operates only within the region of interest.
   * @todo : Create Regression Tests
   * @author Patrick Lacz
   */
  public static void flipXY(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));

    nativeFlip(image.getNativeDefinition(),
               roi.getMinX(), roi.getMinY(),
               roi.getWidth(), roi.getHeight(), 2);
  }

  /**
   * Create the region of interest containing the transformed region of interest, and the Affine
   * Transform mapping to that region.
   * @todo : Create Regression Tests
   *
   * @param outROI The output RegionOfInterest. This object is modified.
   * @param outXform The output AffineTransform. This object is modified.
   * @author Patrick Lacz
   */
  public static void boundTransformedRegionOfInterest(RegionOfInterest sourceROI, AffineTransform sourceXform, RegionOfInterest outROI, AffineTransform outXform)
  {
    Assert.expect(sourceROI != null);
    Assert.expect(sourceXform != null);
    Assert.expect(outROI != null);
    Assert.expect(outXform != null);

    double matrix[] = new double[6];
    double mappedBounds[] = new double[4];

    sourceXform.getMatrix(matrix);
    nativeAffineBounds(matrix, sourceROI.getMinX(), sourceROI.getMinY(), sourceROI.getWidth(), sourceROI.getHeight(),
                        mappedBounds);

    int minX = (int)Math.floor(Math.min(mappedBounds[0], mappedBounds[2]));
    int maxX = (int)Math.ceil(Math.max(mappedBounds[0], mappedBounds[2]));
    int minY = (int)Math.floor(Math.min(mappedBounds[1], mappedBounds[3]));
    int maxY = (int)Math.ceil(Math.max(mappedBounds[1], mappedBounds[3]));

    int resultWidth = maxX - minX + 1;
    int resultHeight = maxY - minY + 1;

    // Update the transform so that no part of the image is truncated away.
    outXform.setToIdentity();
    outXform.concatenate(sourceXform);
    outXform.preConcatenate(AffineTransform.getTranslateInstance(-minX, -minY));

    outROI.setMinXY(minX,minY);
    outROI.setWidthKeepingSameMinX(resultWidth);
    outROI.setHeightKeepingSameMinY(resultHeight);
    outROI.setOrientationInDegrees(0);
  }

  /**
   * Return an image rotated by the specified amount.  The return image will be the same size as the original image,
   * but note that there may be "invalid" pixels in the corners due to the rotation.  These will be filled in with the
   * specified backgroundColor.
   *
   * <pre>
   * Input:
   *
   * 11211
   * 11211
   * 11222
   * 11111
   * 11111
   *
   * Rotated by 45 degrees:
   *
   *
   * @author Peter Esbensen
   */
  public static Image rotate(Image image, float backgroundColor, int rotationInDegrees)
  {
    Assert.expect(image != null);
    Assert.expect(backgroundColor >= 0f);

    AffineTransform rotation = AffineTransform.getRotateInstance( Math.toRadians(rotationInDegrees) );
    Image rotatedImage = Transform.applyAffineTransform(rotation, image, 0.0f);

    // the rotated image will probably be bigger since it is sized to include the post-rotated region, so let's fix that.
    // Figure out a crop region

    RegionOfInterest desiredROI = RegionOfInterest.createRegionFromImage(image);

    int increaseInSizeX = rotatedImage.getWidth() - image.getWidth();
    Assert.expect(increaseInSizeX >= 0);
    int increaseInSizeY = rotatedImage.getHeight() - image.getHeight();
    Assert.expect(increaseInSizeY >= 0);
    desiredROI.translateXY(increaseInSizeX / 2, increaseInSizeY / 2);

    // now we can crop the rotated image back to the original size
    Image croppedRotatedImage = Image.createCopy(rotatedImage, desiredROI);

    return croppedRotatedImage;

  }

  /**
   * Apply the given transformation to the image.
   *
   * Uses boundTransformedRegionOfInterest to return an image that contains the resulting image irrespective
   * of translations.
   *
   * @author Patrick Lacz
   */
  public static Image applyAffineTransform(AffineTransform transform, Image image, float backgroundColor)
  {
    Assert.expect(transform != null);
    Assert.expect(image != null);

    RegionOfInterest oldRoi = RegionOfInterest.createRegionFromImage(image);
    RegionOfInterest newRoi = new RegionOfInterest(oldRoi);
    AffineTransform newXform = new AffineTransform();
    boundTransformedRegionOfInterest(oldRoi, transform, newRoi, newXform);

    // create an image that contains the entire result image
    Image resultImage = new Image(newRoi.getWidth(), newRoi.getHeight());
    // fill the image with the specified background color to avoid random data.
    Paint.fillImage(resultImage, backgroundColor);

    copyImageIntoImageWithTransform(resultImage, image, newXform);

    return resultImage;
  }

 /**
   * Apply the given transformation to the image.
   *
   * @return An Image of the specified RegionOfInterest's length and width.
   * @author Patrick Lacz
   */
  public static Image applyAffineTransform(Image sourceImage, AffineTransform transform, RegionOfInterest resultRoiRelativeToSource, float backgroundColor)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(transform != null);
    Assert.expect(resultRoiRelativeToSource != null); // this roi need not fit within the source Image
    Assert.expect(backgroundColor >= 0.0f && backgroundColor <= 255.0);
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format

    // create an image that contains the entire result image
    Image resultImage = new Image(
      resultRoiRelativeToSource.getWidth(),
      resultRoiRelativeToSource.getHeight());
    // fill the image with 0's to avoid random data.
    Paint.fillImage(resultImage, backgroundColor);

    double matrix[] = new double[6];
    transform.getMatrix(matrix);

    nativeAffineTransform(matrix,
                          sourceImage.getNativeDefinition(),
                          0, 0, sourceImage.getWidth(), sourceImage.getHeight(),
                          resultImage.getNativeDefinition(),
                          resultRoiRelativeToSource.getMinX(),
                          resultRoiRelativeToSource.getMinY(),
                          resultRoiRelativeToSource.getWidth(),
                          resultRoiRelativeToSource.getHeight());

    return resultImage;
  }

  /**
   * @author Patrick Lacz
   */
  public static void copyImageIntoImage(Image sourceImage, RegionOfInterest sourceRoi, Image destImage, RegionOfInterest destRoi)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(sourceRoi != null);
    Assert.expect(destImage != null);
    Assert.expect(destRoi != null);

    Assert.expect(sourceRoi.fitsWithinImage(sourceImage));
    Assert.expect(destRoi.fitsWithinImage(destImage));
    Assert.expect(sourceRoi.getWidth() == destRoi.getWidth());
    Assert.expect(sourceRoi.getHeight() == destRoi.getHeight());

    nativeCopy(sourceImage.getNativeDefinition(), sourceRoi.getMinX(), sourceRoi.getMinY(),
               destImage.getNativeDefinition(), destRoi.getMinX(), destRoi.getMinY(),
               destRoi.getWidth(), destRoi.getHeight());
  }

  /**
   * @author Patrick Lacz
   */
  public static void copyImageIntoImage(Image sourceImage, Image destImage)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(destImage != null);
    Assert.expect(sourceImage.getWidth() == destImage.getWidth());
    Assert.expect(sourceImage.getHeight() == destImage.getHeight());

    nativeCopy(sourceImage.getNativeDefinition(), 0, 0,
               destImage.getNativeDefinition(), 0, 0,
               sourceImage.getWidth(), sourceImage.getHeight());
  }

  /**
   * @author Patrick Lacz
   */
  public static Image resizeImage(Image originalImage, int newWidth, int newHeight)
  {
    Assert.expect(originalImage != null);
    Assert.expect(newWidth > 0);
    Assert.expect(newHeight > 0);

    Image resultImage = new Image(newWidth, newHeight);

    copyImageIntoImageWithResize(resultImage, originalImage, 0, 0, newWidth, newHeight);
    return resultImage;
  }

  /**
   * @todo Create Regression Tests
   * @author Patrick Lacz
   */
  public static void copyImageIntoImageWithResize(Image backgroundImage, Image sourceImage, int x, int y, int newWidth, int newHeight)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(sourceImage.getWidth() > 0);
    Assert.expect(sourceImage.getHeight() > 0);
    Assert.expect(backgroundImage.getBytesPerPixel() == 4); // float format
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format

    double matrix[] = new double[6];
    nativeGetResizeTransform(sourceImage.getWidth(), sourceImage.getHeight(), newWidth, newHeight, matrix);

    double scaleWidth = matrix[0]; // X scale from the 2x3 matrix [0][0]
    double scaleHeight = matrix[4]; // Y scale from the 2x3 matrix [1][1]
    AffineTransform transform = AffineTransform.getScaleInstance(scaleWidth, scaleHeight);
    transform.translate(x, y);

    copyImageIntoImageWithTransform(backgroundImage, sourceImage, transform);
  }

  /**
   * @todo Create Regression Tests
   * @author Patrick Lacz
   */
  public static void copyImageIntoImageWithTransform(Image backgroundImage, Image sourceImage, AffineTransform transform)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(transform != null);
    Assert.expect(backgroundImage.getBytesPerPixel() == 4); // float format
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format


    RegionOfInterest imageOutputRegion = RegionOfInterest.createRegionFromImage(sourceImage);  // overwritten
    AffineTransform scratchTransform = new AffineTransform(); // this is unused, but generated by this method.
    boundTransformedRegionOfInterest(RegionOfInterest.createRegionFromImage(sourceImage), transform, imageOutputRegion, scratchTransform);

    // require more than one pixel of overlap
    if (imageOutputRegion.getMaxX() < 4 || (imageOutputRegion.getMinX() > backgroundImage.getWidth() - 5))
      return;
    if (imageOutputRegion.getMaxY() < 4 || (imageOutputRegion.getMinY() > backgroundImage.getHeight() - 5))
      return;

    double matrix[] = new double[6];
    transform.getMatrix(matrix);

    nativeAffineTransform(matrix,
                          sourceImage.getNativeDefinition(),
                          0, 0, sourceImage.getWidth(), sourceImage.getHeight(),
                          backgroundImage.getNativeDefinition(),
                          0, 0, backgroundImage.getWidth(), backgroundImage.getHeight());
  }


  /**
   * @param interpolation 0 = Nearest Neighbor, 1 = Linear, 2 = Cubic
   * @author Patrick Lacz
   */
  public static void remapIntoImage(
      Image sourceImage,
      RegionOfInterest sourceRoi,
      Image xInputImage,
      RegionOfInterest xInputRoi,
      Image yInputImage,
      RegionOfInterest yInputRoi,
      Image destImage,
      RegionOfInterest destRoi,
      int interpolation)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(sourceRoi != null);
    Assert.expect(sourceRoi.fitsWithinImage(sourceImage));
    Assert.expect(xInputImage != null);
    Assert.expect(xInputRoi != null);
    Assert.expect(xInputRoi.fitsWithinImage(xInputImage));
    Assert.expect(yInputImage != null);
    Assert.expect(yInputRoi != null);
    Assert.expect(yInputRoi.fitsWithinImage(yInputImage));
    Assert.expect(destImage != null);
    Assert.expect(destRoi != null);
    Assert.expect(destRoi.fitsWithinImage(destImage));

    Assert.expect(xInputRoi.getWidth() <= destRoi.getWidth());
    Assert.expect(yInputRoi.getWidth() <= destRoi.getWidth());
    Assert.expect(xInputRoi.getHeight() <= destRoi.getHeight());
    Assert.expect(yInputRoi.getHeight() <= destRoi.getHeight());

    nativeRemap(sourceImage.getNativeDefinition(),
                sourceRoi.getMinX(), sourceRoi.getMinY(), sourceRoi.getWidth(), sourceRoi.getHeight(),
                xInputImage.getNativeDefinition(), xInputRoi.getMinX(), xInputRoi.getMinY(),
                yInputImage.getNativeDefinition(), yInputRoi.getMinX(), yInputRoi.getMinY(),
                destImage.getNativeDefinition(), destRoi.getMinX(), destRoi.getMinY(),
                interpolation); // 0 = Nearest Neighbor, 1 = Linear, 2 = Cubic
  }

  /**
   * @author Patrick Lacz
   * @param flipDirection 0 = vertical, 1 = horizontal, 2 = both
   */
  private static native void nativeFlip(int[] sourceImage, int roiMinX, int roiMinY, int roiWidth, int roiHeight,
                                        int flipDirection);

  /**
   * @author Patrick Lacz
   */
   private static native void nativeAffineQuad(double rowMajorMatrix[],
                                               int minX, int minY, int width, int height,
                                               double result4x2[]);
  /**
   * @author Patrick Lacz
   */
   private static native void nativeAffineBounds(double rowMajorMatrix[],
                                               int minX, int minY, int width, int height,
                                               double result4[]);

  /**
   * @author Patrick Lacz
   */
   private static native void nativeGetResizeTransform(int originalWidth, int originalHeight, int newWidth, int newHeight, double matrix[]);


  /**
   * @author Patrick Lacz
   */
  private static native void nativeRemap(int[] sourceImage,
                                         int sourceMinX, int sourceMinY, int sourceWidth, int sourceHeight,
                                         int[] xDataImage, int xDataMinX, int xDataMinY,
                                         int[] yDataImage, int yDataMinX, int yDataMinY,
                                         int[] destImage, int destImageMinX, int destImageMinY,
                                         int interpolation);

  /**
   * @author Patrick Lacz
   */
  private static native void nativeAffineTransform(double rowMajorMatrix[],
      int[] sourceImage, int sourceRoiMinX, int sourceRoiMinY, int sourceRoiWidth, int sourceRoiHeight,
      int[] destImage, int destRoiMinX, int destRoiMinY, int destRoiWidth, int destRoiHeight);


  /**
   * @author Patrick Lacz
   */
  private static native void nativeCopy(int[] sourceImage, int sourceRoiX, int sourceRoiY,
                                        int[] destImage, int destRoiX, int destRoiY,
                                        int destWidth, int destheight);
  }
