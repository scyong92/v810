package com.axi.util.image;

import java.awt.image.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Patrick Lacz
 */
class Test_Statistics extends UnitTest
{
  /**
   * @author Patrick Lacz
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Statistics());
  }

  /**
   * @author Patrick Lacz
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Image image = loadTestImage(getTestDataDir(), "test1.png");

    /* test1.png is an image that looks like:

      0    0    0   127 127 127
      0    0    0   127 127 127
      0    0    0   127 127 127
      0    0    0   255 255 255
      0    0    0   255 255 255
      0    0    0   255 255 255
     */

    testMinMaxMean(image);

    testObroundMean(image);

    testMeanAroundRegion(image);

    testPercentile(image);

    testMedianAroundRegion(image);

    testCircularMedian(image);

    // uncomment to perform timing tests
    //testTiming();
  }

  /**
   * @author Patrick Lacz
   */
  private void testMinMaxMean(Image image)
  {
    Assert.expect(image != null);

    // test illegal regions of interest
    Collection<RegionOfInterest> regions = ImageUnitTestUtil.generateBadRegionsOfInterest(image);
    for (RegionOfInterest regionOfInterest : regions)
    {
      // Min
      try
      {
        Statistics.minValue(image, regionOfInterest);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }

      // Max
      try
      {
        Statistics.maxValue(image, regionOfInterest);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }

      // Mean
      try
      {
        Statistics.mean(image, regionOfInterest);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }

      // Mean with StdDev
      try
      {
        DoubleRef a = new DoubleRef(0.0), b = new DoubleRef(0.0);
        Statistics.meanStdDev(image, regionOfInterest, a, b);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
    }

    // Whole Image.
    DoubleRef meanResultRef = new DoubleRef(), stdDevResultRef = new DoubleRef();
    float minResult = Statistics.minValue(image);
    Expect.expect(MathUtil.fuzzyEquals(minResult, 0.f));
    float maxResult = Statistics.maxValue(image);
    Expect.expect(MathUtil.fuzzyEquals(maxResult, 255.f));
    float meanResult = Statistics.mean(image);
    Expect.expect(MathUtil.fuzzyEquals(meanResult, 95.5f));
    Statistics.meanStdDev(image, meanResultRef, stdDevResultRef);
    Expect.expect(MathUtil.fuzzyEquals(meanResultRef.getValue(), 95.5));
    Expect.expect(MathUtil.fuzzyEquals(stdDevResultRef.getValue(), 105.67994));

    // With Region of Interest
    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder(1, 1, 3, 3, 0, RegionShapeEnum.RECTANGULAR);
    minResult = Statistics.minValue(image, roi);
    Expect.expect(MathUtil.fuzzyEquals(minResult, 0.f));
    maxResult = Statistics.maxValue(image, roi);
    Expect.expect(MathUtil.fuzzyEquals(maxResult, 255.f));
    meanResult = Statistics.mean(image, roi);
    Expect.expect(MathUtil.fuzzyEquals(meanResult, 56.555555f));
    Statistics.meanStdDev(image, roi, meanResultRef, stdDevResultRef);
    Expect.expect(MathUtil.fuzzyEquals(meanResultRef.getValue(), 56.555555));
    Expect.expect(MathUtil.fuzzyEquals(stdDevResultRef.getValue(), 87.239276));

    // Irrespective of Rotation
    roi.setOrientationInDegrees(90);
    minResult = Statistics.minValue(image, roi);
    Expect.expect(MathUtil.fuzzyEquals(minResult, 0.f));
    maxResult = Statistics.maxValue(image, roi);
    Expect.expect(MathUtil.fuzzyEquals(maxResult, 255.f));
    meanResult = Statistics.mean(image, roi);
    Expect.expect(MathUtil.fuzzyEquals(meanResult, 56.555555f));
    Statistics.meanStdDev(image, roi, meanResultRef, stdDevResultRef);
    Expect.expect(MathUtil.fuzzyEquals(meanResultRef.getValue(), 56.555555));
    Expect.expect(MathUtil.fuzzyEquals(stdDevResultRef.getValue(), 87.239276));


    roi.setOrientationInDegrees(180);
    minResult = Statistics.minValue(image, roi);
    Expect.expect(MathUtil.fuzzyEquals(minResult, 0.f));
    maxResult = Statistics.maxValue(image, roi);
    Expect.expect(MathUtil.fuzzyEquals(maxResult, 255.f));
    meanResult = Statistics.mean(image, roi);
    Expect.expect(MathUtil.fuzzyEquals(meanResult, 56.555555f));
    Statistics.meanStdDev(image, roi, meanResultRef, stdDevResultRef);
    Expect.expect(MathUtil.fuzzyEquals(meanResultRef.getValue(), 56.555555));
    Expect.expect(MathUtil.fuzzyEquals(stdDevResultRef.getValue(), 87.239276));

    // With Region of Interest
    roi = RegionOfInterest.createRegionFromRegionBorder(3, 1, 4, 3, 0, RegionShapeEnum.RECTANGULAR);
    minResult = Statistics.minValue(image, roi);
    Expect.expect(MathUtil.fuzzyEquals(minResult, 127.f));
    maxResult = Statistics.maxValue(image, roi);
    Expect.expect(MathUtil.fuzzyEquals(maxResult, 255.f));
    meanResult = Statistics.mean(image, roi);
    Expect.expect(MathUtil.fuzzyEquals(meanResult, 169.666666f));
    /**
     * @todo more test cases
     */
  }

  /**
   * @author Patrick Lacz
   */
  private void testPercentile(Image image)
  {
    Collection<RegionOfInterest> regions = ImageUnitTestUtil.generateBadRegionsOfInterest(image);
    for (RegionOfInterest regionOfInterest : regions)
    {
      try
      {
        Statistics.getPercentile(image, regionOfInterest, 0.5f);
        Expect.expect( false ); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
    }

    float median = Statistics.median(image);
    Expect.expect(median == 127.f*0.5f);
    float anotherMedian = Statistics.getPercentile(image, RegionOfInterest.createRegionFromImage(image), 0.5f);
    Expect.expect(median == anotherMedian);

    median = Statistics.median(image, new RegionOfInterest( 3, 0, 3, 6, 0, RegionShapeEnum.RECTANGULAR));
    Expect.expect(median == (127.0f+255.0f)*0.5f);

    float thirdQuartile = Statistics.getPercentile(image, RegionOfInterest.createRegionFromImage(image), 0.75f);
    Expect.expect(thirdQuartile == (127.0f*0.75+0.25*255.0f));
  }

  /**
   * @author Patrick Lacz
   */
  private void testObroundMean(Image image)
  {
    // the mean function should be able to take non-rectangular regions
    // and handle them correctly

    RegionOfInterest horizObround = new RegionOfInterest(1,0,5, 3, 0, RegionShapeEnum.OBROUND);
    float mean = Statistics.mean(image, horizObround);
    Expect.expect(MathUtil.fuzzyEquals(mean, 9*127.f / 15));

    RegionOfInterest vertObround = new RegionOfInterest(2, 1, 4, 5, 0, RegionShapeEnum.OBROUND);
    mean = Statistics.mean(image, vertObround);
    Expect.expect(MathUtil.fuzzyEquals(mean, (5*127.f + 8*255.f) / 16));

    RegionOfInterest circleRegion = new RegionOfInterest(2,0, 4, 4, 0, RegionShapeEnum.OBROUND);
    mean = Statistics.mean(image, circleRegion);
    Expect.expect(MathUtil.fuzzyEquals(mean, (8*127.f + 2*255.f) / 12));
  }

  /**
   * @author Patrick Lacz
   */
  private void testMeanAroundRegion(Image image)
  {

    final Image image6x6 = new Image(6, 6);

    // disjoint regions
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        RegionOfInterest outerRegion = new RegionOfInterest(0, 0, 2, 2, 0, RegionShapeEnum.RECTANGULAR);
        RegionOfInterest innerRegion = new RegionOfInterest(3, 3, 2, 2, 0, RegionShapeEnum.RECTANGULAR);
        float mean = Statistics.meanAroundRegion(image6x6, outerRegion, innerRegion);

      }
    });

// Commented (pwl) because Statistics.meanAroundRegion was changed to accept these regions and just ignore
//    the portions outside of the image.
//
//    // one region outside image
//    Expect.expectAssert(new RunnableWithExceptions()
//    {
//      public void run()
//      {
//        RegionOfInterest outerRegion = new RegionOfInterest(0, 0, 4, 8, 0, RegionShapeEnum.RECTANGULAR);
//        RegionOfInterest innerRegion = new RegionOfInterest(1, 1, 2, 2, 0, RegionShapeEnum.RECTANGULAR);
//        float mean = Statistics.meanAroundRegion(new Image(6, 6), outerRegion, innerRegion);
//      }
//    });
//
//    // other region outside image
//    Expect.expectAssert(new RunnableWithExceptions()
//    {
//      public void run()
//      {
//        RegionOfInterest outerRegion = new RegionOfInterest(0, 0, 4, 4, 0, RegionShapeEnum.RECTANGULAR);
//        RegionOfInterest innerRegion = new RegionOfInterest(1, 1, 8, 2, 0, RegionShapeEnum.RECTANGULAR);
//        float mean = Statistics.meanAroundRegion(new Image(6, 6), outerRegion, innerRegion);
//      }
//    });

    // outer region must be rectangular (only bounds of inner region matter, which are always rectangular.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        RegionOfInterest outerRegion = new RegionOfInterest(0, 0, 4, 4, 0, RegionShapeEnum.OBROUND);
        RegionOfInterest innerRegion = new RegionOfInterest(1, 1, 2, 2, 0, RegionShapeEnum.RECTANGULAR);
        float mean = Statistics.meanAroundRegion(image6x6, outerRegion, innerRegion);
      }
    });
    image6x6.decrementReferenceCount();

    // do the computation correctly
    RegionOfInterest imageRegion = RegionOfInterest.createRegionFromImage(image);
    RegionOfInterest innerRegion = RegionOfInterest.createRegionFromCircle(3, 4, 1);
    float mean = Statistics.meanAroundRegion(image, imageRegion, innerRegion);
    Expect.expect(MathUtil.fuzzyEquals(mean, (9*127.f+3*255.f)/(36-9)));
  }

  /**
   * @author Rex Shang
   */
  private void testMedianAroundRegion(Image image)
  {
    final Image sixBySixImage = new Image(6, 6);
    // disjoint regions
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        RegionOfInterest outerRegion = new RegionOfInterest(0, 0, 2, 2, 0, RegionShapeEnum.RECTANGULAR);
        RegionOfInterest innerRegion = new RegionOfInterest(3, 3, 2, 2, 0, RegionShapeEnum.RECTANGULAR);
        float median = Statistics.medianAroundRegion(sixBySixImage, outerRegion, innerRegion);
      }
    });

    // outer region must be rectangular (only bounds of inner region matter, which are always rectangular.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        RegionOfInterest outerRegion = new RegionOfInterest(0, 0, 4, 4, 0, RegionShapeEnum.OBROUND);
        RegionOfInterest innerRegion = new RegionOfInterest(1, 1, 2, 2, 0, RegionShapeEnum.RECTANGULAR);
        float median = Statistics.medianAroundRegion(sixBySixImage, outerRegion, innerRegion);
      }
    });
    sixBySixImage.decrementReferenceCount();

    // do the computation correctly
    // Crop out 1 of the largest #, this would push median down.
    RegionOfInterest imageRegion = RegionOfInterest.createRegionFromImage(image);
    RegionOfInterest innerRegion = RegionOfInterest.createRegionFromCircle(3, 4, 1);
    float median = Statistics.medianAroundRegion(image, imageRegion, innerRegion);
    Expect.expect(MathUtil.fuzzyEquals(median, 0));

    // Crop out 1 of the smallest #, this would push median up.
    imageRegion = RegionOfInterest.createRegionFromImage(image);
    innerRegion = RegionOfInterest.createRegionFromCircle(2, 2, 1);
    median = Statistics.medianAroundRegion(image, imageRegion, innerRegion);
    Expect.expect(MathUtil.fuzzyEquals(median, 127));

    // Crop out all the "0"s, this would give us median in between 127 and 255.
    imageRegion = RegionOfInterest.createRegionFromImage(image);
    innerRegion = new RegionOfInterest(0, 0, 3, 6, 0, RegionShapeEnum.RECTANGULAR);
    median = Statistics.medianAroundRegion(image, imageRegion, innerRegion);
    Expect.expect(MathUtil.fuzzyEquals(median, (255 + 127)*0.5f));

    // Leave 3 "0"s, 3 "127"s and 5 "255"s for median calculation.
    imageRegion = RegionOfInterest.createRegionFromImage(image);
    innerRegion = RegionOfInterest.createRegionFromCircle(2, 2, 2);
    median = Statistics.medianAroundRegion(image, imageRegion, innerRegion);
    Expect.expect(MathUtil.fuzzyEquals(median, 127));
  }

  /**
   * @author Patrick Lacz
   */
  private void testCircularMedian(Image image)
  {
    RegionOfInterest roi = RegionOfInterest.createRegionFromImage(image);

    float median = Statistics.circularMedian(image, roi);
    Expect.expect(median == (0.f + 127.f) / 2.f);
  }

  /**
   * Implemented only to compare techniques of implementing the algorithm. Kept for future reference.
   * @author Patrick Lacz
   */

  private void testTiming()
  {
    Image image = loadTestImage(getTestDataDir(), "testimage_png1.png");

    Assert.expect(image != null);

    System.out.println("Image Dimensions: " + image.getWidth() + " x " + image.getHeight());

    RegionOfInterest horizRoi = RegionOfInterest.createRegionFromRegionBorder(0, 0, image.getWidth()-1, image.getHeight()-1,
        0,RegionShapeEnum.RECTANGULAR);

    int tests = 1000;

    long nanoStart = System.nanoTime();
    for (int i = 0 ; i < tests ; ++i)
    {
      float min = Statistics.minValue(image, horizRoi);
    }
    long nanoEnd = System.nanoTime();

    double duration = (nanoEnd - nanoStart)/tests;
    System.out.println("minValue invokation took on average " + duration/100000.0 + " milliseconds.");

    nanoStart = System.nanoTime();
    for (int i = 0 ; i < tests ; ++i)
    {
      float max = Statistics.maxValue(image, horizRoi);
    }
    nanoEnd = System.nanoTime();
    duration = (nanoEnd - nanoStart)/tests;
    System.out.println("maxValue invokation took on average " + duration/100000.0 + " milliseconds.");

    nanoStart = System.nanoTime();
    for (int i = 0 ; i < tests ; ++i)
    {
      float mean = Statistics.mean(image, horizRoi);
    }
    nanoEnd = System.nanoTime();
    duration = (nanoEnd - nanoStart)/tests;
    System.out.println("Mean invokation took on average " + duration/100000.0 + " milliseconds.");

    DoubleRef a = new DoubleRef(), b = new DoubleRef();
    nanoStart = System.nanoTime();
    for (int i = 0 ; i < tests ; ++i)
    {
      Statistics.meanStdDev(image, horizRoi, a, b);
    }
    nanoEnd = System.nanoTime();
    duration = (nanoEnd - nanoStart)/tests;
    System.out.println("MeanStdDev invokation took on average " + duration/100000.0 + " milliseconds.");

  }

  /**
   * Copied from Test_ImageFeatureExtraction
   * @author Peter Esbensen
   */
  private Image loadTestImage(String directoryPath, String filename)
  {
    Assert.expect(directoryPath != null);
    Assert.expect(directoryPath.length() > 0);
    Assert.expect(filename != null);
    Assert.expect(filename.length() > 0);

    Image resultImage = null;
    try
    {
      resultImage = ImageIoUtil.loadPngImage(directoryPath + File.separator + filename);
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "Exception occurred during PNG Image Load: " + exc.getMessage());
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(false, "FileDoesNotExistException:" + exc.getMessage());
    }
    Assert.expect(resultImage != null);
    return resultImage;
  }
}
