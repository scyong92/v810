package com.axi.util.image;

/**
 * InvertImage - It is a image processing method to creates a reversed image.
 * 
 * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
 */
public class InvertImage extends ImageEnhancerBase
{
  public InvertImage()
  {
  }
  
 public ImageEnhancerType getType()
 {
  return ImageEnhancerType.InvertImage;  
 }
   
  public Image runEnhancement(Image dest, Image src)
  {
    return ImageEnhancer.invertImage(dest, src);
  }
  
  public void updateParameter()
  {
    //no parameter to update
  }
}