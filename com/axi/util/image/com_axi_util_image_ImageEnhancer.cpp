#include <algorithm>

#include <jni.h>
#include "com_axi_util_image_ImageEnhancer.h"

#include "PixelFormat.hpp"
#include "VfImageEnhancer.hpp"

#include "sptAssert.h"

#include "jniImageUtil.h"

//#define _DEBUG
#ifdef _DEBUG
#include <fstream>
#include <iostream>
#endif

using namespace VxImageEnhancer;
static int idValue = 0;

/**
 * nativeResizeLinearPNG
 *
 * @author Bee Hoon
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_ImageEnhancer_nativeResizeLinear32bitsImage(JNIEnv *jEnv, 
																				jclass jContext, 
																				jintArray jDestImage, 
																				jintArray jSrcImage, 
																				jdouble jScaleFactorX, 
																				jdouble jScaleFactorY)
{
 try
  {
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    JNIImageData *pSrcImage = getImageDataFromJava(jEnv, jSrcImage);

	/*VfImageEnhancer::Status::Enum returnStatus = Resize(pDestImage->address, pDestImage->stride, pDestImage->width, pDestImage->height,
		pSrcImage->address, pSrcImage->stride, pSrcImage->width, pSrcImage->height,
		jScaleFactorX, jScaleFactorY, PixelFormat::L32_FLOAT, InterpolationMode::Linear);*/

	#ifdef _DEBUG
	int debugId = idValue++;
	std::ofstream myfile("imageEnhancerLog.txt", std::ios_base::out | std::ios_base::app);
	myfile <<debugId <<",BF RFilter,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height 
	       <<",jScaleFactorX," <<jScaleFactorX <<",jScaleFactorY,"<< jScaleFactorY  << std::endl;
	#endif

	SystemError::ErrorCode returnStatus = Resize(pDestImage->address, pDestImage->stride, pDestImage->width, pDestImage->height,
		pSrcImage->address, pSrcImage->stride, pSrcImage->width, pSrcImage->height,
		jScaleFactorX, jScaleFactorY, PixelFormat::L32_FLOAT, InterpolationMode::Linear);
	
	#ifdef _DEBUG
	jint status = static_cast<int>(returnStatus);	
	
	myfile <<debugId <<",AF RFilter,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height 
	       <<",jScaleFactorX," <<jScaleFactorX <<",jScaleFactorY,"<< jScaleFactorY << ",Status," << status<< std::endl;
	#endif

	//jint status = getStatus(returnStatus);
	/*jint status = 9;
	if (returnStatus == VfImageEnhancer::Status::Success)
	status = 4;
	if (returnStatus == VfImageEnhancer::Status::InvalidArgument)
	status = 3;
	if (returnStatus == VfImageEnhancer::Status::InsufficientMemory)
	status = 2;
	if (returnStatus == VfImageEnhancer::Status::UnknownError)
	status = 1;*/

    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
	releaseImageDataFromJavaUnmodified(jEnv, jSrcImage, pSrcImage);
 }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeResizeCubicPNG
 *
 * @author Bee Hoon
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_ImageEnhancer_nativeResizeCubic32bitsImage(JNIEnv *jEnv, 
																				jclass jContext, 
																				jintArray jDestImage, 
																				jintArray jSrcImage, 
																				jdouble jScaleFactorX, 
																				jdouble jScaleFactorY)
{
 try
  {
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    JNIImageData *pSrcImage = getImageDataFromJava(jEnv, jSrcImage);

	#ifdef _DEBUG
	int debugId = idValue++;
	std::ofstream myfile("imageEnhancerLog.txt", std::ios_base::out | std::ios_base::app);
	myfile <<debugId <<",BF ResizeCubic32bitsImage,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<"SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height 
	       <<"jScaleFactorX," <<jScaleFactorX <<",jScaleFactorY,"<< jScaleFactorY << std::endl;
	#endif

	SystemError::ErrorCode returnStatus = Resize(pDestImage->address, pDestImage->stride, pDestImage->width, pDestImage->height,
		pSrcImage->address, pSrcImage->stride, pSrcImage->width, pSrcImage->height,
		jScaleFactorX, jScaleFactorY, PixelFormat::L32_FLOAT, InterpolationMode::Cubic);
    
	#ifdef _DEBUG
	jint status = static_cast<int>(returnStatus);	
	
	myfile <<debugId <<",AF ResizeCubic32bitsImage,DS,"<< pDestImage->stride<<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height
           <<",jScaleFactorX," <<jScaleFactorX <<",jScaleFactorY,"<< jScaleFactorY << ",Status," << status << std::endl;
	#endif
	
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
	releaseImageDataFromJavaUnmodified(jEnv, jSrcImage, pSrcImage);
 }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}


/**
 * nativeCLAHE
 *
 * @author Bee Hoon
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_ImageEnhancer_nativeCLAHE(JNIEnv *jEnv, 
																		jclass jContext, 
																		jintArray jDestImage, 
																		jintArray jSrcImage, 
																		jint jBlockRadius,
																		jint jBins,
																		jfloat jSlope,
																		jboolean jIsFast)
{
 try
  {
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    JNIImageData *pSrcImage = getImageDataFromJava(jEnv, jSrcImage);

	#ifdef _DEBUG
	int debugId = idValue++;
	std::ofstream myfile("imageEnhancerLog.txt", std::ios_base::out | std::ios_base::app);
	myfile <<debugId <<",BF CLAHE,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height 
	       <<",jBlockRadius," <<jBlockRadius <<",jBins,"<< jBins <<",jSlope,"<< jSlope << std::endl;
	#endif

	SystemError::ErrorCode returnStatus = ContrastLimitAdapHisEqual(pDestImage->address, pDestImage->stride, pDestImage->width, pDestImage->height,
		pSrcImage->address, pSrcImage->stride, pSrcImage->width, pSrcImage->height,
		jBlockRadius, jBins, jSlope, PixelFormat::L32_FLOAT, jIsFast);
    
	#ifdef _DEBUG
	jint status = static_cast<int>(returnStatus);	
	
	myfile <<debugId <<",AF CLAHE,DS,"<< pDestImage->stride<<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height
           <<",jBlockRadius," <<jBlockRadius <<",jBins,"<< jBins <<",jSlope,"<< jSlope<< ",Status," << status << std::endl;
	#endif
	
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
	releaseImageDataFromJavaUnmodified(jEnv, jSrcImage, pSrcImage);
 }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeRemoveArtifactByBoxFilter
 *
 * @author Bee Hoon
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_ImageEnhancer_nativeRemoveArtifactByBoxFilter(JNIEnv *jEnv, 
																							jclass jContext, 
																							jintArray jDestImage, 
																							jintArray jSrcImage, 
																							jint jMaskWidth,
																							jint jMaskHeight,
																							jint jIteration)
{
 try
  {
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    JNIImageData *pSrcImage = getImageDataFromJava(jEnv, jSrcImage);

	#ifdef _DEBUG
	int debugId = idValue++;
	std::ofstream myfile("imageEnhancerLog.txt", std::ios_base::out | std::ios_base::app);
	myfile <<debugId <<",BF RemoveArtifactByBoxFilter,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height 
	       <<",jMaskWidth," <<jMaskWidth <<",jMaskHeight,"<< jMaskHeight <<",jIteration,"<< jIteration << std::endl;
	#endif
	
	SystemError::ErrorCode returnStatus = RemoveArtifactByBoxFilter(pDestImage->address, pDestImage->stride,
		pSrcImage->address, pSrcImage->stride, pDestImage->width, pDestImage->height,
		jMaskWidth, jMaskHeight, jIteration, PixelFormat::L32_FLOAT);
		
	#ifdef _DEBUG
	
	jint status = static_cast<int>(returnStatus);	
	myfile <<debugId <<",AF RemoveArtifactByBoxFilter,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH."<< pSrcImage->height 
	       <<",jMaskWidth," <<jMaskWidth <<",jMaskHeight,"<< jMaskHeight <<",jIteration,"<< jIteration<< ",Status," << status << std::endl;
	#endif

#ifdef _DEBUG2
	std::cout << "RemoveArtifactByBoxFilter" << std::endl;

	jint status = static_cast<int>(returnStatus);	
	std::cout << "Status:" << status;

	int actualWidth = pDestImage->stride/4;
	std::cout << "Image Actual Width: " << actualWidth << std::endl;
	std::cout << "Output file to execution folder..." << std::endl;
	std::ofstream file1("JNI_Ori_RemoveArtifact.csv", std::ios_base::out | std::ios_base::app);
	std::ofstream file2("JNI_RemoveArtifact.csv", std::ios_base::out | std::ios_base::app);
	for(int y=0; y< pDestImage->height; ++y)
		for(int x=0; x< pDestImage->width; ++x)
		{
			file1 << x << "," << y << "," << pSrcImage->address[(y*actualWidth) + x] << std::endl;
			file2 << x << "," << y << "," << pDestImage->address[(y*actualWidth) + x] << std::endl;
		}
#endif

    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
	releaseImageDataFromJavaUnmodified(jEnv, jSrcImage, pSrcImage);
 }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeBoxFilter
 *
 * @author Bee Hoon
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_ImageEnhancer_nativeBoxFilter(JNIEnv *jEnv, 
																				 jclass jContext, 
																				 jintArray jDestImage, 
																				 jintArray jSrcImage, 
																				 jint jMaskWidth,
																				 jint jMaskHeight,
																				 jint jIteration)
{
 try
  {
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    JNIImageData *pSrcImage = getImageDataFromJava(jEnv, jSrcImage);

	#ifdef _DEBUG
	int debugId = idValue++;
	std::ofstream myfile("imageEnhancerLog.txt", std::ios_base::out | std::ios_base::app);
	myfile <<debugId <<",BF BoxFilter,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height 
	       <<",jMaskWidth," <<jMaskWidth <<",jMaskHeight,"<< jMaskHeight <<",jIteration,"<< jIteration << std::endl;
	#endif
	
	SystemError::ErrorCode returnStatus = BoxFilter(pDestImage->address, pDestImage->stride ,pDestImage->width, pDestImage->height,
		pSrcImage->address, pSrcImage->stride, pSrcImage->width, pSrcImage->height,
		jMaskWidth, jMaskHeight, jIteration,PixelFormat::L32_FLOAT);
    
	#ifdef _DEBUG
	jint status = static_cast<int>(returnStatus);	
	myfile <<debugId <<",AF BoxFilter,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height 
	       <<",jMaskWidth," <<jMaskWidth <<",jMaskHeight,"<< jMaskHeight <<",jIteration,"<< jIteration << ",Status," << status << std::endl;
	#endif
	
#ifdef _DEBUG2
	std::cout << "BoxFilter" << std::endl;

	jint status = static_cast<int>(returnStatus);	
	std::cout << "Status:" << status;

	int actualWidth = pDestImage->stride/4;
	std::cout << "Image Actual Width: " << actualWidth << std::endl;
	std::cout << "Output file to execution folder..." << std::endl;
	std::ofstream file1("JNI_Ori_BoxFilter.csv", std::ios_base::out | std::ios_base::app);
	std::ofstream file2("JNI_BoxFilter.csv", std::ios_base::out | std::ios_base::app);
	for(int y=0; y< pDestImage->height; ++y)
		for(int x=0; x< pDestImage->width; ++x)
		{
			file1 << x << "," << y << "," << pSrcImage->address[(y*actualWidth) + x] << std::endl;
			file2 << x << "," << y << "," << pDestImage->address[(y*actualWidth) + x] << std::endl;
		}
#endif

    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
	releaseImageDataFromJavaUnmodified(jEnv, jSrcImage, pSrcImage);
 }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeSubtract
 *
 * @author Bee Hoon
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_ImageEnhancer_nativeSubtract(JNIEnv *jEnv, 
																			jclass jContext, 
																			jintArray jDestImage, 
																			jintArray jSrc1Image, 
																			jintArray jSrc2Image, 
																			jint jWidth,
																			jint jHeight)
{
 try
  {
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    JNIImageData *pSrc1Image = getImageDataFromJava(jEnv, jSrc1Image);
	JNIImageData *pSrc2Image = getImageDataFromJava(jEnv, jSrc2Image);

	#ifdef _DEBUG
	int debugId = idValue++;
	std::ofstream myfile("imageEnhancerLog.txt", std::ios_base::out | std::ios_base::app);
	myfile <<debugId <<",BF Subtract,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrc1Image->stride<<",SW,"<< pSrc1Image->width <<",SH,"<< pSrc1Image->height 
	       <<",jWidth," <<jWidth <<",jHeight,"<< jHeight << std::endl;
	#endif
	
	SystemError::ErrorCode returnStatus = Subtract(pDestImage->address, pDestImage->stride,
		pSrc1Image->address, pSrc1Image->stride, pSrc2Image->address, pSrc2Image->stride,
		jWidth, jHeight, PixelFormat::L32_FLOAT);
    
	#ifdef _DEBUG
	jint status = static_cast<int>(returnStatus);	
	
	myfile <<debugId <<",AF Subtract,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrc1Image->stride<<",SW,"<< pSrc1Image->width <<",SH,"<< pSrc1Image->height 
	       <<",jWidth," << jWidth <<",jHeight,"<< jHeight << ",Status," << status << std::endl;
	#endif
	
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
	releaseImageDataFromJavaUnmodified(jEnv, jSrc1Image, pSrc1Image);
	releaseImageDataFromJavaUnmodified(jEnv, jSrc2Image, pSrc2Image);
 }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeInvertImage
 *
 * @author Ying-Huan.Chu
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_ImageEnhancer_nativeInvertImage(JNIEnv *jEnv,
																			   jclass jContext, 
																			   jintArray jDestImage, 
																			   jintArray jSrcImage)
{
  try
  {
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    JNIImageData *pSrcImage = getImageDataFromJava(jEnv, jSrcImage);
	
	#ifdef _DEBUG
	int debugId = idValue++;
	std::ofstream myfile("imageEnhancerLog.txt", std::ios_base::out | std::ios_base::app);
	myfile <<debugId <<",BF InvertImage,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height << std::endl;
	#endif
	
	SystemError::ErrorCode returnStatus = InvertImage(pDestImage->address, pDestImage->stride, pDestImage->width, pDestImage->height,
				pSrcImage->address, pSrcImage->stride, pSrcImage->width, pSrcImage->height,
				PixelFormat::L32_FLOAT);
		
	#ifdef _DEBUG
	jint status = static_cast<int>(returnStatus);	
	
	myfile <<debugId <<",AF InvertImage,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height << ",Status," << status << std::endl;
	#endif
		
	releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
	releaseImageDataFromJavaUnmodified(jEnv, jSrcImage, pSrcImage);
	
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeColourTwistWithEnum
 *
 * @author Ying-Huan.Chu
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_ImageEnhancer_nativeColourTwistWithEnum(JNIEnv *jEnv,
																					   jclass jContext, 
																					   jintArray jDestImage, 
																					   jintArray jSrcImage,
																					   jint jColourTypeEnumId)
{
  try
  {
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    JNIImageData *pSrcImage = getImageDataFromJava(jEnv, jSrcImage);
	ColourType::Enum colourTypeEnum = static_cast<ColourType::Enum>(jColourTypeEnumId);
	
	#ifdef _DEBUG
	int debugId = idValue++;
	std::ofstream myfile("imageEnhancerLog.txt", std::ios_base::out | std::ios_base::app);
	myfile <<debugId <<",BF ColourTwistWithEnum,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height 
	       <<",colourTypeEnum," <<colourTypeEnum<< std::endl;
	#endif
	
	SystemError::ErrorCode returnStatus = ColourTwist(pDestImage->address, pDestImage->stride, pDestImage->width, pDestImage->height,
				pSrcImage->address, pSrcImage->stride, pSrcImage->width, pSrcImage->height,
				colourTypeEnum, PixelFormat::L32_FLOAT);
		
	#ifdef _DEBUG
	jint status = static_cast<int>(returnStatus);	
	myfile <<debugId <<",AF ColourTwistWithEnum,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height 
	       <<",colourTypeEnum," <<colourTypeEnum << ",Status," << status << std::endl;
	#endif
	
	releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
	releaseImageDataFromJavaUnmodified(jEnv, jSrcImage, pSrcImage);
	
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeColourTwistWithArray
 *
 * @author Ying-Huan.Chu
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_ImageEnhancer_nativeColourTwistWithArray(JNIEnv *jEnv,
																						jclass jContext, 
																						jintArray jDestImage, 
																						jintArray jSrcImage,
																						jobjectArray jColourTwistArray)
{
  try
  {
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    JNIImageData *pSrcImage = getImageDataFromJava(jEnv, jSrcImage);
	
	float localColourTwistArray[3][4];
	for(int i = 0; i < 3; ++i){
	  jfloatArray oneDimensionArray = (jfloatArray)jEnv->GetObjectArrayElement(jColourTwistArray, i);
	  jfloat *element = jEnv->GetFloatArrayElements(oneDimensionArray, 0);
	  for(int j = 0; j < 4; ++j) {
	    localColourTwistArray[i][j]= element[j];
	  }
	}
	
	#ifdef _DEBUG
	int debugId = idValue++;
	std::ofstream myfile("imageEnhancerLog.txt", std::ios_base::out | std::ios_base::app);
	myfile <<debugId <<",BF ColourTwistWithArray,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<"SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height << std::endl;
	#endif
	
	SystemError::ErrorCode returnStatus = ColourTwist(pDestImage->address, pDestImage->stride, pDestImage->width, pDestImage->height,
				pSrcImage->address, pSrcImage->stride, pSrcImage->width, pSrcImage->height,
				localColourTwistArray, PixelFormat::L32_FLOAT);
		
	#ifdef _DEBUG
	jint status = static_cast<int>(returnStatus);	
	
	myfile <<debugId <<",After ColourTwistWithArray,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<"SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height << ",Status," << status << std::endl;
	#endif
		
	releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
	releaseImageDataFromJavaUnmodified(jEnv, jSrcImage, pSrcImage);
	
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeColourMap
 *
 * @author Ying-Huan.Chu
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_ImageEnhancer_nativeColourMap(JNIEnv *jEnv,
																			 jclass jContext, 
																			 jstring dstFullPathFileName,
																			 jstring srcFullPathFileName,
																			 jint jColourMapTypeEnumId)
{
  try
  {
    const char *nativeDstFullPathFileName = jEnv->GetStringUTFChars(dstFullPathFileName, 0);
    const char *nativeSrcFullPathFileName = jEnv->GetStringUTFChars(srcFullPathFileName, 0);
	ColourMapType::Enum colourMapTypeEnum = static_cast<ColourMapType::Enum>(jColourMapTypeEnumId);
	
	ColourMap(nativeSrcFullPathFileName, nativeDstFullPathFileName, colourMapTypeEnum, PixelFormat::L32_FLOAT);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeFFTBandPassFilter
 *
 * @author Lim, Lay Ngor
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_ImageEnhancer_nativeFFTBandPassFilter(JNIEnv *jEnv,
																					 jclass jContext, 
																					 jintArray jDestImage, 
																					 jintArray jSrcImage,																					 
																					 jint jLowPassLargeDownTo,
																					 jint jHiPassSmallUpTo,
																					 jdouble jToleranceOfDirection,
																					 jint jSuppressStripesModeEnumId,
																					 jboolean jEnhanceContrast,
																					 jint jSaturateValue,
																					 jint jInterpolationModeEnumId)
{
 try
  {
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    JNIImageData *pSrcImage = getImageDataFromJava(jEnv, jSrcImage);
	FilterDirectionMode::Enum filterDirectionModeEnum = static_cast<FilterDirectionMode::Enum>(jSuppressStripesModeEnumId);
	InterpolationMode::Enum interpolationModeEnum = static_cast<InterpolationMode::Enum>(jInterpolationModeEnumId);
	
	#ifdef _DEBUG
	int debugId = idValue++;
	std::ofstream myfile("imageEnhancerLog.txt", std::ios_base::out | std::ios_base::app);
	myfile <<debugId <<",BF FFTBandPassFilter,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height 
	       <<",jLowPassLargeDownTo," <<jLowPassLargeDownTo <<",jHiPassSmallUpTo,"<< jHiPassSmallUpTo <<",jToleranceOfDirection,"<< jToleranceOfDirection 
		   << ",jEnhanceContrast," <<jEnhanceContrast <<",jSaturateValue,"<< jSaturateValue << std::endl;
	#endif
	
	SystemError::ErrorCode returnStatus = FFTBandPassFilter(pDestImage->address, pDestImage->stride ,pDestImage->width, pDestImage->height,
		pSrcImage->address, pSrcImage->stride, pSrcImage->width, pSrcImage->height,
		jLowPassLargeDownTo, jHiPassSmallUpTo, jToleranceOfDirection, 
		filterDirectionModeEnum, PixelFormat::L32_FLOAT,
		jEnhanceContrast, jSaturateValue, interpolationModeEnum);
    
	#ifdef _DEBUG
	jint status = static_cast<int>(returnStatus);	
	
	myfile <<debugId <<",AF FFTBandPassFilter,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height 
	       <<",jLowPassLargeDownTo," <<jLowPassLargeDownTo <<",jHiPassSmallUpTo,"<< jHiPassSmallUpTo <<",jToleranceOfDirection,"<< jToleranceOfDirection 
		   << ",jEnhanceContrast," <<jEnhanceContrast <<",jSaturateValue,"<< jSaturateValue << ",Status," << status << std::endl;
	#endif
	
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
	releaseImageDataFromJavaUnmodified(jEnv, jSrcImage, pSrcImage);
 }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
 }
  
/**
 * nativeRFilter
 * @author Siew Yeng
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_ImageEnhancer_nativeRFilter(JNIEnv *jEnv,
																					 jclass jContext, 
																					 jintArray jDestImage,
																					 jintArray jSrcImage,																					 
																					 jfloat jAlpha,
																					 jfloat jBeta,
																					 jint jScale)
{
 try
  {
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    JNIImageData *pSrcImage = getImageDataFromJava(jEnv, jSrcImage);
	
	#ifdef _DEBUG
	int debugId = idValue++;
	std::ofstream myfile("imageEnhancerLog.txt", std::ios_base::out | std::ios_base::app);
	myfile <<debugId <<",BF RFilte,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height 
	       <<",jAlpha," <<jAlpha <<",jBeta,"<< jBeta <<",jScale,"<< jScale << std::endl;
	#endif
	
	SystemError::ErrorCode returnStatus = RFilter(pDestImage->address, pDestImage->stride, pDestImage->width, pDestImage->height,
		pSrcImage->address, pSrcImage->stride, pSrcImage->width, pSrcImage->height,
		jAlpha, jBeta, jScale, PixelFormat::L32_FLOAT );
		
	#ifdef _DEBUG
	jint status = static_cast<int>(returnStatus);	
	
	myfile <<debugId <<",AF RFilter,DS,"<< pDestImage->stride <<",DW,"<< pDestImage->width <<",DH,"<< pDestImage->height
	       <<",SS,"<< pSrcImage->stride<<",SW,"<< pSrcImage->width <<",SH,"<< pSrcImage->height 
	       <<",jAlpha," <<jAlpha <<",jBeta,"<< jBeta <<",jScale,"<< jScale << ",Status," << status<< std::endl;
	#endif
    
#ifdef _DEBUG2
	std::cout << "RFilter" << std::endl;

	jint status = static_cast<int>(returnStatus);	
	std::cout << "Status:" << status;

	int actualWidth = pDestImage->stride/4;
	std::cout << "Image Actual Width: " << actualWidth << std::endl;
	std::cout << "Output file to execution folder..." << std::endl;
	std::ofstream file1("JNI_Ori_RFilter.csv", std::ios_base::out | std::ios_base::app);
	std::ofstream file2("JNI_RFilter.csv", std::ios_base::out | std::ios_base::app);
	for(int y=0; y< pDestImage->height; ++y)
		for(int x=0; x< pDestImage->width; ++x)
		{
			file1 << x << "," << y << "," << pSrcImage->address[(y*actualWidth) + x] << std::endl;
			file2 << x << "," << y << "," << pDestImage->address[(y*actualWidth) + x] << std::endl;
		}
#endif

    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
	releaseImageDataFromJavaUnmodified(jEnv, jSrcImage, pSrcImage);
 }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}
  
/**
 * nativeMotionBlur
 * @author Kok Chun
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_ImageEnhancer_nativeMotionBlur(JNIEnv *jEnv,
																		      jclass jContext, 
																		      jintArray jDestImage,
																			  jintArray jSrcImage,																					 
																			  jint scale,
																			  jint offset,
																			  jint direction,
																			  jint iteration)
{
   try
   {
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    JNIImageData *pSrcImage = getImageDataFromJava(jEnv, jSrcImage);
	FilterDirectionMode::Enum directionModeEnum = static_cast<FilterDirectionMode::Enum>(direction);
	
	SystemError::ErrorCode returnStatus = MotionBlur(pDestImage->address, pDestImage->stride, pDestImage->width, pDestImage->height,
		                                             pSrcImage->address, pSrcImage->stride, pSrcImage->width, pSrcImage->height,
		                                             scale, offset, directionModeEnum, iteration, PixelFormat::L32_FLOAT);

    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
	releaseImageDataFromJavaUnmodified(jEnv, jSrcImage, pSrcImage);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeShadingRemoval
 * @author Kok Chun
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_ImageEnhancer_nativeShadingRemoval(JNIEnv *jEnv,
																		          jclass jContext, 
																		          jintArray jDestImage,
																		   	      jintArray jSrcImage,																					 
																			      jint blurDistance,
																			      jint keepOutDistance,
																			      jint direction)
{
   try
   {
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    JNIImageData *pSrcImage = getImageDataFromJava(jEnv, jSrcImage);
	FilterDirectionMode::Enum directionModeEnum = static_cast<FilterDirectionMode::Enum>(direction);
	
	SystemError::ErrorCode returnStatus = ShadingRemoval(pDestImage->address, pDestImage->stride, pDestImage->width, pDestImage->height,
		                                                 pSrcImage->address, pSrcImage->stride, pSrcImage->width, pSrcImage->height,
		                                                 blurDistance, keepOutDistance, directionModeEnum, PixelFormat::L32_FLOAT);

    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
	releaseImageDataFromJavaUnmodified(jEnv, jSrcImage, pSrcImage);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}