package com.axi.util.image;

/**
 *
 * @author Ying-Huan.Chu
 */
public class ColourTypeEnum extends com.axi.util.Enum
{
  //Lim, Lay Ngor - XCR1652 - Image Post Processing  
  //Modify to fix number so that the value is tally with third party enum file at:
  //thirdParty\VitroxImageEnhancer\Include\ColourType.hpp, enum VxImageEnhancer::ColourType
  //This is to ensure the enum number we pass into the third party function is tally.
  public static final ColourTypeEnum BLUE = new ColourTypeEnum(1);
  public static final ColourTypeEnum GREEN = new ColourTypeEnum(2);
  public static final ColourTypeEnum RED = new ColourTypeEnum(3);
  public static final ColourTypeEnum CYAN = new ColourTypeEnum(4);
  public static final ColourTypeEnum MAGENTA = new ColourTypeEnum(5);
  public static final ColourTypeEnum YELLOW = new ColourTypeEnum(6);

  /**
   * @author Ying-Huan.Chu
   */
  public ColourTypeEnum(int id)
  {
    super(id);
  }
}
