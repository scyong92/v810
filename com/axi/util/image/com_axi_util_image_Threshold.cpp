#include <jni.h>
#include "com_axi_util_image_Threshold.h"

#include "ippcore.h"
#include "ippi.h"

#include "sptAssert.h"

#include "jniImageUtil.h"

/**
 * nativeThresholdInPlace
 * Operates in-place. 
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Threshold_nativeThresholdInPlace(JNIEnv *jEnv, 
                                                                                    jclass jContext, 
                                                                                    jintArray jSourceImage, 
                                                                                    jint roiMinX, 
                                                                                    jint roiMinY,
                                                                                    jint roiWidth, 
                                                                                    jint roiHeight,
                                                                                    jfloat thresholdLT, 
                                                                                    jfloat valueLT,
                                                                                    jfloat thresholdGT, 
                                                                                    jfloat valueGT)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize resultSize = { roiWidth, roiHeight };
    
    IppStatus ippResult = ippiThreshold_LTValGTVal_32f_C1IR(pSourceImage->addressAt(roiMinX, roiMinY), 
                                                            pSourceImage->stride,
                                                            resultSize,
                                                            thresholdLT, 
                                                            valueLT, 
                                                            thresholdGT, 
                                                            valueGT);
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeCountPixelsInRange
 *
 * @author Patrick Lacz
 */
JNIEXPORT jint JNICALL Java_com_axi_util_image_Threshold_nativeCountPixelsInRange(JNIEnv *jEnv, 
                                                                                      jclass jContext, 
                                                                                      jintArray jSourceImage, 
                                                                                      jint roiMinX, 
                                                                                      jint roiMinY,
                                                                                      jint roiWidth, 
                                                                                      jint roiHeight, 
                                                                                      jfloat lowerBound, 
                                                                                      jfloat upperBound)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize ippiRoiSize = { roiWidth, roiHeight };
    
    int count = 0;
    IppStatus ippResult = ippiCountInRange_32f_C1R(pSourceImage->addressAt(roiMinX, roiMinY), 
                                                   pSourceImage->stride,
                                                   ippiRoiSize, 
                                                   &count, 
                                                   lowerBound,
                                                   upperBound);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    return count;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * nativeSearchForFirstPixelInRange
 * Operates in-place. 
 *
 * @author Patrick Lacz
 */
JNIEXPORT jfloat JNICALL Java_com_axi_util_image_Threshold_nativeSearchForFirstPixelInRange(JNIEnv *jEnv, 
                                                                                              jclass jContext, 
                                                                                              jintArray jSourceImage, 
                                                                                              jint roiMinX, 
                                                                                              jint roiMinY,
                                                                                              jint roiWidth, 
                                                                                              jint roiHeight,
                                                                                              jint startX,
                                                                                              jint startY,
                                                                                              jfloat thresholdMin,
                                                                                              jfloat thresholdMax,
                                                                                              jintArray jReturnXY)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);

    int endX = roiMinX + roiWidth;
    int endY = roiMinY + roiHeight;

    sptAssert(startX >= roiMinX || startY >= roiMinY);
    sptAssert(startX < endX || startY < endY);

    int x = startX;
    int y = startY;

    bool foundMatchingPixel = false;
    float matchingValue = 0.f;

    while (x < endX && y < endY)
    {
      matchingValue = *pSourceImage->addressAt(x, y);

      if (matchingValue >= thresholdMin && matchingValue <= thresholdMax)
      {
        foundMatchingPixel = true;
        break;
      }

      // advance to the next pixel
      ++x;
      if (x >= endX)
      {
        ++y;
        x = roiMinX;
      }
    }
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage); 

    jboolean isCopy;
    jint *returnArray = jEnv->GetIntArrayElements(jReturnXY, &isCopy);
    
    if (foundMatchingPixel)
    {
      // write the location in the returnXY array
      returnArray[0] = x;
      returnArray[1] = y;
    }
    else
    {
      // return values indicating the value was not found.
      returnArray[0] = -1;
      returnArray[1] = -1;
    }
    jEnv->ReleaseIntArrayElements(jReturnXY, returnArray, 0);

    // return the value
    return matchingValue;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
  return 0.f;
}


/**
 * nativeHistogramRange
 *
 * @author Patrick Lacz
 */
JNIEXPORT jintArray JNICALL Java_com_axi_util_image_Threshold_nativeHistogramRange(JNIEnv *jEnv, 
                                                                                       jclass jContext, 
                                                                                       jintArray jSourceImage, 
                                                                                       jint roiMinX, 
                                                                                       jint roiMinY, 
                                                                                       jint roiWidth, 
                                                                                       jint roiHeight, 
                                                                                       jfloatArray jBinThresholds)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    int numThresholds = jEnv->GetArrayLength(jBinThresholds);
    jfloat *pThresholds = reinterpret_cast<jfloat*>(jEnv->GetPrimitiveArrayCritical(jBinThresholds, 0));
    
    // The number of bins is one less than the number of thresholds.
    jintArray resultArray = jEnv->NewIntArray(numThresholds-1);
    jint *pResult = reinterpret_cast<jint*>(jEnv->GetPrimitiveArrayCritical(resultArray, 0));
    
    IppiSize roiSize = { roiWidth, roiHeight };
    
    IppStatus ippResult = ippiHistogramRange_32f_C1R(pSourceImage->addressAt(roiMinX, roiMinY), 
                                                     pSourceImage->stride,
                                                     roiSize, 
                                                     reinterpret_cast<Ipp32s*>(pResult), 
                                                     pThresholds, 
                                                     numThresholds);

    // must 'release' the array pointers back Java for the results to be stored.
    //jEnv->ReleasePrimitiveArrayCritical(jBinThresholds, reinterpret_cast<void*>(pThresholds), JNI_ABORT);
    jEnv->ReleasePrimitiveArrayCritical(jBinThresholds, reinterpret_cast<void*>(pThresholds), 0);
    jEnv->ReleasePrimitiveArrayCritical(resultArray, reinterpret_cast<void*>(pResult), 0);
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
    return resultArray;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return NULL;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return NULL;
  }
}
