package com.axi.util.image;

// Blob Finder Demo
// A.Greensted
// http://www.labbookpages.co.uk
// Please use however you like. I'd be happy to hear any feedback or comments.
import com.axi.util.*;
import ij.*;
import ij.process.*;
import java.io.*;
import java.util.*;
import java.awt.image.*;
import javax.imageio.*;

public class BlobAnalyzer
{

  private static BlobAnalyzer _blobAnalyzer;

  public static BlobAnalyzer getInstance()
  {
    if(_blobAnalyzer == null)
    {
      _blobAnalyzer = new BlobAnalyzer();
    }

    return _blobAnalyzer;
  }

  private BlobAnalyzer()
  {
  }

  /**
   * Modify to add in parameter for corners case blob return.
   * @param srcImage
   * @return 
   * @author Lim, Lay Ngor - XCR2100 - BrokenPin
   */
  public java.util.List<java.awt.Shape> analyze(BufferedImage srcImage, boolean detectBlobsAtCorner)
  {

    java.util.List<java.awt.Shape> blobList = new LinkedList<java.awt.Shape>();
    int width = srcImage.getWidth();
    int height = srcImage.getHeight();

    // Get raw image data
    Raster raster = srcImage.getData();
    DataBuffer buffer = (java.awt.image.DataBufferByte) raster.getDataBuffer();

    int type = buffer.getDataType();
   // System.out.println("type:" + type);
    if(type != DataBuffer.TYPE_BYTE)
    {
      System.err.println("Wrong image data type");
      System.exit(1);
    }
    if(buffer.getNumBanks() != 1)
    {
      System.err.println("Wrong image data format");
      System.exit(1);
    }

    DataBufferByte byteBuffer = (DataBufferByte) buffer;
    byte[] srcData = byteBuffer.getData(0);

    // Sanity check image
    if(width * height * 3 != srcData.length)
    {
      System.err.println("Unexpected image data size. Should be RGB image");
      System.exit(1);
    }

    // Create Monochrome version - using basic threshold technique
    byte[] monoData = new byte[width * height];
    int srcPtr = 0;
    int monoPtr = 0;

    while (srcPtr < srcData.length)
    {
      int val = ((srcData[srcPtr] & 0xFF) + (srcData[srcPtr + 1] & 0xFF) + (srcData[srcPtr + 2] & 0xFF)) / 3;
      monoData[monoPtr] = (val > 128) ? (byte) 0xFF : 0;

      srcPtr += 3;
      monoPtr += 1;
    }

    byte[] dstData = new byte[srcData.length];

    // Create Blob Finder
    BlobFinder finder = new BlobFinder(width,height);

    finder.detectBlobs(monoData,dstData,0,-1,(byte) 0,blobList, detectBlobsAtCorner);

    // List Blobs
    //System.out.printf("Found %d blobs:\n",blobList.size());
    //int i = 1;
   /* System.out.println("");
    for (java.awt.Shape blob : blobList)
    {
      System.out.println(blob.getBounds().getMinX() + "," + blob.getBounds().getMinY() + "," + blob.getBounds().getWidth() + "," + blob.getBounds().getHeight());
      i++;
    }*/
    /*// Create GUI
    RGBFrame srcFrame = new RGBFrame(width,height,srcData);
    RGBFrame dstFrame = new RGBFrame(width,height,dstData);

    JPanel panel = new JPanel(new BorderLayout(5,5));
    panel.setBorder(new javax.swing.border.EmptyBorder(5,5,5,5));
    panel.add(srcFrame,BorderLayout.WEST);
    panel.add(dstFrame,BorderLayout.EAST);
    panel.add(new JLabel("A.Greensted - http://www.labbookpages.co.uk",JLabel.CENTER),BorderLayout.SOUTH);

    JFrame frame = new JFrame("Blob Detection Demo");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.getContentPane().add(panel);
    frame.pack();
    frame.setVisible(true);*/

    return blobList;
  }
  
  /**
   * Add blobDetection wrapper function which to be call by V810 function.
   * Please develop the 8 bit blob detection code.
   * Now we convert the 8 bit image to RGB image in order to use the current blobFinder.
   * @param image
   * @return 
   * @author Lim, Lay Ngor - XCR2100 - BrokenPin & OKI Clear tombstone use
   */
  public java.util.List<java.awt.Shape> blobDetection(Image image)
  {
    Assert.expect(image != null);
    
    java.awt.image.BufferedImage bufferImage = image.getBufferedImage();
    ImagePlus imagePlus = new ImagePlus("", bufferImage);

    //convert to RGB
    ImageConverter ic = new ImageConverter(imagePlus);
    ic.convertToRGB();
    ByteArrayOutputStream byar = new ByteArrayOutputStream();
    try
    {
      ImageIO.write(imagePlus.getBufferedImage(), "jpg", byar);
      byar.flush();
    }
    catch (IOException ex)
    {
      System.out.println("blobDetection:" + ex);
    }

    byte[] b = byar.toByteArray();
    InputStream in = new ByteArrayInputStream(b);
    java.awt.image.BufferedImage bImageFromConvert = null;
    try
    {
      bImageFromConvert = ImageIO.read(in);
    }
    catch (IOException ex)
    {
      System.out.println("blobDetection:" + ex);
    }

    java.util.List<java.awt.Shape> sourceShapeList = new LinkedList<java.awt.Shape>();
    //Lim, Lay Ngor - temporary created function for BrokenPin use.
    sourceShapeList = BlobAnalyzer.getInstance().analyze(bImageFromConvert, true);
//    System.out.println("sourceShapeList" + sourceShapeList.size());
    
    imagePlus.flush();
    return sourceShapeList;
  }
  
  public static void main(String[] args)
  {
    /*   BufferedImage srcImage = null;
    try
    {
    File imgFile = new File("C:\\Users\\khang-shian.sham.VITROX\\Desktop\\Alignment Data\\fail\\8\\Raw_Manual_Alignment_NEPCON_WIDE_EW_1.png");
    srcImage = javax.imageio.ImageIO.read(imgFile);
    }
    catch (IOException ioE)
    {
    System.err.println(ioE);
    System.exit(1);
    }
    getInstance().analyze(srcImage);
     */
    for (int i = 0; i < 1; i++)
    {
      System.out.println("i:" + i);
      ImagePlus imagePlus = new ImagePlus("D:\\b4binary.png");
      //Apply Sauvola Local Threshold
      AutoLocalThreshold.getInstance().Sauvola(imagePlus,94 / 2,0.1,94,true);
      //Convert to Binary
      MaskImageConverter cm = new MaskImageConverter();
      cm.convertImageToBinary(imagePlus);
      ij.IJ.save(imagePlus,"D:\\afbinary.png");
      //convert to RGB
      ImageConverter ic = new ImageConverter(imagePlus);
      ic.convertToRGB();
      ByteArrayOutputStream byar = new ByteArrayOutputStream();
      try
      {
        ImageIO.write(imagePlus.getBufferedImage(),"jpg",byar);
        byar.flush();
      }
      catch (IOException ex)
      {
      }
      byte[] b = byar.toByteArray();

      /*  try
      {
      JPEGCodec.createJPEGEncoder(byar).encode(imagePlus.getBufferedImage());
      }
      catch (IOException ex)
      {
      }
      catch (ImageFormatException ex)
      {
      }

      byte[] b = byar.toByteArray();*/
      InputStream in = new ByteArrayInputStream(b);
      BufferedImage bImageFromConvert = null;
      try
      {
        bImageFromConvert = ImageIO.read(in);
      }
      catch (IOException ex)
      {
      }
      
      //Lim, Lay Ngor - Broken pin - modify the original function to support corner blob detection.
      BlobAnalyzer.getInstance().analyze(bImageFromConvert, false);
    }
  }
}
