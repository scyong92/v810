package com.axi.util.image;

import java.io.*;

import com.axi.util.*;
import java.util.Arrays;
import java.util.Collection;
import java.awt.image.BufferedImage;

/**
 * @author Patrick Lacz
 */
public class Test_Threshold extends UnitTest
{
  /**
   * @author Patrick Lacz
   */
  public Test_Threshold()
  {
  }

  /**
   * @author Patrick Lacz
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Threshold());
  }

  /**
   * @author Patrick Lacz
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Image image = loadTestImage(getTestDataDir(), "test1.png");

    testThreshold(image);
    testHistogram(image);
    testCountPixelsInRange(image);
    testEstimatePixelValueAtPercentileUsingHistogram();
    image.decrementReferenceCount();
  }


  /**
   * @author Patrick Lacz
   */
  private void testHistogram(Image originalImage)
  {
    Image image = new Image(originalImage);
     RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder(0,0,
         image.getWidth()-1,image.getHeight()-1,0,RegionShapeEnum.RECTANGULAR);
     int histResult[] = Threshold.histogram(image, roi, 5);
     int expectedResult[] = {18, 0, 9, 0, 9};
     Expect.expect(histResult.length == expectedResult.length);
     for (int i = 0 ; i < histResult.length ; ++i)
       Expect.expect(histResult[i] == expectedResult[i]);
     image.decrementReferenceCount();
  }
  /**
   * @author Matt Wharton
   */
  private void testEstimatePixelValueAtPercentileUsingHistogram()
  {
    // Test asserts.
    // Should assert due to null array.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        Threshold.estimatePixelValueAtPercentileUsingHistogram(null, 50f, 0, 255);
      }
    });

    // This should assert since the percentile is out of bounds.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        Threshold.estimatePixelValueAtPercentileUsingHistogram(new int[]{1, 2}, -5, 0, 255);
      }
    });

    // This should asserts since this overload expects an array with 255 elements.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        Threshold.estimatePixelValueAtPercentileUsingHistogram(new int[]{1, 2}, 40f, 0, 256);
      }
    });

    // This should assert since the min is larger than the max.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        Threshold.estimatePixelValueAtPercentileUsingHistogram(new int[]{1, 2}, 50f, 5, 4);
      }
    });

    // Test with one bin per pixel value.
    int[] histogramBins = new int[256];
    Arrays.fill(histogramBins, 1);
    float fiftiethPercentile = Threshold.getPixelValueAtPercentileUsingHistogram(histogramBins, 50.f, 0, 255);
    Expect.expect(MathUtil.fuzzyEquals(fiftiethPercentile, 127.5f));

    // Test with bins that span multiple pixel values.
    histogramBins = new int[]{ 4, 6, 5 };
    fiftiethPercentile = Threshold.getPixelValueAtPercentileUsingHistogram(histogramBins, 27.f, 0, 255);
    Expect.expect(MathUtil.fuzzyEquals(fiftiethPercentile, 99.45003f));
  }
  /**
   * @author Patrick Lacz
   */
  private void testCountPixelsInRange(Image originalImage)
  {
    Assert.expect(originalImage != null);

    Image image = new Image(originalImage);

    // test illegal regions of interest
    Collection<RegionOfInterest> regions = ImageUnitTestUtil.generateBadRegionsOfInterest(image);
    for (RegionOfInterest regionOfInterest : regions)
    {
      // countPixelsInRange
      try
      {
        Threshold.countPixelsInRange(image, regionOfInterest, 0.0f, 255.f);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
    }

     int zeroCount = Threshold.countPixelsInRange(image, -0.5f, 0.5f);
     Expect.expect(zeroCount == 18);
     int grayCount = Threshold.countPixelsInRange(image, 126.5f, 127.5f);
     Expect.expect(grayCount == 9);
     int lightCount = Threshold.countPixelsInRange(image, 146.5f, 147.5f);
     Expect.expect(lightCount == 0);
     int whiteCount = Threshold.countPixelsInRange(image, 254.5f, 255.5f);
     Expect.expect(whiteCount == 9);

     image.decrementReferenceCount();
   }

  /**
   * @author Patrick Lacz
   */
  private void testThreshold(Image originalImage)
  {
    Assert.expect(originalImage != null);

    Image image = new Image(originalImage);


    RegionOfInterest thresholdRegion = RegionOfInterest.createRegionFromRegionBorder(1,1,4,4,0,RegionShapeEnum.RECTANGULAR);
    // Whole Image.
    Threshold.threshold(image, thresholdRegion, 50.0f, 25.0f, 200.0f, 175.0f);
    float minResult = Statistics.minValue(image);
    Expect.expect(Math.abs(minResult - 0.f) < 0.00001f);
    minResult = Statistics.minValue(image, thresholdRegion);
    Expect.expect(Math.abs(minResult - 25.f) < 0.00001f);

    float maxResult = Statistics.maxValue(image);
    Expect.expect(Math.abs(maxResult - 255.f) < 0.00001f);
    maxResult = Statistics.maxValue(image,  thresholdRegion);
    Expect.expect(Math.abs(maxResult - 175.f) < 0.00001f);

    float meanResult = Statistics.mean(image);
    Expect.expect(Math.abs(meanResult - 92.16666f) < 0.00001f);

    image.decrementReferenceCount();
     // @todo more test cases
  }

  /**
   * Copied from Test_ImageFeatureExtraction
   * @author Peter Esbensen
   */
  private Image loadTestImage(String directoryPath, String filename)
  {
    Assert.expect(directoryPath != null);
    Assert.expect(directoryPath.length() > 0);
    Assert.expect(filename != null);
    Assert.expect(filename.length() > 0);

    Image resultImage = null;
    try
    {
      resultImage = ImageIoUtil.loadPngImage(directoryPath + File.separator + filename);
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "Exception occurred during PNG Image Load: " + exc.getMessage());
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(false, "FileDoesNotExistException:" + exc.getMessage());
    }
    Assert.expect(resultImage != null);
    return resultImage;
  }
 }
