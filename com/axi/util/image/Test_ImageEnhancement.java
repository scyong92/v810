package com.axi.util.image;

import java.io.*;

import com.axi.util.*;
/**
 * @author Patrick Lacz
 */
public class Test_ImageEnhancement extends UnitTest
{
  /**
   * @author Patrick Lacz
   */
  public Test_ImageEnhancement()
  {
  }

  /**
   * @author Patrick Lacz
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ImageEnhancement());
  }

  /**
   * @author Patrick Lacz
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    FloatRef a = new FloatRef();
    FloatRef b = new FloatRef();

    Image smallTestImage = new Image(100, 100);

    // Constant colors should not cause errors
    Paint.fillImage(smallTestImage, 0.0f);
    ImageEnhancement.autoEnhanceContrastInPlace(smallTestImage);
    Paint.fillImage(smallTestImage, 0.0f);
    ImageEnhancement.enhanceContrastInPlace(smallTestImage, RegionOfInterest.createRegionFromImage(smallTestImage), a, b);

    Paint.fillImage(smallTestImage, 160.f);
    ImageEnhancement.autoEnhanceContrastInPlace(smallTestImage);
    Paint.fillImage(smallTestImage, 160.0f);
    ImageEnhancement.enhanceContrastInPlace(smallTestImage, RegionOfInterest.createRegionFromImage(smallTestImage), a, b);

    for (RegionOfInterest badRoi : ImageUnitTestUtil.generateBadRegionsOfInterest(smallTestImage))
    {
      try {
        ImageEnhancement.autoEnhanceContrastInPlace(smallTestImage, badRoi);
        Expect.expect(false);
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
      try
      {
        ImageEnhancement.enhanceContrastInPlace(smallTestImage, badRoi, a, b);
        Expect.expect(false);
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }

    }
    smallTestImage.decrementReferenceCount();
  }
}
