#include <algorithm>

#include <jni.h>
#include "com_axi_util_image_Arithmetic.h"

#include "ippcore.h"
#include "ippi.h"
#include "ipps.h"

#include "sptAssert.h"

#include "jniImageUtil.h"

/**
 * nativeAdd
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Arithmetic_nativeAdd(JNIEnv *jEnv, 
                                                                        jclass jContext, 
                                                                        jintArray jLeftImage, 
                                                                        jint leftBufferOffsetX, 
                                                                        jint leftBufferOffsetY,
                                                                        jintArray jRightImage, 
                                                                        jint rightBufferOffsetX, 
                                                                        jint rightBufferOffsetY,
                                                                        jintArray jDestImage)
{
  try
  {
    JNIImageData *pLeftImage = getImageDataFromJava(jEnv, jLeftImage);
    JNIImageData *pRightImage = getImageDataFromJava(jEnv, jRightImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    
    IppStatus ippResult = ippiAdd_32f_C1R(pLeftImage->addressAt(leftBufferOffsetX, leftBufferOffsetY), 
                                          pLeftImage->stride,
                                          pRightImage->addressAt(rightBufferOffsetX, rightBufferOffsetY), 
                                          pRightImage->stride,
                                          pDestImage->address, 
                                          pDestImage->stride, 
                                          pDestImage->getSize());
    
    releaseImageDataFromJavaUnmodified(jEnv, jLeftImage, pLeftImage);
    releaseImageDataFromJavaUnmodified(jEnv, jRightImage, pRightImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeSubtract
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Arithmetic_nativeSubtract(JNIEnv *jEnv, 
                                                                             jclass jContext, 
                                                                             jintArray jLeftImage, 
                                                                             jint leftBufferOffsetX, 
                                                                             jint leftBufferOffsetY,
                                                                             jintArray jRightImage, 
                                                                             jint rightBufferOffsetX, 
                                                                             jint rightBufferOffsetY,
                                                                             jintArray jDestImage)
{
  try
  {
    JNIImageData *pLeftImage = getImageDataFromJava(jEnv, jLeftImage);
    JNIImageData *pRightImage = getImageDataFromJava(jEnv, jRightImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    
    IppStatus ippResult = ippiSub_32f_C1R(pLeftImage->addressAt(leftBufferOffsetX, leftBufferOffsetY), 
                                          pLeftImage->stride,
                                          pRightImage->addressAt(rightBufferOffsetX, rightBufferOffsetY), 
                                          pRightImage->stride,
                                          pDestImage->address, 
                                          pDestImage->stride, 
                                          pDestImage->getSize());
    
    releaseImageDataFromJavaUnmodified(jEnv, jLeftImage, pLeftImage);
    releaseImageDataFromJavaUnmodified(jEnv, jRightImage, pRightImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
  
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeMultiply
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Arithmetic_nativeMultiply(JNIEnv *jEnv, 
                                                                             jclass jContext, 
                                                                             jintArray jLeftImage, 
                                                                             jint leftBufferOffsetX, 
                                                                             jint leftBufferOffsetY,
                                                                             jintArray jRightImage, 
                                                                             jint rightBufferOffsetX, 
                                                                             jint rightBufferOffsetY,
                                                                             jintArray jDestImage)
{
  try
  {
    JNIImageData *pLeftImage = getImageDataFromJava(jEnv, jLeftImage);
    JNIImageData *pRightImage = getImageDataFromJava(jEnv, jRightImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    
    IppStatus ippResult = ippiMul_32f_C1R(pLeftImage->addressAt(leftBufferOffsetX, leftBufferOffsetY), 
                                          pLeftImage->stride,
                                          pRightImage->addressAt(rightBufferOffsetX, rightBufferOffsetY), 
                                          pRightImage->stride,
                                          pDestImage->address, 
                                          pDestImage->stride, 
                                          pDestImage->getSize());
    
    releaseImageDataFromJavaUnmodified(jEnv, jLeftImage, pLeftImage);
    releaseImageDataFromJavaUnmodified(jEnv, jRightImage, pRightImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeMaximum
 *
 * The implementation of this method requires that the rows of the input and output images be contiguous. That is, that
 * there is no gap in memory between the last pixel on row i and the first pixel on row i + 1.
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Arithmetic_nativeMaximum(JNIEnv *jEnv, 
                                                                            jclass jContext, 
                                                                            jintArray jLeftImage, 
                                                                            jint leftBufferOffsetX, 
                                                                            jint leftBufferOffsetY,
                                                                            jintArray jRightImage, 
                                                                            jint rightBufferOffsetX, 
                                                                            jint rightBufferOffsetY,
                                                                            jintArray jDestImage)
{
  try
  {
    JNIImageData *pLeftImage = getImageDataFromJava(jEnv, jLeftImage);
    JNIImageData *pRightImage = getImageDataFromJava(jEnv, jRightImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    
    IppiSize resultSize = pDestImage->getSize();
    IppStatus ippResult;

    // Allocate an image to hold both images in a way that we can do a 1x2 max filter.
    
    int intermediateBufferStride;
    Ipp32f *intermediateBuffer = ippiMalloc_32f_C1(pDestImage->width, 2 * pDestImage->height, &intermediateBufferStride);
    
    /** @todo PWL : We can do without this intermediate buffer if the stride of the destination image matches that of the intermediate
     *  buffer. In that case, do not allocate the intermediate result buffer. */  
    int intermediateResultBufferStride;
    Ipp32f *intermediateResultBuffer = ippiMalloc_32f_C1(pDestImage->width, pDestImage->height, &intermediateResultBufferStride);
    sptAssert(intermediateBuffer != NULL);
    sptAssert(intermediateBufferStride == intermediateResultBufferStride);
    
    
    // Copy the images into the intermediate images
    ippResult = ippiCopy_32f_C1R(pLeftImage->addressAt(leftBufferOffsetX, leftBufferOffsetY), 
                                 pLeftImage->stride,
                                 intermediateBuffer, 
                                 intermediateBufferStride, 
                                 resultSize);
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
    
    ippResult = ippiCopy_32f_C1R(pRightImage->addressAt(rightBufferOffsetX, rightBufferOffsetY), 
                                 pLeftImage->stride,
                                 reinterpret_cast<Ipp32f*>(reinterpret_cast<Ipp8u*>(intermediateBuffer) + intermediateBufferStride * pDestImage->height), 
                                 intermediateBufferStride, 
                                 resultSize);
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
    
    IppiSize intermediateBufferRow = { (intermediateBufferStride/4) * pDestImage->height, 1 };
    // Perform the maximum operation into the destination
    IppiSize maskSize = { 1, 2 };
    IppiPoint anchorPoint = {0, 0};
    ippResult = ippiFilterMax_32f_C1R(intermediateBuffer, 
                                      intermediateBufferStride * pDestImage->height, 
                                      intermediateResultBuffer, 
                                      intermediateResultBufferStride, 
                                      intermediateBufferRow,
                                      maskSize,
                                      anchorPoint);
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
    
    ippResult = ippiCopy_32f_C1R(intermediateResultBuffer, 
                                 intermediateResultBufferStride,
                                 pDestImage->address, 
                                 pDestImage->stride,
                                 resultSize);
    
    ippiFree(intermediateBuffer);	
    ippiFree(intermediateResultBuffer);
    
    releaseImageDataFromJavaUnmodified(jEnv, jLeftImage, pLeftImage);
    releaseImageDataFromJavaUnmodified(jEnv, jRightImage, pRightImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeMultiplyByConstant
 * Operates in-place. 
 * @todo: change to not-in-place?
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Arithmetic_nativeMultiplyByConstant(JNIEnv *jEnv, 
                                                                                       jclass jContext, 
                                                                                       jintArray jSourceImage, 
                                                                                       jint roiX, 
                                                                                       jint roiY, 
                                                                                       jint roiWidth, 
                                                                                       jint roiHeight, 
                                                                                       jfloat value)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize resultSize = { roiWidth, roiHeight };
    
    IppStatus ippResult = ippiMulC_32f_C1IR(value,
                                            pSourceImage->addressAt(roiX, roiY), pSourceImage->stride,
                                            resultSize);
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeAddConstant
 * Operates in-place. 
 * @todo: change to not-in-place?
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Arithmetic_nativeAddConstant(JNIEnv *jEnv, 
                                                                                jclass jContext, 
                                                                                jintArray jSourceImage, 
                                                                                jint roiX, 
                                                                                jint roiY, 
                                                                                jint roiWidth, 
                                                                                jint roiHeight, 
                                                                                jfloat value)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize resultSize = { roiWidth, roiHeight };
    
    IppStatus ippResult = ippiAddC_32f_C1IR(value,
                                            pSourceImage->addressAt(roiX, roiY), pSourceImage->stride,
                                            resultSize);
  
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeAbsoluteValueInPlace
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Arithmetic_nativeAbsoluteValueInPlace(JNIEnv *jEnv, 
                                                                                         jclass jContext, 
                                                                                         jintArray jSourceImage, 
                                                                                         jint roiX, 
                                                                                         jint roiY, 
                                                                                         jint roiWidth, 
                                                                                         jint roiHeight)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize resultSize = { roiWidth, roiHeight };
    
    IppStatus ippResult = ippiAbs_32f_C1IR(pSourceImage->addressAt(roiX, roiY), 
                                           pSourceImage->stride,
                                           resultSize);
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeNaturalLogarithmInPlace
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Arithmetic_nativeNaturalLogarithmInPlace(JNIEnv *jEnv, 
                                                                                            jclass jContext, 
                                                                                            jintArray jSourceImage, 
                                                                                            jint roiX, 
                                                                                            jint roiY, 
                                                                                            jint roiWidth, 
                                                                                            jint roiHeight)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize resultSize = { roiWidth, roiHeight };
  
    IppStatus ippResult = ippiLn_32f_C1IR(pSourceImage->addressAt(roiX, roiY), 
                                          pSourceImage->stride,
                                          resultSize);
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeExponentialInPlace
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Arithmetic_nativeExponentialInPlace
  (JNIEnv *jEnv, jclass jContext, 
     jintArray jSourceImage, jint roiX, jint roiY, jint roiWidth, jint roiHeight)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize resultSize = { roiWidth, roiHeight };
    
    IppStatus ippResult = ippiExp_32f_C1IR(pSourceImage->addressAt(roiX, roiY), 
                                           pSourceImage->stride,
                                           resultSize);
  
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeSquareRootInPlace
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Arithmetic_nativeSquareRootInPlace(JNIEnv *jEnv, 
                                                                                      jclass jContext, 
                                                                                      jintArray jSourceImage, 
                                                                                      jint roiX, 
                                                                                      jint roiY, 
                                                                                      jint roiWidth, 
                                                                                      jint roiHeight)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize resultSize = { roiWidth, roiHeight };
    
    IppStatus ippResult = ippiSqrt_32f_C1IR(pSourceImage->addressAt(roiX, roiY), 
                                            pSourceImage->stride,
                                            resultSize);
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeSquareInPlace
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Arithmetic_nativeSquareInPlace(JNIEnv *jEnv, 
                                                                                  jclass jContext, 
                                                                                  jintArray jSourceImage, 
                                                                                  jint roiX, 
                                                                                  jint roiY, 
                                                                                  jint roiWidth, 
                                                                                  jint roiHeight)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize resultSize = { roiWidth, roiHeight };
    
    IppStatus ippResult = ippiSqr_32f_C1IR(pSourceImage->addressAt(roiX, roiY), 
                                           pSourceImage->stride,
                                           resultSize);
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativePolarToCartesian
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Arithmetic_nativePolarToCartesian(JNIEnv *jEnv, 
                                                                                     jclass jContext, 
                                                                                     jintArray jRadiusImage, 
                                                                                     jintArray jThetaImage,
                                                                                     jintArray jDestXImage, 
                                                                                     jintArray jDestYImage)
{
  try
  {
    JNIImageData *pRadiusImage = getImageDataFromJava(jEnv, jRadiusImage);
    JNIImageData *pThetaImage = getImageDataFromJava(jEnv, jThetaImage);
    JNIImageData *pDestXImage = getImageDataFromJava(jEnv, jDestXImage);
    JNIImageData *pDestYImage = getImageDataFromJava(jEnv, jDestYImage);
    
    int numberOfPixels = pDestXImage->stride*pDestXImage->height/4;
    
    IppStatus ippResult = ippsPolarToCart_32f(pRadiusImage->address, pThetaImage->address, pDestXImage->address, pDestYImage->address, numberOfPixels);
    
    releaseImageDataFromJavaUnmodified(jEnv, jRadiusImage, pRadiusImage);
    releaseImageDataFromJavaUnmodified(jEnv, jThetaImage, pThetaImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestXImage, pDestXImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestYImage, pDestYImage);
  
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeAnd
 *
 * @author Jack Hwee
*/ 
JNIEXPORT void JNICALL Java_com_axi_util_image_Arithmetic_nativeAnd(JNIEnv *jEnv, 
                                                                        jclass jContext, 
                                                                        jintArray jLeftImage, 
                                                                        jint leftBufferOffsetX, 
                                                                        jint leftBufferOffsetY,
                                                                        jintArray jRightImage, 
                                                                        jint rightBufferOffsetX, 
                                                                        jint rightBufferOffsetY,
                                                                        jintArray jDestImage)
{
  try
  {
    JNIImageData *pLeftImage = getImageDataFromJava(jEnv, jLeftImage);
    JNIImageData *pRightImage = getImageDataFromJava(jEnv, jRightImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
  
    IppStatus ippResult = ippiAnd_32s_C1R(reinterpret_cast<Ipp32s const *>(pLeftImage->addressAt(leftBufferOffsetX, leftBufferOffsetY)), 
                                          pLeftImage->stride,
                                          reinterpret_cast<Ipp32s const *>(pRightImage->addressAt(rightBufferOffsetX, rightBufferOffsetY)), 
                                          pRightImage->stride,
                                          reinterpret_cast<Ipp32s *>(pDestImage->address), 
                                          pDestImage->stride, 
                                          pDestImage->getSize());
    
    releaseImageDataFromJavaUnmodified(jEnv, jLeftImage, pLeftImage);
    releaseImageDataFromJavaUnmodified(jEnv, jRightImage, pRightImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}     

/**
 * nativeOr
 *
 * @author Jack Hwee
*/ 
JNIEXPORT void JNICALL Java_com_axi_util_image_Arithmetic_nativeOr(JNIEnv *jEnv, 
                                                                        jclass jContext, 
                                                                        jintArray jLeftImage, 
                                                                        jint leftBufferOffsetX, 
                                                                        jint leftBufferOffsetY,
                                                                        jintArray jRightImage, 
                                                                        jint rightBufferOffsetX, 
                                                                        jint rightBufferOffsetY,
                                                                        jintArray jDestImage)
{
  try
  {
    JNIImageData *pLeftImage = getImageDataFromJava(jEnv, jLeftImage);
    JNIImageData *pRightImage = getImageDataFromJava(jEnv, jRightImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
  
    IppStatus ippResult = ippiOr_32s_C1R(reinterpret_cast<Ipp32s const *>(pLeftImage->addressAt(leftBufferOffsetX, leftBufferOffsetY)), 
                                          pLeftImage->stride,
                                          reinterpret_cast<Ipp32s const *>(pRightImage->addressAt(rightBufferOffsetX, rightBufferOffsetY)), 
                                          pRightImage->stride,
                                          reinterpret_cast<Ipp32s *>(pDestImage->address), 
                                          pDestImage->stride, 
                                          pDestImage->getSize());
    
    releaseImageDataFromJavaUnmodified(jEnv, jLeftImage, pLeftImage);
    releaseImageDataFromJavaUnmodified(jEnv, jRightImage, pRightImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}     
