package com.axi.util.image;

/**
 *
 * @author Ying-Huan.Chu
 */
public class ColourMapTypeEnum extends com.axi.util.Enum
{
  //Lim, Lay Ngor - XCR1652 - Image Post Processing  
  //Modify to fix number so that the value is tally with third party enum file at:
  //thirdParty\VitroxImageEnhancer\Include\ColourMapType.hpp, enum VxImageEnhancer::ColourMapType
  //This is to ensure the enum number we pass into the third party function is tally.
  public static final ColourMapTypeEnum FIRE = new ColourMapTypeEnum(1);
  public static final ColourMapTypeEnum ICE = new ColourMapTypeEnum(2);
  public static final ColourMapTypeEnum UNIONJACK = new ColourMapTypeEnum(3);
  public static final ColourMapTypeEnum COOL = new ColourMapTypeEnum(4);
  public static final ColourMapTypeEnum SPECTRUM = new ColourMapTypeEnum(5);
  public static final ColourMapTypeEnum GREENFIREBLUE = new ColourMapTypeEnum(6);
  public static final ColourMapTypeEnum ROYAL = new ColourMapTypeEnum(7);
  public static final ColourMapTypeEnum SMART = new ColourMapTypeEnum(8);
  public static final ColourMapTypeEnum THERMAL = new ColourMapTypeEnum(9);
  public static final ColourMapTypeEnum RAINBOW = new ColourMapTypeEnum(10);
  public static final ColourMapTypeEnum SEPIA = new ColourMapTypeEnum(11);

  /**
   * @author Ying-Huan.Chu
   */
  public ColourMapTypeEnum(int id)
  {
    super(id);
  }
}
