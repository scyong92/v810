package com.axi.util.image;

import java.awt.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Peter Esbensen
 */
class Test_ImageFeatureExtraction extends UnitTest
{
  /**
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ImageFeatureExtraction());
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Image image = loadTestImage(getTestDataDir(), "test1.png");

    //comment due to test failed expectation
    //testProfile(image);
    //testOrderStatisticProfile(image);

    testReverseProfile();

   // testGetSmoothedProfile();

    // Test "ring profile".
    testRingProfile();

    // uncomment to perform timing tests
    //testTiming();
    
    //comment due to test failed expectation
    //testCircleProfile();
    //testCircleProfile2();


    //testFindMaxAbsoluteValue();
    testCalcWeightedAverageIndex();
    testFindHorizontalEdge();
    testFindVerticalEdge();
    testFindSystemFiducial();

    //Lim Lay Ngor add
    testCalculateSharpness();
    testCalculateSharpness(image);
    //comment because the function not for unit test
    //testCalculateSharpness_ImageSet();
    
     image.decrementReferenceCount();
  }

  /**
   * Compare two arrays of floats.  Do a fuzzy equals on each element in the
   * array to allow for floating point math imprecision.
   *
   * @author Greg Loring
   */
  private boolean fuzzyEqual(float[] array1, float[] array2)
  {
    return fuzzyEqual(array1, array2, 0.001f); // this test fails on my Core Duo processor when precision required is 0.0001.  -PE
  }

  /**
   * Compare two arrays of floats.  Do a fuzzy equals on each element in the
   * array to allow for floating point math imprecision.
   *
   * @author Peter Esbensen
   * @author Greg Loring (parameterized the tolerance)
   */
  private boolean fuzzyEqual(float[] array1, float[] array2, float tolerance)
  {
    Assert.expect(array1 != null);
    Assert.expect(array2 != null);
    if (array1.length != array2.length)
      return false;

    for (int i = 0; i < array1.length ; ++i)
    {
      if (Math.abs(array1[i] - array2[i]) > tolerance)
      {
        System.out.println(array1[i] - array2[i]);
        return false;
      }
    }
    return true;
  }

  /**
   * @author Peter Esbensen
   */
  private void testProfile(Image image)
  {
    Assert.expect(image != null);

    // test illegal regions of interest
    Collection<RegionOfInterest> regions = ImageUnitTestUtil.generateBadRegionsOfInterest(image);
    for (RegionOfInterest regionOfInterest : regions)
    {
      try
      {
        ImageFeatureExtraction.profile(image, regionOfInterest);
        Expect.expect( false ); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
    }

    /* test1.png is an image that looks like:

      0    0    0   127 127 127
      0    0    0   127 127 127
      0    0    0   127 127 127
      0    0    0   255 255 255
      0    0    0   255 255 255
      0    0    0   255 255 255
    */

     RegionOfInterest roi = RegionOfInterest.createRegionFromImage(image);
     float[] profileResult = ImageFeatureExtraction.profile(image, roi);
     float[] expectedResult = new float[] { 0.0f, 0.0f, 0.0f, 191.0f, 191.0f, 191.0f };
     Expect.expect( fuzzyEqual(profileResult, expectedResult) );

     roi.setOrientationInDegrees(90);
     profileResult = ImageFeatureExtraction.profile(image, roi);
     expectedResult = new float[] { 127.5f, 127.5f, 127.5f, 63.5f, 63.5f, 63.5f };
     Expect.expect( fuzzyEqual(profileResult, expectedResult) );

     roi.setOrientationInDegrees(180);
     profileResult = ImageFeatureExtraction.profile(image, roi);
     expectedResult = new float[] { 191.0f, 191.0f, 191.0f, 0.0f, 0.0f, 0.0f };
     Expect.expect( fuzzyEqual(profileResult, expectedResult) );

     roi.setOrientationInDegrees(270);
     profileResult = ImageFeatureExtraction.profile(image, roi);
     expectedResult = new float[] { 63.5f, 63.5f, 63.5f, 127.5f, 127.5f, 127.5f };
     Expect.expect( fuzzyEqual(profileResult, expectedResult) );

     roi = new RegionOfInterest( 0, 0, image.getWidth(), 3, 0, RegionShapeEnum.RECTANGULAR );
     profileResult = ImageFeatureExtraction.profile(image, roi);
     expectedResult = new float[] { 0.0f, 0.0f, 0.0f, 127.0f, 127.0f, 127.0f };
     Expect.expect( fuzzyEqual(profileResult, expectedResult ) );

     roi = new RegionOfInterest( 0, 0, image.getWidth(), 3, 90, RegionShapeEnum.RECTANGULAR );
     profileResult = ImageFeatureExtraction.profile(image, roi);
     expectedResult = new float[] { 127.0f/2.f, 127.0f/2.f, 127.0f/2.f };
     Expect.expect( fuzzyEqual(profileResult, expectedResult ) );

     roi = new RegionOfInterest( 0, 4, 3,  2, 0, RegionShapeEnum.RECTANGULAR );
     profileResult = ImageFeatureExtraction.profile(image, roi);
     expectedResult = new float[] { 0.0f, 0.0f, 0.0f };
     Expect.expect( fuzzyEqual(profileResult, expectedResult) );
  }

  /**
   * @todo PE use more interesting data
   *
   * @author Peter Esbensen
   */
  private void testOrderStatisticProfile(Image image)
  {
    Assert.expect(image != null);

    // test illegal regions of interest
    Collection<RegionOfInterest> regions = ImageUnitTestUtil.generateBadRegionsOfInterest(image);
    for (RegionOfInterest regionOfInterest : regions)
    {
      try
      {
        ImageFeatureExtraction.profile(image, regionOfInterest);
        Expect.expect( false ); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
    }

    /* test1.png is an image that looks like:

      0    0    0   127 127 127
      0    0    0   127 127 127
      0    0    0   127 127 127
      0    0    0   255 255 255
      0    0    0   255 255 255
      0    0    0   255 255 255
    */

     float orderStatistic = 0.25f;

     RegionOfInterest roi = RegionOfInterest.createRegionFromImage(image);
     float[] profileResult = ImageFeatureExtraction.orderStatisticProfile(image, roi, orderStatistic);
     float[] expectedResult = new float[] { 0.0f, 0.0f, 0.0f, 127.0f, 127.0f, 127.0f };
     Expect.expect( fuzzyEqual(profileResult, expectedResult) );

     roi.setOrientationInDegrees(90);
     profileResult = ImageFeatureExtraction.orderStatisticProfile(image, roi, orderStatistic);
     expectedResult = new float[] { 0f, 0f, 0f, 0f, 0f, 0f };
     Expect.expect( fuzzyEqual(profileResult, expectedResult) );

     roi.setOrientationInDegrees(180);
     profileResult = ImageFeatureExtraction.orderStatisticProfile(image, roi, orderStatistic);
     expectedResult = new float[] { 127.0f, 127.0f, 127.0f, 0f, 0f, 0f };
     Expect.expect( fuzzyEqual(profileResult, expectedResult) );

     roi.setOrientationInDegrees(270);
     profileResult = ImageFeatureExtraction.orderStatisticProfile(image, roi, orderStatistic);
     expectedResult = new float[] {0f, 0f, 0f, 0f, 0f, 0f};
     Expect.expect( fuzzyEqual(profileResult, expectedResult) );
  }


  /**
   * @author Peter Esbensen
   */
  private void testCircleProfile()
  {
    final int RADIUS_OF_CIRCLE = 2;
    final int DIAMETER_OF_CIRCLE = RADIUS_OF_CIRCLE * 2;

    final int TEST_IMAGE_WIDTH = 8;
    final int TEST_IMAGE_HEIGHT = 8;

    // The image should be a black object on a white background.  The test image is an 8x8 square with a circle of
    // radius 2 in the middle of the square.

    BufferedImage testBufferedImage = new BufferedImage(TEST_IMAGE_WIDTH, TEST_IMAGE_HEIGHT,
                                                        BufferedImage.TYPE_BYTE_GRAY);
    Graphics2D testImageG2d = testBufferedImage.createGraphics();
    testImageG2d.setColor(java.awt.Color.WHITE);
    testImageG2d.fillRect(0, 0, testBufferedImage.getWidth(), testBufferedImage.getHeight());
    Shape circle = new Ellipse2D.Double(2, 2, DIAMETER_OF_CIRCLE, DIAMETER_OF_CIRCLE);
    testImageG2d.setColor(Color.BLACK);
    testImageG2d.fill(circle);
    Image testImage = Image.createFloatImageFromBufferedImage(testBufferedImage);

    // Test forward profile
    // The ROI is (diameter + 2) by (diameter) box that starts at (radius - 1) pixels to the left the circle.
    RegionOfInterest regionOfInterest = new RegionOfInterest(1, 2, 6, 4, 0, RegionShapeEnum.OBROUND);
    float[] testProfile = ImageFeatureExtraction.circleProfile(testImage, regionOfInterest);
    Expect.expect(testProfile.length == regionOfInterest.getWidth());
    float[] expectedProfile = new float[] { 255f, 153f, 102f, 153f, 204f, 255f};
    Expect.expect( fuzzyEqual(expectedProfile, testProfile) == true );

    // Test reverse profile
    // The ROI is (diameter + 2) by (diameter) box that starts at (radius - 1) pixels to the left the circle.
    regionOfInterest = new RegionOfInterest(1, 2, 6, 4, 180, RegionShapeEnum.OBROUND);
    testProfile = ImageFeatureExtraction.circleProfile(testImage, regionOfInterest);
    Expect.expect(testProfile.length == regionOfInterest.getWidth());
    expectedProfile = new float[] { 255f, 153f, 51f, 102f, 204f, 255f};
    Expect.expect( fuzzyEqual(expectedProfile, testProfile) == true );

    // Test downwards profile
    // The ROI is (diameter) by (diameter+2) box that starts at (radius - 1) pixels above the circle.
    regionOfInterest = new RegionOfInterest(2, 1, 4, 6, 90, RegionShapeEnum.OBROUND);
    testProfile = ImageFeatureExtraction.circleProfile(testImage, regionOfInterest);
    Expect.expect(testProfile.length == regionOfInterest.getHeight());
    expectedProfile = new float[] { 255f, 255f, 170f, 85f, 255f, 255f };
    Expect.expect( fuzzyEqual(expectedProfile, testProfile) == true );

    // Test upwards profile
    // The ROI is (diameter) by (diameter+2) box that starts at (radius - 1) pixels above the circle.
    regionOfInterest = new RegionOfInterest(2, 1, 4, 6, 270, RegionShapeEnum.OBROUND);
    testProfile = ImageFeatureExtraction.circleProfile(testImage, regionOfInterest);
    Expect.expect(testProfile.length == regionOfInterest.getHeight());
    expectedProfile = new float[] { 255f, 85f, 85f, 170f, 255f, 255f };
    Expect.expect( fuzzyEqual(expectedProfile, testProfile) == true );

    // test illegal regions of interest
    Collection<RegionOfInterest> regions = ImageUnitTestUtil.generateBadRegionsOfInterest(testImage);
    for (RegionOfInterest roi : regions)
    {
      try
      {
        ImageFeatureExtraction.circleProfile(testImage, roi);
        Expect.expect( false ); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
    }
    testImage.decrementReferenceCount();
  }

  /**
   * This method prints out a profile to standard output.  This output can then be cut and paste into this file
   * as the expected results of the testCircleProfile2() tests.
   *
   * @author Sunit Bhalla
   */
  private void printProfile(float[] testProfile)
  {
    final int NUMBERS_PER_LINE = 7;
    for (int i=0; i < testProfile.length; i++)
    {
      System.out.print(testProfile[i] + "f" + ", ");
      if ((i % NUMBERS_PER_LINE) == (NUMBERS_PER_LINE - 1))
        System.out.println();
    }
  }


  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */
  private void testCircleProfile2()
  {

    final int RADIUS_OF_CIRCLE = 30;
    final int DIAMETER_OF_CIRCLE = RADIUS_OF_CIRCLE * 2;

    final int pitch = 100;

    final int TEST_IMAGE_WIDTH = 200;
    final int TEST_IMAGE_HEIGHT = 200;

    final int minXOfCircle = 60;
    final int minYOfCircle = 60;

    // The image should be a black object on a white background.
    BufferedImage testBufferedImage = new BufferedImage(TEST_IMAGE_WIDTH, TEST_IMAGE_HEIGHT,
        BufferedImage.TYPE_BYTE_GRAY);
    Graphics2D testImageG2d = testBufferedImage.createGraphics();
    testImageG2d.setColor(Color.WHITE);
    testImageG2d.fillRect(0, 0, testBufferedImage.getWidth(), testBufferedImage.getHeight());
    Shape circle = new Ellipse2D.Double(minXOfCircle, minYOfCircle, DIAMETER_OF_CIRCLE, DIAMETER_OF_CIRCLE);
    testImageG2d.setColor(Color.BLACK);
    testImageG2d.fill(circle);

    Image testImage = Image.createFloatImageFromBufferedImage(testBufferedImage);

    // Test forward profile
    // The ROI is (IPD) by (diameter) box that starts at (IPD/2 - radius) pixels to the left the circle.
    // Assuming a 30 radius circle that starts at (60, 60) and an IPD of 100: the ROI will be 100 x 60 that
    // starts at (40, 60).
    int minXOfROI = minXOfCircle - (pitch/2 - RADIUS_OF_CIRCLE);
    int minYOfROI = minYOfCircle;
    RegionOfInterest regionOfInterest = new RegionOfInterest(minXOfROI, minYOfROI,
        pitch, DIAMETER_OF_CIRCLE, 0, RegionShapeEnum.OBROUND);
    float[] testProfile = ImageFeatureExtraction.circleProfile(testImage, regionOfInterest);

    // Use for debugging only
    // testImage.printImageWithProfile("forwardProfile", testProfile, minXOfCircle - (IPD/2 - RADIUS_OF_CIRCLE));

    Expect.expect(testProfile.length == regionOfInterest.getWidth());

    float[] expectedProfile = new float[] {
                              255.0001f, 255.0001f, 255.0001f, 255.0001f, 255.0001f, 255.0001f, 255.0001f,
                              255.0001f, 255.0001f, 255.0001f, 255.0001f, 255.0001f, 255.0001f, 255.0001f,
                              255.0001f, 255.0001f, 255.0001f, 255.0001f, 251.64485f, 244.93433f, 201.31587f,
                              73.81579f, 33.552635f, 23.486843f, 13.421053f, 13.421053f, 16.776318f, 20.13158f,
                              26.842106f, 33.55263f, 36.907894f, 40.263157f, 43.61842f, 43.61842f, 43.61842f,
                              46.973682f, 50.32895f, 50.32895f, 53.68421f, 57.039474f, 57.039474f, 57.039474f,
                              60.394737f, 63.75f, 67.10526f, 70.460526f, 73.81579f, 80.52631f, 83.88158f,
                              83.88158f, 90.5921f, 97.30263f, 97.30263f, 100.65789f, 104.01315f, 104.01315f,
                              110.72368f, 117.434204f, 117.434204f, 120.78947f, 127.49999f, 130.85526f, 130.85526f,
                              137.56578f, 137.56578f, 147.63158f, 150.98683f, 157.69736f, 164.40788f, 167.76315f,
                              171.11841f, 177.82893f, 184.53946f, 187.89473f, 194.60526f, 197.96053f, 211.38159f,
                              221.4474f, 224.80267f, 251.64484f, 255.0001f, 255.0001f, 255.0001f, 255.0001f,
                              255.0001f, 255.0001f, 255.0001f, 255.0001f, 255.0001f, 255.0001f, 255.0001f,
                              255.0001f, 255.0001f, 255.0001f, 255.0001f, 255.0001f, 255.0001f, 255.0001f,
                              255.0001f, 255.0001f };

    // Use this to generate new expected results (if needed)
    // printProfile(testProfile);

    Expect.expect( fuzzyEqual(expectedProfile, testProfile) == true );



    // Test reverse profile
    // The ROI is (IPD) by (diameter) box that starts at (IPD/2 - radius) pixels to the left the circle.
    // Assuming a 30 radius circle that starts at (60, 60) and an IPD of 100: the ROI will be 100 x 60 that
    // starts at (40, 60).
    minXOfROI = minXOfCircle - (pitch/2 - RADIUS_OF_CIRCLE);
    minYOfROI = minYOfCircle;
    regionOfInterest = new RegionOfInterest(minXOfROI, minYOfROI,
                                            pitch, DIAMETER_OF_CIRCLE, 180, RegionShapeEnum.OBROUND);
    testProfile = ImageFeatureExtraction.circleProfile(testImage, regionOfInterest);
    Expect.expect(testProfile.length == regionOfInterest.getWidth());
    expectedProfile = new float[] { 255.00012f, 255.00012f, 255.00012f, 255.00012f, 255.00012f, 255.00012f, 255.00012f,
                      255.00012f, 255.00012f, 255.00012f, 255.00012f, 255.00012f, 255.00012f, 255.00012f,
                      255.00012f, 255.00012f, 255.00012f, 251.92784f, 245.78326f, 233.49411f, 138.25299f,
                      33.79518f, 27.6506f, 18.433733f, 12.289156f, 15.361445f, 18.433735f, 21.506023f,
                      27.6506f, 33.795177f, 36.867466f, 39.939754f, 39.939754f, 39.939754f, 39.939754f,
                      43.012043f, 46.084335f, 49.156624f, 52.228912f, 52.228912f, 52.228912f, 52.228912f,
                      55.3012f, 61.445778f, 64.51807f, 64.51807f, 70.66264f, 76.80722f, 76.80722f,
                      79.87951f, 86.024086f, 89.096375f, 89.096375f, 95.24095f, 95.24095f, 98.31324f,
                      104.45782f, 107.530106f, 107.530106f, 113.67468f, 119.81926f, 119.81926f, 122.89155f,
                      125.96384f, 129.03613f, 138.25299f, 138.25299f, 147.46986f, 150.54214f, 156.68672f,
                      156.68672f, 165.9036f, 168.97588f, 175.12045f, 181.26505f, 187.40962f, 193.5542f,
                      205.84338f, 218.13257f, 248.85553f, 255.00012f, 255.00012f, 255.00012f, 255.00012f,
                      255.00012f, 255.00012f, 255.00012f, 255.00012f, 255.00012f, 255.00012f, 255.00012f,
                      255.00012f, 255.00012f, 255.00012f, 255.00012f, 255.00012f, 255.00012f, 255.00012f,
                      255.00012f, 255.00012f
    } ;


    // Use this to generate new expected results (if needed)
    // printProfile(testProfile);

    Expect.expect( fuzzyEqual(expectedProfile, testProfile) == true );

    // Test downwards profile
    // The ROI is (diameter) by (IPD) box that starts at (IPD/2 - radius) pixels above the circle.
    // Assuming a 30 radius circle that starts at (60, 60) and an IPD of 100: the ROI will be 100 x 60 that
    // starts at (40, 60).
    minXOfROI = minXOfCircle;
    minYOfROI = minYOfCircle - (pitch/2 - RADIUS_OF_CIRCLE);
    regionOfInterest = new RegionOfInterest(minXOfROI, minYOfROI,
                                            DIAMETER_OF_CIRCLE, pitch, 90, RegionShapeEnum.OBROUND);
    testProfile = ImageFeatureExtraction.circleProfile(testImage, regionOfInterest);


    Expect.expect(testProfile.length == regionOfInterest.getHeight());

    expectedProfile = new float[] {
                      255.00002f, 255.00002f, 255.00002f, 255.00002f, 255.00002f, 255.00002f, 255.00002f,
                      255.00002f, 255.00002f, 255.00002f, 255.00002f, 255.00002f, 255.00002f, 255.00002f,
                      255.00002f, 255.00002f, 255.00002f, 255.00002f, 251.64474f, 244.93422f, 201.3158f,
                      70.460526f, 30.197369f, 23.486843f, 13.421053f, 10.06579f, 13.421053f, 16.776316f,
                      23.486841f, 30.197369f, 33.55263f, 36.907894f, 40.263157f, 40.263157f, 40.263157f,
                      43.61842f, 46.973682f, 46.973682f, 50.328945f, 53.684208f, 53.684208f, 53.684208f,
                      57.03947f, 60.394733f, 63.749996f, 67.10526f, 70.460526f, 77.17105f, 80.52631f,
                      80.52631f, 87.23684f, 93.947365f, 93.947365f, 97.30263f, 100.65789f, 100.65789f,
                      107.368416f, 114.07894f, 114.07894f, 117.434204f, 124.14473f, 127.49999f, 127.49999f,
                      134.21053f, 134.21053f, 144.27634f, 147.6316f, 154.34213f, 161.05266f, 164.40791f,
                      167.76318f, 174.47371f, 181.18423f, 184.53949f, 191.25002f, 194.60529f, 208.02634f,
                      218.09212f, 221.44739f, 251.64474f, 255.00002f, 255.00002f, 255.00002f, 255.00002f,
                      255.00002f, 255.00002f, 255.00002f, 255.00002f, 255.00002f, 255.00002f, 255.00002f,
                      255.00002f, 255.00002f, 255.00002f, 255.00002f, 255.00002f, 255.00002f, 255.00002f,
                      255.00002f, 255.00002f, };

    // Use this to generate new expected results (if needed)
    // printProfile(testProfile);
    Expect.expect( fuzzyEqual(expectedProfile, testProfile) == true );


    // Test upwards profile
    // The ROI is (diameter) by (IPD) box that starts at (IPD/2 - radius) pixels above the circle.
    // Assuming a 30 radius circle that starts at (60, 60) and an IPD of 100: the ROI will be 100 x 60 that
    // starts at (40, 60).
    minXOfROI = minXOfCircle;
    minYOfROI = minYOfCircle - (pitch/2 - RADIUS_OF_CIRCLE);
    regionOfInterest = new RegionOfInterest(minXOfROI, minYOfROI,
                                            DIAMETER_OF_CIRCLE, pitch, 270, RegionShapeEnum.OBROUND);
    testProfile = ImageFeatureExtraction.circleProfile(testImage, regionOfInterest);
    Expect.expect(testProfile.length == regionOfInterest.getHeight());


    expectedProfile = new float[] {
                      254.99995f, 254.99995f, 254.99995f, 254.99995f, 254.99995f, 254.99995f, 254.99995f,
                      254.99995f, 254.99995f, 254.99995f, 254.99995f, 254.99995f, 254.99995f, 254.99995f,
                      254.99995f, 254.99995f, 254.99995f, 251.92767f, 245.7831f, 233.49394f, 138.25299f,
                      33.795177f, 27.6506f, 18.433735f, 12.289156f, 15.361444f, 18.433733f, 21.506023f,
                      27.6506f, 33.795177f, 36.867466f, 39.939754f, 39.939754f, 39.939754f, 39.939754f,
                      43.012043f, 46.08433f, 49.15662f, 52.22891f, 52.22891f, 52.22891f, 52.22891f,
                      55.301197f, 61.445774f, 64.51807f, 64.51807f, 70.66264f, 76.80722f, 76.80722f,
                      79.87951f, 86.024086f, 89.096375f, 89.096375f, 95.24095f, 95.24095f, 98.31324f,
                      104.45782f, 107.530106f, 107.530106f, 113.67468f, 119.81926f, 119.81926f, 122.89155f,
                      125.96384f, 129.03613f, 138.25299f, 138.25299f, 147.46986f, 150.54214f, 156.68672f,
                      156.68672f, 165.9036f, 168.97588f, 175.12045f, 181.26503f, 187.4096f, 193.55418f,
                      205.84334f, 218.13249f, 248.85538f, 254.99995f, 254.99995f, 254.99995f, 254.99995f,
                      254.99995f, 254.99995f, 254.99995f, 254.99995f, 254.99995f, 254.99995f, 254.99995f,
                      254.99995f, 254.99995f, 254.99995f, 254.99995f, 254.99995f, 254.99995f, 254.99995f,
                      254.99995f, 254.99995f };

    // Use this to generate new expected results (if needed)
    // printProfile(testProfile);

    // some of the laptops inflicted on contractors can't hack the normal tolerance...
    Expect.expect( fuzzyEqual(expectedProfile, testProfile, 2.0e-4f) == true );

    testImage.decrementReferenceCount();
  }

  /**
   * @todo Move to Test_ProfileUtil
   * @author Peter Esbensen
   */
  private void testGetSmoothedProfile()
  {
    float[] profile = { 1, 2, 3, 4, 5 };
    int smoothingKernelLength = 3;
    float[] testProfile = ProfileUtil.getSmoothedProfile(profile,
        smoothingKernelLength);
    float[] expectedProfile = { 4f/3f, 2f, 3f, 4f, 14f/3f };
    Expect.expect( fuzzyEqual( testProfile, expectedProfile ) );

    smoothingKernelLength = 4;
    testProfile = ProfileUtil.getSmoothedProfile(profile,
        smoothingKernelLength);
    expectedProfile = new float[] { 5f/4f, 7f/4f, 2.5f, 3.5f, 17f/4f };
    Expect.expect( fuzzyEqual( testProfile, expectedProfile ) );

    // test bad inputs
    float[] nullProfile = null;
    try
    {
      ProfileUtil.getSmoothedProfile(nullProfile, 3);
    }
    catch (AssertException ae)
    {
      // do nothing, this is expected
    }
    try
    {
      ProfileUtil.getSmoothedProfile(profile, 0);
    }
    catch (AssertException ae)
    {
      // do nothing, this is expected
    }
  }

  /**
   * @todo : Move to Test_ProfileUtil
   * @author Peter Esbensen
   */
  private void testReverseProfile()
  {
    float[] profile = new float[] { 1, 2, 3, 4 };
    float[] expectedProfile = new float[] { 4, 3, 2, 1 };
    ProfileUtil.reverseProfile(profile);
    Expect.expect( fuzzyEqual(profile, expectedProfile) );
    profile = new float[] { 1, 2, 3, 4, 5 };
    expectedProfile = new float[] { 5, 4, 3, 2, 1 };
    ProfileUtil.reverseProfile(profile);
    Expect.expect( fuzzyEqual(profile, expectedProfile) );
  }

  /**
   * Implemented only to compare techniques of implementing the algorithm. Kept for future reference.
   * @author Patrick Lacz
   */
  private void testTiming()
  {
    // TODO: testimage_png1.png is not by default in the autotest/data directory, it is in the autotest/test dir
    // Should it be moved there?
    Image image = loadTestImage(getTestDataDir(), "../test/testimage_png1.png");

    Assert.expect(image != null);

    System.out.println("Image Dimensions: " + image.getWidth() + " x " + image.getHeight());

    RegionOfInterest horizRoi = RegionOfInterest.createRegionFromRegionBorder(0, 0, image.getWidth()-1, image.getHeight()-1,
        0,RegionShapeEnum.RECTANGULAR);
    RegionOfInterest vertRoi = RegionOfInterest.createRegionFromRegionBorder(0, 0, image.getWidth()-1, image.getHeight()-1,
        90,RegionShapeEnum.RECTANGULAR);

    int tests = 100;

    long nanoStart = System.nanoTime();
    for (int i = 0 ; i < tests ; ++i)
    {
      float res[] = ImageFeatureExtraction.profile(image, horizRoi);
    }
    long nanoEnd = System.nanoTime();

    double duration = (nanoEnd - nanoStart)/tests;
    System.out.println("Horizontal invokation took on average " + duration/100000.0 + " milliseconds.");

    nanoStart = System.nanoTime();
    for (int i = 0 ; i < tests ; ++i)
    {
      float res[] = ImageFeatureExtraction.profile(image, vertRoi);
    }
    nanoEnd = System.nanoTime();
    duration = (nanoEnd - nanoStart)/tests;
    System.out.println("Vertical invokation took on average " + duration/100000.0 + " milliseconds.");
    image.decrementReferenceCount();
  }

  /**
   * Tests "ring profile".
   *
   * @author Matt Wharton
   */
  private void testRingProfile()
  {
    // Load up a test image from disk.
    Image ringProfileImage = loadTestImage(getTestDataDir(), "ringprofile1.png");

    RegionOfInterest outerRoi = new RegionOfInterest(10, 10, 80, 80, 0, RegionShapeEnum.OBROUND);
    RegionOfInterest innerRoi = new RegionOfInterest(30, 30, 40, 40, 0, RegionShapeEnum.OBROUND);

    float[] expectedRingProfile = new float[188];
    expectedRingProfile[0] = 0f;
    expectedRingProfile[1] = 0f;
    expectedRingProfile[2] = 216.75f;
    Arrays.fill(expectedRingProfile, 3, 92, 255f);
    expectedRingProfile[92] = 127.5f;
    Arrays.fill(expectedRingProfile, 93, expectedRingProfile.length, 0f);
    float[] ringProfileOptimized = ImageFeatureExtraction.ringProfile(ringProfileImage, outerRoi, innerRoi);

    Expect.expect(fuzzyEqual(expectedRingProfile, ringProfileOptimized), "Ring profiles don't match!");
    ringProfileImage.decrementReferenceCount();
  }

  /**
   * @author Peter Esbensen
   */
  private Image loadTestImage(String directoryPath, String filename)
  {
    Assert.expect(directoryPath != null);
    Assert.expect(directoryPath.length() > 0);
    Assert.expect(filename != null);
    Assert.expect(filename.length() > 0);

    Image resultImage = null;
    try
    {
      resultImage = ImageIoUtil.loadPngImage(directoryPath + File.separator + filename);
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "Exception occurred during PNG Image Load: " + exc.getMessage());
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(false, "FileDoesNotExistException:" + exc.getMessage());
    }
    Assert.expect(resultImage != null);
    return resultImage;
  }

  /**
   * @todo Move to Test_ProfileUtil
   * @author Eddie Williamson
   */
  private void testFindMaxAbsoluteValue()
  {
    float[] profile1 = {1, 2, 3, 4, 5};
    float index = ProfileUtil.findMaxAbsoluteValue(profile1);
    Expect.expect(index == 4);

    float[] profile2 = {1, 2, -43, 4, 5};
    index = ProfileUtil.findMaxAbsoluteValue(profile2);
    Expect.expect(index == 2);


    // test bad inputs
    float[] nullProfile = null;
    try
    {
      ProfileUtil.findMaxAbsoluteValue(nullProfile);
    }
    catch (AssertException ae)
    {
      // do nothing, this is expected
    }
  }


  /**
   * @todo Move to Test_ProfileUtil
   */
  private void testCalcWeightedAverageIndex()
  {
    float[] profile1 = {0, 0.22F, 2, 4.8F, 4.5F, 4.6F, 4.7F, 1.8F, 0, 0};
    float avg1 = ProfileUtil.calcWeightedAverageIndex(profile1, 3, 4);
    float avg2 = ProfileUtil.calcWeightedAverageIndex(profile1, 4, 4);
    float avg3 = ProfileUtil.calcWeightedAverageIndex(profile1, 5, 4);
    float avg4 = ProfileUtil.calcWeightedAverageIndex(profile1, 6, 4);

    Expect.expect(avg1 > 4.4F);
    Expect.expect(avg1 < 4.5F);
    Expect.expect( (avg1-avg2) < 0.1F );
    Expect.expect( (avg1-avg3) < 0.1F );
    Expect.expect( (avg1-avg4) < 0.1F );
  }


  private void testFindHorizontalEdge()
  {
    Image image = null;
    try
    {
      // load image containing a known horizontal edge
      image = ImageIoUtil.loadJpegImage(getTestDataDir() + File.separator + "test2.jpg");
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "Exception occurred during JPG Image Load: " + exc.getMessage());
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(false, "FileDoesNotExistException:" + exc.getMessage());
    }
    Assert.expect(image != null);

    float edge = ImageFeatureExtraction.findHorizontalEdge(image);
    Expect.expect(MathUtil.fuzzyEquals(edge, 24.11622F));
    if (image != null)
      image.decrementReferenceCount();
  }


  private void testFindVerticalEdge()
  {
    Image image = null;
    try
    {
      // load image containing a known vertical edge
      image = ImageIoUtil.loadJpegImage(getTestDataDir() + File.separator + "test2.jpg");
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "Exception occurred during JPG Image Load: " + exc.getMessage());
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(false, "FileDoesNotExistException:" + exc.getMessage());
    }
    Assert.expect(image != null);

    float edge = ImageFeatureExtraction.findVerticalEdge(image);
    Expect.expect(MathUtil.fuzzyEquals(edge, 99.08514F));
    if (image != null)
      image.decrementReferenceCount();

  }


  private void testFindSystemFiducial()
  {
    Image image = null;
    try
    {
      // load image containing a known fiducial corner
      image = ImageIoUtil.loadJpegImage(getTestDataDir() + File.separator + "test2.jpg");
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "Exception occurred during JPG Image Load: " + exc.getMessage());
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(false, "FileDoesNotExistException:" + exc.getMessage());
    }
    Assert.expect(image != null);

    DoubleCoordinate coordinate = ImageFeatureExtraction.findSystemFiducial(image);
    Expect.expect(MathUtil.fuzzyEquals(coordinate.getX(), 99.08514F));
    Expect.expect(MathUtil.fuzzyEquals(coordinate.getY(), 24.11622F));
    image.decrementReferenceCount();
  }
  
    /**
   * Code to test JNI function through VxImageAnalyser: calculateSharpnessProfileExecute() 
   * The image pass in is a very sharp edge images which not so suitable for the unit test here.
   * Image = "C:\\vstore\\v810\\5.4_Dev\\axi\\java\\com\\axi\\util\\image\\autotest\\data\\test1.png"
   * @author Lim Lay Ngor
   */
  private void testCalculateSharpness(Image image)
  {
    //System.out.println("start testCalculateSharpness:");
    
    Assert.expect(image != null);
    //System.out.println("input file: " + image);

    final double value = ImageFeatureExtraction.calculateSharpnessProfileExecute(image);
    //System.out.println("ImageFeatureExtraction.calculateSharpnessProfileExecute(image): " + value);
    
    //Image = "C:\\vstore\\v810\\5.4_Dev\\axi\\java\\com\\axi\\util\\image\\autotest\\data\\test1.png"
    //A very sharp edge images.
    final double expectedResult = 2099.773958333333;
    //System.out.println("expectedResult: " + expectedResult);
    Expect.expect(MathUtil.fuzzyEquals(ImageFeatureExtraction.calculateSharpnessProfileExecute(image), expectedResult));
    //System.out.println("end testCalculateSharpness:");
  }
  
  /**
   * Code to test JNI function through VxImageAnalyser: calculateSharpnessProfileExecute() 
   * The image loaded inside is more suitable for the unit test.
   * @author Lim Lay Ngor
   */
  private void testCalculateSharpness()
  { 
    Image image = null;
    try
    {
      // load image containing a known sharpness value    
      //Image = "C:\\vstore\\v810\\5.4_Dev\\axi\\java\\com\\axi\\util\\image\\autotest\\data\\test2.jpg"
      image = ImageIoUtil.loadJpegImage(getTestDataDir() + File.separator + "test2.jpg");
      System.out.println("testCalculateSharpness: input file: test2.jpg");
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "Exception occurred during Tiff Image Load: " + exc.getMessage());
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(false, "FileDoesNotExistException:" + exc.getMessage());
    }
    Assert.expect(image != null);

    final double value = ImageFeatureExtraction.calculateSharpnessProfileExecute(image);        
    final double expectedResult = 1.7479504489910;        
    System.out.println("ExpectedResult: " + value);
    Expect.expect(MathUtil.fuzzyEquals(value, expectedResult));

     if (image != null)
      image.decrementReferenceCount();
  }

  /**
   * Temporary testing code for Calculate Sharpness Value through VxImageAnalyser
   * @author Lim Lay Ngor
   */
  private void testCalculateSharpness_ImageSet()
  {
    System.out.println("testCalculateSharpness_ImageSet:");

   // java.util.List<Double> sharpnessList = new ArrayList();
    Image image = null;

    final File folder = new File("C:\\Program Files\\AXI System\\v810\\recipes\\panel\\images\\virtualLive\\2012-04-13_11-53-51-285");
	int a = 0;
    try
    {
      for (final File fileEntry : folder.listFiles())
      {
        if (fileEntry.isDirectory())
        {
          //do nothing
        }
        else
        {
          //System.out.println(fileEntry.getName());
          if(a >404)
          {
            System.out.println("over");
          }
          if (fileEntry.getName().endsWith(".jpg"))
          {
            image = ImageIoUtil.loadImage(folder + File.separator + fileEntry.getName());
            double sharpness = ImageFeatureExtraction.calculateSharpnessProfileExecute(image);
            //System.out.println("sharpness = " + sharpness);
            //sharpnessList.add(sharpness);
            ++a;
            System.out.println("a = " + a);
            if (image != null)
              image.decrementReferenceCount();
          }
        }
      }
    }
    catch (CouldNotReadFileException exc)
    {
      System.out.println("a = " + a);
      Expect.expect(false, "Exception occurred during Image Load: " + exc.getMessage());
    }
    catch (FileDoesNotExistException exc)
    {
      System.out.println("a = " + a);
      Expect.expect(false, "FileDoesNotExistException:" + exc.getMessage());
    }

    System.out.println("good a = " + a);
  }
}

