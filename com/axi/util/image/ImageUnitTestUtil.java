package com.axi.util.image;

import java.util.*;

import com.axi.util.*;

/**
 * This class provides utility routines for use in Unit Tests of our image
 * processing primitives.
 *
 * @author Peter Esbensen
 */
class ImageUnitTestUtil
{
  /**
   * @author Peter Esbensen
   */
  public ImageUnitTestUtil()
  {
    // do nothing
  }

  /**
   * Generate a Collection of RegionsOfInterest that do not fit within the
   * given image.
   *
   * @author Peter Esbensen
   */
  static Collection<RegionOfInterest> generateBadRegionsOfInterest(Image image)
  {
    Assert.expect(image != null);

    Collection<RegionOfInterest> badRegions = new ArrayList<RegionOfInterest>();

    int imageHeight = image.getHeight();
    int imageWidth = image.getWidth();

    // Create regions that are out of the image
    badRegions.add( new RegionOfInterest( imageWidth, 0, imageWidth, imageHeight, 0, RegionShapeEnum.OBROUND ) );
    badRegions.add( new RegionOfInterest( 0, imageHeight, imageWidth, imageHeight, 0, RegionShapeEnum.OBROUND ) );

    // Create regions that intersect with the image but go beyond it.
    badRegions.add( new RegionOfInterest( -5, 0, imageWidth, imageHeight, 0, RegionShapeEnum.RECTANGULAR ) );
    badRegions.add( new RegionOfInterest( 0, -5, imageWidth, imageHeight, 0, RegionShapeEnum.RECTANGULAR ) );
    badRegions.add( new RegionOfInterest( 5, 0, imageWidth, imageHeight, 0, RegionShapeEnum.RECTANGULAR ) );
    badRegions.add( new RegionOfInterest( 0, 5, imageWidth, imageHeight, 0, RegionShapeEnum.RECTANGULAR ) );

    return badRegions;
  }
}
