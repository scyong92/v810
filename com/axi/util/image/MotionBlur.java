package com.axi.util.image;

import com.axi.util.*;

/**
 * @author Kok Chun, Tan
 */
public class MotionBlur extends ImageEnhancerBase
{
  public int _scale = 60;
  public int _offset = 80;
  public int _iteration = 1;
  public FilterDirectionModeEnum _direction = FilterDirectionModeEnum.Horizontal;

  /**
   * @author Kok Chun, Tan
   */
  public MotionBlur(int scale, int offset, FilterDirectionModeEnum direction, int iteration)
  {
    Assert.expect(scale > 0);
    Assert.expect(offset > 0);
    Assert.expect(direction != null);
    Assert.expect(iteration >= 0);
    
    this._scale = scale;
    this._offset = offset;
    this._direction = direction;
    this._iteration = iteration;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public ImageEnhancerType getType()
  {
    return ImageEnhancerType.MotionBlur;  
  }

  /**
   * @author Kok Chun, Tan
   */
  public Image runEnhancement(Image dest, Image src)
  {
    Assert.expect(dest != null);
    Assert.expect(src != null);
    Assert.expect(_scale > 0);
    Assert.expect(_offset > 0);
    Assert.expect(_iteration >= 0);
    return ImageEnhancer.MotionBlur(dest, src, _scale, _offset, _direction, _iteration);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void updateParameter(int scale, int offset, FilterDirectionModeEnum direction, int iteration)
  {
    Assert.expect(scale > 0);
    Assert.expect(offset > 0);
    Assert.expect(direction != null);
    Assert.expect(iteration >= 0);
    
    this._scale = scale;
    this._offset = offset;
    this._direction = direction;
    this._iteration = iteration;
  }
}
