package com.axi.util.image;

import java.util.*;
import java.nio.ByteBuffer;

import com.axi.util.*;

/**
 * Filter class
 *
 * This class holds functions to perform various filters on Image data.
 *
 * @author Patrick Lacz
 */
public class Filter
{
  /**
   * @author Patrick Lacz
   */
  public Filter()
  {
    // do nothing
    // methods in Filter are staticly defined.
  }

  /**
   * @author Patrick Lacz
   */
  static
  {
    System.loadLibrary("nativeImageUtil");
  }

  /**
   * Create a 1D filter suitable for convolveRow or convolveColumn containing
   * <pre>length</pre> elements all set to the given value.
   *
   * Also known as a box or averaging filter.
   *
   * @author Patrick Lacz
   */
  public static float[] createUniform1DFilterKernel(float value, int length)
  {
    Assert.expect(length > 0);
    float kernel[] = new float[length];
    Arrays.fill(kernel, value);
    return kernel;
  }

  /**
   * Divide each value in the image so that the sum of all the pixels equals 1.
   * This is most frequently used to construct averaging kernels.
   *
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  public static void normalize(Image image)
  {
    Assert.expect(image != null);

    double sum = Statistics.sumOfPixels(image);

    if (MathUtil.fuzzyEquals(sum, 0.0))
      return;

    Arithmetic.multiplyByConstantInPlace(image, 1.f/(float)sum);
  }

  /**
   * Divide each value in the image 255 and multiply with absolute(max-min).
   * if maxVal is less or equal to minVal, maxVal will be set to (minVal + 1).
   * This is to prevent divide by zero error and divide by negative numbers.
   * Added by Seng Yew on 22-Apr-2011
   *
   * @author Lim, Seng Yew
   */
  public static void normalizeWithMinMaxValue(Image image, float minVal, float maxVal)
  {
    Assert.expect(image != null);
    if(MathUtil.fuzzyLessThanOrEquals(maxVal, minVal))
    {
      if(MathUtil.fuzzyEquals(minVal,255.0f))
      {
        maxVal = minVal;
        minVal = maxVal - 0.01f;
      }
      else
        maxVal = minVal + 0.01f;
    }
    Arithmetic.addConstantInPlace(image,-minVal);
    float factor = 255.0f/Math.abs(maxVal-minVal);
    Arithmetic.multiplyByConstantInPlace(image, factor);
    // Make sure all values are within 0.0f and 255.0f
    Threshold.clamp(image, 0.f, 255.0f);
  }

  /**
   * Apply a one dimensional filter along the rows of the given image.
   * The edges in the image are avoided: The returned image size will be the
   * same height, but the width will be reduced by kernel.length-1.
   *
   * @author Patrick Lacz
   */
  public static Image convolveRows(Image source, float kernel[])
  {
    Assert.expect(source != null);
    Assert.expect(kernel != null);
    Assert.expect(kernel.length > 1);

    Assert.expect(source.getBytesPerPixel() == 4); // float format

    int width = source.getWidth() - kernel.length + 1;
    int height = source.getHeight();

    Image dest = new Image(width, height);
    nativeConvolveRows(source.getNativeDefinition(), 0, 0, dest.getNativeDefinition(), kernel);
    return dest;
  }

  /**
   * Apply a one dimensional filter along the rows of the given image.
   * This variation includes an explicit region of interest.
   *
   * The returned Image has the same dimensions as the given RegionOfInterest.
   * This function has no explicit edge-handling, so roi should be sized so that the
   * edges are avoided.
   * todo: Add edge handling.
   * @author Patrick Lacz
   */
  public static Image convolveRows(Image source, RegionOfInterest roi, float kernel[])
  {
    Assert.expect(source != null);
    Assert.expect(roi != null);
    Assert.expect(roi.getMaxX()+kernel.length-1 < source.getWidth());
    Assert.expect(roi.fitsWithinImage(source));

    Image dest = new Image(roi.getWidth(), roi.getHeight());
    nativeConvolveRows(source.getNativeDefinition(), roi.getMinX(), roi.getMinY(), dest.getNativeDefinition(), kernel);
    return dest;
  }

  /**
   * Apply a one dimensional uniform filter along the rows of the given image.
   * This variation includes an explicit region of interest.
   *
   * This method will attempt to use a common optimization for uniform kernels, what we call 'marching convolution'.
   *
   * @author Patrick Lacz
   */
  public static Image convolveRowsUniformKernel(Image sourceImage,
                                                RegionOfInterest roi,
                                                int kernelLength,
                                                float kernelValue,
                                                int anchor,
                                                BorderSamplingModeEnum borderSamplingMode)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(sourceImage != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(sourceImage));
    Assert.expect(anchor < kernelLength);
    Assert.expect(borderSamplingMode != null);
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format

    Pair<Image, RegionOfInterest> convolutionSource = getImageAndRegionForConvolution(
      sourceImage, roi, kernelLength, 1, new ImageCoordinate(anchor, 0), borderSamplingMode);

    Image imageToConvolve = convolutionSource.getFirst();
    RegionOfInterest roiToUse = convolutionSource.getSecond();

    Image destImage = new Image(roiToUse.getWidth() - kernelLength + 1, roiToUse.getHeight());
    nativeMarchingConvolveUniform1DKernel(imageToConvolve.getNativeDefinition(), roiToUse.getMinX(), roiToUse.getMinY(),
                                          destImage.getNativeDefinition(), roi.getWidth(), roi.getHeight(), kernelLength, kernelValue, 0);
    imageToConvolve.decrementReferenceCount();
    return destImage;
  }

  /**
   * Apply a one dimensional uniform filter along the columns of the given image.
   * This variation includes an explicit region of interest.
   *
   * This method will attempt to use a common optimization for uniform kernels, what we call 'marching convolution'.
   *
   * @author Patrick Lacz
   */
  public static Image convolveColumnsUniformKernel(Image sourceImage,
                                                RegionOfInterest roi,
                                                int kernelLength,
                                                float kernelValue,
                                                int anchor,
                                                BorderSamplingModeEnum borderSamplingMode)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(sourceImage != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(sourceImage));
    Assert.expect(anchor < kernelLength);
    Assert.expect(borderSamplingMode != null);
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format

    Pair<Image, RegionOfInterest> convolutionSource = getImageAndRegionForConvolution(
      sourceImage, roi, 1, kernelLength, new ImageCoordinate(0, anchor), borderSamplingMode);

    Image imageToConvolve = convolutionSource.getFirst();
    RegionOfInterest roiToUse = convolutionSource.getSecond();

    Image destImage = new Image(roiToUse.getWidth(), roiToUse.getHeight() - kernelLength + 1);
    nativeMarchingConvolveUniform1DKernel(imageToConvolve.getNativeDefinition(), roiToUse.getMinX(), roiToUse.getMinY(),
                                          destImage.getNativeDefinition(), roi.getWidth(), roi.getHeight(), kernelLength, kernelValue, 1);
    imageToConvolve.decrementReferenceCount();
    return destImage;
  }


  /**
   * Apply a one dimensional filter along the columns of the given image.
   * The edges in the image are avoided: The returned image size will be the
   * same height, but the width will be reduced by kernel.length-1.
   *
   * @author Patrick Lacz
   */
  public static Image convolveColumns(Image source, float kernel[])
  {
    Assert.expect(source != null);
    Assert.expect(kernel != null);
    Assert.expect(kernel.length > 1);
    Assert.expect(source.getBytesPerPixel() == 4); // float format

    int width = source.getWidth();
    int height = source.getHeight()-kernel.length + 1;

    Image dest = new Image(width, height);
    nativeConvolveColumns(source.getNativeDefinition(), 0, 0, dest.getNativeDefinition(), kernel);
    return dest;
  }

  /**
   * Apply a one dimensional filter along the columns of the given image.
   * This variation includes an explicit region of interest.
   *
   * The returned Image has the same dimensions as the given RegionOfInterest.
   * This function has no explicit edge-handling, so roi should be sized so that the
   * edges are avoided.
   * todo: Add edge handling.
   *
   * @author Patrick Lacz
   */
  public static Image convolveColumns(Image source, RegionOfInterest roi, float kernel[])
  {
    Assert.expect(source != null);
    Assert.expect(roi != null);
    Assert.expect(roi.getMaxY()+kernel.length-1 < source.getHeight());
    Assert.expect(roi.fitsWithinImage(source));
    Assert.expect(source.getBytesPerPixel() == 4); // float format

    Image dest = new Image(roi.getWidth(), roi.getHeight());
    nativeConvolveColumns(source.getNativeDefinition(), roi.getMinX(), roi.getMinY(), dest.getNativeDefinition(), kernel);
    return dest;
  }

  /**
   * Used to gather the correct image and region of interest for convolution.
   * The region of interest returned is the region that the kernel touches during
   * execution.  The size of the resulting convolution is region size - kernel size + 1.
   *
   * The Image returned will always be a new image.
   *
   * @todo Determine if clients of this function can always ignore anchor after this point.
   *
   * This method may create a new image to return. You must decrementReferenceCount on the return image.
   *
   * @author Patrick Lacz
   */
  private static Pair<Image, RegionOfInterest> getImageAndRegionForConvolution(Image sourceImage,
                                     RegionOfInterest regionOfInterest,
                                     int kernelWidth, int kernelHeight,
                                     ImageCoordinate anchor,
                                     BorderSamplingModeEnum borderSamplingMode)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(sourceImage));
    Assert.expect(anchor != null);
    Assert.expect(anchor.getX() < kernelWidth);
    Assert.expect(anchor.getY() < kernelHeight);
    Assert.expect(borderSamplingMode != null);
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format

    // Always provide a new copy of the region of interest!
    RegionOfInterest roiToUse = new RegionOfInterest(regionOfInterest);

    if (borderSamplingMode.equals(BorderSamplingModeEnum.CONSTRAIN_TO_REGION))
    {
      Assert.expect(kernelWidth <= regionOfInterest.getWidth());
      Assert.expect(kernelHeight <= regionOfInterest.getHeight());

      sourceImage.incrementReferenceCount();
      return new Pair<Image, RegionOfInterest>(sourceImage, roiToUse);
    }

    // Compute the amount of space that hangs over the edge of the image.
    int borderNorth = Math.max(0, anchor.getY() - regionOfInterest.getMinY());
    int borderSouth = Math.max(0, regionOfInterest.getMaxY() + (kernelHeight - anchor.getY()) - sourceImage.getHeight());
    int borderEast = Math.max(0, regionOfInterest.getMaxX() + (kernelWidth - anchor.getX()) - sourceImage.getWidth());
    int borderWest = Math.max(0, anchor.getX() - regionOfInterest.getMinX());

    Image imageToUse;
    if (borderNorth == 0 && borderSouth == 0 && borderEast == 0 && borderWest == 0)
    {
      imageToUse = new Image(sourceImage);
    } else {
      // if (borderSamplingMode.equals(BorderSamplingModeEnum.REPLICATE_IMAGE_BORDER))
      imageToUse = replicateBorder(sourceImage, sourceImage.getWidth() + borderWest + borderEast,
                                         sourceImage.getHeight() + borderNorth + borderSouth,
                                         anchor.getX(), anchor.getY());
    }
    Assert.expect(imageToUse != null);

    // compensate for the border
    roiToUse.translateXY(borderWest, borderNorth);
    // compensate for the anchor
    roiToUse.translateXY(-anchor.getX(), -anchor.getY());
    // compensate for kernel size
    roiToUse.setWidthKeepingSameMinX(roiToUse.getWidth() + kernelWidth - 1);
    roiToUse.setHeightKeepingSameMinY(roiToUse.getHeight() + kernelHeight - 1);

    return new Pair<Image, RegionOfInterest>(imageToUse, roiToUse);
  }

  /**
   * @author Patrick Lacz
   */
  public static Image convolveKernel(Image sourceImage,
                                     RegionOfInterest regionOfInterest,
                                     Image kernel,
                                     ImageCoordinate anchor,
                                     BorderSamplingModeEnum borderSamplingMode)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(kernel != null);
    Assert.expect(anchor != null);
    Assert.expect(borderSamplingMode != null);
    Assert.expect(regionOfInterest.fitsWithinImage(sourceImage));
    Assert.expect(anchor.isInsideImage(kernel));
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format
    Assert.expect(kernel.getBytesPerPixel() == 4); // float format

    Pair<Image, RegionOfInterest> convolutionSource = getImageAndRegionForConvolution(
      sourceImage, regionOfInterest, kernel.getWidth(), kernel.getHeight(), anchor, borderSamplingMode);

    Image imageToConvolve = convolutionSource.getFirst();
    RegionOfInterest roiToUse = convolutionSource.getSecond();

    Image destImage = new Image(roiToUse.getWidth() - kernel.getWidth() + 1, roiToUse.getHeight() - kernel.getHeight() + 1);
    nativeConvolveKernel(imageToConvolve.getNativeDefinition(), roiToUse.getMinX(), roiToUse.getMinY(), kernel.getNativeDefinition(), destImage.getNativeDefinition());
    imageToConvolve.decrementReferenceCount();

    return destImage;
  }

  /**
   * Convolve with the Sobel edge detector kernel, in the horizontal orientation.
   * It named 'horizontal' because it is used to find horizontal edges in the image. It can be used to compute dy.
   *
   * The 'horizontal' Sobel kernel is:
   *   -1 -2 -1
   *    0  0  0
   *   +1 +2 +1
   *
   * The pixels along the border of the image will be replicated for the purposes of this filter.
   * @author Patrick Lacz
   */
  public static Image convolveSobelHorizontal(Image sourceImage)
  {
    return convolveSobelHorizontal(sourceImage, RegionOfInterest.createRegionFromImage(sourceImage));
  }

  /**
   * Convolve with the Sobel edge detector kernel, in the horizontal orientation.
   * It named 'horizontal' because it is used to find horizontal edges in the image. It can be used to compute dy.
   *
   * The 'horizontal' Sobel kernel is:
   *   -1 -2 -1
   *    0  0  0
   *   +1 +2 +1
   *
   * An Image will be returned with the same dimensions as the input region of interest.
   * If required, the borders of the image will be replicated. If the region of interest is smaller than the image,
   * the pixels bordering the region of interest will be used to compute the result.
   *
   * @author Patrick Lacz
   */
  public static Image convolveSobelHorizontal(Image sourceImage, RegionOfInterest roi)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(sourceImage));
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format

    Image result = new Image(roi.getWidth(), roi.getHeight());
    nativeConvolveSobel(sourceImage.getNativeDefinition(),
                        roi.getMinX(), roi.getMinY(),
                        result.getNativeDefinition(),
                        roi.getWidth(), roi.getHeight(), 0);
    return result;
  }

  /**
   * Convolve with the Sobel edge detector kernel, in the vertical orientation.
   * It named 'vertical' because it is used to find vertical edges in the image. It can be used to compute dx.
   *
   * The 'vertical' Sobel kernel is:
   *   +1  0 -1
   *   +2  0 -2
   *   +1  0 -1
   *
   * The pixels along the border of the image will be replicated for the purposes of this filter.
   * @author Patrick Lacz
   */
  public static Image convolveSobelVertical(Image sourceImage)
  {
    return convolveSobelVertical(sourceImage, RegionOfInterest.createRegionFromImage(sourceImage));
  }

  /**
   * Convolve with the Sobel edge detector kernel, in the vertical orientation.
   * It named 'vertical' because it is used to find vertical edges in the image. It can be used to compute dx.
   *
   * The 'vertical' Sobel kernel is:
   *   +1  0  -1
   *   +2  0  -2
   *   +1  0  -1
   *
   * An Image will be returned with the same dimensions as the input region of interest.
   * If required, the borders of the image will be replicated. If the region of interest is smaller than the image,
   * the pixels bordering the region of interest will be used to compute the result.
   *
   * @author Patrick Lacz
   */
  public static Image convolveSobelVertical(Image sourceImage, RegionOfInterest roi)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(sourceImage));
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format

    Image result = new Image(roi.getWidth(), roi.getHeight());
    nativeConvolveSobel(sourceImage.getNativeDefinition(),
                        roi.getMinX(), roi.getMinY(),
                        result.getNativeDefinition(),
                        roi.getWidth(), roi.getHeight(), 1);
    return result;
  }


  /**
   * @todo : Create Regression Tests
   * @author Patrick Lacz
   */
  public static Image convolveLowpass(Image sourceImage)
  {
    return convolveLowpass(sourceImage, RegionOfInterest.createRegionFromImage(sourceImage));
  }

  /**
   * @todo : Create Regression Tests
   * @author Patrick Lacz
   */
  public static Image convolveLowpass(Image sourceImage, RegionOfInterest roi)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(sourceImage));
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format

    Image result = new Image(roi.getWidth(), roi.getHeight());

    nativeConvolveLowpass(sourceImage.getNativeDefinition(),
                        roi.getMinX(), roi.getMinY(),
                        result.getNativeDefinition(),
                        roi.getWidth(), roi.getHeight(), 3);
    return result;
  }

  /**
   * Dilating an image convolves a maximum 'filter' across the region of interest.
   *
   * This method is named dilate to avoid confusion with Statistics.max()
   *
   * @author Patrick Lacz
   */
  public static Image dilate(Image sourceImage)
  {
    return dilate(sourceImage, RegionOfInterest.createRegionFromImage(sourceImage), 3);
  }

  /**
   * Dilating an image convolves a maximum 'filter' across the region of interest.
   *
   * This method is named dilate to avoid confusion with Statistics.max()
   *
   * @author Patrick Lacz
   */
  public static Image dilate(Image sourceImage, RegionOfInterest regionOfInterest)
  {
    return dilate(sourceImage, regionOfInterest, 3);
  }

  /**
   * Dilating an image convolves a maximum 'filter' across the region of interest.
   *
   * This method is named dilate to avoid confusion with Statistics.max()
   *
   * @todo Use the newer IPP 5.1 erode/dialate, open/closing methods.
   * @todo Create Regression Tests
   * @author Patrick Lacz
   */
  public static Image dilate(Image sourceImage, RegionOfInterest roi, int kernelSize)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(sourceImage));
    Assert.expect(kernelSize > 1 && kernelSize % 2 == 1);
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format

    Image destImage = new Image(roi.getWidth(), roi.getHeight());
    nativeMax(sourceImage.getNativeDefinition(),
                        roi.getMinX(), roi.getMinY(),
                        destImage.getNativeDefinition(),
                        kernelSize, kernelSize,
                        kernelSize/2, kernelSize/2);
    return destImage;
  }

  /**
   * Eroding an image convolves a minimum 'filter' across the image.
   *
   * This method is named erode to avoid confusion with Statistics.min()
   *
   * @author Patrick Lacz
   */
  public static Image erode(Image sourceImage)
  {
    return erode(sourceImage, RegionOfInterest.createRegionFromImage(sourceImage), 3);
  }

  /**
   * Eroding an image convolves a minimum 'filter' across the region of interest.
   *
   * This method is named erode to avoid confusion with Statistics.min()
   *
   * @author Patrick Lacz
   */
  public static Image erode(Image sourceImage, RegionOfInterest regionOfInterest)
  {
    return erode(sourceImage, regionOfInterest, 3);
  }

  /**
   * Eroding an image convolves a minimum 'filter' across the region of interest.
   *
   * This method is named erode to avoid confusion with Statistics.min()
   *
   * @todo Use the newer IPP 5.1 erode/dialate, open/closing methods.
   *
   * @todo Create Regression Tests
   * @author Patrick Lacz
   */

  public static Image erode(Image sourceImage, RegionOfInterest roi, int kernelSize)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(roi != null);
    Assert.expect(kernelSize > 1 && kernelSize % 2 == 1);
    Assert.expect(roi.fitsWithinImage(sourceImage));
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format

    Image destImage = new Image(roi.getWidth(), roi.getHeight());
    nativeMin(sourceImage.getNativeDefinition(),
                        roi.getMinX(), roi.getMinY(),
                        destImage.getNativeDefinition(),
                        kernelSize, kernelSize,
                        kernelSize/2, kernelSize/2);
    return destImage;
  }

  /**
   * Performs a dilate-erode cycle of the given size.
   *
   * @todo Use the newer IPP 5.1 erode/dialate, open/closing methods.
   *
   * @todo Create Regression Tests
   * @author Patrick Lacz
   */
  public static Image closing(Image sourceImage, RegionOfInterest roi, int size)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(sourceImage));
    Assert.expect(size > 0);

    Image dilateResult = dilate(sourceImage, roi, 2*size+1);
    RegionOfInterest resultRoi = RegionOfInterest.createRegionFromImage(dilateResult);
    Image erodeResult = erode(dilateResult, resultRoi, 2*size+1);
    dilateResult.decrementReferenceCount();
    return erodeResult;
  }

  /**
   * Performs a erode-dilate cycle of the given size.
   *
   * @todo Use the newer IPP 5.1 erode/dialate, open/closing methods.
   *
   * @todo Create Regression Tests
   * @author Patrick Lacz
   */
  public static Image opening(Image sourceImage, RegionOfInterest roi, int size)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(sourceImage));
    Assert.expect(size > 0);

    Image erodeResult = erode(sourceImage, roi, 2*size+1);
    RegionOfInterest resultRoi = RegionOfInterest.createRegionFromImage(erodeResult);
    Image dilateResult = dilate(erodeResult, resultRoi, 2*size+1);
    erodeResult.decrementReferenceCount();
    return dilateResult;
  }

  /**
   * Applies a median filter to each pixel in the region of interest.
   * This method does not handle kernels that extend over the boundaries. The kernel
   * is anchored to the upper left corner, so in general the size of your region
   * of interest should be:
   * width-kernelWidth+1, height-kernelHeight+1
   *
   * WARNING : This method performs its math at 8u because the IPP has no 32f median filter.
   *  The conversion is handled internally -- the new image is still an image of 32-bit floats.
   *
   * @author Patrick Lacz
   */
  public static Image median(Image sourceImage, RegionOfInterest roi, int kernelWidth, int kernelHeight)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(sourceImage));
    Assert.expect(kernelWidth > 0);
    Assert.expect(kernelHeight > 0);
    Assert.expect(kernelWidth % 2 == 1); // for some reason the ipp implementation requires odd kernel sizes
    Assert.expect(kernelHeight % 2 == 1); // likewise
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format

    // We are not handling borders, so make sure that the ROI doesn't step over them.
    Assert.expect(roi.getMaxX() + kernelWidth - 1 < sourceImage.getWidth());
    Assert.expect(roi.getMaxY() + kernelHeight - 1 < sourceImage.getHeight());

    Image resultImage = new Image(roi.getWidth(), roi.getHeight());

    nativeMedian(sourceImage.getNativeDefinition(),
                 roi.getMinX(), roi.getMinY(),
                 resultImage.getNativeDefinition(),
                 kernelWidth, kernelHeight);
    return resultImage;
  }


  /**
   * @author Patrick Lacz
   * @author Matt Wharton
   *
   * @param matchQuality DoubleRef The quality of the match. If using cross-correlation, ranges from -1 to 1
   * where 1 is a perfect match.  If using sum of squared distances, ranges from 0 to Double.MAX_VALUE where 0 is a
   * perfect match.
   * @return ImageCoordinte The upper left corner of the the position best matching the template image.
   */
  public static ImageCoordinate matchTemplate(Image sourceImage,
                                              Image templateImage,
                                              MatchTemplateTechniqueEnum matchTemplateTechniqueEnum,
                                              DoubleRef matchQuality)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(templateImage != null);
    Assert.expect(matchTemplateTechniqueEnum != null);
    Assert.expect(matchQuality != null);
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format

    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder(0, 0, sourceImage.getWidth() - 1,
        sourceImage.getHeight() - 1, 0, RegionShapeEnum.RECTANGULAR);

    return matchTemplate(sourceImage, templateImage, roi, matchTemplateTechniqueEnum, matchQuality);
  }

  /**
   * @author Patrick Lacz
   * @author Matt Wharton
   *
   * @param matchQuality DoubleRef The quality of the match. If using cross-correlation, ranges from -1 to 1
   * where 1 is a perfect match.  If using sum of squared distances, ranges from 0 to Double.MAX_VALUE where 0 is a
   * perfect match.
   * @return ImageCoordinte The upper left corner of the the position best matching the template image.
   */
  public static ImageCoordinate matchTemplate(Image sourceImage,
                                              Image templateImage,
                                              RegionOfInterest regionOfInterest,
                                              MatchTemplateTechniqueEnum matchTemplateTechniqueEnum,
                                              DoubleRef matchQuality)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(templateImage != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(sourceImage));
    Assert.expect(templateImage.getHeight() <= sourceImage.getHeight());
    Assert.expect(templateImage.getWidth() <= sourceImage.getWidth());
    Assert.expect(matchTemplateTechniqueEnum != null);
    Assert.expect(matchQuality != null);
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format

    Image destImage = new Image(
      regionOfInterest.getWidth() - templateImage.getWidth() + 1,
      regionOfInterest.getHeight() - templateImage.getHeight() + 1);

    nativeMatchTemplate(sourceImage.getNativeDefinition(), regionOfInterest.getMinX(), regionOfInterest.getMinY(),
                        destImage.getNativeDefinition(),
                        templateImage.getNativeDefinition(),
                        regionOfInterest.getWidth(),
                        regionOfInterest.getHeight(),
                        matchTemplateTechniqueEnum.getId());

    ImageCoordinate coord = null;
    if (matchTemplateTechniqueEnum.equals(MatchTemplateTechniqueEnum.CROSS_CORRELATION_NO_PIXEL_VALUE_LEVELING) ||
        matchTemplateTechniqueEnum.equals(MatchTemplateTechniqueEnum.CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING))
    {
      coord = Statistics.maxPixel(destImage);
    }
    else if (matchTemplateTechniqueEnum.equals(MatchTemplateTechniqueEnum.SUM_SQUARED_DISTANCES))
    {
      coord = Statistics.minPixel(destImage);
    }
    else
    {
      Assert.expect(false, "Unexpected match template technique: " + matchTemplateTechniqueEnum.getId());
    }

    matchQuality.setValue(destImage.getPixelValue(coord.getX(), coord.getY()));
    destImage.decrementReferenceCount();

    // correct for the region of interest
    coord.setX(coord.getX() + regionOfInterest.getMinX());
    coord.setY(coord.getY() + regionOfInterest.getMinY());
    return coord;
  }

  /**
   * Performs Sobel edge detection in all 8 orientations (4 are symmetric, so it looks like only 4).
   *
   * @author Patrick Lacz
   */
  public static Image convolveSobelCompass(Image sourceImage, RegionOfInterest sourceRoi)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(sourceRoi != null);
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format

    Image sourceImageToUse = sourceImage;
    RegionOfInterest roiToUse = sourceRoi;
    if (sourceRoi.getMinX() == 0 ||
        sourceRoi.getMinY() == 0 ||
        sourceRoi.getMaxX() == sourceImage.getWidth() - 1 ||
        sourceRoi.getMaxY() == sourceImage.getHeight() - 1)
    {
      sourceImageToUse = new Image(sourceImage.getWidth() + 2, sourceImage.getHeight() + 2);
      nativeBorderReplicate(sourceImage.getNativeDefinition(),
                            sourceImageToUse.getNativeDefinition(),
                            1, 1);
      roiToUse = new RegionOfInterest(roiToUse);
      roiToUse.translateXY(1, 1);
    } else {
      sourceImageToUse.incrementReferenceCount();
    }

    Assert.expect(roiToUse.getMinX() > 0 && roiToUse.getMinY() > 0);
    Assert.expect(roiToUse.getMaxX() < sourceImageToUse.getWidth() - 1);
    Assert.expect(roiToUse.getMaxY() < sourceImageToUse.getHeight() - 1);

    // 'South'
    Image sobelHorizImage = convolveSobelHorizontal(sourceImageToUse, roiToUse);
    // 'East'
    Image sobelVerticalImage = convolveSobelVertical(sourceImageToUse, roiToUse);

    ImageCoordinate oneOneAnchor = new ImageCoordinate(1, 1);

    // There are two other sobel filters used to catch diagonal lines.
    // The "45 Up" kernel finds lines oriented 45 degrees counterclockwise from the x axis, ('Southeast')
    Image sobel45UpKernel = Image.createFloatImageFromArray(3, 3,
          -2f, -1f,  0f,
          -1f,  0f,  1f,
           0f,  1f,  2f );
    Image sobel45UpImage = convolveKernel(sourceImageToUse, roiToUse, sobel45UpKernel, oneOneAnchor,
                                          BorderSamplingModeEnum.REPLICATE_IMAGE_BORDER);
    sobel45UpKernel.decrementReferenceCount();

    // The "45 Down" kernel finds lines oriented 45 degrees clockwise from the x axis. ('Northeast')
    Image sobel45DownKernel = Image.createFloatImageFromArray(3, 3,
           0f,  1f,  2f,
          -1f,  0f,  1f,
          -2f, -1f,  0f );
    Image sobel45DownImage = convolveKernel(sourceImageToUse, roiToUse, sobel45DownKernel, oneOneAnchor,
                                          BorderSamplingModeEnum.REPLICATE_IMAGE_BORDER);
    sobel45DownKernel.decrementReferenceCount();
    sourceImageToUse.decrementReferenceCount();

    // The remaining 4 directions are symmetric.
    Arithmetic.absoluteValueInPlace(sobelHorizImage); // 'North'
    Arithmetic.absoluteValueInPlace(sobelVerticalImage); // 'West'
    Arithmetic.absoluteValueInPlace(sobel45UpImage); // 'Northwest'
    Arithmetic.absoluteValueInPlace(sobel45DownImage); // 'Southwest'

    Image cardinalImage = Arithmetic.maximumOfImages(sobelHorizImage, sobelVerticalImage);
    Image diagonalImage = Arithmetic.maximumOfImages(sobel45UpImage, sobel45DownImage);
    sobelHorizImage.decrementReferenceCount();
    sobelVerticalImage.decrementReferenceCount();
    sobel45UpImage.decrementReferenceCount();
    sobel45DownImage.decrementReferenceCount();

    Image resultImage = Arithmetic.maximumOfImages(cardinalImage, diagonalImage);
    cardinalImage.decrementReferenceCount();
    diagonalImage.decrementReferenceCount();
    return resultImage;
  }

  /**
   * Perform non-maximal supression within 1 graylevel.
   *
   * At each pixel, only keep its value if it is either the maximum of its neighbors or it
   * is within the <code>tolerance</code> graylevel of that maximum.
   *
   * @author Patrick Lacz
   */
  public static void suppressNonMaximalPixelsInPlace(Image sourceImage, RegionOfInterest sourceRoi, float tolerance)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(sourceRoi != null);
    Assert.expect(sourceRoi.fitsWithinImage(sourceImage));
    Assert.expect(tolerance >= 0.f);

    Image maxImage = Filter.dilate(sourceImage, sourceRoi, 3); // dilate is a max filter
    RegionOfInterest maxImageRoi = RegionOfInterest.createRegionFromImage(maxImage);
    Image differenceImage = Arithmetic.subtractImages(sourceImage, sourceRoi, maxImage, maxImageRoi);
    Threshold.threshold(differenceImage, maxImageRoi, -tolerance, 0.f, -tolerance, 1.0f);
    Arithmetic.multiplyImages(sourceImage, sourceRoi, differenceImage, maxImageRoi, sourceImage, sourceRoi);
    maxImage.decrementReferenceCount();
    differenceImage.decrementReferenceCount();
  }


  /**
   * Set up a new image that is larger than the source image. In the extra pixels, assign the value of nearest pixel from the
   * source image.
   *
   * The offset allows you to grow the image non-symmetrically. It is the offset from the new image's upper left pixel to the
   * position of the source image's upper left corner in the enarged image. Normally this is half the size of the kernel you
   * are about to apply.
   *
   * @author Patrick Lacz
   */
  public static Image replicateBorder(Image sourceImage, int newWidth, int newHeight, int offsetX, int offsetY)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(newWidth - offsetX > sourceImage.getWidth());
    Assert.expect(newHeight - offsetY > sourceImage.getHeight());
    Assert.expect(sourceImage.getBytesPerPixel() == 4); // float format

    Image destImage = new Image(newWidth, newHeight);
    nativeBorderReplicate(sourceImage.getNativeDefinition(), destImage.getNativeDefinition(), offsetX, offsetY);
    return destImage;
  }

  /**
   * Find a rectangular region in an image. Unlike matchTemplate, it does not return a match quality metric.
   *
   * The LocateRectangle algorithm (used in LocatePad) works by returning the pixel with the best match
   * of a 'donut' convolved over a search window in an image.
   *
   * This implementation is based on RegionLock in regnlock.c from the 5dx code.
   *
   * The 'donut' is a rectangular filter that has a 'Ring' region and a 'Hole' region. The rectangle to match
   * is the Hole, and the Ring is a region around the Hole that has the opposite greyscale.
   *
   * In Genesis, to find a rectangular joint, we maximize Ring's average greylevel - Hole's average greylevel.
   * (5dx did the opposite)
   *
   * The filter that does this has the following shape:
   *
   *			r  r  r  r  r  r  r  r  r  r
   *			r  r  r  r  r  r  r  r  r  r
   *      r  r -h -h -h -h -h -h  r  r
   *      r  r -h -h -h -h -h -h  r  r
   *      r  r -h -h -h -h -h -h  r  r
   *      r  r -h -h -h -h -h -h  r  r
   *      r  r -h -h -h -h -h -h  r  r
   *      r  r -h -h -h -h -h -h  r  r
   *			r  r  r  r  r  r  r  r  r  r
   *			r  r  r  r  r  r  r  r  r  r
   *
   * Where h = 1.f/(Area of Hole Region) = 1.f/(Hole.width*Hole.height)
   * and r = 1.f/(Area of Ring Region) = 1.f/(Ring.width*Ring.height - Hole.width*Hole.height)
   *
   * The area with -h weights is the 'Hole' region and the area with r weights is the 'Ring' region.
   *
   * Conceptually, this filter is applied at every pixel (in which the filter fits) in the search region. For
   * example, in the above 10x10 filter, a search region size of 20x20 could return values within 11x11 pixels.
   * This means that the location returned by this function will never be within 'borderWidth/Height' pixels of
   * the edge of the search region.  The function returns the top left corner of the rectangle in the image.
   * The java function LocatePad that wraps nativeLocateRectangle converts this to an ImageRectangle.
   *
   * ----------------------
   * Implementation Details
   *
   * In practice, the rectangle could have any size, far larger than the above example.  Implementing this function
   * as a simple convolution is inefficient.  What we are essentially doing is comparing the results of two averages.
   * A good way to speed up processing in these cases is to separate the kernel into an X pass and a Y pass.
   * (Separable Kernels).  The specific kernel we have is not separable, but each of its box filters is.
   *
   * Also, noting that the weights are uniform, we can make another optimization the old implementation referred
   * to as 'marching.' Each pixel result can be written simply as the sum of the previous pixel and some other data.
   * For instance, to average this signal:
   *    2  5  3  1  7  4  8  3  5  3  7  9
   * The uniform box filter:
   *   .2 .2 .2 .2 .2
   * might be used.
   *
   * After the first value is found (3.6), the results of the following filter can be added to it for the next value.
   *  -.2             +.2  ( + 3.6 = 4.0 )
   *     -.2             +.2  ( + 4.0 = 4.6)
   *
   * The idea is that the forward frontier is added and the trailing frontier is subtracted. When combined with an
   * inner region, such as the hole region, we can apply the same technique: add the _difference_ between -h and r
   * along the hole's forward frontier, and subtract the _difference_ along the hole's trailing frontier.
   *
   * The hole and the ring have different sizes, so we generate two results from the first seperable step. In the
   * code these are refered to as the 'hole result' or 'H' and the 'ring result' or 'R'.
   *
   * The recursive forumla is:
   *  f(x+1,y) = f(x,y)
   *    + 1/|R| * (R(x+ringWidth-borderWidth, y-borderHeight) - R(x-borderWidth, y-borderHeight))
   *    + (-1/|H| - 1/|R|) * (H(x+holeWidth, y) - H(x, y))
   *
   * Here we also assume that instead of an averaging box filter, R and H were created with 1's at every location.
   *
   * The implementation organizes the images so that the borderWidth/Height corrections are unnecessary. The returned
   * location is the upper left corner of the inner rectangle.
   *
   * @todo : Create Regression Tests
   * @param borderWidth int The X-dim size of the outer 'ring' around the component to match.
   * @param borderHeight int The Y-dim size of the outer 'ring' around the component to match.
   * @param findDarkBoxInLightField boolean Controls whether a dark or light box is searched for.
   * @return ImageCoordinate The UL pixel of the rectangle in the image (compensates for the border)
   * @author Patrick Lacz
   */
  public static ImageCoordinate locateRectangle(Image image, RegionOfInterest searchRegion, int rectWidth,
                                                int rectHeight, int borderWidth, int borderHeight,
                                                boolean findDarkBoxInLightField)
  {
    Assert.expect(image != null);
    Assert.expect(searchRegion != null);
    Assert.expect(rectWidth > 0);
    Assert.expect(rectHeight > 0);
    Assert.expect(borderWidth >= 0);
    Assert.expect(borderHeight >= 0);
    Assert.expect(image.getBytesPerPixel() == 4); // float format
    //Assert.expect(searchRegion.getRegionShapeEnum() == RegionShapeEnum.RECTANGULAR);

    int ringWidth = rectWidth + 2 * borderWidth;
    int ringHeight = rectHeight + 2 * borderHeight;

    Assert.expect(searchRegion.fitsWithinImage(image));
    Assert.expect(ringWidth <= searchRegion.getWidth());
    Assert.expect(ringHeight <= searchRegion.getHeight());

    int rectArea = rectWidth * rectHeight;
    int ringArea = ringWidth * ringHeight - rectArea;

    // To search for a white rectangle in a dark field, flip the signs here
    //
    // Also, from here on we begin to call the rectangle a 'hole' -- the alliteration
    // with 'ring' and 'rect' makes discussing it difficult. With these scalars
    // we are looking for a dark spot in a light field.
    float ringScalar = 0.5f / ringArea;
    float holeScalar = -1.f / rectArea - ringScalar;

    if (findDarkBoxInLightField == false)
    {
      ringScalar *= -1.f;
      holeScalar *= -1.f;
    }

    ImageCoordinate resultSize = new ImageCoordinate(
        searchRegion.getWidth() - ringWidth + 1,
        searchRegion.getHeight() - ringHeight + 1);

    // Compute 'R', the intermediate result for computing the Ring area.
    Image ringResult = Filter.convolveRowsUniformKernel(image, searchRegion, ringWidth, ringScalar, 0,
        BorderSamplingModeEnum.CONSTRAIN_TO_REGION);

     // Compute 'H', the intermediate result for computing the Hole area.
     // A region of interest is used to restrict the processing to the area that is actually of use
     // to us later. It also means that the pixels are aligned with 'R' (0,0) in both images will be used
     // together.

    RegionOfInterest holeRoi = new RegionOfInterest(
        searchRegion.getMinX() + borderWidth,
        searchRegion.getMinY() + borderHeight, // offset to area capable of being in 'hole'
        searchRegion.getWidth() - 2 * borderWidth,
        searchRegion.getHeight() - 2 * borderHeight,
        0, RegionShapeEnum.RECTANGULAR);

    Image holeResult = Filter.convolveRowsUniformKernel(image, holeRoi, rectWidth, holeScalar, 0, BorderSamplingModeEnum.CONSTRAIN_TO_REGION);

    Image resultImage = new Image(resultSize.getX(), resultSize.getY());

    Assert.expect(ringResult.getHeight() == searchRegion.getHeight());
    Assert.expect(holeResult.getHeight() == searchRegion.getHeight() - 2 * borderHeight);
    Assert.expect(ringResult.getWidth() == resultImage.getWidth());
    Assert.expect(holeResult.getWidth() == resultImage.getWidth());

    nativeMarchingConvolutionForLocateRectangle(ringResult.getNativeDefinition(), holeResult.getNativeDefinition(),
                                                resultImage.getNativeDefinition(), rectHeight, ringHeight);

    // Find the location of the Maximum pixel
    ImageCoordinate maxPixel = Statistics.maxPixel(resultImage);
    maxPixel.setX(maxPixel.getX() + borderWidth + searchRegion.getMinX());
    maxPixel.setY(maxPixel.getY() + borderHeight + searchRegion.getMinY());

    ringResult.decrementReferenceCount();
    holeResult.decrementReferenceCount();
    resultImage.decrementReferenceCount();
    return maxPixel;
  }


  /**
   * Copies the source image into the destination image with extra border pixels replicated from the source image border.
   *
   * The destination buffer must have a size equal to srcWidth + 2 * borderWidth, srcHeight + 2 * borderHeight!
   * @author Patrick Lacz
   */
  private static native void nativeBorderReplicate(int sourceImage[], int destImage[], int boarderX, int boarderY);


  /**
   * @author Patrick Lacz
   * @param srcOffset int The number of pixels to advance to get to the ROI (the ROI w/h is the dest w/h)
   * @param kernel float[] The kernel for the row filter to apply.
   */
  private static native void nativeConvolveRows(int sourceImage[], int offsetX, int offsetY,
      int destImage[], float kernel[]);


  /**
   * @author Patrick Lacz
   * @param srcOffset int The number of pixels to advance to get to the ROI (the ROI w/h is the dest w/h)
   * @param kernel float[] The kernel for the column filter to apply.
   */
  private static native void nativeConvolveColumns(int sourceImage[], int offsetX, int offsetY,
      int destImage[], float kernel[]);

  /**
   * @author Patrick Lacz
   */
  private static native void nativeConvolveKernel(int[] sourceImage, int srcOffsetX, int srcOffsetY,
      int[] kernelImage, int[] destImage);

  /**
   * @author Patrick Lacz
   * @param direction 0 for horizontal, 1 for vertical
   */
  private static native void nativeConvolveSobel(int sourceImage[], int offsetX, int offsetY,
                                                 int destImage[],
                                                 int roiWidth, int roiHeight, int direction);


  /**
   * @author Patrick Lacz
   * @param kernelWidth Must be either 3 or 5.
   */
  private static native void nativeConvolveLowpass(int sourceImage[], int offsetX, int offsetY,
                                                 int destImage[],
                                                 int roiWidth, int roiHeight, int kernelWidth);


  /**
   * @author Patrick Lacz
   */
  private static native void nativeMax(int sourceImage[], int offsetX, int offsetY,
                                       int destImage[], int kernelWidth, int kernelHeight, int anchorX, int anchorY);


  /**
   * @author Patrick Lacz
   */
  private static native void nativeMin(int sourceImage[], int offsetX, int offsetY,
                                       int destImage[], int kernelWidth, int kernelHeight, int anchorX, int anchorY);


  /**
   * @author Patrick Lacz
   */
  private static native void nativeMedian(int sourceImage[], int offsetX, int offsetY,
                                          int destImage[], int kernelWidth, int kernelHeight);


  /**
   * @author Patrick Lacz
   * @author Matt Wharton
   */
  private static native void nativeMatchTemplate(int sourceImage[], int offsetX, int offsetY,
                                                 int destImage[],
                                                 int templateImage[],
                                                 int roiWidth,
                                                 int roiHeight,
                                                 int matchTemplateTechnique);


  /**
   * @author Patrick Lacz
   * @param direction 0 for horizontal (along X), 1 for vertical (along Y)
   */
  private static native void nativeMarchingConvolveUniform1DKernel(int sourceImage[], int offsetX, int offsetY,
      int destImage[], int roiWidth, int roiHeight,
      int kernelLength, float kernelValue, int direction);


  /**
   * @author Patrick Lacz
   */
  private static native void nativeMarchingConvolutionForLocateRectangle(int ringImage[], int holeImage[], int resultImage[],
      int holeWidth, int ringWidth);
}
