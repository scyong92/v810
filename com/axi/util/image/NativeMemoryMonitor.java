package com.axi.util.image;

import java.lang.ref.*;
import java.util.*;
import java.util.concurrent.atomic.*;
import java.nio.*;

import com.axi.util.*;

/**
 * @author Patrick Lacz
 */
public class NativeMemoryMonitor
{
  private static NativeMemoryMonitor _instance = null;

  // These are intentially package-private. The Image class uses them.
  static final int _ALIGNED_MEMORY = 0;
  static final int _CONTIGUOUS_MEMORY = 1;
  static final int _CPP_CONTIGUOUS_MEMORY = 2;
  static final int _DEFAULT = _ALIGNED_MEMORY;

  static final int _SMALLEST_IMAGE_BLOCK_SIZE = 64 * 64 * 4;

  private static final int _DEFAULT_NEAR_LIMITS_SIZE = 650 * 1024*1024;
  private static final int _DEFAULT_MAX_MEMORY = 700 * 1024 * 1024;

  private AtomicLong _currentMemoryUsage = new AtomicLong(0);
  private AtomicLong _numberOfFreedImages = new AtomicLong(0);
  private AtomicLong _numberOfGCFreedImages = new AtomicLong(0);

  private long _maxMemoryUsage = _DEFAULT_MAX_MEMORY;
  private long _nearLimitsSize = _DEFAULT_NEAR_LIMITS_SIZE;

  private Map<PhantomReference<? extends Object>, int[]> _phantomReferenceToNativeDataMap = Collections.synchronizedMap(new HashMap<PhantomReference<? extends Object>,  int[]>());

  private ReferenceQueue<Object> _finalizedPhantomReferenceQueue = new ReferenceQueue<Object>();

  private WorkerThread _resourceReclaimer = new WorkerThread("Native Memory Reclaimer");
  private static WorkerThread _monitorThread = null; // used for debugging

  /**
   * @author Patrick Lacz
   */
  public synchronized static NativeMemoryMonitor getInstance()
  {
    if (_instance == null)
    {
      _instance = new NativeMemoryMonitor();
    }
    return _instance;
  }

  /**
   * @author Patrick Lacz
   */
  private NativeMemoryMonitor()
  {
    startMemoryReclaimingThread();
//    startMemoryMonitoringThread(); // used for debugging / observing memory performance.
  }

  /**
   * @author Patrick Lacz
   */
  private NativeMemoryMonitor(long maximumSpaceInBytes, long nearLimitSize)
  {
    _maxMemoryUsage = maximumSpaceInBytes;
    _nearLimitsSize = nearLimitSize;
    startMemoryReclaimingThread();
//    startMemoryMonitoringThread(); // used for debugging / observing memory performance.
  }

  /**
   * @author Patrick Lacz
   */
  public static void initialize(long maximumSizeOfImagesInBytes, long nearLimitSizeInBytes)
  {
    // if you hit this expect, some image is getting allocated before you called initialize.
    Assert.expect(_instance == null);
    _instance = new NativeMemoryMonitor(maximumSizeOfImagesInBytes, nearLimitSizeInBytes);
  }


  /**
   * @author Patrick Lacz
   */
  private void startMemoryMonitoringThread()
  {
    _monitorThread = new WorkerThread("Memory Monitor Report Thread");
    _monitorThread.invokeLater(new Runnable()
    {
      public void run()
      {
        while (true)
        {
          try
          {
            Thread.sleep(1000 * 5);
          }
          catch (InterruptedException ex)
          {
          }


          NativeMemoryMonitor.getInstance().displayDebugUtilizationReport();
        }
      }
    });

  }

  /**
   * This method is a fail-safe. If an image is not properly reference-counted, (erring on the side of not-enough decrements),
   * we can find the image when it gets garbage collected and free up that memory.
   *
   * This thread performs that function.
   *
   * @author Patrick Lacz
   */
  private void startMemoryReclaimingThread()
  {
    _resourceReclaimer.invokeLater(new Runnable()
       {
         public void run()
         {
           while (true)
           {
             Reference<? extends Object> referenceToObject = null;
             try
             {
               // block until one of the objects we are monitoring is garbage collected
               referenceToObject = _finalizedPhantomReferenceQueue.remove();
             }
             catch (InterruptedException ex)
             {
               continue;
             }
             if (referenceToObject == null)
               continue;

             PhantomReference<? extends Object> phantomReference = (PhantomReference<? extends Object>)referenceToObject;
             if (_phantomReferenceToNativeDataMap.containsKey(phantomReference))
             {
               int imageData[] = _phantomReferenceToNativeDataMap.remove(phantomReference);
               int amountOfMemoryFreed = nativeFree(imageData);
               long newMemoryUsage = _currentMemoryUsage.addAndGet( -amountOfMemoryFreed);

               Assert.expect(newMemoryUsage >= 0);

               _numberOfFreedImages.incrementAndGet();
               _numberOfGCFreedImages.incrementAndGet();
             }
             referenceToObject.clear();
           }
         }
      });
  }

  /**
   * This method is dependent on IPP's memory alignment.
   * Currently, IPP requires 32-byte alignment for every image and every row of every image.
   * (This, in turn, is based on MMX / SSE instruction set requirements, and will probably change only rarely).
   *
   * Nevertheless, ***** BE SURE TO VERIFY THIS FOR EACH NEW RELEASE OF IPP!!! *****
   *
   * @author Patrick Lacz
   */
  public int getAlignedStrideForByteWidth(int widthInBytes)
  {
    int overAlignment = widthInBytes % 32;
    if (overAlignment > 0)
      return widthInBytes + 32 - overAlignment;

    return widthInBytes;
  }


  /**
   * Allocate a new image. The nativeDefinition contains all the information the JNI routines
   * need to know about the image, such as dimensions, stride, and a pointer to the data.
   *
   * allocationMode should be one of _ALIGNED_MEMORY, _CONTIGUOUS_MEMORY, _CPP_CONTIGUOUS_MEMORY, or _DEFAULT
   *
   * @author Patrick Lacz
   */
  public int[] alloc(Image owner, int width, int height, int allocationMode)
  {
    int nativeDefinition[] = new int[Image._LENGTH_OF_NATIVE_DEFINITION_ARRAY];

    // we may store a different native definition that is allocated to the minimum size.
    int nativeDefinitionToUseForAllocation[] = nativeDefinition;
    int allocationModeToUse = allocationMode;

    nativeDefinition[Image._WIDTH_INDEX] = width;
    nativeDefinition[Image._HEIGHT_INDEX] = height;
    int stride = width * 4; // assumes a IEEE 32bit float image.
    if (allocationMode == _ALIGNED_MEMORY)
      stride = getAlignedStrideForByteWidth(width*4); // assumes a IEEE 32bit float image.

    Assert.expect(_phantomReferenceToNativeDataMap != null);

    PhantomReference<Image> phantomReferenceToOwner = new PhantomReference<Image>(owner, _finalizedPhantomReferenceQueue);
    owner.setPhantomReference(phantomReferenceToOwner);

    // attempt allocation
    int size =  stride * height;

    if (size < _SMALLEST_IMAGE_BLOCK_SIZE)
    {
      //allocationModeToUse = _CONTIGUOUS_MEMORY;

      nativeDefinitionToUseForAllocation = new int[Image._LENGTH_OF_NATIVE_DEFINITION_ARRAY];
      nativeDefinitionToUseForAllocation[Image._WIDTH_INDEX] = _SMALLEST_IMAGE_BLOCK_SIZE / 4;  // assumes a IEEE 32bit float image.
      nativeDefinitionToUseForAllocation[Image._HEIGHT_INDEX] = 1;
      size =_SMALLEST_IMAGE_BLOCK_SIZE;
    }

    if (_currentMemoryUsage.get() + size > _maxMemoryUsage)
    {
      // In future implementations, we may want to have an option here that blocks instead of asserting.
      Assert.expect(false, "Ran over native memory limit - Out of Memory " + size + " requested. " + _currentMemoryUsage + "/" + _maxMemoryUsage );
    }

    if (allocationModeToUse == _CONTIGUOUS_MEMORY)
      nativeAllocateContiguous(nativeDefinitionToUseForAllocation);
    else if (allocationModeToUse == _ALIGNED_MEMORY)
      nativeAllocateAligned(nativeDefinitionToUseForAllocation);
    else if (allocationModeToUse == _CPP_CONTIGUOUS_MEMORY)
      nativeAllocateCPPContiguous(nativeDefinitionToUseForAllocation);
    else
      Assert.expect(false);

    // build up the native definition to be used by the other primitives (the ones that don't know that we might have allocated extra memory)
    System.arraycopy(nativeDefinitionToUseForAllocation, 0, nativeDefinition, 0, nativeDefinitionToUseForAllocation.length);
    nativeDefinition[Image._WIDTH_INDEX] = width;
    nativeDefinition[Image._HEIGHT_INDEX] = height;
    if (allocationMode == _ALIGNED_MEMORY)
      nativeDefinition[Image._STRIDE_INDEX] = getAlignedStrideForByteWidth(4 * width); // assumes a IEEE 32bit float image.
    else
      nativeDefinition[Image._STRIDE_INDEX] = 4 * width; // assumes a IEEE 32bit float image.

    // keep track of where the buffer came from.
    _phantomReferenceToNativeDataMap.put(phantomReferenceToOwner, nativeDefinitionToUseForAllocation);

    _currentMemoryUsage.addAndGet(size);

    return nativeDefinition;
  }

  /**
   * @author Patrick Lacz
   */
  public void free(Image owner)
  {
    Assert.expect(_phantomReferenceToNativeDataMap != null);

    PhantomReference<Image> phantomReferenceToOwner = owner.getPhantomReference();

    int imageData[] = _phantomReferenceToNativeDataMap.remove(phantomReferenceToOwner);
    long amountOfMemoryFreed = nativeFree(imageData);
    long newAmountOfMemoryUsed = _currentMemoryUsage.addAndGet(-amountOfMemoryFreed);

    Assert.expect(newAmountOfMemoryUsed >= 0);

    _numberOfFreedImages.incrementAndGet();
  }

  /**
   * @author Patrick Lacz
   */
  public boolean isNearLimits()
  {
    if (_currentMemoryUsage.get() > _nearLimitsSize)
      return true;
    return false;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void displayDebugUtilizationReport()
  {
    double oneMegabyte = 1024.0*1024.0;
    System.out.println("[pwl] Native Memory Utilization Report at " + (new Date(System.currentTimeMillis())).toString());
    System.out.println("[pwl] " + _currentMemoryUsage.get() / oneMegabyte + "MB / " + _maxMemoryUsage / oneMegabyte + "MB currently used.");
    double freeMemory = Runtime.getRuntime().freeMemory() / oneMegabyte;
    double maxMemory = Runtime.getRuntime().maxMemory() / oneMegabyte;
    double totalMemory = Runtime.getRuntime().totalMemory() / oneMegabyte;
    System.out.println("[pwl] JAVA Free Memory : " + freeMemory + " / Max Memory : " + maxMemory + " / Total Memory : " + totalMemory);
  }

  /**
   * @author Patrick Lacz
   */
  public long getTotalFreeMemory()
  {
    return _maxMemoryUsage - (long)_currentMemoryUsage.get();
  }

  /**
   * @author Patrick Lacz
   */
  public float getTotalUsedMemoryInMegabytes()
  {
    return ((float)_currentMemoryUsage.get()) / (1024 * 1024);
  }

  /**
   * @author Patrick Lacz
   */
  private static native void nativeAllocateAligned(int[] image);

  /**
   * @author Patrick Lacz
   */
   private static native void nativeAllocateContiguous(int[] image);

  /**
   * @author Patrick Lacz
   */
   private static native void nativeAllocateCPPContiguous(int[] image);

  /**
   * returns the number of bytes freed by the call
   * @author Patrick Lacz
   */
   private static native int nativeFree(int[] image);
}
