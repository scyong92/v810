#include <jni.h>
#include "com_axi_util_image_ImageIoUtil.h"

#include "jniImageUtil.h"

#include "ippcore.h"
#include "ippi.h"

#include "sptAssert.h"
#include "jniUtil.h"
#include <string>
using namespace std;

#define PNG_USE_DLL

#include "png.h"

/**
 * throwCouldNotReadFileException
 *
 * @author Patrick Lacz
 */
static void throwCouldNotCreateFileException(JNIEnv *jEnv, string const &filename)
{
  sptAssert(jEnv);

  jstring filenameString = jEnv->NewStringUTF(filename.c_str());
  sptAssert(filenameString);

  // find the requested class
  jclass exceptionClass = jEnv->FindClass("com/axi/util/CouldNotCreateFileException");
  sptAssert(exceptionClass);
 
  // find the constructor
  jmethodID constructorId = jEnv->GetMethodID(exceptionClass, "<init>", "(Ljava/lang/String;)V");
  sptAssert(constructorId);
  
  // create the exception.
  jobject exception = jEnv->NewObject(exceptionClass, constructorId, filenameString);
  sptAssert(exception);
  
  // throw the exception.
  jEnv->Throw((jthrowable)exception);
}

/**
 * pngErrorFunction
 *
 * A callback function for png saving. Simply assert with the error message.
 * @author Patrick Lacz
 */
static void pngErrorFunction(png_structp pWriteStruct, png_const_charp message)
{
   sptAssertWithMessage(false, message);
}

/**
 * nativeSavePng
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_ImageIoUtil_nativeSavePng(JNIEnv *jEnv, 
                                                                             jclass jContext, 
                                                                             jintArray jImageDef, 
                                                                             jstring targetFileWithPath, 
                                                                             jstring textComment)
{
  try
  {
    string fileName = getSTLString(jEnv, targetFileWithPath);
    
    // open the file
    FILE *pFile = fopen(fileName.c_str(), "wb");
    
    if (pFile == NULL)
    {
      throwCouldNotCreateFileException(jEnv, fileName);
      return;
    }
    
    // create the png structures
    png_structp pWriteStruct = png_create_write_struct(PNG_LIBPNG_VER_STRING,
                                                       (png_voidp)NULL,
                                                       pngErrorFunction,
                                                       pngErrorFunction);
    
    if (pWriteStruct == NULL)
    {
      fclose(pFile);
      sptAssert(false);
      return;
    }
    
    png_infop pInfoStruct = png_create_info_struct(pWriteStruct);
    
    if (pInfoStruct == NULL)
    {
      png_destroy_write_struct(&pWriteStruct, (png_infopp)NULL);
      fclose(pFile);
      sptAssert(false);
      return;
    }
    
    png_init_io(pWriteStruct, (png_FILE_p)pFile);
    
    JNIImageData *pImage = getImageDataFromJava(jEnv, jImageDef);
    
    png_set_IHDR(pWriteStruct, 
                 pInfoStruct, 
                 pImage->width, 
                 pImage->height, 
                 8, 
                 PNG_COLOR_TYPE_GRAY,
                 PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT,
                 PNG_FILTER_TYPE_DEFAULT);
    
    // set the comments if there are any
    string comment = getSTLString(jEnv, textComment);
    if (comment.length() > 0)
    {
      png_text text_definition;
      
      text_definition.compression = PNG_TEXT_COMPRESSION_NONE;
      text_definition.key = "comment";
      text_definition.text = const_cast<char*>(comment.c_str());
      text_definition.text_length = comment.length();
      
      png_set_text(pWriteStruct, pInfoStruct, &text_definition, 1);
    }
    
    // this option uses a low amount of compression
    png_set_compression_level(pWriteStruct, Z_BEST_SPEED);
    
    // get the data in 8-bit format for writing
    int rowStrideInBytes;
    Ipp8u *pByteImageBuffer = ippiMalloc_8u_C1(pImage->width, pImage->height, &rowStrideInBytes);
    
    if (pByteImageBuffer == NULL)
    {
      sptAssert(false);
      return;
    }
    
    IppStatus ippResult = ippiConvert_32f8u_C1R(pImage->address,
                                                pImage->stride,
                                                pByteImageBuffer,
                                                rowStrideInBytes,
                                                pImage->getSize(),
                                                ippRndNear);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
    
    // png wants an array of pointers to each row
    png_byte ** pRows = new png_byte *[pImage->height];
    
    for (int i = 0 ; i < pImage->height ; ++i)
      pRows[i] = pByteImageBuffer + i * rowStrideInBytes;
    
    releaseImageDataFromJavaUnmodified(jEnv, jImageDef, pImage);
    
    png_set_rows(pWriteStruct, pInfoStruct, pRows);
    
    // write the file
    png_write_png(pWriteStruct, pInfoStruct, PNG_TRANSFORM_IDENTITY, NULL);
    
    // clean up
    png_destroy_write_struct(&pWriteStruct, &pInfoStruct);
    
    ippiFree(pByteImageBuffer);
    delete [] pRows;
    fclose(pFile);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

