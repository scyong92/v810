package com.axi.util.image;

import java.nio.*;

import com.axi.util.*;
import java.awt.image.*;

/**
 * Arithmetic class
 *
 * This class holds functions to perform various arithmetic operations on Image data.
 * Many of the methods are provided in two flavors: one that takes an Image and a RegionOfInterest
 * and one that takes only an Image. In these cases, the one that does not specify a RegionOfInterest operates
 * on all pixels in the Image. If a RegionOfInterest is specified, the operation is limited to only those pixels
 * that lie within the Region of Interest.
 *
 * @author Patrick Lacz
 */
public class Arithmetic
{
  /**
   * @author Patrick Lacz
   */
  public Arithmetic()
  {
    // do nothing
    // methods in Arithmetic are staticly defined.
  }

  /**
   * @author Patrick Lacz
   */
  static
  {
    System.loadLibrary("nativeImageUtil");
  }

  /**
   * Return a new image containing the sum of each pixel in the Region of Interest in image A with the
   * corresponding pixel in the Region of Interest in image B.
   *
   * The regions of interest must have the same width and height. The result image will have the same
   * width and height as the regions of interest.
   *
   * @todo : Create Regression Tests
   * @author Patrick Lacz
   */
  public static Image addImages(Image imageA, RegionOfInterest roiForImageA, Image imageB,
                                RegionOfInterest roiForImageB)
  {
    Assert.expect(imageA != null);
    Assert.expect(roiForImageA != null);
    Assert.expect(roiForImageA.fitsWithinImage(imageA));
    Assert.expect(imageB != null);
    Assert.expect(roiForImageB != null);
    Assert.expect(roiForImageB.fitsWithinImage(imageB));

    Image destImage = new Image(roiForImageA.getWidth(), roiForImageA.getHeight());

    addImages(imageA, roiForImageA, imageB, roiForImageB, destImage, RegionOfInterest.createRegionFromImage(destImage));

    return destImage;
  }

  /**
   * Assigns the destination image to the sum of each pixel in the Region of Interest in image A with the
   * corresponding pixel in the Region of Interest in image B.
   *
   * The regions of interest must have the same width and height.
   *
   * @todo : Create Regression Tests
   * @author Patrick Lacz
   */
  public static void addImages(
      Image imageA, RegionOfInterest roiForImageA,
      Image imageB, RegionOfInterest roiForImageB,
      Image destImage, RegionOfInterest destRoi)
  {
    Assert.expect(imageA != null);
    Assert.expect(roiForImageA != null);
    Assert.expect(imageB != null);
    Assert.expect(roiForImageB != null);
    Assert.expect(destImage != null);
    Assert.expect(destRoi != null);
    Assert.expect(roiForImageA.fitsWithinImage(imageA));
    Assert.expect(roiForImageB.fitsWithinImage(imageB));
    Assert.expect(destRoi.fitsWithinImage(destImage));
    Assert.expect(roiForImageA.getWidth() == destRoi.getWidth());
    Assert.expect(roiForImageA.getHeight() == destRoi.getHeight());
    Assert.expect(destRoi.getWidth() == roiForImageB.getWidth());
    Assert.expect(destRoi.getHeight() == roiForImageB.getHeight());

    nativeAdd(imageA.getNativeDefinition(), roiForImageA.getMinX(), roiForImageA.getMinY(),
              imageB.getNativeDefinition(), roiForImageB.getMinX(), roiForImageB.getMinY(),
              destImage.getNativeDefinition());
  }
  
   /**
   * Assigns the destination image to the AND of each pixel in the Region of Interest in image A with the
   * corresponding pixel in the Region of Interest in image B.
   *
   * The regions of interest must have the same width and height.
   *
   * @todo : Create Regression Tests
   * @author Jack Hwee
   */
  public static void andImages(
      Image imageA, RegionOfInterest roiForImageA,
      Image imageB, RegionOfInterest roiForImageB,
      Image destImage, RegionOfInterest destRoi)
  {
    Assert.expect(imageA != null);
    Assert.expect(roiForImageA != null);
    Assert.expect(imageB != null);
    Assert.expect(roiForImageB != null);
    Assert.expect(destImage != null);
    Assert.expect(destRoi != null);
    Assert.expect(roiForImageA.fitsWithinImage(imageA));
    Assert.expect(roiForImageB.fitsWithinImage(imageB));
    Assert.expect(destRoi.fitsWithinImage(destImage));
//    Assert.expect(roiForImageA.getWidth() == destRoi.getWidth());
//    Assert.expect(roiForImageA.getHeight() == destRoi.getHeight());
//    Assert.expect(destRoi.getWidth() == roiForImageB.getWidth());
//    Assert.expect(destRoi.getHeight() == roiForImageB.getHeight());

    nativeAnd(imageA.getNativeDefinition(), roiForImageA.getMinX(), roiForImageA.getMinY(),
              imageB.getNativeDefinition(), roiForImageB.getMinX(), roiForImageB.getMinY(),
              destImage.getNativeDefinition());
 
  }
  
   /**
   * Assigns the destination image to the AND of each pixel in the Region of Interest in image A with the
   * corresponding pixel in the Region of Interest in image B.
   *
   * The regions of interest must have the same width and height.
   *
   * @todo : Create Regression Tests
   * @author Jack Hwee
   */
  public static void orImages(
      Image imageA, RegionOfInterest roiForImageA,
      Image imageB, RegionOfInterest roiForImageB,
      Image destImage, RegionOfInterest destRoi)
  {
    Assert.expect(imageA != null);
    Assert.expect(roiForImageA != null);
    Assert.expect(imageB != null);
    Assert.expect(roiForImageB != null);
    Assert.expect(destImage != null);
    Assert.expect(destRoi != null);
    Assert.expect(roiForImageA.fitsWithinImage(imageA));
    Assert.expect(roiForImageB.fitsWithinImage(imageB));
    Assert.expect(destRoi.fitsWithinImage(destImage));
//    Assert.expect(roiForImageA.getWidth() == destRoi.getWidth());
//    Assert.expect(roiForImageA.getHeight() == destRoi.getHeight());
//    Assert.expect(destRoi.getWidth() == roiForImageB.getWidth());
//    Assert.expect(destRoi.getHeight() == roiForImageB.getHeight());

    nativeOr(imageA.getNativeDefinition(), roiForImageA.getMinX(), roiForImageA.getMinY(),
              imageB.getNativeDefinition(), roiForImageB.getMinX(), roiForImageB.getMinY(),
              destImage.getNativeDefinition());
 
  }
  
  /**
   * Subtract the pixel values in image B from image A.
   *
   * @author Patrick Lacz
   */
  public static Image subtractImages(Image imageA, Image imageB)
  {
    return subtractImages(imageA, RegionOfInterest.createRegionFromImage(imageA),
                          imageB, RegionOfInterest.createRegionFromImage(imageB));
  }

  /**
   * Subtract the corresponding pixel values in image B from image A.
   *
   * @todo Use the IPP subtraction call rather than this piecemeal construction of 3 methods.
   * @author Patrick Lacz
   */
  public static Image subtractImages(Image imageA, RegionOfInterest roiForImageA, Image imageB, RegionOfInterest roiForImageB)
  {
    Assert.expect(imageA != null);
    Assert.expect(roiForImageA != null);
    Assert.expect(imageB != null);
    Assert.expect(roiForImageB != null);
    Assert.expect(roiForImageA.fitsWithinImage(imageA));
    Assert.expect(roiForImageB.fitsWithinImage(imageB));
    Assert.expect(roiForImageA.getWidth() == roiForImageB.getWidth());
    Assert.expect(roiForImageA.getHeight() == roiForImageB.getHeight());

    Image imageBCopy = Image.createCopy(imageB, roiForImageB);
    multiplyByConstantInPlace(imageBCopy, -1.0f);
    Image result = addImages(imageA, roiForImageA, imageBCopy, RegionOfInterest.createRegionFromImage(imageBCopy));
    imageBCopy.decrementReferenceCount();
    return result;
  }

  /**
   * Convert negative valued pixels to positive valued pixels of the same magnitude.
   *
   * @author Patrick Lacz
   */
  public static void absoluteValueInPlace(Image image)
  {
    absoluteValueInPlace(image, RegionOfInterest.createRegionFromImage(image));
  }

  /**
   * Convert negative valued pixels to positive valued pixels of the same magnitude within the specified region.
   *
   * @author Patrick Lacz
   */
  public static void absoluteValueInPlace(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));

    nativeAbsoluteValueInPlace(image.getNativeDefinition(), roi.getMinX(), roi.getMinY(), roi.getWidth(), roi.getHeight());
  }

  /**
   * Returns a new image that contains the maximum of the correpsonding pixels of each image.
   * The regions of interest much match in dimension.
   *
   * @author Patrick Lacz
   */
  public static Image maximumOfImages(Image imageA, Image imageB)
  {
    return maximumOfImages(imageA, RegionOfInterest.createRegionFromImage(imageA),
                           imageB, RegionOfInterest.createRegionFromImage(imageB));
  }

  /**
   * Returns a new image that contains the maximum of the correpsonding pixels of each image.
   * The regions of interest much match in dimension.
   *
   * @author Patrick Lacz
   */
  public static Image maximumOfImages(Image imageA, RegionOfInterest roiA, Image imageB, RegionOfInterest roiB)
  {
    Assert.expect(imageA != null);
    Assert.expect(roiA != null);
    Assert.expect(imageB != null);
    Assert.expect(roiB != null);
    Assert.expect(roiA.fitsWithinImage(imageA));
    Assert.expect(roiB.fitsWithinImage(imageB));
    Assert.expect(roiA.getWidth() == roiB.getWidth());
    Assert.expect(roiA.getHeight() == roiB.getHeight());

    Image destImage = new Image(roiA.getWidth(), roiA.getHeight());

    nativeMaximum(imageA.getNativeDefinition(), roiA.getMinX(), roiA.getMinY(),
                  imageB.getNativeDefinition(), roiB.getMinX(), roiB.getMinY(),
                  destImage.getNativeDefinition());

    return destImage;
  }

  /**
   * Return a new image containing the product of each pixel in the Region of Interest in image A with the
   * corresponding pixel in the Region of Interest in image B.
   *
   * The regions of interest must have the same width and height. The result image will have the same
   * width and height as the regions of interest.
   *
   * @todo : Create Regression Tests
   * @author Patrick Lacz
   */
  public static Image multiplyImages(Image imageA, RegionOfInterest roiA, Image imageB, RegionOfInterest roiB)
  {
    Assert.expect(imageA != null);
    Assert.expect(roiA != null);
    Assert.expect(imageB != null);
    Assert.expect(roiB != null);
    Assert.expect(roiA.fitsWithinImage(imageA));
    Assert.expect(roiB.fitsWithinImage(imageB));
    Assert.expect(roiA.getWidth() == roiB.getWidth());
    Assert.expect(roiA.getHeight() == roiB.getHeight());

    Image destImage = new Image(roiA.getWidth(), roiA.getHeight());

    nativeMultiply(imageA.getNativeDefinition(), roiA.getMinX(), roiA.getMinY(),
                  imageB.getNativeDefinition(), roiB.getMinX(), roiB.getMinY(),
                  destImage.getNativeDefinition());

    return destImage;
  }

  /**
   * Assigns the destination image to the product of each pixel in the Region of Interest in image A with the
   * corresponding pixel in the Region of Interest in image B.
   *
   * The regions of interest must have the same width and height.
   *
   * @todo : Create Regression Tests
   * @author Patrick Lacz
   */
  public static void multiplyImages(Image imageA, RegionOfInterest roiForImageA,
      Image imageB, RegionOfInterest roiForImageB,
      Image destImage, RegionOfInterest destRoi)
  {
    Assert.expect(imageA != null);
    Assert.expect(roiForImageA != null);
    Assert.expect(imageB != null);
    Assert.expect(roiForImageB != null);
    Assert.expect(destImage != null);
    Assert.expect(destRoi != null);
    Assert.expect(roiForImageA.fitsWithinImage(imageA));
    Assert.expect(roiForImageB.fitsWithinImage(imageB));
    Assert.expect(destRoi.fitsWithinImage(destImage));
    Assert.expect(roiForImageA.getWidth() == destRoi.getWidth());
    Assert.expect(roiForImageA.getHeight() == destRoi.getHeight());
    Assert.expect(destRoi.getWidth() == roiForImageB.getWidth());
    Assert.expect(destRoi.getHeight() == roiForImageB.getHeight());

    nativeMultiply(imageA.getNativeDefinition(), roiForImageA.getMinX(), roiForImageA.getMinY(),
                  imageB.getNativeDefinition(), roiForImageB.getMinX(), roiForImageB.getMinY(),
                  destImage.getNativeDefinition());
  }

  /**
   * @author Patrick Lacz
   */
  public static void exponentialInPlace(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));

    nativeExponentialInPlace(image.getNativeDefinition(), roi.getMinX(), roi.getMinY(), roi.getWidth(), roi.getHeight());
  }

  /**
   * @author Patrick Lacz
   */
  public static void naturalLogarithmInPlace(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));

    nativeNaturalLogarithmInPlace(image.getNativeDefinition(), roi.getMinX(), roi.getMinY(), roi.getWidth(), roi.getHeight());
  }

  /**
   * @author Patrick Lacz
   */
  public static void squareInPlace(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));

    nativeSquareInPlace(image.getNativeDefinition(), roi.getMinX(), roi.getMinY(), roi.getWidth(), roi.getHeight());
  }

  /**
   * @author Patrick Lacz
   */
  public static void squareRootInPlace(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));

    nativeSquareRootInPlace(image.getNativeDefinition(), roi.getMinX(), roi.getMinY(), roi.getWidth(), roi.getHeight());
  }

  /**
   * Add a value to every pixel in the image.
   * @author Patrick Lacz
   */
  public static void addConstantInPlace(Image image, float value)
  {
    Assert.expect(image != null);

    nativeAddConstant(image.getNativeDefinition(), 0, 0, image.getWidth(), image.getHeight(), value);
  }

  /**
   *  Add a value to every pixel in the region of interest.
   * @author Patrick Lacz
   */
  public static void addConstantInPlace(Image image, RegionOfInterest roi, float value)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));

    nativeAddConstant(image.getNativeDefinition(), roi.getMinX(), roi.getMinY(), roi.getWidth(), roi.getHeight(), value);
  }

  /**
   * Multiply every pixel in the image by a constant value.
   * @author Patrick Lacz
   */
  public static void multiplyByConstantInPlace(Image image, float value)
  {
    Assert.expect(image != null);

    nativeMultiplyByConstant(image.getNativeDefinition(), 0, 0, image.getWidth(), image.getHeight(), value);
  }

  /**
   * Multiply every pixel in the region of interest by a constant value.
   * @author Patrick Lacz
   */
  public static void multiplyByConstantInPlace(Image image, RegionOfInterest roi, float value)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));

    nativeMultiplyByConstant(image.getNativeDefinition(), roi.getMinX(), roi.getMinY(), roi.getWidth(), roi.getHeight(), value);
  }

  /**
   * @author Patrick Lacz
   */
  public static void polarToCartesian(Image radiusImage, Image thetaImage, Image destXImage, Image destYImage)
  {
    Assert.expect(radiusImage != null);
    Assert.expect(thetaImage != null);
    Assert.expect(destXImage != null);
    Assert.expect(destYImage != null);

    int width = radiusImage.getWidth();
    int height = radiusImage.getHeight();
    Assert.expect(width == thetaImage.getWidth());
    Assert.expect(height == thetaImage.getHeight());
    Assert.expect(width == destXImage.getWidth());
    Assert.expect(height == destXImage.getHeight());
    Assert.expect(width == destYImage.getWidth());
    Assert.expect(height == destYImage.getHeight());

    Assert.expect(radiusImage.getBytesPerPixel() == 4);
    Assert.expect(thetaImage.getBytesPerPixel() == 4);
    Assert.expect(destXImage.getBytesPerPixel() == 4);
    Assert.expect(destYImage.getBytesPerPixel() == 4);

    nativePolarToCartesian(radiusImage.getNativeDefinition(), thetaImage.getNativeDefinition(), destXImage.getNativeDefinition(), destYImage.getNativeDefinition());
  }

  /**
   * @author Patrick Lacz
   */
  private static native void nativeAdd(int[] leftImage, int leftOffsetX, int leftOffsetY,
                                       int[] rightImage, int rightOffsetX, int rightOffsetY,
                                       int[] destImagee);

  /**
   * @author Patrick Lacz
   */
  private static native void nativeMaximum(int[] leftImage, int leftOffsetX, int leftOffsetY,
                                           int[] rightImage, int rightOffsetX, int rightOffsetY,
                                           int[] destImagee);

  /**
   * @author Patrick Lacz
   */
  private static native void nativeMultiply(int[] leftImage, int leftOffsetX, int leftOffsetY,
                                            int[] rightImage, int rightOffsetX, int rightOffsetY,
                                            int[] destImagee);

  /**
   * @author Patrick Lacz
   */
  private static native void nativeAbsoluteValueInPlace(
      int[] sourceImage,
      int offsetX,
      int offsetY,
      int roiWidth,
      int roiHeight);

  /**
   * @author Patrick Lacz
   */
  private static native void nativeMultiplyByConstant(
      int[] sourceImage,
      int offsetX,
      int offsetY,
      int roiWidth,
      int roiHeight,
      float value);

  /**
   * @author Patrick Lacz
   */
  private static native void nativeAddConstant(
      int[] sourceImage,
      int offsetX,
      int offsetY,
      int roiWidth,
      int roiHeight,
      float value);


  /**
   * @author Patrick Lacz
   */
  private static native void nativeSquareRootInPlace(
      int[] sourceImage,
      int offsetX,
      int offsetY,
      int roiWidth,
      int roiHeight);

  /**
   * @author Patrick Lacz
   */
  private static native void nativeSquareInPlace(
      int[] sourceImage,
      int offsetX,
      int offsetY,
      int roiWidth,
      int roiHeight);

  /**
   * @author Patrick Lacz
   */
  private static native void nativeNaturalLogarithmInPlace(
      int[] sourceImage,
      int offsetX,
      int offsetY,
      int roiWidth,
      int roiHeight);

  /**
   * @author Patrick Lacz
   */
  private static native void nativeExponentialInPlace(
      int[] sourceImage,
      int offsetX,
      int offsetY,
      int roiWidth,
      int roiHeight);

  /**
   * This method can not be used on a region-of-interest.
   * Since the IPP signals library is used for implementation, the pixels must be contiguous in memory.
   * @author Patrick Lacz
   */
  private static native void nativePolarToCartesian(
      int[] sourceRadiusImage,
      int[] sourceThetaImage,
      int[] destXImage,
      int[] destYImage);
  
   /**
   * @author Jack Hwee
   */
  private static native void nativeAnd(int[] leftImage, int leftOffsetX, int leftOffsetY,
                                       int[] rightImage, int rightOffsetX, int rightOffsetY,
                                       int[] destImagee); 
  
   /**
   * @author Jack Hwee
   */
  private static native void nativeOr(int[] leftImage, int leftOffsetX, int leftOffsetY,
                                       int[] rightImage, int rightOffsetX, int rightOffsetY,
                                       int[] destImagee); 
  
  /**
  * Blend the contents of two BufferedImages according to a specified weight.
  * 
  * @param bi1 first BufferedImage
  * @param bi2 second BufferedImage
  * @param weight the fractional percentage of the first image to keep
  *
  * @return new BufferedImage containing blended contents of BufferedImage arguments
  * 
  * Reference : http://www.informit.com/articles/article.aspx?p=1245201
  * @edit bee-hoon.goh
  */
  static public BufferedImage blend (BufferedImage bi1, BufferedImage bi2, double weight)
  {
    if (bi1 == null)
      throw new NullPointerException ("bi1 is null");

    if (bi2 == null)
      throw new NullPointerException ("bi2 is null");

    int width = bi1.getWidth ();
    if (width != bi2.getWidth ())
      throw new IllegalArgumentException ("widths not equal");

    int height = bi1.getHeight ();
    if (height != bi2.getHeight ())
      throw new IllegalArgumentException ("heights not equal");

    BufferedImage bi3 = new BufferedImage (width, height, BufferedImage.TYPE_BYTE_GRAY);
    int [] rgbim1 = new int [width];
    int [] rgbim2 = new int [width];
    int [] rgbim3 = new int [width];

    for (int row = 0; row < height; row++)
    {
       bi1.getRGB (0, row, width, 1, rgbim1, 0, width);
       bi2.getRGB (0, row, width, 1, rgbim2, 0, width);

       for (int col = 0; col < width; col++)
       {
         int rgb1 = rgbim1 [col];
         int r1 = (rgb1 >> 16) & 255;
         int g1 = (rgb1 >> 8) & 255;
         int b1 = rgb1 & 255;

         int rgb2 = rgbim2 [col];
         int r2 = (rgb2 >> 16) & 255;
         int g2 = (rgb2 >> 8) & 255;
         int b2 = rgb2 & 255;

         int r3 = (int) (r1 * weight + r2 * (1.0 - weight));
         int g3 = (int) (g1 * weight + g2 * (1.0 - weight));
         int b3 = (int) (b1 * weight + b2 * (1.0 - weight));
         rgbim3 [col] = (r3 << 16) | (g3 << 8) | b3;
       }
       bi3.setRGB (0, row, width, 1, rgbim3, 0, width);
    }
    return bi3;
  }
}
