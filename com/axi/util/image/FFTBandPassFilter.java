package com.axi.util.image;

/**
 * FFTBandPassFilter - Fast Fourier Transform Band Pass Filter.
 * This method will help in removes the high spatial frequencies (blurring the image) and low spatial frequencies 
 * (similar to subtracting a blurred image). It can also suppress horizontal or vertical stripes that were created by 
 * scanning an image line by line.
 * Please refer http://imagejdocu.tudor.lu/doku.php?id=gui:process:fft#bandpass_filter
 * 
 * @param _lowPassLargeDownTo - Smooth variations of the image with typical sizes of bright or dark patches 
 *                                                         larger than this value are suppressed (background).
 * @param _hiPassSmallUpTo - Determines the amount of smoothing. Objects in the image smaller than this size 
 *                                                  are strongly attanuated. Note that these values are both half the spatial frequencies 
 *                                                  of the actual cutoff. The cutoff is very soft, so the bandpass will noticeably 
 *                                                  attenuate even spatial frequencies in the center of the bandpass unless the 
 *                                                  difference of the two values is large (say, more than a factor of 5 or so).
 * @param _toleranceOfDirection - This is for Suppress Stripes; higher values remove shorter stripes and/or stripes 
 *                                                       that are running under an angle with respect to the horizontal (vertical) direction.
 * @param _suppressStripesModeEnumId - Select whether to eliminate horizontal or vertical stripes. Removal of 
 *                                                                     horizontal stripes is similar to subtracting an image that is only blurred 
 *                                                                     in the horizontal direction from the original.
 * @param _enhanceContrast - To enable the contrast enhancement on the image.
 * @param _saturateValue - allows some intensities to go into saturation, and produces a better visual contrast. 
 *                                            Saturate only has an effect when _enhanceContrast is enabled.
 * @param _interpolationModeEnumId - Enhance contrast interpolation method, could be linear or cubic.
 * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
 */
public class FFTBandPassFilter extends ImageEnhancerBase
{
  public int _lowPassLargeDownTo = 40;
  public int _hiPassSmallUpTo = 3;
  public double _toleranceOfDirection = 5;
  public FilterDirectionModeEnum _suppressStripesModeEnumId = FilterDirectionModeEnum.None;
  public boolean _enhanceContrast = true;
  public int _saturateValue = 5;
  public InterpolationModeEnum _interpolationModeEnumId = InterpolationModeEnum.Linear;  

  public FFTBandPassFilter(int lowPassLargeDownTo, int hiPassSmallUpTo, double toleranceOfDirection, 
    FilterDirectionModeEnum suppressStripesModeEnumId, boolean enhanceContrast,
    int saturateValue, InterpolationModeEnum interpolationModeEnumId)
  {
    _lowPassLargeDownTo = lowPassLargeDownTo;
    _hiPassSmallUpTo = hiPassSmallUpTo;
    _toleranceOfDirection = toleranceOfDirection;
    _suppressStripesModeEnumId = suppressStripesModeEnumId;
    _enhanceContrast = enhanceContrast;
    _saturateValue = saturateValue;
    _interpolationModeEnumId = interpolationModeEnumId;
  }
  
 public ImageEnhancerType getType()
 {
  return ImageEnhancerType.FFTBandPassFilter;  
 }
   
  public Image runEnhancement(Image dest, Image src)
  {
    return ImageEnhancer.FFTBandPassFilter(dest, src, _lowPassLargeDownTo, _hiPassSmallUpTo, _toleranceOfDirection, 
      _suppressStripesModeEnumId, _enhanceContrast, _saturateValue, _interpolationModeEnumId);
  }
  
  public void updateParameter(int lowPassLargeDownTo, int hiPassSmallUpTo, double toleranceOfDirection, 
    FilterDirectionModeEnum suppressStripesModeEnumId, boolean enhanceContrast,
    int saturateValue, InterpolationModeEnum interpolationModeEnumId)
  {
    _lowPassLargeDownTo = lowPassLargeDownTo;
    _hiPassSmallUpTo = hiPassSmallUpTo;
    _toleranceOfDirection = toleranceOfDirection;
    _suppressStripesModeEnumId = suppressStripesModeEnumId;
    _enhanceContrast = enhanceContrast;
    _saturateValue = saturateValue;
    _interpolationModeEnumId = interpolationModeEnumId;
  }
}