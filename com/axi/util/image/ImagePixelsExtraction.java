/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.util.image;

//import com.virtualLive.util.*;

import com.axi.util.*;


/**
 * This class holds image processing primitives for extracting features in
 * pixels(or measurements) from an image. By nature, these primitives are
 * slightly higher level than other image processing primitives, as they may
 * call several lower-level primitives in order to create their result.
 * 
 * @author LayNgor
 */
public class ImagePixelsExtraction
{
  
  /**
   * @author Lim, Lay Ngor
   */
  private ImagePixelsExtraction()
  {
    // do nothing
    //
    // Methods in this class are all expected to be statics, so other code really
    // shouldn't need to create an actual PixelsExtraction object.
  }
  
  /**
   * Fill the pixel to fillValue if the image pixel value is in the roi and less
   * than thresholdToFill(non-inclusive). Return total dark pixels detected.
   *
   * @author Lim, Lay Ngor
   */
  static public int fillAndCalculateDarkPixelsInRoiOfImage(Image dest, 
    RegionOfInterest roi, float thresholdToFill, float fillValue)
  {
    Assert.expect(dest != null);
    Assert.expect(roi != null);
    Assert.expect(thresholdToFill >= 0);
    Assert.expect(fillValue > 0);

    int fillArea = 0; 
//    int nonFillArea = 0;
    for (int x = (int) roi.getMinX(); x <= (int) roi.getMaxX(); ++x)
    {
      for (int y = (int) roi.getMinY(); y <= (int) roi.getMaxY(); ++y)    
      {
        ImageCoordinate pixelLocation = new ImageCoordinate(x, y);
        if (roi.containsPixel(pixelLocation)//if its inside the roi
          && dest.getPixelValue(x, y) < thresholdToFill)//and if it is darker than thresholdToFill
        {
          dest.setPixelValue(x, y, fillValue);
          ++fillArea;
        }
        else
        {
          dest.setPixelValue(x, y, 1);//clear background
//          ++nonFillArea;
        }
      }
    }
//    int total = fillArea + nonFillArea;
//    System.out.println("total:" + total);
    
    return fillArea;
  }
  
  static public float fillAndCalculateDarkPixelsInCircleRoiOfImage(Image dest, 
    RegionOfInterest roi, float thresholdToFill, float fillValue)
  {
    Assert.expect(dest != null);
    Assert.expect(roi != null);
    Assert.expect(thresholdToFill >= 0);
    Assert.expect(fillValue > 0);

    int fillArea = 0; 
    int nonFillArea = 0;
    for (int x = (int) roi.getMinX(); x <= (int) roi.getMaxX(); ++x)
    {
      for (int y = (int) roi.getMinY(); y <= (int) roi.getMaxY(); ++y)    
      {
        ImageCoordinate pixelLocation = new ImageCoordinate(x, y);
        if (roi.containsPixel(pixelLocation))//if its inside the roi
        {
          if(dest.getPixelValue(x, y) < thresholdToFill)
          {
            dest.setPixelValue(x, y, fillValue);
            ++fillArea;
          }
          else
          {
            dest.setPixelValue(x, y, 1);//clear background
            ++nonFillArea;
          }
        }
        else
        {
          dest.setPixelValue(x, y, 1);//clear background
//          ++nonFillArea;
        }
      }
    }
    System.out.println("fillArea" + fillArea);
    int total = fillArea + nonFillArea;
    System.out.println("total:" + total);
    
    return (fillArea / (float)(fillArea + nonFillArea)) * 100;
  }  

  // left right top bottom
  static public void createEllipseTemplate(Image dest, 
    RegionOfInterest roi, float fillValue, int rotate)
  {
    Assert.expect(dest != null);
    Assert.expect(roi != null);
    
    RegionOfInterest obRoundRoi = null;
    
    if (rotate == 1)//right
    {
      obRoundRoi = new RegionOfInterest(roi.getCenterX(),
        roi.getMinY() +1, roi.getWidth()/2 -2,
        roi.getHeight() -2, 0, RegionShapeEnum.RECTANGULAR);
    }
    else if (rotate == 2)//left
    {
      obRoundRoi = new RegionOfInterest(roi.getMinX()+1,
        roi.getMinY() +1, roi.getWidth()/2 -2,
        roi.getHeight() -2, 0, RegionShapeEnum.RECTANGULAR);          
    }
    else if(rotate == 3)//top
    {
      obRoundRoi = new RegionOfInterest(roi.getMinX() + 1,
        roi.getMinY() + 1, roi.getWidth() -2,
        roi.getHeight()/2 -2, 0, RegionShapeEnum.RECTANGULAR);   
    }
    else if(rotate == 4)//bottom
    {
      obRoundRoi = new RegionOfInterest(roi.getMinX() +1,
        roi.getCenterY(), roi.getWidth() -2,
        roi.getHeight()/2 -2, 0, RegionShapeEnum.RECTANGULAR);        
    }
    
    Assert.expect(obRoundRoi != null);
    DoubleCoordinate center = new DoubleCoordinate(obRoundRoi.getCenterX(), obRoundRoi.getCenterY());
    Paint.fillObround(dest, obRoundRoi, center, fillValue);
  }
}
