package com.axi.util.image;

/**
 * RemoveArtifactByBoxFilter - It is a image processing method to remove the image artifact using box blur method.
 * Please refer http://en.wikipedia.org/wiki/Box_blur
 *
 * @param _maskWidth - The size of the mask local region around a pixel in x direction.
 *                                        This size should be larger than the size of features to be preserved.
 * @param _maskHeight - The size of the mask local region around a pixel in y direction.
 *                                         This size should be larger than the size of features to be preserved.
 * @param _iterator - Number of iteration to perform the filter.
 * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
 */
public class RemoveArtifactByBoxFilter extends ImageEnhancerBase
{   
  public int _maskWidth = 30;
  public int _maskHeight = 30;
  public int _iterator = 1;

  public RemoveArtifactByBoxFilter(int maskWidth, int maskHeight, int iterator)
  {
    this._maskWidth = maskWidth;
    this._maskHeight = maskHeight;
    this._iterator = iterator;
  }
  
 public ImageEnhancerType getType()
 {
  return ImageEnhancerType.RemoveArtifactByBoxFilter;  
 }
 
  public Image runEnhancement(Image dest, Image src)
  {   
      return ImageEnhancer.removeArtifactByBoxFilter(dest, src, _maskWidth, _maskHeight, _iterator);      
  }  
  
  public void updateParameter(int maskWidth, int maskHeight, int iterator)
  {
    this._maskWidth = maskWidth;
    this._maskHeight = maskHeight;
    this._iterator = iterator;
  }
}
