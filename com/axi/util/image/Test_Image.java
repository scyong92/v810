package com.axi.util.image;

import com.axi.util.*;
import java.io.*;
import java.nio.ByteBuffer;


/**
 * <p>Title: Test_Image</p>
 * <p>Description: Regression test for Image class</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company:  Agilent Technogies</p>
 * @author Kay Lannen
 * @version 1.0
 */

public class Test_Image extends UnitTest
{

  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Image());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    Image imageBuffer = null;

    ////////////////////////////////////////////////////////////
    // try calling Image constructor with bad arguments //
    ////////////////////////////////////////////////////////////

    try
    {
      imageBuffer = new Image(10, 0);  // height is bad
      Expect.expect(false);  // the last statement should have had an assertion failure
    }
    catch (AssertException ae)
    {
      // do nothing, we expect an assertion failure here!
    }

    try
    {
      imageBuffer = new Image(0, 10);  // width is bad
      Expect.expect(false);  // the last statement should have had an assertion failure
    }
    catch (AssertException ae)
    {
      // do nothing, we expect an assertion failure here!
    }

    /////////////////////////////////////////////////////////////////
    // create an Image and make sure the access methods work //
    /////////////////////////////////////////////////////////////////
    final int TEST_WIDTH = 20;
    final int TEST_HEIGHT = 30;
    imageBuffer = new Image(TEST_WIDTH, TEST_HEIGHT);

    int returnedWidth = imageBuffer.getWidth();
    Expect.expect(returnedWidth == TEST_WIDTH);

    int returnedHeight = imageBuffer.getHeight();
    Expect.expect(returnedHeight == TEST_HEIGHT);

    Expect.expect(imageBuffer.getByteBuffer().isDirect());

    testCreateFloatImageFromByteBuffer();

    imageBuffer.decrementReferenceCount();
  }

  private void testCreateFloatImageFromByteBuffer()
  {
    int width = 130;
    int height = 40;

    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(width*height);
    for (int i = 0 ; i < width*height ; ++i)
      byteBuffer.put((byte)255);

    Image whiteImage = Image.createFloatImageFromByteBuffer(byteBuffer, width, height);
    Expect.expect(whiteImage.getPixelValue(0,0) == 255.0f);
    Expect.expect(whiteImage.getPixelValue(width-1, height-1) == 255.0f);
    Expect.expect(whiteImage.getByteBuffer().isDirect());
    Expect.expect(whiteImage.getBytesPerPixel() == 4);
    whiteImage.decrementReferenceCount();
  }
}
