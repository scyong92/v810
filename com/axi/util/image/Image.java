package com.axi.util.image;

import java.awt.*;
import java.awt.color.*;
import java.awt.image.*;
import java.io.*;
import java.lang.ref.*;
import java.nio.*;
import java.util.*;

import com.axi.util.*;

/**
 * A class for storing raw image data.  The raw data is stored as a
 * <code>ByteBuffer</code>, meaning that it's format can be of any type
 * (float, integer, etc).  This class is designed to be used with single channel
 * images (not multichannel like RGB).
 * <p>
 * This class imposes no data ordering scheme, so users should agree upon this
 * in advance.  Most likely, the data will be ordered such that the top-left
 * pixel is first, and then followed by the remaining pixels, from left-to-right
 * and from top-to-bottom.  But like I say, this ordering scheme is not inherently
 * specified by this class, so users need to agree upon a convention themselves.
 *
 * Keeps a reference count of the number of objects 'using it.
 *   Allocation automatically sets this value to 1.
 *   If an object uses an Image that is passed in as a parameter longer than the execution of that method,
 *      it should increment the reference counter.
 *   If a method returns an Image that it may have allocated, it should document this in the comments and
 *      increment that Images reference counter.
 * Once the reference count drops to zero, the images resources are reclaimed.
 *
 * @author Patrick Lacz
 */
public class Image implements Serializable
{
  // keep track of the size of the current image. These may be obsolete, since these are also defined in the _nativeDefinition.
  private int _bufferHeight = -1;
  private int _bufferWidth = -1;

  // keep a count of the references to this object.
  private int _referenceCount = 0;

  // a debugging tool used to see where reference count error images were allocated.
  private static final boolean _KEEP_ALLOCATION_STACK_TRACES_FOR_DEBUGGING = false;
  private Throwable _debugAllocationTrace = null;

  // indicates if deallocation should go through the NativeMemoryMonitor
  private boolean _wasAllocatedUsingMemoryManager;

  // a container for metadata about the image
  private ImageDescription _imageDescription;

  // the data used by the native calls about this Image.
  private int[] _nativeDefinition = null;
  private PhantomReference<Image> _phantomReference;

  // these MUST match the order of the values in the struct in jniImageUtil.h
  static final int _WIDTH_INDEX = 0;
  static final int _HEIGHT_INDEX = 1;
  static final int _STRIDE_INDEX = 2;
  static int _LENGTH_OF_NATIVE_DEFINITION_ARRAY = 0;

  // a pointer to the java ByteBuffer encapsulating this object. Useful for the Java calls working on this Image.
  private ByteBuffer _bufferData = null;

  private static NativeMemoryMonitor _nativeMemoryMonitor = NativeMemoryMonitor.getInstance();

  /**
   * @author Peter Esbensen
   */
  static
  {
    System.loadLibrary("nativeImageUtil");
    _LENGTH_OF_NATIVE_DEFINITION_ARRAY = Image.nativeGetSizeOfNativeDefinitionInInts();
  }

  /**
   * @author Patrick Lacz
   */
  public Image(int width, int height)
  {
    Assert.expect(height > 0);
    Assert.expect(width > 0);

    // Allocate by bytes, but we want 32bit floats.
    _bufferHeight = height;
    _bufferWidth = width;

    allocate(width, height, NativeMemoryMonitor._DEFAULT);
  }

  /**
   * @author Patrick Lacz
   */
  public Image(Image otherImage)
  {

    _bufferHeight = otherImage.getHeight();
    _bufferWidth = otherImage.getWidth();

    allocate(_bufferWidth, _bufferHeight, NativeMemoryMonitor._DEFAULT);

    Transform.copyImageIntoImage(otherImage, this);
  }

  /**
   * @author Patrick Lacz
   */
  public static Image createCopy(Image otherImage, RegionOfInterest roi)
  {
    Assert.expect(otherImage != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(otherImage));

    Image returnImage = new Image(roi.getWidth(), roi.getHeight());

    Transform.copyImageIntoImage(otherImage, roi, returnImage, RegionOfInterest.createRegionFromImage(returnImage));
    return returnImage;
  }

  /**
   * Convert from a buffer of bytes to an Image, which is a buffer of floats (0 - 255.0), which is
   * assumed by many of the other utility functions in this package.
   * @author Patrick Lacz
   */
  public static Image createFloatImageFromByteBuffer(ByteBuffer bufferOfBytes, int bufferWidth, int bufferHeight)
  {
    Assert.expect(bufferOfBytes != null);
    Assert.expect(bufferWidth > 0);
    Assert.expect(bufferHeight > 0);

    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder(0, 0, bufferWidth - 1, bufferHeight - 1, 0, RegionShapeEnum.RECTANGULAR);
    return createFloatImageFromByteBuffer(bufferOfBytes, bufferWidth, bufferHeight, roi);
  }

  /**
   * Convert from a buffer of bytes to an Image, which is a buffer of floats (0 - 255.0), which is
   * assumed by many of the other utility functions in this package.
   * This method creates an image from a region of interest of the byte buffer.
   * @author Patrick Lacz
   */
  public static Image createFloatImageFromByteBuffer(ByteBuffer bufferOfBytes, int bufferWidth, int bufferHeight, RegionOfInterest roi)
  {
    Assert.expect(bufferOfBytes != null);
    Assert.expect(roi != null);
    Assert.expect(bufferWidth > 0);
    Assert.expect(bufferHeight > 0);

    Assert.expect(roi.getMinX() >= 0 && roi.getMinY() >= 0 &&
                  roi.getMaxX() < bufferWidth && roi.getMaxY() < bufferHeight);

    Image newImage = new Image(roi.getWidth(), roi.getHeight());

    if (bufferOfBytes.isDirect())
    {
      nativeConvertByteToFloatImage(bufferOfBytes,
                                    bufferWidth,
                                    roi.getMinY() * bufferWidth + roi.getMinX(),
                                    newImage.getNativeDefinition());
    }
    else if (bufferOfBytes.hasArray())
    {
      nativeSetImageFromByteArray(newImage.getNativeDefinition(), bufferOfBytes.array());
    }
    else
    {
      Assert.expect(false, "Unhandled ByteBuffer configuration.");
    }
    return newImage;
  }

  /**
   * This is the preferred technique of creating an image from a buffered image.
   * The buffered image constructor will be deprecated.
   *
   * @author Patrick Lacz
   */
  public static Image createFloatImageFromBufferedImage(java.awt.image.BufferedImage bufferedImage)
  {
    Assert.expect(bufferedImage != null);

    java.awt.image.Raster raster = bufferedImage.getRaster();
    int width = raster.getWidth();
    int height = raster.getHeight();
    Image image = new Image(width, height);

    if(raster.getDataBuffer() instanceof DataBufferByte)
    {
      java.awt.image.DataBufferByte asDataBufferByte = ((java.awt.image.DataBufferByte)raster.getDataBuffer());

      nativeSetImageFromByteArray(image.getNativeDefinition(), asDataBufferByte.getData());
    }
    else
    {
      java.awt.image.DataBufferFloat asDataBufferFloat = ((java.awt.image.DataBufferFloat)raster.getDataBuffer());
      nativeSetImageFromArray(image.getNativeDefinition(), asDataBufferFloat.getData());
    }

    return image;
  }

  /**
   * @author Patrick Lacz
   */
  public static Image createUnmonitoredFloatImage(int width, int height)
  {
    Image image = new Image();
    image._bufferData = ByteBuffer.allocateDirect(width * height * 4);
    image._bufferData.order(ByteOrder.LITTLE_ENDIAN);

    image._bufferWidth = width;
    image._bufferHeight = height;
    image._referenceCount = 1;
    image._wasAllocatedUsingMemoryManager = false;
    image._nativeDefinition = new int[_LENGTH_OF_NATIVE_DEFINITION_ARRAY];
    image._nativeDefinition[_WIDTH_INDEX] = width;
    image._nativeDefinition[_HEIGHT_INDEX] = height;
    nativeIntializeJavaDefinition(image._nativeDefinition, image._bufferData);

    return image;
  }

  /**
   * @author Patrick Lacz
   */
  public static Image createContiguousFloatImage(int width, int height)
  {
    Image image = new Image();
    image.allocate(width, height, NativeMemoryMonitor._CONTIGUOUS_MEMORY);
    return image;
  }

  /**
   * @author Patrick Lacz
   */
  public static Image createAlignedFloatImage(int width, int height)
  {
    Image image = new Image();
    image.allocate(width, height, NativeMemoryMonitor._ALIGNED_MEMORY);
    return image;
  }

  /**
   * Create an Image object from an array of floats.
   *
   * This is particularly useful for creating kernels:
   *
   * <code>
   * sobelDiagonalFilter = Image.createfloatImageFromArray(3, 3,
   *                               -2f, -1f, 0f,
   *                               -1f,  0f, 1f,
   *                                0f,  1f, 2f);
   * </code>
   *
   * @author Patrick Lacz
   */
  public static Image createFloatImageFromArray(int width, int height, float ...floatArray)
  {
    Assert.expect(floatArray != null);
    Assert.expect(width * height == floatArray.length);

    Image newImage = new Image(width, height);
    nativeSetImageFromArray(newImage.getNativeDefinition(), floatArray);
    return newImage;
  }

  /**
   * Users of this class should always call the contructor with the full set of arguments, not this default constructor.
   * @author Peter Esbensen
   */
  private Image()
  {
    // do nothing
  }

  /**
   * @author Patrick Lacz
   */
  public int[] getNativeDefinition()
  {
    Assert.expect(_nativeDefinition != null);
    return _nativeDefinition;
  }

  /**
   * Provides access to the underlying representation of the Image.
   * This ByteBuffer may come from an allocated pool, and as a consequence of that, its <code>capacity()</code> will generally
   * return values that are much greater than its actual capacity. Do not use this method and be careful of <code>java.nio</code>
   * methods and objects that might try to make use of this data.
   *
   * @return a ByteBuffer.  Typically, you must somehow know what kind of data is in the ByteBuffer in order to use it,
   * since it could be interpreted as floats, doubles, integers, etc.  Usually, you will want to immediately get the
   * appropriate type of Buffer by calling one of the ByteBuffer static methods like <code>asFloatBuffer</code>.
   *
   * @author Peter Esbensen
   */
  public ByteBuffer getByteBuffer()
  {
    Assert.expect(_nativeDefinition != null);
    if (_bufferData == null)
    {
      _bufferData = nativeGetByteBuffer(_nativeDefinition);
      _bufferData.order(ByteOrder.LITTLE_ENDIAN);
    }
    Assert.expect(_bufferData != null);
    return _bufferData;
  }

  /**
   * @author Patrick Lacz
   */
  public int getBytesPerPixel()
  {
    return 4;
  }

  /**
   * Get the image data in the form of a java.awt.image.BufferedImage.  This function will take a while because I
   * iterate over all pixels, casting them from a float to an integer.
   *
   * @author Peter Esbensen
   */
  public java.awt.image.BufferedImage getBufferedImage()
  {
    ByteBuffer bufferData = getByteBuffer();

    java.awt.image.DataBuffer dataBuffer = null;

    if (getBytesPerPixel() == 4)
    {
      byte[] byteArray = nativeCreateByteArrayFromImage(_nativeDefinition, 0, 0, _bufferWidth, _bufferHeight);
      dataBuffer = new java.awt.image.DataBufferByte(byteArray, byteArray.length);
    }
    else if (getBytesPerPixel() == 1)
    {
      byte[] byteArray = new byte[_bufferWidth * _bufferHeight];
      bufferData.position(0);
      bufferData.get(byteArray);
      bufferData.rewind();

      dataBuffer = new java.awt.image.DataBufferByte(byteArray, byteArray.length);
    }
    else
    {
      Assert.expect(false); // only float and byte formats supported.
    }

    int scanLineStride = _bufferWidth;
    int[] bandOffsets = new int[]
                        {0}; // ditto
    java.awt.Point location = new java.awt.Point(0, 0);
    java.awt.image.WritableRaster raster = java.awt.image.Raster.createInterleavedRaster(dataBuffer,
        _bufferWidth,
        _bufferHeight,
        scanLineStride,
        1, // pixel stride
        bandOffsets,
        location);

    ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_GRAY);
    int[] nBits = {8};
    java.awt.image.ColorModel colorModel = new ComponentColorModel(cs, nBits, false, true,
                                         Transparency.OPAQUE,
                                         DataBuffer.TYPE_BYTE);

    java.awt.image.BufferedImage bufferedImage = new java.awt.image.BufferedImage(colorModel, raster, false, null);

    return bufferedImage;
  }

  /**
   * Get the image data in the form of a java.awt.image.BufferedImage.  This function will take a while because I
   * iterate over all pixels, casting them from a float to an integer.
   *
   * This BufferedImage differs from the getBufferedImage() method in that it creates a buffered image with an ARGB
   * Color format (as opposed to a single-channel format)
   *
   * Other than that, the code is the same as the getBufferedImage method.
   *
   * @author Patrick Lacz
   */
  public java.awt.image.BufferedImage getAlphaBufferedImage(java.awt.Color rgbColor)
  {
    Assert.expect(rgbColor != null);

    int red = rgbColor.getRed();
    int green = rgbColor.getGreen();
    int blue = rgbColor.getBlue();

    java.awt.image.BufferedImage bufferedImage = new java.awt.image.BufferedImage(_bufferWidth, _bufferHeight,
        java.awt.image.BufferedImage.TYPE_INT_ARGB);

    float[] imageData = nativeCreateFloatArrayFromImage(_nativeDefinition, 0, 0, _bufferWidth, _bufferHeight);

    java.awt.image.DataBuffer dataBuffer = new java.awt.image.DataBufferByte(4 * _bufferWidth * _bufferHeight);

    // need to cast the image data to an integer since the Raster cannot handle floats
    for (int i = 0; i < (_bufferWidth * _bufferHeight); ++i)
    {
      dataBuffer.setElem(4 * i, red);
      dataBuffer.setElem(4 * i + 1, green);
      dataBuffer.setElem(4 * i + 2, blue);
      dataBuffer.setElem(4 * i + 3, (int)imageData[i]);
    }

    int scanLineStride = 4 * _bufferWidth;
    int[] bandOffsets = new int[]
                        {0, 1, 2, 3};
    java.awt.Point location = new java.awt.Point(0, 0);
    java.awt.image.Raster raster = java.awt.image.Raster.createInterleavedRaster(dataBuffer,
        _bufferWidth,
        _bufferHeight,
        scanLineStride,
        4,
        bandOffsets,
        location);
    bufferedImage.setData(raster);
    return bufferedImage;
  }

  /**
   * @author Patrick Lacz
   */
  public static float[] createArrayFromImage(Image sourceImage, RegionOfInterest roi)
  {
    Assert.expect(sourceImage != null);
    Assert.expect(roi != null);

    float[] array = nativeCreateFloatArrayFromImage(sourceImage.getNativeDefinition(), roi.getMinX(), roi.getMinY(), roi.getWidth(), roi.getHeight());
    return array;
  }


  /**
   * Get the ImageCoordinate of the pixel closest to the center of the image.
   *
   * @author Peter Esbensen
   */
  public ImageCoordinate getCenterCoordinate()
  {
    return new ImageCoordinate((int)Math.round(_bufferWidth * 0.5),
                               (int)Math.round(_bufferHeight * 0.5));
  }

  /**
   * @author Peter Esbensen
   * @return buffer height in pixels
   */
  public int getHeight()
  {
    return _bufferHeight;
  }

  /**
   * @author Peter Esbensen
   * @return buffer width in pixels
   */
  public int getWidth()
  {
    return _bufferWidth;
  }

  /**
   * @author Patrick Lacz
   */
  public int getStride()
  {
    Assert.expect(_nativeDefinition != null);

    return _nativeDefinition[_STRIDE_INDEX];
  }

  /**
   * @author Patrick Lacz
   */
  public int getCapacity()
  {
    return getStride() * getHeight();
  }


  /**
   * @author Patrick Lacz
   */
  public boolean hasImageDescription()
  {
    return _imageDescription != null;
  }

  /**
   * @author Kay Lannen
   */
  public ImageDescription getImageDescription()
  {
    Assert.expect(_imageDescription != null);

    return _imageDescription;
  }

  /**
   * @author Kay Lannen
   */
  public void setImageDescription(ImageDescription imageDescription)
  {
    Assert.expect(imageDescription != null);

    _imageDescription = imageDescription;
  }

  /**
   * Invasively change the width and height of the image.
   * The new dimensions <b>must</b> have the same size as the old dimensions.
   * This method is being added to support processing 'aggregate' images (aka 3D images) for some algorithms.
   * Do not use this method unless you REALLY know what you're doing.
   *
   * @author Patrick Lacz
   */
  public void setWidthAndHeight(int newWidth, int newHeight, int newStride)
  {
    // check to make certain we're using this method correctly. See the comment above. This is NOT the same as a resize method.
    // feel free to rename this method should you think of a better name.
    Assert.expect(newStride * newHeight == getStride() * _bufferHeight);
    _bufferWidth = newWidth;
    _bufferHeight = newHeight;
    _nativeDefinition[_WIDTH_INDEX] = newWidth;
    _nativeDefinition[_HEIGHT_INDEX] = newHeight;
    _nativeDefinition[_STRIDE_INDEX] = newStride;
  }

  /**
   * Called by the garbage collection system. Do not call.
   * @author Patrick Lacz
   */
  protected void finalize() throws Throwable
  {
    try
    {
      if (_referenceCount != 0 && _wasAllocatedUsingMemoryManager)
      {
        System.out.println("REFERENCE COUNT WARNING: " + _referenceCount + " references. " + _bufferWidth + " x " + _bufferHeight);
        if (_debugAllocationTrace != null)
          _debugAllocationTrace.printStackTrace();
        //  Assert.expect(_referenceCount == 0);
      }
    }
    finally
    {
      super.finalize();
    }
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void incrementReferenceCount()
  {
    Assert.expect(_referenceCount > 0);
    ++_referenceCount;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void decrementReferenceCount()
  {
    --_referenceCount;
    Assert.expect(_referenceCount >= 0);
    if (_referenceCount == 0 && _wasAllocatedUsingMemoryManager)
    {
      // reclaim the resource!
      _nativeMemoryMonitor.free(this);
      _bufferData = null;
    }
  }

  /**
   * Allocate space for the buffer.
   * This method is complicated by the fact that Java's garbage collection isn't guaranteed to have completed by the return of System.gc().
   * So, if we have failed for some reason, we try again (with a small wait).
   * @author Patrick Lacz
   */
  private synchronized void allocate(int width, int height, int nativeMemoryMonitorAllocationMethod)
  {
    Assert.expect(width > 0);
    Assert.expect(height > 0);
    Assert.expect(_nativeDefinition == null);

    _bufferWidth = width;
    _bufferHeight = height;

    _nativeDefinition = _nativeMemoryMonitor.alloc(this, width, height, nativeMemoryMonitorAllocationMethod);

    _wasAllocatedUsingMemoryManager = true;
    _referenceCount = 1;

    if (_KEEP_ALLOCATION_STACK_TRACES_FOR_DEBUGGING)
    {
      Exception e = new Exception("Allocation Stack of Misreferenced Image");
      e.fillInStackTrace();
      _debugAllocationTrace = e;
    }
  }

  /**
   * @author Patrick Lacz
   */
  protected int getByteOffset(int xCoord, int yCoord)
  {
    return yCoord * _nativeDefinition[_STRIDE_INDEX] + xCoord * getBytesPerPixel();
  }


  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   * @author Patrick Lacz
   */
  public float getPixelValue(int xCoord, int yCoord)
  {
    // PWL: the asserts have been combined into one for performance reasons
    Assert.expect(xCoord >= 0 && xCoord < _bufferWidth && yCoord >= 0 && yCoord < _bufferHeight);
    ByteBuffer bufferData = getByteBuffer();
    return bufferData.getFloat(getByteOffset(xCoord, yCoord));
  }

  /**
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  public void setPixelValue(int xCoord, int yCoord, float value)
  {
    // PWL: the asserts have been combined into one for performance reasons
    Assert.expect(xCoord >= 0 && xCoord < _bufferWidth && yCoord >= 0 && yCoord < _bufferHeight);
    ByteBuffer bufferData = getByteBuffer();

    bufferData.putFloat(getByteOffset(xCoord, yCoord), value);
  }

  /**
   * Bulk set a run of of pixels along a row to the same value.
   * @author Patrick Lacz
   */
  public void setPixelRowValues(int xCoordMin, int xCoordMax, int yCoord, float value)
  {
    Assert.expect(xCoordMin >= 0);
    Assert.expect(yCoord >= 0);
    Assert.expect(xCoordMin < _bufferWidth);
    Assert.expect(xCoordMax < _bufferWidth);
    Assert.expect(xCoordMax >= xCoordMin);
    Assert.expect(yCoord < _bufferHeight);
    Assert.expect(getBytesPerPixel() == 4); // float format

    ByteBuffer bufferData = getByteBuffer();

    float fillArray[] = new float[xCoordMax - xCoordMin + 1];
    Arrays.fill(fillArray, value);

    bufferData.position(getByteOffset(xCoordMin, yCoord));
    bufferData.asFloatBuffer().put(fillArray, 0, fillArray.length);
    bufferData.rewind();
  }

  /**
   * This is a debugging utility.
   * @author Patrick Lacz
   *
   */
  public void printImageCSV(String imageName)
  {
    Assert.expect(imageName != null);

    printImageCSV(imageName, System.out);
  }

  /**
   * This is a debugging utility.

   * @author Patrick Lacz
   * @author Matt Wharton
   */
  public void printImageCSV(String imageName, PrintStream outputStream)
  {
    Assert.expect(imageName != null);
    Assert.expect(outputStream != null);

    outputStream.print(imageName);
    for (int x = 0; x < getWidth(); ++x)
    {
      outputStream.print(", " + x);
    }

    outputStream.println();

    for (int y = 0; y < getHeight(); ++y)
    {
      outputStream.print("Row " + y);
      for (int x = 0; x < getWidth(); ++x)
      {
        outputStream.print(", " + getPixelValue(x, y));
      }
      outputStream.println();
    }

  }

  /**
   * This is a debugging utility.
   * @author Sunit Bhalla
   *
   */
  public void printImage(String imageName)
  {
    Assert.expect(imageName != null);
    System.out.println("Printing data for image: " + imageName);
    System.out.println("  Height: " + getHeight());
    System.out.println("  Width: " + getWidth());
    for (int y = 0; y < getHeight(); y++)
    {
      for (int x = 0; x < getWidth(); x++)
      {
        double pixelValue = getPixelValue(x, y);
        System.out.println("  Pixel at (" + x + ", " + y + ") is " +
                           pixelValue);
      }
    }
  }

  /**
   * Do Not use this method in production code! It compares each value of the image, which is very slow.
   * @author Patrick Lacz
   */
  public boolean equals(Image otherImage)
  {
    Assert.expect(otherImage != null);

    if (otherImage.getWidth() != getWidth() || otherImage.getHeight()  != getHeight())
      return false;
    for (int r = 0 ; r < getHeight() ; ++r)
    {
      for (int c = 0 ; c < getWidth() ; ++c)
      {
        boolean pixelCompare = otherImage.getPixelValue(c, r) == getPixelValue(c, r);
        if (pixelCompare == false)
          return false;
      }
    }
    return true;
  }

  /**
   * Do Not use this method in production code! It compares each value of the image, which is very slow.
   * @author Patrick Lacz
   */
  public boolean fuzzyEquals(Image otherImage, float threshold)
  {
    Assert.expect(otherImage != null);

    double doubleThreshold = threshold;

    if (otherImage.getWidth() != getWidth() || otherImage.getHeight()  != getHeight())
      return false;
    for (int r = 0 ; r < getHeight() ; ++r)
    {
      for (int c = 0 ; c < getWidth() ; ++c)
      {
        boolean pixelCompare = MathUtil.fuzzyEquals(otherImage.getPixelValue(c, r), getPixelValue(c, r), doubleThreshold);
        if (pixelCompare == false)
          return false;
      }
    }
    return true;
  }

  /**
   * @author Roy Williams
   */
  public void setPhantomReference(PhantomReference<Image> phantomReference)
  {
    Assert.expect(phantomReference != null);

    _phantomReference = phantomReference;
  }

  /**
   * Get the image data in the form of a java.awt.image.BufferedImage.  This function will take a while because I
   * iterate over all pixels, casting them from a float to an integer.
   *
   * @author Wei Chin
   */
  public java.awt.image.BufferedImage get32BitBufferedImage()
  {
    java.awt.image.DataBuffer dataBuffer = null;

    if (getBytesPerPixel() == 4)
    {
      float[] floatArray = nativeCreateFloatArrayFromImage(_nativeDefinition, 0, 0, _bufferWidth, _bufferHeight);
      dataBuffer = new DataBufferFloat(floatArray, floatArray.length);
    }
    else
    {
      Assert.expect(false); // only float and byte formats supported.
    }
    int scanLineStride = _bufferWidth;
    int[] bandOffsets = new int[]
                        {0}; // ditto
    java.awt.Point location = new java.awt.Point(0, 0);

    ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_GRAY);
    int[] nBits = {16};
    java.awt.image.ColorModel colorModel = new ComponentColorModel(cs, nBits, false, true,
                                         Transparency.OPAQUE,
                                         DataBuffer.TYPE_FLOAT);
    SampleModel sm = new ComponentSampleModel(DataBuffer.TYPE_FLOAT, _bufferWidth, _bufferHeight, 1, scanLineStride, bandOffsets);
    WritableRaster raster = Raster.createWritableRaster(sm, dataBuffer, location);

    java.awt.image.BufferedImage bufferedImage = new java.awt.image.BufferedImage(colorModel, raster, false, null);
    return bufferedImage;
  }

  /**
   * @author Roy Williams
   */
  public PhantomReference<Image> getPhantomReference()
  {
    return _phantomReference;
  }

  /**
   * @author Patrick Lacz
   */
  static native void nativeIntializeJavaDefinition(int[] image, ByteBuffer directByteBuffer);

  /**
   * Returns the size of the int array to be used for the native definitions.
   * @author Patrick Lacz
   */
  private static native int nativeGetSizeOfNativeDefinitionInInts();

  /**
   * @author Patrick Lacz
   */
  private static native void nativeConvertByteToFloatImage(ByteBuffer byteBuffer, int bufferWidth, int roiOffset, int[] sourceImage);

  /**
   * @author Patrick Lacz
   */
  private static native float[] nativeCreateFloatArrayFromImage(int[] sourceImage, int roiX, int roiY, int roiWidth, int roiHeight);

  /**
   * @author Patrick Lacz
   */
  private static native byte[] nativeCreateByteArrayFromImage(int[] sourceImage, int roiX, int roiY, int roiWidth, int roiHeight);

  /**
   * @author Patrick Lacz
   */
  private static native void nativeSetImageFromArray(int[] destImage, float sourceArray[]);

  /**
   * @author Patrick Lacz
   */
  private static native void nativeSetImageFromByteArray(int[] destImage, byte sourceArray[]);

  /**
   * @author Patrick Lacz
   */
  private static native ByteBuffer nativeGetByteBuffer(int[] image);
}
