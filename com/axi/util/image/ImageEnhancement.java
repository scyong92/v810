package com.axi.util.image;

import java.util.Collection;

import com.axi.util.*;


/**
 * Routines to be used for preparing or modifying images in specific ways. These methods are often aggregates of several other util.image calls.
 *
 * @author Patrick Lacz
 */
public class ImageEnhancement
{
  /**
   * @author Patrick Lacz
   */
  public ImageEnhancement()
  {
    // do nothing, this is a static class
  }

  /**
   * This version of enhanceContrast attempts to avoid noisy pixels contributing to the range.
   * It defines it range to be the 2.5 percentile and the 97.5 percentile of all the pixels.
   *
   * @author Patrick Lacz
   */
  public static void autoEnhanceContrastInPlace(Image image)
  {
    Assert.expect(image != null);

    autoEnhanceContrastInPlace(image, RegionOfInterest.createRegionFromImage(image));
  }

  private static final float _AUTO_ENHANCE_RANGE_MAX = 0.95f;
  private static final float _AUTO_ENHANCE_RANGE_MIN = 0.05f;
  private static final float _AUTO_ENHANCE_IGNORE_PERCENT = 0.0f; // Jack Hwee- previously ignores .5%, not half, now preserved all details of the image
  private static final float _AUTO_ENHANCE_MAXIMUM_BLACK_LEVEL = 0.3f;
  private static final float _AUTO_ENHANCE_MINIMUM_WHITE_LEVEL = 0.7f;

  private static final String _ALPHA_DESCRIPTION_KEYNAME = "autoEnhanceAlpha";
  private static final String _BETA_DESCRIPTION_KEYNAME = "autoEnhanceBeta";

  /**
   * This version of enhanceContrast attempts to avoid noisy pixels contributing to the range.
   * It defines it range to be the 0.5 percentile and the 99.5 percentile of all the pixels.
   *
   * @author Patrick Lacz
   */
  public static void autoEnhanceContrastInPlace(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));

    int imageHistogram[] = Threshold.histogram(image, roi, 256);
    Assert.expect(imageHistogram.length == 256);

    // ignore black pixels
    imageHistogram[0] = 0;

    // This method takes the fraction, not actually the percent.
    float lowThreshold = Threshold.getPixelValueAtPercentileUsingHistogram(imageHistogram, _AUTO_ENHANCE_IGNORE_PERCENT, 0, 255);
    float highThreshold = Threshold.getPixelValueAtPercentileUsingHistogram(imageHistogram, 100.f - _AUTO_ENHANCE_IGNORE_PERCENT, 0, 255);
    // @todo make version of estimatePixelValueAtPercentileUsingHistogram that starts from the top with high %s

    // we don't want to stretch the contrast too much for cases where there's only a little data. It looks grainy and you lose
    // the information that it is shaded, etc. So, we establish a maximum to how far we move black, and a minimum to how low we move white.
    lowThreshold = Math.min(lowThreshold, _AUTO_ENHANCE_MAXIMUM_BLACK_LEVEL * 255.f);
    highThreshold = Math.max(highThreshold, _AUTO_ENHANCE_MINIMUM_WHITE_LEVEL * 255.f);

    // avoid divide by zero
    if (MathUtil.fuzzyEquals(lowThreshold, highThreshold, 0.00001))
      return;

    Assert.expect(_AUTO_ENHANCE_RANGE_MAX > _AUTO_ENHANCE_RANGE_MIN);
    Assert.expect(_AUTO_ENHANCE_RANGE_MAX > 0.0f && _AUTO_ENHANCE_RANGE_MAX <= 1.0f);
    Assert.expect(_AUTO_ENHANCE_RANGE_MIN >= 0.0f && _AUTO_ENHANCE_RANGE_MIN < 1.0f);

    final float preSqrtCeiling = _AUTO_ENHANCE_RANGE_MAX * _AUTO_ENHANCE_RANGE_MAX;
    final float preSqrtFloor = _AUTO_ENHANCE_RANGE_MIN * _AUTO_ENHANCE_RANGE_MIN;

    float range_size = preSqrtCeiling - preSqrtFloor;

    Arithmetic.addConstantInPlace(image, roi, -lowThreshold);
    float alpha = range_size/(highThreshold-lowThreshold);
    Arithmetic.multiplyByConstantInPlace(image, roi, alpha);
    Threshold.clamp(image, roi, 0.f, range_size);
    Arithmetic.addConstantInPlace(image, roi, preSqrtFloor);
    Arithmetic.squareRootInPlace(image, roi);
    Arithmetic.multiplyByConstantInPlace(image, roi, 255.f);

    // the operation should be reversable if the whole image is being enhanced
    if (RegionOfInterest.createRegionFromImage(image).fitsWithinRegionOfInterest(roi))
    {
      if (image.hasImageDescription() == false)
        image.setImageDescription(new ImageDescription(""));
      image.getImageDescription().setParameter(_ALPHA_DESCRIPTION_KEYNAME, alpha);
      image.getImageDescription().setParameter(_BETA_DESCRIPTION_KEYNAME, lowThreshold);

    }
  }

  /**
   * @author Patrick Lacz
   */
  public static void reverseAutoEnhanceContrastInPlace(Image image)
  {
    // we can only reverse the contrast enhancement if the whole image was modified.
    Assert.expect(image != null);
    if (image.hasImageDescription() == false)
      return;
    ImageDescription imageDescription = image.getImageDescription();

    if (imageDescription.hasParameter(_ALPHA_DESCRIPTION_KEYNAME) && imageDescription.hasParameter(_BETA_DESCRIPTION_KEYNAME))
    {
      float alpha = ((Number)imageDescription.getParameter(_ALPHA_DESCRIPTION_KEYNAME)).floatValue();
      float beta = ((Number)imageDescription.getParameter(_BETA_DESCRIPTION_KEYNAME)).floatValue();
      final float preSqrtFloor = _AUTO_ENHANCE_RANGE_MIN * _AUTO_ENHANCE_RANGE_MIN;

      RegionOfInterest roi = RegionOfInterest.createRegionFromImage(image);
      Arithmetic.multiplyByConstantInPlace(image, roi, 1.f/255.f);
      Arithmetic.squareInPlace(image, roi);
      Arithmetic.addConstantInPlace(image, roi, -preSqrtFloor);
      Arithmetic.multiplyByConstantInPlace(image, roi, 1.f/alpha);
      Arithmetic.addConstantInPlace(image, roi, beta);
    }
  }

  /**
   * This version of enhanceContrast attempts to avoid noisy pixels contributing to the range.
   * It defines it range to be the 0.5 percentile and the 99.5 percentile of all the pixels.
   *
   * This method operates on a set of images, applying the least restrictive change to all the images in the set.
   *
   * @author Patrick Lacz
   */
  public static void autoEnhanceContrastInPlace(Collection<Image> images)
  {
    Assert.expect(images != null);

    if (images.isEmpty())
      return;

    // we don't want to stretch the contrast too much for cases where there's only a little data. It looks grainy and you lose
    // the information that it is shaded, etc. So, we establish a maximum to how far we move black, and a minimum to how low we move white.
    float minOfLowThresholds = _AUTO_ENHANCE_MAXIMUM_BLACK_LEVEL * 255.f;
    float maxOfHighThresholds = _AUTO_ENHANCE_MINIMUM_WHITE_LEVEL * 255.f;

    for (Image image : images)
    {
      RegionOfInterest roi = RegionOfInterest.createRegionFromImage(image);

      int imageHistogram[] = Threshold.histogram(image, roi, 256);
      Assert.expect(imageHistogram.length == 256);

      // ignore black pixels
      imageHistogram[0] = 0;

      // This method takes the fraction, not actually the percent.
      float lowThreshold = Threshold.getPixelValueAtPercentileUsingHistogram(imageHistogram, _AUTO_ENHANCE_IGNORE_PERCENT, 0, 255);
      float highThreshold = Threshold.getPixelValueAtPercentileUsingHistogram(imageHistogram, 100.f - _AUTO_ENHANCE_IGNORE_PERCENT, 0, 255);

      minOfLowThresholds = Math.min(lowThreshold, minOfLowThresholds);
      maxOfHighThresholds = Math.max(highThreshold, maxOfHighThresholds);
    }

    // avoid divide by zero
    if (MathUtil.fuzzyEquals(minOfLowThresholds, maxOfHighThresholds, 0.00001))
      return;

    Assert.expect(_AUTO_ENHANCE_RANGE_MAX > _AUTO_ENHANCE_RANGE_MIN);
    Assert.expect(_AUTO_ENHANCE_RANGE_MAX > 0.0f && _AUTO_ENHANCE_RANGE_MAX <= 1.0f);
    Assert.expect(_AUTO_ENHANCE_RANGE_MIN >= 0.0f && _AUTO_ENHANCE_RANGE_MIN < 1.0f);

    final float preSqrtCeiling = _AUTO_ENHANCE_RANGE_MAX * _AUTO_ENHANCE_RANGE_MAX;
    final float preSqrtFloor = _AUTO_ENHANCE_RANGE_MIN * _AUTO_ENHANCE_RANGE_MIN;

    float range_size = preSqrtCeiling - preSqrtFloor;

    for (Image image : images)
    {
      RegionOfInterest roi = RegionOfInterest.createRegionFromImage(image);

      Arithmetic.addConstantInPlace(image, roi, -minOfLowThresholds);
      float alpha = range_size / (maxOfHighThresholds - minOfLowThresholds);
      Arithmetic.multiplyByConstantInPlace(image, roi, alpha);
      Threshold.clamp(image, roi, 0.f, range_size);
      Arithmetic.addConstantInPlace(image, roi, preSqrtFloor);
      Arithmetic.squareRootInPlace(image, roi);
      Arithmetic.multiplyByConstantInPlace(image, roi, 255.f);

      // the operation should be reversable if the whole image is being enhanced
      if (RegionOfInterest.createRegionFromImage(image).fitsWithinRegionOfInterest(roi))
      {
        if (image.hasImageDescription() == false)
          image.setImageDescription(new ImageDescription(""));
        image.getImageDescription().setParameter(_ALPHA_DESCRIPTION_KEYNAME, alpha);
        image.getImageDescription().setParameter(_BETA_DESCRIPTION_KEYNAME, minOfLowThresholds);
      }
    }
  }

  /**
   * This version, which reports back to the caller the range of the data, works from the Minimum and Maximum of the
   * pixel data range.
   * @author Patrick Lacz
   */
  public static void enhanceContrastInPlace(Image image, RegionOfInterest roi, FloatRef minimumOfRange, FloatRef maximumOfRange)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(roi.fitsWithinImage(image));
    Assert.expect(minimumOfRange != null);
    Assert.expect(maximumOfRange != null);

    float minValue = Statistics.minValue(image, roi);
    float maxValue = Statistics.maxValue(image, roi);
    minimumOfRange.setValue(minValue);
    maximumOfRange.setValue(maxValue);

    // avoid divide by zero
    if (MathUtil.fuzzyEquals(minValue, maxValue, 0.00001))
      maxValue += .5f;

    Arithmetic.addConstantInPlace(image, roi, -minValue);
    Arithmetic.multiplyByConstantInPlace(image, roi, 255.f/(minValue-maxValue));
  }

}
