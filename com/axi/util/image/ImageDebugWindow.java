package com.axi.util.image;

import javax.swing.*;

import com.axi.util.*;

/**
 * A Utility class designed to help with debugging algorithms and procedures that work with Image objects.
 *
 * WARNING!!! Methods on this class should be used for debuging purposes only!
 * Do not check in code that uses these methods.
 *
 * @author Patrick Lacz
 */
public class ImageDebugWindow
{
  public ImageDebugWindow()
  {
    // static class, nothing to do
  }

  /**
   * This is a private helper class to watch the Window until it has been closed.
   * Use the waitUntilClosed method to delay a thread until an interrupt is received or the window is closed.
   *
   * @author Patrick Lacz
   */
  static private class BlockUntilWindowClosedListener implements java.awt.event.WindowListener
  {
    public synchronized void waitUntilWindowClosed()
    {
      try
      {
        wait();
      }
      catch (InterruptedException ex)
      {
      }
    }

    // Flip the boolean indicating the the window has closed.
    public synchronized void windowClosed(java.awt.event.WindowEvent we)
    {
      notifyAll();
    }

    // Do nothing for other window events
    public void windowDeactivated(java.awt.event.WindowEvent we)
    {}

    public void windowActivated(java.awt.event.WindowEvent we)
    {}

    public void windowDeiconified(java.awt.event.WindowEvent we)
    {}

    public void windowOpened(java.awt.event.WindowEvent we)
    {}

    public void windowClosing(java.awt.event.WindowEvent we)
    {}

    public void windowIconified(java.awt.event.WindowEvent we)
    {}
  };

  /**
   * Show a debug window containing a com.axi.util.image.Image and block until the window is closed.
   * WARNING : This method should only be called to assist in debugging code.
   *
   * @author Patrick Lacz
   */
  static public void displayAndBlock(Image imageToDisplay, String windowTitle)
  {
    Assert.expect(imageToDisplay != null);
    Assert.expect(windowTitle != null);

    final java.awt.image.BufferedImage awtImage = imageToDisplay.getBufferedImage();

    final int awtImageWidth = imageToDisplay.getWidth();
    final int awtImageHeight = imageToDisplay.getHeight();

    displayAndBlock(awtImage, awtImageWidth, awtImageHeight, windowTitle);
  }

  /**
   * Show a debug window containing a java.awt.Image and block until the window is closed.
   * WARNING : This method should only be called to assist in debugging code.
   *
   * @author Patrick Lacz
   */
  static public void displayAndBlock(final java.awt.image.BufferedImage awtImage, final int awtImageWidth, final int awtImageHeight,
                                     String windowTitle)
  {
    Assert.expect(awtImage != null);
    Assert.expect(windowTitle != null);

    final int windowWidth = Math.max(awtImageWidth, 200);
    final int windowHeight = Math.max(awtImageHeight + 50, 100);

    JFrame windowFrame = new JFrame(windowTitle);

    windowFrame.setSize(windowWidth, windowHeight);

    windowFrame.add(new JComponent()
    {
      public void paint(java.awt.Graphics g)
      {
        // Center the image
        g.drawImage(awtImage, (windowWidth - awtImageWidth) / 2, 10 + (windowHeight - awtImageHeight - 50) / 2, this);
      }
    });

    windowFrame.setDefaultCloseOperation(javax.swing.JFrame.DISPOSE_ON_CLOSE);

    // This listener is defined privately above.
    BlockUntilWindowClosedListener imageWindowListener = new BlockUntilWindowClosedListener();
    windowFrame.addWindowListener(imageWindowListener);

    windowFrame.setVisible(true);

    imageWindowListener.waitUntilWindowClosed();
  }
}
