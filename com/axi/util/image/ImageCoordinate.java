package com.axi.util.image;

import java.io.*;

import com.axi.util.*;

/**
 * Represents a coordinate in image space.  The origin is the upper left corner of the image.
 * The x axis grows to the right and the y axis grows down.
 *
 * @author Tony Turner
 * @author Matt Wharton
 */
public class ImageCoordinate extends IntCoordinate implements Serializable, Comparable<ImageCoordinate>
{
  /**
   * Copy constructor.
   *
   * @author Matt Wharton
   */
  public ImageCoordinate(ImageCoordinate rhs)
  {
    super(rhs);
  }

  /**
   * Constructor.
   *
   * @param x the x value of this coordinate.
   * @param y the y value of this coordinate.
   * @author Matt Wharton
   */
  public ImageCoordinate(int x, int y)
  {
    super(x, y);
  }

  /**
   * Constructor taking a DoubleCoordinate.  Will round this to the nearest integer.
   *
   * @author Peter Esbensen
   */
  public ImageCoordinate(DoubleCoordinate coord)
  {
    super((new Long(Math.round(coord.getX()))).intValue(),
          (new Long(Math.round(coord.getY()))).intValue());
  }

  /**
   * @return true if neither the x nor the y coordinate is negative
   * @author Peter Esbensen
   */
  public boolean isNotNegative()
  {
    return ((getX() >= 0) && (getY() >= 0));
  }

  /**
   * @return true if the point is a pixel that falls within the specified image
   * @author Patrick Lacz
   */
  public boolean isInsideImage(Image image)
  {
    Assert.expect(image != null);
    return (getX() >= 0 && getX() < image.getWidth() &&
            getY() >= 0 && getY() < image.getHeight());
  }

  /**
   * @author Matt Wharton
   */
  private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
  {
    is.defaultReadObject();
    deserializePoint(is);
  }

  /**
   * @author Matt Wharton
   */
  private void writeObject(ObjectOutputStream os) throws IOException
  {
    os.defaultWriteObject();
    serializePoint(os);
  }

  /**
   * @author Patrick Lacz
   */
  public int compareTo(ImageCoordinate other)
  {
    int xComparison = getX() - other.getX();
    if (xComparison == 0)
    {
      return getY() - other.getY();
    }
    return xComparison;
  }
}
