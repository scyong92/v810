package com.axi.util.image;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;


/**
 * @author Kay Lannen
 * @author Patrick Lacz
 */

public class ImageDescription
{
  private Map<String, Object> _parameterKeyToValueMap = new HashMap<String, Object>();
  private static final Pattern _parameterPattern = Pattern.compile("(\\w+)=(?:([\\w\\.\\+\\-]+)|\"([^\"\n]+)\")");
  private static final Pattern _listPattern = Pattern.compile("(\\w+)=\\[(.*?)\\]");
  private static final NumberFormat _numberFormat = NumberFormat.getInstance(Locale.US);

  /**
   * @author Patrick Lacz
   */
  public ImageDescription(String stringDescription)
  {
    Assert.expect(stringDescription != null);
    Assert.expect(_parameterPattern != null);

    Matcher matcher = _parameterPattern.matcher(stringDescription);
    matcher.reset();
    while (matcher.find())
    {
      Assert.expect(matcher.groupCount()==3);
      String keyString = matcher.group(1);
      String valueString = matcher.group(2); // may return null if we did not match the single word pattern
      if (valueString != null)
      {
        try
        {
          Number valueNumber = _numberFormat.parse(valueString);
          setParameter(keyString, valueNumber);
        }
        catch (NumberFormatException e)
        {
          // do nothing
        }
        catch (ParseException ex)
        {
          // just treat as a string
          setParameter(keyString, valueString);
        }
      }
      else
      {
        valueString = matcher.group(3);
        setParameter(keyString, valueString);
      }
    }
    
    // Chnee Khang Wah, 2013-02-20, read Sharpness profile from description
    // This match the expression as below:
    // Key = [...]
    // everything wihtin the square bracket export as string
    matcher = _listPattern.matcher(stringDescription);
    matcher.reset();
    while (matcher.find())
    {
      String keyString = matcher.group(1);
      String valueString = matcher.group(2); // may return null if we did not match the single word pattern
      if (valueString != null)
      {
        try
        {
          setParameter(keyString, valueString);
        }
        catch (NumberFormatException e)
        {
          // do nothing
        }
      }
    }
   }

   /**
    * @author Patrick Lacz
    */
   public String createStringDescription()
   {
     String outputString = "";
     for (Map.Entry<String, Object> entry :  _parameterKeyToValueMap.entrySet())
     {
       outputString += entry.getKey() + "=";
       if (entry.getValue() instanceof String)
         outputString += "\"" + (String)entry.getValue() + "\"";
       else if (entry.getValue() instanceof List) // Chnee Khang Wah, 2013-02-19
       {
         outputString += entry.getValue().toString();
       }
       else
         outputString += entry.getValue().toString();

       outputString += "\n";
     }
     return outputString;
   }

  /**
   * @author Patrick Lacz
   */
  public boolean hasParameter(String key)
  {
    Assert.expect(key != null);
    Assert.expect(_parameterKeyToValueMap != null);

   return (_parameterKeyToValueMap.containsKey(key));
  }

  /**
   * @author Patrick Lacz
   */
  public Object getParameter(String key)
  {
    Assert.expect(key != null);
    Assert.expect(_parameterKeyToValueMap != null);

    Assert.expect(_parameterKeyToValueMap.containsKey(key));
    return _parameterKeyToValueMap.get(key);
  }

  /**
   * @author Patrick Lacz
   */
  public void setParameter(String key, Object value)
  {
    Assert.expect(key != null);
    Assert.expect(value != null);
    Assert.expect(_parameterKeyToValueMap != null);

    // if you want to store other values, figure out how it will work with the parsing
    Assert.expect(value instanceof String || value instanceof Number || value instanceof List);

    _parameterKeyToValueMap.put(key, value);
  }

}
