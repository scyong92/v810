package com.axi.util.image;

import java.io.*;

import com.axi.util.*;
import java.awt.Shape;

/**
 * This class extends the functionality in IntRectangle.  The behavior is largely the same, but this
 * class uses "screen coordinates" (y grows down instead of up).  The unit is, of course, pixels.
 *
 * @author Matt Wharton
 */
public class ImageRectangle extends IntRectangle implements Serializable
{
  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  public ImageRectangle(int topLeftX, int topLeftY, int width, int height)
  {
    super(topLeftX, topLeftY, width, height);
  }


  /**
   * Copy constructor.
   *
   * @author Matt Wharton
   */
  public ImageRectangle(ImageRectangle rhs)
  {
    super(rhs);
  }

  /**
   * @author Peter Esbensen
   */
  public ImageRectangle(Shape shape)
  {
    super(shape);
  }

  /**
   * For an image, the maximum pixel is the pixel before the minX + width.
   * ie, a 8x1 region with a lower-left coordinate of (0,0) has an upper right
   *  coordinate of (7,0).
   * @author Patrick Lacz
   */
  public int getMaxX()
  {
    return getMinX() + getWidth() - 1;
  }

  /**
   * @author Patrick Lacz
   */
  public int getMaxY()
  {
    return getMinY() + getHeight() - 1;
  }

  /**
   * @author Roy Williams
   */
  public String toString()
  {
    String space = " ";
    return space + getMinX()  +
           space + getMinY()  +
           space + getWidth() +
           space + getHeight();
  }
}
