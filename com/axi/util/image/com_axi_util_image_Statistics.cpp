#include "com_axi_util_image_Statistics.h"
#include "com/axi/util/jniUtil.h"
#include "sptAssert.h"

#include "ippcore.h"
#include "ippi.h"
#include "ipps.h"
#include "ippcv.h" // ippiMean_StDev_32f_C1R

#include "jniImageUtil.h"

/**
 * nativeSumOfPixels
 *
 * @author Patrick Lacz
 */
JNIEXPORT jdouble JNICALL Java_com_axi_util_image_Statistics_nativeSumOfPixels(JNIEnv *jEnv, 
                                                                                   jclass jContext, 
                                                                                   jintArray jSourceImage, 
                                                                                   jint offsetX, 
                                                                                   jint offsetY, 
                                                                                   jint roiWidth, 
                                                                                   jint roiHeight)
{ 
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize ippiRoiSize = { roiWidth, roiHeight };
  
    Ipp64f sum;
    IppStatus ippResult = ippiSum_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),
                                          pSourceImage->stride, 
                                          ippiRoiSize,
                                          &sum,										// returns a single Ipp64f
                                          ippAlgHintAccurate);						// ippAlgHintAccurate or ippAlgHintFast
    
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    return static_cast<double>(sum);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * nativeMean
 *
 * Apply a mean filter with an appropriately sized window.
 *
 * @author Patrick Lacz
 */
JNIEXPORT jfloat JNICALL Java_com_axi_util_image_Statistics_nativeMean(JNIEnv *jEnv, 
                                                                           jclass jContext, 
                                                                           jintArray jSourceImage, 
                                                                           jint roiMinX, 
                                                                           jint roiMinY, 
                                                                           jint roiWidth, 
                                                                           jint roiHeight)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize ippiRoiSize = { roiWidth, roiHeight };
  
    Ipp64f mean;
    IppStatus ippResult = ippiMean_32f_C1R(pSourceImage->addressAt(roiMinX, roiMinY),
                                           pSourceImage->stride,
                                           ippiRoiSize,
                                           &mean,										// returns a single Ipp64f
                                           ippAlgHintAccurate);						// ippAlgHintAccurate or ippAlgHintFast
    
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    return static_cast<float>(mean);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * nativeMeanStdDev
 *
 * Find the mean and Standard Deviation.
 *
 * Returns nothing, results are passed back via DoubleRef's.
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Statistics_nativeMeanStdDev(JNIEnv *jEnv, 
                                                                               jclass jContext, 
                                                                               jintArray jSourceImage, 
                                                                               jint roiMinX, 
                                                                               jint roiMinY, 
                                                                               jint roiWidth, 
                                                                               jint roiHeight, 
                                                                               jobject meanRef, 
                                                                               jobject stdDevRef)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize ippiRoiSize = { roiWidth, roiHeight };
    
    Ipp64f mean, stdDev;
    IppStatus ippResult = ippiMean_StdDev_32f_C1R(pSourceImage->addressAt(roiMinX, roiMinY),
                                                  pSourceImage->stride,
                                                  ippiRoiSize,
                                                  &mean, 
                                                  &stdDev);
    
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    
    doubleRefSetValue(jEnv, 0, meanRef, mean);
    doubleRefSetValue(jEnv, 0, stdDevRef, stdDev);
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}


/**
 * nativeMax
 *
 * Apply a max filter with an appropriately sized window.
 *
 * Returns {x, y}
 * @author Patrick Lacz
 */
JNIEXPORT jintArray JNICALL Java_com_axi_util_image_Statistics_nativeMax(JNIEnv *jEnv, 
                                                                             jclass jContext, 
                                                                             jintArray jSourceImage, 
                                                                             jint roiMinX, 
                                                                             jint roiMinY, 
                                                                             jint roiWidth, 
                                                                             jint roiHeight)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize ippiRoiSize = { roiWidth, roiHeight };
    
    jintArray resultXY = jEnv->NewIntArray(2);
    int *pResult = reinterpret_cast<int*>(jEnv->GetPrimitiveArrayCritical(resultXY, 0));
    float result = 0.f;
    
    IppStatus ippResult = ippiMaxIndx_32f_C1R(pSourceImage->addressAt(roiMinX, roiMinY),
                                              pSourceImage->stride,
                                              ippiRoiSize,
                                              &result, 
                                              &pResult[0], 
                                              &pResult[1]);			// returns a Ipp32f with the maximum value, x, y coordinates
    
    pResult[0] += roiMinX;
    pResult[1] += roiMinY;
    jEnv->ReleasePrimitiveArrayCritical(resultXY, pResult, 0);
    
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    return resultXY;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return NULL;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return NULL;
  }
}

/**
 * nativeMin
 *
 * Apply a min filter with an appropriately sized window.
 *
 * @author Patrick Lacz
 */
JNIEXPORT jintArray JNICALL Java_com_axi_util_image_Statistics_nativeMin(JNIEnv *jEnv, 
                                                                             jclass jContext, 
                                                                             jintArray jSourceImage, 
                                                                             jint roiMinX, 
                                                                             jint roiMinY, 
                                                                             jint roiWidth, 
                                                                             jint roiHeight)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize ippiRoiSize = { roiWidth, roiHeight };
    
    jintArray resultXY = jEnv->NewIntArray(2);
    int *pResult = reinterpret_cast<int*>(jEnv->GetPrimitiveArrayCritical(resultXY, 0));
    jfloat result = 0.f;
    
    IppStatus ippResult = ippiMinIndx_32f_C1R(pSourceImage->addressAt(roiMinX, roiMinY),
                                              pSourceImage->stride,
                                              ippiRoiSize,
                                              &result, 
                                              &pResult[0], 
                                              &pResult[1]);			// returns a Ipp32f with the minimum value, x, y coordinates
    
    pResult[0] += roiMinX;
    pResult[1] += roiMinY;
    jEnv->ReleasePrimitiveArrayCritical(resultXY, pResult, 0);
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    return resultXY;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return NULL;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return NULL;
  }
}


/**
 * nativeSortAscend
 *
 * this method works with strided memory, but is much better when passed contiguous memory.
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Statistics_nativeSortAscending(JNIEnv *jEnv, 
                                                                                  jclass jContext, 
                                                                                  jintArray jSourceImage)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    int length = pSourceImage->width * pSourceImage->height;
    
    if (pSourceImage->stride == pSourceImage->width * sizeof(Ipp32f))
    {
      IppStatus ippResult = ippsSortAscend_32f_I(pSourceImage->address, length);
      sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    }
    else
    {
      Ipp32f *pContiguousBuffer = ippsMalloc_32f(length);
      IppStatus ippResult = ippiCopy_32f_C1R(pSourceImage->address, 
                                             pSourceImage->stride,
                                             pContiguousBuffer, 
                                             pSourceImage->width * sizeof(Ipp32f), 
                                             pSourceImage->getSize());
      sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
      
      ippResult = ippsSortAscend_32f_I(pContiguousBuffer, length);
      sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
      
      ippResult = ippiCopy_32f_C1R(pContiguousBuffer, 
                                   pSourceImage->width * sizeof(Ipp32f), 
                                   pSourceImage->address, 
                                   pSourceImage->stride,
                                   pSourceImage->getSize());
      sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
      ippiFree(pContiguousBuffer);
    }
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeNormalizedCentralMoments
 * mode = 0 : let algorithm choose mode
 *        1 : choose fast execution
 *        2 : choose accurate execution
 */
JNIEXPORT jdoubleArray JNICALL Java_com_axi_util_image_Statistics_nativeNormalizedCentralMoments(JNIEnv *jEnv, 
                                                                                                     jclass jContext, 
                                                                                                     jintArray jSourceImage, 
                                                                                                     jint offsetX, 
                                                                                                     jint offsetY, 
                                                                                                     jint roiWidth, 
                                                                                                     jint roiHeight, 
                                                                                                     jint order, 
                                                                                                     jint mode)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    // the number of moments computed is Sum_{x=0}^{order + 1} x
    // this is just that closed form solution.
    int numberOfMomentsToCompute = (order + 1) * (order + 2) / 2;
    sptAssert(numberOfMomentsToCompute > 0);
    
    jdoubleArray resultArray = jEnv->NewDoubleArray(numberOfMomentsToCompute);
    
    IppiSize roiSize = { roiWidth, roiHeight };
    
    IppiMomentState_64f *pMomentState = NULL;
    
    // choose the appropriate execution mode
    // see the function comments for these values
    IppHintAlgorithm hint = ippAlgHintNone;
    if (mode == 1)
      hint = ippAlgHintFast;
    else if (mode == 2)
      hint = ippAlgHintAccurate;
    
    IppStatus ippResult = ippiMomentInitAlloc_64f(&pMomentState, hint);
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    
    ippResult = ippiMoments64f_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),
                                       pSourceImage->stride,
                                       roiSize, 
                                       pMomentState);
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    
    
    Ipp64f *pResult = reinterpret_cast<Ipp64f*>(jEnv->GetPrimitiveArrayCritical(resultArray, 0));
    
    int numberOfMomentsComputed = 0; 
    for (int o = 0 ; o <= order ; ++o)
    {
      for (int i = 0 ; i <= o ; ++i)
      {
        ippResult = ippiGetNormalizedCentralMoment_64f(pMomentState, 
                                                       o-i, 
                                                       i, 
                                                       0, 
                                                       &pResult[numberOfMomentsComputed]);
        
        if (ippResult == ippStsMoment00ZeroErr)
        {
          // the image is probably all black, don't assert, but just return something reasonable.
          pResult[numberOfMomentsComputed] = 0.0;
          ippResult = ippStsNoErr;
        }
        sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
        ++numberOfMomentsComputed;
      }
    }
    
    ippiMomentFree_64f(pMomentState);
    jEnv->ReleasePrimitiveArrayCritical(resultArray, reinterpret_cast<void*>(pResult), 0);
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    
    return resultArray;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return NULL;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return NULL;
  }
}

/**
 * nativeHuMoments
 * mode = 0 : let algorithm choose mode
 *        1 : choose fast execution
 *        2 : choose accurate execution
 */
JNIEXPORT jdoubleArray JNICALL Java_com_axi_util_image_Statistics_nativeHuMoments(JNIEnv *jEnv, 
                                                                                      jclass jContext, 
                                                                                      jintArray jSourceImage, 
                                                                                      jint offsetX, 
                                                                                      jint offsetY, 
                                                                                      jint roiWidth, 
                                                                                      jint roiHeight, 
                                                                                      jint mode)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    jdoubleArray resultArray = jEnv->NewDoubleArray(7); // There are seven Hu Moments
    IppiSize roiSize = { roiWidth, roiHeight };
    IppiMomentState_64f *pMomentState = NULL;
    
    // choose the appropriate execution mode
    // see the function comments for these values
    IppHintAlgorithm hint = ippAlgHintNone;
    if (mode == 1)
      hint = ippAlgHintFast;
    else if (mode == 2)
      hint = ippAlgHintAccurate;
    
    IppStatus ippResult = ippiMomentInitAlloc_64f(&pMomentState, hint);
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
    
    ippResult = ippiMoments64f_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),
                                       pSourceImage->stride,
                                       roiSize, 
                                       pMomentState);
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
    
    IppiHuMoment_64f huMomentArray;
    ippResult = ippiGetHuMoments_64f(pMomentState, 0, huMomentArray);
    
    ippiMomentFree_64f(pMomentState);
    
    // copy the moment array into the return double array
    jdouble *pResult = reinterpret_cast<jdouble*>(jEnv->GetPrimitiveArrayCritical(resultArray, 0));
    for (int i = 0 ; i < 7 ; ++i)
      pResult[i] = huMomentArray[i];
    jEnv->ReleasePrimitiveArrayCritical(resultArray, reinterpret_cast<void*>(pResult), 0);
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    return resultArray;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return NULL;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return NULL;
  }
}
