package com.axi.util.image;

import com.axi.util.*;

/**
 * @author Matt Wharton
 */
public class MatchTemplateTechniqueEnum extends com.axi.util.Enum
{
  // WARNING: If you change any of these enum values, you must also be sure to update the corresponding values in
  //          com/axi/util/image/com_axi_util_image_Filter.cpp.
  public static final MatchTemplateTechniqueEnum SUM_SQUARED_DISTANCES = new MatchTemplateTechniqueEnum(0);
  public static final MatchTemplateTechniqueEnum CROSS_CORRELATION_NO_PIXEL_VALUE_LEVELING = new MatchTemplateTechniqueEnum(1);
  public static final MatchTemplateTechniqueEnum CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING = new MatchTemplateTechniqueEnum(2);

  /**
   * @author Matt Wharton
   */
  private MatchTemplateTechniqueEnum(int id)
  {
    super(id);
  }
}
