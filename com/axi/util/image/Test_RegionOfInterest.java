package com.axi.util.image;

import com.axi.util.*;
import java.io.PrintWriter;
import com.axi.util.UnitTest;
import java.io.BufferedReader;

/**
 * @author Peter Esbensen
 */
class Test_RegionOfInterest extends UnitTest
{
  /**
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_RegionOfInterest());
  }

  /**
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testBadRegionOfInterests();
    testCreateCircle();

    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder( 10, 10, 20, 20, 0, RegionShapeEnum.RECTANGULAR );
    Expect.expect(roi.getMinX() == 10);
    Expect.expect(roi.getMinY() == 10);
    int width = roi.getWidth();
    Expect.expect(width == 11);
    int height = roi.getHeight();
    Expect.expect(height == 11);
    Expect.expect(roi.getMaxX() == 20);
    Expect.expect(roi.getMaxY() == 20);

    roi.setWidthKeepingSameCenter(15);
    int minX = roi.getMinX();
    Expect.expect(minX == 8);
    width = roi.getWidth();
    Expect.expect(width == 15);
    roi.setHeightKeepingSameCenter(18);
    int minY = roi.getMinY();
    Expect.expect(minY == 6);
    height = roi.getHeight();
    Expect.expect(height == 18);

    roi.setWidthKeepingSameMinX(3);
    minX = roi.getMinX();
    Expect.expect(minX == 8);
    width = roi.getWidth();
    Expect.expect(width == 3);
    int maxX = roi.getMaxX();
    Expect.expect(maxX == 10);

    roi.setHeightKeepingSameMinY(100);
    minY = roi.getMinY();
    height = roi.getHeight();
    Expect.expect(minY == 6);
    Expect.expect(height == 100);
    int maxY = roi.getMaxY();
    Expect.expect(maxY == 105);

    testMinMaxAlongAcross();

    testCopyConstructor();
  }

  /**
   * @author Peter Esbensen
   */
  void testMinMaxAlongAcross()
  {
    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder( 10, 15, 20, 25, 0, RegionShapeEnum.RECTANGULAR );
    Expect.expect(roi.getMinCoordinateAlong() == 10);
    Expect.expect(roi.getMinCoordinateAcross() == 15);
    Expect.expect(roi.getMaxCoordinateAlong() == 20);
    Expect.expect(roi.getMaxCoordinateAcross() == 25);

    roi.setOrientationInDegrees(90);
    Expect.expect(roi.getMinCoordinateAlong() == 15);
    Expect.expect(roi.getMinCoordinateAcross() == 10);
    Expect.expect(roi.getMaxCoordinateAlong() == 25);
    Expect.expect(roi.getMaxCoordinateAcross() == 20);

    roi.setOrientationInDegrees(180);
    Expect.expect(roi.getMinCoordinateAlong() == 10);
    Expect.expect(roi.getMinCoordinateAcross() == 15);
    Expect.expect(roi.getMaxCoordinateAlong() == 20);
    Expect.expect(roi.getMaxCoordinateAcross() == 25);

    roi.setOrientationInDegrees(270);
    Expect.expect(roi.getMinCoordinateAlong() == 15);
    Expect.expect(roi.getMinCoordinateAcross() == 10);
    Expect.expect(roi.getMaxCoordinateAlong() == 25);
    Expect.expect(roi.getMaxCoordinateAcross() == 20);
  }

  /**
   * @author Patrick Lacz
   */
  public void testBadRegionOfInterests()
  {
    // Orientation must be multiple of 90.
    try {
      RegionOfInterest badRoi = new RegionOfInterest(0, 0, 10, 10, 45, RegionShapeEnum.RECTANGULAR);
      Expect.expect(false);
    } catch (AssertException e) {
      // expected, do nothing
    }

    // negative width/height is also bad
    try {
      RegionOfInterest badRoi = new RegionOfInterest(0, 0, 10, -10, 0, RegionShapeEnum.RECTANGULAR);
      Expect.expect(false);
    } catch (AssertException e) {
      // expected, do nothing
    }

    try {
      RegionOfInterest badRoi = new RegionOfInterest(0, 0, -10, 10, 0, RegionShapeEnum.RECTANGULAR);
      Expect.expect(false);
    } catch (AssertException e) {
      // expected, do nothing
    }

    // Width and height must be > 0
    try {
      RegionOfInterest badRoi = new RegionOfInterest(0, 0, 0, 10, 0, RegionShapeEnum.RECTANGULAR);
      Expect.expect(false);
    } catch (AssertException e) {
      // expected, do nothing
    }
    try {
      RegionOfInterest badRoi = new RegionOfInterest(0, 0, 10, 0, 0, RegionShapeEnum.RECTANGULAR);
      Expect.expect(false);
    } catch (AssertException e) {
      // expected, do nothing
    }
    try {
      RegionOfInterest badRoi = new RegionOfInterest(0, 0, -10, 10, 0, RegionShapeEnum.RECTANGULAR);
      Expect.expect(false);
    } catch (AssertException e) {
      // expected, do nothing
    }
    try {
      RegionOfInterest badRoi = new RegionOfInterest(0, 0, 10, -10, 0, RegionShapeEnum.RECTANGULAR);
      Expect.expect(false);
    } catch (AssertException e) {
      // expected, do nothing
    }
  }

  /**
   * @author Patrick Lacz
   */
  public void testCreateCircle()
  {
    RegionOfInterest circleRoi = RegionOfInterest.createRegionFromCircle(1, 4, 1);
    Assert.expect(circleRoi.getMinX() == 0);
    Assert.expect(circleRoi.getMinY() == 3);
    Assert.expect(circleRoi.getWidth() == 3);
    Assert.expect(circleRoi.getHeight() == 3);
    Assert.expect(circleRoi.getRegionShapeEnum() == RegionShapeEnum.OBROUND);
  }

  /**
   * @author Sunit Bhalla
   */
  public void testCopyConstructor()
  {
    RegionOfInterest r1 = new RegionOfInterest(1, 2, 3, 4, 90, RegionShapeEnum.RECTANGULAR);
    RegionOfInterest r2 = r1;
    Expect.expect(r1.getMinX() == r2.getMinX());
    Expect.expect(r1.getMinY() == r2.getMinY());
    Expect.expect(r1.getWidth() == r2.getWidth());
    Expect.expect(r1.getHeight() == r2.getHeight());
    Expect.expect(r1.getOrientationInDegrees() == r2.getOrientationInDegrees());
    Expect.expect(r1.getRegionShapeEnum().equals(r2.getRegionShapeEnum()));
  }
}
