package com.axi.util.image;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Patrick Lacz
 */
class Test_Paint extends UnitTest
{
  /**
   * @author Patrick Lacz
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Paint());
  }

  /**
   * @author Patrick Lacz
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Image image = loadTestImage(getTestDataDir(), "test1.png");

    /* test1.png is an image that looks like:

      0    0    0   127 127 127
      0    0    0   127 127 127
      0    0    0   127 127 127
      0    0    0   255 255 255
      0    0    0   255 255 255
      0    0    0   255 255 255
     */
    testBadInputs(image);

    testFloodFill(image);

    image.decrementReferenceCount();
  }

  /**
   * @author Patrick Lacz
   */
  private void testBadInputs(Image image)
  {
    Assert.expect(image != null);

    RegionOfInterest goodRoi = RegionOfInterest.createRegionFromImage(image);

    // test illegal regions of interest
    Collection<RegionOfInterest> regions = ImageUnitTestUtil.generateBadRegionsOfInterest(image);
    for (RegionOfInterest badRoi : regions)
    {
      // floodFillExact
      try
      {
        Paint.floodFillExact(image, badRoi, 0, 0, 0.f);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }

      // floodFillRange
      try
      {
        Paint.floodFillRange(image, badRoi, 0, 0, 0.f, 0.5f, 0.5f);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }

      // floodFillGradient
      try
      {
        Paint.floodFillGradient(image, badRoi, 0, 0, 0.f, 0.5f, 0.5f);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }

      // fillRegionOfInterest
      try
      {
        Paint.fillRegionOfInterest(image, badRoi, 0.f);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }

      // fillGradientXRegionOfInterest
      try
      {
        Paint.fillGradientXRegionOfInterest(image, badRoi, 0.f, 10.f);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }

      // fillGradientYRegionOfInterest
      try
      {
        Paint.fillGradientYRegionOfInterest(image, badRoi, 0.f, 10.f);
        Expect.expect(false); // shouldn't get here due to assert
      }
      catch (AssertException ae)
      {
        // do nothing, this is expected
      }
    }
  }

  /**
   * @author Patrick Lacz
   */
  private void testFloodFill(Image image)
  {
    Assert.expect(image != null);

    Image fillImage = new Image(image);

    RegionOfInterest fillImageRoi = RegionOfInterest.createRegionFromImage(fillImage);
    int numberOfPixelsChanged = Paint.floodFillExact(fillImage, fillImageRoi, 1, 1, 64.f);
    Expect.expect(numberOfPixelsChanged == 18);

    numberOfPixelsChanged = Paint.floodFillRange(fillImage, fillImageRoi, 4, 0, 0.f, 70.f, 70.f);
    Expect.expect(numberOfPixelsChanged == 27);

    fillImage.decrementReferenceCount();
    fillImage = new Image(image);

    numberOfPixelsChanged = Paint.floodFillGradient(fillImage, fillImageRoi, 5, 1, 0.f, 0.f, 200.f);
    Expect.expect(numberOfPixelsChanged == 18);
    fillImage.decrementReferenceCount();
  }

  /**
   * Copied from Test_ImageFeatureExtraction
   * @author Peter Esbensen
   */
  private Image loadTestImage(String directoryPath, String filename)
  {
    Assert.expect(directoryPath != null);
    Assert.expect(directoryPath.length() > 0);
    Assert.expect(filename != null);
    Assert.expect(filename.length() > 0);

    Image resultImage = null;
    try
    {
      resultImage = ImageIoUtil.loadPngImage(directoryPath + File.separator + filename);
    }
    catch (CouldNotReadFileException exc)
    {
      Expect.expect(false, "Exception occurred during PNG Image Load: " + exc.getMessage());
    }
    catch (FileDoesNotExistException exc)
    {
      Expect.expect(false, "FileDoesNotExistException:" + exc.getMessage());
    }
    Assert.expect(resultImage != null);
    return resultImage;
  }
}
