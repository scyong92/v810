package com.axi.util.image;

import com.axi.util.Enum;

/**
 * A PrimitiveDirectionEnum is used to identify the direction of the primitive.
 *
 * @author Sunit Bhalla
 */
public class PrimitiveDirectionEnum extends com.axi.util.Enum
{


  private static int _index = 0;
  public static final PrimitiveDirectionEnum FORWARD = new PrimitiveDirectionEnum(++_index);
  public static final PrimitiveDirectionEnum REVERSE = new PrimitiveDirectionEnum(++_index);
  public static final PrimitiveDirectionEnum UPWARDS = new PrimitiveDirectionEnum(++_index);
  public static final PrimitiveDirectionEnum DOWNWARDS = new PrimitiveDirectionEnum(++_index);

  /**
   * @author Sunit Bhalla
   */
  private PrimitiveDirectionEnum(int id)
  {
    super(id);
  }
}
