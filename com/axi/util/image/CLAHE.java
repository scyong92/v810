package com.axi.util.image;

/**
 * CLAHE - Contrast Limit Adaptive Histogram Equalization.
 * It is a image processing method to enhance the local contrast of an image.
 * Please refer http://fiji.sc/Enhance_Local_Contrast_(CLAHE)
 *
 * @param _blockRadius - The size of the local region around a pixel for which the histogram is equalized. 
 *                                          This size should be larger than the size of features to be preserved.
 * @param _bin - The number of histogram bins used for histogram equalization. 
 *                           The implementation internally works with byte resolution, so values larger than 256 are not meaningful. 
 *                           This value also limits the quantification of the output when processing 8bit gray or 24bit RGB images. 
 *                           The number of histogram bins should be smaller than the number of pixels in a block.
 * @param _slope - Limits the contrast stretch in the intensity transfer function. 
 *                               Very large values will let the histogram equalization do whatever it wants to do, that is result in maximal local contrast. 
 *                               The value 1 will result in the original image.
 * @param _isFast - Use the fast but less accurate version of the filter. The fast version does not evaluate the 
 *                                intensity transfer function for each pixel independently but for a grid of adjacent boxes of the 
 *                                given block size only and interpolates for locations in between.
 * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
 */
public class CLAHE extends ImageEnhancerBase
{
  public int _blockRadius = 30;
  public int _bin = 256;
  public int _slope = 3;
  public boolean _isFast = true;

  public CLAHE(int blockRadius, int bin, int slope, boolean isFast)
  {
    this._blockRadius = blockRadius;
    this._bin = bin;
    this._slope = slope;
    this._isFast = isFast;
  }
  
 public ImageEnhancerType getType()
 {
  return ImageEnhancerType.CLAHE;  
 }
   
  public Image runEnhancement(Image dest, Image src)
  {
    return ImageEnhancer.contrastLimitAdapHisEqual(dest, src, _blockRadius, _bin, _slope, _isFast);
  }
  
  public void updateParameter(int blockRadius, int bin, int slope, boolean isFast)
  {
    this._blockRadius = blockRadius;
    this._bin = bin;
    this._slope = slope;
    this._isFast = isFast;
  }
}