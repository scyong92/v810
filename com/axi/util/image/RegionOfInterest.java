package com.axi.util.image;

import java.awt.*;
import java.awt.geom.*;

import com.axi.util.*;

/**
 * This class defines a rectangular region of interest.  The
 * region can be an exact rectangle or a rectangle with rounded
 * ends (and obround shape)
 *
 * Dimensions can be queried as image coordinates (X,Y) or relative to
 * the definied orientation (Along, Across).
 *
 * The minimum width or height of the RegionOfInterest is 1.
 *
 * @author Bill Darbie
 */
public class RegionOfInterest extends ImageRectangle
{
  // can be a rectangle or a obround (rectangle with rounded ends)
  private RegionShapeEnum _regionShapeEnum;

  // the orientation is 0, 90, 180, or 270 - one of the 'cardinal' directions,
  // otherwise the region of interest is not aligned properly.
  // This setting is used by the algorithms to determine in which orientation
  // to expect the data to lie, for most other purposes use orientation 0.
  private int _orientationInDegrees;
  static private int _orientationOfLine;  // 1=\ 2=/

    /**
   * @author Anthony Fong
   */
  static public RegionOfInterest createLineRegionFromRegionBorder(int topLeftX,
      int topLeftY,
      int bottomRightX,
      int bottomRightY,
      int orientationInDegrees,
      RegionShapeEnum regionShapeEnum)
  {
      if( topLeftX <= bottomRightX )
      {
        _orientationOfLine = 1;

      }
      else
      {
          int temp;          
          temp = bottomRightX;
          bottomRightX  = topLeftX;
          topLeftX  = temp;
        _orientationOfLine = 2;
      }

      if( topLeftY > bottomRightY )
      {

          int temp;
          temp = bottomRightY;
          bottomRightY  = topLeftY;
          topLeftY  = temp;

//          temp = bottomRightX;
//          bottomRightX  = topLeftX;
//          topLeftX  = temp;

      }

      int width =Math.abs( bottomRightX - topLeftX);
      int height = Math.abs( bottomRightY - topLeftY);

    return (new RegionOfInterest( topLeftX,
                                  topLeftY,
                                  width,
                                  height,
                                  orientationInDegrees,
                                  regionShapeEnum,
                                  _orientationOfLine ));
  }

  /**
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  static public RegionOfInterest createRegionFromRegionBorder(int topLeftX,
      int topLeftY,
      int bottomRightX,
      int bottomRightY,
      int orientationInDegrees,
      RegionShapeEnum regionShapeEnum)
  {
    Assert.expect( topLeftX >= 0 );
    Assert.expect( topLeftY >= 0 );
    Assert.expect( bottomRightX >= 0 );
    Assert.expect( bottomRightY >= 0 );
    Assert.expect( topLeftX <= bottomRightX );
    Assert.expect( topLeftY <= bottomRightY );

    return (new RegionOfInterest( topLeftX,
                                  topLeftY,
                                  bottomRightX - topLeftX + 1,
                                  bottomRightY - topLeftY + 1,
                                  orientationInDegrees,
                                  regionShapeEnum ));
  }

  /**
   * @author Matt Wharton
   */
  public static RegionOfInterest createRegionFromRegionCenter(int centerX,
                                                              int centerY,
                                                              int width,
                                                              int height,
                                                              int orientationInDegrees,
                                                              RegionShapeEnum regionShapeEnum)
  {
    Assert.expect(centerX >= 0);
    Assert.expect(centerY >= 0);
    Assert.expect(width > 0);
    Assert.expect(height > 0);
    Assert.expect((orientationInDegrees >= 0) && (orientationInDegrees < 360));
    Assert.expect(regionShapeEnum != null);

    RegionOfInterest roi = new RegionOfInterest(0, 0, width, height, orientationInDegrees, regionShapeEnum);
    roi.setCenterXY(centerX, centerY);

    return roi;
  }

  /**
   * @author Patrick Lacz
   */
  static public RegionOfInterest createRegionFromImage(Image image)
  {
    Assert.expect( image != null );

    return new RegionOfInterest(0, 0, image.getWidth(), image.getHeight(), 0, RegionShapeEnum.RECTANGULAR);
  }

  /**
   * @author Patrick Lacz
   */
  static public RegionOfInterest createRegionFromOrientation(
      int topLeftX,
      int topLeftY,
      int lengthAlong,
      int lengthAcross,
      int orientationInDegrees,
      RegionShapeEnum regionShapeEnum)
  {
    Assert.expect( topLeftX >= 0 );
    Assert.expect( topLeftY >= 0 );
    Assert.expect( lengthAlong > 0 );
    Assert.expect( lengthAcross > 0 );
    Assert.expect( orientationInDegrees % 90 == 0 );
    Assert.expect( orientationInDegrees >= 0 && orientationInDegrees < 360 );

    RegionOfInterest roi = new RegionOfInterest(topLeftX, topLeftY, 0, 0, orientationInDegrees, regionShapeEnum);
    roi.setLengthAlong(lengthAlong);
    roi.setLengthAcross(lengthAcross);
    return roi;
  }

  /**
   * @author Patrick Lacz
   */
  public static RegionOfInterest createRegionFromCircle(int centerX, int centerY, int radius)
  {
    Assert.expect( centerX >= radius );
    Assert.expect( centerY >= radius );

    RegionOfInterest roi = new RegionOfInterest(centerX-radius, centerY-radius, 2*radius+1, 2*radius+1, 0, RegionShapeEnum.OBROUND);
    return roi;
  }

  /**
   * @author Patrick Lacz
   * @author Matt Wharton
   */
  public static RegionOfInterest createRegionFromIntersection(RegionOfInterest roi, Image image)
  {
    Assert.expect(roi != null);
    Assert.expect(image != null);
    Assert.expect(roi.intersectsImage(image));

    return RegionOfInterest.createRegionFromRegionBorder(
        Math.max(roi.getMinX(), 0),
        Math.max(roi.getMinY(), 0),
        Math.min(Math.max(0, roi.getMaxX()), image.getWidth()-1),
        Math.min(Math.max(0, roi.getMaxY()), image.getHeight()-1),
        roi.getOrientationInDegrees(),
        roi.getRegionShapeEnum());
  }
  /**
   * @author Patrick Lacz
   */
  public static RegionOfInterest createRegionFromIntersection(RegionOfInterest roiA, RegionOfInterest roiB)
  {
    Assert.expect(roiA != null);
    Assert.expect(roiB != null);
    Assert.expect(roiA.getOrientationInDegrees() == roiB.getOrientationInDegrees());
    Assert.expect(roiA.getRegionShapeEnum() == roiB.getRegionShapeEnum());
    Assert.expect(roiA.intersects(roiB));

    return RegionOfInterest.createRegionFromRegionBorder(
        Math.max(roiA.getMinX(), roiB.getMinX()),
        Math.max(roiA.getMinY(), roiB.getMinY()),
        Math.min(roiA.getMaxX(), roiB.getMaxX()),
        Math.min(roiA.getMaxY(), roiB.getMaxY()),
        roiA.getOrientationInDegrees(),
        roiA.getRegionShapeEnum());
  }
  /**
   * @author Anthony Fong
   */
  private RegionOfInterest(int topLeftX,
                          int topLeftY,
                          int width,
                          int height,
                          int orientationInDegrees,
                          RegionShapeEnum regionShapeEnum,
                          int orientationOfLine)
  {
    super(topLeftX, topLeftY, width, height);
    Assert.expect(getWidth() >= 0);
    Assert.expect(getHeight() >= 0);

    Assert.expect( orientationInDegrees % 90 == 0 );
    Assert.expect( orientationInDegrees >= 0 && orientationInDegrees < 360 );
    Assert.expect(regionShapeEnum != null);

    _orientationInDegrees = orientationInDegrees;
    _regionShapeEnum = regionShapeEnum;
    _orientationOfLine = orientationOfLine;
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public RegionOfInterest(int topLeftX,
                          int topLeftY,
                          int width,
                          int height,
                          int orientationInDegrees,
                          RegionShapeEnum regionShapeEnum)
  {
    super(topLeftX, topLeftY, width, height);
    Assert.expect(getWidth() > 0);
    Assert.expect(getHeight() > 0);

    Assert.expect( orientationInDegrees % 90 == 0 );
    Assert.expect( orientationInDegrees >= 0 && orientationInDegrees < 360 );
    Assert.expect(regionShapeEnum != null);

    _orientationInDegrees = orientationInDegrees;
    _regionShapeEnum = regionShapeEnum;
  }

  /**
   * Copy constructor
   *
   * @author Peter Esbensen
   */
  public RegionOfInterest(RegionOfInterest rhs)
  {
    super(rhs.getMinX(), rhs.getMinY(), rhs.getWidth(), rhs.getHeight());

    _orientationInDegrees = rhs.getOrientationInDegrees();
    _regionShapeEnum = rhs.getRegionShapeEnum();
  }

  /**
   * Copy constructor - able to specify the regionShape to be used
   *
   * @author Bee Hoon,Goh
   */
  public RegionOfInterest(RegionOfInterest rhs, RegionShapeEnum regionShapeEnum)
  {
    super(rhs.getMinX(), rhs.getMinY(), rhs.getWidth(), rhs.getHeight());

    _orientationInDegrees = rhs.getOrientationInDegrees();
    _regionShapeEnum = regionShapeEnum;
  }
  
  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public RegionOfInterest(ImageRectangle imageRectangle,
                          int orientationInDegrees,
                          RegionShapeEnum regionShapeEnum)
  {
    super(imageRectangle);
    Assert.expect(getWidth() > 0);
    Assert.expect(getHeight() > 0);

    while (orientationInDegrees < 0)
      orientationInDegrees += 360;
    while (orientationInDegrees >= 360)
      orientationInDegrees -= 360;

    Assert.expect( orientationInDegrees % 90 == 0 );
    Assert.expect( orientationInDegrees >= 0 && orientationInDegrees < 360 );
    Assert.expect(regionShapeEnum != null);

    _orientationInDegrees = orientationInDegrees;
    _regionShapeEnum = regionShapeEnum;
  }

  /**
   * @author Patrick Lacz
   */
  public boolean equals(Object o)
  {
    if (o == null)
      return false;
    if (o instanceof RegionOfInterest)
    {
      RegionOfInterest other = (RegionOfInterest)o;
      if (other.getMinX() == getMinX() &&
          other.getMaxX() == getMaxX() &&
          other.getMinY() == getMinY() &&
          other.getMaxY() == getMaxY() &&
          other.getRegionShapeEnum() == getRegionShapeEnum() &&
          other.getOrientationInDegrees() == getOrientationInDegrees())
        return true;
    }
    return false;
  }

  /**
   * @author Matt Wharton
   */
  public RegionShapeEnum getRegionShapeEnum()
  {
    Assert.expect(_regionShapeEnum != null);

    return _regionShapeEnum;
  }

  /**
   * @author Patrick Lacz
   */
  public void setRegionShapeEnum(RegionShapeEnum newShapeEnum)
  {
    Assert.expect(newShapeEnum != null);
    _regionShapeEnum = newShapeEnum;
  }

  /**
   * @return Orientation is one of 0, 90, 180, and 270.
   * @author Peter Esbensen
   */
  public int getOrientationInDegrees()
  {
    return _orientationInDegrees;
  }

  /**
   * @param orientationInDegrees Must be 0, 90, 180, or 270. Larger (or smaller) multiples of 90 are mapped to these values.
   * @author Peter Esbensen
   */
  public void setOrientationInDegrees(int orientationInDegrees)
  {
    while (orientationInDegrees < 0)
      orientationInDegrees += 360;
    while (orientationInDegrees > 359)
      orientationInDegrees -= 360;

    Assert.expect( orientationInDegrees % 90 == 0 );
    Assert.expect( orientationInDegrees >= 0 && orientationInDegrees < 360 );

    _orientationInDegrees = orientationInDegrees;
  }

  /**
   * @author Peter Esbensen
   */
  public boolean fitsWithinImage(Image image)
  {
    Assert.expect(image != null);
    return ((getMaxX() < image.getWidth()) &&
            (getMaxY() < image.getHeight()) &&
            (getMinX() >= 0) &&
            (getMinY() >= 0));
  }

  /**
   * @author Patrick Lacz
   */
  public boolean fitsWithinRegionOfInterest(RegionOfInterest roi)
  {
    Assert.expect(roi != null);
    return getMaxX() <= roi.getMaxX() &&
        getMinX() >= roi.getMinX() &&
        getMaxY() <= roi.getMaxY() &&
        getMinY() >= roi.getMinY();
  }

  /**
   * @author Patrick Lacz
   */
  public boolean containsPixel(ImageCoordinate pixelLocation)
  {
    Assert.expect(pixelLocation != null);

    int x = pixelLocation.getX();
    int y = pixelLocation.getY();

    boolean returnValue = false;
    if (x >= getMinX() && x <= getMaxX() && y >= getMinY() && y <= getMaxY())
    {
      returnValue = true;
    }

    if (_regionShapeEnum.equals(RegionShapeEnum.OBROUND) && returnValue == true)
    {
      int width = getWidth();
      int height = getHeight();
      DoubleCoordinate circleCenter = null;
      double radius = 0.0;
      if (width > height)
      {
        // horizontal
        radius = Math.ceil(height / 2.0);
        if (x < (getMinX() + radius))
        {
          // test against left side
         circleCenter = new DoubleCoordinate(getMinX() + radius, getMinY() + radius);
        }
        else if (x > (getMaxX() - radius))
        {
          // test against right side
         circleCenter = new DoubleCoordinate(getMaxX() - radius, getMinY() + radius);
        }
      }
      else if (width < height)
      {
        // vertical
        radius = Math.ceil(width / 2.0);
        if (y < (getMinY() + radius))
        {
          // test against top side
         circleCenter = new DoubleCoordinate(getMinX() + radius, getMinY() + radius);
        }
        else if (y > (getMaxY() - radius))
        {
          // test against bottom side
         circleCenter = new DoubleCoordinate(getMinX() + radius, getMaxY() - radius);
        }
      }
      else
      {
        // circular
        radius = Math.ceil(height / 2.0);
        circleCenter = new DoubleCoordinate(getMinX() + radius, getMinY() + radius);
      }

      if (circleCenter != null)
      {
        // if the point lies outside the circle, reject. (we only created the circle if the point lies in the pertainent region)

        // we use the implict formula for a circle
        //   0 = (x-x0)^2 + (y-y0)^2 - R^2
        // solve the quadratic equation for this row
        // A = 1
        // B = -2*x0
        // C = x0^2 + (y-y0)^2-R^2
        double A = 1.0;
        double B = -2.0 * circleCenter.getX();
        double differenceInY = (y + 0.5 - circleCenter.getY());
        double x0Squared = circleCenter.getX() * circleCenter.getX();
        double radiusSquared = radius * radius;

        double C = x0Squared + differenceInY * differenceInY - radiusSquared;

        double underSqrt = B * B - 4 * A * C;
        if (underSqrt < 0.0) // imaginary roots -- no circle intersection
        {
          returnValue = false;
        }
        else
        {
          double result0 = ( -B - Math.sqrt(underSqrt)) / (2 * A);
          double result1 = ( -B + Math.sqrt(underSqrt)) / (2 * A);

          // bias rounding down
          int beginX = (int)Math.round(result0 - 0.0001);
          // bias rounding up
          int endX = (int)Math.round(result1 + 0.0001) - 1;

          if (x < beginX || x > endX)
            returnValue = false;
        }
      }
    }

    return returnValue;
  }


  /**
   * @author Patrick Lacz
   * @author Matt Wharton
   */
  public boolean intersectsRegionOfInterest(RegionOfInterest roi)
  {
    Assert.expect(roi != null);
    return getMinX() < roi.getMaxX() &&
        getMaxX() > roi.getMinX() &&
        getMinY() < roi.getMaxY() &&
        getMaxY() > roi.getMinY();
  }

  /**
   * @author Matt Wharton
   */
  public boolean intersectsImage(Image image)
  {
    Assert.expect(image != null);

    RegionOfInterest imageRoi = RegionOfInterest.createRegionFromImage(image);

    return intersectsRegionOfInterest(imageRoi);
  }

  /**
   * @author Peter Esbensen
   */
  public void setWidthKeepingSameCenter(int width)
  {
    width = Math.max(width, 1);
    double differenceInWidth = width - getWidth();
    int newMinX = (int)(getMinX() - (differenceInWidth * 0.5));
    setRect(newMinX,
            getMinY(),
            width,
            getHeight());
  }

  /**
   * @author Peter Esbensen
   */
  public void setHeightKeepingSameCenter(int height)
  {
    height = Math.max(height, 1);
    double differenceInHeight = height - getHeight();
    int newMinY = (int)(getMinY() - (differenceInHeight * 0.5));
    setRect(getMinX(),
            newMinY,
            getWidth(),
            height);
  }

  /**
   * @author Goh Bee Hoon
   */
  public void setWidthWithoutKeepingSameCenter(int width, float position)
  {
    width = Math.max(width, 1);
    double differenceInWidth = width - getWidth();
    int newMinX = (int)(getMinX() - (differenceInWidth * position));
    setRect(newMinX,
            getMinY(),
            width,
            getHeight());
  }
  
  /**
   * @author Goh Bee Hoon
   */
  public void setHeightWithoutKeepingSameCenter(int height, float position)
  {
    height = Math.max(height, 1);
    double differenceInHeight = height - getHeight();
    int newMinY = (int)(getMinY() - (differenceInHeight * position));
    setRect(getMinX(),
            newMinY,
            getWidth(),
            height);
  }
  
  /**
   * @author Peter Esbensen
   */
  public void setWidthKeepingSameMinX(int width)
  {
    width = Math.max(width, 1);
    setRect(getMinX(),
            getMinY(),
            width,
            getHeight());
  }

  /**
   * @author Peter Esbensen
   */
  public void setHeightKeepingSameMinY(int height)
  {
    height = Math.max(height, 1);
    setRect(getMinX(),
            getMinY(),
            getWidth(),
            height);
  }

  /**
   * @author Patrick Lacz
   */
  public void setCenterXY(int x, int y)
  {
    setMinXY(x - getWidth()/2, y - getHeight()/2);
  }

  /**
   * @author Sunit Bhalla
   */
  public Shape getShape()
  {
    Shape returnShape = null;
    if (_regionShapeEnum.equals(RegionShapeEnum.RECTANGULAR))
      returnShape = new Rectangle2D.Double(getMinX(), getMinY(), getWidth(), getHeight());
    if (_regionShapeEnum.equals(RegionShapeEnum.OBROUND))
      returnShape = GeomUtil.createObround(getMinX(), getMinY(), getWidth(), getHeight());
    if (_regionShapeEnum.equals(RegionShapeEnum.LINE))
    {
        if(_orientationOfLine==1) // 1=\
        {
            returnShape = new Line2D.Double(getMinX(),
                                            getMinY(),
                                            getMinX() + getWidth(),
                                            getMinY() + getHeight());
        }
        else if(_orientationOfLine==2) // 2=/
        {
            returnShape = new Line2D.Double(getMinX() + getWidth(),
                                            getMinY(),
                                            getMinX(),
                                            getMinY() + getHeight());
        }

    }
    return returnShape;
  }

  /**
   * Gets the width or height as apporpriate by the orientation.
   * For an orentation at 0 degrees, this is the height (Y).
   * @author Patrick Lacz
   */
  public int getLengthAcross()
  {
    int length = 0;
    switch ( getOrientationInDegrees() )
    {
      case 0:
      case 180:
        length = getHeight();
        break;
      case 90:
      case 270:
        length = getWidth();
        break;
      default:
        Assert.expect(false);
    }
    return length;
  }

  /**
   * Gets the width or height as appropriate by the orientation.
   * For an orentation at 0 degrees, this is the width (X).
   * @author Patrick Lacz
   */
  public int getLengthAlong()
  {
    int length = 0;
    switch ( getOrientationInDegrees() )
    {
      case 0:
      case 180:
        length = getWidth();
        break;
      case 90:
      case 270:
        length = getHeight();
        break;
      default:
        Assert.expect(false);
    }
    return length;
  }

  /**
   * Sets the width or height as appropriate by the orientation.
   * For an orentation at 0 degrees, this is the width (X).
   * @author Patrick Lacz
   */
  public void setLengthAlong(int newLength)
  {
    switch ( getOrientationInDegrees() )
    {
      case 0:
      case 180:
        setWidthKeepingSameCenter( newLength );
        break;
      case 90:
      case 270:
        setHeightKeepingSameCenter( newLength );
        break;
    }
  }

  /**
   * Sets the width or height as apporpriate by the orientation.
   * For an orentation at 0 degrees, this is the height (Y).
   * @author Patrick Lacz
   */
  public void setLengthAcross(int newLength)
  {
    switch (getOrientationInDegrees())
    {
      case 0:
      case 180:
        setHeightKeepingSameCenter( newLength );
        break;
      case 90:
      case 270:
        setWidthKeepingSameCenter( newLength );
        break;
      default:
        Assert.expect(false);
    }
  }

  /**
   * Sets the width or height as apporpriate by the orientation.
   * For an orentation at 0 degrees, this is the height (Y).
   * @author Goh Bee Hoon
   */
  public void setLengthAcrossWithAdjustableCenter(int newLength, float position)
  {
    switch (getOrientationInDegrees())
    {
      case 0:
      case 180:
        setHeightWithoutKeepingSameCenter( newLength, position );
        break;
      case 90:      
      case 270:
        setWidthWithoutKeepingSameCenter( newLength, position );
        break;
      default:
        Assert.expect(false);
    }
  }

  /**
   * Moves the region in image space (X,Y)
   * @author Patrick Lacz
   */
  public void translateXY(int dx, int dy)
  {
    setMinXY( getMinX()+dx, getMinY()+dy );
  }

  /**
   * Moves the region relative to the orientation
   * @author Patrick Lacz
   */
  public void translateAlongAcross(int dAlong, int dAcross)
  {
    switch (getOrientationInDegrees())
    {
      case 0:
        translateXY( dAlong, dAcross );
        break;
      case 90:
        translateXY( dAcross, -dAlong );
        break;
      case 180:
        translateXY( -dAlong, -dAcross );
        break;
      case 270:
        translateXY( -dAcross, dAlong );
        break;
      default:
        Assert.expect(false);
    }
  }

  /**
   * Scales the region in image space (X,Y)
   * @author Patrick Lacz
   */
  public void scaleFromCenterXY(double dx, double dy)
  {
    setWidthKeepingSameCenter( (int)(getWidth() * dx) );
    setHeightKeepingSameCenter( (int)(getHeight() * dy) );
  }

  /**
   * Scales the region relative to the orientation
   * @author Patrick Lacz
   */
  public void scaleFromCenterAlongAcross(double dAlong, double dAcross)
  {
    switch (getOrientationInDegrees())
    {
      case 0:
      case 180:
        scaleFromCenterXY( dAlong, dAcross );
        break;
      case 90:
      case 270:
        scaleFromCenterXY( dAcross, dAlong );
        break;
      default:
        Assert.expect(false);
    }
  }

  /**
   * Get the center of the region in the "along" dimension
   *
   * @author Peter Esbensen
   */
  public double getMinCoordinateAlong()
  {
    int orientationInDegrees = getOrientationInDegrees();

    int minAlong = 0;
    if ( (orientationInDegrees == 0) || (orientationInDegrees == 180) )
    {
      minAlong = getMinX();
    }
    else if ( (orientationInDegrees == 90) || (orientationInDegrees == 270) )
    {
      minAlong = getMinY();
    }
    else
      Assert.expect(false);
    return minAlong;
  }

  /**
   * Set the center of the region in the "along" dimension
   *
   * @author Lim Seng Yew
   */
  public void setMinCoordinateAlong(int newMinAlong)
  {
    int orientationInDegrees = getOrientationInDegrees();

    if ( (orientationInDegrees == 0) || (orientationInDegrees == 180) )
    {
      setMinXY(newMinAlong,getMinY());
    }
    else if ( (orientationInDegrees == 90) || (orientationInDegrees == 270) )
    {
      setMinXY(getMinX(),newMinAlong);
    }
    else
      Assert.expect(false);
  }

  /**
   * Get the center of the region in the "along" dimension
   *
   * @author Peter Esbensen
   */
  public double getMinCoordinateAcross()
  {
    int orientationInDegrees = getOrientationInDegrees();

    int minAcross = 0;
    if ( (orientationInDegrees == 0) || (orientationInDegrees == 180) )
    {
      minAcross = getMinY();
    }
    else if ( (orientationInDegrees == 90) || (orientationInDegrees == 270) )
    {
      minAcross = getMinX();
    }
    else
      Assert.expect(false);
    return minAcross;
  }

  /**
   * Get the maximum coordinate in the "across" dimension
   *
   * @author Peter Esbensen
   */
  public double getMaxCoordinateAcross()
  {
    int orientationInDegrees = getOrientationInDegrees();

    int maxAcross = 0;
    if ( (orientationInDegrees == 0) || (orientationInDegrees == 180) )
    {
      maxAcross = getMaxY();
    }
    else if ( (orientationInDegrees == 90) || (orientationInDegrees == 270) )
    {
      maxAcross = getMaxX();
    }
    else
      Assert.expect(false);
    return maxAcross;
  }

  /**
   * Get the maximum coordinate in the "along" dimension
   *
   * @author Peter Esbensen
   */
  public double getMaxCoordinateAlong()
  {
    int orientationInDegrees = getOrientationInDegrees();

    int maxAlong = 0;
    if ( (orientationInDegrees == 0) || (orientationInDegrees == 180) )
    {
      maxAlong = getMaxX();
    }
    else if ( (orientationInDegrees == 90) || (orientationInDegrees == 270) )
    {
      maxAlong = getMaxY();
    }
    else
      Assert.expect(false);
    return maxAlong;
  }


  /**
   * Get the center of the region in the "along" dimension
   * @author Peter Esbensen
   */
  public double getCenterAlong()
  {
    int orientationInDegrees = getOrientationInDegrees();

    int centerAlong = 0;
    if ( (orientationInDegrees == 0) || (orientationInDegrees == 180) )
    {
      centerAlong = getCenterX();
    }
    else if ( (orientationInDegrees == 90) || (orientationInDegrees == 270) )
    {
      centerAlong = getCenterY();
    }
    else
      Assert.expect(false);
    return centerAlong;
  }

  /**
   * Get the center of the region in the "across" dimension
   * @author Peter Esbensen
   */
  public double getCenterAcross()
  {
    int orientationInDegrees = getOrientationInDegrees();

    int centerAcross = 0;
    if ( (orientationInDegrees == 0) || (orientationInDegrees == 180) )
    {
      centerAcross = getCenterY();
    }
    else if ( (orientationInDegrees == 90) || (orientationInDegrees == 270) )
    {
      centerAcross = getCenterX();
    }
    else
      Assert.expect(false);
    return centerAcross;
  }

}

