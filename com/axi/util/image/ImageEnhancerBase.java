package com.axi.util.image;

/**
 *This class keep all supported ImageEnhancer type and its parameters,
 * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
 */
public abstract class ImageEnhancerBase
{
  public enum ImageEnhancerType
  {
    Resize,
    RemoveArtifactByBoxFilter,
    CLAHE,
    FFTBandPassFilter,
    InvertImage,
    BoxFilter,//Not supported    
    ColourTwist,//Not supported
    ColourMap, //Not supported
    MotionBlur
  }
  
  public ImageEnhancerBase()
  {
    //Do nothing
  }

  public abstract ImageEnhancerType getType();
  public abstract Image runEnhancement(Image dest, Image src);
  
}
 



