#include "com_axi_util_image_Paint.h"
#include "com/axi/util/jniUtil.h"
#include "sptAssert.h"

#include "ippcore.h"
#include "ippi.h"
#include "ippcv.h"

#include "jniImageUtil.h"

/**
 * nativeFill
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Paint_nativeFill(JNIEnv *jEnv, 
                                                                    jclass jContext, 
                                                                    jintArray jSourceImage, 
                                                                    jint roiMinX, 
                                                                    jint roiMinY, 
                                                                    jint roiWidth, 
                                                                    jint roiHeight, 
                                                                    jfloat value)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    IppiSize roiSize = { roiWidth, roiHeight };
    
    IppStatus ippResult = ippiSet_32f_C1R(value, 
                                          pSourceImage->addressAt(roiMinX, roiMinY),
                                          pSourceImage->stride,
                                          roiSize);
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}
  
/**
 * nativeGradient
 *
 * This is not a derivative filter! It draws a gradient background into the region of interest.
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Paint_nativeGradient(JNIEnv *jEnv, 
                                                                        jclass jContext, 
                                                                        jintArray jSourceImage, 
                                                                        jint roiMinX, 
                                                                        jint roiMinY, 
                                                                        jint roiWidth, 
                                                                        jint roiHeight, 
                                                                        jfloat startValue, 
                                                                        jfloat slope, 
                                                                        jboolean isHorizontal)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize roiSize = { roiWidth, roiHeight };
    
    IppStatus ippResult = ippiImageRamp_32f_C1R(pSourceImage->addressAt(roiMinX, roiMinY), 
                                                pSourceImage->stride,
                                                roiSize, 
                                                startValue, 
                                                slope, 
                                                isHorizontal ? ippAxsHorizontal : ippAxsVertical);
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}


/**
 * nativeFloodFillExact
 *
 * Performs a flood fill where the only values changed are the contiguous region where the pixel values are exactly the same as the seed.
 * 
 * @author Patrick Lacz
 */
JNIEXPORT jint JNICALL Java_com_axi_util_image_Paint_nativeFloodFillExact(JNIEnv *jEnv, 
                                                                              jclass jContext, 
                                                                              jintArray jSourceImage, 
                                                                              jint roiMinX, 
                                                                              jint roiMinY, 
                                                                              jint roiWidth, 
                                                                              jint roiHeight, 
                                                                              jint seedX, 
                                                                              jint seedY, 
                                                                              jfloat newValue)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize roiSize = { roiWidth, roiHeight };
    IppiPoint seedXY = { seedX, seedY };
    
    IppiConnectedComp connectedComponentResult;
    int bufferSize;
    IppStatus ippResult = ippiFloodFillGetSize(roiSize, &bufferSize);
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    
    Ipp8u *pBuffer = new Ipp8u[bufferSize];
    
    ippResult = ippiFloodFill_4Con_32f_C1IR(pSourceImage->addressAt(roiMinX, roiMinY), 
                                            pSourceImage->stride, 
                                            roiSize,
                                            seedXY,
                                            newValue,
                                            &connectedComponentResult,
                                            pBuffer);
    
    delete [] pBuffer;
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    return (int)connectedComponentResult.area;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}
  
/**
 * nativeFloodFillRange
 * 
 * Performs a flood fill where the values changed are the pixels in a contiguous region around the seed, within the specified tolerance of the seed pixel's value.
 *
 * @author Patrick Lacz
 */
JNIEXPORT jint JNICALL Java_com_axi_util_image_Paint_nativeFloodFillRange(JNIEnv *jEnv,
                                                                              jclass jContext, 
                                                                              jintArray jSourceImage, 
                                                                              jint roiMinX, 
                                                                              jint roiMinY, 
                                                                              jint roiWidth, 
                                                                              jint roiHeight, 
                                                                              jint seedX, 
                                                                              jint seedY, 
                                                                              jfloat newValue, 
                                                                              jfloat minBelowSeed, 
                                                                              jfloat maxAboveSeed)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize roiSize = { roiWidth, roiHeight };
    IppiPoint seedXY = { seedX, seedY };
    
    IppiConnectedComp connectedComponentResult;
    int bufferSize;
    IppStatus ippResult = ippiFloodFillGetSize(roiSize, &bufferSize);
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    
    Ipp8u *pBuffer = new Ipp8u[bufferSize];
    
    ippResult = ippiFloodFill_Range4Con_32f_C1IR(pSourceImage->addressAt(roiMinX, roiMinY), 
                                                 pSourceImage->stride, 
                                                 roiSize,
                                                 seedXY,
                                                 newValue,
                                                 minBelowSeed,
                                                 maxAboveSeed,
                                                 &connectedComponentResult,
                                                 pBuffer);
    
    delete [] pBuffer;
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    return (int)connectedComponentResult.area;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}


  
/**
 * nativeFloodFillGradient
 * 
 * Performs a flood fill where the values changed are the pixels in a contiguous region around the seed, 
 * where the change from one pixel to the next does not exceed the specified tolerances.
 *
 * @author Patrick Lacz
 */
JNIEXPORT jint JNICALL Java_com_axi_util_image_Paint_nativeFloodFillGradient(JNIEnv *jEnv, 
                                                                                 jclass jContext, 
                                                                                 jintArray jSourceImage, 
                                                                                 jint roiMinX, 
                                                                                 jint roiMinY, 
                                                                                 jint roiWidth, 
                                                                                 jint roiHeight, 
                                                                                 jint seedX, 
                                                                                 jint seedY, 
                                                                                 jfloat newValue, 
                                                                                 jfloat minBelowNeighbor, 
                                                                                 jfloat maxAboveNeighbor)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize roiSize = { roiWidth, roiHeight };
    IppiPoint seedXY = { seedX, seedY };
    
    IppiConnectedComp connectedComponentResult;
    int bufferSize;
    IppStatus ippResult = ippiFloodFillGetSize_Grad(roiSize, &bufferSize);
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    
    Ipp8u *pBuffer = new Ipp8u[bufferSize];
  
    ippResult = ippiFloodFill_Grad4Con_32f_C1IR(pSourceImage->addressAt(roiMinX, roiMinY), 
                                                pSourceImage->stride, 
                                                roiSize,
                                                seedXY,
                                                newValue,
                                                minBelowNeighbor,
                                                maxAboveNeighbor,
                                                &connectedComponentResult,
                                                pBuffer);
    
    delete [] pBuffer;
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    return (int)connectedComponentResult.area;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}
