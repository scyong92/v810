#ifndef COM_AXI_UTIL_JNIIMAGEUTIL_H
#define COM_AXI_UTIL_JNIIMAGEUTIL_H

#include <jni.h>
#include "ippcore.h"

#define UNALLOCATED -1
#define JAVA_ALLOCATED 0
#define CPP_NEW_ALLOCATED 1
#define C_MALLOC_ALLOCATED 2
#define IPP_CONTIGUOUS_ALLOCATED 3
#define IPP_ALIGNED_ALLOCATED 4

/**
 * A struct representing the data in the image data passed down from JNI.
 *
 * @author Patrick Lacz
 */
typedef struct {
	int width; // width must be first, it is used by java.
	int height; // height must be second, it is used by java
	int stride; // stride must by third, it is used by java
  int allocation;
	Ipp32f *address;

  /**
   * @author Patrick Lacz
   */
  Ipp32f *addressAt(int x, int y)
  {
    return reinterpret_cast<Ipp32f*>(reinterpret_cast<Ipp8u*>(address) + y*stride) + x;
  }

  /**
   * @author Patrick Lacz
   */
  IppiSize getSize()
  {
    IppiSize size;
    size.width = width;
    size.height = height;
    return size;
  }
} JNIImageData;

/**
 * @author Patrick Lacz
 */
JNIImageData *getImageDataFromJava(JNIEnv *jEnv, jintArray jImage);


/**
 * Use this function if you have modified the JNIImageData and want that change to be permanent.
 * @author Patrick Lacz
 */
void releaseImageDataFromJavaModified(JNIEnv *jEnv, jintArray jImage, JNIImageData *pImageData);

/**
 * Use this function if you have not modified the JNIImageData.
 * @author Patrick Lacz
 */
void releaseImageDataFromJavaUnmodified(JNIEnv *jEnv, jintArray jImage, JNIImageData *pImageData);

#endif
