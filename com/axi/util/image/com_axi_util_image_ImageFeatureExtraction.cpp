#include <jni.h>
#include "com_axi_util_image_ImageFeatureExtraction.h"

#include "sptAssert.h"

#include "ippcore.h"
#include "ippi.h"
#include "ipps.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <algorithm>
#include <string>

//Lim Lay Ngor Add:
#include "VxImageAnalyser.hpp"

using namespace std;

#include "jniImageUtil.h"

/**
 * nativeDoHorizontalProfile
 *
 * Apply a mean filter with an appropriately sized window.
 *
 * @author Patrick Lacz
 */
JNIEXPORT jfloatArray JNICALL Java_com_axi_util_image_ImageFeatureExtraction_nativeDoHorizontalProfile(JNIEnv *jEnv, 
                                                                                                           jclass jContext, 
                                                                                                           jintArray jSourceImage, 
                                                                                                           jint roiMinX, 
                                                                                                           jint roiMinY, 
                                                                                                           jint roiWidth, 
                                                                                                           jint roiHeight)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    // the result array is the size of the ROI.
    jfloatArray resultArray = jEnv->NewFloatArray(roiWidth);
    Ipp32f *pResult = reinterpret_cast<Ipp32f*>(jEnv->GetPrimitiveArrayCritical(resultArray, 0));
    
    IppiSize ippiDestRoi = { roiWidth, 1 };
    
    // build the kernel
    // PL: see nativeDoVerticalProfile for commands on dynamic vs static kernels.
    Ipp32f *pKernel = ippsMalloc_32f(roiHeight);
    std::fill_n(pKernel, roiHeight, 1.f/roiHeight);
    
    // for FilterColumn/Row, the achor point is from the LR, not the UL.
    int anchor = roiHeight-1;
    
    IppStatus ippResult = ippiFilterColumn_32f_C1R(pSourceImage->addressAt(roiMinX, roiMinY), 
                                                   pSourceImage->stride,
                                                   static_cast<Ipp32f*>(pResult),		// write results directly to return array, a n x 1 "image"
                                                   sizeof(Ipp32f)*ippiDestRoi.width,	//Destination Row-Step, Should not matter! (one row on output)
                                                   ippiDestRoi,						// size of the results (roiWidth x 1)
                                                   pKernel, roiHeight, anchor);
    
    // remember to delete the kernel
    ippsFree(pKernel);
    
    // must 'release' the pResult pointer back Java for the results to be stored.
    jEnv->ReleasePrimitiveArrayCritical(resultArray, pResult, 0);
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
  
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
    return resultArray;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return NULL;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return NULL;
  }
}

/**
 * nativeDoVerticalProfile
 *
 * Apply a mean filter with an appropriately sized window.
 *
 * @author Patrick Lacz
 */
JNIEXPORT jfloatArray JNICALL Java_com_axi_util_image_ImageFeatureExtraction_nativeDoVerticalProfile(JNIEnv *jEnv, 
                                                                                                         jclass jContext,
                                                                                                         jintArray jSourceImage, 
                                                                                                         jint roiMinX, 
                                                                                                         jint roiMinY, 
                                                                                                         jint roiWidth, 
                                                                                                         jint roiHeight)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    jfloatArray resultArray = jEnv->NewFloatArray(roiHeight);
    Ipp32f *pResult = reinterpret_cast<Ipp32f*>(jEnv->GetPrimitiveArrayCritical(resultArray, 0));
    
    IppiSize ippiDestRoi = { 1, roiHeight };
    
    // build the averaging kernel
    // PL: I've tried adding in logic to have a smaller static kernel, but this code was marginally but
    // consistantly faster.
    Ipp32f *pKernel = ippsMalloc_32f(roiWidth);
    std::fill_n(pKernel, roiWidth, 1.f/roiWidth);
    
    // for FilterColumn/Row, the achor point is from the LR, not the UL.
    int anchor = roiWidth-1;
    
    IppStatus ippResult = ippiFilterRow_32f_C1R(pSourceImage->addressAt(roiMinX, roiMinY), 
                                                pSourceImage->stride,
                                                static_cast<Ipp32f*>(pResult),	// write results directly to return array, a n x 1 "image"
                                                sizeof(Ipp32f), 						//Destination Row-Step, each entry is its own row
                                                ippiDestRoi,		    				// size of the results (roiWidth x 1)
                                                pKernel, 
                                                roiWidth, 
                                                anchor);
    
    // remember to delete the kernel when through
    ippsFree(pKernel);
    
    // must 'release' the pResult pointer back Java for the results to be stored.
    jEnv->ReleasePrimitiveArrayCritical(resultArray, pResult, 0);
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
    return resultArray;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return NULL;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return NULL;
  }
}

/**
 * nativeHoughCircleTransform
 *
 * Perform Hough Circle Transform on input image and return an array of radius.
 *
 * @author Cheah Lee Herng
 */
JNIEXPORT jintArray JNICALL Java_com_axi_util_image_ImageFeatureExtraction_nativeHoughCircleTransform(JNIEnv *jEnv, 
                                                                                                         jclass jContext,
                                                                                                         jstring fullPathFileName,
                                                                                                         jint minRadius,
                                                                                                         jint maxRadius)
{
  try
  {
    sptAssertWithMessage(fullPathFileName != NULL, "Empty image path");
  
    cv::Mat src, src_gray;
    const char *nativeFullPathFileName = jEnv->GetStringUTFChars(fullPathFileName, 0);
    jintArray resultArray; 
    
    // Read in input image
    src = cv::imread( nativeFullPathFileName, 1 );
    
    if ( src.data )
    {
      /// Convert it to gray 
      cv::cvtColor( src, src_gray, CV_BGR2GRAY );
      
      /// Reduce the noise so we avoid false circle detection
      cv::GaussianBlur( src_gray, src_gray, cv::Size(9, 9), 2, 2 );
      
      vector<cv::Vec3f> circles;
      
      /// Apply the Hough Transform to find the circles
      cv::HoughCircles( src_gray, circles, CV_HOUGH_GRADIENT, 2, src_gray.rows/4, 200, 100, minRadius, maxRadius );
      
      if (circles.size() > 0)
      {
        // Initialize int array
        resultArray = jEnv->NewIntArray(circles.size());
        jint temp[256];    /* make sure it is large enough! */
        
        /// Get the radius of circles detected
        for( size_t i = 0; i < circles.size(); i++ )
        {
          temp[i] = cvRound(circles[i][2]);
        }
        
        // move from the temp structure to the java structure
        jEnv->SetIntArrayRegion(resultArray, 0, circles.size(), temp);
      }
      else
        resultArray = jEnv->NewIntArray(0);
    }
    else
      resultArray = jEnv->NewIntArray(0);
    
    return resultArray;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return NULL;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return NULL;
  }
}

/**
 * nativeFitEllipse
 *
 * Fit Ellipse and find contours on input image and return an array of radius.
 * Note that this function returns diameter of circle instead of radius. 
 *
 * @author Cheah Lee Herng
 */
JNIEXPORT jfloatArray JNICALL Java_com_axi_util_image_ImageFeatureExtraction_nativeFitEllipse(JNIEnv *jEnv, 
                                                                                              jclass jContext,
                                                                                              jstring fullPathFileName,
                                                                                              jint minRadius,
                                                                                              jint maxRadius)
{
  try
  {
    sptAssertWithMessage(fullPathFileName != NULL, "Empty image path");
  
    cv::Mat src, src_gray;
    const char *nativeFullPathFileName = jEnv->GetStringUTFChars(fullPathFileName, 0);
    jfloatArray resultArray;
    int minDiameter = minRadius * 2;
    int maxDiameter = maxRadius * 2;
    jfloat temp[256] = {0};
    int countResult = 0;
    int sliderPos = 140;
    
    // Read in input image
    src = cv::imread( nativeFullPathFileName, 1 );
    
    if ( src.data )
    {
      /// Convert it to gray 
      cv::cvtColor( src, src_gray, CV_BGR2GRAY );
      
      cv::vector<vector<cv::Point> > contours;
      cv::Mat bimage = src_gray >= sliderPos;
      
      cv::findContours(bimage, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);
      
      for(size_t i = 0; i < contours.size(); i++)
      {
        size_t count = contours[i].size();
        if( count < 6 )
            continue;
        
        cv::Mat pointsf;
        cv::Mat(contours[i]).convertTo(pointsf, CV_32F);
        cv::RotatedRect box = fitEllipse(pointsf);
        
        if( MAX(box.size.width, box.size.height) > MIN(box.size.width, box.size.height)*30 )
            continue;
        
        // We get the box width instead of height simply because height has shadows while width does not have.    
        float finalLength = box.size.width;
        if (finalLength >= minDiameter && finalLength <= maxDiameter)
        {        
          temp[countResult++] = finalLength;
        }
      } 
      
      if (countResult > 0)
      {
        // Initialize int array
        resultArray = jEnv->NewFloatArray(countResult);
        
        // move from the temp structure to the java structure
        jEnv->SetFloatArrayRegion(resultArray, 0, countResult, temp);
      }
      else
        resultArray = jEnv->NewFloatArray(0);
    }
    else
      resultArray = jEnv->NewFloatArray(0);
    
    return resultArray;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return NULL;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return NULL;
  }
}

/**
 * nativeCalculateSharpnessProfileExecute
 *
 * Calculate sharpness profile value using HaarWaveletTransformPlugin.dll
 *
 * @author Lim Lay Ngor
 */
JNIEXPORT jdouble JNICALL Java_com_axi_util_image_ImageFeatureExtraction_nativeCalculateSharpnessProfileExecute(JNIEnv *jEnv,
																									   jclass jContext,
																									   jintArray jSourceImage)
{
	try
	{
		JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);		
		jdouble result = VxImageAnalyser::CalculateVariance(static_cast<float*>(pSourceImage->address), pSourceImage->stride, pSourceImage->width, pSourceImage->height);
		//jdouble result = Vx::CalculateVariance(reintepret_cast<unsigned char*>(pSourceImage->address), pSourceImage->stride, pSourceImage->width, pSourceImage->height);
		//jdouble result = Vx::CalculateVariance("C:\\top_high_4962_4606_269_272_0.jpg");

		return result;
	}
	catch (AssertException& aex)
	{
		raiseJavaAssertWithMessage(aex.getMessage());
		return NULL;
	}
	catch (...)
	{
		raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
		return NULL;
	}
}
