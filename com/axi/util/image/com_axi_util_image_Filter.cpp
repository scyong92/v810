#include <jni.h>

#include "com_axi_util_image_Filter.h"

#include "ippcore.h"
#include "ippi.h"
#include "ippcv.h"

#include "jniImageUtil.h"

#include "sptAssert.h"

#include "StringUtil.h"

#include <string>

#include "malloc.h"
#include "Windows.h"

using std::string;

/**
 * WARNING: If you change any of these enum values, you must also be sure to update the corresponding values in
 *          com/axi/util/image/MatchTemplateTechniqueEnum.java.
 */
enum MatchTemplateTechniqueEnum
{
  SUM_SQUARED_DISTANCES = 0,
  CROSS_CORRELATION_NO_PIXEL_VALUE_LEVELING = 1,
  CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING = 2,
};

/**
 * nativeConvolveRows
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Filter_nativeConvolveRows(JNIEnv *jEnv,
                                                                             jclass jContext,
                                                                             jintArray jSourceImage,
                                                                             jint offsetX,
                                                                             jint offsetY,
                                                                             jintArray jDestImage,
                                                                             jfloatArray jKernelArray)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);

    // Get access to a raw pointer for the data.
    int kernelSize = jEnv->GetArrayLength(jKernelArray);
    float *pKernel = reinterpret_cast<float*>(jEnv->GetPrimitiveArrayCritical(jKernelArray, 0));

    IppStatus ippResult = ippiFilterRow_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),
                                                pSourceImage->stride,
                                                pDestImage->address,
                                                pDestImage->stride,
                                                pDestImage->getSize(),
                                                pKernel,
                                                kernelSize,
                                                kernelSize - 1); // kernel orientation is from the LR, always anchor to the leftmost pixel.

    // Release access to the raw pointer.
    jEnv->ReleasePrimitiveArrayCritical(jKernelArray, pKernel, 0);
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);

    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}


/**
 * nativeConvolveColumns
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Filter_nativeConvolveColumns(JNIEnv *jEnv,
                                                                                jclass jContext,
                                                                                jintArray jSourceImage,
                                                                                jint offsetX,
                                                                                jint offsetY,
                                                                                jintArray jDestImage,
                                                                                jfloatArray jKernelArray)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);

    // Get access to a raw pointer for the data.
    int kernelSize = jEnv->GetArrayLength(jKernelArray);
    float *pKernel = reinterpret_cast<float*>(jEnv->GetPrimitiveArrayCritical(jKernelArray, 0));

    IppStatus ippResult = ippiFilterColumn_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),
                                                   pSourceImage->stride,
                                                   pDestImage->address,
                                                   pDestImage->stride,
                                                   pDestImage->getSize(),
                                                   pKernel,
                                                   kernelSize,
                                                   kernelSize - 1); // kernel orientation is from the LR, always anchor to the leftmost pixel.

    // Release access to the raw pointer.
    jEnv->ReleasePrimitiveArrayCritical(jKernelArray, pKernel, 0);
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);

    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeConvolveKernel
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Filter_nativeConvolveKernel(JNIEnv *jEnv,
                                                                               jclass jContext,
                                                                               jintArray jSourceImage,
                                                                               jint offsetX,
                                                                               jint offsetY,
                                                                               jintArray jKernelImage,
                                                                               jintArray jDestImage)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    JNIImageData *pKernelImage = getImageDataFromJava(jEnv, jKernelImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);

    IppiSize roiSize = { pDestImage->width + pKernelImage->width - 1, pDestImage->height + pKernelImage->height - 1 };

    IppStatus ippResult = ippiConvValid_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),
                                                pSourceImage->stride,
                                                roiSize,
                                                pKernelImage->address,
                                                pKernelImage->stride,
                                                pKernelImage->getSize(),
                                                pDestImage->address,
                                                pDestImage->stride);

    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));

    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    releaseImageDataFromJavaUnmodified(jEnv, jKernelImage, pKernelImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}


/**
 * nativeBorderReplicate
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Filter_nativeBorderReplicate(JNIEnv *jEnv,
                                                                                jclass jContext,
                                                                                jintArray jSourceImage,
                                                                                jintArray jDestImage,
                                                                                jint anchorX,
                                                                                jint anchorY)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);

    IppStatus ippResult = ippiCopyReplicateBorder_32s_C1R(reinterpret_cast<Ipp32s const *>(pSourceImage->address),
                                                          pSourceImage->stride,
                                                          pSourceImage->getSize(),
                                                          reinterpret_cast<Ipp32s *>(pDestImage->address),
                                                          pDestImage->stride,
                                                          pDestImage->getSize(),
                                                          anchorY,
                                                          anchorX);

    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);

    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeConvolveSobel
 *
 * @param direction 0 for horizontal, 1 for vertical
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Filter_nativeConvolveSobel(JNIEnv *jEnv,
                                                                              jclass jContext,
                                                                              jintArray jSourceImage,
                                                                              jint offsetX,
                                                                              jint offsetY,
                                                                              jintArray jDestImage,
                                                                              jint roiWidth,
                                                                              jint roiHeight,
                                                                              jint direction)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);

    sptAssert(direction == 0 || direction == 1);

    IppiSize roiSize = {roiWidth, roiHeight};

    // The ippiFilterSobel*Border require some additional space for computation. Allocate that space.
    int bufferSize;
    IppStatus ippResult;
    if (direction == 0)
      ippResult = ippiFilterSobelHorizGetBufferSize_32f_C1R(roiSize, ippMskSize3x3, &bufferSize);
    else
      ippResult = ippiFilterSobelVertGetBufferSize_32f_C1R(roiSize, ippMskSize3x3, &bufferSize);
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
    sptAssert(bufferSize > 0);


    int workingBufferStride = 0; // don't really care about this.
    Ipp8u *workingBuffer = ippiMalloc_8u_C1(bufferSize, 1, &workingBufferStride);
    sptAssert(workingBuffer != NULL);

    // Execute the kernel.
    // We are using the *Border functions so that we don't need to perform
    // an extra copy of the image when the roi is too close to the edge.
    if (direction == 0)
    {
      ippResult = ippiFilterSobelHorizBorder_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),
                                                     pSourceImage->stride,
                                                     pDestImage->address,
                                                     pDestImage->stride,
                                                     roiSize,
                                                     ippMskSize3x3, // use a 3x3 Sobel kernel
                                                     ippBorderRepl, // Replicate the needed border pixels
                                                     0.0f, // not used for replication mode
                                                     workingBuffer);
    }
    else
    {
      ippResult = ippiFilterSobelVertBorder_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),
                                                    pSourceImage->stride,
                                                    pDestImage->address,
                                                    pDestImage->stride,
                                                    roiSize,
                                                    ippMskSize3x3, // use a 3x3 Sobel kernel
                                                    ippBorderRepl, // Replicate the needed border pixels
                                                    0.0f, // not used for replication mode
                                                    workingBuffer);
    }
    ippiFree(workingBuffer);

    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);

    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeMax
 * Applies a 'maximum' kernel (aka dialate) of the specified size to each pixel in the region of interest.
 * The anchor pixel is assigned to the maximum value within that kernel.
 *
 * This is different from Statistics.max() which returns the single maximum pixel within the region of interest.
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Filter_nativeMax(JNIEnv *jEnv,
                                                                    jclass jContext,
                                                                    jintArray jSourceImage,
                                                                    jint offsetX,
                                                                    jint offsetY,
                                                                    jintArray jDestImage,
                                                                    jint kernelWidth,
                                                                    jint kernelHeight,
                                                                    jint anchorPointX,
                                                                    jint anchorPointY)
{
  try
  {
    sptAssert(kernelWidth > 0);
    sptAssert(kernelHeight > 0);
    sptAssert(anchorPointX < kernelWidth && anchorPointX >= 0);
    sptAssert(anchorPointY < kernelHeight && anchorPointY >= 0);

    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);

    IppiSize kernelSize = {kernelWidth, kernelHeight};
    IppiPoint anchorPoint = {anchorPointX, anchorPointY};

    // allocate a buffer to handle the borders.
    int bufferSize = 0;
    IppStatus ippResult = ippiFilterMaxGetBufferSize_32f_C1R(pDestImage->width, kernelSize, &bufferSize);
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));

    int bufferSizeStride = 0; // don't really care about this.
    Ipp8u *buffer = ippiMalloc_8u_C1(bufferSize, 1, &bufferSizeStride);

    ippResult = ippiFilterMaxBorderReplicate_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),
                                                     pSourceImage->stride,
                                                     pDestImage->address, pDestImage->stride,
                                                     pDestImage->getSize(),
                                                     kernelSize,
                                                     anchorPoint,
                                                     buffer);

    ippiFree(buffer);

    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);

    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}


/**
 * nativeMin
 * Applies a 'minimum' kernel (aka dialate) of the specified size to each pixel in the region of interest.
 * The anchor pixel is assigned to the maximum value within that kernel.
 *
 * This is different from Statistics.min() which returns the single minimum pixel within the region of interest.
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Filter_nativeMin(JNIEnv *jEnv,
                                                                    jclass jContext,
                                                                    jintArray jSourceImage,
                                                                    jint offsetX,
                                                                    jint offsetY,
                                                                    jintArray jDestImage,
                                                                    jint kernelWidth,
                                                                    jint kernelHeight,
                                                                    jint anchorPointX,
                                                                    jint anchorPointY)
{
  try
  {
    sptAssert(kernelWidth > 0);
    sptAssert(kernelHeight > 0);
    sptAssert(anchorPointX < kernelWidth && anchorPointX >= 0);
    sptAssert(anchorPointY < kernelHeight && anchorPointY >= 0);

    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);

    IppiSize kernelSize = {kernelWidth, kernelHeight};
    IppiPoint anchorPoint = {anchorPointX, anchorPointY};

    // allocate a buffer to handle the borders.
    int bufferSize = 0;
    IppStatus ippResult = ippiFilterMaxGetBufferSize_32f_C1R(pDestImage->width, kernelSize, &bufferSize);
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));

    int bufferSizeStride = 0; // don't really care about this.
    Ipp8u *buffer = ippiMalloc_8u_C1(bufferSize, 1, &bufferSizeStride);

    ippResult = ippiFilterMinBorderReplicate_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),
                                                     pSourceImage->stride,
                                                     pDestImage->address,
                                                     pDestImage->stride,
                                                     pDestImage->getSize(),
                                                     kernelSize,
                                                     anchorPoint,
                                                     buffer);

    ippiFree(buffer);

    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);

    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeMedian
 *
 * IPP does not provide any 32f implementations of its Median filter!
 * We convert to/from 8u for this method.
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Filter_nativeMedian(JNIEnv *jEnv,
                                                                       jclass jContext,
                                                                       jintArray jSourceImage,
                                                                       jint offsetX,
                                                                       jint offsetY,
                                                                       jintArray jDestImage,
                                                                       jint kernelWidth,
                                                                       jint kernelHeight)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    sptAssert(kernelWidth > 0);
    sptAssert(kernelHeight > 0);
    sptAssert(offsetX >= 0);
    sptAssert(offsetY >= 0);

    int sourceStepBytes;
    Ipp8u *pSource8u = ippiMalloc_8u_C1(pDestImage->width + kernelWidth - 1, pDestImage->height + kernelHeight - 1, &sourceStepBytes);
    int destStepBytes;
    Ipp8u *pDest8u = ippiMalloc_8u_C1(pDestImage->width, pDestImage->height, &destStepBytes);

    sptAssert(pSource8u != NULL);
    sptAssert(pDest8u != NULL);

    IppiSize sourceCopySize = {pDestImage->width + kernelWidth - 1, pDestImage->height + kernelHeight - 1};

    IppiSize roiSize = pDestImage->getSize();

    // Convert to 8u
    IppStatus ippResult = ippiConvert_32f8u_C1R(pSourceImage->addressAt(offsetX, offsetY),
                                                pSourceImage->stride,
                                                pSource8u,		// 8bit source
                                                sourceStepBytes,  // the step for the row; might not be width due to ipp optimizations
                                                sourceCopySize,	// the size of the image
                                                ippRndNear); // round to nearest integer
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));

    if (kernelWidth == 1 && (kernelHeight == 3 || kernelHeight == 5))
    {
      // use the vertical Median method
      IppiMaskSize mask;
      int roiCorrection;
      if (kernelHeight == 3)
      {
        mask = ippMskSize1x3;
        roiCorrection = 1;
      }
      else
      {
        mask = ippMskSize1x5;
        roiCorrection = 2;
      }

      // the additional pointer offset is to compensate for the fact that this filter anchors at the midpoint of the filter rather than 0, 0, which is
      // assumed by the ROI
      ippResult = ippiFilterMedianVert_8u_C1R(pSource8u + roiCorrection*sourceStepBytes, sourceStepBytes, pDest8u, destStepBytes, roiSize, mask);
    }
    else if (kernelHeight == 1 && (kernelWidth == 3 || kernelWidth == 5))
    {
      // use the horizontal Median method
      IppiMaskSize mask;
      int roiCorrection;
      if (kernelWidth == 3)
      {
        mask = ippMskSize3x1;
        roiCorrection = 1;
      }
      else
      {
        mask = ippMskSize5x1;
        roiCorrection = 2;
      }

      // the additional pointer offset is to compensate for the fact that this filter anchors at the midpoint of the filter rather than 0, 0, which is
      // assumed by the ROI
      ippResult = ippiFilterMedianHoriz_8u_C1R(pSource8u + roiCorrection, sourceStepBytes, pDest8u, destStepBytes, roiSize, mask);
    }
    else
    {
      sptAssert(kernelWidth > 0);
      sptAssert(kernelHeight > 0);

      // kernel dimensions must be odd.
      sptAssert(kernelWidth % 2 == 1);
      sptAssert(kernelHeight % 2 == 1);

      // use the general Median method
      IppiPoint anchor = {0, 0}; // always anchor at zero, zero -- for now, perhaps add it as an argument.
      IppiSize maskSize = {kernelWidth, kernelHeight};
      ippResult = ippiFilterMedian_8u_C1R(pSource8u,
                                          sourceStepBytes,
                                          pDest8u,
                                          destStepBytes,
                                          roiSize,
                                          maskSize,
                                          anchor);
    }

    ippiFree(pSource8u);
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));

    // convert back to 32f
    ippResult = ippiConvert_8u32f_C1R(pDest8u, destStepBytes, pDestImage->address, pDestImage->stride, roiSize);

    ippiFree(pDest8u);

    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);

    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}



/**
 * nativeConvolveLowpass
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Filter_nativeConvolveLowpass(JNIEnv *jEnv,
                                                                                jclass jContext,
                                                                                jintArray jSourceImage,
                                                                                jint offsetX,
                                                                                jint offsetY,
                                                                                jintArray jDestImage,
                                                                                jint roiWidth,
                                                                                jint roiHeight,
                                                                                jint kernelWidth)
{
  try
  {
    sptAssert(kernelWidth == 3 || kernelWidth == 5);

    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);

    IppiSize roiSize = {roiWidth, roiHeight};
    IppiMaskSize maskSize = (kernelWidth == 3 ? ippMskSize3x3 : ippMskSize5x5);

    // The ippiFilter*Border require some additional space for computation. Allocate that space.
    int bufferSize;
    IppStatus ippResult = ippiFilterLowpassGetBufferSize_32f_C1R(roiSize, maskSize, &bufferSize);

    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
    sptAssert(bufferSize > 0);

    int workingBufferStride = 0;
    Ipp8u *workingBuffer = ippiMalloc_8u_C1(bufferSize, 1, &workingBufferStride);
    sptAssert(workingBuffer != NULL);

    // Execute the kernel.
    // We are using the *Border functions so that we don't need to perform
    // an extra copy of the image when the roi is too close to the edge.
    ippResult = ippiFilterLowpassBorder_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),
                                                pSourceImage->stride,
                                                pDestImage->address,
                                                pDestImage->stride,
                                                roiSize,
                                                maskSize,
                                                ippBorderRepl, // Replicate the needed border pixels
                                                0.0f, // not used for replication mode
                                                workingBuffer);

    ippiFree(workingBuffer);

    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);

    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * @author John Heumann
 *
 * This routine is used only for debugging an IPP out of memory error
 * Once that's taken care of, it can be safely removed.
 */
void appendHeapInfo(string &msg, bool compactHeaps)
{
  //get all the heaps in the process
  HANDLE heaps [100];
  DWORD numHeaps = ::GetProcessHeaps (100, heaps);

  //get the default heap and the CRT heap (both are among
  //those retrieved above)
  const HANDLE default_heap = ::GetProcessHeap ();
  const HANDLE crt_heap = (HANDLE) _get_heap_handle ();
  unsigned long totalHeapUsed = 0;
  unsigned long totalHeapFree = 0;

  for (unsigned int i = 0; i < numHeaps; i++) {
    if (compactHeaps)
      HeapCompact(heaps[i], 0);

    //query the heap attributes
    ULONG heap_info = 0;
    SIZE_T ret_size = 0;
    if (HeapQueryInformation (heaps[i],
              HeapCompatibilityInformation,
              &heap_info,
              sizeof (heap_info),
              &ret_size)) {

      //walk the heap and total the size of each allocated block inside it
      PROCESS_HEAP_ENTRY entry;
      memset (&entry, 0, sizeof (entry));
      unsigned long nUsed = 0;
      unsigned long heapUsed = 0;
      unsigned long nFree = 0;
      unsigned long heapFree = 0;
      unsigned long largestFree = 0;
      while (HeapWalk (heaps[i], &entry)) {
    if (entry.wFlags & PROCESS_HEAP_ENTRY_BUSY) {
      heapUsed += entry.cbData;
      nUsed++;
    }
    else if (entry.wFlags & PROCESS_HEAP_UNCOMMITTED_RANGE) {
      heapFree += entry.cbData;
      nFree++;
      if (entry.cbData > largestFree)
        largestFree = entry.cbData;
    }
      }
      msg += "|Heap number: " + StringUtil::intToString(i + 1);           
      msg += " Heap type: " + StringUtil::intToString(heap_info);           
      msg += " Default process heap? ";
      if (heaps[i] == default_heap)
    msg += "1";
      else 
    msg += "0";
      msg += " Crt heap? ";
      if (heaps[i] == crt_heap)
    msg += "1";
      else
    msg += "0";
      msg += " Blocks used: " + StringUtil::intToString(nUsed);           
      msg += " Bytes used: " + StringUtil::intToString(heapUsed);           
      msg += " Blocks free: " + StringUtil::intToString(nFree);
      msg += " Bytes free: " + StringUtil::intToString(heapFree);
      msg += " Largest free block: " + StringUtil::intToString(largestFree);           
      totalHeapUsed += heapUsed;
      totalHeapFree += heapFree;
    }    
  }
  msg += "|Total heap used: " + StringUtil::intToString(totalHeapUsed);
  msg += " Total heap free: " + StringUtil::intToString(totalHeapFree);
}

/**
 * nativeMatchTemplate
 *
 * @author Patrick Lacz
 * @author Matt Wharton
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Filter_nativeMatchTemplate(JNIEnv *jEnv,
                                                                              jclass jContext,
                                                                              jintArray jSourceImage,
                                                                              jint offsetX,
                                                                              jint offsetY,
                                                                              jintArray jDestImage,
                                                                              jintArray jTemplateImage,
                                                                              jint roiWidth,
                                                                              jint roiHeight,
                                                                              jint matchTemplateTechnique)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    JNIImageData *pTemplateImage = getImageDataFromJava(jEnv, jTemplateImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);

    IppiSize roiSize = { roiWidth, roiHeight };
    IppiSize templateSize = pTemplateImage->getSize();
    IppiSize sourceSize = pSourceImage->getSize();
    IppiSize destSize = pDestImage->getSize();

    IppStatus ippResult = static_cast<IppStatus>(0);
    switch (matchTemplateTechnique)
    {
      case SUM_SQUARED_DISTANCES:
        ippResult = ippiSqrDistanceValid_Norm_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),            // image to search
                                                      pSourceImage->stride,
                                                      roiSize,		                                    // size of region to search
                                                      pTemplateImage->address,                              // image to match
                                                      pTemplateImage->stride,
                                                      templateSize,		                            // match the whole template image.
                                                      pDestImage->address,                                  // destination image
                                                      pDestImage->stride);
        break;

      case CROSS_CORRELATION_NO_PIXEL_VALUE_LEVELING:
        ippResult = ippiCrossCorrValid_Norm_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),              // image to search
                                                    pSourceImage->stride,
                                                    roiSize,		                                    // size of region to search
                                                    pTemplateImage->address,                                // image to match
                                                    pTemplateImage->stride,
                                                    templateSize,			                    // match the whole template image.
                                                    pDestImage->address,                                    // destination image
                                                    pDestImage->stride);
      break;

      case CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING:
        ippResult = ippiCrossCorrValid_NormLevel_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),         // image to search
                                                         pSourceImage->stride,
                                                         roiSize,		                            // size of region to search
                                                         pTemplateImage->address,                           // image to match
                                                         pTemplateImage->stride,
                                                         templateSize,	                                    // match the whole template image.
                                                         pDestImage->address,                               // destination image
                                                         pDestImage->stride);
        break;

      default:
        sptAssertWithMessage(false, "Unexpected match template technique!");
        break;
    }

    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    releaseImageDataFromJavaUnmodified(jEnv, jTemplateImage, pTemplateImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);

    //TODO: Following messages and heap walking code is purely for debugging
    // an intermittent IPP out of memory error. When this issue is resolved,
    // the following should be commented out or deleted.
    if (ippResult != 0)
    {
      string msg = "";
      msg += ippGetStatusString(ippResult);
      msg += "|source width: " + StringUtil::intToString(sourceSize.width);
      msg += " source height: " + StringUtil::intToString(sourceSize.height);
      msg += "|template width: " + StringUtil::intToString(templateSize.width);
      msg += " template height: " + StringUtil::intToString(templateSize.height);
      msg += "|dest width: " + StringUtil::intToString(destSize.width);
      msg += " dest height: " + StringUtil::intToString(destSize.height);
      msg += "|offsetX: " + StringUtil::intToString(offsetX);
      msg += " offsetY: " + StringUtil::intToString(offsetY);
      msg += "|roiWidth: " + StringUtil::intToString(roiWidth);
      msg += " roiHeight: " + StringUtil::intToString(roiHeight);
      msg += "|Before compaction: ";
      appendHeapInfo(msg, false);
      msg += "|After compaction: ";
      appendHeapInfo(msg, true);


      // Now that we've compacted the heaps, try matching again...
      switch (matchTemplateTechnique)
      {
        case SUM_SQUARED_DISTANCES:
          ippResult = ippiSqrDistanceValid_Norm_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),          // image to search
                                                        pSourceImage->stride,
                                                        roiSize,		                            // size of region to search
                                                        pTemplateImage->address,                            // image to match
                                                        pTemplateImage->stride,
                                                        templateSize,		                            // match the whole template image.
                                                        pDestImage->address,                                // destination image
                                                        pDestImage->stride);
          break;
  
        case CROSS_CORRELATION_NO_PIXEL_VALUE_LEVELING:
          ippResult = ippiCrossCorrValid_Norm_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),            // image to search
                                                      pSourceImage->stride,
                                                      roiSize,		                                    // size of region to search
                                                      pTemplateImage->address,                              // image to match
                                                      pTemplateImage->stride,
                                                      templateSize,			                    // match the whole template image.
                                                      pDestImage->address,                                  // destination image
                                                      pDestImage->stride);
        break;
  
        case CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING:
          ippResult = ippiCrossCorrValid_NormLevel_32f_C1R(pSourceImage->addressAt(offsetX, offsetY),       // image to search
                                                           pSourceImage->stride,
                                                           roiSize,		                            // size of region to search
                                                           pTemplateImage->address,                         // image to match
                                                           pTemplateImage->stride,
                                                           templateSize,	                            // match the whole template image.
                                                           pDestImage->address,                             // destination image
                                                           pDestImage->stride);
          break;
  
        default:
          sptAssertWithMessage(false, "Unexpected match template technique!");
          break;
      }
      // If matching still fails, assert and print the enormous message
      if (ippResult != 0)
        sptAssertWithMessage(false, msg);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * FilterColumn
 *
 * A helper function to reduce the clutter in nativeLocateRectangle
 * @todo Convert to use the already existing native filter column function.
 *
 * @author Patrick Lacz
 */
static IppStatus FilterColumn(Ipp32f const *pSource, int sourceStride, Ipp32f *pDest, int destStride, IppiSize destSize, int kernelSize, Ipp32f value)
{
  int kernelStride = 0;
  Ipp32f *pKernel = ippiMalloc_32f_C1(kernelSize, 1, &kernelStride);
  std::fill_n(pKernel, kernelSize, value);

  // for FilterColumn/Row, the achor point is from the LR, not the UL.
  int anchor = kernelSize-1;

  IppStatus ippResult = ippiFilterColumn_32f_C1R(
                                                 pSource,
                                                 sourceStride,	// stride is in bytes
                                                 pDest,
                                                 destStride,
                                                 destSize,
                                                 pKernel, kernelSize, anchor);

  // clean up
  ippiFree(pKernel);

  return ippResult;
}

/**
 * nativeMarchingConvolveUniform1DKernel
 *
 * Instead of actually using a 1d kernel, we keep track of two frontiers and a running
 * sum. This is noticably faster for large kernel sizes. (2xN^2 instead of kxN^2)
 *
 * @param direction 0 = horizontal along X, 1 = vertical along Y
 * @todo optimize with IPP
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Filter_nativeMarchingConvolveUniform1DKernel(JNIEnv *jEnv,
                                                                                                jclass context,
                                                                                                jintArray jSourceImage,
                                                                                                jint offsetX,
                                                                                                jint offsetY,
                                                                                                jintArray jDestImage,
                                                                                                jint roiWidth,
                                                                                                jint roiHeight,
                                                                                                jint kernelLength,
                                                                                                jfloat kernelValue,
                                                                                                jint direction)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);

    int alongSourceKernelStep = 0, acrossSourceKernelStep = 0;
    int alongDestKernelStep = 0, acrossDestKernelStep = 0;
    int numberOfRows = 0, numberOfColumns = 0;
    if (direction == 0)
    {
      alongSourceKernelStep = 1;
      acrossSourceKernelStep = pSourceImage->stride/sizeof(Ipp32f);
      alongDestKernelStep = 1;
      acrossDestKernelStep = pDestImage->stride/sizeof(Ipp32f);
      numberOfRows = roiHeight;
      numberOfColumns = roiWidth;
    }
    else if (direction == 1)
    {
      alongSourceKernelStep = pSourceImage->stride/sizeof(Ipp32f);
      acrossSourceKernelStep = 1;
      alongDestKernelStep = pDestImage->stride/sizeof(Ipp32f);
      acrossDestKernelStep = 1;
      numberOfRows = roiWidth;
      numberOfColumns = roiHeight;
    }
    else
      sptAssertWithMessage(false, "Invalid value for direction.");


    // This 'row' may actually be a column, but I couldn't think of a nice descriptive word for that.
    // The 'row' is across the kernel, meaning that the kernel does not look at pixels on different 'rows'
    // The 'column' is along the kernel, meaning that the kernel looks at (kernelLength) pixels on this row.
    for (int row = 0 ; row < numberOfRows ; ++row)
    {
      Ipp32f *pSourceBase = pSourceImage->addressAt(offsetX, offsetY) + row * acrossSourceKernelStep;
      Ipp32f *pLeadingFrontier = pSourceBase;
      Ipp32f *pSourceEnd = pSourceBase + numberOfColumns * alongSourceKernelStep;
      Ipp32f *pDestIterator = pDestImage->address + row * acrossDestKernelStep;

      // compute the initial value of the kernel
      double sum = 0.0;
      for ( int column = 0 ; column < kernelLength ; ++column)
      {
        sum += *pLeadingFrontier;
        pLeadingFrontier += alongSourceKernelStep;
      }
      // record this first value.
      *pDestIterator = static_cast<float>(sum * kernelValue);
      pDestIterator += alongDestKernelStep;

      Ipp32f *pTrailingFrontier = pSourceBase;

      while (pLeadingFrontier < pSourceEnd)
      {
        // compute the next kernel summation
        sum += *pLeadingFrontier;
        sum -= *pTrailingFrontier;

        // write the new result
        *pDestIterator = static_cast<float>(sum * kernelValue);

      // iterate to the next position.
        pLeadingFrontier += alongSourceKernelStep;
        pTrailingFrontier += alongSourceKernelStep;
        pDestIterator += alongDestKernelStep;
      }
    }

    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeMarchingConvolutionForLocateRectangle
 * The 'Marching Convolution' algorithm below is an optimization for uniform box filters:
 *
 * Each pixel result can be written simply as the sum of the previous pixel and some other data.
 * For instance, to average this signal:
 * <pre>
 *    2  5  3  1  7  4  8  3  5  3  7  9
 * </pre>
 * With this uniform box filter:
 * <pre>
 *   .2 .2 .2 .2 .2
 * </pre>
 *
 * This filter can be computed in a different way, tracking a running sum and sampling from two points.
 * After the first value is found (3.6), the results of the following filter can be added to it for the next value.
 * <pre>
 *  -.2             +.2  ( + 3.6 = 4.0 )
 *     -.2             +.2  ( + 4.0 = 4.6)
 * </pre>
 *
 * The idea is that the forward frontier is added and the trailing frontier is subtracted. When combined with an
 * inner region, such as the hole region, we can apply the same technique: add the _difference_ between -h and r
 * along the hole's forward frontier, and subtract the _difference_ along the hole's trailing frontier.
 *
 * The hole and the ring have different sizes, so we generate two results from the first seperable step. In the
 * code these are refered to as the 'hole result' or 'H' and the 'ring result' or 'R'.
 *
 * The recursive forumla used is:
 * <pre>
 *  f(x,y+1) = f(x,y)
 *    + 1/|R| * (R(x-borderWidth, y+ringHeight-borderHeight) - R(x-borderWidth, y-borderHeight))
 *    + (-1/|H| - 1/|R|) * (H(x, y+holeHeight) - H(x, y))
 * </pre>
 *
 * Choosing to filter first along the X direction then Y allows us to get better performance from the
 * IPP FilterRow calls used in the first part of computation. The performance in this native section is
 * about equal regardless of convolving in X or Y.
 *
 * The implementation organizes the images so that the borderWidth/Height corrections are unnecessary.
 * This formula assume that R and H were created with 1's at every location; our code assumes that
 * 1/|R| was used for 'R' and (-1/|H|-1/|R|) is used for 'H', essentially premultiplying the values in the equation
 * above.
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Filter_nativeMarchingConvolutionForLocateRectangle(JNIEnv *jEnv,
                                                                                                      jclass context,
                                                                                                      jintArray jRingResultImage,
                                                                                                      jintArray jHoleResultImage,
                                                                                                      jintArray jDestImage,
                                                                                                      jint holeWidth,
                                                                                                      jint ringWidth)
{
  try
  {
    JNIImageData *pRingResultImage = getImageDataFromJava(jEnv, jRingResultImage);
    JNIImageData *pHoleResultImage = getImageDataFromJava(jEnv, jHoleResultImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);

    /**
     * Compute the first values (or 'base case') for the left-hand side of the result image.
     * This is just the sum of the first (width) entries for each row.
     */

    // Allocate space for some temporary results

    int resultStrideInBytes = 0;
    Ipp32f *pFirstRowRing = ippiMalloc_32f_C1(pDestImage->width, 1, &resultStrideInBytes);
    Ipp32f *pFirstRowHole = ippiMalloc_32f_C1(pDestImage->width, 1, &resultStrideInBytes);

    sptAssert(pFirstRowRing != NULL);
    sptAssert(pFirstRowHole != NULL);

    IppiSize firstRowSize = { pDestImage->width, 1 };

    IppStatus ippResult = FilterColumn(pRingResultImage->address,
                                       pRingResultImage->stride,
                                       pFirstRowRing,
                                       resultStrideInBytes,
                                       firstRowSize,
                                       ringWidth,
                                       1.f);
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));

    ippResult = FilterColumn(pHoleResultImage->address,
                             pHoleResultImage->stride,
                             pFirstRowHole,
                             resultStrideInBytes,
                             firstRowSize,
                             holeWidth,
                             1.f);
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));

    ippResult = ippiAdd_32f_C1R(pFirstRowRing,
                                resultStrideInBytes,
                                pFirstRowHole,
                                resultStrideInBytes,
                                pDestImage->address,
                                pDestImage->stride,
                                firstRowSize);
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));

    ippiFree(pFirstRowRing);
    ippiFree(pFirstRowHole);

    /**
     * Run the recursive definition of this filter by marching along each column.
     */

    // \TODO: PL: Should be able to use ipps' IIRSparse functions here.
    for (int c = 0 ; c < pDestImage->width ; ++c)
    {
      for (int r = 0 ; r < (pDestImage->height-1) ; ++r)
      {
        Ipp32f *pPrevious = pDestImage->addressAt(c, r);
        Ipp32f *pAssignTo = pDestImage->addressAt(c, r+1);

        Ipp32f *pRingTail = pRingResultImage->addressAt(c, r);
        Ipp32f *pRingHead = pRingResultImage->addressAt(c, r+ringWidth);

        Ipp32f *pHoleTail = pHoleResultImage->addressAt(c, r);
        Ipp32f *pHoleHead = pHoleResultImage->addressAt(c, r+holeWidth);

        *pAssignTo = *pPrevious  + *pRingHead - *pRingTail + *pHoleHead - *pHoleTail;
      }
    }

    releaseImageDataFromJavaUnmodified(jEnv, jRingResultImage, pRingResultImage);
    releaseImageDataFromJavaUnmodified(jEnv, jHoleResultImage, pHoleResultImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}
