package com.axi.util.image;

/**
 * ResizeImage - It is an image processing method resize the image base on input scale factor
 *
 * @param _scaleX, _scaleY 
 *       - Factors by which the x and y dimensions of the source ROI are changed. 
 *         The factor value greater than 1 corresponds to increasing the image size in that dimension.
 * @param _resizeMethod - Resize interpolation method, could be linear or cubic.
 * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
 */
public class ResizeImage extends ImageEnhancerBase
{   
  public float _scaleX = 1.5f;
  public float _scaleY = 1.5f;
  public InterpolationModeEnum _resizeMethod = InterpolationModeEnum.Linear;

  public ResizeImage(float scaleX, float scaleY, InterpolationModeEnum resizeMethod)
  {
    this._scaleX = scaleX;
    this._scaleY = scaleY;
    this._resizeMethod = resizeMethod;
  }
  
  public ResizeImage(float scaleX, float scaleY)
  {
    this._scaleX = scaleX;
    this._scaleY = scaleY;
  }
  
 public ImageEnhancerType getType()
 {
  return ImageEnhancerType.Resize;  
 }
   
  public Image runEnhancement(Image dest, Image src)
  {
    //checking for null image
    if(_resizeMethod == InterpolationModeEnum.Linear)
       return ImageEnhancer.resizeLinear32bitsImage(dest, src, _scaleX, _scaleY);
    else if(_resizeMethod == InterpolationModeEnum.Cubic)
      return ImageEnhancer.resizeCubic32bitsImage(dest, src, _scaleX, _scaleY);
    else
      return null;
  }
  
  public void updateParameter(int scaleX, int scaleY, InterpolationModeEnum resizeMethod)
  {
    //checking scale != 0, method not null
    this._scaleX = scaleX;
    this._scaleY = scaleY;
    this._resizeMethod = resizeMethod;
  }  
  
  public void updateParameter(int scaleX, int scaleY)
  {
    this._scaleX = scaleX;
    this._scaleY = scaleY;
  }  
}
