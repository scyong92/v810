#include "jniImageUtil.h"

#include "sptAssert.h"

// The critical cals are supposed to be faster, with less likelyhood of a copy, but also may make the java engine block.
// If we pursue critical sections, we may want to immediately release after copying the data.
//#define USE_CRITICAL_JNI_CALLS

/**
 * @author Patrick Lacz
 */
JNIImageData *getImageDataFromJava(JNIEnv *jEnv, jintArray jImage)
{
  jboolean isCopy;
#ifdef USE_CRITICAL_JNI_CALLS
  JNIImageData *pImage = reinterpret_cast<JNIImageData*>(jEnv->GetPrimitiveArrayCritical(jImage, &isCopy));
#else
  JNIImageData *pImage = reinterpret_cast<JNIImageData*>(jEnv->GetIntArrayElements(jImage, &isCopy));
#endif
  sptAssert(pImage != NULL);
  return pImage;
}


/**
 * Use this function if you have modified the JNIImageData and want that change to be permanent.
 * @author Patrick Lacz
 */
void releaseImageDataFromJavaModified(JNIEnv *jEnv, jintArray jImage, JNIImageData *pImageData)
{
#ifdef USE_CRITICAL_JNI_CALLS
  jEnv->ReleasePrimitiveArrayCritical(jImage, reinterpret_cast<void*>(pImageData), 0);
#else
  jEnv->ReleaseIntArrayElements(jImage, reinterpret_cast<jint*>(pImageData), 0);
#endif
}

/**
 * Use this function if you have not modified the JNIImageData.
 * @author Patrick Lacz
 */
void releaseImageDataFromJavaUnmodified(JNIEnv *jEnv, jintArray jImage, JNIImageData *pImageData)
{
#ifdef USE_CRITICAL_JNI_CALLS
  jEnv->ReleasePrimitiveArrayCritical(jImage, reinterpret_cast<void*>(pImageData), JNI_ABORT);
#else
  jEnv->ReleaseIntArrayElements(jImage, reinterpret_cast<jint*>(pImageData), JNI_ABORT);
#endif
}