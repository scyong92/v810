package com.axi.util.image;

import com.axi.util.Enum;

/**
 * Specifies a technique for handling the areas near the edge of a region of interest (and in particular image)
 * when filtering with a kernel.
 *
 * @author Patrick Lacz
 */
public class BorderSamplingModeEnum extends Enum
{
  private static int _index = 0;

  // When the area sampled by the filter extends beyond the boundaries of the image, use the
  // closest pixel on the border of the image. This is the choice you want for most cases.
  // Returned image dimensions are equal to the specified region of interest.
  public static final BorderSamplingModeEnum REPLICATE_IMAGE_BORDER = new BorderSamplingModeEnum(++_index);

  // Sample only from the area within the region of interest. The dimensions of the
  // returned image will be (image size - kernel size + 1) for both width and height.
  public static final BorderSamplingModeEnum CONSTRAIN_TO_REGION = new BorderSamplingModeEnum(++_index);

  public BorderSamplingModeEnum(int id)
  {
    super(id);
  }
}
