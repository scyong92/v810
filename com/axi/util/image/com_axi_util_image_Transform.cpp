#include "com_axi_util_image_Transform.h"
#include "com/axi/util/jniUtil.h"
#include "sptAssert.h"

#include "ippcore.h"
#include "ippi.h"

#include "jniImageUtil.h"

/**
 * nativeFlip
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Transform_nativeFlip(JNIEnv *jEnv, 
                                                                        jclass jContext, 
                                                                        jintArray jSourceImage, 
                                                                        jint roiMinX, 
                                                                        jint roiMinY, 
                                                                        jint roiWidth, 
                                                                        jint roiHeight, 
                                                                        jint mode)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize ippiRoiSize = { roiWidth, roiHeight };
    
    IppiAxis ippiAxis;
    switch (mode)
    {
      case 0:
        ippiAxis = ippAxsHorizontal;
        break;
      case 1:
        ippiAxis = ippAxsVertical;
        break;
      case 2:
        ippiAxis = ippAxsBoth;
        break;
    }
    
    IppStatus ippResult = ippiMirror_32s_C1IR(reinterpret_cast<Ipp32s *>(pSourceImage->addressAt(roiMinX, roiMinY)), 
                                              pSourceImage->stride,
                                              ippiRoiSize,
                                              ippiAxis);
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeAffineQuad
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Transform_nativeAffineQuad(JNIEnv *jEnv, 
                                                                              jclass jContext, 
                                                                              jdoubleArray jMatrix, 
                                                                              jint quadMinX, 
                                                                              jint quadMinY, 
                                                                              jint quadWidth, 
                                                                              jint quadHeight, 
                                                                              jdoubleArray jResult4x2)
{
  try
  {
    sptAssert(jEnv->GetArrayLength(jMatrix) == 6);
    sptAssert(jEnv->GetArrayLength(jResult4x2) == 8);
    
    Ipp64f *t = reinterpret_cast<Ipp64f*>(jEnv->GetPrimitiveArrayCritical(jMatrix, 0));
    
    IppiRect srcRoi = { quadMinX, quadMinY, quadWidth, quadHeight };
    
    double quad[4][2];
    double transformMatrix[2][3] = { { t[0], t[2], t[4] }, { t[1], t[3], t[5] } };
    
    IppStatus ippResult = ippiGetAffineQuad(srcRoi, 
                                            quad,
                                            transformMatrix);
    
    // clean up
    jEnv->ReleasePrimitiveArrayCritical(jMatrix, reinterpret_cast<void*>(t), 0);
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    
    Ipp64f *result = reinterpret_cast<Ipp64f*>(jEnv->GetPrimitiveArrayCritical(jResult4x2, 0));
    for (int i = 0 ; i < 4 ; ++i)
    {
      result[i*2] = quad[i][0];
      result[i*2+1] = quad[i][1];
    }
    jEnv->ReleasePrimitiveArrayCritical(jResult4x2, reinterpret_cast<void*>(result), 0);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeAffineBounds
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Transform_nativeAffineBounds(JNIEnv *jEnv, 
                                                                                jclass jContext, 
                                                                                jdoubleArray jMatrix, 
                                                                                jint quadMinX, 
                                                                                jint quadMinY, 
                                                                                jint quadWidth, 
                                                                                jint quadHeight, 
                                                                                jdoubleArray jResult2x2)
{
  try
  {
    sptAssert(jEnv->GetArrayLength(jMatrix) == 6);
    sptAssert(jEnv->GetArrayLength(jResult2x2) == 4);
    
    Ipp64f *t = reinterpret_cast<Ipp64f*>(jEnv->GetPrimitiveArrayCritical(jMatrix, 0));
    
    IppiRect srcRoi = { quadMinX, quadMinY, quadWidth, quadHeight };
    
    double quad[2][2];
    double transformMatrix[2][3] = { { t[0], t[2], t[4] }, { t[1], t[3], t[5] } };
    
    IppStatus ippResult = ippiGetAffineBound(srcRoi, 
                                             quad,
                                             transformMatrix);
    
    // clean up
    jEnv->ReleasePrimitiveArrayCritical(jMatrix, reinterpret_cast<void*>(t), 0);
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    
    Ipp64f *result = reinterpret_cast<Ipp64f*>(jEnv->GetPrimitiveArrayCritical(jResult2x2, 0));
    result[0] = quad[0][0];
    result[1] = quad[0][1];
    result[2] = quad[1][0];
    result[3] = quad[1][1];
    jEnv->ReleasePrimitiveArrayCritical(jResult2x2, reinterpret_cast<void*>(result), 0);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeGetResizeTransform
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Transform_nativeGetResizeTransform(JNIEnv *jEnv, 
                                                                                      jclass jContext, 
                                                                                      jint originalWidth, 
                                                                                      jint originalHeight, 
                                                                                      jint newWidth, 
                                                                                      jint newHeight, 
                                                                                      jdoubleArray jMatrix)
{
  try
  {
    IppiRect srcRoi = {0, 0, originalWidth, originalHeight};
    
    double quad[][2] = {{0.0, 0.0}, {newWidth, 0.0}, {newWidth, newHeight}, {0.0, newHeight}};
    double coeffs[2][3];
    
    IppStatus ippResult = ippiGetAffineTransform(srcRoi, quad, coeffs);
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
    
    Ipp64f *result = reinterpret_cast<Ipp64f*>(jEnv->GetPrimitiveArrayCritical(jMatrix, 0));
    result[0] = coeffs[0][0];
    result[1] = coeffs[0][1];
    result[2] = coeffs[0][2];
    result[3] = coeffs[1][0];
    result[4] = coeffs[1][1];
    result[5] = coeffs[1][2];
    jEnv->ReleasePrimitiveArrayCritical(jMatrix, reinterpret_cast<void*>(result), 0);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeAffineTransform
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Transform_nativeAffineTransform(JNIEnv *jEnv, 
                                                                                   jclass jContext, 
                                                                                   jdoubleArray jMatrix, 
                                                                                   jintArray jSourceImage, 
                                                                                   jint sourceMinX, 
                                                                                   jint sourceMinY, 
                                                                                   jint sourceWidth, 
                                                                                   jint sourceHeight, 
                                                                                   jintArray jDestImage, 
                                                                                   jint destMinX, 
                                                                                   jint destMinY, 
                                                                                   jint destWidth, 
                                                                                   jint destHeight)
{
  try
  {
    sptAssert(jEnv->GetArrayLength(jMatrix) == 6);
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    
    Ipp64f *t = reinterpret_cast<Ipp64f*>(jEnv->GetPrimitiveArrayCritical(jMatrix, 0));
    double transformMatrix[2][3] = { { t[0], t[2], t[4] }, { t[1], t[3], t[5] } };
    
    IppiRect srcRoi = { sourceMinX, sourceMinY, sourceWidth, sourceHeight };
    IppiRect destRoi = { destMinX, destMinY, destWidth, destHeight };
    
    IppStatus ippResult = ippiWarpAffine_32f_C1R(pSourceImage->address, 
                                                 pSourceImage->getSize(), 
                                                 pSourceImage->stride,
                                                 srcRoi, 
                                                 pDestImage->address, 
                                                 pDestImage->stride,
                                                 destRoi,
                                                 transformMatrix,
                                                 IPPI_INTER_LINEAR);
    
    jEnv->ReleasePrimitiveArrayCritical(jMatrix, reinterpret_cast<void*>(t), 0);
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
    
    // ignore no-intersection warnings. Warnings are greater than zero (ippStsNoErr)
    sptAssertWithMessage(ippResult >= ippStsNoErr, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeCopy
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Transform_nativeCopy(JNIEnv *jEnv, 
                                                                        jclass jContext, 
                                                                        jintArray jSourceImage, 
                                                                        jint roiMinX, 
                                                                        jint roiMinY, 
                                                                        jintArray jDestImage, 
                                                                        jint destMinX, 
                                                                        jint destMinY, 
                                                                        jint roiWidth, 
                                                                        jint roiHeight)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    
    IppiSize roiSize = { roiWidth, roiHeight };
    
    IppStatus ippResult = ippiCopy_32f_C1R(pSourceImage->addressAt(roiMinX, roiMinY),
                                           pSourceImage->stride,
                                           pDestImage->addressAt(destMinX, destMinY),
                                           pDestImage->stride,
                                           roiSize);
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
    
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * nativeGradient
 *
 * This is not a derivative filter! It draws a gradient background into the region of interest.
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Transform_nativeGradient(JNIEnv *jEnv, 
                                                                            jclass jContext, 
                                                                            jintArray jSourceImage,
                                                                            jint roiMinX, 
                                                                            jint roiMinY, 
                                                                            jint roiWidth, 
                                                                            jint roiHeight,
                                                                            jfloat startValue, 
                                                                            jfloat slope, 
                                                                            jboolean isHorizontal)
{
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    
    IppiSize roiSize = { roiWidth, roiHeight };
    
    IppStatus ippResult = ippiImageRamp_32f_C1R(pSourceImage->addressAt(roiMinX, roiMinY), 
                                                pSourceImage->stride,
                                                roiSize, 
                                                startValue, 
                                                slope, 
                                                isHorizontal ? ippAxsHorizontal : ippAxsVertical);
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}


/**
 * nativeRemap
 *
 * Interpolation:
 * 0 = Nearest Neighbor
 * 1 = Linear (really bilinear)
 * 2 = Cubic
 *
 * @author Patrick Lacz
 */
JNIEXPORT void JNICALL Java_com_axi_util_image_Transform_nativeRemap(JNIEnv *jEnv, 
                                                                         jclass jContext,
                                                                         jintArray jSourceImage, 
                                                                         jint sourceMinX, 
                                                                         jint sourceMinY, 
                                                                         jint sourceWidth, 
                                                                         jint sourceHeight,
                                                                         jintArray xDataImage, 
                                                                         jint xDataOffsetX, 
                                                                         jint xDataOffsetY,
                                                                         jintArray yDataImage, 
                                                                         jint yDataOffsetX, 
                                                                         jint yDataOffsetY,
                                                                         jintArray jDestImage, 
                                                                         jint destOffsetX, 
                                                                         jint destOffsetY, 
                                                                         jint interpolation)
{ 
  try
  {
    JNIImageData *pSourceImage = getImageDataFromJava(jEnv, jSourceImage);
    JNIImageData *pXDataImage = getImageDataFromJava(jEnv, xDataImage);
    JNIImageData *pYDataImage = getImageDataFromJava(jEnv, yDataImage);
    JNIImageData *pDestImage = getImageDataFromJava(jEnv, jDestImage);
    
    IppiRect sourceRect = { sourceMinX, sourceMinY, sourceWidth, sourceHeight };
    
    int ippInterpolation = 0;
    switch (interpolation)
    {
      case 0:
        ippInterpolation = IPPI_INTER_NN;
        break;
      case 1:
        ippInterpolation = IPPI_INTER_LINEAR;
        break;
      case 2:
        ippInterpolation = IPPI_INTER_CUBIC;
        break;
      default:
        sptAssertWithMessage(false, "Invalid interpolation value.");
    }
    
    IppStatus ippResult = ippiRemap_32f_C1R(pSourceImage->addressAt(sourceMinX, sourceMinY),
                                            pSourceImage->getSize(),
                                            pSourceImage->stride,
                                            sourceRect,
                                            pXDataImage->addressAt(xDataOffsetX, xDataOffsetY),
                                            pXDataImage->stride,
                                            pYDataImage->addressAt(yDataOffsetX, yDataOffsetY),
                                            pYDataImage->stride,
                                            pDestImage->addressAt(destOffsetX, destOffsetY),
                                            pDestImage->stride,
                                            pDestImage->getSize(),
                                            ippInterpolation);
    
    releaseImageDataFromJavaUnmodified(jEnv, jSourceImage, pSourceImage);
    releaseImageDataFromJavaUnmodified(jEnv, xDataImage, pXDataImage);
    releaseImageDataFromJavaUnmodified(jEnv, yDataImage, pYDataImage);
    releaseImageDataFromJavaUnmodified(jEnv, jDestImage, pDestImage);
    
    sptAssertWithMessage(ippResult == ippStsNoErr, ippGetStatusString(ippResult));
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}
