package com.axi.util.image;

/**
 *
 * @author Lim, Lay Ngor
 */
public class InterpolationModeEnum extends com.axi.util.Enum
{
  //Modify to fix number so that the value is tally with third party enum file at:
  //thirdParty\VitroxImageEnhancer\Include/VfImageEnhancer.hpp, enum VxImageEnhancer::InterpolationMode
  //This is to ensure the enum number we pass into the third party function is tally.    
  //Only list out supported enum that being use by JNI function here
  public static final InterpolationModeEnum Linear = new InterpolationModeEnum(2);
  public static final InterpolationModeEnum Cubic = new InterpolationModeEnum(3);

  /**
   * @author Lim, Lay Ngor
   */
  public InterpolationModeEnum(int id)
  {
    super(id);
  }
}
