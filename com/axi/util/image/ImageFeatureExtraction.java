package com.axi.util.image;

import java.awt.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.nio.*;
import java.util.*;
import java.util.List;

import com.axi.util.*;

/**
 * This class holds image processing primitives for extracting features (or
 * measurements) from an image.  By nature, these primitives are slightly
 * higher level than other image processing primitives, as they may call
 * several lower-level primitives in order to create their result.
 *
 * @author Peter Esbensen
 */
public class ImageFeatureExtraction
{
  // Some common multiples of PI which are used in some of the methods here.
  private static final double _HALF_PI = Math.PI / 2f;
  private static final double _TWO_PI = Math.PI * 2f;
  private static final double _THREE_HALF_PI = (Math.PI * 3f) / 2f;

  // creating semicircular kernels is very expensive so will cache the most recently used ones in these collections
  static private List<Image> _cachedForwardSemiCircleKernels = Collections.synchronizedList(new LinkedList<Image>());
  static private List<Image> _cachedReverseSemiCircleKernels = Collections.synchronizedList(new LinkedList<Image>());
  static private List<Image> _cachedUpwardsSemiCircleKernels = Collections.synchronizedList(new LinkedList<Image>());
  static private List<Image> _cachedDownwardsSemiCircleKernels = Collections.synchronizedList(new LinkedList<Image>());

  /**
   * @author Patrick Lacz
   */
  static
  {
    System.loadLibrary("nativeImageUtil");
  }

  /**
   * @author Peter Esbensen
   */
  private ImageFeatureExtraction()
  {
    // do nothing
    //
    // Methods in this class are all expected to be statics, so other code really
    // shouldn't need to create an actual FeatureExtraction object.
  }

  /*
   * This is a high level function that builds a histogram profile base on profile(...) function in this class.
   * This is not an efficient way of doing it, but I cannot find an appropriate function in Intel IPP library to use.
   * There are 2 functions that can do the same work, but the result is limited. They are ippiHistogramRange and ippiHistogramEven.
   * The max count can only up to 256, because it only support 8-bit unsigned or 16-bit signed.
   *
   * @author Lim, Seng Yew
   */
  static public int[] histogramProfile(Image image, RegionOfInterest regionOfInterest, int numOfGrayLevel)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(image));

    int[] histogramProfile = new int[numOfGrayLevel];
    for (int i=0; i<numOfGrayLevel; ++i)
    {
      histogramProfile[i] = 0;
    }
    int roiMinX = regionOfInterest.getMinX();
    int roiMinY = regionOfInterest.getMinY();
    int offsetValue, xCoord, yCoord;
    float grayLevelFloat;
    int grayLevelInteger;
    // Copied code from Image.getPixelValue(), due to performance consideration.
    // Creating ByteBuffer takes time, so do it once and get all the pixel values is more efficient than calling image.getPixelValue().
    // Orientation is irrelevant in this calculation.
    ByteBuffer bufferData = image.getByteBuffer();
    for (int iRow=0; iRow<regionOfInterest.getHeight(); ++iRow)
    {
      for (int iCol=0; iCol<regionOfInterest.getWidth(); ++iCol)
      {
        xCoord = roiMinX + iCol;
        yCoord = roiMinY + iRow;
        offsetValue = yCoord * image.getStride() + xCoord * image.getBytesPerPixel();
        grayLevelFloat = bufferData.getFloat(offsetValue);
        // [XCR2121] - Ying-Huan.Chu
        // We will find the minimum number between 255 and grayLevelFloat so that in any case that the grayLevelFloat is more than 255, 255 will be used.
        grayLevelInteger = (int)Math.min(255, Math.floor(grayLevelFloat));

        //Lim, Lay Ngor - XCR2121 - Set the gray value to numOfGrayLevel if the round up value is numOfGrayLevel.
        //This problem is most likely happen when user turn on the pre-processing feature. 
        //The image after the processing will contain pixels with float gray value.
        //e.g. The float type gray value: 255.xx will round up become 256 and causing system assert.
        //So we only convert gray value with 255.xx to 255. System will still assert any value >= 256

        Assert.expect(grayLevelInteger >= 0 && grayLevelInteger < numOfGrayLevel, "Error in ImageFeatureExtraction.java, histogramProfile(...). Graylevel integer value not in 0-" + numOfGrayLevel + " range. Invalid value found is " + grayLevelInteger + ".");
        histogramProfile[grayLevelInteger]++;
      }
    }
    bufferData = null;

    return histogramProfile;
  }

  /**
   * Project image data within the RegionOfInterest into a single dimension
   * vector.  If the orientation of the RegionOfInterest is horizontal
   * (0 or 180 degrees) then we create the profile by averaging along the
   * columns in the image data.  If the orientation is vertical (90 or 270)
   * then we create the profile by averaging along the rows.
   *
   * <pre>
   *
   * For example, if the image data within our RegionOfInterest contains:
   *
   * 3 4 5 6
   * 4 5 6 7
   * 5 6 7 8
   *
   * Then, if the orientation is 0, the result should be:
   *
   * 4.0 5.0 6.0 7.0
   *
   * If the orienation is 90, the result should be:
   *
   * 6.5 5.5 4.5
   *
   * If the orientation is 180, the result should be:
   *
   * 7.0 6.0 5.0 4.0
   *
   * And if the orientation is 270, the result should be:
   * 4.5 5.5 6.5
   *
   * </pre>
   *
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  static public float[] profile(Image image, RegionOfInterest regionOfInterest)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(image));

    float[] profile = null;

    if (regionOfInterest.getOrientationInDegrees() == 0)
    {
      profile = doHorizontalProfile(image, regionOfInterest);
    }
    else if (regionOfInterest.getOrientationInDegrees() == 180)
    {
      profile = doHorizontalProfile(image, regionOfInterest);
      ProfileUtil.reverseProfile(profile);
    }
    else if (regionOfInterest.getOrientationInDegrees() == 90)
    {
      profile = doVerticalProfile(image, regionOfInterest);
      ProfileUtil.reverseProfile(profile);
    }
    else if (regionOfInterest.getOrientationInDegrees() == 270)
    {
      profile = doVerticalProfile(image, regionOfInterest);
    }
    else
    {
      Assert.expect(false);  // this only works with cardinal orientations (0, 90, 180, 270)
    }

    return profile;
  }

  /**
   * Run a profile across the given image in the specified RegionOfInterest, in the direction specified by the
   * RegionOfInterest.  This profile will be based on a semicircular mask that might look something like:
   *
   * <pre>
   *  0 0 1
   *  0 1 0
   *  0 0 1
   * </pre>
   *
   * optimize PWL : We may be able to optimize by adding together subimages cooresponding to the ones in the mask above.
   *
   *
   * @param regionOfInterest is the region that the profile should be run across
   * @author Sunit Bhalla
   * @author Peter Esbensen
   */
  static public float[] circleProfile(Image image, RegionOfInterest regionOfInterest)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);

    // The width and height of the regionOfInterest should be at least two.  For instance, if the height
    // is 1.0, when we divide by two to get the radius of the circle, we'll have a radius of 0.
    //  The formula to find the radius is (int) (height * .5).
    Assert.expect(regionOfInterest.getWidth() > 1);
    Assert.expect(regionOfInterest.getHeight() > 1);

    // We will check that the ROI fits in the image in the helper "doCircleProfile" methods because
    // they know exactly how much of the image they need

    int orientation = regionOfInterest.getOrientationInDegrees();
    float[] returnProfile = null;

    if (orientation == 0)
      returnProfile = doForwardCircleProfile(image, regionOfInterest);
    else if (orientation == 180)
      returnProfile = doReverseCircleProfile(image, regionOfInterest);
    else if (orientation == 90)
      returnProfile = doDownwardCircleProfile(image, regionOfInterest);
    else if (orientation == 270)
      returnProfile = doUpwardCircleProfile(image, regionOfInterest);
    else
      Assert.expect(false);
    return returnProfile;
  }

  /**
   * Creates a profile based on the order statistic from the given iamge and ROI

   * @author Peter Esbensen
   */
  static public float[] orderStatisticProfile(Image image, RegionOfInterest regionOfInterest, float orderStatistic)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(image));
    Assert.expect(orderStatistic >= 0.0f);
    Assert.expect(orderStatistic <= 1.0f);

    float[] profile = null;

    if (regionOfInterest.getOrientationInDegrees() == 0)
    {
      profile = doHorizontalOrderStatisticProfile(image, regionOfInterest, orderStatistic);
    }
    else if (regionOfInterest.getOrientationInDegrees() == 180)
    {
      profile = doHorizontalOrderStatisticProfile(image, regionOfInterest, orderStatistic);
      ProfileUtil.reverseProfile(profile);
    }
    else if (regionOfInterest.getOrientationInDegrees() == 90)
    {
      profile = doVerticalOrderStatisticsProfile(image, regionOfInterest, orderStatistic);
      ProfileUtil.reverseProfile(profile);
    }
    else if (regionOfInterest.getOrientationInDegrees() == 270)
    {
      profile = doVerticalOrderStatisticsProfile(image, regionOfInterest, orderStatistic);
    }
    else
    {
      Assert.expect(false); // this only works with cardinal orientations (0, 90, 180, 270)
    }

    return profile;
  }

  /**
   * @author Peter Esbensen
   */
  static private float[] doHorizontalOrderStatisticProfile(Image image,
                                                           RegionOfInterest regionOfInterest,
                                                           float orderStatistic)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(image));


    float[] profileResult = new float[regionOfInterest.getMaxX() - regionOfInterest.getMinX() + 1];

    // march along the columns, doing the requested order statistic
    RegionOfInterest subregion = new RegionOfInterest(regionOfInterest);
    int minX = regionOfInterest.getMinX();
    for (int x = regionOfInterest.getMinX(); x <= regionOfInterest.getMaxX(); ++x)
    {
      subregion.setCenterXY(x, regionOfInterest.getCenterY());
      subregion.setWidthKeepingSameCenter(1);
      profileResult[x - minX] = Statistics.getPercentile(image, subregion, orderStatistic);
    }

    return profileResult;
  }

  /**
   * @author Peter Esbensen
   */
  static private float[] doVerticalOrderStatisticsProfile(Image image,
                                                          RegionOfInterest regionOfInterest,
                                                          float orderStatistic)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(image));

    // march along the rows, doing the requested order statistic

    float[] profileResult = new float[regionOfInterest.getMaxY() - regionOfInterest.getMinY() + 1];

    // march along the columns, doing the requested order statistic
    RegionOfInterest subregion = new RegionOfInterest(regionOfInterest);
    int minY = regionOfInterest.getMinY();
    for (int y = minY; y <= regionOfInterest.getMaxY(); ++y)
    {
      subregion.setCenterXY(regionOfInterest.getCenterX(), y);
      subregion.setHeightKeepingSameCenter(1);
      profileResult[y - minY] = Statistics.getPercentile(image, subregion, orderStatistic);
    }

    return profileResult;

  }

  /**
   * Truncate the region, if necessary, so that the circle profile primitive will
   * be sure to have all the pixels it needs in the given image.
   *
   * @author Peter Esbensen
   */
  static public RegionOfInterest truncateRegionOfInterestToImageForCircleProfile(
      RegionOfInterest regionOfInterest,
      Image image)
  {
    Assert.expect(regionOfInterest != null);
    Assert.expect(image != null);

    RegionOfInterest actualRegionUsedByPrimitive = getActualRegionAffectedByCircleProfile(regionOfInterest);

    int minX = actualRegionUsedByPrimitive.getMinX();
    int maxX = actualRegionUsedByPrimitive.getMaxX();
    int minY = actualRegionUsedByPrimitive.getMinY();
    int maxY = actualRegionUsedByPrimitive.getMaxY();

    int newMinX = regionOfInterest.getMinX();
    int newMaxX = regionOfInterest.getMaxX();
    int newMinY = regionOfInterest.getMinY();
    int newMaxY = regionOfInterest.getMaxY();

    int lastValidImageX = image.getWidth() - 1;
    int lastValidImageY = image.getHeight() - 1;

    if (minX < 0)
    {
      newMinX = Math.min(maxX, regionOfInterest.getMinX() - minX);
    }
    if (maxX > lastValidImageX)
    {
      int pixelsBeyondImageBoundary = maxX - lastValidImageX;
      newMaxX = regionOfInterest.getMaxX() - pixelsBeyondImageBoundary;
      newMinX = Math.min(newMinX, newMaxX);
    }
    if (minY < 0)
    {
      newMinY = Math.min(maxY, regionOfInterest.getMinY() - minY);
    }
    if (maxY > lastValidImageY)
    {
      int pixelsBeyondImageBoundary = maxY - lastValidImageY;
      newMaxY = regionOfInterest.getMaxY() - pixelsBeyondImageBoundary;
      newMinY = Math.min(newMinY, newMaxY);
    }
    return RegionOfInterest.createRegionFromRegionBorder(
      newMinX, newMinY, newMaxX, newMaxY,
      regionOfInterest.getOrientationInDegrees(),
      regionOfInterest.getRegionShapeEnum());
  }

  /**
   * Return the actual region of interest inspected by the circle profile primitive,
   * given that the circle profile primitive is not symmetric and will analyze pixels
   * outside of the original region of interest.
   *
   * @author Peter Esbensen
   */
  static private RegionOfInterest getActualRegionAffectedByCircleProfile(RegionOfInterest regionOfInterest)
  {
    Assert.expect(regionOfInterest != null);

    int radiusOfCircle = 0;
    RegionOfInterest actualRegionAffectedByPrimitive = null;

    if (regionOfInterest.getOrientationInDegrees() == 0)
    {   
      radiusOfCircle = (int)(regionOfInterest.getHeight() * 0.5);      
      //Jack Hwee - make sure the width that pass into RegionOfInterest is not 0. 
      if (regionOfInterest.getWidth() + (radiusOfCircle - 1) <= 0)
      {
        actualRegionAffectedByPrimitive = new RegionOfInterest(
        regionOfInterest.getMinX(), regionOfInterest.getMinY(),
        regionOfInterest.getWidth() + (radiusOfCircle), regionOfInterest.getHeight(),
        regionOfInterest.getOrientationInDegrees(), RegionShapeEnum.RECTANGULAR);
      }
      else
      {
        actualRegionAffectedByPrimitive = new RegionOfInterest(
        regionOfInterest.getMinX(), regionOfInterest.getMinY(),
        regionOfInterest.getWidth() + (radiusOfCircle - 1), regionOfInterest.getHeight(),
        regionOfInterest.getOrientationInDegrees(), RegionShapeEnum.RECTANGULAR);
      }
    }
    else
    if (regionOfInterest.getOrientationInDegrees() == 180)
    {
      radiusOfCircle = (int)(regionOfInterest.getHeight() * 0.5);
      //Jack Hwee - make sure the width that pass into RegionOfInterest is not 0. 
      if (regionOfInterest.getWidth() + (radiusOfCircle - 1) <= 0)
      {
        actualRegionAffectedByPrimitive = new RegionOfInterest(
            regionOfInterest.getMinX() - (radiusOfCircle - 1), regionOfInterest.getMinY(),
            regionOfInterest.getWidth() + (radiusOfCircle), regionOfInterest.getHeight(),
            regionOfInterest.getOrientationInDegrees(), RegionShapeEnum.RECTANGULAR);  
      }
      else
      {
        actualRegionAffectedByPrimitive = new RegionOfInterest(
            regionOfInterest.getMinX() - (radiusOfCircle - 1), regionOfInterest.getMinY(),
            regionOfInterest.getWidth() + (radiusOfCircle - 1), regionOfInterest.getHeight(),
            regionOfInterest.getOrientationInDegrees(), RegionShapeEnum.RECTANGULAR);
      }
    }
    else
    if (regionOfInterest.getOrientationInDegrees() == 90)
    {     
      radiusOfCircle = (int)(regionOfInterest.getWidth() * 0.5);
      //Jack Hwee - make sure the height that pass into RegionOfInterest is not 0. 
      if (regionOfInterest.getHeight() + (radiusOfCircle - 1) <= 0)
      {
        actualRegionAffectedByPrimitive = new RegionOfInterest(
            regionOfInterest.getMinX(), regionOfInterest.getMinY(),
            regionOfInterest.getWidth(), regionOfInterest.getHeight() + (radiusOfCircle),
            regionOfInterest.getOrientationInDegrees(), RegionShapeEnum.RECTANGULAR);
      }
      else
      {
        actualRegionAffectedByPrimitive = new RegionOfInterest(
            regionOfInterest.getMinX(), regionOfInterest.getMinY(),
            regionOfInterest.getWidth(), regionOfInterest.getHeight() + (radiusOfCircle - 1),
            regionOfInterest.getOrientationInDegrees(), RegionShapeEnum.RECTANGULAR);
      }
    }
    else
    if (regionOfInterest.getOrientationInDegrees() == 270)
    {
      radiusOfCircle = (int)(regionOfInterest.getWidth() * 0.5);    
      //Jack Hwee - make sure the height that pass into RegionOfInterest is not 0. 
      if (regionOfInterest.getHeight() + (radiusOfCircle - 1) <= 0)
      {
        actualRegionAffectedByPrimitive = new RegionOfInterest(
            regionOfInterest.getMinX(), regionOfInterest.getMinY() - (radiusOfCircle - 1),
            regionOfInterest.getWidth(), regionOfInterest.getHeight() + (radiusOfCircle),
            regionOfInterest.getOrientationInDegrees(), RegionShapeEnum.RECTANGULAR);
      }
      else
      {
        actualRegionAffectedByPrimitive = new RegionOfInterest(
            regionOfInterest.getMinX(), regionOfInterest.getMinY() - (radiusOfCircle - 1),
            regionOfInterest.getWidth(), regionOfInterest.getHeight() + (radiusOfCircle - 1),
            regionOfInterest.getOrientationInDegrees(), RegionShapeEnum.RECTANGULAR);
      }
    }
    else
      Assert.expect(false);  // only works with cardinal orientations
    Assert.expect(actualRegionAffectedByPrimitive != null);
    return actualRegionAffectedByPrimitive;
  }

  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */
  static private float[] doForwardCircleProfile(Image image, RegionOfInterest regionOfInterest)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);

    RegionOfInterest actualRegionAffectedByPrimitive = getActualRegionAffectedByCircleProfile(regionOfInterest);

    Assert.expect(actualRegionAffectedByPrimitive.fitsWithinImage(image));

    Image semiCircleKernel = createSemiCircleKernel(actualRegionAffectedByPrimitive.getHeight(),
                                                    PrimitiveDirectionEnum.FORWARD);

    Image returnImage = Filter.convolveKernel(image,
                                              actualRegionAffectedByPrimitive,
                                              semiCircleKernel,
                                              new ImageCoordinate(0, 0), // anchor
                                              BorderSamplingModeEnum.CONSTRAIN_TO_REGION);

    float[] returnArray = Image.createArrayFromImage(returnImage, RegionOfInterest.createRegionFromImage(returnImage));

    semiCircleKernel.decrementReferenceCount();
    returnImage.decrementReferenceCount();

    return returnArray;
  }

  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */
  static private float[] doReverseCircleProfile(Image image, RegionOfInterest regionOfInterest)

  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);

    RegionOfInterest actualRegionAffectedByPrimitive = getActualRegionAffectedByCircleProfile(regionOfInterest);

    Assert.expect(actualRegionAffectedByPrimitive.fitsWithinImage(image));

    Image semiCircleKernel = createSemiCircleKernel(actualRegionAffectedByPrimitive.getHeight(), PrimitiveDirectionEnum.REVERSE);

    Image returnImage = Filter.convolveKernel(image,
                                              actualRegionAffectedByPrimitive,
                                              semiCircleKernel,
                                              new ImageCoordinate(0, 0), // anchor
                                              BorderSamplingModeEnum.CONSTRAIN_TO_REGION);

    float[] returnArray = Image.createArrayFromImage(returnImage, RegionOfInterest.createRegionFromImage(returnImage));

    semiCircleKernel.decrementReferenceCount();
    returnImage.decrementReferenceCount();
    ProfileUtil.reverseProfile(returnArray);

    return returnArray;
  }

  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */
  static private float[] doDownwardCircleProfile(Image image, RegionOfInterest regionOfInterest)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);

    RegionOfInterest actualRegionAffectedByPrimitive = getActualRegionAffectedByCircleProfile(regionOfInterest);

    Assert.expect(actualRegionAffectedByPrimitive.fitsWithinImage(image));

    Image semiCircleKernel = createSemiCircleKernel(actualRegionAffectedByPrimitive.getWidth(),
        PrimitiveDirectionEnum.DOWNWARDS);

    Image returnImage = Filter.convolveKernel(image,
                                              actualRegionAffectedByPrimitive,
                                              semiCircleKernel,
                                              new ImageCoordinate(0, 0), // anchor
                                              BorderSamplingModeEnum.CONSTRAIN_TO_REGION);

    float[] returnArray = Image.createArrayFromImage(returnImage, RegionOfInterest.createRegionFromImage(returnImage));
    semiCircleKernel.decrementReferenceCount();
    returnImage.decrementReferenceCount();


    return returnArray;
  }

  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */
  static private float[] doUpwardCircleProfile(Image image, RegionOfInterest regionOfInterest)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);

    RegionOfInterest actualRegionAffectedByPrimitive = getActualRegionAffectedByCircleProfile(regionOfInterest);

    Assert.expect(actualRegionAffectedByPrimitive.fitsWithinImage(image));

    Image semiCircleKernel = createSemiCircleKernel(actualRegionAffectedByPrimitive.getWidth(),
        PrimitiveDirectionEnum.UPWARDS);

    Image returnImage = Filter.convolveKernel(image,
                                              actualRegionAffectedByPrimitive,
                                              semiCircleKernel,
                                              new ImageCoordinate(0, 0), // anchor
                                              BorderSamplingModeEnum.CONSTRAIN_TO_REGION);

    float[] returnArray = Image.createArrayFromImage(returnImage, RegionOfInterest.createRegionFromImage(returnImage));

    ProfileUtil.reverseProfile(returnArray);
    semiCircleKernel.decrementReferenceCount();
    returnImage.decrementReferenceCount();


    return returnArray;
  }

  /**
   * Return a semicircular kernel of the same radius and direction if it
   * exists in our cached kernel lists.  If it does not exist, return null (sorry
   * Bill)
   *
   * @author Peter Esbensen
   */
  static private Image getCachedSemiCircleKernelIfItExists(int diameter, PrimitiveDirectionEnum direction)
  {
    Assert.expect(diameter >= 0);

    Assert.expect(direction != null);

    List<Image> imageList = getImageListCache(direction);

    Assert.expect(imageList != null);
    synchronized( imageList )
    {
      for (Image image : imageList)
      {
        if (Math.max(image.getWidth(), image.getHeight()) == diameter)
        {
          imageList.remove(image);
          imageList.add(0, image);
          return image;
        }
      }
    }
    return null;  // return null if the image wasn't in the list
  }

  /**
   * Store the given Image in our cache of semicircular kernels.  If the
   * cache is full, it will replace the kernel that was used the longest time ago.
   *
   * @author Peter Esbensen
   */
  static private void cacheSemiCircleKernel(Image image, int diameter, PrimitiveDirectionEnum direction)
  {
    Assert.expect(image != null);
    Assert.expect(diameter >= 0);
    Assert.expect(direction != null);
    Assert.expect(Math.max(image.getWidth(), image.getHeight()) == diameter);

    List<Image> imageList = getImageListCache(direction);

    synchronized (imageList)
    {
      int listSize = imageList.size();
      final int MAX_LIST_SIZE = 10;
      if (listSize < MAX_LIST_SIZE)
      {
        image.incrementReferenceCount();
        imageList.add(image); // just append to list
      }
      else
      {
        image.incrementReferenceCount();
        imageList.get(MAX_LIST_SIZE - 1).decrementReferenceCount();
        imageList.set(MAX_LIST_SIZE - 1, image); // replace the last image on the list
      }
    }
  }

  /**
   * Creates a semi circle kernel.  The background of this semicircle is black, and the actual semicircle is white/grey.
   * The kernal is normalized so that the sum of the values of the kernal equals 1.0.
   *
   * optimize PWL : convert to use Paint.fillCircle, which will be more efficient than awt painting.
   *
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */
  static private Image createSemiCircleKernel(int diameter, PrimitiveDirectionEnum direction)

  {
    Assert.expect(diameter >= 0);
    Assert.expect(direction != null);

    int radius = (int)(diameter * 0.5);

    Image cachedImage = getCachedSemiCircleKernelIfItExists(diameter, direction);

    if (cachedImage != null)
    {
      cachedImage.incrementReferenceCount();
      return cachedImage;
    }

    BufferedImage kernelBufferedImage = null;
    Graphics2D kernelG2d = null;

    Arc2D arc2D = null;

    int kernelWidth = 0;
    int kernelHeight = 0;

    // The kernel size is determined by the direction of the primitive.  For forward and reverse directions, the
    // kernel is radius wide by diameter high.  For the upwards and downwards directions, the kernel is diameter
    // wide by radius high.
    if (direction.equals(PrimitiveDirectionEnum.FORWARD) ||
        direction.equals(PrimitiveDirectionEnum.REVERSE))
    {
      kernelWidth = radius;
      kernelHeight = diameter;
    }
    else if (direction.equals(PrimitiveDirectionEnum.UPWARDS) ||
             direction.equals(PrimitiveDirectionEnum.DOWNWARDS))
    {
      kernelWidth = diameter;
      kernelHeight = radius;
    }
    else
      Assert.expect(false);

    // Create the kernel bounding rectangle and fill it in with black pixels
    kernelBufferedImage = new BufferedImage(kernelWidth, kernelHeight, BufferedImage.TYPE_BYTE_GRAY);
    kernelG2d = kernelBufferedImage.createGraphics();
    kernelG2d.setColor(Color.BLACK);
    kernelG2d.fillRect(0, 0, kernelBufferedImage.getWidth(), kernelBufferedImage.getHeight());

    // Create the semicircle.  If these seem rotated 180 degrees from what you expect, remember that we have to do that
    // because convolution requires the kernel to be rotated 180 degrees.
    if (direction.equals(PrimitiveDirectionEnum.FORWARD))
      arc2D = new Arc2D.Double( -1 * radius, 0, diameter, diameter, 270, 180, Arc2D.OPEN);
    else if (direction.equals(PrimitiveDirectionEnum.REVERSE))

      arc2D = new Arc2D.Double(0, 0, diameter, diameter, 90, 180, Arc2D.OPEN);
    else if (direction.equals(PrimitiveDirectionEnum.DOWNWARDS))
      arc2D = new Arc2D.Double(0, -1 * radius, diameter, diameter, 180, 180, Arc2D.OPEN);
    else if (direction.equals(PrimitiveDirectionEnum.UPWARDS))
      arc2D = new Arc2D.Double(0, 0, diameter, diameter, 0, 180, Arc2D.OPEN);
    else
      Assert.expect(false); // invalid direction

    kernelG2d.setColor(Color.WHITE);
    kernelG2d.draw(arc2D);

    Image kernelImage = Image.createFloatImageFromBufferedImage(kernelBufferedImage);

    // Normalize the kernel so that all the sum of the kernel values is 1.0.
    Filter.normalize(kernelImage);

    cacheSemiCircleKernel(kernelImage, diameter, direction);

    return kernelImage;
  }

  /**
   * @author ???
   */
  static public boolean regionIsValidForCircleProfile(RegionOfInterest regionOfInterest)
  {
    Assert.expect(regionOfInterest != null);
    return ((regionOfInterest.getWidth() > 1) && (regionOfInterest.getHeight() > 1));
  }

  /**
   * @author Patrick Lacz
   */
  static private float[] doHorizontalProfile(Image image,
                                             RegionOfInterest regionOfInterest)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(image));

    Assert.expect(image.getStride() != 0);
    Assert.expect(image.getStride() % 4 == 0);

    float result[] = nativeDoHorizontalProfile(image.getNativeDefinition(),
                                             regionOfInterest.getMinX(),
                                             regionOfInterest.getMinY(),
                                             regionOfInterest.getWidth(),
                                             regionOfInterest.getHeight());
    return result;
  }

  /**
   * @author Patrick Lacz
   */
  static native private float[] nativeDoHorizontalProfile(int[] sourceImage, int roiMinX, int roiMinY, int roiWidth, int roiHeight);

   /**
   * @author Lim Lay Ngor
   */
 static public double calculateSharpnessProfileExecute(Image image)
  {
    Assert.expect(image != null);
    Assert.expect(image.getStride() != 0);
    Assert.expect(image.getStride() % 4 == 0);

    double result= nativeCalculateSharpnessProfileExecute(image.getNativeDefinition());
    return result;
  }
  
   /**
   * @author Lim Lay Ngor
   */
  static native private double nativeCalculateSharpnessProfileExecute(int[] sourceImage);
  
  /**
   * @author Peter Esbensen
   */
  static private float[] doVerticalProfile(Image image,
                                           RegionOfInterest regionOfInterest)
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    Assert.expect(regionOfInterest.fitsWithinImage(image));

    float result[] = nativeDoVerticalProfile(image.getNativeDefinition(),
                                      regionOfInterest.getMinX(),
                                      regionOfInterest.getMinY(),
                                      regionOfInterest.getWidth(),
                                      regionOfInterest.getHeight());
    return result;
  }

  /**
   * @author Patrick Lacz
   */
  static native private float[] nativeDoVerticalProfile(int[] sourceImage, int roiMinX, int roiMinY, int roiWidth, int roiHeight);

  /**
   * @author Peter Esbensen
   */
  static private List<Image> getImageListCache(PrimitiveDirectionEnum direction)
  {
    Assert.expect(direction != null);

    List<Image> imageList = null;
    if (direction.equals(PrimitiveDirectionEnum.FORWARD))
    {
      imageList = _cachedForwardSemiCircleKernels;
    }
    else if (direction.equals(PrimitiveDirectionEnum.REVERSE))
    {
      imageList = _cachedReverseSemiCircleKernels;
    }
    else if (direction.equals(PrimitiveDirectionEnum.UPWARDS))
    {
      imageList = _cachedUpwardsSemiCircleKernels;
    }
    else if (direction.equals(PrimitiveDirectionEnum.DOWNWARDS))
    {
      imageList = _cachedDownwardsSemiCircleKernels;
    }

    return imageList;
  }

  /**
   * Find the location of a vertical edge in an image. Assumes
   * there is one and only one edge to find.
   * @param image The image to search.
   * @return Location of the vertical edge.
   * @author Eddie Williamson
   */
  public static float findVerticalEdge(Image image)
  {
    Assert.expect(image != null);

    // Number of points to average over to smooth out the
    // profile data. This also affects the width of the edge
    // and therefore the number of points we must include
    // in the weighted average.
    int averagingSpan = 5;

    // 0 degrees for horizontal profile to find vertical edge
    int orientation = 0;

    // Search the entire image
    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder(0, 0, image.getWidth() - 1,
        image.getHeight() - 1, orientation, RegionShapeEnum.RECTANGULAR);

    float[] profile = profile(image, roi);
    float[] smoothedProfile = ProfileUtil.getSmoothedProfile(profile, averagingSpan);
    float[] derivative = ProfileUtil.createDerivativeProfile(smoothedProfile, 2);
    int maxDerivativeIndex = ProfileUtil.findMaxAbsoluteValue(derivative);
    float edge = ProfileUtil.calcWeightedAverageIndex(derivative, maxDerivativeIndex, averagingSpan - 1);

    return edge;
  }
  
  /**
   * Find the location of a vertical edge in an image. Assumes
   * there two edges to find.
   * @param image The image to search.
   * @return Location of the vertical edge.
   * @author Jack Hwee
   */
  public static java.awt.geom.Point2D findVerticalEdgeForMask(Image image)
  {
    Assert.expect(image != null);

    // Number of points to average over to smooth out the
    // profile data. This also affects the width of the edge
    // and therefore the number of points we must include
    // in the weighted average.
    int averagingSpan = 5;

    // 0 degrees for horizontal profile to find vertical edge
    int orientation = 0;  //0

    // Search the entire image
    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder(0, 0, image.getWidth() - 1,
        image.getHeight() - 1, orientation, RegionShapeEnum.RECTANGULAR);

   // float[] profile = profile(image, roi);
    float[] profile = orderStatisticProfile(image, roi, 0.00f);
  //  float[] smoothedProfile = ProfileUtil.getSmoothedProfile(profile, averagingSpan);
    float[] derivative = ProfileUtil.createDerivativeProfile(profile, 2);
    java.awt.geom.Point2D maxDerivativeIndex = ProfileUtil.findMaxAbsoluteValueForMask(derivative);
    
   // float edge = ProfileUtil.calcWeightedAverageIndex(derivative, maxDerivativeIndex, averagingSpan - 1);
    return maxDerivativeIndex;
  }
 
  /**
   * Find the location of a horizontal edge in an image. Assumes
   * there is one and only one edge to find.
   * @param image The image to search.
   * @return Location of the horizontal edge.
   * @author Eddie Williamson
   */
  public static float findHorizontalEdge(Image image)
  {
    Assert.expect(image != null);

    // Number of points to average over to smooth out the
    // profile data. This also affects the width of the edge
    // and therefore the number of points we must include
    // in the weighted average.
    int averagingSpan = 5;

    // 270 degrees for vertical profile to find horizontal edge
    int orientation = 270;

    // Search the entire image
    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder(0, 0, image.getWidth() - 1,
        image.getHeight() - 1, orientation, RegionShapeEnum.RECTANGULAR);

    float[] profile = profile(image, roi);
    float[] smoothedProfile = ProfileUtil.getSmoothedProfile(profile, averagingSpan);
    float[] derivative = ProfileUtil.createDerivativeProfile(smoothedProfile, 2);
    int maxDerivativeIndex = ProfileUtil.findMaxAbsoluteValue(derivative);
    float edge = ProfileUtil.calcWeightedAverageIndex(derivative, maxDerivativeIndex, averagingSpan - 1);

    return edge;
  }
  
   /**
   * Find the location of a horizontal edge in an image. Assumes
   * there are two edges to find.
   * @param image The image to search.
   * @return Location of the horizontal edge.
   * @author Jack Hwee
   */
  public static java.awt.geom.Point2D findHorizontalEdgeForMask(Image image)
  {
    Assert.expect(image != null);

    // Number of points to average over to smooth out the
    // profile data. This also affects the width of the edge
    // and therefore the number of points we must include
    // in the weighted average.
    int averagingSpan = 5;

    // 270 degrees for vertical profile to find horizontal edge
    int orientation = 270;  //270

    // Search the entire image
    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder(0, 0, image.getWidth() - 1,
        image.getHeight() - 1, orientation, RegionShapeEnum.RECTANGULAR);

   // float[] profile = profile(image, roi);
   // float[] smoothedProfile = ProfileUtil.getSmoothedProfile(profile, averagingSpan);
    float[] profile = orderStatisticProfile(image, roi, 0.0f);
    float[] derivative = ProfileUtil.createDerivativeProfile(profile, 2);
    java.awt.geom.Point2D maxDerivativeIndex = ProfileUtil.findMaxAbsoluteValueForMask(derivative);
   // float edge = ProfileUtil.calcWeightedAverageIndex(derivative, maxDerivativeIndex, averagingSpan - 1);

    return maxDerivativeIndex;
  }

  /**
   * Locate the system fiducial within the image. Assumes it and
   * only it is in the image.
   * @param image The image to search.
   * @return Coordinates of the system fiducial.
   * @author Eddie Williamson
   */
  public static DoubleCoordinate findSystemFiducial(Image image)
  {
    Assert.expect(image != null);

    float verticalEdge = findVerticalEdge(image);
    float horizontalEdge = findHorizontalEdge(image);
    DoubleCoordinate coordinate = new DoubleCoordinate(verticalEdge, horizontalEdge);

    return coordinate;
  }

  /**
   * @author Patrick Lacz
   */
  public static float[] ringProfile(Image image, RegionOfInterest outerRegion, RegionOfInterest innerRegion)
  {
    Assert.expect(image != null);
    Assert.expect(outerRegion != null);
    Assert.expect(innerRegion != null);
    Assert.expect(outerRegion.fitsWithinImage(image));
    Assert.expect(innerRegion.fitsWithinImage(image));

    double outerDiameter = ((double)outerRegion.getWidth() + (double)outerRegion.getHeight()) / 2.0;
    double innerDiameter = ((double)innerRegion.getWidth() + (double)innerRegion.getHeight()) / 2.0;
    Assert.expect(outerDiameter > innerDiameter);

    double sampleDiameter = (outerDiameter + innerDiameter) / 2.0;
    int numberOfBinsTheta = (int)Math.floor(sampleDiameter * Math.PI);
    int numberOfBinsR = (int)Math.floor((outerDiameter - innerDiameter) / 2.0);

    double thetaStep = (Math.PI * 2) / (double)numberOfBinsTheta;

    double centerX = outerRegion.getCenterX();
    double centerY = outerRegion.getCenterY();

    // generate a map from our rectangular regions to profile to the source image.
    /** optimize PWL : These images can be cached, then we can translate each value in them to the correct center. */
    Image thetaImage = new Image(numberOfBinsTheta, numberOfBinsR);
    Image radiusImage = new Image(numberOfBinsTheta, numberOfBinsR);

    RegionOfInterest mapRoi = RegionOfInterest.createRegionFromImage(thetaImage);

    boolean clockwise = false;
    if (clockwise)
      Paint.fillGradientXRegionOfInterest(thetaImage, mapRoi, 0.0f, (float)(numberOfBinsTheta * thetaStep));
    else
      Paint.fillGradientXRegionOfInterest(thetaImage, mapRoi, (float)(numberOfBinsTheta * thetaStep), 0.0f);

    Paint.fillGradientYRegionOfInterest(radiusImage, mapRoi, (float)(outerDiameter/2.0), (float)(innerDiameter/2.0));

    // x = R*cos(theta) + offset
    // y = R*sin(theta) + offset
    Image xMapImage = new Image(numberOfBinsTheta, numberOfBinsR);
    Image yMapImage = new Image(numberOfBinsTheta, numberOfBinsR);
    Arithmetic.polarToCartesian(radiusImage, thetaImage, xMapImage, yMapImage);

    radiusImage.decrementReferenceCount();
    thetaImage.decrementReferenceCount();

    /** optimize PWL : Before this point, we could have cached these images by radius. */
    Arithmetic.addConstantInPlace(xMapImage, mapRoi, (float)centerX);
    Arithmetic.addConstantInPlace(yMapImage, mapRoi, (float)centerY);

    Image sampleImage = new Image(numberOfBinsTheta, numberOfBinsR);

    if (outerRegion.getMaxX() >= image.getWidth() - 1
        || outerRegion.getMinX() <= 0
        || outerRegion.getMaxY() >= image.getHeight() - 1
        || outerRegion.getMinY() <= 0)
    {
      // do some clamping to be sure we don't step over the image boundaries.
      Threshold.clamp(xMapImage, 0.0f, (float)(image.getWidth() - 1));
      Threshold.clamp(yMapImage, 0.0f, (float)(image.getHeight() - 1));
    }

    // temporary fixed by wei chin for jni crash when the map image width > source image
    // will review later
    if(xMapImage.getWidth() >=  image.getWidth() - 1 ||
       yMapImage.getWidth() >=  image.getWidth() - 1 ||
       xMapImage.getHeight() >=  image.getHeight() - 1 ||
       yMapImage.getHeight() >=  image.getHeight() - 1 )
    {
      // do some clamping to be sure we don't step over the image boundaries.
      Threshold.clamp(xMapImage, 0.0f, (float)(image.getWidth() - 1));
      Threshold.clamp(yMapImage, 0.0f, (float)(image.getHeight() - 1));
    }

    Transform.remapIntoImage(image, RegionOfInterest.createRegionFromImage(image), xMapImage, mapRoi, yMapImage, mapRoi, sampleImage, mapRoi, 0);

    float rawProfile[] = profile(sampleImage, mapRoi);

    xMapImage.decrementReferenceCount();
    yMapImage.decrementReferenceCount();
    sampleImage.decrementReferenceCount();

    return rawProfile;
  }

  /**
   * @author Cheah Lee Herng
   */
  public static int[] findCircleRadius(String imageFullPath, CircleDetectionTechniqueEnum circleDetectionTechniqueEnum, int minRadius, int maxRadius)
  {
      Assert.expect(imageFullPath != null);
      Assert.expect(circleDetectionTechniqueEnum != null);
      Assert.expect(minRadius > 0);
      Assert.expect(maxRadius > 0);
      Assert.expect(minRadius <= maxRadius);
      
      int[] circleRadiusArray = {0};
      
      if (circleDetectionTechniqueEnum.equals(CircleDetectionTechniqueEnum.HOUGH_CIRCLE_TRANSFORM))
          circleRadiusArray = nativeHoughCircleTransform(imageFullPath, minRadius, maxRadius);
      else
          Assert.expect(false, "Invalid CircleDetectionTechniqueEnum.");
      
      return circleRadiusArray;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static float[] findCircleDiameter(String imageFullPath, CircleDetectionTechniqueEnum circleDetectionTechniqueEnum, int minRadius, int maxRadius)
  {
      Assert.expect(imageFullPath != null);
      Assert.expect(circleDetectionTechniqueEnum != null);
      Assert.expect(minRadius > 0);
      Assert.expect(maxRadius > 0);
      Assert.expect(minRadius <= maxRadius);
      
      float[] circleDiameterArray = {0.0f};
      
      if (circleDetectionTechniqueEnum.equals(CircleDetectionTechniqueEnum.FIT_ELLIPSE))
          circleDiameterArray = nativeFitEllipse(imageFullPath, minRadius, maxRadius);
      else
          Assert.expect(false, "Invalid CircleDetectionTechniqueEnum.");
      
      return circleDiameterArray;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  static native private int[] nativeHoughCircleTransform(String imageFullPath, int minRadius, int maxRadius);
  
  /**
   * @author Cheah Lee Herng
   */
  static native private float[] nativeFitEllipse(String imageFullPath, int minRadius, int maxRadius);
}
