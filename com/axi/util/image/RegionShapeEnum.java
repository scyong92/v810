package com.axi.util.image;

/**
 * @author Peter Esbensen
 */
public class RegionShapeEnum extends com.axi.util.Enum
{
  private static int _index = 0;
  public static final RegionShapeEnum RECTANGULAR = new RegionShapeEnum(_index++);
  public static final RegionShapeEnum OBROUND = new RegionShapeEnum(_index++);
  public static final RegionShapeEnum LINE = new RegionShapeEnum(_index++);

  /**
   * @author Peter Esbensen
   */
  public RegionShapeEnum(int id)
  {
    super(id);
  }


}
