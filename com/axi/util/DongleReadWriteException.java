/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.util;

/**
 *
 * @author wei-chin.chong
 */
public class DongleReadWriteException extends LocalizedException
{
  /**
   * @author Chong, Wei Chin
   */
  public DongleReadWriteException(String errorCode)
  {
    super(new LocalizedString("BUS_SOFTWARE_LICENSE_READ_WRITE_EXCEPTION_KEY",new Object[]{errorCode}) );
  }
}