package com.axi.util;

import java.util.*;

/**
 * @author Cheah Lee Herng
 */
public class AssertObservable extends Observable 
{
  private static AssertObservable _instance = null;
  
  /**
   * @author Cheah Lee Herng
   */
  public static synchronized AssertObservable getInstance()
  {
    if (_instance == null)
    {
      _instance = new AssertObservable();
    }
    return _instance;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized void sendEvent()
  {
    setChanged();
    notifyObservers();
  }
}
