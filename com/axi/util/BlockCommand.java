package com.axi.util;

import java.util.*;

import com.axi.util.*;

/**
 * All the Commands executed within a command block
 * are treated as a single command for the purposes of undo and redo commands.
 *
 * @author Bill Darbie
 */
public class BlockCommand extends Command
{
  private List<Command> _commands = new ArrayList<Command>();
  private String _description;

  /**
   * @author Bill Darbie
   */
  BlockCommand(String description)
  {
    Assert.expect(description != null);
    _description = description;
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    Assert.expect(_description != null);
    return _description;
  }

  /**
   * @author Bill Darbie
   */
  void addCommand(Command command)
  {
    Assert.expect(command != null);

    _commands.add(command);
  }

  /**
   * @author Bill Darbie
   */
  void removeLastCommand()
  {
    Assert.expect(_commands != null);
    int index = _commands.size() - 1;
    Assert.expect(index >= 0);
    _commands.remove(index);
  }

  /**
   * Execute the command.  If the command will cause a change of state it should return true.
   * If the commands execution will not cause a state change, it should return false.
   *
   * @author Bill Darbie
   */
  public boolean execute() throws Exception
  {
    Assert.expect(_commands != null);

    for (Command command : _commands)
    {
      command.execute();
    }

    // if there are no commands left on the list
    // return false indicating that this BlockCommand
    // will not change state
    if (_commands.isEmpty())
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  public void undo() throws Exception
  {
    Assert.expect(_commands != null);

    for(int i = _commands.size() - 1; i >= 0; --i)
    {
      Command command = (Command)_commands.get(i);
      command.undo();
    }
  }

  /**
   * @return the number of Commands that this BlockCommand will execute() when it's execute() or undo()
   * methods are called.
   *
   * @author Bill Darbie
   */
  public int getNumCommands()
  {
    Assert.expect(_commands != null);

    return _commands.size();
  }

  /**
   * @return the List of Command objects in this BlockCommand.
   * @author Bill Darbie
   */
  public List<Command> getCommands()
  {
    Assert.expect(_commands != null);

    return _commands;
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    Assert.expect(_commands != null);

    if (_commands.size() == 1)
    {
      // there is only one Command, return its description instead of the BlockCommand description
      Command command = (Command)_commands.get(0);
      return command.getDescription();
    }

    Assert.expect(_description != null);
    return _description;
  }
}
