package com.axi.util;

import java.io.*;

import com.axi.util.*;
//import com.axi.xRayTest.datastore.*;

/**
 * This class performs a test on the QuoteTokenizer class
 * @author George A. David
 */
public class Test_QuoteTokenizer extends UnitTest
{
  final String _QUOTE = "\"";
  final String _THIS = "This";
  final String _IS = "is";
  final String _A = "a";
  final String _TEST = "test";
  final String _FOR = "for";
  final String _THE = "the";
  final String _QUOTE_TOKENIZER = "QuoteTokenizer";
  final String _CLASS = "class";
  final String _A_SPACE = " ";
  final String _FIRST_LINE = _QUOTE + _THIS + _QUOTE + _QUOTE + _IS + _QUOTE + _QUOTE +
                             _A + _QUOTE + _QUOTE + _TEST + _QUOTE + _QUOTE + _FOR +
                             _QUOTE + _QUOTE + _THE + _QUOTE + _QUOTE + _QUOTE_TOKENIZER +
                             _QUOTE + _QUOTE + _CLASS + _QUOTE;
  final String _SECOND_LINE = _THIS + _A_SPACE + _IS + _A_SPACE + _A + _A_SPACE + _TEST +
                              _A_SPACE + _FOR + _A_SPACE + _THE + _A_SPACE + _QUOTE_TOKENIZER +
                              _A_SPACE + _CLASS;
  final String _THIRD_LINE = _FIRST_LINE + _SECOND_LINE;
  final String _FOURTH_LINE = _SECOND_LINE + _FIRST_LINE;
  final String _EMPTY_LINE = "";

  public static void main(String args[])
  {
    UnitTest.execute(new Test_QuoteTokenizer());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    QuoteTokenizer qt = new QuoteTokenizer(_FIRST_LINE);
    Expect.expect(qt.hasMoreTokens());

    String token = qt.nextToken();
    Expect.expect(token != null && token.equals(_THIS));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_IS));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_A));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_TEST));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_FOR));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_THE));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_QUOTE_TOKENIZER));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_CLASS));

    token = qt.nextToken();
    Expect.expect(token == null);

    qt = new QuoteTokenizer(_SECOND_LINE);

    Expect.expect(qt.hasMoreTokens() == false);

    token = qt.nextToken();
    Expect.expect(token == null);

    qt = new QuoteTokenizer(_THIRD_LINE);

    Expect.expect(qt.hasMoreTokens());

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_THIS));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_IS));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_A));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_TEST));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_FOR));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_THE));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_QUOTE_TOKENIZER));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_CLASS));

    token = qt.nextToken();
    Expect.expect(token == null);

    qt = new QuoteTokenizer(_FOURTH_LINE);

    Expect.expect(qt.hasMoreTokens());

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_THIS));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_IS));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_A));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_TEST));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_FOR));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_THE));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_QUOTE_TOKENIZER));

    token = qt.nextToken();
    Expect.expect(token != null && token.equals(_CLASS));

    token = qt.nextToken();
    Expect.expect(token == null);

    qt = new QuoteTokenizer(_EMPTY_LINE);
    Expect.expect(qt.hasMoreTokens() == false);

    token = qt.nextToken();
    Expect.expect(token == null);

    boolean exception = false;
    try
    {
      qt = new QuoteTokenizer(null);
    }
    catch(AssertException e)
    {
      exception = true;
    }
    Expect.expect(exception);
  }
}