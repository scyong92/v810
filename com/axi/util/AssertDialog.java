package com.axi.util;

import java.util.*;
import java.awt.*;
import java.net.*;
import java.io.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

/**
* This class provides a dialog window displaying an internal error message.
* The customer to send back stack trace information to Agilent automatically
* or view the stack trace information.
*
* @author Bill Darbie
* @author Steve Anonson
*/
public class AssertDialog extends JDialog
{
  // default size for the dialog
  private static final int _DEFAULT_WIDTH = 725;
  private static final int _DEFAULT_HEIGHT = 350;

  private static final String _TITLE_STRING = "INTERNAL ERROR";
  private static final int _VIEW_STACK_OFFSET = 40;
  private static final int _VIEW_EMAIL_OFFSET = 20;

  private String _stackTrace = null;
  private String _errorMessage = null;
  private String _customerCompanyName = null;
  private String _customerName = null;
  private String _customerEmail = null;
  private String _customerPhoneNumber = null;
  private String _customerSmtpServer = null;
  private String _axiEmailAddress = null;
  private String _emailSubject = null;

  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelLayout = new BorderLayout();
  private JLabel _assertLabel = new JLabel();
  private JPanel _northPanel = new JPanel();
  private BorderLayout _northPanelLayout = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _southPanel = new JPanel();
  private BorderLayout _centerPanelLayout = new BorderLayout();
  private BorderLayout _southPanelLayout = new BorderLayout();
  private JPanel _buttonPanel = new JPanel();
  private GridLayout _buttonPanelLayout = new GridLayout();
  private JButton _emailButton = new JButton();
  private JButton _viewButton = new JButton();
  private JButton _cancelButton = new JButton();
  private Border _buttonPanelBorder = null;
  private JSeparator _buttonSeparator = new JSeparator();
  private Border _buttonSeparatorBorder = null;
  private Border _infoTextAreaBorder = null;
  private JEditorPane _infoTextArea = new JEditorPane();
  private JButton _blankButton1 = new JButton();

  /**
  * This constructor creates the dialog and positions it on the screen in
  * the upper left quadrant.  It is private to the class and is called only
  * by the public constructors.
  *
  * @author Steve Anonson
  */
  private AssertDialog(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    jbInit();
    pack();
    _cancelButton.requestFocus();
    // Position the dialog in the upper left quadrant of the screen.
    setSize(_DEFAULT_WIDTH, _DEFAULT_HEIGHT);
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = getSize();
    if (frameSize.height > screenSize.height)
      frameSize.height = screenSize.height;
    if (frameSize.width > screenSize.width)
      frameSize.width = screenSize.width;
    setLocation((screenSize.width - frameSize.width) / 4, (screenSize.height - frameSize.height) / 4);
  }

  /*
  * This constructor is hidden to force the use of the other constructor.
  */
  private AssertDialog()
  {
    /* Don't use this contructor */
  }

  /**
  * This constructor is the public way to create the dialog.
  *
  * @author Steve Anonson
  */
  public AssertDialog(Frame frame, boolean modal)
  {
    this(frame, _TITLE_STRING, modal);
  }

  /**
   * @author Bill Darbie
   */
  public AssertDialog(Frame frame,
                      boolean modal,
                      String description,
                      String customerCompanyName,
                      String customerName,
                      String customerEmailAddress,
                      String customerPhoneNumber,
                      String stackTrace,
                      String smtpServer,
                      String supportEmailAddress,
                      String emailSubject)
  {
    this(frame, _TITLE_STRING, modal);
    setHtmlDescription(description);
    setCustomerCompanyName(customerCompanyName);
    setCustomerName(customerName);
    setCustomerEmailAddress(customerEmailAddress);
    setCustomerPhoneNumber(customerPhoneNumber);
    setStackTrace(stackTrace);
    setSMTPServer(smtpServer);
    setAgilentSupportEmailAddress(supportEmailAddress);
    setEmailSubject(emailSubject);
  }

  /**
  * This method creates the contents of the dialog.
  *
  * @author Steve Anonson
  */
  private void jbInit()
  {
    _buttonPanelBorder = BorderFactory.createEmptyBorder(5,5,5,5);
    _buttonSeparatorBorder = BorderFactory.createEmptyBorder(5,0,0,0);
    _infoTextAreaBorder = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new java.awt.Color(178, 178, 178),new java.awt.Color(124, 124, 124)),BorderFactory.createEmptyBorder(10,5,10,5));
    _mainPanel.setLayout(_mainPanelLayout);
    _assertLabel.setFont(new java.awt.Font("Dialog", 1, 16));
    _assertLabel.setForeground(Color.red);
    _assertLabel.setBorder(BorderFactory.createRaisedBevelBorder());
    _assertLabel.setRequestFocusEnabled(false);
    _assertLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _assertLabel.setText("An Internal Error Has Occurred");
    _northPanel.setLayout(_northPanelLayout);
    _centerPanel.setLayout(_centerPanelLayout);
    _southPanel.setLayout(_southPanelLayout);
    _buttonPanel.setLayout(_buttonPanelLayout);
    _emailButton.setFont(new java.awt.Font("SansSerif", 0, 16));
    _emailButton.setMnemonic('S');
    _emailButton.setText("Send Email...");
    java.awt.event.ActionListener emailAL = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        emailButton_actionPerformed(e);
      }
    };
    _emailButton.addActionListener( emailAL );
    _emailButton.registerKeyboardAction( emailAL,
                                         KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                                         JComponent.WHEN_FOCUSED);

    _viewButton.setEnabled(false);
    _viewButton.setFont(new java.awt.Font("SansSerif", 0, 16));
    _viewButton.setMnemonic('V');
    _viewButton.setText("View Stack Trace...");
    java.awt.event.ActionListener viewAL = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        viewButton_actionPerformed(e);
      }
    };
    _viewButton.addActionListener( viewAL );
    _viewButton.registerKeyboardAction( viewAL,
                                        KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                                        JComponent.WHEN_FOCUSED);

    _cancelButton.setFont(new java.awt.Font("SansSerif", 0, 16));
//    _cancelButton.setNextFocusableComponent(_emailButton);
    _cancelButton.setMnemonic('E');
    _cancelButton.setText("Exit");
    java.awt.event.ActionListener cancelAL = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    };
    _cancelButton.addActionListener( cancelAL );
    _cancelButton.registerKeyboardAction( cancelAL,
                                          KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                                          JComponent.WHEN_FOCUSED);

    _buttonPanel.setBorder(_buttonPanelBorder);
    _buttonPanelLayout.setHgap(5);
    _buttonSeparator.setBorder(_buttonSeparatorBorder);
    _infoTextArea.setContentType("text/html");
    _infoTextArea.setText("jEditorPane1");
    _infoTextArea.setMaximumSize(new Dimension(400, 200));
    _infoTextArea.setBorder(_infoTextAreaBorder);
    _infoTextArea.setEditable(false);
    _infoTextArea.setRequestFocusEnabled(false);
    _infoTextArea.setFont(new java.awt.Font("Serif", 0, 16));
    _infoTextArea.addHyperlinkListener(new javax.swing.event.HyperlinkListener()
    {

      public void hyperlinkUpdate(HyperlinkEvent e)
      {
        infoTextArea_hyperlinkUpdate(e);
      }
    });
    _blankButton1.setEnabled(false);
    _blankButton1.setOpaque(false);
    _blankButton1.setVisible(false);
    getContentPane().add(_mainPanel);
    _mainPanel.add(_northPanel, BorderLayout.NORTH);
    _northPanel.add(_assertLabel, BorderLayout.CENTER);
    _mainPanel.add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.add(_infoTextArea, BorderLayout.CENTER);
    _mainPanel.add(_southPanel, BorderLayout.SOUTH);
    _southPanel.add(_buttonSeparator, BorderLayout.NORTH);
    _southPanel.add(_buttonPanel, BorderLayout.CENTER);
    // Remove the email button due to, well, the end of software.
//    _buttonPanel.add(_emailButton, null);
    _buttonPanel.add(_viewButton, null);
    _buttonPanel.add(_blankButton1, null);
    _buttonPanel.add(_cancelButton, null);

    _cancelButton.requestFocus();
  }

  /**
  * This method creates an AssertEmailDialog with the required information and
  * displays it to the user.
  *
  * @author Steve Anonson
  */
  private void createEmailDialog()
  {
    AssertEmailDialog aed = new AssertEmailDialog(this, true,
                                                  _customerCompanyName,
                                                  _customerName,
                                                  _customerEmail,
                                                  _customerPhoneNumber,
                                                  _customerSmtpServer,
                                                  _axiEmailAddress,
                                                  _emailSubject);
    Point pnt = getLocation();
    aed.setLocation(pnt.x + _VIEW_EMAIL_OFFSET, pnt.y + _VIEW_EMAIL_OFFSET);
    aed.setStackTrace(_stackTrace);
    aed.setVisible(true);
    aed.toFront();
  }

  /**
  * This is the method that gets called when the send email button is clicked.
  *
  * @author Steve Anonson
  */
  private void emailButton_actionPerformed(ActionEvent e)
  {
    createEmailDialog();
  }

  /**
  * This method creates an AssertViewStackDialog with the stack trace information
  * and displays it to the user.
  *
  * @author Steve Anonson
  */
  private void createViewDialog()
  {
    AssertViewStackDialog avsd = new AssertViewStackDialog(this, false, _stackTrace);
    Point pnt = getLocation();
    avsd.setLocation(pnt.x + _VIEW_STACK_OFFSET, pnt.y + _VIEW_STACK_OFFSET);
    avsd.setVisible(true);
    avsd.toFront();
  }

  /**
  * This is the method that gets called when the view stack trace button is pressed.
  * It creates an AssertViewStackDialog with the stack trace information and
  * displays it to the user.
  *
  * @author Steve Anonson
  */
  private void viewButton_actionPerformed(ActionEvent e)
  {
    createViewDialog();
  }

  /**
  * This method removes the AssertDialog.
  *
  * @author Steve Anonson
  */
  private void cancelDialog()
  {
    setVisible(false);
    dispose();
  }

  /**
  * This is the method that gets called when the cancel button is pressed.
  *
  * @author Steve Anonson
  */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    cancelDialog();
  }

  /**
  * This method gets called when the user clicks on a hyperlink in the
  * message area.
  *
  * @author Steve Anonson
  */
  private void infoTextArea_hyperlinkUpdate(HyperlinkEvent e)
  {
    if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
    {
      URL url = e.getURL();
      if (url == null)
        return;
      try
      {
        BrowserLauncher.openURL(url.toString());
      }
      catch (IOException ioe)
      {
        String msg = "The browser could not be started with URL: " + url.toString();
        JOptionPane.showMessageDialog(this,msg,"Browser Error",JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  /**
  * Set the stack trace string that will appear in the view stack dialog.
  * This is the String that will be emailed back to SPT if the customer clicks
  * send email.
  *
  * @param stackTrace String containing the stack trace.
  * @author Steve Anonson
  */
  void setStackTrace(String stackTrace)
  {
    _stackTrace = stackTrace;
    if (_stackTrace != null)
      _viewButton.setEnabled(true);
  }

  /**
  * Set the error description.
  * This converts \n to <br> so that the text has the appropriate line breaks.
  * Kind of kludgy though.
  *
  * @author Steve Anonson
  */
  void setDescription(String description)
  {
    StringTokenizer st = new StringTokenizer( description, "\n");
    if (st.countTokens() > 1)
    {
      StringBuffer buffer = new StringBuffer();
      while (st.hasMoreTokens())
      {
        buffer.append(st.nextToken());
        if (st.hasMoreTokens())
          buffer.append("<br>");
      }
      setHtmlDescription(buffer.toString());
    }
    else
      setHtmlDescription(description);
  }

  /**
  * Set the error description.
  *
  * @author Steve Anonson
  */
  void setHtmlDescription(String description)
  {
    _errorMessage = description;
    _infoTextArea.setText(_errorMessage);
  }

  /**
  * Set the customer company name.
  *
  * @author Steve Anonson
  */
  void setCustomerCompanyName(String customerCompanyName)
  {
    _customerCompanyName = customerCompanyName;
  }

  /**
  * Set the customer technician/operator name.
  *
  * @author Steve Anonson
  */
  void setCustomerName(String customerName)
  {
    _customerName = customerName;
  }

  /**
  * Set customer email address.
  *
  * @author Steve Anonson
  */
  void setCustomerEmailAddress(String customerEmailAddress)
  {
    _customerEmail = customerEmailAddress;
  }

  /**
  * Set customer phone number.
  *
  * @author Steve Anonson
  */
  void setCustomerPhoneNumber(String customerPhoneNumber)
  {
    _customerPhoneNumber = customerPhoneNumber;
  }

  /**
  * Set the name of the customer SMTP server.
  *
  * @author Steve Anonson
  */
  void setSMTPServer(String smtpServer)
  {
    _customerSmtpServer = smtpServer;
  }

  /**
  * Set Agilent support email address.
  *
  * @author Steve Anonson
  */
  void setAgilentSupportEmailAddress(String axiSupportEmailAddress)
  {
    _axiEmailAddress = axiSupportEmailAddress;
  }

  /**
  * Set email subject.
  *
  * @author Steve Anonson
  */
  void setEmailSubject(String emailSubject)
  {
    _emailSubject = emailSubject;
  }

  /*
  * Main function used only for testing.
  */
  public static void main( String [] args )
  {
    AssertDialog dlg = new AssertDialog(null, true);
    // Add listener to call the class exit routine if dialog is closed.
    dlg.addWindowListener( new WindowAdapter()
    {
      public void windowClosing( WindowEvent e )
      {
        System.exit(0);
      }
    } );
    dlg.setDescription("Here is a string\nwith embedded newline\n\tand\ntab characters.");
    dlg.setStackTrace("stack trace");
    dlg.setVisible(true);
    dlg.toFront();
  }
}
