package com.axi.util;

import java.io.*;
import java.rmi.*;

/**
* Selftest class
*
* @author Bill Darbie
*/
public class Test_RemoteObservable extends UnitTest implements RemoteObserverInt
{
  private MyRemoteObservable _remoteObservable = null;
  private String _name;
  private int _numLoops = 10;
  private static int _numUpdates = 0;

  Test_RemoteObservable(String name)
  {
    _name = name;
    try
    {
      _remoteObservable = new MyRemoteObservable();
    }
    catch(RemoteException re)
    {
      re.printStackTrace();
    }
  }

  static public void main(String[] args)
  {
    UnitTest.execute(new Test_RemoteObservable("Observer1"));
  }

  // test:
  //   - the order that notifyObservers is called is the same order that the updates goes out
  //     do some sleeps to test this
  //   - the notifyObservers immediately returns
  //   - adding/removing observers while updates are happening
  public void test(BufferedReader is, PrintWriter os)
  {
    Expect.expect(_remoteObservable.countObservers() == 0);
    Expect.expect(_remoteObservable.hasChanged() == false);

    _remoteObservable.addObserver(this);
    // add it again - the second add should be ignored and update only called once per notifyObservers call
    _remoteObservable.addObserver(this);

    Expect.expect(_remoteObservable.countObservers() == 1);

    // this call should do nothing
    _remoteObservable.notifyObservers(new Integer(-1));

    // this call should cause update to get called - the update prints should
    // print the integers in order or there is a bug
    System.out.println("Testing that notifyObservers() is an asynchonous call and that updates always get");
    System.out.println("called in the same order as the notifyObservers() calls happen");
    System.out.println("All done calling dataChanged prints should happen before any update");
    System.out.println("prints happen or there is a bug - tests asynchronous nature of notifyObservers() call");
    System.out.println("All update calls should come up in numerical order or there is a bug - tests");
    System.out.println("that order of notifyObservers() and update() calls match");
    System.out.println("");
    for(int i = 0; i < _numLoops; ++i)
    {
      _remoteObservable.dataChanged(i);
      System.out.println("done calling dataChanged with an int of " + i);
    }
    Expect.expect(_remoteObservable.hasChanged() == true);

    waitForUpdatesToComplete(1);

    // now test that observers can be added and removed while updates are happening
    System.out.println();
    System.out.println("Testing that more than one observer will get updated properly");
    System.out.println("also testing the observers can be removed and added as updates occur");
    System.out.println("all updates for the 0 notifyObservers() call should happen first, then the 1's, etc");
    System.out.println();

    Test_RemoteObservable observer2 = new Test_RemoteObservable("Observer2");
    _remoteObservable.addObserver(observer2);
    System.out.println("added observer2");
    Expect.expect(_remoteObservable.countObservers() == 2);

    Test_RemoteObservable observer3 = new Test_RemoteObservable("Observer3");
    _remoteObservable.addObserver(observer3);
    System.out.println("added observer3");
    Expect.expect(_remoteObservable.countObservers() == 3);

    Test_RemoteObservable observer4 = new Test_RemoteObservable("Observer4");


    for(int i = 0; i < _numLoops; ++i)
    {
      _remoteObservable.dataChanged(i);
      System.out.println("done calling dataChanged with an int of " + i);
    }

    _remoteObservable.deleteObserver(observer3);
    _remoteObservable.addObserver(observer4);

    Expect.expect(_remoteObservable.countObservers() == 3);

    waitForUpdatesToComplete(3);

    // now test deleteObservers()
    _remoteObservable.deleteObservers();
    Expect.expect(_remoteObservable.countObservers() == 0);

    System.exit(0);
  }

  private void waitForUpdatesToComplete(int numObservers)
  {
    int numUpdates = 0;
    while (numUpdates < _numLoops * numObservers)
    {
      try
      {
        Thread.sleep(500);
      }
      catch(InterruptedException ie)
      {
      }

      // do not read _numUpdates while another thread is changing it
      synchronized(this)
      {
        numUpdates = _numUpdates;
      }
    }

    synchronized(this)
    {
      _numUpdates = 0;
    }
  }

  public synchronized void update(RemoteObservableInt remoteObservable, Object hint)
  {
    // sleep here to allow all the dataChanged calls to happen first.  This will prove
    // that the notifyObservers() call does return immediately (an asynchronous call)
    // this also makes the multitheading more likely to be predictable as far as when
    // all the print statements will occur
    try
    {
      Thread.sleep(300);
    }
    catch(InterruptedException ie)
    {
    }

    ++_numUpdates;

    Integer value = (Integer)hint;
    System.out.println("update was called with a hint of " + value);
  }
}

class MyRemoteObservable extends RemoteObservable
{
  MyRemoteObservable() throws RemoteException
  {
    // do nothing
  }

  void dataChanged(int i)
  {
    setChanged();
    notifyObservers(new Integer(i));
  }
}
