package com.axi.util;

/**
 * This exception gets thrown when a divide by zero is encountered.
 * @author George A. David
 */
public class DivideByZeroException extends Exception
{
  /**
   * @author George A. David
   */
  public DivideByZeroException()
  {
    super("Attempted to divide by zero");
  }
}
