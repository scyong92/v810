package com.axi.util;

import java.io.*;

/**
 * This exception gets thrown when a directory was expected, but a file was
 * found instead.
 *
 * @author Bill Darbie
 */
public class FileFoundWhereDirectoryWasExpectedException extends IOException
{
  private String _directoryName;

  /**
   * @author Bill Darbie
   */
  public FileFoundWhereDirectoryWasExpectedException(String directoryName)
  {
    super("The file " + directoryName + " was found where a directory was expected.");
    Assert.expect(directoryName != null);
    _directoryName = directoryName;
  }

  /**
   * @author Bill Darbie
   */
  public String getDirectoryName()
  {
    return _directoryName;
  }
}