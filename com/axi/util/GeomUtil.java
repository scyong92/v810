package com.axi.util;

import java.awt.geom.*;

/**
 * This class contains utility methods for geometric shapes.
 * @author George A. David
 * @author Bill Darbie
 */
public class GeomUtil
{
  /**
   * Rotate pointToRotate around the origin degreesRotation
   * @author George A. David
   */
  public static void rotatePointAroundOrigin(Point2D pointToRotateIn, double degreesRotationIn, Point2D resultOut)
  {
    Point2D.Double pointOfRotationIn = new Point2D.Double(0, 0);
    rotatePoint1AroundPoint2(pointToRotateIn, pointOfRotationIn, degreesRotationIn, resultOut);
  }

  /**
   * Rotate pointToRotate around the origin degreesRotation
   * @author George A. David
   */
  public static void rotatePointAroundOrigin(Point2D pointToRotateInOut, double degreesRotationIn)
  {
    rotatePointAroundOrigin(pointToRotateInOut, degreesRotationIn, pointToRotateInOut);
  }

  /**
   * Rotate pointToRotate around pointOfRotation by degreesRotation
   * @author George A. David
   */
  public static void rotatePoint1AroundPoint2(Point2D pointToRotateIn,
                                              Point2D pointOfRotationIn,
                                              double degreesRotationIn,
                                              Point2D resultOut)
  {
    Assert.expect(pointToRotateIn != null);
    Assert.expect(pointOfRotationIn != null);
    Assert.expect(resultOut != null);

    double x = pointToRotateIn.getX() - pointOfRotationIn.getX();
    double y = pointToRotateIn.getY() - pointOfRotationIn.getY();
    double radians = Math.toRadians(degreesRotationIn);
    double sinRadians = Math.sin(radians);
    double cosRadians = Math.cos(radians);
    // 2d rotation
    // Xn = Xo cos(theta) - Yo sin(theta)
    // Yn = Yo cos(theta) + Xo sin(theta)
    // theta is in radians
    double newXCoord = x * cosRadians - y * sinRadians;
    double newYCoord = y * cosRadians + x * sinRadians;
    resultOut.setLocation(newXCoord + pointOfRotationIn.getX(), newYCoord + pointOfRotationIn.getY());
  }

  /**
   * Rotate pointToRotate around pointOfRotation by degreesRotation
   * @author George A. David
   */
  public static void rotatePoint1AroundPoint2(Point2D pointToRotateInOut,
                                              Point2D pointOfRotationIn,
                                              double degreesRotationIn)
  {
    rotatePoint1AroundPoint2(pointToRotateInOut,
                             pointOfRotationIn,
                             degreesRotationIn,
                             pointToRotateInOut);
  }

  /**
   * Create an obround, which is a rectangle with semicircles on the two shorter
   * sides.
   *
   * @author Peter Esbensen
   */
  public static java.awt.Shape createObround(int lowerLeftLocationX,
                                             int lowerLeftLocationY,
                                             int width,
                                             int height)
  {
    int arcSize = 0;
    if (width < height)
      arcSize = width;
    else
      arcSize = height;
    return new RoundRectangle2D.Double(lowerLeftLocationX,
                                       lowerLeftLocationY,
                                       width,
                                       height,
                                       arcSize,
                                       arcSize);
  }

  /**
   * Create a rectangle in the form of a GeneralPath from the four corners passed in.
   * @author George A. David
   */
  public static GeneralPath createShape(Point2D topLeftIn,
                                        Point2D topRightIn,
                                        Point2D bottomLeftIn,
                                        Point2D bottomRightIn)
  {
    Assert.expect(topLeftIn != null);
    Assert.expect(topRightIn != null);
    Assert.expect(bottomLeftIn != null);
    Assert.expect(bottomRightIn != null);

    // create the lines that define the shape
    Line2D topLine = new Line2D.Double(topLeftIn, topRightIn);
    Line2D bottomLine = new Line2D.Double(bottomLeftIn, bottomRightIn);
    Line2D leftLine = new Line2D.Double(topLeftIn, bottomLeftIn);
    Line2D rightLine = new Line2D.Double(topRightIn, bottomRightIn);

    // create the shape from the four lines
    GeneralPath polygon = new GeneralPath(topLine);
    polygon.append(leftLine, true);
    polygon.append(bottomLine, true);
    polygon.append(rightLine, true);

    return polygon;
  }

  /**
   * Given the bottom left coordinate and the width and length of a rectangle, this
   * method will calculate the four corners of the rectangle.
   * @author Bill Darbie
   */
  public static void calculateCornersOfRectangle(Point2D bottomLeftCoordinateIn,
                                                 double widthIn,
                                                 double lengthIn,
                                                 Point2D topLeftOut,
                                                 Point2D topRightOut,
                                                 Point2D bottomLeftOut,
                                                 Point2D bottomRightOut)
  {
    Assert.expect(bottomLeftCoordinateIn != null);
    Assert.expect(widthIn > 0);
    Assert.expect(lengthIn > 0);
    Assert.expect(topLeftOut != null);
    Assert.expect(topRightOut != null);
    Assert.expect(bottomRightOut != null);
    Assert.expect(bottomLeftOut != null);

    topLeftOut.setLocation(bottomLeftCoordinateIn.getX(), bottomLeftCoordinateIn.getY() + lengthIn);
    topRightOut.setLocation(bottomLeftCoordinateIn.getX() + widthIn, bottomLeftCoordinateIn.getY() + lengthIn);
    bottomRightOut.setLocation(bottomLeftCoordinateIn.getX() + widthIn, bottomLeftCoordinateIn.getY());
    bottomLeftOut.setLocation(bottomLeftCoordinateIn);
  }

  /**
   * Given the bottom left coordinate and the width and length of a rectangle, this
   * method will calculate the four corners of the rectangle.
   * @author Bill Darbie
   */
  public static void calculateCornersOfRectangle(DoubleCoordinate bottomLeftCoordinateIn,
                                                 double widthIn,
                                                 double lengthIn,
                                                 DoubleCoordinate topLeftOut,
                                                 DoubleCoordinate topRightOut,
                                                 DoubleCoordinate bottomLeftOut,
                                                 DoubleCoordinate bottomRightOut)
  {
    Assert.expect(bottomLeftCoordinateIn != null);
    Assert.expect(widthIn > 0);
    Assert.expect(lengthIn > 0);
    Assert.expect(topLeftOut != null);
    Assert.expect(topRightOut != null);
    Assert.expect(bottomRightOut != null);
    Assert.expect(bottomLeftOut != null);

    Point2D bottomLeftCoordinatePointIn = new Point2D.Double(bottomLeftCoordinateIn.getX(), bottomLeftCoordinateIn.getY());
    Point2D topLeftPointOut = new Point2D.Double();
    Point2D topRightPointOut = new Point2D.Double();
    Point2D bottomRightPointOut = new Point2D.Double();
    Point2D bottomLeftPointOut = new Point2D.Double();

    calculateCornersOfRectangle(bottomLeftCoordinatePointIn,
                                widthIn,
                                lengthIn,
                                topLeftPointOut,
                                topRightPointOut,
                                bottomLeftPointOut,
                                bottomRightPointOut);
    topLeftOut.setLocation(topLeftPointOut);
    topRightOut.setLocation(topRightPointOut);
    bottomRightOut.setLocation(bottomRightPointOut);
    bottomLeftOut.setLocation(bottomLeftPointOut);
  }

  /**
   * Rotate the rectangle passed in as four points by degrees rotation around the bottom left corner of the
   * rectangle.
   * @author George A. David
   */
  public static void rotateRectangleAroundLowerLeftCorner(Point2D topLeftInOut,
                                                          Point2D topRightInOut,
                                                          Point2D bottomRightInOut,
                                                          Point2D bottomLeftInOut,
                                                          double degreesRotationIn)
  {
    rotateRectangleAroundPoint(topLeftInOut,
                               topRightInOut,
                               bottomRightInOut,
                               bottomLeftInOut,
                               degreesRotationIn,
                               new Point2D.Double(bottomLeftInOut.getX(), bottomLeftInOut.getY()));
  }

  /**
   * Rotate the rectangle passed in as four points by degrees rotation
   * @author George A. David
   */
  public static void rotateRectangleAroundLowerLeftCorner(DoubleCoordinate topLeftInOut,
                                                          DoubleCoordinate topRightInOut,
                                                          DoubleCoordinate bottomRightInOut,
                                                          DoubleCoordinate bottomLeftInOut,
                                                          double degreesRotationIn)
  {
    Assert.expect(topLeftInOut != null);
    Assert.expect(topRightInOut != null);
    Assert.expect(bottomRightInOut != null);
    Assert.expect(bottomLeftInOut != null);

    Point2D topLeftPoint = new Point2D.Double(topLeftInOut.getX(), topLeftInOut.getY());
    Point2D topRightPoint = new Point2D.Double(topRightInOut.getX(), topRightInOut.getY());
    Point2D bottomRightPoint = new Point2D.Double(bottomRightInOut.getX(), bottomRightInOut.getY());
    Point2D bottomLeftPoint = new Point2D.Double(bottomLeftInOut.getX(), bottomLeftInOut.getY());

    rotateRectangleAroundPoint(topLeftPoint,
                               topRightPoint,
                               bottomRightPoint,
                               bottomLeftPoint,
                               degreesRotationIn,
                               new Point2D.Double(bottomLeftInOut.getX(), bottomLeftInOut.getY()));

    topLeftInOut.setLocation(topLeftPoint);
    topRightInOut.setLocation(topRightPoint);
    bottomRightInOut.setLocation(bottomRightPoint);
    bottomLeftInOut.setLocation(bottomLeftPoint);
  }

  /**
   * Rotate the rectangle passed in as four points around the pointOfRotation by degreesRotation.
   * @author George A. David
   */
  public static void rotateRectangleAroundPoint(Point2D topLeftInOut,
                                                Point2D topRightInOut,
                                                Point2D bottomRightInOut,
                                                Point2D bottomLeftInOut,
                                                double degreesRotationIn,
                                                Point2D pointOfRotationIn)
  {
    Assert.expect(topLeftInOut != null);
    Assert.expect(topRightInOut != null);
    Assert.expect(bottomRightInOut != null);
    Assert.expect(bottomLeftInOut != null);
    Assert.expect(pointOfRotationIn != null);

    rotatePoint1AroundPoint2(topLeftInOut, pointOfRotationIn, degreesRotationIn, topLeftInOut);
    rotatePoint1AroundPoint2(topRightInOut, pointOfRotationIn, degreesRotationIn, topRightInOut);
    rotatePoint1AroundPoint2(bottomRightInOut, pointOfRotationIn, degreesRotationIn, bottomRightInOut);
    rotatePoint1AroundPoint2(bottomLeftInOut, pointOfRotationIn, degreesRotationIn, bottomLeftInOut);

  }

  /**
   * Rotate the rectangle passed in as four points around the pointOfRotation by degreesRotation.
   * @author George A. David
   */
  public static void rotateRectangleAroundPoint(DoubleCoordinate topLeft,
                                                DoubleCoordinate topRight,
                                                DoubleCoordinate bottomRight,
                                                DoubleCoordinate bottomLeft,
                                                double degreesRotation,
                                                DoubleCoordinate rotationCoordinate)
  {
    Assert.expect(topLeft != null);
    Assert.expect(topRight != null);
    Assert.expect(bottomRight != null);
    Assert.expect(bottomLeft != null);
    Assert.expect(rotationCoordinate != null);

    Point2D topLeftPoint = new Point2D.Double(topLeft.getX(), topLeft.getY());
    Point2D topRightPoint = new Point2D.Double(topRight.getX(), topRight.getY());
    Point2D bottomRightPoint = new Point2D.Double(bottomRight.getX(), bottomRight.getY());
    Point2D bottomLeftPoint = new Point2D.Double(bottomLeft.getX(), bottomLeft.getY());
    Point2D pointOfRotation = new Point2D.Double(rotationCoordinate.getX(), rotationCoordinate.getY());

    rotateRectangleAroundPoint(topLeftPoint,
                               topRightPoint,
                               bottomRightPoint,
                               bottomLeftPoint,
                               degreesRotation,
                               pointOfRotation);

    topLeft.setLocation(topLeftPoint);
    topRight.setLocation(topRightPoint);
    bottomRight.setLocation(bottomRightPoint);
    bottomLeft.setLocation(bottomLeftPoint);
  }

  /**
   * Add point1 to point2 and put the result in result
   * @author George A. David
   */
  public static void add(Point2D point1In, Point2D point2In, Point2D resultOut)
  {
    Assert.expect(point1In != null);
    Assert.expect(point2In != null);
    Assert.expect(resultOut != null);

    resultOut.setLocation(point1In.getX() + point2In.getX(), point1In.getY() + point2In.getY());
  }

  /**
   * Add point1 to point2 and put the result in point1
   * @author George A. David
   */
  public static void add(Point2D point1InOut, Point2D point2In)
  {
    add(point1InOut, point2In, point1InOut);
  }

  /**
   * Add the offset to all four points.
   * @author George A. David
   */
  public static void add(Point2D offsetIn,
                         Point2D topLeftInOut,
                         Point2D topRightInOut,
                         Point2D bottomRightInOut,
                         Point2D bottomLeftInOut)
  {
    add(topLeftInOut, offsetIn, topLeftInOut);
    add(topRightInOut, offsetIn, topRightInOut);
    add(bottomRightInOut, offsetIn, bottomRightInOut);
    add(bottomLeftInOut, offsetIn, bottomLeftInOut);
  }

  /**
   * Add the offset to all four points.
   * @author George A. David
   */
  public static void add(DoubleCoordinate offsetIn,
                         DoubleCoordinate topLeftInOut,
                         DoubleCoordinate topRightInOut,
                         DoubleCoordinate bottomRightInOut,
                         DoubleCoordinate bottomLeftInOut)

  {
    Assert.expect(offsetIn != null);
    Assert.expect(topLeftInOut != null);
    Assert.expect(topRightInOut != null);
    Assert.expect(bottomRightInOut != null);
    Assert.expect(bottomLeftInOut != null);

    Point2D offsetPoint = new Point2D.Double(offsetIn.getX(), offsetIn.getY());
    Point2D topLeftPoint = new Point2D.Double(topLeftInOut.getX(), topLeftInOut.getY());
    Point2D topRightPoint = new Point2D.Double(topRightInOut.getX(), topRightInOut.getY());
    Point2D bottomRightPoint = new Point2D.Double(bottomRightInOut.getX(), bottomRightInOut.getY());
    Point2D bottomLeftPoint = new Point2D.Double(bottomLeftInOut.getX(), bottomLeftInOut.getY());

    add(offsetPoint,
        topLeftPoint,
        topRightPoint,
        bottomRightPoint,
        bottomLeftPoint);

    topLeftInOut.setLocation(topLeftPoint);
    topRightInOut.setLocation(topRightPoint);
    bottomRightInOut.setLocation(bottomRightPoint);
    bottomLeftInOut.setLocation(bottomLeftPoint);
  }

  /**
   * Transform the point from an x,y coordinate system that has the origin at the bottom left
   * to an x,y coordinate system that has the origin at the top left.
   *
   * @author Bill Darbie
   */
  public static void transformOriginFromLowerLeftToUpperLeft(DoubleRef xInOut, DoubleRef yInOut, double maxYin)
  {
    Assert.expect(xInOut != null);
    Assert.expect(yInOut != null);

    Point2D point = new Point2D.Double(xInOut.getValue(), yInOut.getValue());
    transformOriginFromLowerLeftToUpperLeft(point, maxYin);
    xInOut.setValue(point.getX());
    yInOut.setValue(point.getY());
  }

  /**
   * Transform the point from an x,y coordinate system that has the origin at the bottom left
   * to an x,y coordinate system that has the origin at the top left.
   *
   * @author George A. David
   */
  public static void transformOriginFromLowerLeftToUpperLeft(Point2D pointIn, double maxYin, Point2D resultOut)
  {
    Assert.expect(pointIn != null);
    Assert.expect(resultOut != null);

    resultOut.setLocation(pointIn.getX(), maxYin - pointIn.getY());
  }

  /**
   * Transform the point from an x,y coordinate system that has the origin at the bottom left
   * to an x,y coordinate system that has the origin at the top left.
   * @author George A. David
   */
  public static void transformOriginFromLowerLeftToUpperLeft(Point2D pointInOut, double maxYin)
  {
    transformOriginFromLowerLeftToUpperLeft(pointInOut, maxYin, pointInOut);
  }

  /**
   * Using the supplied offset, transform the rectangle to screen coordinates.
   * @author George A. David
   */
  public static void transformOriginFromLowerLeftToUpperRight(Point2D topLeftInOut,
                                                              Point2D topRightInOut,
                                                              Point2D bottomRightInOut,
                                                              Point2D bottomLeftInOut,
                                                              double maxYin)
  {
    transformOriginFromLowerLeftToUpperLeft(topLeftInOut, maxYin, topLeftInOut);
    transformOriginFromLowerLeftToUpperLeft(topRightInOut, maxYin, topRightInOut);
    transformOriginFromLowerLeftToUpperLeft(bottomRightInOut, maxYin, bottomRightInOut);
    transformOriginFromLowerLeftToUpperLeft(bottomLeftInOut, maxYin, bottomLeftInOut);
  }

  /**
   * return a Point2D with the minimum x and minimum y of the two points passed in.
   * @author George A. David
   */
  public static void calculateMinimumPoint(Point2D point1In, Point2D point2In, Point2D minimumPointOut)
  {
    Assert.expect(point1In != null);
    Assert.expect(point2In != null);
    Assert.expect(minimumPointOut != null);

    minimumPointOut.setLocation(Math.min(point1In.getX(), point2In.getX()), Math.min(point1In.getY(), point2In.getY()));
  }

  /**
   * @return a Point2D with the smallest x and smallest y of the four points passed in.
   * @author George A. David
   */
  public static Point2D getLowerLeftCornerOfRectangle(Point2D topLeftIn,
                                                      Point2D topRightIn,
                                                      Point2D bottomRightIn,
                                                      Point2D bottomLeftIn)
  {
    Point2D minimumPoint = new Point2D.Double(0, 0);
    calculateMinimumPoint(topLeftIn, topRightIn, minimumPoint);
    calculateMinimumPoint(bottomRightIn, minimumPoint, minimumPoint);
    calculateMinimumPoint(bottomLeftIn, minimumPoint, minimumPoint);

    return minimumPoint;
  }

  /**
   * @return a DoubleCoodinate with the smallest x and smallest y of the four points passed in.
   * @author George A. David
   */
  public static DoubleCoordinate getLowerLeftCornerOfRectangle(DoubleCoordinate topLeftIn,
                                                               DoubleCoordinate topRightIn,
                                                               DoubleCoordinate bottomRightIn,
                                                               DoubleCoordinate bottomLeftIn)
  {
    Assert.expect(topLeftIn != null);
    Assert.expect(topRightIn != null);
    Assert.expect(bottomRightIn != null);
    Assert.expect(bottomLeftIn != null);

    Point2D topLeftPoint = new Point2D.Double(topLeftIn.getX(), topLeftIn.getY());
    Point2D topRightPoint = new Point2D.Double(topRightIn.getX(), topRightIn.getY());
    Point2D bottomRightPoint = new Point2D.Double(bottomRightIn.getX(), bottomRightIn.getY());
    Point2D bottomLeftPoint = new Point2D.Double(bottomLeftIn.getX(), bottomLeftIn.getY());

    Point2D minimumPoint = getLowerLeftCornerOfRectangle(topLeftPoint,
                                                         topRightPoint,
                                                         bottomRightPoint,
                                                         bottomLeftPoint);

    return new DoubleCoordinate(minimumPoint);
  }

  /**
   * @return the lower left corner of the rectangle described by a coordinate, a width, and a length after
   *         it has been rotated by degreesRotation
   * @author George A. David
   */
  public static DoubleCoordinate getLowerLeftCornerOfRectangle(DoubleCoordinate coordinateOfRectangleIn,
                                                               double widthIn,
                                                               double lengthIn,
                                                               double degreesRotationIn)
  {
    Assert.expect(coordinateOfRectangleIn != null);

    Point2D topLeft = new Point2D.Double();
    Point2D topRight = new Point2D.Double();
    Point2D bottomRight = new Point2D.Double();
    Point2D bottomLeft = new Point2D.Double();
    Point2D coordinate = new Point2D.Double(coordinateOfRectangleIn.getX(), coordinateOfRectangleIn.getY());

    calculateCornersOfRectangle(coordinate,
                                widthIn,
                                lengthIn,
                                topLeft,
                                topRight,
                                bottomLeft,
                                bottomRight);
    rotateRectangleAroundPoint(topLeft,
                               topRight,
                               bottomRight,
                               bottomLeft,
                               degreesRotationIn,
                               coordinate);
    Point2D lowerLeftCorner = getLowerLeftCornerOfRectangle(topLeft, topRight, bottomRight, bottomLeft);

    return new DoubleCoordinate(lowerLeftCorner);
  }

  /**
   * return the new width and length of the rectangle after rotation.
   * @author George A. David
   */
  public static void rotateWidthAndLengthOfRectangleAroundOrigin(DoubleRef widthInOut, DoubleRef lengthInOut, double degreesRotationIn)
  {
    Assert.expect(widthInOut != null);
    Assert.expect(lengthInOut != null);

    Point2D point = new Point2D.Double(widthInOut.getValue(), lengthInOut.getValue());
    rotatePointAroundOrigin(point, degreesRotationIn, point);
    widthInOut.setValue(Math.abs(point.getX()));
    lengthInOut.setValue(Math.abs(point.getY()));
  }

  /**
   * @author Matt Wharton
   */
  public static double getCircumferenceOfEllipse(Ellipse2D ellipse)
  {
    Assert.expect(ellipse != null);

    double horizontalRadius = Math.round(ellipse.getWidth() / 2d);
    double verticalRadius = Math.round(ellipse.getHeight() / 2d);
    double circumference = (2 * Math.PI) * Math.sqrt((horizontalRadius * horizontalRadius) + (verticalRadius * verticalRadius));

    return circumference;
  }

  /**
   * @author George A. David
   */
  public static double calculateSlope(double x1, double y1,
                                      double x2, double y2)
  {
    Assert.expect(x1 != x2, "Divide by zero error");
    return (y2 - y1) / (x2 - x1);
  }

  /**
   * give two coordinates in a straing line, calculate the y-intercept
   * @author George A. David
   */
  public static double calculateYintercept(double x1, double y1, double x2, double y2)
  {
    double slope = calculateSlope(x1, y1, x2, y2);

    return calculateYintercept(x1, y1, slope);
  }

  /**
   * given one coordinate and the slope, calculate the y-intercept
   * @author George A. David
   */
  public static double calculateYintercept(double x1, double y1, double slope)
  {
    // using the equation y = mx + b
    // b = y - mx
    return y1 - (slope * x1);
  }

  /**
   * given two coordinates, calculate the midpoint of the segment
   * encompassed by xStart and xEnd
   * @author George A. David
   */
  public static DoubleCoordinate calculateMidPointUsingXvalues(double x1, double y1,
                                                   double x2, double y2,
                                                   double xStart, double xEnd)
  {
    Assert.expect(x1 != x2, "Divide by zero error");

    double slope = (y1 - y2) / (x1 - x2);
    double yIntercept = calculateYintercept(x1, y1, slope);

    return calculateMidPointUsingXvalues(slope, yIntercept, xStart, xEnd);
  }

  /**
   * @author George A. David
   */
  /**
   * given two coordinates, calculate the midpoint of the segment
   * encompassed by xStart and xEnd
   * @author George A. David
   */
  public static DoubleCoordinate calculateMidPointUsingXvalues(double slope,
                                                   double yIntercept,
                                                   double xStart, double xEnd)
  {
    // recalling y = mx + b
    double yStart = slope * xStart + yIntercept;
    double yEnd = slope * xEnd + yIntercept;

    return new DoubleCoordinate((xEnd + xStart) / 2d, (yEnd + yStart) / 2d);
  }
}
