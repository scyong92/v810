package com.axi.util;

import java.util.*;

/**
* ThreadSafeObservable provides the same functionality as java.util.Observable
* except that it supports the Observer/Observable design pattern in a mutli-threaded environment.
* Extend any class that you want to be Observable with this class just as you would with
* java.util.Observable.  Your Observer class will need to implement the Observer interface
* just like you would with the java.util.Observer class.
*
* This class is written to handle multiple threads.  It provides the following multiple
* threading features.  The notifyObservers() call returns immediately.  The actual updating of
* observers happens on another thread.  This mimimizes how much the code calling the notifyObservers()
* will be slowed down.  It also decreases the chance of a deadlock.  The update() method will
* be called in the same order that the notifyObservers() calls come in.
*
* Observers should ALWAYS spawn another thread to handle the update call.  This allows this
* class to notify all observers as quickly as possible.  It also prevents thread deadlock.
* Also the observers update() call should be synchonized since it is possible for more than
* one thread to call it.  If your update call is going to call any Swing methods, you must
* call SwingUtilites - invokeLater() to get the GUI calls on the proper thread.
*
* If you need an Observer/Observable that uses RMI see the RemoteObservable and RemoteObserverInt
* classes.
*
* @see java.util.Observable
* @see javax.swing.SwingUtilities - invokeLater(Runnable)
* @see RemoteObservable
* @see RemoteObserverInt
* @author Bill Darbie
*/
public class ThreadSafeObservable extends Observable
{
  private WrappedThreadSafeObservable _wrappedThreadSafeObservable;

  /**
   * @author Bill Darbie
   */
  private ThreadSafeObservable()
  {
    // do nothing
  }

  /**
   * @param threadName is the name that will show up in a debugger for the thread this Observable uses.
   * @author Bill Darbie
   */
  public ThreadSafeObservable(String threadName)
  {
    Assert.expect(threadName != null);

    _wrappedThreadSafeObservable = new WrappedThreadSafeObservable(this, threadName);
  }

  /**
   * Add a remote observer to the list of Observers that will get called
   * when a notifyObservers() call is made.
   *
   * @author Bill Darbie
   */
  public void addObserver(Observer observer)
  {
    Assert.expect(observer != null);

    _wrappedThreadSafeObservable.addObserver(observer);
  }

  /**
   * Delete an Observer from the list of Observers that will get called
   * when a notifyObservers() call is made
   *
   * @author Bill Darbie
   */
  public void deleteObserver(Observer observer)
  {
    _wrappedThreadSafeObservable.deleteObserver(observer);
  }

  /**
   * Delete the entire list of Observers.
   *
   * @author Bill Darbie
   */
  public synchronized void deleteObservers()
  {
    _wrappedThreadSafeObservable.deleteObservers();
  }

  /**
   * Iterate over the list of observers and call their update() method to
   * tell them that some data that they are observing has changed.  Note that
   * the update method that this calls MUST be synchronized and must spawn off a new
   * thread to do any calls back into the server.  Failure to do this will cause
   * a potential deadlock situation or unpredictable program behavior.  If your
   * update() method is going to call Swing methods it must use SwingUtils - invokeLater()
   *
   * @see javax.swing.SwingUtilities - invokeLater(Runnable)
   * @author Bill Darbie
   */
  public void notifyObservers()
  {
    _wrappedThreadSafeObservable.notifyObservers();
  }

  /**
   * Iterate over the list of observers and call their update() method to
   * tell them that some data that they are observing has changed.  Note that
   * the update method that this calls MUST be synchronized and must spawn off a new
   * thread to do any calls back into the server.  Failure to do this will cause
   * a potential deadlock situation or unpredictable program behavior.  If your
   * update() method is going to call Swing methods it must use SwingUtils - invokeLater()
   *
   * @see javax.swing.SwingUtilities - invokeLater(Runnable)
   * @author Bill Darbie
   */
  public void notifyObservers(Object arg)
  {
    _wrappedThreadSafeObservable.notifyObservers(arg);
  }

  /**
   * Return a count of the number of Observers that will be notified if
   * notifyObservers is called.
   *
   * @author Bill Darbie
   */
  public int countObservers()
  {
    return _wrappedThreadSafeObservable.countObservers();
  }

  /**
   * @return true if something in the Observable class has changed since the last notifyObservers()
   * call was made.
   *
   * @author Bill Darbie
   */
  public boolean hasChanged()
  {
    return _wrappedThreadSafeObservable.hasChanged();
  }

  /**
   * Notifies this class that something has changed in the Observable data.
   * If notifyObservers() is called and setChanged was never called, then
   * observers update() methods will NOT be called since no data was changed.
   *
   * @author Bill Darbie
   */
  protected void setChanged()
  {
    _wrappedThreadSafeObservable.setChanged();
  }

  /**
   * clears the flag that is set by setChanged()
   * @author Bill Darbie
   * @author Patrick Lacz
   */
  protected void clearChanged()
  {
    // do not call into the _wrappedThreadSafeObservable. It will call out to here when it is finished with notification.
    // this method should not be invoked outside of WrappedThreadSafeObservable or ThreadSafeObservable.
  }
}
