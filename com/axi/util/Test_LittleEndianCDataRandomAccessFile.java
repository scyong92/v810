package com.axi.util;
import java.io.*;

/**
 * Implemented to test the LittleEndianCDataRandomAccessFile class
 *
 * @see LittleEndianCDataRandomAccessFile
 * @author Michael Martinez-Schiferl
 */
public class Test_LittleEndianCDataRandomAccessFile extends UnitTest
{
  /**
   * @author Michael Martinez-Schiferl
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_LittleEndianCDataRandomAccessFile());
  }

  /**
   * @author Michael Martinez-Schiferl
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      String fileName = "temp";
      LittleEndianCDataRandomAccessFile raf = new LittleEndianCDataRandomAccessFile(fileName, "rw");

      // test writeByte(byte) and readByte()
      raf.seek(0);
      byte byteValue = 0x24;
      raf.writeByte(byteValue);
      raf.seek(0);
      Expect.expect(raf.readByte() == byteValue);

      // test readUnsignedByte
      raf.seek(0);
      // -0x0b = 0x00FB in 2's complement
      raf.writeByte(-0x0b);
      raf.seek(0);
      Expect.expect(raf.readByte() == -0x0b);
      raf.seek(0);
      Expect.expect(raf.readUnsignedByte() == (byte)0xf5);

      // test readUnsignedChar
      raf.seek(0);
      Expect.expect(raf.readUnsignedChar() == 0xf5);

      // test writeByte(int) and readByte()
      raf.seek(0);
      int intValue = 0x1234;
      raf.writeByte(intValue);
      raf.seek(0);
      byte result = raf.readByte();
      Expect.expect(result == 0x34);

      // test writeByte(byte[]) and readByte()
      raf.seek(0);
      try
      {
        raf.write(null);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }

      byte[] bytes = new byte[3];
      bytes[0] = 0x10;
      bytes[1] = 0x11;
      bytes[2] = 0x12;
      raf.write(bytes);
      raf.seek(0);
      Expect.expect(raf.readByte() == bytes[0]);
      Expect.expect(raf.readByte() == bytes[1]);
      Expect.expect(raf.readByte() == bytes[2]);

      // test readFully(byte[])
      raf.seek(0);
      try
      {
        raf.readFully(null);
        Expect.expect(false);
      }
      catch (AssertException ae)
      {
        // do nothing
      }

      byte[] readBytes = new byte[3];
      raf.readFully(readBytes);
      Expect.expect(readBytes[0] == bytes[0]);
      Expect.expect(readBytes[1] == bytes[1]);
      Expect.expect(readBytes[2] == bytes[2]);

      // test readFully(byte[], int offset, int length)
      raf.seek(0);
      try
      {
        raf.readFully(null, 2, 2);
        Expect.expect(false);
      }
      catch (AssertException ae)
      {
        // do nothing
      }
      readBytes = new byte[3];
      try
      {
        raf.readFully(readBytes, -1, 2);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }
      try
      {
        raf.readFully(readBytes, 2, -1);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }
      raf.readFully(readBytes, 1, 2);
      Expect.expect(readBytes[1] == bytes[0]);
      Expect.expect(readBytes[2] == bytes[1]);

      // test writeBytes(String)
      raf.seek(0);
      String str = "hello";
      raf.writeBytes(str);
      raf.writeByte('\0');
      raf.seek(0);
      // add 1 to the length to account for the null terminator
      Assert.expect(raf.readString(str.length() + 1).equals(str));

      try
      {
        raf.writeBytes(null);
        Expect.expect(false);
      }
      catch (AssertException ae)
      {
        // do nothing
      }
      try
      {
        raf.readString(-1);
        Expect.expect(false);
      }
      catch (AssertException ae)
      {
        // do nothing
      }

      // test writeBoolean
      raf.seek(0);
      raf.writeBoolean(true);
      raf.seek(0);
      Expect.expect(raf.readBoolean() == true);
      raf.seek(0);
      raf.writeBoolean(false);
      raf.seek(0);
      Expect.expect(raf.readBoolean() == false);

      // test writeShort();
      raf.seek(0);
      short shortValue = -2;
      raf.writeShort(shortValue);
      raf.seek(0);
      Expect.expect(raf.readShort() == shortValue);

      // test readUnsignedShort
      raf.seek(0);
      Expect.expect(raf.readUnsignedShort() == 0xfffe); // 0xfffe is the 2's complement representation of -2

      // test writeInt()
      raf.seek(0);
      raf.writeInt(-77);
      raf.seek(0);
      Expect.expect(raf.readInt() == -77);

      // test readUnsignedInt()
      raf.seek(0);
      Expect.expect(raf.readUnsignedInt() == 0xffffffffffffffb3L);

      // test writeLong()
      raf.seek(0);
      long longValue = 25;
      raf.writeLong(longValue);
      raf.seek(0);
      Expect.expect(raf.readLong() == longValue);

      // test writeFloat
      float floatValue = 1234.56f;
      raf.seek(0);
      raf.writeFloat(floatValue);
      raf.seek(0);
      Expect.expect(raf.readFloat() == floatValue);

      // test writeDouble
      double doubleValue = 7654.32;
      raf.seek(0);
      raf.writeDouble(doubleValue);
      raf.seek(0);
      Expect.expect(raf.readDouble() == doubleValue);

      // test writePascalString
      str = "hi";
      raf.seek(0);
      raf.writePascalString(str);
      raf.seek(0);
      Expect.expect(raf.readPascalString().equals(str));

      // test writePascalPackedArray();
      str = "abc";
      raf.seek(0);
      raf.writePascalPackedArray(str);
      raf.seek(0);
      Expect.expect(raf.readPascalPackedArray(str.length()).equals(str));
    }
    catch(IOException ioe)
    {
      ioe.printStackTrace();
    }
  }
}
