package com.axi.util;

import java.io.*;
import java.net.*;
import java.nio.*;
import java.util.*;

/**
 * This is a unit test for both SocketServer and SocketClient
 *
 * @author Eugene Kim-Leighton
 */
public class Test_SocketClient extends UnitTest implements SocketServerMessageHandlerInt, Runnable
{
  private static final String _CR_LF         = "\r\n";
  private static final String _LF            = "\n";
  private SocketServer        _socketServer  = null;
  private SocketClient        _socketClient  = null;
  private int                 _port          = 0;
  private boolean             _serverStarted = false;
  private Thread              _thread        = null;
  private String              _handShake     = "OK\n";
  private boolean _createBulkTransferString = true;
  private static String _bulkTransferString;
  private static final int _BULK_DATA_TRANSFER_SIZE = 1024 * 1024 * 5;
  private int _LENGTH_OF_SYSTEM_TIME_IN_MILLIS_STRING = 0;

  {
    // Because the length of the current time string will change eventually
    // calculate it on the fly :-)
    long currentTime = System.currentTimeMillis();
    String currentTimeString = Long.toString(currentTime);
    _LENGTH_OF_SYSTEM_TIME_IN_MILLIS_STRING = currentTimeString.length();
  }

  /**
   * @author Eugene Kim-Leighton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_SocketClient());
  }

  /**
   * @author Eugene Kim-Leighton
   * @author Tony Turner (added NIO support, and receiveBuffer capability)
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      //test if an assertion is made for a invalid server's port number
      try
      {
        _socketServer = new SocketServer(-1, this);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }

      //test if an assetion is made for a invalid server's processMessage interface
      try
      {
        _socketServer = new SocketServer(_port, null);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }

      // test if an assertion is made for a invalid client's IPAddress
      try
      {
        _socketClient = new SocketClient(null, _port);
        Expect.expect(false);
      }
      catch (IllegalArgumentException iae)
      {
        // Do nothing.
      }
      catch(AssertException ae)
      {
        // do nothing
      }

      // test if an assertion is made for a invalid client's port number
      try
      {
        _socketClient = new SocketClient("localhost", -1);
        Expect.expect(false);
      }
      catch (IllegalArgumentException iae)
      {
        // Do nothing.
      }
      catch(AssertException ae)
      {
        // do nothing
      }

      // start the server and client in handshake mode
      _socketServer = new SocketServer(_port, this);
      int localServerPort = _socketServer.getServerSocket().getLocalPort();
      _socketClient = new SocketClient("localhost", localServerPort);
      _thread = new Thread(this);
      _thread.start();

      _socketClient.connect();
      Assert.expect(_socketClient.isConnected());

      String message = "Snoopy says" + _CR_LF;
      List<String> list = _socketClient.sendRequestAndReceiveReplies(message, _handShake, true, true);
      Expect.expect(list.size() == 2);
      String response = list.get(0);
      Expect.expect(response.equals("Snoopy"));
      response = list.get(1);
      Expect.expect(response.equals("says"));

      message = "Hello Hello Hello Is anybody in there?"+ _LF;
      list = _socketClient.sendRequestAndReceiveReplies(message, _handShake, true, true);
      Expect.expect(list.size() == 7);

      message = "Hello Hello Hello Is anybody in there?"+ _CR_LF;
      list = _socketClient.sendRequestAndReceiveReplies(message, _handShake, true, false);
      Expect.expect(list.size() == 8);

      message = "Woodstock says"+ _CR_LF;
      list = _socketClient.sendRequestAndReceiveReplies(message, _handShake, true, false);
      Expect.expect(list.size() == 3);

      // added test of the ByteBuffer support
      message = "getImageInfo"+ _CR_LF;

      // Reid Hayhow, changed to add number of bytes we expect
      ByteBuffer receivingBuffer = _socketClient.sendRequestAndReturnRawByteBuffer(message, 16, false);
      Expect.expect(receivingBuffer.capacity() == 16);

      //Bulk Data Transfer testing
      long totalTimeElapsedForListReturnCall = 0;
      long totalTimeElapsedForRawByteBufferReturn = 0;
      int numberOfBulkDataTransfers = 0;
      for (; numberOfBulkDataTransfers < 10; numberOfBulkDataTransfers++)
      {
        message = "bulk" + _LF;
        list = _socketClient.sendRequestAndReceiveReplies(message, _handShake, true, false);

        // Get the time the data transfer finished
        long timeThisTransferFinished = System.currentTimeMillis();

        // As part of the bulk transfer we can get the time the transfer started
        long timeThisTransferStarted = new Long(list.get(1));
        // Calculate the total time
        long totalTimeForThisTransfer = timeThisTransferFinished - timeThisTransferStarted;
        totalTimeElapsedForListReturnCall += totalTimeForThisTransfer;
        Expect.expect(list.size() == 3);
        Expect.expect(list.get(0).length() == (_BULK_DATA_TRANSFER_SIZE));

        long timeBulkTransferStarted = System.currentTimeMillis();

        // The message size will be the size of the data + the length of the timestamp + the echo of the initial message
        int messageSize = _BULK_DATA_TRANSFER_SIZE + _LENGTH_OF_SYSTEM_TIME_IN_MILLIS_STRING + message.length();
        receivingBuffer = _socketClient.sendRequestAndReturnRawByteBuffer(message,
                                                                          messageSize,
                                                                          false);
        Expect.expect(receivingBuffer.capacity() == messageSize);
        long timeBulkTransferFinished = System.currentTimeMillis();
        totalTimeElapsedForRawByteBufferReturn = timeBulkTransferFinished - timeBulkTransferStarted;
      }
//      System.out.println("totalDelta for " + numberOfBulkDataTransfers + " runs = " + totalTimeElapsedForListReturnCall);
//      System.out.println("totalBulkDelta for " + numberOfBulkDataTransfers + " runs = " + totalTimeElapsedForRawByteBufferReturn);

      // Now test a premature close of the server socket by sending the command
      message = "premature"+ _CR_LF;
      // It should throw an exception
      try
      {
        list = _socketClient.sendRequestAndReceiveReplies(message, _handShake, true, false);
        Expect.expect(false);
      }
      catch(IOException ioex)
      {
        // expected failure because I disconnected the server it by sending the
        // premature command
      }

      message = "disconnect"+ _LF;

      // Now, if I try to talk to the server, the connection should throw an exception
      // because I have not reestablished the connection
      try
      {
        _socketClient.disconnectFromServer(message, _handShake, true, true);
        Expect.expect(false);
      }
      catch(IOException ioex)
      {
        // Do nothing, I expect this exception because we aren't connected
      }

      // OK, be nice and reconnect the client before sending the disconnect command
      _socketClient.connect();
      _socketClient.disconnectFromServer(message, _handShake, true, true);

      // Again, connect before continuing since we just disconnected
      _socketClient.connect();

      message = "premature"+ _CR_LF;
      // Test premature closure returning raw byte buffer as well
      try
      {
        receivingBuffer = _socketClient.sendRequestAndReturnRawByteBuffer(message, 11, false);
        Expect.expect(false);
      }
      catch(IOException ioex)
      {
        // expected failure, the server disconnected because I sent the
        // premature command
      }

      // Don't bother to test sending a command to the disconnected socket, that was
      // already done above, just reconnect and try killing it
      _socketClient.connect(500);

      try
      {
        // Send a message that will return some data but NOT the full OK\n
        message = "hang" + _CR_LF;
        list = _socketClient.sendRequestAndReceiveReplies(message, _handShake, true, false);
        Expect.expect(false);
      }
      catch(SocketTimeoutException ste)
      {
        // this is expected
      }

      try
      {
        // Send a message that will return some data but NOT the full OK\n
        message = "hang" + _CR_LF;
        receivingBuffer = _socketClient.sendRequestAndReturnRawByteBuffer(message, 6, false);
        Expect.expect(false);
      }
      catch(SocketTimeoutException ste)
      {
        // this is expected
      }

      Expect.expect(_socketClient.getTimeoutInMilliSeconds() == 500);

//      _socketClient.setTimeoutInMilliSeconds(0);

      message = "kill"+ _CR_LF;
      _socketClient.disconnectFromServer(message, _handShake, true, true);
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * @author Eugene Kim-Leighton
   */
  public List<String> processMessage(String clientMessage)
  {
    Assert.expect(clientMessage != null);

    List<String> list = new ArrayList<String>();

    if(clientMessage.equals("") == false)
    {
      StringTokenizer tokens = new StringTokenizer(clientMessage);
      String token = tokens.nextToken();

      if(token.equals("disconnect"))
      {
        list.add("server is disconnected");
        _socketServer.disconnectFromClient();
      }
      else if(token.equals("kill"))
      {
        list.add("server is shut down");
        _socketServer.killServer();
      }
      else if (token.equals("bulk"))
      {
        //Only create the bulk transfer string once because it is expensive
        if (_createBulkTransferString == true)
        {
          byte[] temporaryByteArray = new byte[_BULK_DATA_TRANSFER_SIZE];
          for (int i = 0; i < temporaryByteArray.length; i++)
          {
            //Populate the list with uppercase ASCII characters in order, ABC...XYZ
            int value = ((i % 23) + 65);
            temporaryByteArray[i] = (byte)value;
          }
          _bulkTransferString = new String(temporaryByteArray);
          _createBulkTransferString = false;
        }
        list.add(_bulkTransferString);

        //Add the time to the list before starting the transfer
        long start = System.currentTimeMillis();
        list.add(Long.toString(start));
      }
      else
      {
        tokens = new StringTokenizer(clientMessage);
        for(int i = tokens.countTokens(); i != 0; --i)
        {
          list.add(tokens.nextToken());
        }
      }
      if(token.equals("premature"))
      {
        //Premature means to send the data back without OK and disconnect
        //The client will be waiting for the OK when the disconnect happens, OUCH
        _socketServer.disconnectFromClient();
      }
      else if(token.equals("hang"))
      {
        //leave the server open, send back some data BUT don't give the OK
      }
      else
      {
        list.add("OK");
      }
    }
    return list;
  }

  /**
   * @author Eugene Kim-Leighton
   */
  public void restartServer() throws InterruptedException
  {
    _thread.join();
    _thread = new Thread(this);
    _thread.start();

    synchronized(this)
    {
      while(_serverStarted == false)
      {
        wait(200);
      }
    }
  }

  /**
   * @author Eugene Kim-Leighton
   */
  public void run()
  {
    try
    {
      _serverStarted = true;
      synchronized(this)
      {
        notifyAll();
      }
      _socketServer.acceptConnectionAndProcessMessage();
      _serverStarted = false;
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
}
