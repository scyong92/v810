package com.axi.util;

import java.awt.*;
import java.util.*;

/**
 * @author Kok Chun, Tan
 */
public class TwoOptSorterAlgorithm
{
  private static TwoOptSorterAlgorithm _instance;
  private java.util.List<Point> _points = new ArrayList<Point>();
  
  /**
   * @author Kok Chun, Tan
   */
  public TwoOptSorterAlgorithm()
  {
    //nothing
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static synchronized TwoOptSorterAlgorithm getInstance()
  {
    if (_instance == null)
      _instance = new TwoOptSorterAlgorithm();

    return _instance;
  }
  
  /**
   * Add point.
   * @author Kok Chun, Tan
   */
  private void addPoint(int x, int y)
  {
    Point locationPoint = new Point(x, y);
    _points.add(locationPoint);
  }
  
  /**
   * Add all points from a list of points.
   * @author Kok Chun, Tan
   */
  public void addPoints(java.util.List<Point> points)
  {
    _points.addAll(points);
  }
  
  /**
   * Sort an get Two-Opt location list.
   * @author Kok Chun, Tan
   */
  public java.util.List<Point> sort()
  {
    clear();
    
    int distance = 0;
    int bestDistance = getTotalDistance();
    int previousBestDistance = 0;
    
    do
    {
      previousBestDistance = bestDistance;
      for (int i = 0; i < _points.size(); i++)
      {
        for (int j = i + 1; j < _points.size(); j++)
        {
          Collections.swap(_points, i, j);
          distance = getTotalDistance();
          if (distance < bestDistance)
          {
            bestDistance = distance;
          }
          else
          {
            Collections.swap(_points, i, j);
          }
        }
      }
    }while(previousBestDistance != bestDistance);
    
    return _points;
  }
  
  /**
   * Sort an get Two-Opt location list by given a location list.
   * @author Kok Chun, Tan
   */
  public java.util.List<Point> sort(java.util.List<Point> points)
  {
    Assert.expect(points != null);
    clear();
    
    addPoints(points);
    int distance = 0;
    int bestDistance = getTotalDistance();
    int previousBestDistance = 0;
    
    do
    {
      previousBestDistance = bestDistance;
      for (int i = 0; i < _points.size(); i++)
      {
        for (int j = i + 1; j < _points.size(); j++)
        {
          Collections.swap(_points, i, j);
          distance = getTotalDistance();
          if (distance < bestDistance)
          {
            bestDistance = distance;
          }
          else
          {
            Collections.swap(_points, i, j);
          }
        }
      }
    }while(previousBestDistance != bestDistance);
    
    return _points;
  }
  
  /**
   * Get total distance of Two-Opt location list.
   * @author Kok Chun, Tan
   */
  public int getTotalDistance()
  {
    double totalDistance = 0.0;

    for (int point = 0; point < _points.size(); point++)
    {
      Point fromPoint = _points.get(point);

      Point toPoint;

      if (point + 1 < _points.size())
      {
        toPoint = _points.get(point+1);
        totalDistance += fromPoint.distance(toPoint);
      }
    }

    return (int)totalDistance;
  }
  
  /**
   * Clear location point list
   * @author Kok Chun, Tan
   */
  public void clear()
  {
    _points.clear();
  }
  
}
