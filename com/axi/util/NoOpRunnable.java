package com.axi.util;

/**
 * This class simply does a sleep for a specified time interval (default is 1 second).
 * It's effectively a no-op type operation that I thought would be a useful tool for testing threading architectures.
 *
 * @author Matt Wharton
 */
public class NoOpRunnable implements Runnable
{
  private long _timeToSleepInMillis = 1000;

  /**
   * Default constructor
   *
   * @author Matt Wharton
   */
  public NoOpRunnable()
  {
    _timeToSleepInMillis = 1000;
  }


  /**
   * Constructor
   *
   * @param milliSecondsToSleep is the number of milliseconds to sleep
   * @author Matt Wharton
   */
  public NoOpRunnable( long milliSecondsToSleep )
  {
    Assert.expect( milliSecondsToSleep >= 0, "Sleep time must be non-negative!" );

    _timeToSleepInMillis = milliSecondsToSleep;
  }


  /**
   * Does a 1 second sleep.  Echos an alert message if an InterruptedException is encountered.
   *
   * @author Matt Wharton
   */
  public void run()
  {
    try
    {
      Thread.sleep( _timeToSleepInMillis );
    }
    catch ( InterruptedException iex )
    {
      System.out.println( "NoOpRunnable (not Girl) Interrupted" );
    }
    catch (Throwable throwable)
    {
      Assert.logException(throwable);
    }
  }
}
