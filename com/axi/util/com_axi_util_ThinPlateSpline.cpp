#include <map>

/**
 * This is the source file for ThinPlateSpline JNI layer
 * @author Cheah Lee Herng
 */
#ifdef _MSC_VER
  #pragma warning(disable: 4786)
#endif

#include "JniUtil.h"
#include "util/src/ThinPlateSpline.h"
#include "util/src/sptAssert.h"

#include "com_axi_util_ThinPlateSpline.h"

using namespace std;

static map<string, ThinPlateSpline*> _regionPositionNameToInstanceMap;
static ThinPlateSpline* _pThinPlateSpline = 0;
static string const THIN_PLATE_SPLINE_HARDWARE_EXCEPTION = "com/axi/v810/hardware/NativeHardwareException";

///////////////////////////////////////////////////////////////////////////////
//
// @author Cheah Lee Herng
//////////////////////////////////////////////////////////////////////////////
static void setPointPositionNameToInstanceMap(string regionPositionName)
{
  map<string, ThinPlateSpline*>::iterator it = _regionPositionNameToInstanceMap.find(regionPositionName);
  ThinPlateSpline* pThinPlateSpline = 0;

  if (it == _regionPositionNameToInstanceMap.end())
  {
    pThinPlateSpline = new ThinPlateSpline();
    _regionPositionNameToInstanceMap.insert(make_pair(regionPositionName, pThinPlateSpline));

    pThinPlateSpline = 0;
  }
}

///////////////////////////////////////////////////////////////////////////////
//
// @author Cheah Lee Herng
//////////////////////////////////////////////////////////////////////////////
static ThinPlateSpline* getThinPlateSpline(string regionPositionName)
{
  map<string, ThinPlateSpline*>::iterator it = _regionPositionNameToInstanceMap.find(regionPositionName);
  sptAssert(it != _regionPositionNameToInstanceMap.end());

  return it->second;  
}

/*
 * Class:     com_axi_util_ThinPlateSpline_nativeReset
 * Method:    nativeReset
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_axi_util_ThinPlateSpline_nativeReset(JNIEnv* pEnv, 
                                                                     jobject object,
                                                                     jstring regionPositionName)
{
  try
  {
    setPointPositionNameToInstanceMap(getSTLString(pEnv, regionPositionName));
    
    _pThinPlateSpline = getThinPlateSpline(getSTLString(pEnv, regionPositionName));
    _pThinPlateSpline->reset();
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_util_ThinPlateSpline_nativeAddPoint
 * Method:    nativeAddPoint
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_axi_util_ThinPlateSpline_nativeAddPoint(JNIEnv* pEnv, jobject object,
                                                                        jstring regionPositionName,
                                                                        jfloat focusRegionCenterXCoordinateInNanometers, 
                                                                        jfloat focusRegionCenterYCoordinateInNanometers, 
                                                                        jfloat zHeight_FiducialRelativeNanometers)
{
  try
  {
    setPointPositionNameToInstanceMap(getSTLString(pEnv, regionPositionName));
    
    _pThinPlateSpline = getThinPlateSpline(getSTLString(pEnv, regionPositionName));    
    _pThinPlateSpline->addPoint(focusRegionCenterXCoordinateInNanometers,
                                focusRegionCenterYCoordinateInNanometers,
                                zHeight_FiducialRelativeNanometers);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_util_ThinPlateSpline_nativeUpdate
 * Method:    nativeUpdate
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_axi_util_ThinPlateSpline_nativeUpdate(JNIEnv* pEnv, 
                                                                      jobject object,
                                                                      jstring regionPositionName)
{
  try
  {
    setPointPositionNameToInstanceMap(getSTLString(pEnv, regionPositionName));
    
    _pThinPlateSpline = getThinPlateSpline(getSTLString(pEnv, regionPositionName));    
    _pThinPlateSpline->update();
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_util_ThinPlateSpline_nativeIsSufficientSurfaceModelDataPoint
 * Method:    nativeIsSufficientSurfaceModelDataPoint
 * Signature: ()V
 */ 
JNIEXPORT jboolean JNICALL Java_com_axi_util_ThinPlateSpline_nativeIsSufficientSurfaceModelDataPoint(JNIEnv* pEnv, 
                                                                                                     jobject object, 
                                                                                                     jstring regionPositionName)
{
  try
  {
    setPointPositionNameToInstanceMap(getSTLString(pEnv, regionPositionName));
    
    _pThinPlateSpline = getThinPlateSpline(getSTLString(pEnv, regionPositionName));    
    return _pThinPlateSpline->isSufficientSurfaceModelDataPoint();
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_util_ThinPlateSpline_nativeInterpolateZHeightInNanoMeters
 * Method:    nativeInterpolateZHeightInNanoMeters
 * Signature: ()V
 */
JNIEXPORT jdouble JNICALL Java_com_axi_util_ThinPlateSpline_nativeInterpolateZHeightInNanoMeters(JNIEnv* pEnv, jobject object,
                                                                                                 jstring regionPositionName, 
                                                                                                 jfloat focusRegionCenterXCoordinateInNanometers, 
                                                                                                 jfloat focusRegionCenterYCoordinateInNanometers)
{
  try
  {
    setPointPositionNameToInstanceMap(getSTLString(pEnv, regionPositionName));
    
    _pThinPlateSpline = getThinPlateSpline(getSTLString(pEnv, regionPositionName));   
    return _pThinPlateSpline->interpolateZHeightInNanoMeters(focusRegionCenterXCoordinateInNanometers, 
                                                             focusRegionCenterYCoordinateInNanometers);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_util_ThinPlateSpline_nativeGetNumberOfControlPoints
 * Method:    nativeGetNumberOfControlPoints
 * Signature: ()V
 */
JNIEXPORT jint JNICALL Java_com_axi_util_ThinPlateSpline_nativeGetNumberOfControlPoints(JNIEnv* pEnv, 
                                                                                        jobject object,
                                                                                        jstring regionPositionName)
{
  try
  {
    setPointPositionNameToInstanceMap(getSTLString(pEnv, regionPositionName));
    
    _pThinPlateSpline = getThinPlateSpline(getSTLString(pEnv, regionPositionName));
    return _pThinPlateSpline->getNumberOfControlPoints();
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}