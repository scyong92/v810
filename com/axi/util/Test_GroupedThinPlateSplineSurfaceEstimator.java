package com.axi.util;

import java.io.*;

/**
 * Unit test for GroupedThinPlateSpingeSurfaceEstimator.
 *
 * @author John Heumann
 */
public class Test_GroupedThinPlateSplineSurfaceEstimator extends UnitTest
{
  private final double _x0[] = { 0.0, 1.0, 0.0, 1.0  };
  private final double _y0[] = { 0.0, 0.0, 1.0, 1.0 };

  private final double _tolerance = 0.1;

  /**
   * Constructor.
   *
   * @author John Heumann
   */
  private Test_GroupedThinPlateSplineSurfaceEstimator()
  {
  }

  /**
   * @author John Heumann
   */
  private void testConstructorAssertsAndExceptions()
  {
    // Check constructor asserts
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        final double x[] = {1, 2};
        final double y[] = {1, 2};
        final double z[] = {1, 2};
        // minimum of 3 points are required for an ungrouped fit
        new GroupedThinPlateSplineSurfaceEstimator(x, y, z, 0);
      }
    });

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        final double x[] = {1, 2};
        final double y[] = {1, 2};
        final double z[] = {1, 2};
        final int group[] = {0, 1};
        final int side[] = {1, -1, -1, 1};
        // minimum of 3 points required for a grouped fit
        new GroupedThinPlateSplineSurfaceEstimator(x, y, z, 2, group, side, 1.0);
      }
    });

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        final double x[] = {1, 2, 3, 4, 5};
        final double y[] = {1, 2, 3, 4, 5};
        final double z[] = {1, 2, 3, 4, 5};
        final int group[] = {0, 1, 0, 1, 2};
        final int side[] = {1, -1, -1, 2, -1};
        // invalid side specified
        new GroupedThinPlateSplineSurfaceEstimator(x, y, z, 3, group, side, 1.0);
      }
    });

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        final double x[] = {1, 2, 3, 4, 5};
        final double y[] = {1, 2, 3, 4, 5};
        final double z[] = {1, 2, 3, 4, 5};
        final int group[] = {0, 1, -1, 1, 2};
        final int side[] = {1, -1, -1, 1, -1};
        // invalid group (too small) specified
        new GroupedThinPlateSplineSurfaceEstimator(x, y, z, 3, group, side, 1.0);
      }
    });

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        final double x[] = {1, 2, 3, 4, 5};
        final double y[] = {1, 2, 3, 4, 5};
        final double z[] = {1, 2, 3, 4, 5};
        final int group[] = {0, 1, 2, 1, 3};
        final int side[] = {1, -1, -1, 1, -1};
        // invalid group (too large) specified
        new GroupedThinPlateSplineSurfaceEstimator(x, y, z, 3, group, side, 1.0);
      }
    });

    Expect.expectException(new RunnableWithExceptions()
    {
      public void run()
      {
        final double x[] = {1, 1, 1};
        final double y[] = {1, 2, 3};
        final double z[] = {1, 2, 3};
        // points are colinear... matrix will be singular
        new GroupedThinPlateSplineSurfaceEstimator(x, y, z, 0.0);
      }
    }, RuntimeException.class);

  }

  /**
   * @author John Heumann
   */
  private void testSimpleUngroupedThinPlateSpline()
  {

    double x[] = new double[441];
    double y[] = new double[441];
    double z[] = new double[441];

    // Define a sinc function for use as a test case
    int k = 0;
    for (int i = -10; i <= 10; i++)
    {
      for (int j= -10; j <= 10; j++)
      {
        x[k] = i;
        y[k] = j;
        double r = Math.sqrt(i*i + j*j);
        if (r == 0)
          z[k] = 1.0;
        else
          z[k] = Math.sin(r) / r;
        k++;
      }
    }

    GroupedThinPlateSplineSurfaceEstimator tps = new GroupedThinPlateSplineSurfaceEstimator(x, y, z, 0.0);
    double err = tps.getCrossValidationError();
    Expect.expect(Math.abs(err - 0.003137) < 1e-5);
    double zHat[] = tps.getEstimatedHeights();
    err = 0.0;
    k = 0;
    for (int i = -10; i <= 10; i++)
    {
      for (int j= -10; j <= 10; j++)
      {
        double temp = Math.abs(zHat[k] - z[k++]);
        if (temp > err)
          err = temp;
      }
    }
    Expect.expect(err < 1e-6);  // should interpolate almost exactly

    // Exercise the single-point, ungrouped interface
    double temp = tps.getEstimatedHeight(0, 0);
    Expect.expect(Math.abs(temp - 1.0) < 1e-5);

    // Exercise the multiple-point, ungrouped interface
    double xv[] = {0.0, 1.0, 0.0, 1.0};
    double yv[] = {0.0, 0.0, 1.0, 1.0};
    zHat = tps.getEstimatedHeights(xv, yv);
    Expect.expect(Math.abs(zHat[0] - 1.0) < 1e-5);
    Expect.expect(Math.abs(zHat[1] - 0.841471) < 1e-5);
    Expect.expect(Math.abs(zHat[2] - 0.841471) < 1e-5);
    Expect.expect(Math.abs(zHat[3] - 0.698456) < 1e-5);

    // Exercise the single-point, grouped interface
    temp = tps.getEstimatedHeight(0, 0, 0, 1);
    Expect.expect(Math.abs(temp - 1.0) < 1e-5);

    // Exercise the multiple-point, grouped interface
    int groupv[] = {0, 0, 0, 0};
    int sidev[] = {1, 1, 1, 1};
    zHat = tps.getEstimatedHeights(xv, yv, groupv, sidev);
    Expect.expect(Math.abs(zHat[0] - 1.0) < 1e-5);
    Expect.expect(Math.abs(zHat[1] - 0.841471) < 1e-5);
    Expect.expect(Math.abs(zHat[2] - 0.841471) < 1e-5);
    Expect.expect(Math.abs(zHat[3] - 0.698456) < 1e-5);

    // Check for expected variation of cross-validation error with stiffness
    tps = new GroupedThinPlateSplineSurfaceEstimator(x, y, z, 1.0);
    err = tps.getCrossValidationError();
    Expect.expect(Math.abs(err - 0.007596) < 1e-5);

    tps = new GroupedThinPlateSplineSurfaceEstimator(x, y, z, 10.0);
    err = tps.getCrossValidationError();
    Expect.expect(Math.abs(err - 0.02957) < 1e-5);

    tps = new GroupedThinPlateSplineSurfaceEstimator(x, y, z, 100.0);
    err = tps.getCrossValidationError();
    Expect.expect(Math.abs(err - 0.06742) < 1e-5);

    // Check getOffsets
    double[] offsets = tps.getOffsets(1);
    Expect.expect(offsets.length == 1);
    Expect.expect(offsets[0] == 0.0);
  }

  /**
   * @author John Heumann
   */
  private void testGroupedThinPlateSpline()
  {

    double x[] = new double[441];
    double y[] = new double[441];
    double z[] = new double[441];
    int group[] = new int[441];
    int side[] = new int[441];
    int currentSide = 1;

    // Define a sinc function for use as a test case
    // Split the data into 2 groups, with group 0 offest +/-0.5
    // and group 1 offset +/-1
    int k = 0;
    for (int i = -10; i <= 10; i++)
    {
      for (int j= -10; j <= 10; j++)
      {
        x[k] = i;
        y[k] = j;
        double r = Math.sqrt(i*i + j*j);
        if (r == 0)
          z[k] = 1.0;
        else
          z[k] = Math.sin(r) / r;
        side[k] = currentSide;
        if (k % 2 == 1)
        {
          group[k] = 1;
          z[k] += currentSide;
          currentSide = -currentSide;
        }
        else
        {
          group[k] = 0;
          z[k] += 0.5 * currentSide;
        }
        k++;
      }
    }

    GroupedThinPlateSplineSurfaceEstimator tps =
      new GroupedThinPlateSplineSurfaceEstimator(x, y, z, 2, group, side, 0.0);
    double err = tps.getCrossValidationError();
    Expect.expect(Math.abs(err - 0.003137) < 1e-5);
    double zHat[] = tps.getEstimatedHeights();
    err = 0.0;
    k = 0;
    for (int i = -10; i <= 10; i++)
    {
      for (int j= -10; j <= 10; j++)
      {
        double temp = Math.abs(zHat[k] - z[k++]);
        if (temp > err)
          err = temp;
      }
    }
    Expect.expect(err < 1e-6);  // should interpolate almost exactly

    // Single-point, ungrouped interface should not be allowed
    final GroupedThinPlateSplineSurfaceEstimator tpsCopy = tps;
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        double temp = tpsCopy.getEstimatedHeight(0, 0);
      }
    });


    // Multiple-point, ungrouped interface should not be allowed
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        double xv[] = {0.0, 1.0, 0.0, 1.0};
        double yv[] = {0.0, 0.0, 1.0, 1.0};
        double zHat[] = tpsCopy.getEstimatedHeights(xv, yv);
      }
    });

    // Exercise the single-point, grouped interface
    double temp = tps.getEstimatedHeight(0, 0, 0, 1);
    Expect.expect(Math.abs(temp - 1.5) < 1e-3);

    // Exercise the multiple-point, grouped interface
    double xv[] = {0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0};
    double yv[] = {0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0};
    int groupv[] = {0, 0, 1, 1, 0, 0, 1, 1};
    int sidev[] = {-1, 1, -1, 1, -1, 1, -1, 1};
    zHat = tps.getEstimatedHeights(xv, yv, groupv, sidev);
    Expect.expect(Math.abs(zHat[0] - 0.5) < 1e-3);
    Expect.expect(Math.abs(zHat[1] - 1.5) < 1e-3);
    Expect.expect(Math.abs(zHat[2] - 0.0) < 1e-3);
    Expect.expect(Math.abs(zHat[3] - 2.0) < 1e-3);
    Expect.expect(Math.abs(zHat[4] - (0.698456 - 0.5)) < 1e-3);
    Expect.expect(Math.abs(zHat[5] - (0.698456 + 0.5)) < 1e-3);
    Expect.expect(Math.abs(zHat[6] - (0.698456 - 1.0)) < 1e-3);
    Expect.expect(Math.abs(zHat[7] - (0.698456 + 1.0)) < 1e-3);

    // Check the getOffsets interface
    double offsets[] = tps.getOffsets(1);
    Expect.expect(Math.abs(offsets[0]) < 1e-5);
    Expect.expect(Math.abs(offsets[1] - 0.5) < 1e-3);
  }

  /**
   * Main unit test method.
   *
   * @author John Heumann
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testConstructorAssertsAndExceptions();
    testSimpleUngroupedThinPlateSpline();
    testGroupedThinPlateSpline();
  }

  /**
   * Unit test entry point.
   *
   * @author John Heumann
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_GroupedThinPlateSplineSurfaceEstimator());
  }
}
