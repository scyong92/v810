package com.axi.util;

import java.io.*;

/**
 * @author Bill Darbie
 */
public class CouldNotReadFileException extends IOException
{
  private String _fileName;

  /**
   * @author Bill Darbie
   */
  public CouldNotReadFileException(String fileName)
  {
    super("ERROR: Could not read file or directory " + fileName);
    Assert.expect(fileName != null);
    _fileName = fileName;
  }

  /**
   * @author Bill Darbie
   */
  public String getFileName()
  {
    return _fileName;
  }
}
