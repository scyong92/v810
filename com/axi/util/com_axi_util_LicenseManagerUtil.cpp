#ifdef _MSC_VER
  #pragma warning (disable:4715)
  #pragma warning (disable:4530)
  #pragma warning (disable:4786)
#endif

#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <map>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <windows.h>

#include "util/src/AssertException.h"
#include "util/src/FileNotFoundDatastoreException.h"
#include "util/src/InvalidFileDatastoreException.h"
#include "util/src/LicenseManager.h"
#include "util/src/sptAssert.h"

#include "JNIUtil.h"
#include "com_axi_util_LicenseManagerUtil.h"

using namespace std;

// this is necessary for the Microsoft call that is needed to determine the MAC Address
// mdw & eew 9/10/02
struct AdapterStatus
{
  ADAPTER_STATUS adapter;
  NAME_BUFFER    nameBuff[30];
};

static AdapterStatus adapterStatus;

static string FILE_DOES_NOT_EXIST_EXCEPTION = "com/axi/util/FileDoesNotExistException";
static string INVALID_FILE_EXCEPTION = "com/axi/util/InvalidFileException";
static string LM_LICENSE_FILE = "LM_LICENSE_FILE=";

static map<string, LicenseManager*> licenseDirectoryToInstanceMap;
static map<LicenseManager*, int> instanceToCounterMap;

/**
 * This function converts the c++ excpetion "FileNotFoundDatastoreException" 
 * to java exception "FileDoesNotExistException" and throw it to Java code.
 * 
 * @author Eugene Kim-Leighton
 */
void throwFileDoesNotExistException(JNIEnv* pEnv, FileNotFoundDatastoreException const& fnfde)
{
  sptAssert(pEnv);

  jclass javaExceptionClass = pEnv->FindClass(FILE_DOES_NOT_EXIST_EXCEPTION.c_str());
  sptAssert(javaExceptionClass);

  string message = fnfde.getFileNameWithFullPath();    
  pEnv->ThrowNew(javaExceptionClass, message.c_str());
}
  
/**
 * This function converts the c++ excpetion "InvalidFileDatastoreException" 
 * to java exception "InvalidFileException" and throw it to Java code.
 * 
 * @author Eugene Kim-Leighton
 */  
void throwInvalidFileException(JNIEnv* pEnv, InvalidFileDatastoreException const& ifde)
{
  sptAssert(pEnv);

  jclass javaExceptionClass = pEnv->FindClass(INVALID_FILE_EXCEPTION.c_str());
  sptAssert(javaExceptionClass);

  string message = ifde.getFileNameWithFullPath();
  pEnv->ThrowNew(javaExceptionClass, message.c_str());
}

/**
 * This function creates one instance of the LicenseManager per license directory. 
 *
 * Class:     com_axi_util_LicenseManagerUtil
 * Method:    nativeCreateInstance
 * Signature: (Ljava/lang/String;)V
 *
 * @author Eugene Kim-Leigthon
 */
JNIEXPORT void JNICALL 
Java_com_axi_util_LicenseManagerUtil_nativeCreateInstance(JNIEnv* pEnv, jobject obj, jstring licenseDirectory)
{
  try
  {
    string licenseDirectoryStr = getSTLString(pEnv, licenseDirectory);
    map<string, LicenseManager*>::const_iterator licenseDirectoryToInstanceMapIt = licenseDirectoryToInstanceMap.find(licenseDirectoryStr);
    
    LicenseManager* pLicenseManager = 0;
    if(licenseDirectoryToInstanceMapIt != licenseDirectoryToInstanceMap.end())
    {
      pLicenseManager = licenseDirectoryToInstanceMapIt->second;
      
      map<LicenseManager*, int>::const_iterator instanceToCounterMapIt = instanceToCounterMap.find(pLicenseManager);
      sptAssert(instanceToCounterMapIt != instanceToCounterMap.end());
      
      int counter = instanceToCounterMapIt->second;
      counter++;
      instanceToCounterMap[pLicenseManager] = counter;  
    }
    else
    {
      pLicenseManager = LicenseManager::getInstance(licenseDirectoryStr);
      licenseDirectoryToInstanceMap.insert(make_pair(licenseDirectoryStr, pLicenseManager));
      instanceToCounterMap.insert(make_pair(pLicenseManager, 1));
    }
    
    sptAssert(pLicenseManager);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * This function deletes the LicenseManager instance.
 *
 * Class:     com_axi_util_LicenseManagerUtil
 * Method:    nativeRemoveInstance
 * Signature: (Ljava/lang/String;)V
 *
 * @author Eugene Kim-Leigthon
 */
JNIEXPORT void JNICALL 
Java_com_axi_util_LicenseManagerUtil_nativeRemoveInstance(JNIEnv* pEnv, jobject obj, jstring licenseDirectory)
{
  try
  {
    string licenseDirectoryStr = getSTLString(pEnv, licenseDirectory);
    map<string, LicenseManager*>::iterator licenseDirectoryToInstanceMapIt = licenseDirectoryToInstanceMap.find(licenseDirectoryStr);
    sptAssert(licenseDirectoryToInstanceMapIt != licenseDirectoryToInstanceMap.end());
    
    LicenseManager* pLicenseManager = licenseDirectoryToInstanceMapIt->second;
    sptAssert(pLicenseManager);

    map<LicenseManager*, int>::iterator intstanceToCounterMapIt = instanceToCounterMap.find(pLicenseManager);
    sptAssert(intstanceToCounterMapIt != instanceToCounterMap.end());
    
    int counter = intstanceToCounterMapIt->second;
    sptAssert(counter > 0);
    counter--;
    
    if(counter == 0)
    {
      LicenseManager::removeInstance(pLicenseManager);
      licenseDirectoryToInstanceMap.erase(licenseDirectoryToInstanceMapIt);
      instanceToCounterMap.erase(intstanceToCounterMapIt);  
      sptAssert(pLicenseManager == 0);
    }
    else
    {
      instanceToCounterMap[pLicenseManager] = counter;
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * This function determines whether or not the given version of feature is enabled in the system by reading the license.  
 * Please refer comments in com/axi/util/LicenseManagerUtil.java for more information.
 * 
 * Class:     com_axi_util_LicenseManagerUtil
 * Method:    nativeIsFeatureEnabled
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
 *
 * @author Eugene Kim-Leighton
 */
JNIEXPORT jboolean JNICALL 
Java_com_axi_util_LicenseManagerUtil_nativeIsFeatureEnabled__Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_String_2
  (JNIEnv* pEnv, jobject obj, jstring licenseDirectory, jstring featureName, jstring versionNumber, jstring hostIdLabel, jstring hostId)
{
  try
  {
    try
    {
      string licenseDirectoryStr = getSTLString(pEnv, licenseDirectory);
      map<string, LicenseManager*>::const_iterator it = licenseDirectoryToInstanceMap.find(licenseDirectoryStr);
      sptAssert(it != licenseDirectoryToInstanceMap.end());
      
      LicenseManager* pLicenseManager = it->second;
      sptAssert(pLicenseManager);
      
      string featureNameStr = getSTLString(pEnv, featureName);
      string versionNumberStr = getSTLString(pEnv, versionNumber);
      string hostIdLabelStr = getSTLString(pEnv, hostIdLabel);
      string hostIdStr = getSTLString(pEnv, hostId);
      
      return pLicenseManager->isFeatureEnabled(featureNameStr, versionNumberStr, hostIdLabelStr, hostIdStr);
    }
    catch(FileNotFoundDatastoreException& fnfde)
    {
      throwFileDoesNotExistException(pEnv, fnfde);
    }
    catch(InvalidFileDatastoreException& ifde)
    {
      throwInvalidFileException(pEnv, ifde);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/**
 * This function determines whether or not the given version of feature is enabled in the system by reading the license.  
 * The license must be generated with MAC address.  
 * Please refer comments in com/axi/util/LicenseManagerUtil.java for more information.
 *
 * Class:     com_axi_util_LicenseManagerUtil
 * Method:    nativeIsFeatureEnabled
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
 *
 * @author Eugene Kim-Leighton
 */
JNIEXPORT jboolean JNICALL 
Java_com_axi_util_LicenseManagerUtil_nativeIsFeatureEnabled__Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_String_2
  (JNIEnv* pEnv, jobject obj, jstring licenseDirectory, jstring featureName, jstring versionNumber)
{
  try
  {
    try
    {
      string licenseDirectoryStr = getSTLString(pEnv, licenseDirectory);
      map<string, LicenseManager*>::const_iterator it = licenseDirectoryToInstanceMap.find(licenseDirectoryStr);
      sptAssert(it != licenseDirectoryToInstanceMap.end());
      
      LicenseManager* pLicenseManager = it->second;
      sptAssert(pLicenseManager);
      
      string featureNameStr = getSTLString(pEnv, featureName);
      string versionNumberStr = getSTLString(pEnv, versionNumber);
      
      return pLicenseManager->isFeatureEnabled(featureNameStr, versionNumberStr);
    }
    catch(FileNotFoundDatastoreException& fnfde)
    {
      throwFileDoesNotExistException(pEnv, fnfde);
    }
    catch(InvalidFileDatastoreException& ifde)
    {
      throwInvalidFileException(pEnv, ifde);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/**
 * This function determines whether or not the vendor string is found in the license.  
 * The vendor string found after the key word VENDOR_STRING in the license.
 * Please refer comments in com/axi/util/LicenseManagerUtil.java for more information.
 *
 * Class:     com_axi_util_LicenseManagerUtil
 * Method:    nativeIsVendorStringAvailable
 * Signature: (Ljava/lang/String;Ljava/lang/String;)Z
 *
 * @author Eugene Kim-Leighton
 */
JNIEXPORT jboolean JNICALL 
Java_com_axi_util_LicenseManagerUtil_nativeIsVendorStringAvailable(JNIEnv* pEnv, jobject obj, jstring licenseDirectory, jstring featureName)
{
  try
  {
    string licenseDirectoryStr = getSTLString(pEnv, licenseDirectory);
    map<string, LicenseManager*>::const_iterator it = licenseDirectoryToInstanceMap.find(licenseDirectoryStr);
    sptAssert(it != licenseDirectoryToInstanceMap.end());
    
    LicenseManager* pLicenseManager = it->second;
    sptAssert(pLicenseManager);
    
    string featureNameStr = getSTLString(pEnv, featureName);
    
    return pLicenseManager->isVendorStringAvailable(featureNameStr);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/**
 * This function returns the vendor string if vendor string is found.  This function should ONLY be called  
 * if Java_com_axi_util_LicenseManager_nativeIsVendorStringAvailable() returned true, otherwise,
 * assertion will be made.
 * Please refer comments in com/axi/util/LicenseManagerUtil.java for more information.
 *
 * Class:     com_axi_util_LicenseManagerUtil
 * Method:    nativeGetVendorString
 * Signature: (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
 *
 * @author Eugene Kim-Leighton
 */
JNIEXPORT jstring JNICALL 
Java_com_axi_util_LicenseManagerUtil_nativeGetVendorString(JNIEnv* pEnv, jobject obj, jstring licenseDirectory, jstring featureName)
{
  try
  {
    string licenseDirectoryStr = getSTLString(pEnv, licenseDirectory);
    map<string, LicenseManager*>::const_iterator it = licenseDirectoryToInstanceMap.find(licenseDirectoryStr);
    sptAssert(it != licenseDirectoryToInstanceMap.end());
    
    LicenseManager* pLicenseManager = it->second;
    sptAssert(pLicenseManager);
    
    string featureNameStr = getSTLString(pEnv, featureName);
    
    string vendorStringStr = pLicenseManager->getVendorString(featureNameStr);
    sptAssert(vendorStringStr.empty() == false);
    
    jstring vendorString = pEnv->NewStringUTF(vendorStringStr.c_str());
    sptAssert(vendorString);
    
    return vendorString;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return NULL;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return NULL;
  }
}


/**
 *  This function will determine the mac address for the given network card number.
 *  @author Matt Wharton
 *  @author Erica Wheatcroft
 */
JNIEXPORT jstring JNICALL
Java_com_axi_util_LicenseManagerUtil_nativeGetHostId( JNIEnv* pEnv, jobject obj )
{
  try
  {
    NCB ncb = {0};
    int const NUMBER_OF_MAC_ADDRESS_BYTES = 6;
    
    string macAddress = "000000000000";
    jstring hostId = pEnv->NewStringUTF( macAddress.c_str() );
    
    // Enumerate the available ethernet adapters.
    LANA_ENUM adapterList = {0};
    memset( &ncb, 0, sizeof(NCB) );
    ncb.ncb_command = NCBENUM;
    ncb.ncb_buffer = reinterpret_cast<unsigned char*>(&adapterList);
    ncb.ncb_length = sizeof(adapterList);
    unsigned char retCode = Netbios( &ncb );
    
    if ( (retCode != NRC_GOODRET) || (adapterList.length <= 0) )
    {
      return hostId;
    }
    
    // Get the lana id of the first enumerated interface.
    long networkCardNumber = adapterList.lana[0];
    
    // Reset the Netbios interface.
    memset( &ncb, 0, sizeof(NCB) );
    ncb.ncb_command = NCBRESET;
    ncb.ncb_lana_num = static_cast<unsigned char>(networkCardNumber);
    retCode = Netbios( &ncb );
    
    if ( retCode != NRC_GOODRET )
    {
      return hostId;
    }
    
    // Obtain information (including MAC address) for the interface.
    memset( &ncb, 0, sizeof(ncb) );
    ncb.ncb_command = NCBASTAT;
    ncb.ncb_lana_num = static_cast<unsigned char>(networkCardNumber);
    strcpy( reinterpret_cast<char*>(ncb.ncb_callname), "*               " );
    ncb.ncb_buffer = reinterpret_cast<unsigned char*>(&adapterStatus);
    ncb.ncb_length = sizeof(adapterStatus);
    retCode = Netbios( &ncb );
    
    ostringstream macAddressBuffer;

    if ( retCode == NRC_GOODRET )
    {
      // Extract the MAC address.
      for ( int i = 0; i < NUMBER_OF_MAC_ADDRESS_BYTES; ++i )
      {
        short macByte = adapterStatus.adapter.adapter_address[i];
        macAddressBuffer << setw(2) << setfill('0') << hex << macByte;
      }
      macAddress = macAddressBuffer.str();
    transform( macAddress.begin(), macAddress.end(), macAddress.begin(), toupper );
    }
    else
    {
      return hostId;
    }
    
    hostId = pEnv->NewStringUTF( macAddress.c_str() );
    return hostId;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return NULL;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return NULL;
  }
}

/**
 * Method to purge the env variable for LM_LICENSE_FILE, if one exists.
 * The code knows where the file is located and the env variable can only
 * confuse it, thus this method to set it to null. This does NOT change
 * the Windows base env setting, but since it is run each time the software
 * starts up, that is OK.
 *
 * @author Reid Hayhow
 */
JNIEXPORT void JNICALL 
Java_com_axi_util_LicenseManagerUtil_nativePurgeEnv(JNIEnv* envPtr, jclass myClass)
{
  try
  {
    // Set the env to null out the LM_LICENSE_FILE value so that flexLM
    // only looks at the path that we give to it
    _putenv(LM_LICENSE_FILE.c_str());
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}
