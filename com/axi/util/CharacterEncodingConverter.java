package com.axi.util;

import java.io.*;
import java.nio.charset.*;

/**
 * Convert the inFile from one character encoding to another.  For example
 * from UTF16 to ascii with escaped unicode characters.
 *
 * @author Bill Darbie
 */
public class CharacterEncodingConverter
{
  /**
   * @author Bill Darbie
   */
  public CharacterEncodingConverter()
  {
    // do nothing
  }

  /**
   * Convert the inFile from one character encoding to another.  For example
   * from UTF16 to ascii with escaped unicode characters
   *
   * inputFileName will be read in using the decoding specified by decodingStr, then it will be written out
   * to outputFileName with the encoding defined by encodingStr
   *
   * Every Java implementation should support these encoding/decodings:
   * US-ASCII Seven-bit ASCII, a.k.a. ISO646-US, a.k.a. the Basic Latin block of the Unicode character set
   * ISO-8859-1 ISO Latin Alphabet No. 1, a.k.a. ISO-LATIN-1
   * UTF-8 Eight-bit UCS Transformation Format
   * UTF-16BE Sixteen-bit UCS Transformation Format, big-endian byte order
   * UTF-16LE Sixteen-bit UCS Transformation Format, little-endian byte order
   * UTF-16 Sixteen-bit UCS Transformation Format, byte order identified by an optional byte-order mark
   *
   * @author Bill Darbie
   */
  public void convert(String inputFileName,
                      String outputFileName,
                      String decodingStr,
                      String encodingStr) throws CharacterEncodingNotSupportedException,
                                                 CouldNotReadFileException,
                                                 CouldNotCreateFileException,
                                                 CouldNotRenameFileException,
                                                 CouldNotDeleteFileException,
                                                  FileNotFoundException

  {
    Assert.expect(inputFileName != null);
    Assert.expect(outputFileName != null);
    Assert.expect(decodingStr != null);
    Assert.expect(encodingStr != null);

    if (Charset.isSupported(decodingStr) == false)
      throw new CharacterEncodingNotSupportedException(decodingStr);
    if (Charset.isSupported(encodingStr) == false)
      throw new CharacterEncodingNotSupportedException(encodingStr);

    Charset inCharset = Charset.forName(decodingStr);
    Charset outCharset = Charset.forName(encodingStr);

    CharsetEncoder encoder = outCharset.newEncoder();
    CharsetDecoder decoder = inCharset.newDecoder();


    // open the file for reading
    FileInputStream fis = null;
    try
    {
      fis = new FileInputStream(inputFileName);
    }
    catch (FileNotFoundException ex)
    {
      CouldNotReadFileException cnrfe = new CouldNotReadFileException(inputFileName);
      cnrfe.initCause(ex);
      throw cnrfe;
    }
    InputStreamReader isr = new InputStreamReader(fis, decoder); // looks good for reading in
    BufferedReader is = new BufferedReader(isr);

    // open a temporary file for writing
    String tempOutputFileName = FileUtil.getTempFileName(outputFileName);
    FileOutputStream fos = null;
    try
    {
      fos = new FileOutputStream(tempOutputFileName);
    }
    catch (FileNotFoundException ex)
    {
      CouldNotCreateFileException cncre = new CouldNotCreateFileException(tempOutputFileName);
      cncre.initCause(ex);
      throw cncre;
    }
    OutputStreamWriter osw = new OutputStreamWriter(fos);
    BufferedWriter bw = new BufferedWriter(osw);
    PrintWriter os = new PrintWriter(bw);

    boolean exception = false;
    try
    {
      int size = 2048;
      char[] charArray = new char[size];
      int numChars = 0;
      while (numChars > -1)
      {
        numChars = is.read(charArray, 0, size);
        for (int i = 0; i < numChars; ++i)
        {
          char ch = charArray[i];
          if (encoder.canEncode(ch))
            os.write(ch);
          else
          {
            // we have a character that is not an character supported by the encode, so write out the unicode hex equivalent
            // this is due to some special character
            // @author Wei Chin, Chong fix for CR32410
            StringBuffer outBuffer = new StringBuffer(size);
            if ((ch < 0x0020) || (ch > 0x007e))
            {
              outBuffer.append('\\');
              outBuffer.append('u');
              outBuffer.append(MathUtil.baseTenIntegerToHex((ch >> 12) & 0xF));
              outBuffer.append(MathUtil.baseTenIntegerToHex((ch >> 8) & 0xF));
              outBuffer.append(MathUtil.baseTenIntegerToHex((ch >> 4) & 0xF));
              outBuffer.append(MathUtil.baseTenIntegerToHex(ch & 0xF));
            }
            os.write(outBuffer.toString());
          }
        }
      }
    }
    catch (IOException ioe)
    {
      exception = true;
      CouldNotReadFileException cnrfe = new CouldNotReadFileException(inputFileName);
      cnrfe.initCause(ioe);
      throw cnrfe;
    }
    finally
    {
      try
      {
        if (is != null)
          is.close();
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      if (os != null)
        os.close();

      if (exception)
      {
        try
        {
          if (FileUtil.exists(tempOutputFileName))
            FileUtil.delete(tempOutputFileName);
        }
        catch (CouldNotDeleteFileException ex)
        {
          ex.printStackTrace();
        }
      }
    }
    // now move the temporary file over to the original file
    FileUtil.rename(tempOutputFileName, outputFileName);
  }
}
