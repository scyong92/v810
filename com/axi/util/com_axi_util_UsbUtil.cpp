/**
 * This is the source file for UsbUtil JNI layer
 * @author George A. David
 */
#ifdef _MSC_VER
  #pragma warning(disable: 4786)
#endif

#include <list>
#include <string>
#include <iostream>

#include "JniUtil.h"
#include "util/src/CommunicationHardwareException.h"
#include "util/src/UsbUtil.h"
#include "util/src/FtdiUsbUtil.h"
#include "util/src/sptAssert.h"

#include "com_axi_util_UsbUtil.h"

using namespace std;

static UsbUtil* pUsbUtil = NULL;

/**
 * @author George A. David
 */
UsbUtil::UsbDriverTypeEnum getUsbTypeEnum(jint usbDriverType)
{
  UsbUtil::UsbDriverTypeEnum usbDriverTypeEnum;
  if(usbDriverType == UsbUtil::FTDI)
  {
    usbDriverTypeEnum = UsbUtil::FTDI;
  }
  else
  {
    // this should not happen
    sptAssert(false);
  }

  return usbDriverTypeEnum;
}

/**
 * @author George A. David
 */
void throwCommunicationException(JNIEnv* pEnv, CommunicationHardwareException& che)
{
  sptAssert(pEnv);
  
  // create a message java string
  jstring message = pEnv->NewStringUTF(che.getMessage().c_str());
  sptAssert(message);

  // create a key java string
  jstring key = pEnv->NewStringUTF(che.getKey().c_str());
  sptAssert(key);
  
  // create an array list from the list of paramameters.
  jobject arrayList = createJavaUtilArrayList(pEnv, che.getParameters());
  sptAssert(arrayList);

  // find the CommunicationException class
  jclass exceptionClass = pEnv->FindClass("com/axi/util/CommunicationException");
  sptAssert(exceptionClass);
 
  // find the constructor
  jmethodID constructorId = pEnv->GetMethodID(exceptionClass, "<init>", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V");
  sptAssert(constructorId);
  
  // create the exception.
  jobject exception = pEnv->NewObject(exceptionClass, constructorId, message, key, arrayList);
  sptAssert(exception);
  
  // throw the exception.
  pEnv->Throw((jthrowable)exception);
}

/**
 * @author George A. David
 */
JNIEXPORT void JNICALL
Java_com_axi_util_UsbUtil_nativeDestructor(JNIEnv* pEnv, jobject object)
{
  try
  {
    sptAssert(pEnv);
    //  cout << "#### nativeDestructor ###" << endl;
    
    //  delete pUsbUtil;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * @author George A. David
 */
JNIEXPORT void JNICALL
Java_com_axi_util_UsbUtil_nativeConnect(JNIEnv* pEnv, jobject usbUtilObject, jint usbType, jstring deviceDescription)
{
  try
  {
    sptAssert(pEnv);
    //  cout << "#### nativeConnect ###" << endl;
    
    UsbUtil* pUsbUtil = UsbUtil::getInstance(getUsbTypeEnum(usbType));
    
    try
    {
      pUsbUtil->connect(getSTLString(pEnv, deviceDescription));
    }
    catch(CommunicationHardwareException& che)
    {
      //    string key = che.getKey();
      //    list<string> parameters = che.getParameters();
      //    cout << che.getMessage() << endl;
      //    cout << "Key: " + key << endl;
      throwCommunicationException(pEnv, che);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * @author George A. David
 */
JNIEXPORT void JNICALL
Java_com_axi_util_UsbUtil_nativeDisconnect(JNIEnv* pEnv, jobject object, jint usbType, jstring deviceDescription)
{
  try
  {
    sptAssert(pEnv);
    //  cout << "#### nativeDisconnect ###" << endl;
    
    UsbUtil* pUsbUtil = UsbUtil::getInstance(getUsbTypeEnum(usbType));
    
    try
    {
      pUsbUtil->disconnect(getSTLString(pEnv, deviceDescription));
  }
    catch(CommunicationHardwareException& che)
    {
      //    string key = che.getKey();
      //    list<string> parameters = che.getParameters();
      //    cout << che.getMessage() << endl;
      //    cout << "Key: " + key << endl;
      throwCommunicationException(pEnv, che);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  } 
}

/**
 * @author George A. David
 */
JNIEXPORT void JNICALL
Java_com_axi_util_UsbUtil_nativeSendMessage(JNIEnv* pEnv, jobject object, jint usbType, jstring deviceDescription, jstring message)
{
  try
  {
    sptAssert(pEnv);
    //  cout << "#### nativeSendMessage ###" << endl;
    UsbUtil* pUsbUtil = UsbUtil::getInstance(getUsbTypeEnum(usbType));
    
    try
    {
      pUsbUtil->sendMessage(getSTLString(pEnv, deviceDescription), getSTLString(pEnv, message));
    }
    catch(CommunicationHardwareException& che)
    {
      //    string key = che.getKey();
      //    list<string> parameters = che.getParameters();
      //    cout << che.getMessage() << endl;
      //    cout << "Key: " + key << endl;
      throwCommunicationException(pEnv, che);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  } 
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * @author George A. David
 */
JNIEXPORT jobject JNICALL
Java_com_axi_util_UsbUtil_nativeGetReplies(JNIEnv* pEnv, jobject object, jint usbType, jstring deviceDescription)
{
  try
  {
    sptAssert(pEnv);
    //  cout << "#### nativeGetReplies ###" << endl;
    
    UsbUtil* pUsbUtil = UsbUtil::getInstance(getUsbTypeEnum(usbType));

    jobject replies = 0;
    try
    {
      replies = createJavaUtilArrayList(pEnv, pUsbUtil->getReplies(getSTLString(pEnv, deviceDescription)));
      cout << "After createJavaUtilArrayList()" << endl;
    }
    catch(CommunicationHardwareException& che)
    {
      //    string key = che.getKey();
      //    list<string> parameters = che.getParameters();
      //    cout << che.getMessage() << endl;
      //    cout << "Key: " + key << endl; 
      throwCommunicationException(pEnv, che);
    }

    return replies;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * @author George A. David
 */
JNIEXPORT jobject JNICALL
Java_com_axi_util_UsbUtil_nativeSendMessageAndGetReplies(JNIEnv* pEnv, jobject object, jint usbType, jstring deviceDescription, jstring message)
{
  try
  {
    sptAssert(pEnv);
    //  cout << "#### nativeSendMessageAndGetReplies ###" << endl;
    
    UsbUtil* pUsbUtil = UsbUtil::getInstance(getUsbTypeEnum(usbType));
    
    jobject replies = 0;
    try
    {
      replies = createJavaUtilArrayList(pEnv, pUsbUtil->sendMessageAndGetReplies(getSTLString(pEnv, deviceDescription), getSTLString(pEnv, message)));
    }
    catch(CommunicationHardwareException& che)
    {
      //    string key = che.getKey();
      //    list<string> parameters = che.getParameters();
      //    cout << che.getMessage() << endl;
      //    cout << "Key: " + key << endl;
      throwCommunicationException(pEnv, che);
    }
    
    return replies;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

