package com.axi.util;

import java.io.*;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;


/**
 * The FileLogger class will create a log file and append log strings
 * to it.  Each log string is preceded by a header containing the current
 * system time.
 *
 * Users specify the full path name for the log file (including the
 * file extension).  The maximum size the log file will be allowed to
 * reach can optionally be specified.  If this size is going to be surpassed
 * during an append, the FileLogger will instead delete lines from the beginning
 * of the file (i.e. the oldest lines first) to maintain maximum size.  The
 * deletion of lines is not smart, meaning when it is done the first line won't
 * necessarily be a header line or if log strings contain multiple lines the
 * first line might be in the middle of a log string.
 *
 * NOTE for the AXI do NOT use this class,
 * use com.axi.xRayTest.util.FileLoggerAxi instead.  It is a wrapper for
 * this class that converts all the exceptions thrown here into DatastoreExceptions
 * If you add a method to this class MAKE SURE TO ADD IT to FileLoggerAxi also.
 *
 * @author John Dutton
 */
public class FileLogger
{
  private String _logFileFullPathName;
  private int _maxSizeOfFile = Integer.MAX_VALUE;
  private boolean _alwaysGrowLogFile = false;
  private FileWriterUtil _fileWriterUtil;
  private static final SimpleDateFormat _headerSimpleDateFormat =
      new SimpleDateFormat("MM/dd/yyyy h:mm:ss a", new Locale("en", "US"));

  /**
   * Creates logger than will grow indefinitely.
   *
   * @author John Dutton
   */
  public FileLogger(String logFileFullPathName)
  {
    Assert.expect(logFileFullPathName != null);

    _logFileFullPathName = logFileFullPathName;
    _fileWriterUtil = new FileWriterUtil(logFileFullPathName, true);
    _alwaysGrowLogFile = true;
  }

  /**
   * @param maxSizeOfFile specifies the largest size in bytes the log file
   * should ever reach.  Set a reasonable limit since maintaining this size requires
   * a file copy for each append after the log file is "full".
   *
   * @author John Dutton
   */
  public FileLogger(String logFileFullPathName, int maxSizeOfFile)
  {
    Assert.expect(logFileFullPathName != null);
    Assert.expect(maxSizeOfFile > 0);

    _logFileFullPathName = logFileFullPathName;
    _fileWriterUtil = new FileWriterUtil(logFileFullPathName, true);
    _maxSizeOfFile = maxSizeOfFile;
  }

  /**
   * Appends log string to log file and truncates the log file if
   * its size has grown greater than maximum size allowed.
   *
   * @author John Dutton
   */
  public void append(String logString) throws CouldNotCreateFileException, FileNotFoundException,
       CouldNotDeleteFileException, CouldNotRenameFileException, CouldNotReadFileException,
       CouldNotCloseFileException, FileFoundWhereDirectoryWasExpectedException
  {
    Assert.expect(logString != null);

    // Avoid exception that would be caused if _logFilesDirectory doesn't already exist.
    String logFileDirectory = FileUtil.getParent(_logFileFullPathName);
    if (!FileUtil.existsDirectory(logFileDirectory))
      FileUtil.mkdirs(logFileDirectory);

    _fileWriterUtil.append(createNewLogStringHeader() + logString);
    truncateLogFileIfNeeded();
  }
  
  /**
   * Appends log string to log file and truncates the log file if
   * its size has grown greater than maximum size allowed.
   *
   * @author Swee Yee Wong
   */
  public void appendWithoutDateTime(String logString) throws CouldNotCreateFileException, FileNotFoundException,
       CouldNotDeleteFileException, CouldNotRenameFileException, CouldNotReadFileException,
       CouldNotCloseFileException, FileFoundWhereDirectoryWasExpectedException
  {
    Assert.expect(logString != null);

    // Avoid exception that would be caused if _logFilesDirectory doesn't already exist.
    String logFileDirectory = FileUtil.getParent(_logFileFullPathName);
    if (!FileUtil.existsDirectory(logFileDirectory))
      FileUtil.mkdirs(logFileDirectory);

    _fileWriterUtil.append(logString);
    truncateLogFileIfNeeded();
  }

  /**
   * Routine truncates log file by copying most recent log records to a
   * temporary file and then renaming it to be the real log file.
   *
   * @author John Dutton
   */
  private void truncateLogFileIfNeeded() throws CouldNotCreateFileException, FileNotFoundException,
       CouldNotDeleteFileException, CouldNotRenameFileException, CouldNotReadFileException,
       CouldNotCloseFileException
  {
    if (_alwaysGrowLogFile)
      return;
    long logFileSize = FileUtil.getFileSizeInBytes(_logFileFullPathName);
    if (logFileSize > _maxSizeOfFile)
    {
      // Open a unique temp file for output
      String tempFileName = FileUtil.getTempFileName(_logFileFullPathName);
      FileWriterUtil os = new FileWriterUtil(tempFileName, false);
      os.open();

      // Open log file as line reader for input
      FileReader fileReader = new FileReader(_logFileFullPathName);
      LineNumberReader is = new LineNumberReader(fileReader);

      try // paired with catch to handle exceptions from inner try
      {
        boolean exceptionInInnerTry = true;
        try // paired with finally to make sure streams are closed
        {
          // Set file position bytesToDiscard from file's beginning.  These bytes
          // will be discarded since they are the oldest log lines.
          long bytesToDiscard = logFileSize - _maxSizeOfFile;
          is.skip(bytesToDiscard);

          // The call to skip could leave input stream positioned in the middle of
          // a line, so read a line to get on line boundary.  This avoids having the
          // new log file begin with a partial line.
          is.readLine();

          // Copy remaining lines into temp file.
          String copyString = is.readLine();
          while (copyString != null)
          {
            os.writeln(copyString);
            copyString = is.readLine();
          }
          exceptionInInnerTry = false;
        }
        finally
        {
          // Seems odd, but FileWriterUtil doesn't throw an exception on close.
          if (os != null)
            os.close();
          try
          {
            if (is != null)
              is.close();
          }
          catch (IOException ioe)
          {
            if (!exceptionInInnerTry)
            {
              CouldNotCloseFileException cncfe =
                  new CouldNotCloseFileException(_logFileFullPathName);
              cncfe.initCause(ioe);
              throw cncfe;
            }
            else
            {
              // Two exceptions occurred.  Don't throw this one. However, log
              // stack trace to have some record of IOException.
              ioe.printStackTrace();
            }
          }
        }
      }
      catch (IOException ioex)
      {
        if (FileUtil.exists(tempFileName))
        {
          try
          {
            FileUtil.delete(tempFileName);
          }
          catch (CouldNotDeleteFileException cndfe)
          {
            // Don't throw this one.  However, log stack trace to have some
            // record of CouldNotDeleteFileException.
            cndfe.printStackTrace();
          }
        }
        CouldNotReadFileException ex = new CouldNotReadFileException(_logFileFullPathName);
        ex.initCause(ioex);
        throw ex;
      }

      // Rename temp file so it becomes the new log file.  FileUtilAxi.rename
      // will overwrite the original log file.
      try
      {
        FileUtil.rename(tempFileName, _logFileFullPathName);
      }
      catch (CouldNotDeleteFileException cndfe)
      {
        // Couldn't delete log file, so rename didn't happend.  Cleanup by
        // deleting temp file.
        FileUtil.delete(tempFileName);
      }
    }
  }

  /**
   * Creates a timestamped header for a log string.  Header will be on its own line.
   *
   * @author John Dutton
   */
  private String createNewLogStringHeader()
  {
    Date currentDate = new Date();
    return _headerSimpleDateFormat.format(currentDate) + ", ";
  }
}
