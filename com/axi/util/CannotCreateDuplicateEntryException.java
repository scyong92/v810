package com.axi.util;

/**
 * This exception gets thrown when attempting
 * to add a duplicate entry to a list, hash map,
 * array, etc...
 * @author George A. David
 */
public class CannotCreateDuplicateEntryException extends Exception
{
  private String _duplicateEntry;

  /**
   * @author George A. David
   */
  public CannotCreateDuplicateEntryException(String duplicateEntry)
  {
    super("Attempted to add a duplicate " + duplicateEntry + " to a Java collection class");
    Assert.expect(duplicateEntry != null);
    Assert.expect(duplicateEntry.length() > 0);
    _duplicateEntry = duplicateEntry;
  }

  /**
   * @author Bill Darbie
   */
  public String getDuplicateEntry()
  {
    return _duplicateEntry;
  }
}

