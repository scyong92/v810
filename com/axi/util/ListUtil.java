package com.axi.util;

import java.util.*;


/**
 * @author George A. David
 */
public class ListUtil
{
  /**
   * @author George A. David
   */
  private ListUtil()
  {
    // a static class that should not be instantiated
  }

  /**
   * @author George A. David
   */
  public static void fuzzyAddIfNotPresent(List<Integer> integers, int value, int differenceAllowedToBeConsideredEqual)
  {
    for(int integer : integers)
    {
      if(MathUtil.fuzzyEquals(integer, value, differenceAllowedToBeConsideredEqual))
        return;
    }

    integers.add(value);
  }

  /**
   * @author George A. David
   */
  public static boolean fuzzyContains(List<Integer> integers, int value, int differenceAllowedToBeConsideredEqual)
  {
    Assert.expect(integers != null);

    for(int i : integers)
    {
      if(MathUtil.fuzzyEquals(i, value, differenceAllowedToBeConsideredEqual))
        return true;
    }

    return false;
  }

  /**
   * @author George A. David
   */
  public static int findIndexOfValueGreaterThanOrEqualTo(List<Double> doubles, double value)
  {
    int index = Collections.binarySearch(doubles, value);
    if (index < 0)
    {
      // ok, the value was not found, so per the binarySearch spec, the value returned
      //  " (-(insertion point) - 1). The insertion point is defined as the point at
      //  which the key would be inserted into the list: the index of the first
      // element greater than the key"
      index = -(index + 1); // first element larger
    }

    return index;
  }

  /**
   * @author George A. David
   */
  public static int findIndexOfValueGreaterThanOrEqualTo(List<Integer> integers, int value)
  {
    int index = Collections.binarySearch(integers, value);
    if (index < 0)
    {
      // ok, the value was not found, so per the binarySearch spec, the value returned
      //  " (-(insertion point) - 1). The insertion point is defined as the point at
      //  which the key would be inserted into the list: the index of the first
      // element greater than the key"
      index = -(index + 1); // first element larger
    }

    return index;
  }

  /**
   * @author George A. David
   */
  public static int findIndexOfValueLessThanOrEqualTo(List<Double> doubles, double value)
  {
    int index = Collections.binarySearch(doubles, value);
    if (index < 0)
    {
      // ok, the value was not found, so per the binarySearch spec, the value returned
      //  " (-(insertion point) - 1). The insertion point is defined as the point at
      //  which the key would be inserted into the list: the index of the first
      // element greater than the key"
      index = -(index + 1) - 1; // last element smaller
    }

    return index;
  }

  /**
   * @author George A. David
   */
  public static int findIndexOfValueLessThanOrEqualTo(List<Integer> integers, int value)
  {
    int index = Collections.binarySearch(integers, value);
    if (index < 0)
    {
      // ok, the value was not found, so per the binarySearch spec, the value returned
      //  " (-(insertion point) - 1). The insertion point is defined as the point at
      //  which the key would be inserted into the list: the index of the first
      // element greater than the key"
      index = -(index + 1) - 1; // last element smaller
    }

    return index;
  }

  /**
   * based on the binarySearchResult, it determines the previous and next
   * indicies.
   * @author George A. David
   */
  private static Pair<Integer, Integer> findSurroundingIndicies(int binarySearchResult, int maxIndex)
  {
    int previousIndex = -1;
    int nextIndex = -1;
    if (binarySearchResult < 0)
    {
      // ok, the value was not found, so per the binarySearch spec, the value returned
      //  " (-(insertion point) - 1). The insertion point is defined as the point at
      //  which the key would be inserted into the list: the index of the first
      // element greater than the key"
      nextIndex = -(binarySearchResult + 1);
      previousIndex = nextIndex - 1;
    }
    else
    {
      previousIndex = binarySearchResult - 1;
      nextIndex = binarySearchResult + 1;
    }

    // make sure we have valid indicies.
    if(previousIndex < 0)
      previousIndex = 0;

    if(nextIndex > maxIndex)
      nextIndex = maxIndex;

    Pair<Integer, Integer> pair = new Pair<Integer, Integer>(previousIndex, nextIndex);
    return pair;
  }

  /**
   * finds the indicies of the value less than and the value greater than.
   * The return value pair with the first value the index of the value less than,
   * and the second value the index of the value greater than.
   * @author George A. David
   */
  public static Pair<Integer, Integer> findIndiciesOfSurroundingValues(List<Integer> integers, int value)
  {
    int index = Collections.binarySearch(integers, value);
    return ListUtil.findSurroundingIndicies(index, integers.size() - 1);
  }

  /**
   * finds the indicies of the value less than and the value greater than.
   * The return value pair with the first value the index of the value less than,
   * and the second value the index of the value greater than.
   * @author George A. David
   */
  public static Pair<Integer, Integer> findIndiciesOfSurroundingValues(List<Double> doubles, double value)
  {
    Assert.expect(doubles != null);

    int index = Collections.binarySearch(doubles, value);
    return ListUtil.findSurroundingIndicies(index, doubles.size() - 1);
  }
}
