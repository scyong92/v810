package com.axi.util;



/**
 * Utility for Internet Control Message Protocol packets.
 * @author Rex Shang
 */
public class ICMPPacketUtil
{
  private static ICMPPacketUtil _instance = null;
  private static final boolean _DO_NOT_FRAGMENT = false;

  /**
   * @author Rex Shang
   */
  private ICMPPacketUtil()
  {
  }

  /**
   * @author Rex Shang
   */
  public static synchronized ICMPPacketUtil getInstance()
  {
    if (_instance == null)
      _instance = new ICMPPacketUtil();

    return _instance;
  }

  /**
   * Get the Maximum Transmission Unit for the local host.
   * A remote ip address is needed to determine how big of data we can send.
   * @author Rex Shang
   */
  public int getMTUSize(String localIpAddress, String remoteIpAddress)
  {
    int localMTUSize = nativeGetMTUSize(localIpAddress, remoteIpAddress);
    return localMTUSize;
  }

  /**
   * Get the Maximum Transmission Unit for the local host.
   * NOTE:  This method will use the default local ip address that can connect to ipAddress.
   * @author Rex Shang
   */
  public int getMTUSize(String ipAddress)
  {
    return getMTUSize(null, ipAddress);
  }

  /**
   * @return boolean retuns whether we can ping destination with the packetSize without fragment.
   * @author Rex Shang
   */
  public boolean canHostTransmitPacketSize(String ipAddress, int packetSize)
  {
    boolean status = nativeCanHostTransmitPacketSize(ipAddress, packetSize, _DO_NOT_FRAGMENT);
    return status;
  }

 /**
  * @return boolean retuns whether we can ping destination with the packetSize without fragment.
  * @author Rex Shang
  */
  public boolean canHostTransmitPacketSizeWithFragment(String ipAddress, int packetSize)
  {
    boolean status = nativeCanHostTransmitPacketSize(ipAddress, packetSize, true);
    return status;
  }

  /**
   * Test harness.
   * @author Rex Shang
   */
  public static void main(String[] argv)
  {
    System.loadLibrary("nativeAxiUtil");
    ICMPPacketUtil me = ICMPPacketUtil.getInstance();

    String remote = "156.140.77.81";

    int packetSize = me.getMTUSize(remote);
    System.out.println("packet size is: " + packetSize);

    int remoteMTU = 1501;
    boolean isOk = false;
    while (isOk == false && remoteMTU >= 0)
    {
      isOk = me.canHostTransmitPacketSize(remote, remoteMTU);
      if (isOk == false)
        remoteMTU--;
    }
    System.out.println(remote + " " + remoteMTU + " " + isOk);
  }

  /**
   * @author Rex Shang
   */
  private native int nativeGetMTUSize(String localIpAddress, String remoteIpAddress);

  /**
   * @author Rex Shang
   */
  private native boolean nativeCanHostTransmitPacketSize(String ipAddress, int packetSize, boolean allowFragment);

}
