package com.axi.util;

import java.io.*;

/**
 * Throw this class instead of java.io.FileNotFoundException because this one
 * provides a getFileName() method.
 *
 * @author Bill Darbie
 */
public class FileDoesNotExistException extends IOException
{
  private String _fileName;

  /**
   * @author Bill Darbie
   */
  public FileDoesNotExistException(String fileName)
  {
    super("The file or directory " + fileName + " does not exist.");
    Assert.expect(fileName != null);

    _fileName = fileName;
  }

  /**
   * @author Bill Darbie
   */
  public String getFileName()
  {
    return _fileName;
  }
}
