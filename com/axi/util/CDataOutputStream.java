package com.axi.util;

import java.io.*;

import com.axi.util.*;

/**
 * This class allows you to write a binary file from Java that can be read in from
 * 32 bit C or C++.
 *
 * PE:  You should probably use java.nio.ByteBuffers instead . . .
 *
 * @see LittleEndianCDataOutputStream
 * @see BigEndianCDataOutputStream *
 * @author Bill Darbie
 */
abstract public class CDataOutputStream extends FilterOutputStream implements DataOutput
{
  protected OutputStream _os = null;

  /**
   * @author Bill Darbie
   */
  private CDataOutputStream()
  {
    super(null);
    // do nothing
  }

  /**
   * Create a CDataOutputStream the writes out 32 bit C or C++ files in either little endian
   * or big endian format.
   *
   * @author Bill Darbie
   */
  public CDataOutputStream(OutputStream os)
  {
    super(os);

    Assert.expect(os != null);

    _os = os;
  }


  /**
   * Writes to the output stream the eight
   * low-order bits of the argument <code>aByte</code>.
   * The 24 high-order  bits of <code>aByte</code>
   * are ignored.
   *
   * @param      aByte   the byte to be written.
   * @exception  IOException  if an I/O error occurs.
   * @author Bill Darbie
   */
  public void write(int aByte) throws IOException
  {
    _os.write(aByte);
  }

  /**
   * Writes to the output stream all the bytes in array <code>b</code>.
   * If <code>byteArray</code> is <code>null</code>,
   * a <code>NullPointerException</code> is thrown.
   * If <code>byteArray.length</code> is zero, then
   * no bytes are written. Otherwise, the byte
   * <code>byteArray[0]</code> is written first, then
   * <code>byteArray[1]</code>, and so on; the last byte
   * written is <code>b[btyeArray.length-1]</code>.
   *
   * @param      byteArray   the data.
   * @exception  IOException  if an I/O error occurs.
   * @author Bill Darbie
   */
  public void write(byte byteArray[]) throws IOException
  {
    Assert.expect(byteArray != null);

    _os.write(byteArray);
  }

  /**
   * Writes <code>length</code> bytes from array
   * <code>byteArray</code>, in order,  to
   * the output stream.  If <code>length</code> is zero,
   * then no bytes are written. Otherwise, the
   * byte <code>byteArray[offset]</code> is written first,
   * then <code>btyeArray[offset+1]</code>, and so on; the
   * last byte written is <code>btyeArray[offset+length-1]</code>.
   *
   * @param byteArray the data.
   * @param offset the start offset in the data.
   * @param length the number of bytes to write.
   * @author Bill Darbie
   */
  public void write(byte byteArray[], int offset, int length) throws IOException
  {
    Assert.expect(byteArray != null);
    Assert.expect(offset >= 0);
    Assert.expect(length >= 0);
    Assert.expect(offset + length <= byteArray.length);

    _os.write(byteArray, offset, length);
  }

  /**
   * Writes a <code>boolean</code> value to this output stream.
   * If the argument <code>value</code>
   * is <code>true</code>, the value <code>(byte)1</code>
   * is written; if <code>value</code> is <code>false</code>,
   * the  value <code>(byte)0</code> is written.
   * The byte written by this method may
   * be read by the <code>readBoolean</code>
   * method of interface <code>DataInput</code>,
   * which will then return a <code>boolean</code>
   * equal to <code>value</code>.
   *
   * @param      value   the boolean to be written.
   * @exception  IOException  if an I/O error occurs.
   * @author Bill Darbie
   */
  public void writeBoolean(boolean value) throws IOException
  {
    if (value)
      _os.write(1);
    else
      _os.write(0);
  }

  /**
   * Writes to the output stream the eight low-
   * order bits of the argument <code>value</code>.
   * The 24 high-order bits of <code>value</code>
   * are ignored. (This means  that <code>writeByte</code>
   * does exactly the same thing as <code>write</code>
   * for an integer argument.) The byte written
   * by this method may be read by the <code>readByte</code>
   * method of interface <code>DataInput</code>,
   * which will then return a <code>byte</code>
   * equal to <code>(byte)value</code>.
   *
   * @param      value   the byte value to be written.
   * @exception  IOException  if an I/O error occurs.
   * @author Bill Darbie
   */
  public void writeByte(int value) throws IOException
  {
    _os.write(value);
  }

  /**
   * Writes two bytes to the output
   * stream to represent the value of the argument.
   * The byte values to be written, in the  order
   * shown, are for big endian: <p>
   * <pre><code>
   * (byte)(0xff &amp; (value &gt;&gt; 8))
   * (byte)(0xff &amp; value)
   * </code> </pre> <p>
   * for little endian:
   * <pre><code>
   * (byte)(0xff &amp; value)
   * (byte)(0xff &amp; (value &gt;&gt; 8))
   * </code> </pre> <p>
   *
   * The bytes written by this method may be
   * read by the <code>readShort</code> method
   * of interface <code>DataInput</code> , which
   * will then return a <code>short</code> equal
   * to <code>(short)value</code>.
   *
   * @param value   the <code>short</code> value to be written.
   * @exception  IOException  if an I/O error occurs.
   * @author Bill Darbie
   */
  abstract public void writeShort(int value) throws IOException;

  /**
   * Writes a <code>char</code> value, wich
   * is comprised of two bytes, to the
   * output stream.
   * The byte values to be written, in the  order
   * shown, are:
   * <p><pre><code>
   * (byte)(0xff &amp; (value &gt;&gt; 8))
   * (byte)(0xff &amp; value)
   * </code></pre><p>
   * The bytes written by this method may be
   * read by the <code>readChar</code> method
   * of interface <code>DataInput</code> , which
   * will then return a <code>char</code> equal
   * to <code>(char)value</code>.
   *
   * @param value   the <code>char</code> value to be written.
   * @exception  IOException  if an I/O error occurs.
   * @author Bill Darbie
   */
  public void writeChar(int value) throws IOException
  {

    Assert.expect(false, "Method CDataOutputStream.writeChar() not supported, C/C++ do not support 2 byte characters");
  }

  /**
   * Writes an <code>int</code> value, which is
   * comprised of four bytes, to the output stream.
   * The byte values to be written, in the  order
   * shown, are for big endian:
   * <p><pre><code>
   * (byte)(0xff &amp; (value &gt;&gt; 24))
   * (byte)(0xff &amp; (value &gt;&gt; 16))
   * (byte)(0xff &amp; (value &gt;&gt; &#32; &#32;8))
   * (byte)(0xff &amp; value)
   * </code></pre><p>
   * for little endian:
   * <p><pre><code>
   * (byte)(0xff &amp; (value &gt;&gt; 24))
   * (byte)(0xff &amp; (value &gt;&gt; 16))
   * (byte)(0xff &amp; (value &gt;&gt; &#32; &#32;8))
   * (byte)(0xff &amp; value)
   * </code></pre><p>   *
   * The bytes written by this method may be read
   * by the <code>readInt</code> method of interface
   * <code>DataInput</code> , which will then
   * return an <code>int</code> equal to <code>v</code>.
   *
   * @param      value   the <code>int</code> value to be written.
   * @exception  IOException  if an I/O error occurs.
   * @author Bill Darbie
   */
  abstract public void writeInt(int value) throws IOException;

  /**
   * Writes an <code>long</code> value, which is
   * comprised of four bytes, to the output stream.
   * The byte values to be written, in the  order
   * shown, are:
   * <p><pre><code>
   * (byte)(0xff &amp; (v &gt;&gt; 24))
   * (byte)(0xff &amp; (v &gt;&gt; 16))
   * (byte)(0xff &amp; (v &gt;&gt;  8))
   * (byte)(0xff &amp; v)
   * </code></pre><p>
   * The upper four bytes of the JAVA 8 byte long are discarded.
   * The bytes written by this method may be
   * read by the <code>readLong</code> method
   * of interface <code>DataInput</code> , which
   * will then return a <code>long</code> equal
   * to <code>v</code>.
   *
   * @param value   the <code>long</code> value to be written.
   * @exception  IOException  if an I/O error occurs.
   * @author Bill Darbie
   */
  abstract public void writeLong(long value) throws IOException;

  /**
   * Writes a <code>float</code> value,
   * which is comprised of four bytes, to the output stream.
   * It does this as if it first converts this
   * <code>float</code> value to an <code>int</code>
   * in exactly the manner of the <code>Float.floatToIntBits</code>
   * method  and then writes the <code>int</code>
   * value in exactly the manner of the  <code>writeInt</code>
   * method.  The bytes written by this method
   * may be read by the <code>readFloat</code>
   * method of interface <code>DataInput</code>,
   * which will then return a <code>float</code>
   * equal to <code>value</code>.
   *
   * @param      value   the <code>float</code> value to be written.
   * @exception  IOException  if an I/O error occurs.
   * @author Bill Darbie
   */
  public void writeFloat(float value) throws IOException
  {
    int bits = Float.floatToRawIntBits(value);
    writeInt(bits);
  }

  /**
   * Writes a <code>double</code> value,
   * which is comprised of eight bytes, to the output stream.
   * It does this as if it first converts this
   * <code>double</code> value to a <code>long</code>
   * in exactly the manner of the <code>Double.doubleToLongBits</code>
   * method  and then writes the <code>long</code>
   * value in exactly the manner of the  <code>writeLong</code>
   * method. The bytes written by this method
   * may be read by the <code>readDouble</code>
   * method of interface <code>DataInput</code>,
   * which will then return a <code>double</code>
   * equal to <code>value</code>.
   *
   * @param      value   the <code>double</code> value to be written.
   * @exception  IOException  if an I/O error occurs.
   */
  abstract public void writeDouble(double value) throws IOException;

  /**
   * Writes a string to the output stream.
   * For every character in the string
   * <code>string</code>,  taken in order, one byte
   * is written to the output stream.  If
   * <code>string</code> is <code>null</code>, a <code>NullPointerException</code>
   * is thrown.<p>  If <code>s.length</code>
   * is zero, then no bytes are written. Otherwise,
   * the character <code>string[0]</code> is written
   * first, then <code>string[1]</code>, and so on;
   * the last character written is <code>s[string.length-1]</code>.
   * For each character, one byte is written,
   * the low-order byte, in exactly the manner
   * of the <code>writeByte</code> method . The
   * high-order eight bits of each character
   * in the string are ignored.  At the end of the string
   * a null terminator is written out.
   *
   * @param      string   the string of bytes to be written.
   * @exception  IOException  if an I/O error occurs.
   * @author Bill Darbie
   */
  public void writeBytes(String string) throws IOException
  {
    Assert.expect(string != null);

    _os.write(string.getBytes());
    // add on a C/C++ style null terminator
    _os.write(0);
  }

  /**
   * Writes every character in the string <code>string</code>,
   * to the output stream, in order,
   * two bytes per character. If <code>string</code>
   * is <code>null</code>, a <code>NullPointerException</code>
   * is thrown.  If <code>string.length</code>
   * is zero, then no characters are written.
   * Otherwise, the character <code>string[0]</code>
   * is written first, then <code>string[1]</code>,
   * and so on; the last character written is
   * <code>string[string.length-1]</code>. For each character,
   * two bytes are actually written, high-order
   * byte first, in exactly the manner of the
   * <code>writeChar</code> method.
   *
   * @param      string   the string value to be written.
   * @exception  IOException  if an I/O error occurs.
   * @author Steve Anonson
   */
  public void writeChars(String string)
  {
    Assert.expect(false, "Method writeChars() not implemented, C/C++ do not support 2 byte characters.");
  }

  /**
   * Writes two bytes of length information
   * to the output stream, followed
   * by the Java modified UTF representation
   * of  every character in the string <code>s</code>.
   * If <code>s</code> is <code>null</code>,
   * a <code>NullPointerException</code> is thrown.
   * Each character in the string <code>s</code>
   * is converted to a group of one, two, or
   * three bytes, depending on the value of the
   * character.<p>
   * If a character <code>c</code>
   * is in the range <code>&#92;u0001</code> through
   * <code>&#92;u007f</code>, it is represented
   * by one byte:<p>
   * <pre>(byte)c </pre>  <p>
   * If a character <code>c</code> is <code>&#92;u0000</code>
   * or is in the range <code>&#92;u0080</code>
   * through <code>&#92;u07ff</code>, then it is
   * represented by two bytes, to be written
   * in the order shown:<p> <pre><code>
   * (byte)(0xc0 | (0x1f &amp; (c &gt;&gt; 6)))
   * (byte)(0x80 | (0x3f &amp; c))
   *  </code></pre>  <p> If a character
   * <code>c</code> is in the range <code>&#92;u0800</code>
   * through <code>uffff</code>, then it is
   * represented by three bytes, to be written
   * in the order shown:<p> <pre><code>
   * (byte)(0xe0 | (0x0f &amp; (c &gt;&gt; 12)))
   * (byte)(0x80 | (0x3f &amp; (c &gt;&gt;  6)))
   * (byte)(0x80 | (0x3f &amp; c))
   *  </code></pre>  <p> First,
   * the total number of bytes needed to represent
   * all the characters of <code>s</code> is
   * calculated. If this number is larger than
   * <code>65535</code>, then a <code>UTFDataFormatError</code>
   * is thrown. Otherwise, this length is written
   * to the output stream in exactly the manner
   * of the <code>writeShort</code> method;
   * after this, the one-, two-, or three-byte
   * representation of each character in the
   * string <code>s</code> is written.<p>  The
   * bytes written by this method may be read
   * by the <code>readUTF</code> method of interface
   * <code>DataInput</code> , which will then
   * return a <code>String</code> equal to <code>s</code>.
   *
   * @param      str   the string value to be written.
   * @exception  IOException  if an I/O error occurs.
   */
  public void writeUTF(String str) throws IOException
  {
    Assert.expect(false, "Method writeUTF() not implemented, C/C++ do not support UTF.");
  }

  /**
   * Write a Pascal style string of characters.
   * The value in the first byte determines how many of the characters are put
   * into the String to be returned.
   *
   * @param string is the string to be written
   * @exception  IOException if there is an IO error.
   * @author Bill Darbie
   */
  public void writePascalString(String string) throws IOException
  {
    Assert.expect(string != null);

    int length = string.length();
    byte[] bytes = string.getBytes();
    _os.write(length);
    _os.write(bytes);
  }

  /**
   * Write a Pascal style packed array of characters.
   *
   * @param string is the string to be written
   * @exception  IOException if there is an IO error.
   * @author Bill Darbie
   */
  public void writePascalPackedArray(String string) throws IOException
  {
    Assert.expect(string != null);

    byte[] bytes = string.getBytes();
    _os.write(bytes);
  }
}
