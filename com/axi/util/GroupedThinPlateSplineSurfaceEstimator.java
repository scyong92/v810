package com.axi.util;

import Jama.Matrix;
import java.awt.geom.*;

/**
 * Title: GroupedThinPlateSplineSurfaceEstimator
 *
 * Description: Fit an (optionally) grouped thin plate spline to a set of {x, y, z, group, side}
 * data points. Groups consists of subsets of points with a common additive z offset including a
 * sign determined by side. Fitted coefficients and offsets can then be used to predict z height
 * at other (x, y, group, side) locations.
 *
 * For the X6000, this class is designed to be used in the following manner:
 *   a) During a learning step, a fit will be done to a set of (x, y, z, group, side) points
 *      encompassing the entire board. The fitted offsets from this step will be used subsequently,
 *      and the rest of the data discarded. Offsets for groups with insufficient data will be set to 0.
 *   b) During runtime, the fitted offsets will be used to restore data from different subtypes back to
 *      a common reference plane, and then a local (ungrouped) thin plate spline will be fit to
 *      neighboring region to a local surface which will be used for predicting z height.
 *
 * Copyright: Copyright (c) 2008, Agilent Technologies, Inc. All rights reserved.
 *
 * @author John Heumann
 */
public class GroupedThinPlateSplineSurfaceEstimator
{
  final private double _DEFAULT_STIFFNESS = 10.0;
  private double _lambda;
  private int _nDataPoints;
  private double _x[] = null;
  private double _y[] = null;
  private double _z[] = null;
  private int _nGroups;
  private int _group[] = null;
  private int _groupCount[] = null;
  private int _side[] = null;
  private boolean _groupedThinPlateSpline;
  private int _mSize;                // size of square system matrix
  private Matrix _LHS;               // square system matrix
  private Matrix _RegularizedLHSInv; // inverse of regularized system matrix
  private Matrix _rhs;               // right hand side vector
  private Matrix _coefficients;      // solution vector

  /**
   * Constructor
   * @author John Heumann
   *
   * @param x y z      location of a data point
   * @param nGroups    number of distinct groups
   * @param group      group number for the current point (0 .. nGroups - 1 inclusive; NOTE: ZERO-BASED!!!)
   * @param side       -1 = bottom, 1 = top
   * @param stiffness  stiffness parameter. Defaults to 10 if negative.
   */
  public GroupedThinPlateSplineSurfaceEstimator(double x[], double y[], double z[],
                                                int nGroups, int group[], int side[], double stiffness)
  {
    Assert.expect(x != null);
    Assert.expect(y != null);
    Assert.expect(z != null);
    Assert.expect(nGroups >  0);
    Assert.expect(group != null);
    Assert.expect(side != null);

    _nDataPoints = x.length;
    Assert.expect(_nDataPoints >= 3);
    Assert.expect(_nDataPoints == y.length);
    Assert.expect(_nDataPoints == z.length);
    Assert.expect(_nDataPoints == group.length);
    Assert.expect(_nDataPoints == side.length);

    for (int i = 0; i < _nDataPoints; i++)
    {
      Assert.expect(side[i] == -1 || side[i] == 1);
      Assert.expect(group[i] >= 0 && group[i] < nGroups);
    }

    _x = x;
    _y = y;
    _z = z;
    _nGroups = nGroups;
    _mSize = _nDataPoints + _nGroups + 2;

    _group = group;
    _groupCount = new int[nGroups];
    /** not needed... initialized to 0 by new
    for (int i = 0; i < nGroups; i++)
      _groupCount[i] = 0;
     **/
    _side = side;
    if (stiffness >= 0)
      _lambda = stiffness;
    else
      _lambda = _DEFAULT_STIFFNESS;

    _groupedThinPlateSpline = true;

    countGroupMembers();

    fitGroupedThinPlateSpline();
  }

  /**
   * Constructor for a simple (ungrouped, single-sided) thin plate spline
   * @author John Heumann
   *
   * @param x y z      location of a data point
   * @param stiffness  stiffness parameter. Defaults to 10.0 if negative.
   *
   */
  public GroupedThinPlateSplineSurfaceEstimator(double x[], double y[], double z[], double stiffness)
  {
    Assert.expect(x != null);
    Assert.expect(y != null);
    Assert.expect(z != null);

    _nDataPoints = x.length;
    Assert.expect(_nDataPoints >= 3);
    Assert.expect(_nDataPoints == y.length);
    Assert.expect(_nDataPoints == z.length);

    _x = x;
    _y = y;
    _z = z;
    _nGroups = 1;
    _mSize = _nDataPoints + _nGroups + 2;

    _group = new int[_nDataPoints];
    _groupCount = new int[1];
    // _groupCount[0] = 0;         Not needed... initialized to 0 by new
    _side = new int[_nDataPoints];
    for (int i = 0; i < _nDataPoints; i++)
    {
      _group[i] = 0;
      _side[i] = 1;
    }

    if (stiffness >= 0)
      _lambda = stiffness;
    else
      _lambda = _DEFAULT_STIFFNESS;

    _groupedThinPlateSpline = false;

    _groupCount[0] = _nDataPoints;

    fitGroupedThinPlateSpline();
  }

  /**
   * Get the predicted z heights at the set of points used for fitting
   * Note: fitted offsets will be used for all groups. (I.e. offset will
   * not be forced to 0 for groups with too few training samples).
   *
   * @author John Heumann
   */
  public double[] getEstimatedHeights()
  {
    return doHeightEstimation(_x, _y, _group, _side);
  }

  /**
   * Get the predicted z heights at a given set of {x, y, group, side} points
   * Note: fitted offsets will be used for all groups. (I.e. offset will not
   * be forced to 0 for groups with too few training samples).
   *
   * @author John Heumann
   */
  public double[] getEstimatedHeights(double x[], double y[], int group[], int side[])
  {
    Assert.expect(x != null);
    Assert.expect(y != null);
    Assert.expect(group != null);
    Assert.expect(side != null);

    int n = x.length;
    Assert.expect(n > 0);
    Assert.expect(n == y.length);
    Assert.expect(n == group.length);
    Assert.expect(n == side.length);

    for (int i = 0; i < n; i++)
    {
      Assert.expect(side[i] == -1 || side[i] == 1);
      Assert.expect(group[i] >= 0 && group[i] < _nGroups);
    }

    return doHeightEstimation(x, y, group, side);
  }

  /**
   * Get the predicted z heights at a given set of {x, y} points
   * This interface is only valid for the simple ungrouped, single-sided case
   *
   * @author John Heumann
   */
  public double[] getEstimatedHeights(double x[], double y[])
  {
    Assert.expect(_groupedThinPlateSpline == false);
    Assert.expect(x != null);
    Assert.expect(y != null);

    int  n = x.length;
    Assert.expect(n > 0);
    Assert.expect(n == y.length);

    int group[] = new int[n];
    int side[] = new int[n];
    for (int i = 0; i < n; i++)
    {
      group[i] = 0;
      side[i] = 1;
    }

    return doHeightEstimation(x, y, group, side);
  }

  /**
   * Get the predicted z height at a single {x, y, group, side} point
   * Note: fitted offsets will be used for all groups. (I.e. offset will
   * not be forced to 0 for groups with too few training samples).
   *
   * @author John Heumann
   */
  public double getEstimatedHeight(double x, double y, int group, int side)
  {
    Assert.expect(side == -1 || side == 1);
    Assert.expect(group >= 0 && group < _nGroups);

    double xv[] = new double[1];
    xv[0] = x;

    double yv[] = new double[1];
    yv[0] = y;

    int groupv[] = new int[1];
    groupv[0] = group;

    int sidev[] = new int[1];
    sidev[0] = side;

    double zv[] = doHeightEstimation(xv, yv, groupv, sidev);

    return zv[0];
  }

  /**
   *
   * Get the predicted z height at a single {x, y} point
   * This interface is only valid for the simple ungrouped, single-sided case
   *
   * @author John Heumann
   */
  public double getEstimatedHeight(double x, double y)
  {
    Assert.expect(_groupedThinPlateSpline == false);

    double xv[] = new double[1];
    xv[0] = x;

    double yv[] = new double[1];
    yv[0] = y;

    int groupv[] = new int[1];
    groupv[0] = 0;

    int sidev[] = new int[1];
    sidev[0] = 1;

    double zv[] = doHeightEstimation(xv, yv, groupv, sidev);
    return zv[0];
  }

  /**
   * @author John Heumann
   *
   * @return the root median square of the cross-validation error over the original data.
   *         Median is used instead of mean for robustness against outliers.
   *
   * In most cases, the default stiffness of 10.0 is adequate for our applications.
   * If  greater accuracy is desired, this routine can be used to choose a stiffness
   * to minimize the cross-validiation error.
   *
   * Note: the current implementation is not particularly efficient, since we re-initiize for
   * each value of stiffness. Additionally, solving for the coefficients and then solving for
   * the inverse matrix to find CV error is redundant. I've implemented it this way because in
   * our current use model the inverse matrix and CV error are not needed. If that changes, we
   * should consider restructuring this class.
   */
  public double getCrossValidationError()
  {
    if (_RegularizedLHSInv == null)
    {
      // Add the regularization terms on the diagonal of this system matrix
      for (int i = 0; i < _nDataPoints; i++)
        _LHS.set(i, i, _lambda);

      _RegularizedLHSInv = _LHS.inverse();

      // Remove the regularization terms
      for (int i = 0; i < _nDataPoints; i++)
        _LHS.set(i, i, 0.0);
    }

    Matrix squaredErrs = new Matrix(_nDataPoints, 1);
    for (int i = 0; i < _nDataPoints; i++)
    {
      double temp = -_coefficients.get(i, 0) / _RegularizedLHSInv.get(i, i);
      squaredErrs.set(i, 0, temp * temp);
    }
    double se[] = squaredErrs.getRowPackedCopy();
    java.util.Arrays.sort(se);

    // compute square root of median squared error
    int i = _nDataPoints / 2;
    double errSquared;
    if (_nDataPoints % 2 == 0)
      errSquared = 0.5 * (se[i] + se[i + 1]);
    else
      errSquared = se[i + 1];

    return Math.sqrt(errSquared);
  }


  /**
   * @author John Heumann
   *
   * @return the fitted, group-specific offsets
   *         Offsets for groups with fewer than minCount training samples will be set to 0
   *
   */
  public double[] getOffsets(int minCount)
  {
    double[] offsets = new double[_nGroups];  // implicitly sets all offsets to 0 initially
    // Want offsets wrt fitted surface. When there is only one group, set to 0.
    if (_nGroups == 1)
      offsets[0] = 0.0;
    else
    {
      // With multiple groups, use the offset relative to group 0
      // A fancier strategy, which we might wish to implement at some point,
      // would be to use offsets relative to the group with the most samples.
      double offset0 = _coefficients.get(_nDataPoints, 0);
      for (int i = 0; i < _nGroups; i++)
        if (_groupCount[i] >= minCount)
          offsets[i] = _coefficients.get(_nDataPoints + i, 0) - offset0;
    }

    return offsets;
  }

  /**
   * @author John Heumann
   */
  private void countGroupMembers()
  {
    for (int i = 0; i < _nDataPoints; i++)
      _groupCount[_group[i]] += 1;
  }

  /**
   * @author John Heumann
   */
  private double[] doHeightEstimation(double x[], double y[], int group[], int side[])
  {
    final int n = x.length;
    Assert.expect(y.length == n);
    Assert.expect(group.length == n);
    Assert.expect(side.length == n);

    Matrix M = new Matrix(x.length, _mSize);   // all entries initialized to 0
    final int kn = _nDataPoints + _nGroups;
    final int kn1 = kn + 1;

    for (int i = 0; i < x.length; i++)
    {
      for (int j = 0; j < _nDataPoints; j++)
      {
        double r = Point2D.Double.distance(x[i], y[i], _x[j], _y[j]);
        M.set(i, j, thinPlateSplineRadialBasisFunction(r));
      }
      Assert.expect(group[i] >= 0 && group[i] < _nGroups);
      Assert.expect(side[i]== -1 || side[i] == 1);
      M.set(i, _nDataPoints + group[i], side[i]);
      M.set(i, kn, x[i]);
      M.set(i, kn1, y[i]);
    }

    return M.times(_coefficients).getRowPackedCopy();
  }

  /**
   * @author John Heumann
   */
  private void fitGroupedThinPlateSpline()
  {
    _LHS = initializeLHSSystemMatrix();
    // Add the regularization terms on the diagonal of this system matrix
    for (int i = 0; i < _nDataPoints; i++)
      _LHS.set(i, i, _lambda);

    _RegularizedLHSInv = null;
    _rhs = initializeRHSVector();

    _coefficients = _LHS.solve(_rhs);

    // Remove the regularization terms
    for (int i = 0; i < _nDataPoints; i++)
      _LHS.set(i, i, 0.0);
  }

  /**
   * @author John Heumann
   */
  private Matrix initializeRHSVector()
  {
    Matrix M = new Matrix(_mSize, 1);
    // Note implicit M[i, 1] = 0 for i = nDataPoints .. _mSize
    for (int i = 0; i < _nDataPoints; i++)
      M.set(i, 0, _z[i]);

    return M;
  }

  /**
   * @author John Heumann
   */
  private Matrix initializeLHSSystemMatrix()
  {
    final int kn = _nDataPoints + _nGroups;
    Matrix M = new Matrix(_mSize, _mSize);   // all values initialized to 0

    for (int i = 0; i < _nDataPoints; i++)
    {
      for (int j = 0; j < i; j++)
      {
          double r = Point2D.Double.distance(_x[i], _y[i], _x[j], _y[j]);
          double temp = thinPlateSplineRadialBasisFunction(r);
          M.set(i, j, temp);
          M.set(j, i, temp);
      }
      Assert.expect(_group[i] >= 0 && _group[i] < _nGroups);
      Assert.expect(_side[i] == -1 || _side[i] == 1);
      int k = _nDataPoints + _group[i];
      M.set(i, k, _side[i]);
      M.set(k, i, _side[i]);
      k = kn;
      M.set(i, k, _x[i]);
      M.set(k++, i, _x[i]);
      M.set(i, k, _y[i]);
      M.set(k, i, _y[i]);
    }

    return M;
  }

  private double thinPlateSplineRadialBasisFunction(double r)
  {
    if (r <= 0.0)   // should never be negative, but let's play it safe
      return 0.0;
    else
      return r * r * Math.log(r);
  }

}
