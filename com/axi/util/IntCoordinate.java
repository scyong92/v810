package com.axi.util;

import java.awt.geom.*;
import java.io.*;

import com.axi.util.*;

/**
* This class represents a two dimensional set of coordinates.
*
* @author Bill Darbie
* @author Matt Wharton
*/
public class IntCoordinate implements Serializable
{
  // This class really just wraps a java.awt.geom.Point2D.Double.
  // NOTE: This IS a serializable class and the Point2D is supposed to be transient.  This is handled
  // via the overridden readObject/writeObject methods below.
  private transient Point2D.Double _point = null;

  /**
   * Construct this class and set the coordinates to 0,0.
   *
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public IntCoordinate()
  {
    _point = new Point2D.Double(0d, 0d);
  }

  /**
   * Make a deep copy of the object passed in..
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public IntCoordinate(IntCoordinate rhs)
  {
    Assert.expect(rhs != null);

    _point = new Point2D.Double(rhs.getX(), rhs.getY());
  }

  /**
   * Construct the class with the coordinates passed in.
   *
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public IntCoordinate(int x, int y)
  {
    _point = new Point2D.Double(x, y);
  }

  /**
   * Construct the class with the coordinates passed in.
   *
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public IntCoordinate(long x, long y)
  {
    _point = new Point2D.Double(x, y);
  }


  /**
   * Make a deep copy of the given DoubleCoordinate, rounding the coordinate
   * to the nearest integer.
   *
   * @author Peter Esbensen
   */
  public IntCoordinate(DoubleCoordinate rhs)
  {
    Assert.expect(rhs != null);
    _point = new Point2D.Double( Math.round(rhs.getX()), Math.round(rhs.getY()) );
  }

  /**
   * Set the coordinates to new values
   *
   * @author Bill Darbie
   * @author Matt Wharton
   */
  void setXY(int x, int y)
  {
    setLocation(x, y);
  }

  /**
   * Sets the coordinates to new values.
   *
   * @author Matt Wharton
   */
  public void setLocation(int x, int y)
  {
    _point.setLocation( x, y );
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public void setX(int x)
  {
    _point.setLocation(x, _point.getY());
  }

  /**
   * @author Bill Darbie
   */
  public int getX()
  {
    return (int)_point.getX();
  }

  /**
   * @author Bill Darbie
   */
  public void setY(int y)
  {
    _point.setLocation(_point.getX(), y);
  }

  /**
   * @author Bill Darbie
   */
  public int getY()
  {
    return (int)_point.getY();
  }


  /**
   * Calculates the distance between the specified coordinates.
   *
   * @param x1 the x coordinate of the first point.
   * @param y1 the y coordinate of the first point.
   * @param x2 the x coordinate of the second point.
   * @param y2 the y coordinate of the second point.
   * @return the distance between the two points.
   * @author Matt Wharton
   */
  public static double distance(int x1, int y1, int x2, int y2)
  {
    return Point2D.distance(x1, y1, x2, y2);
  }


  /**
   * Calculates the distance between this IntCoordinate the specified coordinate.
   *
   * @param x the x dimension of the specified coordinate.
   * @param y the y dimension of the specified coordinate.
   * @return the distance between the IntCoordinate and the specified coordinate.
   * @author Matt Wharton
   */
  public double distance(int x, int y)
  {
    return _point.distance(x, y);
  }


  /**
   * Calculates the distance between this IntCoordinate the specified IntCoordinate.
   *
   * @param coordinate the IntCoordinate to measure the distance to.
   * @return the distance between the IntCoordinate and the specified IntCoordinate.
   * @author Matt Wharton
   */
  public double distance(IntCoordinate coordinate)
  {
    Assert.expect(coordinate != null);

    return _point.distance(coordinate.getX(), coordinate.getY());
  }


  /**
   * Calculates the squared distance between the specified coordinates.
   *
   * @param x1 the x coordinate of the first point.
   * @param y1 the y coordinate of the first point.
   * @param x2 the x coordinate of the second point.
   * @param y2 the y coordinate of the second point.
   * @return the squared distance between the two points.
   * @author Matt Wharton
   */
  public static double distanceSq(int x1, int y1, int x2, int y2)
  {
    return Point2D.distanceSq(x1, y1, x2, y2);
  }


  /**
   * Calculates the squared distance between this IntCoordinate the specified coordinate.
   *
   * @param x the x dimension of the specified coordinate.
   * @param y the y dimension of the specified coordinate.
   * @return the squared distance between the IntCoordinate and the specified coordinate.
   * @author Matt Wharton
   */
  public double distanceSq(int x, int y)
  {
    return _point.distanceSq(x, y);
  }


  /**
   * Calculates the squared distance between this IntCoordinate the specified IntCoordinate.
   *
   * @param coordinate the IntCoordinate to measure the squared distance to.
   * @return the squared distance between the IntCoordinate and the specified IntCoordinate.
   * @author Matt Wharton
   */
  public double distanceSq(IntCoordinate coordinate)
  {
    Assert.expect(coordinate != null);

    return _point.distanceSq(coordinate.getX(), coordinate.getY());
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return _point.toString();
  }

  /**
   * Returns true if the x and y coordinates of the two objects are the same.
   *
   * @author Bill Darbie
   */
  public boolean equals(Object object)
  {
    boolean equal = false;

    if ((object != null) && (object instanceof IntCoordinate))
    {
      IntCoordinate coord = (IntCoordinate)object;
      if ((coord.getX() == getX()) && (coord.getY() == getY()))
        equal = true;
    }

    return equal;
  }

  /**
   * Return an IntCoordinate that is the result of subtracting the given IntCoordinate from this one.  This is
   * vector subtraction.
   *
   * @author Peter Esbensen
   */
  public IntCoordinate minus(IntCoordinate rhs)
  {
    return new IntCoordinate((int)(_point.getX() - rhs.getX()), (int)(_point.getY() - rhs.getY()));
  }

  /**
   * Return an IntCoordinate that is the result of adding the given IntCoordinate to this one.  This is
   * vector addition.
   *
   * @author Peter Esbensen
   */
  public IntCoordinate plus(IntCoordinate rhs)
  {
    // Because we're adding nanometer based coordinates, there is real potential to overflow MAX_INT
    // so, here, I'm converting to longs, and using doubles to get around that.
    // Generally, INTs are ok, MAX_INT is over 89 inches long, but when adding coordinate to compute an
    // average, it can overflow.
    DoubleCoordinate dc = new DoubleCoordinate((long)(_point.getX() + rhs.getX()), (long)(_point.getY() + rhs.getY()));
    return new IntCoordinate(dc);
  }

  /**
   * Return a DoubleCoordinate that is the result of multiplying this IntCoordintae by the given factor.  This is
   * vector scaling.<p>
   *
   * For example,<p>
   * <code>
   * IntCoordinate x = new IntCoordinate(2,3); <br>
   * DoubleCoordinate y = x.scale(2.0f); <br>
   * <br>
   * // y now equals (4.0, 6.0) <br>
   * </code>
   *
   * @author Peter Esbensen
   */
  public DoubleCoordinate scale(double scalingFactor)
  {
    return new DoubleCoordinate((_point.getX() * scalingFactor), (_point.getY() * scalingFactor));
  }

  /**
   * @author Bill Darbie
   */
  public int hashCode()
  {
    return _point.hashCode();
  }

  /**
   * Returns the underlying Point2D of this IntCoordinate.
   *
   * @author Matt Wharton
   */
  public Point2D getPoint2D()
  {
    Assert.expect(_point != null);

    return _point;
  }

  /**
   * @author Matt Wharton
   */
  protected void deserializePoint(ObjectInputStream is) throws IOException
  {
    Assert.expect(is != null);

    double tempX = is.readDouble();
    double tempY = is.readDouble();
    _point = new Point2D.Double(tempX, tempY);
  }

  /**
   * @author Matt Wharton
   */
  protected void serializePoint(ObjectOutputStream os) throws IOException
  {
    Assert.expect(os != null);
    Assert.expect(_point != null);

    os.writeDouble(_point.getX());
    os.writeDouble(_point.getY());
  }

  /**
   * @author Matt Wharton
   */
  private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
  {
    is.defaultReadObject();
    deserializePoint(is);
  }

  /**
   * @author Matt Wharton
   */
  private void writeObject(ObjectOutputStream os) throws IOException
  {
    os.defaultWriteObject();
    serializePoint(os);
  }
}
