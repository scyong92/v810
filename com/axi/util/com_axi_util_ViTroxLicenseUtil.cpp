#ifdef _MSC_VER
  #pragma warning (disable:4715)
  #pragma warning (disable:4530)
  #pragma warning (disable:4786)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#include "../thirdParty/ViTroxLicense/src/LicenseAPI.h"
#include "util/src/AssertException.h"
#include "util/src/sptAssert.h"

#include "JNIUtil.h"
#include "com_axi_util_VitroxLicenseUtil.h"

using namespace std;

static string INVALID_LICENSE_EXCEPTION = "com/axi/util/VitroxLicenseException";

/**
 * This function converts the c++ excpetion "VitroxLicenseException" 
 * to java exception "VitroxLicenseException" and throw it to Java code.
 * 
 * @author Wei Chin
 */
void throwVitroxLicenseException(JNIEnv* pEnv)
{
  sptAssert(pEnv);

  jclass javaExceptionClass = pEnv->FindClass(INVALID_LICENSE_EXCEPTION.c_str());
  sptAssert(javaExceptionClass);

  pEnv->ThrowNew(javaExceptionClass, "ViTrox License Server is Missing");
}

/**
 * This function challenge the DLL with valid key and result
 *
 * Class:     com_axi_util_ViTroxLicenseUtil
 * Method:    nativeChallengeMe
 * Signature: (Ljava/lang/String;Ljava/lang/String;)V
 *
 * @author Chong, Wei Chin
 */
JNIEXPORT jstring JNICALL 
Java_com_axi_util_ViTroxLicenseUtil_nativeChallengeMe(JNIEnv* pEnv, jobject obj, jstring message)
{
  try
  {
    string messageStr = getSTLString(pEnv, message);

	char challengeResult[256];
		
	ChallengeMe(messageStr,challengeResult);
	
	return pEnv->NewStringUTF(challengeResult);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    throwVitroxLicenseException(pEnv);
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * This function challenge the DLL with valid key and result
 *
 * Class:     com_axi_util_ViTroxLicenseUtil
 * Method:    nativeChallengeMe
 * Signature: (Ljava/lang/String;Ljava/lang/String;)V
 *
 * @author Chong, Wei Chin
 */
JNIEXPORT jstring JNICALL 
Java_com_axi_util_ViTroxLicenseUtil_nativeChallengeMeManage(JNIEnv* pEnv, jobject obj, jstring message)
{
  try
  {
	string messageStr = getSTLString(pEnv, message);

	char challengeResult[256];
		
	ChallengeMeManaged(const_cast<char*>(messageStr.c_str()),challengeResult);
	
	return pEnv->NewStringUTF(challengeResult);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    throwVitroxLicenseException(pEnv);
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/**
 * This function determines whether or not the given version of feature is enabled in the system by reading the license.  
 * Please refer comments in com/axi/util/ViTroxLicenseUtil.java for more information.
 * 
 * Class:     com_axi_util_ViTroxLicenseUtil
 * Method:    nativeGetServerModeLicenseStatus
 * Signature: (Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Z
 *
 * @author Chong Wei Chin
 */
JNIEXPORT jstring JNICALL 
Java_com_axi_util_ViTroxLicenseUtil_nativeGetServerModeLicenseStatus(JNIEnv* pEnv, jobject obj, jstring serverip, jint productID)
{
  try
  {
	string serverIpStr = getSTLString(pEnv, serverip);
	char customCString[512];
	
	int status = -1;
    status = GetServerModeLicenseStatus(const_cast<char*>(serverIpStr.c_str()), productID, customCString);
	if(status == 0)
		return pEnv->NewStringUTF(customCString);
	else
	{
		itoa(status,customCString,10);
		return pEnv->NewStringUTF(customCString);
	}
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    throwVitroxLicenseException(pEnv);
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}


/**
 * This function determines whether or not the given version of feature is enabled in the system by reading the license.  
 * Please refer comments in com/axi/util/ViTroxLicenseUtil.java for more information.
 * 
 * Class:     com_axi_util_ViTroxLicenseUtil
 * Method:    nativeGetServerModeLicenseStatus2
 * Signature: (Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Z
 *
 * @author Chong Wei Chin
 */
JNIEXPORT jstring JNICALL 
Java_com_axi_util_ViTroxLicenseUtil_nativeGetServerModeLicenseStatus2(JNIEnv* pEnv, jobject obj, jstring serverip, jint productID, jstring versionString)
{
  try
  {
	string serverIpStr = getSTLString(pEnv, serverip);
	string versionNumberStr = getSTLString(pEnv, versionString);
    char customCString[512];
	int status = -1;
	status = GetServerModeLicenseStatus2(const_cast<char*>(serverIpStr.c_str()), productID, const_cast<char*>(versionNumberStr.c_str()), customCString);
	if (status == 0)
		return pEnv->NewStringUTF(customCString);
	else
	{
		itoa(status,customCString,10);
		return pEnv->NewStringUTF(customCString);
	}
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    throwVitroxLicenseException(pEnv);
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/**
 * This function determines dll version
 * Please refer comments in com/axi/util/ViTroxLicenseUtil.java for more information.
 * 
 * Class:     com_axi_util_ViTroxLicenseUtil
 * Method:    nativeGetDllVersion
 * Signature: ()Ljava/lang/String
 *
 * @author Chong Wei Chin
 */
JNIEXPORT jstring JNICALL 
Java_com_axi_util_ViTroxLicenseUtil_nativeGetDllVersion(JNIEnv* pEnv, jobject obj)
{
  try
  {
	char customCString[50];
	GetDLLVersion(customCString);
	return pEnv->NewStringUTF(customCString);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
	  throwVitroxLicenseException(pEnv);
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

