package com.axi.util;

import java.io.*;

/**
 * Unit test for Pair.
 *
 * @author Matt Wharton
 */
public class Test_Pair extends UnitTest
{
  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  private Test_Pair()
  {
    // Do nothing...
  }

  /**
   * @author Matt Wharton
   */
  private void testAsserts()
  {
    // Make sure the copy constructor asserts when passed a null reference.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        new Pair<Integer, Integer>((Pair<Integer, Integer>)null);
      }
    });
  }

  /**
   * @author Matt Wharton
   */
  private void testHappyPath()
  {
    // Test the default constructor.
    Pair<Integer, String> pair1 = new Pair<Integer, String>();
    Expect.expect(pair1.getFirst() == null);
    Expect.expect(pair1.getSecond() == null);

    // Test the various setters.
    pair1.setFirst(1);
    pair1.setSecond("one");
    Expect.expect(pair1.getFirst() == 1);
    Expect.expect(pair1.getSecond().equals("one"));

    pair1.setPair(66, "order");
    Expect.expect(pair1.getFirst() == 66);
    Expect.expect(pair1.getSecond().equals("order"));

    pair1.setPair(new Pair<Integer, String>(0, "zero"));
    Expect.expect(pair1.getFirst() == 0);
    Expect.expect(pair1.getSecond().equals("zero"));

    // Test the constructor which takes the raw pair values.
    Pair<Integer, String> pair2 = new Pair<Integer, String>(2, "two");
    Expect.expect(pair2.getFirst() == 2);
    Expect.expect(pair2.getSecond().equals("two"));

    // Test the copy constructor.
    Pair<Integer, String> pair3 = new Pair<Integer, String>(new Pair<Integer, String>(3, "three"));
    Expect.expect(pair3.getFirst() == 3);
    Expect.expect(pair3.getSecond().equals("three"));
  }

  /**
   * @author Matt Wharton
   */
  private void testEqualsAndHashcode()
  {
    Pair<Integer, String> pair1 = new Pair<Integer, String>(1, "one");
    Pair<Integer, String> pair2 = new Pair<Integer, String>(1, "one");
    Pair<Integer, String> pair3 = new Pair<Integer, String>(2, "two");

    Expect.expect(pair1.equals(pair1));
    Expect.expect(pair1.equals(pair2));
    Expect.expect(pair1.equals(pair3) == false);

    Expect.expect(pair1.hashCode() == pair2.hashCode());
  }

  /**
   * Main unit test method.
   *
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    // Test that asserts are raised in the right circumstances.
    testAsserts();

    // Test the "happy path".
    testHappyPath();

    // Test the equals and hashcode methods.
    testEqualsAndHashcode();
  }

  /**
   * Unit test entry point.
   *
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Pair());
  }
}
