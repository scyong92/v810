package com.axi.util;

import java.io.*;
import java.security.*;
import java.util.*;

/**
 * @author Chong, Wei Chin
 */
public class ViTroxLicenseUtil
{
  static private ViTroxLicenseUtil _instance = null;

  private String _serverIP = "127.0.0.1";
//  private final String _dllVersion = "2.0.2";
  
  private final int _LICENSE_OK = 0;
  private final int _LICENSE_NO_LICENSE = 1;
  private final int _LICENSE_MAX_USER_EXCEED = 2;
  private final int _LICENSE_INVALID_HWID = 3;
  private final int _LICENSE_EXPIRED = 4;
  private final int _LICENSE_ERROR = 5;
  private final int _LICENSE_INVALID_CLIENT_HWID = 6;
  private final int _LICENSE_INVALID_VERSION_UNIQUE_KEY = 7;

  private final String _CHALLENGE_CODE = "ViTroXv810" ;
  private static final char[] _symbols = new char[26];
  private Random _random = new Random();

  public static final String _SEPERATOR = "[##]";
  
  static
  {
    try
    {
      System.loadLibrary("nativeAxiUtil");  
    }
    catch(UnsatisfiedLinkError e)
    {
      Assert.expect(false, e.getMessage());
    }

    for (int idx = 0; idx < 26; ++idx)
      _symbols[idx] = (char) ('A' + idx);
  }

  /**
   * @return ViTroxLicenseUtil ViTroxLicenseUtil instance
   * @author Chong, Wei Chin
   */
  public static ViTroxLicenseUtil getInstance() throws VitroxLicenseException
  {
    if(_instance == null)
      _instance = new ViTroxLicenseUtil();

    return _instance;
  }

  /**
   * @author Chong, Wei Chin
   */
  private ViTroxLicenseUtil() throws VitroxLicenseException
  {
    try
    {
      init();
    }
    catch(NoSuchAlgorithmException nsae)
    {
      throw VitroxLicenseException.getLicenseFailedToInitializeException(nsae.getMessage());
    }
    catch(UnsupportedEncodingException usee)
    {
      throw VitroxLicenseException.getLicenseFailedToInitializeException(usee.getMessage());
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  private void init() throws NoSuchAlgorithmException, UnsupportedEncodingException, VitroxLicenseException
  {
    String myChallenge = null;
    try
    {
      myChallenge = new String(_CHALLENGE_CODE.getBytes("UTF8"));
      myChallenge = _symbols[_random.nextInt(26)] + myChallenge + _symbols[_random.nextInt(26)];
    }
    catch (UnsupportedEncodingException ex)
    {
      myChallenge = _CHALLENGE_CODE;
    }
    String expectedResult = null;
    
    expectedResult = getHashWithHEX(myChallenge, "SHA-512");

    // challenge the dll
    String output = nativeChallengeMeManage(myChallenge);
//    String currentDLLVersion = nativeGetDllVersion();
    if(expectedResult.equals(output) == false) //
//       || currentDLLVersion.equals(_dllVersion) == false)
    {
      throw VitroxLicenseException.getLicenseFailedToInitializeException("Challenge Code Fail! \nexpectedResult : " + expectedResult + "\noutput : " + output);
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  public void setServerIP(String serverIPAddress)
  {
    Assert.expect(serverIPAddress != null);

    _serverIP = serverIPAddress;
  }

  /**
   * @author Chong, Wei Chin
   */
  public String checkLicenseStatus(int productID, String versionString) throws VitroxLicenseException
  {
    int status = -1;
    String statusString = nativeGetServerModeLicenseStatus2(_serverIP, productID, versionString);

    if(statusString.length() > 3)
    {
      status = _LICENSE_OK;
    }
    else
    {
      try
      {
        status = Integer.parseInt(statusString);
      }
      catch(NumberFormatException nfe)
      {
        status = _LICENSE_ERROR;
      }
    }
    
    if(status == _LICENSE_OK)
    {
      return statusString;
    }
    else if(status == _LICENSE_NO_LICENSE)
    {
      throw VitroxLicenseException.getNoLicenseException();
    }
    else if(status == _LICENSE_MAX_USER_EXCEED)
    {
      throw VitroxLicenseException.getLicenseExceedMaxUserException("Exceed Number Of User");
    }
    else if(status == _LICENSE_INVALID_HWID)
    {
      throw VitroxLicenseException.getInvalidLicenseException("Invalid Hardware ID");
    }
    else if(status == _LICENSE_EXPIRED)
    {
      throw VitroxLicenseException.getLicenseExpiredException("License Had Expired!");
    }
    else if(status == _LICENSE_ERROR)
    {
       throw VitroxLicenseException.getInvalidLicenseException("");
    }
    else if(status == _LICENSE_INVALID_CLIENT_HWID)
    {
      throw VitroxLicenseException.getInvalidLicenseException("Invalid Client");
    }
    else if(status == _LICENSE_INVALID_VERSION_UNIQUE_KEY)
    {
      throw VitroxLicenseException.getInvalidLicenseException("Invalid Version");
    }
    else
    {
      throw VitroxLicenseException.getInvalidLicenseException("License Currupted");
    }
  }

  /**
   * @param message
   * @param algorithm
   * @return
   * 
   * @author Chong, Wei Chin
   */
  public String getHashWithHEX(String message, String algorithm) throws UnsupportedEncodingException, NoSuchAlgorithmException
  {
    byte[] buffer = message.getBytes();
    MessageDigest md = MessageDigest.getInstance(algorithm);
    md.update(buffer);
    String digestString = new String(md.digest(), "ISO-8859-1");
    String hexValue = StringUtil.convertUnicodeStringIntoHexCodedString(digestString, '-', true);
    return hexValue;
  }

  native String nativeGetServerModeLicenseStatus(String serverip, int ProductID) throws VitroxLicenseException; 
  native String nativeGetServerModeLicenseStatus2(String serverip, int ProductID, String versionString) throws VitroxLicenseException;
  native String nativeChallengeMe(String strMessage) throws VitroxLicenseException;
  native String nativeChallengeMeManage(String strMessage) throws VitroxLicenseException;
  native String nativeGetDllVersion() throws VitroxLicenseException;
}
