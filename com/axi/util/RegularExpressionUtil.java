package com.axi.util;

import java.util.regex.*;

/**
 * This class provides validation for regular expressions.  NOTE for AXI do NOT use this class,
 * use com.axi.xRayTest.util.RegularExpressionUtilAxi instead.  It is a wrapper for
 * this class that converts all the exceptions thrown here into DatastoreExceptions
 * If you add a method to this class MAKE SURE TO ADD IT to RegularExpressionUtilAxi also.
 *
 * @author Laura Cormos
 */
public class RegularExpressionUtil
{
  public static final String FLOATING_POINT_PATTERN_STRING = "[-+]?(?:[0-9]*\\.)?[0-9]+";
  public static final String SCIENTIFIC_FLOATING_POINT_PATTERN_STRING = "[-+]?(?:[0-9]*\\.)?[0-9]+(?:[eE][-+]?[0-9]+)?";

  /**
   * Validate a regular expression and return a boolean
   * @author Laura Cormos
   */
  public static boolean isRegExValid(String regExPattern)
  {
    Assert.expect(regExPattern != null);

    boolean valid = true;
    try
    {
      Pattern.compile(regExPattern);
    }
    catch (PatternSyntaxException ex)
    {
      valid = false;
    }

    return valid;
  }


  /**
   * Validate a regular expression and throw an exception with more information on why this regEx is not valid
   * @author Laura Cormos
   */
  public static void checkRegExIsValid(String regExPattern) throws InvalidRegularExpressionException
  {
    Assert.expect(regExPattern != null);

    // now check the validity of the regular expression
    try
    {
      Pattern.compile(regExPattern);
    }
    catch(PatternSyntaxException pse)
    {
      InvalidRegularExpressionException ire = new InvalidRegularExpressionException(pse);
      ire.initCause(pse);
      throw ire;
    }
  }

}
