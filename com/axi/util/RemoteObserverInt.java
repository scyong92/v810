package com.axi.util;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * RemoteObserverInt provides the same functionality as java.util.Observer
 * except that it supports the Observer/Observable design pattern through
 * RMI calls.  Simply implement any class that you want to be an Observer
 * class with this interface.
 * @see java.util.Observer
 * @author Bill Darbie
 */
public interface RemoteObserverInt extends Remote
{
  /**
   * Any class that is an Observer MUST provide this method.  Note that if your
   * class uses Swing you MUST call all swing events on the Swing/awt event dispatch thread.
   * If your code does not use Swing you still need to make all your calls
   * happen on a new thread to prevent a deadlock situation.  You also need to
   * make your update method synchronized since it can get called
   * asynchronosly by other threads (because of RMI).
   * Here is the sample code that you should use for a client that calls Swing
   * components:
   * <PRE>
     public synchronized void update(RemoteObservableInt ro, Object arg)
     {
       // calling of Swing components MUST be done on the event
       // dispatching thread if we do not want nasty things like
       // deadlock - invokeLater is just what we need to do this
       SwingUtils.invokeLater(new Runnable()
       {
         public void run()
         {
           // this is a private method you provide that does the work
           // on swing components
           updateOnEventDispatchingThread();
         }
       });
     }
     </PRE>
   * @author Bill Darbie
   */
  void update(RemoteObservableInt ro, Object arg) throws RemoteException;
}
