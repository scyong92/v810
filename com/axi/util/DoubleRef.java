package com.axi.util;

import java.io.*;

/**
* DoubleRef allows doubles to be passed by reference.  A DoubleRef
* can be passed to a method and have its value changed by the method that
* it was passed into.
* This class is needed because the java.lang.Double class does not provide
* a set method.
*
* @author Bill Darbie
*/
public class DoubleRef implements Serializable
{
  private double _value;

  /**
   * @author Bill Darbie
   */
  public DoubleRef()
  {
    _value = 0.0;
  }

  /**
   * @author Bill Darbie
   */
  public DoubleRef(double value)
  {
    _value = value;
  }

  /**
   * @author Bill Darbie
   */
  public DoubleRef(DoubleRef doubleRef)
  {
    Assert.expect(doubleRef != null);
    _value = doubleRef._value;
  }

  /**
   * @author Bill Darbie
   */
  public void setValue(double value)
  {
    _value = value;
  }

  /**
   * @author Bill Darbie
   */
  public double getValue()
  {
    return _value;
  }

  /**
   * @author Bill Darbie
   */
  public boolean equals(Object rhs)
  {
    if (rhs == this)
      return true;

    if (rhs instanceof DoubleRef)
    {
      DoubleRef doubleRef = (DoubleRef)rhs;
      if (_value == doubleRef._value)
        return true;
    }

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public int hashCode()
  {
    Double dbl = new Double(_value);

    return dbl.hashCode();
  }

  /**
   * Returns true if the values are equal enough. This was ported from
   * util/src/Double.cpp where Bill Darbie had implemented this.
   * @author George A. David
   */
  public boolean fuzzyEquals(DoubleRef rhs)
  {
    Assert.expect(rhs != null);
    return MathUtil.fuzzyEquals(_value, rhs.getValue());
  }
}
