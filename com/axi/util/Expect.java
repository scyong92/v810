package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * The Expect class is very similar to the Assert class.  It will check
 * to see if a condition in your code is true.  If it is true it does
 * nothing.  If the condition is false it will print out information
 * about the failure to standard error and the program will continue
 * execution.  No log file is used.  This class is useful for test code.
 * It is not intended to be used for customer code. Use Assert for
 * customer code.
 * @author Bill Darbie
 */
public class Expect
{
  /**
   * see expect(condition, message)
   */
  public static void expect(boolean condition)
  {
    expect(condition, "");
  }

  /**
   * If condtion is true this method does nothing, if condition
   * is false this method will print to standard error the message
   * passed in and the stack trace.
   */
  public static void expect(boolean condition, String message)
  {
    if (condition == false)
    {
      AssertException assertException = new AssertException(message);

      System.err.println("####  Expect.expect() failed ####");
      assertException.printStackTrace();
    }
  }

  /**
   * Runs the specified RunnableWithExceptions and checks to see if the specified exception is thrown.
   *
   * @param executableCode The code to execute.
   * @param expectedExceptionClass A Class object representing the type of exception we're expecting.
   * @author Matt Wharton
   */
  public static void expectException(RunnableWithExceptions executableCode, Class expectedExceptionClass)
  {
    // The executable code Runnable can't be null.
    if (executableCode == null)
    {
      expect(false, "executableCode is null!");
      return;
    }

    // If the exception class is not a 'Throwable', we have a problem.
    if (expectedExceptionClass.isInstance(new Throwable()))
    {
      expect(false, "Specified exception class <" + expectedExceptionClass.getSimpleName() + "> is not a Throwable!");
      return;
    }

    try
    {
      executableCode.run();
      expect(false, "No exceptions were thrown!");
      return;
    }
    catch (Throwable caughtException)
    {
      if (caughtException.getClass() == expectedExceptionClass)
      {
        // Yay!  Got our exception!
        return;
      }
      else
      {
        // <HandWave>This is not the exception you're looking for...</HandWave>
        StringBuilder errorMessage = new StringBuilder();
        errorMessage.append("Expected exception <");
        errorMessage.append(expectedExceptionClass.getSimpleName());
        errorMessage.append(">, but got <");
        errorMessage.append(caughtException.getClass().getSimpleName());
        errorMessage.append(">.");
        expect(false, errorMessage.toString());
        return;
      }
    }
  }

  /**
   * Executes the specified RunnableWithExceptions and checks to see if an AssertException is thrown.
   *
   * @param executableCode The code to execute.
   * @author Matt Wharton
   */
  public static void expectAssert(RunnableWithExceptions executableCode)
  {
    expectException(executableCode, AssertException.class);
  }
}


