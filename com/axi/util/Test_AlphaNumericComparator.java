package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * Test class for AlphaNumericComparator
 *
 * @author Matt Wharton
 */

public class Test_AlphaNumericComparator extends UnitTest
{

  /**
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute( new Test_AlphaNumericComparator() );
  }

  /**
   * @author Matt Wharton
   */
  public void test( BufferedReader is, PrintWriter os )
  {
    String[] unsortedListElements = { "u1", "1", "11", "4_sdf", "u11", "u2", "2", "_", "", "u1_a", "u1_1", "u1_2",
                                      "u1_11", "u", "17.1f", "16d" };
    String[] sortedListElements = { "", "1", "2", "4_sdf", "11", "16d", "17.1f", "u", "u1", "u1_1", "u1_2", "u1_11", "u1_a", "u2",
                                    "u11", "_" };

    List<String> list = new ArrayList<String>();

    for ( int i = 0; i < unsortedListElements.length; i++ )
      list.add( unsortedListElements[i] );

    Collections.sort( list, new AlphaNumericComparator() );

    int i = 0;
    for (String currentElement: list)
    {
      Expect.expect( currentElement.equals( sortedListElements[i++] ) );
    }
  }
}
