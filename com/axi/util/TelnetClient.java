package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * This class provides an interface to handle common Telnet client control.
 * All the control is managed in teh background, not through a console or graphical user interface.
 * The main method defined here are for login, logout, sending commands or requests to the Telnet server,
 * and acquiring the responses from a Telnet server.
 * The reason why only limited Telnet functionalities are provided here is
 * becuase the commands or requests that the Telnet user (client) sends heavily depends
 * on what the remote system, in which a Telnet server is running, is capable of.
 *
 * When the user of this class constructs a Telnet client, the user must know the followings:
 *  1) format of login prompt for the user name (i.e. "Login name:")
 *  2) format of login prompt for the password
 *  3) format of a command prompt
 * The prompt information allows the client to know when to send out a request by determining the proper
 * prompt from the server.
 *
 * @author Eugene Kim-Leighton
 */
public class TelnetClient
{
  private static final int    _TELNET_PORT            = 23;

  private static final String _COLON_STRING           = ":";
  private static final String _GREATER_BRACKET_STRING = ">";
  private static final String _SPACE_STRING           = " ";

  private static final String _LOGOUT                 = "exit";
  private static final String _ERROR                  = "error";
  private static final String _DENIED                 = "denied";
  private static final String _INCORRECT              = "incorrect";
  private static final String _UNKNOWN_COMMAND        = "Unknown";
  private static final String _TERMINATED             = "Terminated.";
  private static final String _END_OF_STRING          = "\r\n";

  private String              _userNameLoginPrompt   = null;
  private String              _passwordLoginPrompt   = null;
  private String              _commandPrompt         = null;

  private SocketClient        _socketClient          = null;
  private String _ipAddress = "";

  /**
   * @param ipAddress is the address of the Telnet server that the client will connect to
   *
   * @author Eugene Kim-Leighton
   */
  public TelnetClient(String ipAddress)
  {
    this(ipAddress, _COLON_STRING, _COLON_STRING, _GREATER_BRACKET_STRING);
    Assert.expect(ipAddress != null);
    _ipAddress = ipAddress;
  }

  /**
   * @param ipAddress is the address of the Telnet server to connect
   * @param userNameLoginPrompt is the known prompt for the user name
   * @param passwordLoginPrompt is the known prompt for the password
   * @param commandPrompt is the known prompt for any commands
   *
   * @author Eugene Kim-Leighton
   */
  public TelnetClient(String ipAddress,
                      String userNameLoginPrompt,
                      String passwordLoginPrompt,
                      String commandPrompt)
  {
    Assert.expect(ipAddress != null);
    Assert.expect(userNameLoginPrompt != null);
    Assert.expect(passwordLoginPrompt != null);
    Assert.expect(commandPrompt != null);

    _userNameLoginPrompt = userNameLoginPrompt;
    _passwordLoginPrompt = passwordLoginPrompt;
    _commandPrompt       = commandPrompt;

    _socketClient        = new SocketClient(ipAddress, _TELNET_PORT);
    _ipAddress = ipAddress;
  }

  /**
   * This function checks whether or not there the error message from the list of reponses that Telnet server sends
   * @throws SocketMessageInvalidException if the Telnet server's response indicates that
   *                                      the Telnet client's request is not valid for server's system.
   * @throws SocketMessageFailedException if the Telnet server's response indicates that
   *                                      there was an error perfoming the Telnet client's request.
   * @author Eugene Kim-Leighton
   */
  private void determineRepliesAreValid(List<String> list) throws SocketMessageInvalidException, SocketMessageFailedException
  {
    Assert.expect(list != null);

    for (String reply : list)
    {
      StringTokenizer tokens = new StringTokenizer (reply);
      while(tokens.hasMoreTokens())
      {
        String token = tokens.nextToken();
        if(token.equalsIgnoreCase(_ERROR) || token.equalsIgnoreCase(_DENIED) || token.equalsIgnoreCase(_INCORRECT))
          throw new SocketMessageFailedException(reply);

        if(token.equalsIgnoreCase(_UNKNOWN_COMMAND))
          throw new SocketMessageInvalidException(reply);
      }
    }
  }

  /**
   * To login to a Telnet server, the client needs to wait for the login prompt with welcome message from the server
   * after connection is made to ensure that there is no error occured while connection is established.
   * Then the client sends out the login user name and waits for the password prompt.
   * The client will make sure it receives the command prompt after sending out the password to make sure that
   * the login process has succeeded.
   *
   * @param userName
   * @param password
   * @return true if the login process has succeeded.
   *
   * @author Eugene Kim-Leighton
   */
  public boolean login(String userName, String password)
  {
    Assert.expect(userName != null);
    Assert.expect(password != null);

    boolean loginPassed = true;
    try
    {
      _socketClient.connect();
      boolean matchEntireLine = true;
      boolean isHandshake = false;
      receiveReplies(_userNameLoginPrompt, matchEntireLine, isHandshake);
      String request = userName + _END_OF_STRING;

      sendRequestAndReceiveReplies(request, _passwordLoginPrompt, matchEntireLine, isHandshake);
      request = password + _END_OF_STRING;

      sendRequestAndReceiveReplies(request, _commandPrompt, matchEntireLine, isHandshake);
    }
    catch (IOException ioe)
    {
      loginPassed = false;
    }
    catch(SocketMessageFailedException smfe)
    {
      loginPassed = false;
    }
    catch(SocketMessageInvalidException smie)
    {
      loginPassed = false;
    }

    return loginPassed;
  }

  /**
   * This sends out the reuest for logging out, waits for the response, that is "Connection Terminated." from the server,
   * and close the connection from the Telnet server.
   *
   * @throws SocketMessageFailedException if there is an error
   *                                      for completing logout request in either server or client side.
   * @author Eugene Kim-Leighton
   */
  public void logout() throws SocketMessageFailedException
  {
    try
    {
      String request = _LOGOUT + _END_OF_STRING;
      boolean matchEntireLine = false;
      boolean isHandshake = false;
      _socketClient.disconnectFromServer(request, _TERMINATED, matchEntireLine, isHandshake);
    }
    catch(IOException ioe)
    {
      SocketMessageFailedException ex = new SocketMessageFailedException(ioe.getMessage());
      ex.initCause(ioe);
      throw ex;
    }
  }

  /**
   * This function sends out the request and receives the list of replies from the Telnet server.
   * Also this checks whether or not an error condition is contained in the reply.
   *
   * @param request is a client's request to the server. It should not be a null.
   * @param endOfRepliesIndicator is a string that client is expecting to receive at the end of each server's reply.
   *                            This allows the client know when to stop receiving the respose from server.
   * @param matchEntireLine is the flag indicating whether the client should ensure that
   *                           "endOfReplyIndicator" is received with an exact match.
   *                           Otherwise, the client will only look for "endOfReplyIndicator" and ignore the rest of the
   *                           server's reply.
   *
   * @throws SocketMessageInvalidException if the Telnet server's response indicates that
   *                                      the Telnet client's request is not valid for server's system.
   * @throws SocketMessageFailedException if the Telnet server's response indicates that
   *                                      there was an error perfoming the Telnet client's request.
   *
   * @author Eugene Kim-Leighton
   */
  public void sendRequestAndReceiveReplies(String request, String endOfRepliesIndicator, boolean matchEntireLine, boolean isHandshake)
         throws SocketMessageInvalidException, SocketMessageFailedException
  {
    try
    {
      List<String> list = _socketClient.sendRequestAndReceiveReplies(request, endOfRepliesIndicator, matchEntireLine, isHandshake);
      determineRepliesAreValid(list);
    }
    catch(IOException ioe)
    {
      SocketMessageFailedException ex = new SocketMessageFailedException(ioe.getMessage());
      ex.initCause(ioe);
      throw ex;
    }
  }

  /**
   * This function receives the replies from the Telnet server
   * and checks whether or not there is a negative replies before logging in.
   * If the Telnet server doesn't send out login prompt
   * then there is either a problem with server or the connection between the client and server.
   *
   * @param endOfReplyIndicator is a string that client is expecting to receive at the end of each server's reply.
   *                            This allows the client know when to stop receiving the respose from server.
   * @param matchEntireLine is the flag indicating whether the client should ensure that
   *                           "endOfReplyIndicator" is received with an exact match.
   *                           Otherwise, the client will only look for "endOfReplyIndicator" and ignore the rest of the
   *                           server's reply.
   *
   * @throws SocketConnectionFailedException if there is an error estblishing the connection
   *                                         before logging in to the Telnet server
   *
   * @author Eugene Kim-Leighton
   */
  private void receiveReplies(String endOfRepliesIndicator, boolean matchEntireLine, boolean isHandshake)
         throws SocketConnectionFailedException
  {
    try
    {
      _socketClient.receiveReplies(endOfRepliesIndicator, matchEntireLine, isHandshake);
    }
    catch(IOException ioe)
    {
      SocketConnectionFailedException ex = new SocketConnectionFailedException(_ipAddress, _TELNET_PORT, ioe.getMessage());
      ex.initCause(ioe);
      throw ex;
    }
  }
}
