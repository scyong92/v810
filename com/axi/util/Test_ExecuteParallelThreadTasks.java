package com.axi.util;

import java.io.*;
import java.util.concurrent.*;

/**
 * @author Bill Darbie
 * @author Reid Hayhow
 */
public class Test_ExecuteParallelThreadTasks extends UnitTest
{

  private ExecuteParallelThreadTasks<Integer> _tasks = new ExecuteParallelThreadTasks<Integer>();
  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ExecuteParallelThreadTasks());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {

    testRunWithNoExceptions();
    testRunWithExceptions();
    testRunWithExceptionsAfterSleep();
    testCancel();
  }

  /**
   * @author Bill Darbie
   */
  private void testRunWithNoExceptions()
  {
    System.out.println("\nrun three tasks in parallel");
    Future<Integer> future1 = _tasks.submitThreadTask(new IntegerThreadTask("1", 1, false));
    Future<Integer> future2 = _tasks.submitThreadTask(new IntegerThreadTask("2", 2, false));
    Future<Integer> future3 = _tasks.submitThreadTask(new IntegerThreadTask("3", 3, false));

    try
    {
      _tasks.waitForTasksToComplete();
    }
    catch (Exception ex)
    {
      System.out.println(ex.getMessage());
    }
    System.out.println("DONE runing three tasks in parallel");

    System.out.println("test that we still get the results of each task");
    try
    {
      System.out.println(future1.get());
      System.out.println(future2.get());
      System.out.println(future3.get());
    }
    catch (Exception ex)
    {
      System.out.println(ex.getMessage());
    }
  }

  /**
   * @author Bill Darbie
   */
  private void testRunWithExceptions()
  {
    System.out.println("\nrun three tasks in parallel with 2 exceptions");
    Future<Integer> future1 = _tasks.submitThreadTask(new IntegerThreadTask("1", 1, false));
    Future<Integer> future2 = _tasks.submitThreadTask(new IntegerThreadTask("2", 2, true));
    Future<Integer> future3 = _tasks.submitThreadTask(new IntegerThreadTask("3", 3, true));

    try
    {
      _tasks.waitForTasksToComplete();
      Assert.expect(false);
    }
    catch (Exception ex)
    {
      System.out.println(ex.getMessage());
    }
    System.out.println("DONE runing three tasks in parallel");

    System.out.println("test that we still get the results of each task");
    try
    {
      System.out.println(future1.get());
    }
    catch (Exception ex)
    {
      Expect.expect(false);
    }

    try
    {
      future2.get();
      Expect.expect(false);
    }
    catch (Exception ex)
    {
      System.out.println(ex.getMessage());
    }

    try
    {
      future3.get();
      Expect.expect(false);
    }
    catch (Exception ex)
    {
      System.out.println(ex.getMessage());
    }
  }

  /**
   * @author Reid Hayhow
   */
  private void testRunWithExceptionsAfterSleep()
  {
    System.out.println("\nrun two tasks in parallel with 2 exception after sleep");
    Future<Integer> future1 = _tasks.submitThreadTask(new IntegerThreadTask("1", 1, true));
    Future<Integer> future2 = _tasks.submitThreadTask(new IntegerThreadTask("2", 2, true));

    try
    {
      Thread.currentThread().sleep(3000);
    }
    catch (InterruptedException ex1)
    {
      //do nothing
    }

    try
    {
      _tasks.waitForTasksToComplete();
      Expect.expect(false);
    }
    catch (Exception ex)
    {
      System.out.println(ex.getMessage());
    }
    System.out.println("DONE runing two tasks in parallel with exceptions");

    System.out.println("test that we still get the results of each task");
    try
    {
      System.out.println(future1.get());
      Expect.expect(false);
    }
    catch (Exception ex)
    {
      System.out.println(ex.getMessage());
    }

    try
    {
      System.out.println(future2.get());
      Expect.expect(false);
    }
    catch (Exception ex)
    {
      System.out.println(ex.getMessage());
    }
  }


  /**
   * @author Bill Darbie
   */
  private void testCancel()
  {
    ExecuteParallelThreadTasks<Integer> tasks = new ExecuteParallelThreadTasks<Integer>();

    System.out.println("\nrun three tasks in parallel and cancel");
    Future<Integer> future1 = tasks.submitThreadTask(new IntegerThreadTask("1", 1, false));
    Future<Integer> future2 = tasks.submitThreadTask(new IntegerThreadTask("2", 2, false));
    Future<Integer> future3 = tasks.submitThreadTask(new IntegerThreadTask("3", 3, false));

    try
    {
      tasks.cancel();
      tasks.waitForTasksToComplete();
    }
    catch (Exception ex)
    {
      System.out.println(ex.getMessage());
    }
    System.out.println("DONE runing three tasks in parallel");

    System.out.println("test that we still get the results of each task");
    try
    {
      System.out.println(future1.get());
      System.out.println(future2.get());
      System.out.println(future3.get());
    }
    catch (Exception ex)
    {
      System.out.println(ex.getMessage());
    }
  }
}

/**
 * @author Bill Darbie
 */
class IntegerThreadTask extends ThreadTask<Integer>
{
  private int _i;
  private boolean _exception;
  private boolean _cancel = false;

  /**
   * @author Bill Darbie
   */
  IntegerThreadTask(String name, int i, boolean exception)
  {
    super(name);
    _i = i;
    _exception = exception;
  }

  /**
   * @author Bill Darbie
   */
  protected Integer executeTask() throws Exception
  {
    Thread.sleep(_i * 1000);

    if (_cancel)
    {
      _i = 0;
      return _i;
    }

    if (_exception)
      throw new Exception("Exception for task: " + _i);

    System.out.println(_i);

    return _i;
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel()
  {
    _cancel = true;
  }

  /**
   * @author Bill Darbie
   */
  protected void clearCancel()
  {
    _cancel = false;
  }
}


