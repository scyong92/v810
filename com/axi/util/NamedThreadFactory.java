package com.axi.util;

import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

/**
 * An implemntation of ThreadFactory which requires the factory to be named.  This allows for cleaner
 * generated thread names.
 *
 * @author Matt Wharton
 */
public class NamedThreadFactory implements ThreadFactory
{
  private AtomicInteger _threadNumber = new AtomicInteger(1);
  private String _threadFactoryName;

  /**
   * @author Matt Wharton
   */
  public NamedThreadFactory(String threadFactoryName)
  {
    Assert.expect(threadFactoryName != null);

    _threadFactoryName = threadFactoryName;
  }

  /**
   * Constructs a new <tt>Thread</tt>.
   *
   * @param runnable a Runnable to be executed by new thread instance
   * @return constructed thread

   * @author Matt Wharton
   */
  public Thread newThread(Runnable runnable)
  {
    Thread thread = new Thread(runnable, _threadFactoryName + " - thread " + _threadNumber.getAndIncrement());

    if (thread.isDaemon())
        thread.setDaemon(false);
    if (thread.getPriority() != Thread.NORM_PRIORITY)
        thread.setPriority(Thread.NORM_PRIORITY);

    return thread;
  }
}
