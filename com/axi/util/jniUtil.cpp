#ifdef _MSC_VER
  #pragma warning (disable:4530)
  #pragma warning (disable:4786)
#endif

#include <assert.h>
#include <iostream>
#include <jni.h>
#include <string>
#include <vector>

#include "util/src/sptAssert.h"

#include "jniUtil.h"

using namespace std;

/**
* Given a reference to a java doubleRef object, this method will call its
* setDouble method to set the doubleRefs value to whatever is passed in through
* doubleVal
*
* @param pEnv is a pointer to the JNI environment
* @param obj
* @param doubleRef is the reference to the Java DoubleRef object to be set by calling its setDouble() method
* @param doubleVal is the value to set doubleRef to
* @author Bill Darbie
*/
void doubleRefSetValue(JNIEnv* pEnv,
                       jobject obj,
                       jobject doubleRef,
                       double  doubleVal)
{
  // call DoubleRef.setValue(double)
  sptAssert(pEnv);
  jclass cls = pEnv->GetObjectClass(doubleRef);

  jmethodID mid = pEnv->GetMethodID(cls, "setValue", "(D)V");
  sptAssert(mid);

  pEnv->CallVoidMethod(doubleRef, mid, doubleVal);
}

/**
* Same as doubleRefSetValue except for Boolean.
* @author Bill Darbie
*/
void booleanRefSetValue(JNIEnv* pEnv,
                        jobject obj,
                        jobject booleanRef,
                        bool    booleanVal)
{
  // call BooleanRef.setValue(boolean)
  sptAssert(pEnv);
  jclass cls = pEnv->GetObjectClass(booleanRef);

  jmethodID mid = pEnv->GetMethodID(cls, "setValue", "(Z)V");
  sptAssert(mid);

  pEnv->CallVoidMethod(booleanRef, mid, booleanVal);
}

/**
* Same as doubleRefSetValue except for Boolean.
* @author Bill Darbie
*/
void integerRefSetValue(JNIEnv* pEnv,
                        jobject obj,
                        jobject integerRef,
                        int     integerVal)
{
  // call IntegerRef.setValue(boolean)
  sptAssert(pEnv);
  jclass cls = pEnv->GetObjectClass(integerRef);

  jmethodID mid = pEnv->GetMethodID(cls, "setValue", "(I)V");
  sptAssert(mid);

  pEnv->CallVoidMethod(integerRef, mid, integerVal);
}

/**
* Take a java string and return the equivalent STL string
* @author Bill Darbie
*/
string getSTLString(JNIEnv* pEnv, jstring javaString)
{
  sptAssert(pEnv);
  // convert the java string to a char*
  char const* pStr = pEnv->GetStringUTFChars(javaString, 0); 

  // now convert the char* to an STL string
  string stlString = pStr;

  // now free the memory for the c string
  pEnv->ReleaseStringUTFChars(javaString, pStr);

  return stlString;
}

/**
* Given a Java StringBuffer object, return an STL string from it.
* @author Bill Darbie
*/
string getSTLStringFromJavaStringBuffer(JNIEnv* pEnv, jobject jStringBuffer)
{
  sptAssert(pEnv);

  jclass cls = pEnv->GetObjectClass(jStringBuffer);
  jmethodID mid = pEnv->GetMethodID(cls, "toString", "()Ljava/lang/String;");
  sptAssert(mid);

  jstring javaString = (jstring)pEnv->CallObjectMethod(jStringBuffer, mid);
  string stlString = getSTLString(pEnv, javaString);

  return stlString;
}

/**
* Set a Java StringBuffer object to the contents of the passed in STL string.
* @author Bill Darbie
*/
void setJavaStringBuffer(JNIEnv* pEnv, jobject jStringBuffer, string const& stlString)
{
  sptAssert(pEnv);

  // create a jstring from stlString
  jstring jstr = pEnv->NewStringUTF(stlString.c_str());
  sptAssert(jstr);

  // call StringBuffer.replace() method
  jclass cls = pEnv->GetObjectClass(jStringBuffer);

  jmethodID mid = pEnv->GetMethodID(cls, "replace", "(IILjava/lang/String;)Ljava/lang/StringBuffer;");
  sptAssert(mid);

  pEnv->CallObjectMethod(jStringBuffer, mid, 0, stlString.size(), jstr);
}

/**
 * Create a java.util.ArrayList of String objects from an STL string list.
 * @author George A. David
 */
jobject createJavaUtilArrayList(JNIEnv* pEnv, list<string> const& stlStringList)
{
  sptAssert(pEnv);

  // find reference to the class
  jclass arrayListClass = pEnv->FindClass("java/util/ArrayList");
  sptAssert(arrayListClass);

  // get a reference to the constructor
  jmethodID constructorId = pEnv->GetMethodID(arrayListClass, "<init>", "()V");
  sptAssert(constructorId);

  // create the array list class
  jobject arrayList = pEnv->NewObject(arrayListClass, constructorId);
  sptAssert(arrayList);

  // get a reference to the ArrayList.add(Object object) method
  jmethodID addMethodId = pEnv->GetMethodID(arrayListClass, "add", "(Ljava/lang/Object;)Z");
  sptAssert(addMethodId);

  // add all strings to the list
  list<string>::const_iterator it;
  for(it = stlStringList.begin(); it != stlStringList.end(); ++it)
  {
    // create a java string
    jstring javaString = pEnv->NewStringUTF((*it).c_str());
    sptAssert(javaString);
    //add the java string to the array list class
    sptAssert(pEnv->CallBooleanMethod(arrayList, addMethodId, javaString));
  }

  return arrayList;
}

/*
* Load a STL vector<int> from a jintArray
* @author Dave Ferguson
*/
void loadSTLIntVectorFromJavaIntArray(JNIEnv* env, 
                                      jintArray arrayOfInts,
                                      vector<int>& vectorOfInts)
{
  sptAssert(env);
  sptAssert(arrayOfInts);

  jsize len = env->GetArrayLength(arrayOfInts);

  vectorOfInts.clear();
  vectorOfInts.resize(len);
  jint *p = env->GetIntArrayElements(arrayOfInts, 0);

  sptAssert(p);

  // STL copy
  copy(p,p+len, vectorOfInts.begin());

  env->ReleaseIntArrayElements(arrayOfInts, p, 0);

}

/*
* Load a STL vector<double> from a jdoubleArray
* @author Peter Esbensen
*/
void loadSTLDoubleVectorFromJavaDoubleArray(JNIEnv* env, 
                                            jdoubleArray arrayOfDoubles,
                                            vector<double>& vectorOfDoubles)
{
  sptAssert(env);
  sptAssert(arrayOfDoubles);

  jsize len = env->GetArrayLength(arrayOfDoubles);

  vectorOfDoubles.clear();
  vectorOfDoubles.resize(len);
  jdouble *p = env->GetDoubleArrayElements(arrayOfDoubles, 0);

  sptAssert(p);

  // STL copy
  copy(p,p+len, vectorOfDoubles.begin());

  env->ReleaseDoubleArrayElements(arrayOfDoubles, p, 0);
}


/*
* Load an STL vector<jobject> from a jobjectArray
*
* @author Peter Esbensen
*/
void loadSTLObjectVectorFromJavaObjectArray(JNIEnv* env, 
                                            jobjectArray arrayOfObjects,
                                            std::vector< jobject >& vectorOfJobjects)
{
  sptAssert(env);
  sptAssert(arrayOfObjects);

  jsize len = env->GetArrayLength(arrayOfObjects);

  vectorOfJobjects.clear();
  for (int i=0; i<len; ++i)
  {
    vectorOfJobjects.push_back(env->GetObjectArrayElement(arrayOfObjects, i));
  }
}

/*
* Create a jintArray containing the integers in the given STL vector
*
* @author Peter Esbensen
*/
jintArray createJavaIntArrayFromSTLVector(JNIEnv* env, 
                                          std::vector< int > const& vectorOfInts)
{
  sptAssert(env);
  int size = vectorOfInts.size();

  jintArray javaIntArray = env->NewIntArray(size);

  jint *body = env->GetIntArrayElements(javaIntArray, 0);
  for (int i=0; i<size; ++i)
    body[i] = vectorOfInts[i];
  env->ReleaseIntArrayElements(javaIntArray, body, 0);
  return javaIntArray;
}

//////////////////////////////////////////////////////////////////////////////
// Throw a C/C++ to Java JNI exception
//
// This function will allow a C/C++ JNI function to throw an exception back 
// up to the Java layer.
//
// Note: This function should rarely be used because virtually all Java 
//       exception handling will localize the message.  Only cases where,
//       for example, division-wide or company-wide Java utilities can be 
//       used, would this exception handler be necessary.  For example, 
//       com/axi/util/UsbUtil.java is a utility class that can be used
//       across company-wide software developers.  In this case, other software
//       architectures might not support localization so they would need the
//       "message" parameter for error messages, logging, etc.
//      
//
// @param pEnv is the environment pointer for active JNI call
// @param exceptionMessage is the message provided by the C/C++ client
// @param exceptionKey is the localization key that can be used by the Java
//                     layer to map to the localized message.
// @param exceptionParameters are the list of parameters that will be put
//        into the localized message if necessary.
// @param javaExceptionClassPackage defines which Java exception class to use.
//        For example, the user of this function would request the following
//        exception "com/axi/v810/hardware/PanelPositionerHardwareException"
// @author Greg Esparza
//////////////////////////////////////////////////////////////////////////////
void throwJniException(JNIEnv* pEnv, 
                       string const& exceptionMessage,
                       string const& exceptionKey,
                       list<string> const& exceptionParameters,
                       string javaExceptionClassPackage)
{
  sptAssert(pEnv);
  
  // create a message java string
  // Note that if the message is empty, just put in a dummy message so the JNI conversion will work correctly.
  jstring message = pEnv->NewStringUTF(exceptionMessage.c_str());
  sptAssert(message);

  // create a localization key java string
  jstring key = pEnv->NewStringUTF(exceptionKey.c_str());
  sptAssert(key);
  
  // create an array list from the list of paramameters.
  jobject arrayList = createJavaUtilArrayList(pEnv, exceptionParameters);
  sptAssert(arrayList);

  // find the requested class
  jclass exceptionClass = pEnv->FindClass(javaExceptionClassPackage.c_str());
  sptAssert(exceptionClass);
 
  // find the constructor
  jmethodID constructorId = pEnv->GetMethodID(exceptionClass, "<init>", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V");
  sptAssert(constructorId);
  
  // create the exception.
  jobject exception = pEnv->NewObject(exceptionClass, constructorId, message, key, arrayList);
  sptAssert(exception);
  
  // throw the exception.
  pEnv->Throw((jthrowable)exception);
}

//////////////////////////////////////////////////////////////////////////////
// Throw a C/C++ to Java JNI exception
//
// This function will allow a C/C++ JNI function to throw an exception back 
// up to the Java layer.
//
// Note: This function is the preferred JNI exception handler for Java
//       architectures that support localization.  Only the localization
//       key and list of parameters are needed for the Java architecture
//       to generate a localized message.
//      
//
// @param pEnv is the environment pointer for active JNI call
// @param exceptionKey is the localization key that can be used by the Java
//                     layer to map to the localized message.
// @param exceptionParameters are the list of parameters that will be put
//        into the localized message if necessary.
// @param javaExceptionClassPackage defines which Java exception class to use.
//        For example, the user of this function would request the following
//        exception "com/axi/v810/hardware/PanelPositionerHardwareException"
// @author Greg Esparza
//////////////////////////////////////////////////////////////////////////////
void throwJniException(JNIEnv* pEnv, 
                       string const& exceptionKey, 
                       list<string> const& exceptionParameters, 
                       string javaExceptionClassPackage)

{
  sptAssert(pEnv);
  
  // create a localization key java string
  jstring key = pEnv->NewStringUTF(exceptionKey.c_str());
  sptAssert(key);
  
  // create an array list from the list of paramameters.
  jobject arrayList = createJavaUtilArrayList(pEnv, exceptionParameters);
  sptAssert(arrayList);

  // find the requested class
  jclass exceptionClass = pEnv->FindClass(javaExceptionClassPackage.c_str());
  sptAssert(exceptionClass);
 
  // find the constructor
  jmethodID constructorId = pEnv->GetMethodID(exceptionClass, "<init>", "(Ljava/lang/String;Ljava/util/List;)V");
  sptAssert(constructorId);
  
  // create the exception.
  jobject exception = pEnv->NewObject(exceptionClass, constructorId, key, arrayList);
  sptAssert(exception);
  
  // throw the exception.
  pEnv->Throw((jthrowable)exception);
}

/*
* Create a jfloatArray containing the integers in the given STL vector
*
* @author Peter Esbensen
*/
jfloatArray createJavaFloatArrayFromSTLVector(JNIEnv* env, std::vector< float > const& vectorOfFloats)
{
  sptAssert(env);
  int size = vectorOfFloats.size();

  jfloatArray javaFloatArray = env->NewFloatArray(size);

  jfloat *body = env->GetFloatArrayElements(javaFloatArray, 0);
  for (int i=0; i<size; ++i)
    body[i] = vectorOfFloats[i];
  env->ReleaseFloatArrayElements(javaFloatArray, body, 0);
  return javaFloatArray;
}

/**
 * Checks to see if any exceptions have been thrown by java code.  This will simply re-throw the exception to your C++
 * code, so you better make sure that you have an appropriate catch block surrounding this.  One thing you may want to
 * do in your catch block is to simply clean up and then call env->Throw(javaException) to send the java exception back
 * to java.
 *
 * If you aren't calling this after making your c->java calls (or doing your own exception checking), you are headed for
 * trouble.  When an exception occurs in these situations (including Assertion failures), java will immediately return to your
 * C++ code without going to any java exception handlers.  It's up to you to handle the java exception properly.
 *
 * @author Peter Esbensen
 */
void checkForJavaException(JNIEnv* env)
{
  jthrowable exception = env->ExceptionOccurred();
  if (exception)
  {
    //env->ExceptionDescribe();
    env->ExceptionClear();
    throw exception;
  }
}

//////////////////////////////////////////////////////////////////////////////
// Load a Java String Array List from an STL list of strings
//
// Note: the Java String Array List must be allocated in Java, 
//       then passed down through the native interface where it will
//       be populated in this function.
//
// @author Greg Esparza
//////////////////////////////////////////////////////////////////////////////
void loadJavaStringArrayFromStlStringList(JNIEnv* pEnv, jobject javaArrayListToLoadTo, std::list<std::string> const& stlListToLoadFrom)
{
  sptAssert(pEnv);
  sptAssert(javaArrayListToLoadTo);

  // find reference to the class
  jclass arrayListClass = pEnv->FindClass("java/util/ArrayList");
  sptAssert(arrayListClass);


  // get a reference to the ArrayList.add(Object object) method
  jmethodID addMethodId = pEnv->GetMethodID(arrayListClass, "add", "(Ljava/lang/Object;)Z");
  sptAssert(addMethodId);

  // add all strings to the list
  list<string>::const_iterator it;
  for(it = stlListToLoadFrom.begin(); it != stlListToLoadFrom.end(); ++it)
  {
    // create a java string
    jstring javaString = pEnv->NewStringUTF((*it).c_str());
    sptAssert(javaString);
    //add the java string to the array list class
    sptAssert(pEnv->CallBooleanMethod(javaArrayListToLoadTo, addMethodId, javaString));
  }
}
