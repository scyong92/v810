package com.axi.util;

/**
 * This class is thrown when x-axis list and y-axis list have different sizes in XYPlotDialog
 *
 * @author Ying-Huan.Chu
 */
public class XYListSizeDifferentException extends LocalizedException
{
  /**
   * @param localizedString - the Localized String for the exception.
   * @author Ying-Huan.Chu
   */
  public XYListSizeDifferentException(LocalizedString localizedString)
  {
    super(localizedString);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public LocalizedString getLocalizedString()
  {
    return super.getLocalizedString();
  }
}
