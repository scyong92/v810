package com.axi.util;

/**
 * @author Bill Darbie
 */
public class CharacterEncodingNotSupportedException extends Exception
{
  private String _encodingString;

  /**
   * @author Bill Darbie
   */
  public CharacterEncodingNotSupportedException(String encodingString)
  {
    Assert.expect(encodingString != null);
    _encodingString = encodingString;
  }

  /**
   * @author Bill Darbie
   */
  public String getEncodingString()
  {
    Assert.expect(_encodingString != null);
    return _encodingString;
  }
}
