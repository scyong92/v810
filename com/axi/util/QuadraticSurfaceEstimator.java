package com.axi.util;

import Jama.Matrix;
import java.lang.Exception;

/**
 * <p>
 * Title: QuadraticSurfaceEstimator
 * </p>
 *
 * <p>
 * Description: Fit a bilinear, linear / quadratic, or quadratic surface to a set of {x, y, z} data points,
 * and use the fitted surface to estimate z at the original or other {x, y} locations.
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2008, Agilent Technologies, Inc. All rights reserved.
 * </p>
 *
 * @author John Heumann
 */
public class QuadraticSurfaceEstimator
{
  private QuadraticSurfaceEstimatorTypeEnum _surfaceType;
  private int _nCoefficients;
  private Matrix _A;             // The original system matrix
  private Matrix _coefficients;  // _A * _coefficients ~ _rhs (in a least-squares sense)

  /**
   * Constructor.
   * @author John Heumann
   */
  public QuadraticSurfaceEstimator(double x[], double y[], double z[],
                                   QuadraticSurfaceEstimatorTypeEnum surfaceType)
  {
    final int nSamples = x.length;
    Assert.expect(nSamples == y.length);
    Assert.expect(nSamples == z.length);

    _surfaceType = surfaceType;
    if (surfaceType == QuadraticSurfaceEstimatorTypeEnum.BILINEAR)
    {
      Assert.expect(nSamples >= 4);
      _nCoefficients = 4; // x, y, xy, and 1
    }
    else if (surfaceType == QuadraticSurfaceEstimatorTypeEnum.LINEAR_X_QUADRATIC_Y)
    {
      Assert.expect(nSamples >= 5);
      _nCoefficients = 5; // x, y, xy, y^2, and 1
    }
    else // (surfaceType == QuadraticSurfaceEstimatorTypeEnum.QUADRATIC)
    {
      Assert.expect(nSamples >= 6);
      _nCoefficients = 6; // x, y, xy, x^2, y^2, and 1
    }

    _A = initializeMatrix(nSamples, _nCoefficients, x, y);

    final Matrix rhs = new Matrix(z, nSamples);
    _coefficients = _A.solve(rhs);
  }

  /**
   *
   * Get the predicted z heights at the set of x, y points used for fitting the surface
   *
   * @author John Heumann
   */
  public double[] getEstimatedHeights()
  {
    Matrix zPredicted = _A.times(_coefficients);

    return zPredicted.getRowPackedCopy();
  }

  /**
   *
   * Get the predicted z heights at a given set of x, y points
   *
   * @author John Heumann
   */
  public double[] getEstimatedHeights(double x[], double y[])
  {
    Assert.expect(x.length == y.length);

    Matrix A = initializeMatrix(x.length, _nCoefficients, x, y);
    Matrix zPredicted = A.times(_coefficients);

    return zPredicted.getRowPackedCopy();
  }

  /**
   *
   * Get the predicted z heights at a given x, y point
   *
   * @author John Heumann
   */
  public double getEstimatedHeight(double x, double y)
  {
    double xm[] = new double[1];
    double ym[] = new double[1];
    xm[1] = x;
    ym[1] = y;
    Matrix A = initializeMatrix(xm.length, _nCoefficients, xm, ym);
    Matrix zPredicted = A.times(_coefficients);

    return zPredicted.getRowPackedCopy()[1];
  }

  /**
   * @author John Heumann
   */
 private Matrix initializeMatrix(int nRows, int nCols, double x[], double y[])
  {
    Matrix M = new Matrix(nRows, nCols);

    for (int i = 0; i < nRows; i++)
    {
      M.set(i, 0, x[i]);
      M.set(i, 1, y[i]);
      M.set(i, 2, x[i] * y[i]);
      if (_surfaceType == QuadraticSurfaceEstimatorTypeEnum.BILINEAR)
      {
        M.set(i, 3, 1.0);
      }
      else if (_surfaceType == QuadraticSurfaceEstimatorTypeEnum.LINEAR_X_QUADRATIC_Y)
      {
        M.set(i, 3, y[i] * y[i]);
        M.set(i, 4, 1.0);
      }
      else // (_surfaceType == QuadraticSurfaceEstimatorTypeEnum.QUADRATIC)
      {
        M.set(i, 3, x[i] * x[i]);
        M.set(i, 4, y[i] * y[i]);
        M.set(i, 5, 1.0);
      }
    }

    return M;
  }

}
