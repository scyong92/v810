package com.axi.util;

import java.io.*;
import java.math.*;
import java.util.*;

/**
 * This class performs a Unit Test on the StringUtil class.
 *
 * @author Keith Lee
 */
public class Test_StringUtil extends UnitTest
{
  private static final String _HTML_START_PRE_TAG = "<PRE>";
  private static final String _HTML_END_PRE_TAG = "</PRE>";

  private static final String _BAD_NUMBER = "35.A";

  private static final String _POSITIVE_DECIMAL = "35.0";
  private static final String _POSITIVE_INTEGER = "35";
  private static final double _POSITIVE_DOUBLE = 35;
  private static final int _POSITIVE_INT = 35;
  private static final long _POSITIVE_LONG = 35L;

  private static final String _NEGATIVE_DECIMAL = "-2.0";
  private static final String _NEGATIVE_INTEGER = "-2";
  private static final double _NEGATIVE_DOUBLE = -2;
  private static final int _NEGATIVE_INT = -2;
  private static final long _NEGATIVE_LONG = -2L;

  private static final String _POSITIVE_ZERO_DECIMAL = "0.00";
  private static final String _POSITIVE_ZERO_INTEGER = "0";
  private static final String _NEGATIVE_ZERO_DECIMAL = "-0.00";
  private static final String _NEGATIVE_ZERO_INTEGER = "-0";
  private static final double _ZERO_DOUBLE = 0;
  private static final int _ZERO_INT = 0;
  private static final long _ZERO_LONG = 0L;

  private static final String _POSITIVE_MIN_INTEGER = "7";
  private static final int _POSITIVE_MIN_INT = 7;
  private static final String _POSITIVE_MAX_INTEGER = "60";
  private static final int _POSITIVE_MAX_INT = 60;
  private static final double _NEGATIVE_MIN_DOUBLE = -7.1;
  private static final double _NEGATIVE_MAX_DOUBLE = -0.001;
  private static final String _POSITIVE_MIN_DECIMAL = "0.1";
  private static final double _POSITIVE_MIN_DOUBLE = 0.1;
  private static final String _POSITIVE_MAX_DECIMAL = "60.7";
  private static final double _POSITIVE_MAX_DOUBLE = 60.7;
  private static final long _NEGATIVE_MIN_LONG = -1234567890L;
  private static final long _NEGATIVE_MAX_LONG = -1L;
  private static final long _POSITIVE_MIN_LONG = 7L;
  private static final long _POSITIVE_MAX_LONG = 9876543210L;

  private static final int _NEGATIVE_MIN_INT = -7;
  private static final int _NEGATIVE_MAX_INT = -1;

  private static final double _NOT_A_NUMBER_DOUBLE = Double.NaN;
  private static final int _NOT_A_NUMBER_INT = Integer.MAX_VALUE;
  private static final long _NOT_A_NUMBER_LONG = Long.MAX_VALUE;

  private static final String _BAD_BOOLEAN = "FALSEE";
  private static final String _BOOLEAN = "fAlsE";
  private static final boolean _BOOLEAN_VALUE = false;

  private static final String _HUGE_POSITIVE_DECIMAL = "123749817239847129834709.1234712341294923874923";
  private static final String _HUGE_NEGATIVE_DECIMAL = "-23485092384095283952038450912341234.1234123412938471098279";

  private static final String _HUGE_POSITIVE_INTEGER = "17489123749817238409721934712937419028374981027349182374";
  private static final String _HUGE_NEGATIVE_INTEGER = "-1237137123087109457123894723198471289741923749172349817";

  public static void main(String[] args)
  {
    // setting the time zone to GMT
    // previously, the time zone was set to GMT-8 which is Colorado time
    // this worked find, but as soon as we became multi-site, some of the test
    // cases in testConvertStringToTimeInMilliSeconds()
    // would fail because the new engineers were in a new time zone.
    //
    // George David.
    TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
    UnitTest.execute(new Test_StringUtil());
  }

  /**
   * @author Keith Lee
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Assert.expect(is != null);
    Assert.expect(os != null);

    String lineSeparator = System.getProperty("line.separator");
    Expect.expect(lineSeparator.equals(StringUtil.getLineSeparator()));
    testConvertStringToDouble(is, os);
    testConvertStringToInt(is, os);
    testConvertStringToBoolean(is, os);
    testConvertStringToLong(is, os);
    testConvertStringToFloat();
    testConvertStringToBigDecimal();
    testConvertStringToBigInteger();
    testLegacyConvertStringToInt();
    testLegacyConvertStringToPositiveInt();
    testLegacyConvertStringToNegativeInt();
    testLegacyConvertStringToDouble();
    testIsAlphaNumeric();
    testStartsWithNumber();
    testConvertStringToStrictInt();
    testConvertStringToPositiveStrictInt();
    testConvertIntegerToZeroPaddedString();
    testJoin();
    testReplace();
    testConvertHexCodedStringIntoUnicodeString(os);
    testConvertUnicodeStringIntoHexCodedString(os);
    testFormat();
    testConvertStringToTimeInMilliSeconds();

    /**
     * @todo REB someone needs to add a test for 'convertStringToBitSet'.
     * @todo REB someone needs to add a test for 'convertStringToStrictLong'.
     * @todo REB someone needs to add a test for 'countInstancesOfChar'.
     * @todo REB someone needs to add a test for 'convertMilliSecondsToElapsedTime'.
     * @todo REB someone needs to add a test for 'isNumeric'.
     * @todo REB someone needs to add a test for 'isInteger'.
     * @todo REB someone needs to add a test for 'legacyConvertStringToPositiveDouble'.
     */
    }

  /**
   * @author Greg Esparza
   */
  void testFormat()
  {
    String testString1 = "\n String #1 not formatted";
    String finalString = StringUtil.format(testString1, testString1.length() + 1);
    Expect.expect(finalString.equals(testString1));

    String testString2 = "<pre>" +
                         "\n\nString #2 has the following format:" +
                         "\n     1) Do step 1." +
                         "\n     2) Do step 2." +
                         "\n     3) Do step 3." +
                         "</pre>";

    finalString = StringUtil.format(testString2, 10);
    Expect.expect(finalString.equals(StringUtil.getPreFormattedStringWithoutHtmlTags(testString2)));

    String testString3 = "<pre>" +
                         "\n\nString #3 has the following format:" +
                         "\n4) Do step 4. This has a long line of instructions that seem to go on forever but that is OK because we do not want to force any reformat of this text." +
                         "\n  5) Do step 5." +
                         "\n    6) Do step 6." +
                         "</pre>";

    finalString = StringUtil.format(testString2 + testString3, 10);
    String newTestString2 = StringUtil.getPreFormattedStringWithoutHtmlTags(testString2);
    String newTestString3 = StringUtil.getPreFormattedStringWithoutHtmlTags(testString3);
    Expect.expect(finalString.equals(newTestString2 + newTestString3));

    String testString4 = "\n\n String #4 is           not             formatted";

    finalString = StringUtil.format(testString1 + testString2 + testString3 + testString4, 10);
    Expect.expect(finalString.equals(testString1 + testString2 + testString3 + testString4) == false);
    //System.out.println(finalString);

    testString1 = "String #1 < has a less than character but it is not a preformat > tag.";
    finalString = StringUtil.format(testString1, testString1.length() + 1);
    Expect.expect(testString1.equals(finalString));

    testString1 = "String #1 < not a preformat";
    testString2 = "<pre> preformat string is embedded </pre>";
    testString3 = "> tag.";
    finalString = StringUtil.format(testString1 + testString2 + testString3, 50);
    newTestString2 = StringUtil.getPreFormattedStringWithoutHtmlTags(testString2);
    Expect.expect(finalString.equals(testString1 + newTestString2 + testString3));

    //Ensure that short strings without preformatting don't crash
    testString1 = "Pass";
    finalString = StringUtil.format(testString1, 50);
    Expect.expect(finalString.equalsIgnoreCase(testString1));

    // check that a string that has a messed up <pre></pre> pair still works
    testString1 = "<pre> hello";
    finalString = StringUtil.format(testString1, 50);
    Expect.expect(finalString.equalsIgnoreCase(testString1));

    // check that a string that has a messed up <pre></pre> pair still works
    testString1 = "<pre> hello <\\pre>";
    finalString = StringUtil.format(testString1, 50);
    Expect.expect(finalString.equalsIgnoreCase(testString1));

    //Empty strings should NOT cause an assert
    testString1 = "";
    finalString = StringUtil.format(testString1, 50);
    Expect.expect(finalString.equalsIgnoreCase(testString1));
  }

  /**
   * @author Keith Lee
   */
  void testConvertStringToDouble(BufferedReader is, PrintWriter os)
  {
    Assert.expect(is != null);
    Assert.expect(os != null);

    boolean exception = false;
    double convertedDouble = _NOT_A_NUMBER_DOUBLE;

    // convert String to double
    try
    {
      StringUtil.convertStringToDouble(null);
    }
    catch (AssertException e)
    {
      exception = true;
    }
    catch (BadFormatException e)
    {
      // this exception should not happen
      e.printStackTrace();
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      StringUtil.convertStringToDouble("");
    }
    catch (BadFormatException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      StringUtil.convertStringToDouble(_BAD_NUMBER);
    }
    catch (BadFormatException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      convertedDouble = StringUtil.convertStringToDouble(_POSITIVE_DECIMAL);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedDouble == _POSITIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      convertedDouble = StringUtil.convertStringToDouble(_POSITIVE_INTEGER);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedDouble == _POSITIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      convertedDouble = StringUtil.convertStringToDouble(_NEGATIVE_DECIMAL);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedDouble == _NEGATIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      convertedDouble = StringUtil.convertStringToDouble(_NEGATIVE_INTEGER);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedDouble == _NEGATIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      convertedDouble = StringUtil.convertStringToDouble(_POSITIVE_ZERO_DECIMAL);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedDouble == _ZERO_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      convertedDouble = StringUtil.convertStringToDouble(_POSITIVE_ZERO_INTEGER);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedDouble == _ZERO_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      convertedDouble = StringUtil.convertStringToDouble(_NEGATIVE_DECIMAL);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedDouble == _NEGATIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      convertedDouble = StringUtil.convertStringToDouble(_NEGATIVE_INTEGER);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedDouble == _NEGATIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    // convert String to positive double
    try
    {
      StringUtil.convertStringToPositiveDouble(null);
    }
    catch (AssertException e)
    {
      exception = true;
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      StringUtil.convertStringToPositiveDouble("");
    }
    catch (BadFormatException e)
    {
      exception = true;
    }
    catch (ValueOutOfRangeException ex)
    {
      exception = false;
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      StringUtil.convertStringToPositiveDouble(_BAD_NUMBER);
    }
    catch (BadFormatException e)
    {
      exception = true;
    }
    catch (ValueOutOfRangeException ex)
    {
      exception = false;
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      convertedDouble = StringUtil.convertStringToPositiveDouble(_POSITIVE_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _POSITIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      convertedDouble = StringUtil.convertStringToPositiveDouble(_POSITIVE_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _POSITIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      StringUtil.convertStringToPositiveDouble(_NEGATIVE_DECIMAL);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      StringUtil.convertStringToPositiveDouble(_NEGATIVE_INTEGER);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      exception = true;
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      convertedDouble = StringUtil.convertStringToPositiveDouble(_POSITIVE_ZERO_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _ZERO_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      convertedDouble = StringUtil.convertStringToPositiveDouble(_POSITIVE_ZERO_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _ZERO_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      convertedDouble = StringUtil.convertStringToPositiveDouble(_NEGATIVE_ZERO_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _ZERO_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      convertedDouble = StringUtil.convertStringToPositiveDouble(_NEGATIVE_ZERO_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _ZERO_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    // convert String to negative double
    try
    {
      StringUtil.convertStringToNegativeDouble(null);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);

    }

    try
    {
      StringUtil.convertStringToNegativeDouble("");
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToNegativeDouble(_BAD_NUMBER);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToNegativeDouble(_POSITIVE_DECIMAL);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }


    try
    {
      StringUtil.convertStringToNegativeDouble(_POSITIVE_INTEGER);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      convertedDouble = StringUtil.convertStringToNegativeDouble(_NEGATIVE_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _NEGATIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      convertedDouble = StringUtil.convertStringToNegativeDouble(_NEGATIVE_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _NEGATIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      convertedDouble = StringUtil.convertStringToNegativeDouble(_POSITIVE_ZERO_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _ZERO_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      convertedDouble = StringUtil.convertStringToNegativeDouble(_POSITIVE_ZERO_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _ZERO_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      convertedDouble = StringUtil.convertStringToNegativeDouble(_NEGATIVE_ZERO_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _ZERO_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      convertedDouble = StringUtil.convertStringToNegativeDouble(_NEGATIVE_ZERO_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _ZERO_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    // convert String to double within specified range
    try
    {
      StringUtil.convertStringToDouble(null, _POSITIVE_MIN_DOUBLE, _POSITIVE_MAX_DOUBLE);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }


    try
    {
      // min greater than max
      StringUtil.convertStringToDouble(_POSITIVE_INTEGER, _POSITIVE_MAX_DOUBLE, _POSITIVE_MIN_DOUBLE);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToDouble("", _NEGATIVE_MIN_DOUBLE, _POSITIVE_MAX_DOUBLE);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToDouble(_BAD_NUMBER, _NEGATIVE_MIN_DOUBLE, _POSITIVE_MAX_DOUBLE);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      // all values equal
      convertedDouble =  StringUtil.convertStringToDouble(_POSITIVE_MIN_DECIMAL, _POSITIVE_MIN_DOUBLE, _POSITIVE_MIN_DOUBLE);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _POSITIVE_MIN_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      // all values equal
      convertedDouble =  StringUtil.convertStringToDouble(_POSITIVE_INTEGER, _POSITIVE_DOUBLE, _POSITIVE_DOUBLE);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _POSITIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      // value below range, min equals max
      StringUtil.convertStringToDouble(_NEGATIVE_DECIMAL, _POSITIVE_MIN_DOUBLE, _POSITIVE_MIN_DOUBLE);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value below range, min equals max
      StringUtil.convertStringToDouble(_NEGATIVE_INTEGER, _POSITIVE_MIN_DOUBLE, _POSITIVE_MIN_DOUBLE);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value above range, min equals max
      StringUtil.convertStringToDouble(_POSITIVE_MAX_DECIMAL, _POSITIVE_MIN_DOUBLE, _POSITIVE_MIN_DOUBLE);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value above range, min equals max
      StringUtil.convertStringToDouble(_POSITIVE_MAX_INTEGER, _POSITIVE_MIN_DOUBLE, _POSITIVE_MIN_DOUBLE);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value equals min
      convertedDouble = StringUtil.convertStringToDouble(_POSITIVE_MIN_DECIMAL, _POSITIVE_MIN_DOUBLE, _POSITIVE_MAX_DOUBLE);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    Expect.expect(convertedDouble == _POSITIVE_MIN_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      // value equals min
      convertedDouble = StringUtil.convertStringToDouble(_POSITIVE_INTEGER, _POSITIVE_DOUBLE, _POSITIVE_MAX_DOUBLE);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _POSITIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      // value equals max
      convertedDouble = StringUtil.convertStringToDouble(_POSITIVE_MAX_DECIMAL, _POSITIVE_MIN_DOUBLE, _POSITIVE_MAX_DOUBLE);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _POSITIVE_MAX_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      // value equals max
      convertedDouble = StringUtil.convertStringToDouble(_POSITIVE_INTEGER, _POSITIVE_MIN_DOUBLE, _POSITIVE_DOUBLE);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _POSITIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      // value below range
      StringUtil.convertStringToDouble(_NEGATIVE_DECIMAL, _POSITIVE_MIN_DOUBLE, _POSITIVE_MAX_DOUBLE);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value below range
      StringUtil.convertStringToDouble(_NEGATIVE_INTEGER, _POSITIVE_MIN_DOUBLE, _POSITIVE_MAX_DOUBLE);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value above range
      StringUtil.convertStringToDouble(_POSITIVE_DECIMAL, _NEGATIVE_MIN_DOUBLE, _NEGATIVE_MAX_DOUBLE);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value above range
      StringUtil.convertStringToDouble(_POSITIVE_INTEGER, _NEGATIVE_MIN_DOUBLE, _NEGATIVE_MAX_DOUBLE);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // negative value
      convertedDouble = StringUtil.convertStringToDouble(_NEGATIVE_DECIMAL, _NEGATIVE_MIN_DOUBLE, _NEGATIVE_MAX_DOUBLE);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _NEGATIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      // negative value
      convertedDouble = StringUtil.convertStringToDouble(_NEGATIVE_INTEGER, _NEGATIVE_MIN_DOUBLE, _NEGATIVE_MAX_DOUBLE);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _NEGATIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      // positive value
      convertedDouble = StringUtil.convertStringToDouble(_POSITIVE_DECIMAL, _POSITIVE_MIN_DOUBLE, _POSITIVE_MAX_DOUBLE);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _POSITIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;

    try
    {
      // positive value
      convertedDouble = StringUtil.convertStringToDouble(_POSITIVE_INTEGER, _POSITIVE_MIN_DOUBLE, _POSITIVE_MAX_DOUBLE);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedDouble == _POSITIVE_DOUBLE);
    convertedDouble = _NOT_A_NUMBER_DOUBLE;
  }

  /**
   * @author Matt Wharton
   */
  private void testConvertStringToFloat()
  {
    // Test some edge cases.
    Expect.expectException(new RunnableWithExceptions()
    {
      public void run() throws BadFormatException
      {
        StringUtil.convertStringToFloat("");
      }
    }, BadFormatException.class);
    Expect.expectException(new RunnableWithExceptions()
    {
      public void run() throws BadFormatException
      {
        StringUtil.convertStringToFloat("asdf");
      }
    }, BadFormatException.class);

    try
    {
      float floatVal = StringUtil.convertStringToFloat("1.0");
      Expect.expect(floatVal == 1f);
    }
    catch (BadFormatException bfe)
    {
      // Shouldn't get here.
      Expect.expect(false);
    }

    try
    {
      float floatVal = StringUtil.convertStringToFloat("2f");
      Expect.expect(floatVal == 2f);
    }
    catch (BadFormatException bfe)
    {
      // Shouldn't get here.
      Expect.expect(false);
    }

    try
    {
      float floatVal = StringUtil.convertStringToFloat("-3.");
      Expect.expect(floatVal == -3f);
    }
    catch (BadFormatException bfe)
    {
      // Shouldn't get here.
      Expect.expect(false);
    }
  }

  /**
   * @author Keith Lee
   */
  void testConvertStringToInt(BufferedReader is, PrintWriter os)
  {
    Assert.expect(is != null);
    Assert.expect(os != null);

    boolean exception = false;
    int convertedInt = _NOT_A_NUMBER_INT;

    // convert String to int
    try
    {
      StringUtil.convertStringToInt(null);
    }
    catch (AssertException e)
    {
      exception = true;
    }
    catch (BadFormatException e)
    {
      // this exception should not happen
      e.printStackTrace();
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      StringUtil.convertStringToInt("");
    }
    catch (BadFormatException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      StringUtil.convertStringToInt(_BAD_NUMBER);
    }
    catch (BadFormatException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      convertedInt = StringUtil.convertStringToInt(_POSITIVE_DECIMAL);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedInt == _POSITIVE_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      convertedInt = StringUtil.convertStringToInt(_POSITIVE_INTEGER);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedInt == _POSITIVE_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      convertedInt = StringUtil.convertStringToInt(_NEGATIVE_DECIMAL);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedInt == _NEGATIVE_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      convertedInt = StringUtil.convertStringToInt(_NEGATIVE_INTEGER);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedInt == _NEGATIVE_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      convertedInt = StringUtil.convertStringToInt(_POSITIVE_ZERO_DECIMAL);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedInt == _ZERO_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      convertedInt = StringUtil.convertStringToInt(_POSITIVE_ZERO_INTEGER);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedInt == _ZERO_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      convertedInt = StringUtil.convertStringToInt(_NEGATIVE_ZERO_DECIMAL);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedInt == _ZERO_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      convertedInt = StringUtil.convertStringToInt(_NEGATIVE_ZERO_INTEGER);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedInt == _ZERO_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    // convert String to positive int
    try
    {
      StringUtil.convertStringToPositiveInt(null);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToPositiveInt("");
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToPositiveInt(_BAD_NUMBER);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      convertedInt = StringUtil.convertStringToPositiveInt(_POSITIVE_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _POSITIVE_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      convertedInt = StringUtil.convertStringToPositiveInt(_POSITIVE_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _POSITIVE_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      StringUtil.convertStringToPositiveInt(_NEGATIVE_DECIMAL);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      StringUtil.convertStringToPositiveInt(_NEGATIVE_INTEGER);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      convertedInt = StringUtil.convertStringToPositiveInt(_POSITIVE_ZERO_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _ZERO_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      convertedInt = StringUtil.convertStringToPositiveInt(_POSITIVE_ZERO_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _ZERO_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      convertedInt = StringUtil.convertStringToPositiveInt(_NEGATIVE_ZERO_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _ZERO_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      convertedInt = StringUtil.convertStringToPositiveInt(_NEGATIVE_ZERO_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _ZERO_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    // convert String to negative int
    try
    {
      StringUtil.convertStringToNegativeInt(null);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToNegativeInt("");
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToNegativeInt(_BAD_NUMBER);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToNegativeInt(_POSITIVE_DECIMAL);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      StringUtil.convertStringToNegativeInt(_POSITIVE_INTEGER);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      convertedInt = StringUtil.convertStringToNegativeInt(_NEGATIVE_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _NEGATIVE_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      convertedInt = StringUtil.convertStringToNegativeInt(_NEGATIVE_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _NEGATIVE_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      convertedInt = StringUtil.convertStringToNegativeInt(_POSITIVE_ZERO_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _ZERO_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      convertedInt = StringUtil.convertStringToNegativeInt(_POSITIVE_ZERO_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _ZERO_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      convertedInt = StringUtil.convertStringToNegativeInt(_NEGATIVE_ZERO_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      convertedInt = StringUtil.convertStringToNegativeInt(_NEGATIVE_ZERO_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _ZERO_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    // convert String to int within specified range
    try
    {
      StringUtil.convertStringToInt(null, _POSITIVE_MIN_INT, _POSITIVE_MAX_INT);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      // min greater than max
      StringUtil.convertStringToInt(_POSITIVE_INTEGER, _POSITIVE_MAX_INT, _POSITIVE_MIN_INT);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }
    catch (BadFormatException e)
    {
      // this exception should not happen
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToInt("", _NEGATIVE_MIN_INT, _POSITIVE_MAX_INT);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToInt(_BAD_NUMBER, _NEGATIVE_MIN_INT, _POSITIVE_MAX_INT);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      // all values equal
      convertedInt = StringUtil.convertStringToInt(_POSITIVE_DECIMAL, _POSITIVE_INT, _POSITIVE_INT);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _POSITIVE_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      // all values equal
      convertedInt = StringUtil.convertStringToInt(_POSITIVE_MIN_INTEGER, _POSITIVE_MIN_INT, _POSITIVE_MIN_INT);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _POSITIVE_MIN_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      // value below range, min equals max
      StringUtil.convertStringToInt(_NEGATIVE_DECIMAL, _POSITIVE_MIN_INT, _POSITIVE_MIN_INT);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value below range, min equals max
      StringUtil.convertStringToInt(_NEGATIVE_INTEGER, _POSITIVE_MIN_INT, _POSITIVE_MIN_INT);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value above range, min equals max
      StringUtil.convertStringToInt(_POSITIVE_DECIMAL, _POSITIVE_MIN_INT, _POSITIVE_MIN_INT);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value above range, min equals max
      StringUtil.convertStringToInt(_POSITIVE_INTEGER, _POSITIVE_MIN_INT, _POSITIVE_MIN_INT);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value equals min
      convertedInt = StringUtil.convertStringToInt(_POSITIVE_DECIMAL, _POSITIVE_INT, _POSITIVE_MAX_INT);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      // value equals min
      convertedInt = StringUtil.convertStringToInt(_POSITIVE_MIN_INTEGER, _POSITIVE_MIN_INT, _POSITIVE_MAX_INT);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _POSITIVE_MIN_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      // value equals max
      convertedInt = StringUtil.convertStringToInt(_POSITIVE_DECIMAL, _POSITIVE_MIN_INT, _POSITIVE_INT);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _POSITIVE_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      // value equals max
      convertedInt = StringUtil.convertStringToInt(_POSITIVE_MAX_INTEGER, _POSITIVE_MIN_INT, _POSITIVE_MAX_INT);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _POSITIVE_MAX_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      // value below range
      StringUtil.convertStringToInt(_NEGATIVE_DECIMAL, _POSITIVE_MIN_INT, _POSITIVE_MAX_INT);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value below range
      StringUtil.convertStringToInt(_NEGATIVE_INTEGER, _POSITIVE_MIN_INT, _POSITIVE_MAX_INT);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value above range
      StringUtil.convertStringToInt(_POSITIVE_DECIMAL, _NEGATIVE_MIN_INT, _NEGATIVE_MAX_INT);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value above range
      StringUtil.convertStringToInt(_POSITIVE_INTEGER, _NEGATIVE_MIN_INT, _NEGATIVE_MAX_INT);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // negative value
      convertedInt = StringUtil.convertStringToInt(_NEGATIVE_DECIMAL, _NEGATIVE_MIN_INT, _NEGATIVE_MAX_INT);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _NEGATIVE_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      // negative value
      convertedInt = StringUtil.convertStringToInt(_NEGATIVE_INTEGER, _NEGATIVE_MIN_INT, _NEGATIVE_MAX_INT);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _NEGATIVE_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      // positive value
      convertedInt = StringUtil.convertStringToInt(_POSITIVE_DECIMAL, _POSITIVE_MIN_INT, _POSITIVE_MAX_INT);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _POSITIVE_INT);
    convertedInt = _NOT_A_NUMBER_INT;

    try
    {
      // positive value
      convertedInt = StringUtil.convertStringToInt(_POSITIVE_INTEGER, _POSITIVE_MIN_INT, _POSITIVE_MAX_INT);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedInt == _POSITIVE_INT);
    convertedInt = _NOT_A_NUMBER_INT;
  }

  /**
   * @author Keith Lee
   */
  void testConvertStringToBoolean(BufferedReader is, PrintWriter os)
  {
    Assert.expect(is != null);
    Assert.expect(os != null);

    boolean exception = false;
    boolean convertedBoolean = !_BOOLEAN_VALUE;

    // convert String to boolean
    try
    {
      StringUtil.convertStringToBoolean(null);
    }
    catch (AssertException e)
    {
      exception = true;
    }
    catch (BadFormatException e)
    {
      // this exception should not happen
      e.printStackTrace();
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      StringUtil.convertStringToBoolean("");
    }
    catch (BadFormatException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      StringUtil.convertStringToBoolean(_BAD_BOOLEAN);
    }
    catch (BadFormatException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      convertedBoolean = StringUtil.convertStringToBoolean(_BOOLEAN);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedBoolean == _BOOLEAN_VALUE);
  }

  /**
   * @author Keith Lee
   */
  void testConvertStringToLong(BufferedReader is, PrintWriter os)
  {
    Assert.expect(is != null);
    Assert.expect(os != null);

    boolean exception = false;
    long convertedLong = _NOT_A_NUMBER_LONG;

    // convert String to long
    try
    {
      StringUtil.convertStringToLong(null);
    }
    catch (AssertException e)
    {
      exception = true;
    }
    catch (BadFormatException e)
    {
      // this exception should not happen
      e.printStackTrace();
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      StringUtil.convertStringToLong("");
    }
    catch (BadFormatException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      StringUtil.convertStringToLong(_BAD_NUMBER);
    }
    catch (BadFormatException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      StringUtil.convertStringToLong(_POSITIVE_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }

    try
    {
      convertedLong = StringUtil.convertStringToLong(_POSITIVE_INTEGER);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedLong == _POSITIVE_LONG);
    convertedLong = _NOT_A_NUMBER_LONG;

    try
    {
      convertedLong = StringUtil.convertStringToLong(_NEGATIVE_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }

    try
    {
      convertedLong = StringUtil.convertStringToLong(_NEGATIVE_INTEGER);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedLong == _NEGATIVE_LONG);
    convertedLong = _NOT_A_NUMBER_LONG;

    try
    {
      StringUtil.convertStringToLong(_POSITIVE_ZERO_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }

    try
    {
      convertedLong = StringUtil.convertStringToLong(_POSITIVE_ZERO_INTEGER);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedLong == _ZERO_LONG);
    convertedLong = _NOT_A_NUMBER_LONG;

    try
    {
      StringUtil.convertStringToLong(_NEGATIVE_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }

    try
    {
      convertedLong = StringUtil.convertStringToLong(_NEGATIVE_INTEGER);
    }
    catch (BadFormatException e)
    {
      e.printStackTrace();
    }
    Expect.expect(convertedLong == _NEGATIVE_LONG);
    convertedLong = _NOT_A_NUMBER_LONG;

    // convert String to positive long
    try
    {
      StringUtil.convertStringToPositiveLong(null);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToPositiveLong("");
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToPositiveLong(_BAD_NUMBER);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToPositiveLong(_POSITIVE_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      convertedLong = StringUtil.convertStringToPositiveLong(_POSITIVE_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedLong == _POSITIVE_LONG);
    convertedLong = _NOT_A_NUMBER_LONG;

    try
    {
      StringUtil.convertStringToPositiveLong(_NEGATIVE_DECIMAL);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      StringUtil.convertStringToPositiveLong(_NEGATIVE_INTEGER);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      StringUtil.convertStringToPositiveLong(_POSITIVE_ZERO_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);

    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      convertedLong = StringUtil.convertStringToPositiveLong(_POSITIVE_ZERO_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedLong == _ZERO_LONG);
    convertedLong = _NOT_A_NUMBER_LONG;

    try
    {
      StringUtil.convertStringToPositiveLong(_NEGATIVE_ZERO_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      convertedLong = StringUtil.convertStringToPositiveLong(_NEGATIVE_ZERO_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedLong == _ZERO_LONG);
    convertedLong = _NOT_A_NUMBER_LONG;

    // convert String to negative long
    try
    {
      StringUtil.convertStringToNegativeLong(null);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToNegativeLong("");
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToNegativeLong(_BAD_NUMBER);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToNegativeLong(_POSITIVE_DECIMAL);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      StringUtil.convertStringToNegativeLong(_POSITIVE_INTEGER);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      StringUtil.convertStringToNegativeLong(_NEGATIVE_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      convertedLong = StringUtil.convertStringToNegativeLong(_NEGATIVE_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedLong == _NEGATIVE_LONG);
    convertedLong = _NOT_A_NUMBER_LONG;

    try
    {
      StringUtil.convertStringToNegativeLong(_POSITIVE_ZERO_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      convertedLong = StringUtil.convertStringToNegativeLong(_POSITIVE_ZERO_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedLong == _ZERO_LONG);
    convertedLong = _NOT_A_NUMBER_LONG;

    try
    {
      StringUtil.convertStringToNegativeLong(_NEGATIVE_ZERO_DECIMAL);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      convertedLong = StringUtil.convertStringToNegativeLong(_NEGATIVE_ZERO_INTEGER);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedLong == _ZERO_LONG);
    convertedLong = _NOT_A_NUMBER_LONG;

    // convert String to long within specified range
    try
    {
      StringUtil.convertStringToLong(null, _POSITIVE_MIN_LONG, _POSITIVE_MAX_LONG);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      // min greater than max
      StringUtil.convertStringToLong(_POSITIVE_INTEGER, _POSITIVE_MAX_LONG, _POSITIVE_MIN_LONG);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToLong("", _NEGATIVE_MIN_LONG, _POSITIVE_MAX_LONG);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToLong(_BAD_NUMBER, _NEGATIVE_MIN_LONG, _POSITIVE_MAX_LONG);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      // all values equal
      StringUtil.convertStringToLong(_POSITIVE_MIN_DECIMAL, _POSITIVE_MIN_LONG, _POSITIVE_MIN_LONG);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      // all values equal
      convertedLong =  StringUtil.convertStringToLong(_POSITIVE_INTEGER, _POSITIVE_LONG, _POSITIVE_LONG);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedLong == _POSITIVE_LONG);
    convertedLong = _NOT_A_NUMBER_LONG;

    try
    {
      // value below range, min equals max
      StringUtil.convertStringToLong(_NEGATIVE_DECIMAL, _POSITIVE_MIN_LONG, _POSITIVE_MIN_LONG);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value below range, min equals max
      StringUtil.convertStringToLong(_NEGATIVE_INTEGER, _POSITIVE_MIN_LONG, _POSITIVE_MIN_LONG);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value above range, min equals max
      StringUtil.convertStringToLong(_POSITIVE_MAX_DECIMAL, _POSITIVE_MIN_LONG, _POSITIVE_MIN_LONG);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      // value above range, min equals max
      StringUtil.convertStringToLong(_POSITIVE_MAX_INTEGER, _POSITIVE_MIN_LONG, _POSITIVE_MIN_LONG);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value equals min
      StringUtil.convertStringToLong(_POSITIVE_MIN_DECIMAL, _POSITIVE_MIN_LONG, _POSITIVE_MAX_LONG);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      // value equals min
      convertedLong = StringUtil.convertStringToLong(_POSITIVE_INTEGER, _POSITIVE_LONG, _POSITIVE_MAX_LONG);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedLong == _POSITIVE_LONG);
    convertedLong = _NOT_A_NUMBER_LONG;

    try
    {
      // value equals max
      StringUtil.convertStringToLong(_POSITIVE_MAX_DECIMAL, _POSITIVE_MIN_LONG, _POSITIVE_MAX_LONG);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      // do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      // value equals max
      convertedLong = StringUtil.convertStringToLong(_POSITIVE_INTEGER, _POSITIVE_MIN_LONG, _POSITIVE_LONG);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedLong == _POSITIVE_LONG);
    convertedLong = _NOT_A_NUMBER_LONG;

    try
    {
      // value below range
      StringUtil.convertStringToLong(_NEGATIVE_DECIMAL, _POSITIVE_MIN_LONG, _POSITIVE_MAX_LONG);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value below range
      StringUtil.convertStringToLong(_NEGATIVE_INTEGER, _POSITIVE_MIN_LONG, _POSITIVE_MAX_LONG);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value above range
      StringUtil.convertStringToLong(_POSITIVE_DECIMAL, _NEGATIVE_MIN_LONG, _NEGATIVE_MAX_LONG);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // value above range
      StringUtil.convertStringToLong(_POSITIVE_INTEGER, _NEGATIVE_MIN_LONG, _NEGATIVE_MAX_LONG);
      Expect.expect(false);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    try
    {
      // negative value
      StringUtil.convertStringToLong(_NEGATIVE_DECIMAL, _NEGATIVE_MIN_LONG, _NEGATIVE_MAX_LONG);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      // negative value
      convertedLong = StringUtil.convertStringToLong(_NEGATIVE_INTEGER, _NEGATIVE_MIN_LONG, _NEGATIVE_MAX_LONG);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedLong == _NEGATIVE_LONG);
    convertedLong = _NOT_A_NUMBER_LONG;

    try
    {
      // positive value
      StringUtil.convertStringToLong(_POSITIVE_DECIMAL, _POSITIVE_MIN_LONG, _POSITIVE_MAX_LONG);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      // positive value
      convertedLong = StringUtil.convertStringToLong(_POSITIVE_INTEGER, _POSITIVE_MIN_LONG, _POSITIVE_MAX_LONG);
    }
    catch (BadFormatException e)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
    Expect.expect(convertedLong == _POSITIVE_LONG);
    convertedLong = _NOT_A_NUMBER_LONG;

    try
    {
      long value = StringUtil.convertStringToLong("1142367344629");
      Expect.expect(value == 1142367344629L);
    }
    catch (BadFormatException bfe)
    {
      bfe.printStackTrace();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void testReplace()
  {
    // test replace method
    String orig = "hi there, what is it. there are";
    String newStr = StringUtil.replace(orig, "there", "bark");
    Expect.expect(newStr.equals("hi bark, what is it. bark are"));

    orig = "\nline one\nline two\nline three";
    newStr = StringUtil.replace(orig, "\n", "\n  ");
    Expect.expect(newStr.equals("\n  line one\n  line two\n  line three"));

    orig = "";
    newStr = StringUtil.replace(orig, "a", "b");
    Expect.expect(newStr.equals(orig));

    orig = "hi there";
    newStr = StringUtil.replace(orig, "hi", "hi");
    Expect.expect(newStr.equals(orig));

    // test format method
    String str = "a b c d e f g h i j k l hi there how are you";
    String formated = StringUtil.format(str, 4);
    String separator = StringUtil.getLineSeparator();
    Expect.expect(formated.equals("a b c" +
                                  separator +
                                  "d e f" +
                                  separator +
                                  "g h i" +
                                  separator +
                                  "j k l" +
                                  separator +
                                  "hi there" +
                                  separator +
                                  "how are" +
                                  separator +
                                  "you"));

    str = "Unrecognized error code: 56";
    formated = StringUtil.format(str, 50); // should add no separators
    Expect.expect(formated.equals(str));
  }

  /**
   * @author Matt Wharton
   */
  private void testConvertStringToBigDecimal()
  {
    try
    {
      BigDecimal bd = StringUtil.convertStringToBigDecimal( _HUGE_POSITIVE_DECIMAL );
    }
    catch ( BadFormatException bfe )
    {
      Expect.expect( false );  // Shouldn't ever happen.
    }

    try
    {
      BigDecimal bd = StringUtil.convertStringToBigDecimal( _HUGE_NEGATIVE_DECIMAL );
    }
    catch ( BadFormatException bfe )
    {
      Expect.expect( false ); // Shouldn't ever happen.
    }

    try
    {
      BigDecimal bd = StringUtil.convertStringToBigDecimal( _BAD_NUMBER );
      Expect.expect( false );  // Shouldn't reach this line.  We expect a BadFormatException to get thrown.
    }
    catch ( BadFormatException bfe )
    {
      // Do nothing.  We expect this to occur.
    }
  }

  /**
   * @author Matt Wharton
   */
  private void testConvertStringToBigInteger()
  {
    try
    {
      BigInteger bi = StringUtil.convertStringToBigInteger( _HUGE_POSITIVE_INTEGER );
    }
    catch ( BadFormatException bfe )
    {
      Expect.expect( false );  // Shouldn't ever happen.
    }

    try
    {
      BigInteger bi = StringUtil.convertStringToBigInteger( _HUGE_NEGATIVE_INTEGER );
    }
    catch ( BadFormatException bfe )
    {
      Expect.expect( false ); // Shouldn't ever happen.
    }

    try
    {
      BigInteger bi = StringUtil.convertStringToBigInteger( _BAD_NUMBER );
      Expect.expect( false );  // Shouldn't reach this line.  We expect a BadFormatException to get thrown.
    }
    catch ( BadFormatException bfe )
    {
      // Do nothing.  We expect this to occur.
    }

  }

  /**
   * @author George A. David
   */
  private void testLegacyConvertStringToInt()
  {
    try
    {
      StringUtil.legacyConvertStringToInt(null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }

    String token = "abc";
    try
    {
      StringUtil.legacyConvertStringToInt(token);
      Expect.expect(false);
    }
    catch (BadFormatException exception)
    {
      //do nothing
    }

    token = "2323";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToInt(token) == 2323);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }

    token = "23a5";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToInt(token) == 23);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }

    token = "23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToInt(token) == 23);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }

    token = "-23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToInt(token) == -23);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }

    token = "+23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToInt(token) == 23);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.legacyConvertStringToInt(null, 0, 10);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "1";
    try
    {
      StringUtil.legacyConvertStringToInt(token, 10, 0);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }
    catch (BadFormatException exception)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "abc";
    try
    {
      StringUtil.legacyConvertStringToInt(token, 0, 10);
      Expect.expect(false);
    }
    catch (BadFormatException exception)
    {
      //do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "2323";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToInt(token, 0, 2323) == 2323);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "23a5";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToInt(token, 0, 23) == 23);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToInt(token, 0, 23) == 23);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "-23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToInt(token, -23, 0) == -23);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "+23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToInt(token, 23, 50) == 23);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }


    token = "100";
    try
    {
      StringUtil.legacyConvertStringToInt(token, 23, 50);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing

    }
  }

  /**
   * @author George A. David
   */
  private void testLegacyConvertStringToPositiveInt()
  {
    try
    {
      StringUtil.legacyConvertStringToPositiveInt(null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }


    String token = "abc";
    try
    {
      StringUtil.legacyConvertStringToPositiveInt(token);
      Expect.expect(false);
    }
    catch (BadFormatException exception)
    {
      //do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }


    token = "2323";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToPositiveInt(token) == 2323);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }


    token = "23a5";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToPositiveInt(token) == 23);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }


    token = "23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToPositiveInt(token) == 23);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }


    token = "+23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToPositiveInt(token) == 23);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }


    token = "-23.5a6";
    try
    {
      StringUtil.legacyConvertStringToPositiveInt(token);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }
  }

  /**
   * @author George A. David
   */
  private void testLegacyConvertStringToNegativeInt()
  {
    try
    {
      StringUtil.legacyConvertStringToNegativeInt(null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }


    String token = "abc";
    try
    {
      StringUtil.legacyConvertStringToNegativeInt(token);
      Expect.expect(false);
    }
    catch (BadFormatException exception)
    {
      //do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "-2323";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToNegativeInt(token) == -2323);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "-23a5";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToNegativeInt(token) == -23);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "-23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToNegativeInt(token) == -23);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "+23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToNegativeInt(token) == 23);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    token = "23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToNegativeInt(token) == 23);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    token = "-23.5a6";
    try
    {
      StringUtil.legacyConvertStringToNegativeInt(token);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
  }

  /**
   * @author George A. David
   */
  private void testLegacyConvertStringToDouble()
  {
    try
    {
      StringUtil.legacyConvertStringToDouble(null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }

    String token = "abc";
    try
    {
      StringUtil.legacyConvertStringToDouble(token);
      Expect.expect(false);
    }
    catch (BadFormatException exception)
    {
      //do nothing
    }

    token = "23.55";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToDouble(token) == 23.55);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }

    token = "23a5";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToDouble(token) == 23.0);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }

    token = "23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToDouble(token) == 23.5);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }

    token = "-23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToDouble(token) == -23.5);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }

    token = "+23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToDouble(token) == 23.5);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }


    try
    {
      StringUtil.legacyConvertStringToDouble(null, 0, 10);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "1";
    try
    {
      StringUtil.legacyConvertStringToDouble(null, 10, 0);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "abc";
    try
    {
      StringUtil.legacyConvertStringToDouble(token, 0, 10);
      Expect.expect(false);
    }
    catch (BadFormatException exception)
    {
      //do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "23.55";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToDouble(token, 0, 23.55) == 23.55);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "23a5";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToDouble(token, 23, 50) == 23.0);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToDouble(token, 23, 50) == 23.5);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "-23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToDouble(token, -50, -22) == -23.5);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "+23.5a6";
    try
    {
      Expect.expect(StringUtil.legacyConvertStringToDouble(token, 0, 100) == 23.5);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "-9";
    try
    {
      StringUtil.legacyConvertStringToDouble(token, 0, 100);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }
  }

  /**
   * @author George A. David
   */
  private void testIsAlphaNumeric()
  {
    try
    {
      StringUtil.isAlphaNumeric(null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      // do nothing
    }

    String token = "123";
    Expect.expect(StringUtil.isAlphaNumeric(token) == false);

    token = "1.23";
    Expect.expect(StringUtil.isAlphaNumeric(token) == false);

    token = "1.2.3";
    Expect.expect(StringUtil.isAlphaNumeric(token) == true);

    token = "123a";
    Expect.expect(StringUtil.isAlphaNumeric(token) == true);

    token = "";
    Expect.expect(StringUtil.isAlphaNumeric(token) == true);
  }

  /**
   * @author George A. David
   */
  private void testStartsWithNumber()
  {
    try
    {
      StringUtil.startsWithNumber(null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    String token = "a23";
    Expect.expect(StringUtil.startsWithNumber(token) == false);

    token = "0.233x";
    Expect.expect(StringUtil.startsWithNumber(token) == true);

    token = ".5";
    Expect.expect(StringUtil.startsWithNumber(token) == false);

    token = "+5a";
    Expect.expect(StringUtil.startsWithNumber(token) == false);

    token = "-5a";
    Expect.expect(StringUtil.startsWithNumber(token) == false);
  }

  /**
   * @author George A. David
   */
  private void testConvertStringToStrictInt()
  {
    try
    {
      StringUtil.convertStringToStrictInt(null);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }
    String token = "";
    try
    {
      StringUtil.convertStringToStrictInt(token);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      //do nothing
    }

    token = "1.0";
    try
    {
      StringUtil.convertStringToStrictInt(token);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      //do nothing
    }
    token = "1a";
    try
    {
      StringUtil.convertStringToStrictInt(token);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      //do nothing
    }
    token = "1";
    try
    {
      Expect.expect(StringUtil.convertStringToStrictInt(token) == 1);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    token = "-1";
    try
    {
      Expect.expect(StringUtil.convertStringToStrictInt(token) == -1);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    token = "+1";
    try
    {
      Expect.expect(StringUtil.convertStringToStrictInt(token) == 1);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToStrictInt(null, 0, 10);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "";
    try
    {
      StringUtil.convertStringToStrictInt(token, 10, 0);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    try
    {
      StringUtil.convertStringToStrictInt(token, 0, 10);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      //do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "1.0";
    try
    {
      StringUtil.convertStringToStrictInt(token, 0, 10);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      //do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "1a";
    try
    {
      StringUtil.convertStringToStrictInt(token, 0, 10);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      //do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "11";
    try
    {
      StringUtil.convertStringToStrictInt(token, 0, 10);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    token = "-1";
    try
    {
      StringUtil.convertStringToStrictInt(token, 0, 10);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    token = "+1";
    try
    {
      Expect.expect(StringUtil.convertStringToStrictInt(token, 0, 10) == 1);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
  }

  /**
   * @author Peter Esbensen
   */
  private void testConvertStringToPascalString()
  {
    try
    {
      StringUtil.convertStringToPascalString(null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      // do nothing, we expected this
    }

    String javaString = new String("I like fixed arrays!");
    byte[] pascalString = StringUtil.convertStringToPascalString(javaString);

    // the first byte should be the string length
    Expect.expect(pascalString[0] == (byte)20);
    Expect.expect(pascalString.length == 21);
  }

  /**
   * @author George A. David
   */
  private void testConvertStringToPositiveStrictInt()
  {
    try
    {
      StringUtil.convertStringToPositiveStrictInt(null);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    String token = "";
    try
    {
      StringUtil.convertStringToPositiveStrictInt(token);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      //do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "1.0";
    try
    {
      StringUtil.convertStringToPositiveStrictInt(token);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      //do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "1a";
    try
    {
      StringUtil.convertStringToPositiveStrictInt(token);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      //do nothing
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "1";
    try
    {
      Expect.expect(StringUtil.convertStringToPositiveStrictInt(token) == 1);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }

    token = "-1";
    try
    {
      StringUtil.convertStringToPositiveStrictInt(token);
      Expect.expect(false);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      // do nothing
    }

    token = "+1";
    try
    {
      Expect.expect(StringUtil.convertStringToPositiveStrictInt(token) == 1);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false);
    }
    catch (ValueOutOfRangeException ex)
    {
      Expect.expect(false);
    }
  }

  /**
   * @author Kay Lannen
   */
  private void testConvertIntegerToZeroPaddedString()
  {
    String strResult;
    strResult = StringUtil.convertIntegerToZeroPaddedString( 1, 1 );
    Expect.expect( strResult.equals( "1" ));

    strResult = StringUtil.convertIntegerToZeroPaddedString( 1, 3 );
    Expect.expect( strResult.equals( "001" ));

    strResult = StringUtil.convertIntegerToZeroPaddedString( 10, 1 );
    Expect.expect( strResult.equals( "10" ));

    strResult = StringUtil.convertIntegerToZeroPaddedString( 10, 6 );
    Expect.expect( strResult.equals( "000010" ));

    strResult = StringUtil.convertIntegerToZeroPaddedString( -1, 5 );
    Expect.expect( strResult.equals( "-00001" ));

  }

  /**
   * @author Matt Wharton
   */
  private void testJoin()
  {
    // Test the array version of join.
    String[] stringsToJoinAsArray1 = { "one", "two", "three" };
    String joinedString = StringUtil.join(stringsToJoinAsArray1, ',');
    Expect.expect(joinedString.equals("one,two,three"));

    String[] stringsToJoinAsArray2 = { "one" };
    joinedString = StringUtil.join(stringsToJoinAsArray2, ',');
    Expect.expect(joinedString.equals("one"));

    String[] stringsToJoinAsArray3 = { };
    joinedString = StringUtil.join(stringsToJoinAsArray3, ',');
    Expect.expect(joinedString.equals(""));

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        StringUtil.join((String[])null, ',');
      }
    });

    // Test the List<String> version of join.
    List<String> stringsToJoinAsList1 = Arrays.asList(new String[]{ "one", "two", "three" });
    joinedString = StringUtil.join(stringsToJoinAsList1, ',');
    Expect.expect(joinedString.equals("one,two,three"));

    List<String> stringsToJoinAsList2 = Arrays.asList(new String[]{ "one" });
    joinedString = StringUtil.join(stringsToJoinAsList2, ',');
    Expect.expect(joinedString.equals("one"));

    List<String> stringsToJoinAsList3 = Arrays.asList(new String[]{ });
    joinedString = StringUtil.join(stringsToJoinAsList3, ',');
    Expect.expect(joinedString.equals(""));

    List<String> stringsToJoinAsList4 = new ArrayList<String>();
    stringsToJoinAsList4.add("foo");
    stringsToJoinAsList4.add("bar");
    joinedString = StringUtil.join(stringsToJoinAsList4, ',');
    Expect.expect(joinedString.equals("foo,bar"));

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        StringUtil.join((List<String>)null, ',');
      }
    });
  }

  /**
   * @author Bob Balliew
   */
  private void testConvertHexCodedStringIntoUnicodeString(PrintWriter os)
  {
    try
    {
      String unicodedString;
      // Simple test.
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("66,6f,6f");
      Expect.expect(unicodedString.equals("foo"));

      // The following tests the correct encoding of all legal 8 bit patterns.
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0");
      Expect.expect(unicodedString.equals("\u0000"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00");
      Expect.expect(unicodedString.equals("\u0000"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("000");
      Expect.expect(unicodedString.equals("\u0000"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0000");
      Expect.expect(unicodedString.equals("\u0000"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("1");
      Expect.expect(unicodedString.equals("\u0001"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("01");
      Expect.expect(unicodedString.equals("\u0001"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("001");
      Expect.expect(unicodedString.equals("\u0001"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0001");
      Expect.expect(unicodedString.equals("\u0001"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("2");
      Expect.expect(unicodedString.equals("\u0002"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("02");
      Expect.expect(unicodedString.equals("\u0002"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("002");
      Expect.expect(unicodedString.equals("\u0002"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0002");
      Expect.expect(unicodedString.equals("\u0002"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("3");
      Expect.expect(unicodedString.equals("\u0003"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("03");
      Expect.expect(unicodedString.equals("\u0003"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("003");
      Expect.expect(unicodedString.equals("\u0003"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0003");
      Expect.expect(unicodedString.equals("\u0003"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("4");
      Expect.expect(unicodedString.equals("\u0004"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("04");
      Expect.expect(unicodedString.equals("\u0004"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("004");
      Expect.expect(unicodedString.equals("\u0004"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0004");
      Expect.expect(unicodedString.equals("\u0004"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("5");
      Expect.expect(unicodedString.equals("\u0005"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("05");
      Expect.expect(unicodedString.equals("\u0005"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("005");
      Expect.expect(unicodedString.equals("\u0005"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0005");
      Expect.expect(unicodedString.equals("\u0005"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("6");
      Expect.expect(unicodedString.equals("\u0006"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("06");
      Expect.expect(unicodedString.equals("\u0006"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("006");
      Expect.expect(unicodedString.equals("\u0006"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0006");
      Expect.expect(unicodedString.equals("\u0006"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("7");
      Expect.expect(unicodedString.equals("\u0007"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("07");
      Expect.expect(unicodedString.equals("\u0007"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("007");
      Expect.expect(unicodedString.equals("\u0007"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0007");
      Expect.expect(unicodedString.equals("\u0007"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("8");
      Expect.expect(unicodedString.equals("\u0008"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("08");
      Expect.expect(unicodedString.equals("\u0008"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("008");
      Expect.expect(unicodedString.equals("\u0008"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0008");
      Expect.expect(unicodedString.equals("\u0008"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("9");
      Expect.expect(unicodedString.equals("\u0009"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("09");
      Expect.expect(unicodedString.equals("\u0009"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("009");
      Expect.expect(unicodedString.equals("\u0009"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0009");
      Expect.expect(unicodedString.equals("\u0009"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("a");
      Expect.expect(unicodedString.equals("\n"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0a");
      Expect.expect(unicodedString.equals("\n"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00a");
      Expect.expect(unicodedString.equals("\n"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("000a");
      Expect.expect(unicodedString.equals("\n"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("A");
      Expect.expect(unicodedString.equals("\n"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0A");
      Expect.expect(unicodedString.equals("\n"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00A");
      Expect.expect(unicodedString.equals("\n"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("000A");
      Expect.expect(unicodedString.equals("\n"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("b");
      Expect.expect(unicodedString.equals("\u000b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0b");
      Expect.expect(unicodedString.equals("\u000b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00b");
      Expect.expect(unicodedString.equals("\u000b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("000b");
      Expect.expect(unicodedString.equals("\u000b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("B");
      Expect.expect(unicodedString.equals("\u000b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0B");
      Expect.expect(unicodedString.equals("\u000b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00B");
      Expect.expect(unicodedString.equals("\u000b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("000B");
      Expect.expect(unicodedString.equals("\u000b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("c");
      Expect.expect(unicodedString.equals("\u000c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0c");
      Expect.expect(unicodedString.equals("\u000c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00c");
      Expect.expect(unicodedString.equals("\u000c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("000c");
      Expect.expect(unicodedString.equals("\u000c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("C");
      Expect.expect(unicodedString.equals("\u000c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0C");
      Expect.expect(unicodedString.equals("\u000c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00C");
      Expect.expect(unicodedString.equals("\u000c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("000C");
      Expect.expect(unicodedString.equals("\u000c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("d");
      Expect.expect(unicodedString.equals("\r"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0d");
      Expect.expect(unicodedString.equals("\r"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00d");
      Expect.expect(unicodedString.equals("\r"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("000d");
      Expect.expect(unicodedString.equals("\r"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("D");
      Expect.expect(unicodedString.equals("\r"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0D");
      Expect.expect(unicodedString.equals("\r"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00D");
      Expect.expect(unicodedString.equals("\r"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("000D");
      Expect.expect(unicodedString.equals("\r"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("e");
      Expect.expect(unicodedString.equals("\u000e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0e");
      Expect.expect(unicodedString.equals("\u000e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00e");
      Expect.expect(unicodedString.equals("\u000e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("000e");
      Expect.expect(unicodedString.equals("\u000e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("E");
      Expect.expect(unicodedString.equals("\u000e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0E");
      Expect.expect(unicodedString.equals("\u000e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00E");
      Expect.expect(unicodedString.equals("\u000e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("000E");
      Expect.expect(unicodedString.equals("\u000e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("f");
      Expect.expect(unicodedString.equals("\u000f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0f");
      Expect.expect(unicodedString.equals("\u000f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00f");
      Expect.expect(unicodedString.equals("\u000f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("000f");
      Expect.expect(unicodedString.equals("\u000f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("F");
      Expect.expect(unicodedString.equals("\u000f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0F");
      Expect.expect(unicodedString.equals("\u000f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00F");
      Expect.expect(unicodedString.equals("\u000f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("000F");
      Expect.expect(unicodedString.equals("\u000f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("10");
      Expect.expect(unicodedString.equals("\u0010"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("010");
      Expect.expect(unicodedString.equals("\u0010"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0010");
      Expect.expect(unicodedString.equals("\u0010"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("11");
      Expect.expect(unicodedString.equals("\u0011"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("011");
      Expect.expect(unicodedString.equals("\u0011"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0011");
      Expect.expect(unicodedString.equals("\u0011"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("12");
      Expect.expect(unicodedString.equals("\u0012"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("012");
      Expect.expect(unicodedString.equals("\u0012"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0012");
      Expect.expect(unicodedString.equals("\u0012"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("13");
      Expect.expect(unicodedString.equals("\u0013"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("013");
      Expect.expect(unicodedString.equals("\u0013"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0013");
      Expect.expect(unicodedString.equals("\u0013"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("14");
      Expect.expect(unicodedString.equals("\u0014"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("014");
      Expect.expect(unicodedString.equals("\u0014"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0014");
      Expect.expect(unicodedString.equals("\u0014"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("15");
      Expect.expect(unicodedString.equals("\u0015"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("015");
      Expect.expect(unicodedString.equals("\u0015"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0015");
      Expect.expect(unicodedString.equals("\u0015"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("16");
      Expect.expect(unicodedString.equals("\u0016"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("016");
      Expect.expect(unicodedString.equals("\u0016"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0016");
      Expect.expect(unicodedString.equals("\u0016"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("17");
      Expect.expect(unicodedString.equals("\u0017"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("017");
      Expect.expect(unicodedString.equals("\u0017"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0017");
      Expect.expect(unicodedString.equals("\u0017"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("18");
      Expect.expect(unicodedString.equals("\u0018"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("018");
      Expect.expect(unicodedString.equals("\u0018"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0018");
      Expect.expect(unicodedString.equals("\u0018"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("19");
      Expect.expect(unicodedString.equals("\u0019"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("019");
      Expect.expect(unicodedString.equals("\u0019"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0019");
      Expect.expect(unicodedString.equals("\u0019"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("1a");
      Expect.expect(unicodedString.equals("\u001a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("01a");
      Expect.expect(unicodedString.equals("\u001a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("001a");
      Expect.expect(unicodedString.equals("\u001a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("1A");
      Expect.expect(unicodedString.equals("\u001a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("01A");
      Expect.expect(unicodedString.equals("\u001a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("001A");
      Expect.expect(unicodedString.equals("\u001a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("1b");
      Expect.expect(unicodedString.equals("\u001b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("01b");
      Expect.expect(unicodedString.equals("\u001b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("001b");
      Expect.expect(unicodedString.equals("\u001b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("1B");
      Expect.expect(unicodedString.equals("\u001b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("01B");
      Expect.expect(unicodedString.equals("\u001b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("001B");
      Expect.expect(unicodedString.equals("\u001b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("1c");
      Expect.expect(unicodedString.equals("\u001c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("01c");
      Expect.expect(unicodedString.equals("\u001c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("001c");
      Expect.expect(unicodedString.equals("\u001c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("1C");
      Expect.expect(unicodedString.equals("\u001c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("01C");
      Expect.expect(unicodedString.equals("\u001c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("001C");
      Expect.expect(unicodedString.equals("\u001c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("1d");
      Expect.expect(unicodedString.equals("\u001d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("01d");
      Expect.expect(unicodedString.equals("\u001d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("001d");
      Expect.expect(unicodedString.equals("\u001d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("1D");
      Expect.expect(unicodedString.equals("\u001d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("01D");
      Expect.expect(unicodedString.equals("\u001d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("001D");
      Expect.expect(unicodedString.equals("\u001d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("1e");
      Expect.expect(unicodedString.equals("\u001e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("01e");
      Expect.expect(unicodedString.equals("\u001e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("001e");
      Expect.expect(unicodedString.equals("\u001e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("1E");
      Expect.expect(unicodedString.equals("\u001e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("01E");
      Expect.expect(unicodedString.equals("\u001e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("001E");
      Expect.expect(unicodedString.equals("\u001e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("1f");
      Expect.expect(unicodedString.equals("\u001f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("01f");
      Expect.expect(unicodedString.equals("\u001f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("001f");
      Expect.expect(unicodedString.equals("\u001f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("1F");
      Expect.expect(unicodedString.equals("\u001f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("01F");
      Expect.expect(unicodedString.equals("\u001f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("001F");
      Expect.expect(unicodedString.equals("\u001f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("20");
      Expect.expect(unicodedString.equals("\u0020"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("020");
      Expect.expect(unicodedString.equals("\u0020"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0020");
      Expect.expect(unicodedString.equals("\u0020"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("21");
      Expect.expect(unicodedString.equals("\u0021"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("021");
      Expect.expect(unicodedString.equals("\u0021"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0021");
      Expect.expect(unicodedString.equals("\u0021"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("22");
      Expect.expect(unicodedString.equals("\""));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("022");
      Expect.expect(unicodedString.equals("\""));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0022");
      Expect.expect(unicodedString.equals("\""));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("23");
      Expect.expect(unicodedString.equals("\u0023"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("023");
      Expect.expect(unicodedString.equals("\u0023"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0023");
      Expect.expect(unicodedString.equals("\u0023"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("24");
      Expect.expect(unicodedString.equals("\u0024"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("024");
      Expect.expect(unicodedString.equals("\u0024"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0024");
      Expect.expect(unicodedString.equals("\u0024"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("25");
      Expect.expect(unicodedString.equals("\u0025"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("025");
      Expect.expect(unicodedString.equals("\u0025"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0025");
      Expect.expect(unicodedString.equals("\u0025"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("26");
      Expect.expect(unicodedString.equals("\u0026"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("026");
      Expect.expect(unicodedString.equals("\u0026"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0026");
      Expect.expect(unicodedString.equals("\u0026"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("27");
      Expect.expect(unicodedString.equals("\u0027"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("027");
      Expect.expect(unicodedString.equals("\u0027"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0027");
      Expect.expect(unicodedString.equals("\u0027"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("28");
      Expect.expect(unicodedString.equals("\u0028"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("028");
      Expect.expect(unicodedString.equals("\u0028"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0028");
      Expect.expect(unicodedString.equals("\u0028"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("29");
      Expect.expect(unicodedString.equals("\u0029"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("029");
      Expect.expect(unicodedString.equals("\u0029"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0029");
      Expect.expect(unicodedString.equals("\u0029"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("2a");
      Expect.expect(unicodedString.equals("\u002a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("02a");
      Expect.expect(unicodedString.equals("\u002a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("002a");
      Expect.expect(unicodedString.equals("\u002a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("2A");
      Expect.expect(unicodedString.equals("\u002a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("02A");
      Expect.expect(unicodedString.equals("\u002a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("002A");
      Expect.expect(unicodedString.equals("\u002a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("2b");
      Expect.expect(unicodedString.equals("\u002b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("02b");
      Expect.expect(unicodedString.equals("\u002b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("002b");
      Expect.expect(unicodedString.equals("\u002b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("2B");
      Expect.expect(unicodedString.equals("\u002b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("02B");
      Expect.expect(unicodedString.equals("\u002b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("002B");
      Expect.expect(unicodedString.equals("\u002b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("2c");
      Expect.expect(unicodedString.equals("\u002c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("02c");
      Expect.expect(unicodedString.equals("\u002c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("002c");
      Expect.expect(unicodedString.equals("\u002c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("2C");
      Expect.expect(unicodedString.equals("\u002c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("02C");
      Expect.expect(unicodedString.equals("\u002c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("002C");
      Expect.expect(unicodedString.equals("\u002c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("2d");
      Expect.expect(unicodedString.equals("\u002d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("02d");
      Expect.expect(unicodedString.equals("\u002d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("002d");
      Expect.expect(unicodedString.equals("\u002d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("2D");
      Expect.expect(unicodedString.equals("\u002d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("02D");
      Expect.expect(unicodedString.equals("\u002d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("002D");
      Expect.expect(unicodedString.equals("\u002d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("2e");
      Expect.expect(unicodedString.equals("\u002e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("02e");
      Expect.expect(unicodedString.equals("\u002e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("002e");
      Expect.expect(unicodedString.equals("\u002e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("2E");
      Expect.expect(unicodedString.equals("\u002e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("02E");
      Expect.expect(unicodedString.equals("\u002e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("002E");
      Expect.expect(unicodedString.equals("\u002e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("2f");
      Expect.expect(unicodedString.equals("\u002f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("02f");
      Expect.expect(unicodedString.equals("\u002f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("002f");
      Expect.expect(unicodedString.equals("\u002f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("2F");
      Expect.expect(unicodedString.equals("\u002f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("02F");
      Expect.expect(unicodedString.equals("\u002f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("002F");
      Expect.expect(unicodedString.equals("\u002f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("30");
      Expect.expect(unicodedString.equals("\u0030"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("030");
      Expect.expect(unicodedString.equals("\u0030"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0030");
      Expect.expect(unicodedString.equals("\u0030"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("31");
      Expect.expect(unicodedString.equals("\u0031"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("031");
      Expect.expect(unicodedString.equals("\u0031"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0031");
      Expect.expect(unicodedString.equals("\u0031"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("32");
      Expect.expect(unicodedString.equals("\u0032"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("032");
      Expect.expect(unicodedString.equals("\u0032"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0032");
      Expect.expect(unicodedString.equals("\u0032"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("33");
      Expect.expect(unicodedString.equals("\u0033"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("033");
      Expect.expect(unicodedString.equals("\u0033"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0033");
      Expect.expect(unicodedString.equals("\u0033"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("34");
      Expect.expect(unicodedString.equals("\u0034"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("034");
      Expect.expect(unicodedString.equals("\u0034"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0034");
      Expect.expect(unicodedString.equals("\u0034"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("35");
      Expect.expect(unicodedString.equals("\u0035"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("035");
      Expect.expect(unicodedString.equals("\u0035"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0035");
      Expect.expect(unicodedString.equals("\u0035"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("36");
      Expect.expect(unicodedString.equals("\u0036"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("036");
      Expect.expect(unicodedString.equals("\u0036"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0036");
      Expect.expect(unicodedString.equals("\u0036"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("37");
      Expect.expect(unicodedString.equals("\u0037"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("037");
      Expect.expect(unicodedString.equals("\u0037"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0037");
      Expect.expect(unicodedString.equals("\u0037"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("38");
      Expect.expect(unicodedString.equals("\u0038"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("038");
      Expect.expect(unicodedString.equals("\u0038"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0038");
      Expect.expect(unicodedString.equals("\u0038"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("39");
      Expect.expect(unicodedString.equals("\u0039"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("039");
      Expect.expect(unicodedString.equals("\u0039"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0039");
      Expect.expect(unicodedString.equals("\u0039"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("3a");
      Expect.expect(unicodedString.equals("\u003a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("03a");
      Expect.expect(unicodedString.equals("\u003a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("003a");
      Expect.expect(unicodedString.equals("\u003a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("3A");
      Expect.expect(unicodedString.equals("\u003a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("03A");
      Expect.expect(unicodedString.equals("\u003a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("003A");
      Expect.expect(unicodedString.equals("\u003a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("3b");
      Expect.expect(unicodedString.equals("\u003b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("03b");
      Expect.expect(unicodedString.equals("\u003b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("003b");
      Expect.expect(unicodedString.equals("\u003b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("3B");
      Expect.expect(unicodedString.equals("\u003b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("03B");
      Expect.expect(unicodedString.equals("\u003b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("003B");
      Expect.expect(unicodedString.equals("\u003b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("3c");
      Expect.expect(unicodedString.equals("\u003c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("03c");
      Expect.expect(unicodedString.equals("\u003c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("003c");
      Expect.expect(unicodedString.equals("\u003c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("3C");
      Expect.expect(unicodedString.equals("\u003c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("03C");
      Expect.expect(unicodedString.equals("\u003c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("003C");
      Expect.expect(unicodedString.equals("\u003c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("3d");
      Expect.expect(unicodedString.equals("\u003d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("03d");
      Expect.expect(unicodedString.equals("\u003d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("003d");
      Expect.expect(unicodedString.equals("\u003d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("3D");
      Expect.expect(unicodedString.equals("\u003d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("03D");
      Expect.expect(unicodedString.equals("\u003d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("003D");
      Expect.expect(unicodedString.equals("\u003d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("3e");
      Expect.expect(unicodedString.equals("\u003e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("03e");
      Expect.expect(unicodedString.equals("\u003e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("003e");
      Expect.expect(unicodedString.equals("\u003e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("3E");
      Expect.expect(unicodedString.equals("\u003e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("03E");
      Expect.expect(unicodedString.equals("\u003e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("003E");
      Expect.expect(unicodedString.equals("\u003e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("3f");
      Expect.expect(unicodedString.equals("\u003f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("03f");
      Expect.expect(unicodedString.equals("\u003f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("003f");
      Expect.expect(unicodedString.equals("\u003f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("3F");
      Expect.expect(unicodedString.equals("\u003f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("03F");
      Expect.expect(unicodedString.equals("\u003f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("003F");
      Expect.expect(unicodedString.equals("\u003f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("40");
      Expect.expect(unicodedString.equals("\u0040"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("040");
      Expect.expect(unicodedString.equals("\u0040"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0040");
      Expect.expect(unicodedString.equals("\u0040"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("41");
      Expect.expect(unicodedString.equals("\u0041"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("041");
      Expect.expect(unicodedString.equals("\u0041"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0041");
      Expect.expect(unicodedString.equals("\u0041"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("42");
      Expect.expect(unicodedString.equals("\u0042"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("042");
      Expect.expect(unicodedString.equals("\u0042"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0042");
      Expect.expect(unicodedString.equals("\u0042"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("43");
      Expect.expect(unicodedString.equals("\u0043"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("043");
      Expect.expect(unicodedString.equals("\u0043"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0043");
      Expect.expect(unicodedString.equals("\u0043"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("44");
      Expect.expect(unicodedString.equals("\u0044"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("044");
      Expect.expect(unicodedString.equals("\u0044"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0044");
      Expect.expect(unicodedString.equals("\u0044"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("45");
      Expect.expect(unicodedString.equals("\u0045"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("045");
      Expect.expect(unicodedString.equals("\u0045"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0045");
      Expect.expect(unicodedString.equals("\u0045"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("46");
      Expect.expect(unicodedString.equals("\u0046"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("046");
      Expect.expect(unicodedString.equals("\u0046"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0046");
      Expect.expect(unicodedString.equals("\u0046"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("47");
      Expect.expect(unicodedString.equals("\u0047"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("047");
      Expect.expect(unicodedString.equals("\u0047"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0047");
      Expect.expect(unicodedString.equals("\u0047"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("48");
      Expect.expect(unicodedString.equals("\u0048"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("048");
      Expect.expect(unicodedString.equals("\u0048"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0048");
      Expect.expect(unicodedString.equals("\u0048"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("49");
      Expect.expect(unicodedString.equals("\u0049"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("049");
      Expect.expect(unicodedString.equals("\u0049"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0049");
      Expect.expect(unicodedString.equals("\u0049"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("4a");
      Expect.expect(unicodedString.equals("\u004a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("04a");
      Expect.expect(unicodedString.equals("\u004a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("004a");
      Expect.expect(unicodedString.equals("\u004a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("4A");
      Expect.expect(unicodedString.equals("\u004a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("04A");
      Expect.expect(unicodedString.equals("\u004a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("004A");
      Expect.expect(unicodedString.equals("\u004a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("4b");
      Expect.expect(unicodedString.equals("\u004b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("04b");
      Expect.expect(unicodedString.equals("\u004b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("004b");
      Expect.expect(unicodedString.equals("\u004b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("4B");
      Expect.expect(unicodedString.equals("\u004b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("04B");
      Expect.expect(unicodedString.equals("\u004b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("004B");
      Expect.expect(unicodedString.equals("\u004b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("4c");
      Expect.expect(unicodedString.equals("\u004c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("04c");
      Expect.expect(unicodedString.equals("\u004c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("004c");
      Expect.expect(unicodedString.equals("\u004c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("4C");
      Expect.expect(unicodedString.equals("\u004c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("04C");
      Expect.expect(unicodedString.equals("\u004c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("004C");
      Expect.expect(unicodedString.equals("\u004c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("4d");
      Expect.expect(unicodedString.equals("\u004d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("04d");
      Expect.expect(unicodedString.equals("\u004d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("004d");
      Expect.expect(unicodedString.equals("\u004d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("4D");
      Expect.expect(unicodedString.equals("\u004d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("04D");
      Expect.expect(unicodedString.equals("\u004d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("004D");
      Expect.expect(unicodedString.equals("\u004d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("4e");
      Expect.expect(unicodedString.equals("\u004e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("04e");
      Expect.expect(unicodedString.equals("\u004e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("004e");
      Expect.expect(unicodedString.equals("\u004e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("4E");
      Expect.expect(unicodedString.equals("\u004e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("04E");
      Expect.expect(unicodedString.equals("\u004e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("004E");
      Expect.expect(unicodedString.equals("\u004e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("4f");
      Expect.expect(unicodedString.equals("\u004f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("04f");
      Expect.expect(unicodedString.equals("\u004f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("004f");
      Expect.expect(unicodedString.equals("\u004f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("4F");
      Expect.expect(unicodedString.equals("\u004f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("04F");
      Expect.expect(unicodedString.equals("\u004f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("004F");
      Expect.expect(unicodedString.equals("\u004f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("50");
      Expect.expect(unicodedString.equals("\u0050"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("050");
      Expect.expect(unicodedString.equals("\u0050"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0050");
      Expect.expect(unicodedString.equals("\u0050"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("51");
      Expect.expect(unicodedString.equals("\u0051"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("051");
      Expect.expect(unicodedString.equals("\u0051"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0051");
      Expect.expect(unicodedString.equals("\u0051"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("52");
      Expect.expect(unicodedString.equals("\u0052"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("052");
      Expect.expect(unicodedString.equals("\u0052"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0052");
      Expect.expect(unicodedString.equals("\u0052"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("53");
      Expect.expect(unicodedString.equals("\u0053"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("053");
      Expect.expect(unicodedString.equals("\u0053"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0053");
      Expect.expect(unicodedString.equals("\u0053"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("54");
      Expect.expect(unicodedString.equals("\u0054"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("054");
      Expect.expect(unicodedString.equals("\u0054"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0054");
      Expect.expect(unicodedString.equals("\u0054"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("55");
      Expect.expect(unicodedString.equals("\u0055"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("055");
      Expect.expect(unicodedString.equals("\u0055"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0055");
      Expect.expect(unicodedString.equals("\u0055"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("56");
      Expect.expect(unicodedString.equals("\u0056"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("056");
      Expect.expect(unicodedString.equals("\u0056"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0056");
      Expect.expect(unicodedString.equals("\u0056"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("57");
      Expect.expect(unicodedString.equals("\u0057"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("057");
      Expect.expect(unicodedString.equals("\u0057"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0057");
      Expect.expect(unicodedString.equals("\u0057"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("58");
      Expect.expect(unicodedString.equals("\u0058"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("058");
      Expect.expect(unicodedString.equals("\u0058"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0058");
      Expect.expect(unicodedString.equals("\u0058"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("59");
      Expect.expect(unicodedString.equals("\u0059"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("059");
      Expect.expect(unicodedString.equals("\u0059"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0059");
      Expect.expect(unicodedString.equals("\u0059"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("5a");
      Expect.expect(unicodedString.equals("\u005a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("05a");
      Expect.expect(unicodedString.equals("\u005a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("005a");
      Expect.expect(unicodedString.equals("\u005a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("5A");
      Expect.expect(unicodedString.equals("\u005a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("05A");
      Expect.expect(unicodedString.equals("\u005a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("005A");
      Expect.expect(unicodedString.equals("\u005a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("5b");
      Expect.expect(unicodedString.equals("\u005b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("05b");
      Expect.expect(unicodedString.equals("\u005b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("005b");
      Expect.expect(unicodedString.equals("\u005b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("5B");
      Expect.expect(unicodedString.equals("\u005b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("05B");
      Expect.expect(unicodedString.equals("\u005b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("005B");
      Expect.expect(unicodedString.equals("\u005b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("5c");
      Expect.expect(unicodedString.equals("\\"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("05c");
      Expect.expect(unicodedString.equals("\\"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("005c");
      Expect.expect(unicodedString.equals("\\"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("5C");
      Expect.expect(unicodedString.equals("\\"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("05C");
      Expect.expect(unicodedString.equals("\\"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("005C");
      Expect.expect(unicodedString.equals("\\"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("5d");
      Expect.expect(unicodedString.equals("\u005d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("05d");
      Expect.expect(unicodedString.equals("\u005d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("005d");
      Expect.expect(unicodedString.equals("\u005d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("5D");
      Expect.expect(unicodedString.equals("\u005d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("05D");
      Expect.expect(unicodedString.equals("\u005d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("005D");
      Expect.expect(unicodedString.equals("\u005d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("5e");
      Expect.expect(unicodedString.equals("\u005e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("05e");
      Expect.expect(unicodedString.equals("\u005e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("005e");
      Expect.expect(unicodedString.equals("\u005e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("5E");
      Expect.expect(unicodedString.equals("\u005e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("05E");
      Expect.expect(unicodedString.equals("\u005e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("005E");
      Expect.expect(unicodedString.equals("\u005e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("5f");
      Expect.expect(unicodedString.equals("\u005f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("05f");
      Expect.expect(unicodedString.equals("\u005f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("005f");
      Expect.expect(unicodedString.equals("\u005f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("5F");
      Expect.expect(unicodedString.equals("\u005f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("05F");
      Expect.expect(unicodedString.equals("\u005f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("005F");
      Expect.expect(unicodedString.equals("\u005f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("60");
      Expect.expect(unicodedString.equals("\u0060"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("060");
      Expect.expect(unicodedString.equals("\u0060"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0060");
      Expect.expect(unicodedString.equals("\u0060"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("61");
      Expect.expect(unicodedString.equals("\u0061"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("061");
      Expect.expect(unicodedString.equals("\u0061"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0061");
      Expect.expect(unicodedString.equals("\u0061"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("62");
      Expect.expect(unicodedString.equals("\u0062"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("062");
      Expect.expect(unicodedString.equals("\u0062"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0062");
      Expect.expect(unicodedString.equals("\u0062"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("63");
      Expect.expect(unicodedString.equals("\u0063"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("063");
      Expect.expect(unicodedString.equals("\u0063"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0063");
      Expect.expect(unicodedString.equals("\u0063"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("64");
      Expect.expect(unicodedString.equals("\u0064"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("064");
      Expect.expect(unicodedString.equals("\u0064"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0064");
      Expect.expect(unicodedString.equals("\u0064"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("65");
      Expect.expect(unicodedString.equals("\u0065"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("065");
      Expect.expect(unicodedString.equals("\u0065"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0065");
      Expect.expect(unicodedString.equals("\u0065"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("66");
      Expect.expect(unicodedString.equals("\u0066"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("066");
      Expect.expect(unicodedString.equals("\u0066"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0066");
      Expect.expect(unicodedString.equals("\u0066"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("67");
      Expect.expect(unicodedString.equals("\u0067"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("067");
      Expect.expect(unicodedString.equals("\u0067"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0067");
      Expect.expect(unicodedString.equals("\u0067"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("68");
      Expect.expect(unicodedString.equals("\u0068"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("068");
      Expect.expect(unicodedString.equals("\u0068"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0068");
      Expect.expect(unicodedString.equals("\u0068"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("69");
      Expect.expect(unicodedString.equals("\u0069"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("069");
      Expect.expect(unicodedString.equals("\u0069"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0069");
      Expect.expect(unicodedString.equals("\u0069"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("6a");
      Expect.expect(unicodedString.equals("\u006a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("06a");
      Expect.expect(unicodedString.equals("\u006a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("006a");
      Expect.expect(unicodedString.equals("\u006a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("6A");
      Expect.expect(unicodedString.equals("\u006a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("06A");
      Expect.expect(unicodedString.equals("\u006a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("006A");
      Expect.expect(unicodedString.equals("\u006a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("6b");
      Expect.expect(unicodedString.equals("\u006b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("06b");
      Expect.expect(unicodedString.equals("\u006b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("006b");
      Expect.expect(unicodedString.equals("\u006b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("6B");
      Expect.expect(unicodedString.equals("\u006b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("06B");
      Expect.expect(unicodedString.equals("\u006b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("006B");
      Expect.expect(unicodedString.equals("\u006b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("6c");
      Expect.expect(unicodedString.equals("\u006c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("06c");
      Expect.expect(unicodedString.equals("\u006c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("006c");
      Expect.expect(unicodedString.equals("\u006c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("6C");
      Expect.expect(unicodedString.equals("\u006c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("06C");
      Expect.expect(unicodedString.equals("\u006c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("006C");
      Expect.expect(unicodedString.equals("\u006c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("6d");
      Expect.expect(unicodedString.equals("\u006d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("06d");
      Expect.expect(unicodedString.equals("\u006d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("006d");
      Expect.expect(unicodedString.equals("\u006d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("6D");
      Expect.expect(unicodedString.equals("\u006d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("06D");
      Expect.expect(unicodedString.equals("\u006d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("006D");
      Expect.expect(unicodedString.equals("\u006d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("6e");
      Expect.expect(unicodedString.equals("\u006e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("06e");
      Expect.expect(unicodedString.equals("\u006e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("006e");
      Expect.expect(unicodedString.equals("\u006e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("6E");
      Expect.expect(unicodedString.equals("\u006e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("06E");
      Expect.expect(unicodedString.equals("\u006e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("006E");
      Expect.expect(unicodedString.equals("\u006e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("6f");
      Expect.expect(unicodedString.equals("\u006f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("06f");
      Expect.expect(unicodedString.equals("\u006f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("006f");
      Expect.expect(unicodedString.equals("\u006f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("6F");
      Expect.expect(unicodedString.equals("\u006f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("06F");
      Expect.expect(unicodedString.equals("\u006f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("006F");
      Expect.expect(unicodedString.equals("\u006f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("70");
      Expect.expect(unicodedString.equals("\u0070"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("070");
      Expect.expect(unicodedString.equals("\u0070"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0070");
      Expect.expect(unicodedString.equals("\u0070"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("71");
      Expect.expect(unicodedString.equals("\u0071"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("071");
      Expect.expect(unicodedString.equals("\u0071"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0071");
      Expect.expect(unicodedString.equals("\u0071"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("72");
      Expect.expect(unicodedString.equals("\u0072"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("072");
      Expect.expect(unicodedString.equals("\u0072"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0072");
      Expect.expect(unicodedString.equals("\u0072"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("73");
      Expect.expect(unicodedString.equals("\u0073"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("073");
      Expect.expect(unicodedString.equals("\u0073"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0073");
      Expect.expect(unicodedString.equals("\u0073"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("74");
      Expect.expect(unicodedString.equals("\u0074"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("074");
      Expect.expect(unicodedString.equals("\u0074"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0074");
      Expect.expect(unicodedString.equals("\u0074"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("75");
      Expect.expect(unicodedString.equals("\u0075"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("075");
      Expect.expect(unicodedString.equals("\u0075"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0075");
      Expect.expect(unicodedString.equals("\u0075"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("76");
      Expect.expect(unicodedString.equals("\u0076"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("076");
      Expect.expect(unicodedString.equals("\u0076"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0076");
      Expect.expect(unicodedString.equals("\u0076"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("77");
      Expect.expect(unicodedString.equals("\u0077"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("077");
      Expect.expect(unicodedString.equals("\u0077"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0077");
      Expect.expect(unicodedString.equals("\u0077"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("78");
      Expect.expect(unicodedString.equals("\u0078"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("078");
      Expect.expect(unicodedString.equals("\u0078"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0078");
      Expect.expect(unicodedString.equals("\u0078"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("79");
      Expect.expect(unicodedString.equals("\u0079"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("079");
      Expect.expect(unicodedString.equals("\u0079"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0079");
      Expect.expect(unicodedString.equals("\u0079"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("7a");
      Expect.expect(unicodedString.equals("\u007a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("07a");
      Expect.expect(unicodedString.equals("\u007a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("007a");
      Expect.expect(unicodedString.equals("\u007a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("7A");
      Expect.expect(unicodedString.equals("\u007a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("07A");
      Expect.expect(unicodedString.equals("\u007a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("007A");
      Expect.expect(unicodedString.equals("\u007a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("7b");
      Expect.expect(unicodedString.equals("\u007b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("07b");
      Expect.expect(unicodedString.equals("\u007b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("007b");
      Expect.expect(unicodedString.equals("\u007b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("7B");
      Expect.expect(unicodedString.equals("\u007b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("07B");
      Expect.expect(unicodedString.equals("\u007b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("007B");
      Expect.expect(unicodedString.equals("\u007b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("7c");
      Expect.expect(unicodedString.equals("\u007c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("07c");
      Expect.expect(unicodedString.equals("\u007c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("007c");
      Expect.expect(unicodedString.equals("\u007c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("7C");
      Expect.expect(unicodedString.equals("\u007c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("07C");
      Expect.expect(unicodedString.equals("\u007c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("007C");
      Expect.expect(unicodedString.equals("\u007c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("7d");
      Expect.expect(unicodedString.equals("\u007d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("07d");
      Expect.expect(unicodedString.equals("\u007d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("007d");
      Expect.expect(unicodedString.equals("\u007d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("7D");
      Expect.expect(unicodedString.equals("\u007d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("07D");
      Expect.expect(unicodedString.equals("\u007d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("007D");
      Expect.expect(unicodedString.equals("\u007d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("7e");
      Expect.expect(unicodedString.equals("\u007e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("07e");
      Expect.expect(unicodedString.equals("\u007e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("007e");
      Expect.expect(unicodedString.equals("\u007e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("7E");
      Expect.expect(unicodedString.equals("\u007e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("07E");
      Expect.expect(unicodedString.equals("\u007e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("007E");
      Expect.expect(unicodedString.equals("\u007e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("7f");
      Expect.expect(unicodedString.equals("\u007f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("07f");
      Expect.expect(unicodedString.equals("\u007f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("007f");
      Expect.expect(unicodedString.equals("\u007f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("7F");
      Expect.expect(unicodedString.equals("\u007f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("07F");
      Expect.expect(unicodedString.equals("\u007f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("007F");
      Expect.expect(unicodedString.equals("\u007f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("80");
      Expect.expect(unicodedString.equals("\u0080"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("080");
      Expect.expect(unicodedString.equals("\u0080"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0080");
      Expect.expect(unicodedString.equals("\u0080"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("81");
      Expect.expect(unicodedString.equals("\u0081"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("081");
      Expect.expect(unicodedString.equals("\u0081"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0081");
      Expect.expect(unicodedString.equals("\u0081"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("82");
      Expect.expect(unicodedString.equals("\u0082"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("082");
      Expect.expect(unicodedString.equals("\u0082"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0082");
      Expect.expect(unicodedString.equals("\u0082"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("83");
      Expect.expect(unicodedString.equals("\u0083"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("083");
      Expect.expect(unicodedString.equals("\u0083"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0083");
      Expect.expect(unicodedString.equals("\u0083"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("84");
      Expect.expect(unicodedString.equals("\u0084"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("084");
      Expect.expect(unicodedString.equals("\u0084"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0084");
      Expect.expect(unicodedString.equals("\u0084"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("85");
      Expect.expect(unicodedString.equals("\u0085"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("085");
      Expect.expect(unicodedString.equals("\u0085"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0085");
      Expect.expect(unicodedString.equals("\u0085"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("86");
      Expect.expect(unicodedString.equals("\u0086"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("086");
      Expect.expect(unicodedString.equals("\u0086"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0086");
      Expect.expect(unicodedString.equals("\u0086"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("87");
      Expect.expect(unicodedString.equals("\u0087"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("087");
      Expect.expect(unicodedString.equals("\u0087"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0087");
      Expect.expect(unicodedString.equals("\u0087"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("88");
      Expect.expect(unicodedString.equals("\u0088"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("088");
      Expect.expect(unicodedString.equals("\u0088"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0088");
      Expect.expect(unicodedString.equals("\u0088"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("89");
      Expect.expect(unicodedString.equals("\u0089"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("089");
      Expect.expect(unicodedString.equals("\u0089"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0089");
      Expect.expect(unicodedString.equals("\u0089"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("8a");
      Expect.expect(unicodedString.equals("\u008a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("08a");
      Expect.expect(unicodedString.equals("\u008a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("008a");
      Expect.expect(unicodedString.equals("\u008a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("8A");
      Expect.expect(unicodedString.equals("\u008a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("08A");
      Expect.expect(unicodedString.equals("\u008a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("008A");
      Expect.expect(unicodedString.equals("\u008a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("8b");
      Expect.expect(unicodedString.equals("\u008b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("08b");
      Expect.expect(unicodedString.equals("\u008b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("008b");
      Expect.expect(unicodedString.equals("\u008b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("8B");
      Expect.expect(unicodedString.equals("\u008b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("08B");
      Expect.expect(unicodedString.equals("\u008b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("008B");
      Expect.expect(unicodedString.equals("\u008b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("8c");
      Expect.expect(unicodedString.equals("\u008c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("08c");
      Expect.expect(unicodedString.equals("\u008c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("008c");
      Expect.expect(unicodedString.equals("\u008c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("8C");
      Expect.expect(unicodedString.equals("\u008c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("08C");
      Expect.expect(unicodedString.equals("\u008c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("008C");
      Expect.expect(unicodedString.equals("\u008c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("8d");
      Expect.expect(unicodedString.equals("\u008d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("08d");
      Expect.expect(unicodedString.equals("\u008d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("008d");
      Expect.expect(unicodedString.equals("\u008d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("8D");
      Expect.expect(unicodedString.equals("\u008d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("08D");
      Expect.expect(unicodedString.equals("\u008d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("008D");
      Expect.expect(unicodedString.equals("\u008d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("8e");
      Expect.expect(unicodedString.equals("\u008e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("08e");
      Expect.expect(unicodedString.equals("\u008e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("008e");
      Expect.expect(unicodedString.equals("\u008e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("8E");
      Expect.expect(unicodedString.equals("\u008e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("08E");
      Expect.expect(unicodedString.equals("\u008e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("008E");
      Expect.expect(unicodedString.equals("\u008e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("8f");
      Expect.expect(unicodedString.equals("\u008f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("08f");
      Expect.expect(unicodedString.equals("\u008f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("008f");
      Expect.expect(unicodedString.equals("\u008f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("8F");
      Expect.expect(unicodedString.equals("\u008f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("08F");
      Expect.expect(unicodedString.equals("\u008f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("008F");
      Expect.expect(unicodedString.equals("\u008f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("90");
      Expect.expect(unicodedString.equals("\u0090"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("090");
      Expect.expect(unicodedString.equals("\u0090"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0090");
      Expect.expect(unicodedString.equals("\u0090"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("91");
      Expect.expect(unicodedString.equals("\u0091"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("091");
      Expect.expect(unicodedString.equals("\u0091"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0091");
      Expect.expect(unicodedString.equals("\u0091"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("92");
      Expect.expect(unicodedString.equals("\u0092"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("092");
      Expect.expect(unicodedString.equals("\u0092"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0092");
      Expect.expect(unicodedString.equals("\u0092"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("93");
      Expect.expect(unicodedString.equals("\u0093"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("093");
      Expect.expect(unicodedString.equals("\u0093"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0093");
      Expect.expect(unicodedString.equals("\u0093"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("94");
      Expect.expect(unicodedString.equals("\u0094"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("094");
      Expect.expect(unicodedString.equals("\u0094"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0094");
      Expect.expect(unicodedString.equals("\u0094"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("95");
      Expect.expect(unicodedString.equals("\u0095"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("095");
      Expect.expect(unicodedString.equals("\u0095"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0095");
      Expect.expect(unicodedString.equals("\u0095"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("96");
      Expect.expect(unicodedString.equals("\u0096"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("096");
      Expect.expect(unicodedString.equals("\u0096"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0096");
      Expect.expect(unicodedString.equals("\u0096"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("97");
      Expect.expect(unicodedString.equals("\u0097"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("097");
      Expect.expect(unicodedString.equals("\u0097"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0097");
      Expect.expect(unicodedString.equals("\u0097"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("98");
      Expect.expect(unicodedString.equals("\u0098"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("098");
      Expect.expect(unicodedString.equals("\u0098"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0098");
      Expect.expect(unicodedString.equals("\u0098"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("99");
      Expect.expect(unicodedString.equals("\u0099"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("099");
      Expect.expect(unicodedString.equals("\u0099"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0099");
      Expect.expect(unicodedString.equals("\u0099"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("9a");
      Expect.expect(unicodedString.equals("\u009a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("09a");
      Expect.expect(unicodedString.equals("\u009a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("009a");
      Expect.expect(unicodedString.equals("\u009a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("9A");
      Expect.expect(unicodedString.equals("\u009a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("09A");
      Expect.expect(unicodedString.equals("\u009a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("009A");
      Expect.expect(unicodedString.equals("\u009a"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("9b");
      Expect.expect(unicodedString.equals("\u009b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("09b");
      Expect.expect(unicodedString.equals("\u009b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("009b");
      Expect.expect(unicodedString.equals("\u009b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("9B");
      Expect.expect(unicodedString.equals("\u009b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("09B");
      Expect.expect(unicodedString.equals("\u009b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("009B");
      Expect.expect(unicodedString.equals("\u009b"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("9c");
      Expect.expect(unicodedString.equals("\u009c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("09c");
      Expect.expect(unicodedString.equals("\u009c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("009c");
      Expect.expect(unicodedString.equals("\u009c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("9C");
      Expect.expect(unicodedString.equals("\u009c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("09C");
      Expect.expect(unicodedString.equals("\u009c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("009C");
      Expect.expect(unicodedString.equals("\u009c"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("9d");
      Expect.expect(unicodedString.equals("\u009d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("09d");
      Expect.expect(unicodedString.equals("\u009d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("009d");
      Expect.expect(unicodedString.equals("\u009d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("9D");
      Expect.expect(unicodedString.equals("\u009d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("09D");
      Expect.expect(unicodedString.equals("\u009d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("009D");
      Expect.expect(unicodedString.equals("\u009d"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("9e");
      Expect.expect(unicodedString.equals("\u009e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("09e");
      Expect.expect(unicodedString.equals("\u009e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("009e");
      Expect.expect(unicodedString.equals("\u009e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("9E");
      Expect.expect(unicodedString.equals("\u009e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("09E");
      Expect.expect(unicodedString.equals("\u009e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("009E");
      Expect.expect(unicodedString.equals("\u009e"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("9f");
      Expect.expect(unicodedString.equals("\u009f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("09f");
      Expect.expect(unicodedString.equals("\u009f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("009f");
      Expect.expect(unicodedString.equals("\u009f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("9F");
      Expect.expect(unicodedString.equals("\u009f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("09F");
      Expect.expect(unicodedString.equals("\u009f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("009F");
      Expect.expect(unicodedString.equals("\u009f"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("a0");
      Expect.expect(unicodedString.equals("\u00a0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0a0");
      Expect.expect(unicodedString.equals("\u00a0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00a0");
      Expect.expect(unicodedString.equals("\u00a0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("a1");
      Expect.expect(unicodedString.equals("\u00a1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0a1");
      Expect.expect(unicodedString.equals("\u00a1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00a1");
      Expect.expect(unicodedString.equals("\u00a1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("a2");
      Expect.expect(unicodedString.equals("\u00a2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0a2");
      Expect.expect(unicodedString.equals("\u00a2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00a2");
      Expect.expect(unicodedString.equals("\u00a2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("a3");
      Expect.expect(unicodedString.equals("\u00a3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0a3");
      Expect.expect(unicodedString.equals("\u00a3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00a3");
      Expect.expect(unicodedString.equals("\u00a3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("a4");
      Expect.expect(unicodedString.equals("\u00a4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0a4");
      Expect.expect(unicodedString.equals("\u00a4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00a4");
      Expect.expect(unicodedString.equals("\u00a4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("a5");
      Expect.expect(unicodedString.equals("\u00a5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0a5");
      Expect.expect(unicodedString.equals("\u00a5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00a5");
      Expect.expect(unicodedString.equals("\u00a5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("a6");
      Expect.expect(unicodedString.equals("\u00a6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0a6");
      Expect.expect(unicodedString.equals("\u00a6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00a6");
      Expect.expect(unicodedString.equals("\u00a6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("a7");
      Expect.expect(unicodedString.equals("\u00a7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0a7");
      Expect.expect(unicodedString.equals("\u00a7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00a7");
      Expect.expect(unicodedString.equals("\u00a7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("a8");
      Expect.expect(unicodedString.equals("\u00a8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0a8");
      Expect.expect(unicodedString.equals("\u00a8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00a8");
      Expect.expect(unicodedString.equals("\u00a8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("a9");
      Expect.expect(unicodedString.equals("\u00a9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0a9");
      Expect.expect(unicodedString.equals("\u00a9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00a9");
      Expect.expect(unicodedString.equals("\u00a9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("aa");
      Expect.expect(unicodedString.equals("\u00aa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0aa");
      Expect.expect(unicodedString.equals("\u00aa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00aa");
      Expect.expect(unicodedString.equals("\u00aa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("aA");
      Expect.expect(unicodedString.equals("\u00aa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0aA");
      Expect.expect(unicodedString.equals("\u00aa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00aA");
      Expect.expect(unicodedString.equals("\u00aa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("ab");
      Expect.expect(unicodedString.equals("\u00ab"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0ab");
      Expect.expect(unicodedString.equals("\u00ab"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00ab");
      Expect.expect(unicodedString.equals("\u00ab"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("aB");
      Expect.expect(unicodedString.equals("\u00ab"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0aB");
      Expect.expect(unicodedString.equals("\u00ab"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00aB");
      Expect.expect(unicodedString.equals("\u00ab"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("ac");
      Expect.expect(unicodedString.equals("\u00ac"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0ac");
      Expect.expect(unicodedString.equals("\u00ac"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00ac");
      Expect.expect(unicodedString.equals("\u00ac"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("aC");
      Expect.expect(unicodedString.equals("\u00ac"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0aC");
      Expect.expect(unicodedString.equals("\u00ac"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00aC");
      Expect.expect(unicodedString.equals("\u00ac"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("ad");
      Expect.expect(unicodedString.equals("\u00ad"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0ad");
      Expect.expect(unicodedString.equals("\u00ad"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00ad");
      Expect.expect(unicodedString.equals("\u00ad"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("aD");
      Expect.expect(unicodedString.equals("\u00ad"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0aD");
      Expect.expect(unicodedString.equals("\u00ad"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00aD");
      Expect.expect(unicodedString.equals("\u00ad"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("ae");
      Expect.expect(unicodedString.equals("\u00ae"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0ae");
      Expect.expect(unicodedString.equals("\u00ae"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00ae");
      Expect.expect(unicodedString.equals("\u00ae"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("aE");
      Expect.expect(unicodedString.equals("\u00ae"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0aE");
      Expect.expect(unicodedString.equals("\u00ae"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00aE");
      Expect.expect(unicodedString.equals("\u00ae"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("af");
      Expect.expect(unicodedString.equals("\u00af"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0af");
      Expect.expect(unicodedString.equals("\u00af"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00af");
      Expect.expect(unicodedString.equals("\u00af"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("aF");
      Expect.expect(unicodedString.equals("\u00af"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0aF");
      Expect.expect(unicodedString.equals("\u00af"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00aF");
      Expect.expect(unicodedString.equals("\u00af"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("A0");
      Expect.expect(unicodedString.equals("\u00a0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0A0");
      Expect.expect(unicodedString.equals("\u00a0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00A0");
      Expect.expect(unicodedString.equals("\u00a0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("A1");
      Expect.expect(unicodedString.equals("\u00a1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0A1");
      Expect.expect(unicodedString.equals("\u00a1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00A1");
      Expect.expect(unicodedString.equals("\u00a1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("A2");
      Expect.expect(unicodedString.equals("\u00a2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0A2");
      Expect.expect(unicodedString.equals("\u00a2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00A2");
      Expect.expect(unicodedString.equals("\u00a2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("A3");
      Expect.expect(unicodedString.equals("\u00a3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0A3");
      Expect.expect(unicodedString.equals("\u00a3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00A3");
      Expect.expect(unicodedString.equals("\u00a3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("A4");
      Expect.expect(unicodedString.equals("\u00a4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0A4");
      Expect.expect(unicodedString.equals("\u00a4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00A4");
      Expect.expect(unicodedString.equals("\u00a4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("A5");
      Expect.expect(unicodedString.equals("\u00a5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0A5");
      Expect.expect(unicodedString.equals("\u00a5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00A5");
      Expect.expect(unicodedString.equals("\u00a5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("A6");
      Expect.expect(unicodedString.equals("\u00a6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0A6");
      Expect.expect(unicodedString.equals("\u00a6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00A6");
      Expect.expect(unicodedString.equals("\u00a6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("A7");
      Expect.expect(unicodedString.equals("\u00a7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0A7");
      Expect.expect(unicodedString.equals("\u00a7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00A7");
      Expect.expect(unicodedString.equals("\u00a7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("A8");
      Expect.expect(unicodedString.equals("\u00a8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0A8");
      Expect.expect(unicodedString.equals("\u00a8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00A8");
      Expect.expect(unicodedString.equals("\u00a8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("A9");
      Expect.expect(unicodedString.equals("\u00a9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0A9");
      Expect.expect(unicodedString.equals("\u00a9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00A9");
      Expect.expect(unicodedString.equals("\u00a9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Aa");
      Expect.expect(unicodedString.equals("\u00aa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Aa");
      Expect.expect(unicodedString.equals("\u00aa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Aa");
      Expect.expect(unicodedString.equals("\u00aa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("AA");
      Expect.expect(unicodedString.equals("\u00aa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0AA");
      Expect.expect(unicodedString.equals("\u00aa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00AA");
      Expect.expect(unicodedString.equals("\u00aa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Ab");
      Expect.expect(unicodedString.equals("\u00ab"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Ab");
      Expect.expect(unicodedString.equals("\u00ab"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Ab");
      Expect.expect(unicodedString.equals("\u00ab"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("AB");
      Expect.expect(unicodedString.equals("\u00ab"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0AB");
      Expect.expect(unicodedString.equals("\u00ab"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00AB");
      Expect.expect(unicodedString.equals("\u00ab"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Ac");
      Expect.expect(unicodedString.equals("\u00ac"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Ac");
      Expect.expect(unicodedString.equals("\u00ac"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Ac");
      Expect.expect(unicodedString.equals("\u00ac"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("AC");
      Expect.expect(unicodedString.equals("\u00ac"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0AC");
      Expect.expect(unicodedString.equals("\u00ac"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00AC");
      Expect.expect(unicodedString.equals("\u00ac"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Ad");
      Expect.expect(unicodedString.equals("\u00ad"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Ad");
      Expect.expect(unicodedString.equals("\u00ad"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Ad");
      Expect.expect(unicodedString.equals("\u00ad"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("AD");
      Expect.expect(unicodedString.equals("\u00ad"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0AD");
      Expect.expect(unicodedString.equals("\u00ad"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00AD");
      Expect.expect(unicodedString.equals("\u00ad"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Ae");
      Expect.expect(unicodedString.equals("\u00ae"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Ae");
      Expect.expect(unicodedString.equals("\u00ae"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Ae");
      Expect.expect(unicodedString.equals("\u00ae"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("AE");
      Expect.expect(unicodedString.equals("\u00ae"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0AE");
      Expect.expect(unicodedString.equals("\u00ae"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00AE");
      Expect.expect(unicodedString.equals("\u00ae"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Af");
      Expect.expect(unicodedString.equals("\u00af"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Af");
      Expect.expect(unicodedString.equals("\u00af"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Af");
      Expect.expect(unicodedString.equals("\u00af"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("AF");
      Expect.expect(unicodedString.equals("\u00af"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0AF");
      Expect.expect(unicodedString.equals("\u00af"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00AF");
      Expect.expect(unicodedString.equals("\u00af"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("b0");
      Expect.expect(unicodedString.equals("\u00b0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0b0");
      Expect.expect(unicodedString.equals("\u00b0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00b0");
      Expect.expect(unicodedString.equals("\u00b0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("b1");
      Expect.expect(unicodedString.equals("\u00b1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0b1");
      Expect.expect(unicodedString.equals("\u00b1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00b1");
      Expect.expect(unicodedString.equals("\u00b1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("b2");
      Expect.expect(unicodedString.equals("\u00b2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0b2");
      Expect.expect(unicodedString.equals("\u00b2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00b2");
      Expect.expect(unicodedString.equals("\u00b2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("b3");
      Expect.expect(unicodedString.equals("\u00b3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0b3");
      Expect.expect(unicodedString.equals("\u00b3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00b3");
      Expect.expect(unicodedString.equals("\u00b3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("b4");
      Expect.expect(unicodedString.equals("\u00b4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0b4");
      Expect.expect(unicodedString.equals("\u00b4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00b4");
      Expect.expect(unicodedString.equals("\u00b4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("b5");
      Expect.expect(unicodedString.equals("\u00b5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0b5");
      Expect.expect(unicodedString.equals("\u00b5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00b5");
      Expect.expect(unicodedString.equals("\u00b5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("b6");
      Expect.expect(unicodedString.equals("\u00b6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0b6");
      Expect.expect(unicodedString.equals("\u00b6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00b6");
      Expect.expect(unicodedString.equals("\u00b6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("b7");
      Expect.expect(unicodedString.equals("\u00b7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0b7");
      Expect.expect(unicodedString.equals("\u00b7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00b7");
      Expect.expect(unicodedString.equals("\u00b7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("b8");
      Expect.expect(unicodedString.equals("\u00b8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0b8");
      Expect.expect(unicodedString.equals("\u00b8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00b8");
      Expect.expect(unicodedString.equals("\u00b8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("b9");
      Expect.expect(unicodedString.equals("\u00b9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0b9");
      Expect.expect(unicodedString.equals("\u00b9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00b9");
      Expect.expect(unicodedString.equals("\u00b9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("ba");
      Expect.expect(unicodedString.equals("\u00ba"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0ba");
      Expect.expect(unicodedString.equals("\u00ba"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00ba");
      Expect.expect(unicodedString.equals("\u00ba"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("bA");
      Expect.expect(unicodedString.equals("\u00ba"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0bA");
      Expect.expect(unicodedString.equals("\u00ba"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00bA");
      Expect.expect(unicodedString.equals("\u00ba"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("bb");
      Expect.expect(unicodedString.equals("\u00bb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0bb");
      Expect.expect(unicodedString.equals("\u00bb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00bb");
      Expect.expect(unicodedString.equals("\u00bb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("bB");
      Expect.expect(unicodedString.equals("\u00bb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0bB");
      Expect.expect(unicodedString.equals("\u00bb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00bB");
      Expect.expect(unicodedString.equals("\u00bb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("bc");
      Expect.expect(unicodedString.equals("\u00bc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0bc");
      Expect.expect(unicodedString.equals("\u00bc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00bc");
      Expect.expect(unicodedString.equals("\u00bc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("bC");
      Expect.expect(unicodedString.equals("\u00bc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0bC");
      Expect.expect(unicodedString.equals("\u00bc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00bC");
      Expect.expect(unicodedString.equals("\u00bc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("bd");
      Expect.expect(unicodedString.equals("\u00bd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0bd");
      Expect.expect(unicodedString.equals("\u00bd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00bd");
      Expect.expect(unicodedString.equals("\u00bd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("bD");
      Expect.expect(unicodedString.equals("\u00bd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0bD");
      Expect.expect(unicodedString.equals("\u00bd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00bD");
      Expect.expect(unicodedString.equals("\u00bd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("be");
      Expect.expect(unicodedString.equals("\u00be"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0be");
      Expect.expect(unicodedString.equals("\u00be"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00be");
      Expect.expect(unicodedString.equals("\u00be"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("bE");
      Expect.expect(unicodedString.equals("\u00be"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0bE");
      Expect.expect(unicodedString.equals("\u00be"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00bE");
      Expect.expect(unicodedString.equals("\u00be"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("bf");
      Expect.expect(unicodedString.equals("\u00bf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0bf");
      Expect.expect(unicodedString.equals("\u00bf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00bf");
      Expect.expect(unicodedString.equals("\u00bf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("bF");
      Expect.expect(unicodedString.equals("\u00bf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0bF");
      Expect.expect(unicodedString.equals("\u00bf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00bF");
      Expect.expect(unicodedString.equals("\u00bf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("B0");
      Expect.expect(unicodedString.equals("\u00b0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0B0");
      Expect.expect(unicodedString.equals("\u00b0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00B0");
      Expect.expect(unicodedString.equals("\u00b0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("B1");
      Expect.expect(unicodedString.equals("\u00b1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0B1");
      Expect.expect(unicodedString.equals("\u00b1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00B1");
      Expect.expect(unicodedString.equals("\u00b1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("B2");
      Expect.expect(unicodedString.equals("\u00b2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0B2");
      Expect.expect(unicodedString.equals("\u00b2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00B2");
      Expect.expect(unicodedString.equals("\u00b2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("B3");
      Expect.expect(unicodedString.equals("\u00b3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0B3");
      Expect.expect(unicodedString.equals("\u00b3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00B3");
      Expect.expect(unicodedString.equals("\u00b3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("B4");
      Expect.expect(unicodedString.equals("\u00b4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0B4");
      Expect.expect(unicodedString.equals("\u00b4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00B4");
      Expect.expect(unicodedString.equals("\u00b4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("B5");
      Expect.expect(unicodedString.equals("\u00b5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0B5");
      Expect.expect(unicodedString.equals("\u00b5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00B5");
      Expect.expect(unicodedString.equals("\u00b5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("B6");
      Expect.expect(unicodedString.equals("\u00b6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0B6");
      Expect.expect(unicodedString.equals("\u00b6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00B6");
      Expect.expect(unicodedString.equals("\u00b6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("B7");
      Expect.expect(unicodedString.equals("\u00b7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0B7");
      Expect.expect(unicodedString.equals("\u00b7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00B7");
      Expect.expect(unicodedString.equals("\u00b7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("B8");
      Expect.expect(unicodedString.equals("\u00b8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0B8");
      Expect.expect(unicodedString.equals("\u00b8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00B8");
      Expect.expect(unicodedString.equals("\u00b8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("B9");
      Expect.expect(unicodedString.equals("\u00b9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0B9");
      Expect.expect(unicodedString.equals("\u00b9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00B9");
      Expect.expect(unicodedString.equals("\u00b9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Ba");
      Expect.expect(unicodedString.equals("\u00ba"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Ba");
      Expect.expect(unicodedString.equals("\u00ba"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Ba");
      Expect.expect(unicodedString.equals("\u00ba"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("BA");
      Expect.expect(unicodedString.equals("\u00ba"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0BA");
      Expect.expect(unicodedString.equals("\u00ba"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00BA");
      Expect.expect(unicodedString.equals("\u00ba"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Bb");
      Expect.expect(unicodedString.equals("\u00bb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Bb");
      Expect.expect(unicodedString.equals("\u00bb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Bb");
      Expect.expect(unicodedString.equals("\u00bb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("BB");
      Expect.expect(unicodedString.equals("\u00bb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0BB");
      Expect.expect(unicodedString.equals("\u00bb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00BB");
      Expect.expect(unicodedString.equals("\u00bb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Bc");
      Expect.expect(unicodedString.equals("\u00bc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Bc");
      Expect.expect(unicodedString.equals("\u00bc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Bc");
      Expect.expect(unicodedString.equals("\u00bc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("BC");
      Expect.expect(unicodedString.equals("\u00bc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0BC");
      Expect.expect(unicodedString.equals("\u00bc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00BC");
      Expect.expect(unicodedString.equals("\u00bc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Bd");
      Expect.expect(unicodedString.equals("\u00bd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Bd");
      Expect.expect(unicodedString.equals("\u00bd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Bd");
      Expect.expect(unicodedString.equals("\u00bd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("BD");
      Expect.expect(unicodedString.equals("\u00bd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0BD");
      Expect.expect(unicodedString.equals("\u00bd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00BD");
      Expect.expect(unicodedString.equals("\u00bd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Be");
      Expect.expect(unicodedString.equals("\u00be"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Be");
      Expect.expect(unicodedString.equals("\u00be"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Be");
      Expect.expect(unicodedString.equals("\u00be"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("BE");
      Expect.expect(unicodedString.equals("\u00be"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0BE");
      Expect.expect(unicodedString.equals("\u00be"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00BE");
      Expect.expect(unicodedString.equals("\u00be"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Bf");
      Expect.expect(unicodedString.equals("\u00bf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Bf");
      Expect.expect(unicodedString.equals("\u00bf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Bf");
      Expect.expect(unicodedString.equals("\u00bf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("BF");
      Expect.expect(unicodedString.equals("\u00bf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0BF");
      Expect.expect(unicodedString.equals("\u00bf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00BF");
      Expect.expect(unicodedString.equals("\u00bf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("c0");
      Expect.expect(unicodedString.equals("\u00c0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0c0");
      Expect.expect(unicodedString.equals("\u00c0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00c0");
      Expect.expect(unicodedString.equals("\u00c0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("c1");
      Expect.expect(unicodedString.equals("\u00c1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0c1");
      Expect.expect(unicodedString.equals("\u00c1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00c1");
      Expect.expect(unicodedString.equals("\u00c1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("c2");
      Expect.expect(unicodedString.equals("\u00c2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0c2");
      Expect.expect(unicodedString.equals("\u00c2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00c2");
      Expect.expect(unicodedString.equals("\u00c2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("c3");
      Expect.expect(unicodedString.equals("\u00c3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0c3");
      Expect.expect(unicodedString.equals("\u00c3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00c3");
      Expect.expect(unicodedString.equals("\u00c3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("c4");
      Expect.expect(unicodedString.equals("\u00c4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0c4");
      Expect.expect(unicodedString.equals("\u00c4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00c4");
      Expect.expect(unicodedString.equals("\u00c4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("c5");
      Expect.expect(unicodedString.equals("\u00c5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0c5");
      Expect.expect(unicodedString.equals("\u00c5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00c5");
      Expect.expect(unicodedString.equals("\u00c5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("c6");
      Expect.expect(unicodedString.equals("\u00c6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0c6");
      Expect.expect(unicodedString.equals("\u00c6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00c6");
      Expect.expect(unicodedString.equals("\u00c6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("c7");
      Expect.expect(unicodedString.equals("\u00c7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0c7");
      Expect.expect(unicodedString.equals("\u00c7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00c7");
      Expect.expect(unicodedString.equals("\u00c7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("c8");
      Expect.expect(unicodedString.equals("\u00c8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0c8");
      Expect.expect(unicodedString.equals("\u00c8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00c8");
      Expect.expect(unicodedString.equals("\u00c8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("c9");
      Expect.expect(unicodedString.equals("\u00c9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0c9");
      Expect.expect(unicodedString.equals("\u00c9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00c9");
      Expect.expect(unicodedString.equals("\u00c9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("ca");
      Expect.expect(unicodedString.equals("\u00ca"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0ca");
      Expect.expect(unicodedString.equals("\u00ca"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00ca");
      Expect.expect(unicodedString.equals("\u00ca"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("cA");
      Expect.expect(unicodedString.equals("\u00ca"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0cA");
      Expect.expect(unicodedString.equals("\u00ca"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00cA");
      Expect.expect(unicodedString.equals("\u00ca"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("cb");
      Expect.expect(unicodedString.equals("\u00cb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0cb");
      Expect.expect(unicodedString.equals("\u00cb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00cb");
      Expect.expect(unicodedString.equals("\u00cb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("cB");
      Expect.expect(unicodedString.equals("\u00cb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0cB");
      Expect.expect(unicodedString.equals("\u00cb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00cB");
      Expect.expect(unicodedString.equals("\u00cb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("cc");
      Expect.expect(unicodedString.equals("\u00cc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0cc");
      Expect.expect(unicodedString.equals("\u00cc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00cc");
      Expect.expect(unicodedString.equals("\u00cc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("cC");
      Expect.expect(unicodedString.equals("\u00cc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0cC");
      Expect.expect(unicodedString.equals("\u00cc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00cC");
      Expect.expect(unicodedString.equals("\u00cc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("cd");
      Expect.expect(unicodedString.equals("\u00cd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0cd");
      Expect.expect(unicodedString.equals("\u00cd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00cd");
      Expect.expect(unicodedString.equals("\u00cd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("cD");
      Expect.expect(unicodedString.equals("\u00cd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0cD");
      Expect.expect(unicodedString.equals("\u00cd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00cD");
      Expect.expect(unicodedString.equals("\u00cd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("ce");
      Expect.expect(unicodedString.equals("\u00ce"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0ce");
      Expect.expect(unicodedString.equals("\u00ce"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00ce");
      Expect.expect(unicodedString.equals("\u00ce"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("cE");
      Expect.expect(unicodedString.equals("\u00ce"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0cE");
      Expect.expect(unicodedString.equals("\u00ce"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00cE");
      Expect.expect(unicodedString.equals("\u00ce"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("cf");
      Expect.expect(unicodedString.equals("\u00cf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0cf");
      Expect.expect(unicodedString.equals("\u00cf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00cf");
      Expect.expect(unicodedString.equals("\u00cf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("cF");
      Expect.expect(unicodedString.equals("\u00cf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0cF");
      Expect.expect(unicodedString.equals("\u00cf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00cF");
      Expect.expect(unicodedString.equals("\u00cf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("C0");
      Expect.expect(unicodedString.equals("\u00c0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0C0");
      Expect.expect(unicodedString.equals("\u00c0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00C0");
      Expect.expect(unicodedString.equals("\u00c0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("C1");
      Expect.expect(unicodedString.equals("\u00c1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0C1");
      Expect.expect(unicodedString.equals("\u00c1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00C1");
      Expect.expect(unicodedString.equals("\u00c1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("C2");
      Expect.expect(unicodedString.equals("\u00c2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0C2");
      Expect.expect(unicodedString.equals("\u00c2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00C2");
      Expect.expect(unicodedString.equals("\u00c2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("C3");
      Expect.expect(unicodedString.equals("\u00c3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0C3");
      Expect.expect(unicodedString.equals("\u00c3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00C3");
      Expect.expect(unicodedString.equals("\u00c3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("C4");
      Expect.expect(unicodedString.equals("\u00c4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0C4");
      Expect.expect(unicodedString.equals("\u00c4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00C4");
      Expect.expect(unicodedString.equals("\u00c4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("C5");
      Expect.expect(unicodedString.equals("\u00c5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0C5");
      Expect.expect(unicodedString.equals("\u00c5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00C5");
      Expect.expect(unicodedString.equals("\u00c5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("C6");
      Expect.expect(unicodedString.equals("\u00c6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0C6");
      Expect.expect(unicodedString.equals("\u00c6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00C6");
      Expect.expect(unicodedString.equals("\u00c6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("C7");
      Expect.expect(unicodedString.equals("\u00c7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0C7");
      Expect.expect(unicodedString.equals("\u00c7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00C7");
      Expect.expect(unicodedString.equals("\u00c7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("C8");
      Expect.expect(unicodedString.equals("\u00c8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0C8");
      Expect.expect(unicodedString.equals("\u00c8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00C8");
      Expect.expect(unicodedString.equals("\u00c8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("C9");
      Expect.expect(unicodedString.equals("\u00c9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0C9");
      Expect.expect(unicodedString.equals("\u00c9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00C9");
      Expect.expect(unicodedString.equals("\u00c9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Ca");
      Expect.expect(unicodedString.equals("\u00ca"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Ca");
      Expect.expect(unicodedString.equals("\u00ca"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Ca");
      Expect.expect(unicodedString.equals("\u00ca"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("CA");
      Expect.expect(unicodedString.equals("\u00ca"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0CA");
      Expect.expect(unicodedString.equals("\u00ca"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00CA");
      Expect.expect(unicodedString.equals("\u00ca"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Cb");
      Expect.expect(unicodedString.equals("\u00cb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Cb");
      Expect.expect(unicodedString.equals("\u00cb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Cb");
      Expect.expect(unicodedString.equals("\u00cb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("CB");
      Expect.expect(unicodedString.equals("\u00cb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0CB");
      Expect.expect(unicodedString.equals("\u00cb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00CB");
      Expect.expect(unicodedString.equals("\u00cb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Cc");
      Expect.expect(unicodedString.equals("\u00cc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Cc");
      Expect.expect(unicodedString.equals("\u00cc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Cc");
      Expect.expect(unicodedString.equals("\u00cc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("CC");
      Expect.expect(unicodedString.equals("\u00cc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0CC");
      Expect.expect(unicodedString.equals("\u00cc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00CC");
      Expect.expect(unicodedString.equals("\u00cc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Cd");
      Expect.expect(unicodedString.equals("\u00cd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Cd");
      Expect.expect(unicodedString.equals("\u00cd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Cd");
      Expect.expect(unicodedString.equals("\u00cd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("CD");
      Expect.expect(unicodedString.equals("\u00cd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0CD");
      Expect.expect(unicodedString.equals("\u00cd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00CD");
      Expect.expect(unicodedString.equals("\u00cd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Ce");
      Expect.expect(unicodedString.equals("\u00ce"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Ce");
      Expect.expect(unicodedString.equals("\u00ce"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Ce");
      Expect.expect(unicodedString.equals("\u00ce"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("CE");
      Expect.expect(unicodedString.equals("\u00ce"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0CE");
      Expect.expect(unicodedString.equals("\u00ce"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00CE");
      Expect.expect(unicodedString.equals("\u00ce"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Cf");
      Expect.expect(unicodedString.equals("\u00cf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Cf");
      Expect.expect(unicodedString.equals("\u00cf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Cf");
      Expect.expect(unicodedString.equals("\u00cf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("CF");
      Expect.expect(unicodedString.equals("\u00cf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0CF");
      Expect.expect(unicodedString.equals("\u00cf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00CF");
      Expect.expect(unicodedString.equals("\u00cf"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("d0");
      Expect.expect(unicodedString.equals("\u00d0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0d0");
      Expect.expect(unicodedString.equals("\u00d0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00d0");
      Expect.expect(unicodedString.equals("\u00d0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("d1");
      Expect.expect(unicodedString.equals("\u00d1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0d1");
      Expect.expect(unicodedString.equals("\u00d1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00d1");
      Expect.expect(unicodedString.equals("\u00d1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("d2");
      Expect.expect(unicodedString.equals("\u00d2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0d2");
      Expect.expect(unicodedString.equals("\u00d2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00d2");
      Expect.expect(unicodedString.equals("\u00d2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("d3");
      Expect.expect(unicodedString.equals("\u00d3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0d3");
      Expect.expect(unicodedString.equals("\u00d3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00d3");
      Expect.expect(unicodedString.equals("\u00d3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("d4");
      Expect.expect(unicodedString.equals("\u00d4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0d4");
      Expect.expect(unicodedString.equals("\u00d4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00d4");
      Expect.expect(unicodedString.equals("\u00d4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("d5");
      Expect.expect(unicodedString.equals("\u00d5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0d5");
      Expect.expect(unicodedString.equals("\u00d5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00d5");
      Expect.expect(unicodedString.equals("\u00d5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("d6");
      Expect.expect(unicodedString.equals("\u00d6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0d6");
      Expect.expect(unicodedString.equals("\u00d6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00d6");
      Expect.expect(unicodedString.equals("\u00d6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("d7");
      Expect.expect(unicodedString.equals("\u00d7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0d7");
      Expect.expect(unicodedString.equals("\u00d7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00d7");
      Expect.expect(unicodedString.equals("\u00d7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("d8");
      Expect.expect(unicodedString.equals("\u00d8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0d8");
      Expect.expect(unicodedString.equals("\u00d8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00d8");
      Expect.expect(unicodedString.equals("\u00d8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("d9");
      Expect.expect(unicodedString.equals("\u00d9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0d9");
      Expect.expect(unicodedString.equals("\u00d9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00d9");
      Expect.expect(unicodedString.equals("\u00d9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("da");
      Expect.expect(unicodedString.equals("\u00da"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0da");
      Expect.expect(unicodedString.equals("\u00da"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00da");
      Expect.expect(unicodedString.equals("\u00da"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("dA");
      Expect.expect(unicodedString.equals("\u00da"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0dA");
      Expect.expect(unicodedString.equals("\u00da"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00dA");
      Expect.expect(unicodedString.equals("\u00da"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("db");
      Expect.expect(unicodedString.equals("\u00db"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0db");
      Expect.expect(unicodedString.equals("\u00db"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00db");
      Expect.expect(unicodedString.equals("\u00db"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("dB");
      Expect.expect(unicodedString.equals("\u00db"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0dB");
      Expect.expect(unicodedString.equals("\u00db"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00dB");
      Expect.expect(unicodedString.equals("\u00db"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("dc");
      Expect.expect(unicodedString.equals("\u00dc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0dc");
      Expect.expect(unicodedString.equals("\u00dc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00dc");
      Expect.expect(unicodedString.equals("\u00dc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("dC");
      Expect.expect(unicodedString.equals("\u00dc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0dC");
      Expect.expect(unicodedString.equals("\u00dc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00dC");
      Expect.expect(unicodedString.equals("\u00dc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("dd");
      Expect.expect(unicodedString.equals("\u00dd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0dd");
      Expect.expect(unicodedString.equals("\u00dd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00dd");
      Expect.expect(unicodedString.equals("\u00dd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("dD");
      Expect.expect(unicodedString.equals("\u00dd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0dD");
      Expect.expect(unicodedString.equals("\u00dd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00dD");
      Expect.expect(unicodedString.equals("\u00dd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("de");
      Expect.expect(unicodedString.equals("\u00de"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0de");
      Expect.expect(unicodedString.equals("\u00de"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00de");
      Expect.expect(unicodedString.equals("\u00de"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("dE");
      Expect.expect(unicodedString.equals("\u00de"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0dE");
      Expect.expect(unicodedString.equals("\u00de"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00dE");
      Expect.expect(unicodedString.equals("\u00de"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("df");
      Expect.expect(unicodedString.equals("\u00df"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0df");
      Expect.expect(unicodedString.equals("\u00df"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00df");
      Expect.expect(unicodedString.equals("\u00df"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("dF");
      Expect.expect(unicodedString.equals("\u00df"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0dF");
      Expect.expect(unicodedString.equals("\u00df"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00dF");
      Expect.expect(unicodedString.equals("\u00df"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("D0");
      Expect.expect(unicodedString.equals("\u00d0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0D0");
      Expect.expect(unicodedString.equals("\u00d0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00D0");
      Expect.expect(unicodedString.equals("\u00d0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("D1");
      Expect.expect(unicodedString.equals("\u00d1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0D1");
      Expect.expect(unicodedString.equals("\u00d1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00D1");
      Expect.expect(unicodedString.equals("\u00d1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("D2");
      Expect.expect(unicodedString.equals("\u00d2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0D2");
      Expect.expect(unicodedString.equals("\u00d2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00D2");
      Expect.expect(unicodedString.equals("\u00d2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("D3");
      Expect.expect(unicodedString.equals("\u00d3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0D3");
      Expect.expect(unicodedString.equals("\u00d3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00D3");
      Expect.expect(unicodedString.equals("\u00d3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("D4");
      Expect.expect(unicodedString.equals("\u00d4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0D4");
      Expect.expect(unicodedString.equals("\u00d4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00D4");
      Expect.expect(unicodedString.equals("\u00d4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("D5");
      Expect.expect(unicodedString.equals("\u00d5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0D5");
      Expect.expect(unicodedString.equals("\u00d5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00D5");
      Expect.expect(unicodedString.equals("\u00d5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("D6");
      Expect.expect(unicodedString.equals("\u00d6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0D6");
      Expect.expect(unicodedString.equals("\u00d6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00D6");
      Expect.expect(unicodedString.equals("\u00d6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("D7");
      Expect.expect(unicodedString.equals("\u00d7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0D7");
      Expect.expect(unicodedString.equals("\u00d7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00D7");
      Expect.expect(unicodedString.equals("\u00d7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("D8");
      Expect.expect(unicodedString.equals("\u00d8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0D8");
      Expect.expect(unicodedString.equals("\u00d8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00D8");
      Expect.expect(unicodedString.equals("\u00d8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("D9");
      Expect.expect(unicodedString.equals("\u00d9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0D9");
      Expect.expect(unicodedString.equals("\u00d9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00D9");
      Expect.expect(unicodedString.equals("\u00d9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Da");
      Expect.expect(unicodedString.equals("\u00da"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Da");
      Expect.expect(unicodedString.equals("\u00da"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Da");
      Expect.expect(unicodedString.equals("\u00da"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("DA");
      Expect.expect(unicodedString.equals("\u00da"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0DA");
      Expect.expect(unicodedString.equals("\u00da"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00DA");
      Expect.expect(unicodedString.equals("\u00da"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Db");
      Expect.expect(unicodedString.equals("\u00db"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Db");
      Expect.expect(unicodedString.equals("\u00db"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Db");
      Expect.expect(unicodedString.equals("\u00db"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("DB");
      Expect.expect(unicodedString.equals("\u00db"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0DB");
      Expect.expect(unicodedString.equals("\u00db"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00DB");
      Expect.expect(unicodedString.equals("\u00db"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Dc");
      Expect.expect(unicodedString.equals("\u00dc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Dc");
      Expect.expect(unicodedString.equals("\u00dc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Dc");
      Expect.expect(unicodedString.equals("\u00dc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("DC");
      Expect.expect(unicodedString.equals("\u00dc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0DC");
      Expect.expect(unicodedString.equals("\u00dc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00DC");
      Expect.expect(unicodedString.equals("\u00dc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Dd");
      Expect.expect(unicodedString.equals("\u00dd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Dd");
      Expect.expect(unicodedString.equals("\u00dd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Dd");
      Expect.expect(unicodedString.equals("\u00dd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("DD");
      Expect.expect(unicodedString.equals("\u00dd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0DD");
      Expect.expect(unicodedString.equals("\u00dd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00DD");
      Expect.expect(unicodedString.equals("\u00dd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("De");
      Expect.expect(unicodedString.equals("\u00de"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0De");
      Expect.expect(unicodedString.equals("\u00de"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00De");
      Expect.expect(unicodedString.equals("\u00de"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("DE");
      Expect.expect(unicodedString.equals("\u00de"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0DE");
      Expect.expect(unicodedString.equals("\u00de"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00DE");
      Expect.expect(unicodedString.equals("\u00de"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Df");
      Expect.expect(unicodedString.equals("\u00df"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Df");
      Expect.expect(unicodedString.equals("\u00df"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Df");
      Expect.expect(unicodedString.equals("\u00df"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("DF");
      Expect.expect(unicodedString.equals("\u00df"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0DF");
      Expect.expect(unicodedString.equals("\u00df"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00DF");
      Expect.expect(unicodedString.equals("\u00df"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("e0");
      Expect.expect(unicodedString.equals("\u00e0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0e0");
      Expect.expect(unicodedString.equals("\u00e0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00e0");
      Expect.expect(unicodedString.equals("\u00e0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("e1");
      Expect.expect(unicodedString.equals("\u00e1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0e1");
      Expect.expect(unicodedString.equals("\u00e1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00e1");
      Expect.expect(unicodedString.equals("\u00e1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("e2");
      Expect.expect(unicodedString.equals("\u00e2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0e2");
      Expect.expect(unicodedString.equals("\u00e2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00e2");
      Expect.expect(unicodedString.equals("\u00e2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("e3");
      Expect.expect(unicodedString.equals("\u00e3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0e3");
      Expect.expect(unicodedString.equals("\u00e3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00e3");
      Expect.expect(unicodedString.equals("\u00e3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("e4");
      Expect.expect(unicodedString.equals("\u00e4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0e4");
      Expect.expect(unicodedString.equals("\u00e4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00e4");
      Expect.expect(unicodedString.equals("\u00e4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("e5");
      Expect.expect(unicodedString.equals("\u00e5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0e5");
      Expect.expect(unicodedString.equals("\u00e5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00e5");
      Expect.expect(unicodedString.equals("\u00e5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("e6");
      Expect.expect(unicodedString.equals("\u00e6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0e6");
      Expect.expect(unicodedString.equals("\u00e6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00e6");
      Expect.expect(unicodedString.equals("\u00e6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("e7");
      Expect.expect(unicodedString.equals("\u00e7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0e7");
      Expect.expect(unicodedString.equals("\u00e7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00e7");
      Expect.expect(unicodedString.equals("\u00e7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("e8");
      Expect.expect(unicodedString.equals("\u00e8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0e8");
      Expect.expect(unicodedString.equals("\u00e8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00e8");
      Expect.expect(unicodedString.equals("\u00e8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("e9");
      Expect.expect(unicodedString.equals("\u00e9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0e9");
      Expect.expect(unicodedString.equals("\u00e9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00e9");
      Expect.expect(unicodedString.equals("\u00e9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("ea");
      Expect.expect(unicodedString.equals("\u00ea"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0ea");
      Expect.expect(unicodedString.equals("\u00ea"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00ea");
      Expect.expect(unicodedString.equals("\u00ea"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("eA");
      Expect.expect(unicodedString.equals("\u00ea"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0eA");
      Expect.expect(unicodedString.equals("\u00ea"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00eA");
      Expect.expect(unicodedString.equals("\u00ea"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("eb");
      Expect.expect(unicodedString.equals("\u00eb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0eb");
      Expect.expect(unicodedString.equals("\u00eb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00eb");
      Expect.expect(unicodedString.equals("\u00eb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("eB");
      Expect.expect(unicodedString.equals("\u00eb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0eB");
      Expect.expect(unicodedString.equals("\u00eb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00eB");
      Expect.expect(unicodedString.equals("\u00eb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("ec");
      Expect.expect(unicodedString.equals("\u00ec"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0ec");
      Expect.expect(unicodedString.equals("\u00ec"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00ec");
      Expect.expect(unicodedString.equals("\u00ec"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("eC");
      Expect.expect(unicodedString.equals("\u00ec"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0eC");
      Expect.expect(unicodedString.equals("\u00ec"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00eC");
      Expect.expect(unicodedString.equals("\u00ec"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("ed");
      Expect.expect(unicodedString.equals("\u00ed"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0ed");
      Expect.expect(unicodedString.equals("\u00ed"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00ed");
      Expect.expect(unicodedString.equals("\u00ed"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("eD");
      Expect.expect(unicodedString.equals("\u00ed"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0eD");
      Expect.expect(unicodedString.equals("\u00ed"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00eD");
      Expect.expect(unicodedString.equals("\u00ed"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("ee");
      Expect.expect(unicodedString.equals("\u00ee"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0ee");
      Expect.expect(unicodedString.equals("\u00ee"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00ee");
      Expect.expect(unicodedString.equals("\u00ee"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("eE");
      Expect.expect(unicodedString.equals("\u00ee"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0eE");
      Expect.expect(unicodedString.equals("\u00ee"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00eE");
      Expect.expect(unicodedString.equals("\u00ee"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("ef");
      Expect.expect(unicodedString.equals("\u00ef"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0ef");
      Expect.expect(unicodedString.equals("\u00ef"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00ef");
      Expect.expect(unicodedString.equals("\u00ef"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("eF");
      Expect.expect(unicodedString.equals("\u00ef"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0eF");
      Expect.expect(unicodedString.equals("\u00ef"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00eF");
      Expect.expect(unicodedString.equals("\u00ef"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("E0");
      Expect.expect(unicodedString.equals("\u00e0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0E0");
      Expect.expect(unicodedString.equals("\u00e0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00E0");
      Expect.expect(unicodedString.equals("\u00e0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("E1");
      Expect.expect(unicodedString.equals("\u00e1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0E1");
      Expect.expect(unicodedString.equals("\u00e1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00E1");
      Expect.expect(unicodedString.equals("\u00e1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("E2");
      Expect.expect(unicodedString.equals("\u00e2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0E2");
      Expect.expect(unicodedString.equals("\u00e2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00E2");
      Expect.expect(unicodedString.equals("\u00e2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("E3");
      Expect.expect(unicodedString.equals("\u00e3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0E3");
      Expect.expect(unicodedString.equals("\u00e3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00E3");
      Expect.expect(unicodedString.equals("\u00e3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("E4");
      Expect.expect(unicodedString.equals("\u00e4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0E4");
      Expect.expect(unicodedString.equals("\u00e4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00E4");
      Expect.expect(unicodedString.equals("\u00e4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("E5");
      Expect.expect(unicodedString.equals("\u00e5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0E5");
      Expect.expect(unicodedString.equals("\u00e5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00E5");
      Expect.expect(unicodedString.equals("\u00e5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("E6");
      Expect.expect(unicodedString.equals("\u00e6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0E6");
      Expect.expect(unicodedString.equals("\u00e6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00E6");
      Expect.expect(unicodedString.equals("\u00e6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("E7");
      Expect.expect(unicodedString.equals("\u00e7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0E7");
      Expect.expect(unicodedString.equals("\u00e7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00E7");
      Expect.expect(unicodedString.equals("\u00e7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("E8");
      Expect.expect(unicodedString.equals("\u00e8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0E8");
      Expect.expect(unicodedString.equals("\u00e8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00E8");
      Expect.expect(unicodedString.equals("\u00e8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("E9");
      Expect.expect(unicodedString.equals("\u00e9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0E9");
      Expect.expect(unicodedString.equals("\u00e9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00E9");
      Expect.expect(unicodedString.equals("\u00e9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Ea");
      Expect.expect(unicodedString.equals("\u00ea"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Ea");
      Expect.expect(unicodedString.equals("\u00ea"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Ea");
      Expect.expect(unicodedString.equals("\u00ea"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("EA");
      Expect.expect(unicodedString.equals("\u00ea"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0EA");
      Expect.expect(unicodedString.equals("\u00ea"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00EA");
      Expect.expect(unicodedString.equals("\u00ea"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Eb");
      Expect.expect(unicodedString.equals("\u00eb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Eb");
      Expect.expect(unicodedString.equals("\u00eb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Eb");
      Expect.expect(unicodedString.equals("\u00eb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("EB");
      Expect.expect(unicodedString.equals("\u00eb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0EB");
      Expect.expect(unicodedString.equals("\u00eb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00EB");
      Expect.expect(unicodedString.equals("\u00eb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Ec");
      Expect.expect(unicodedString.equals("\u00ec"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Ec");
      Expect.expect(unicodedString.equals("\u00ec"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Ec");
      Expect.expect(unicodedString.equals("\u00ec"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("EC");
      Expect.expect(unicodedString.equals("\u00ec"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0EC");
      Expect.expect(unicodedString.equals("\u00ec"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00EC");
      Expect.expect(unicodedString.equals("\u00ec"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Ed");
      Expect.expect(unicodedString.equals("\u00ed"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Ed");
      Expect.expect(unicodedString.equals("\u00ed"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Ed");
      Expect.expect(unicodedString.equals("\u00ed"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("ED");
      Expect.expect(unicodedString.equals("\u00ed"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0ED");
      Expect.expect(unicodedString.equals("\u00ed"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00ED");
      Expect.expect(unicodedString.equals("\u00ed"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Ee");
      Expect.expect(unicodedString.equals("\u00ee"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Ee");
      Expect.expect(unicodedString.equals("\u00ee"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Ee");
      Expect.expect(unicodedString.equals("\u00ee"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("EE");
      Expect.expect(unicodedString.equals("\u00ee"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0EE");
      Expect.expect(unicodedString.equals("\u00ee"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00EE");
      Expect.expect(unicodedString.equals("\u00ee"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Ef");
      Expect.expect(unicodedString.equals("\u00ef"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Ef");
      Expect.expect(unicodedString.equals("\u00ef"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Ef");
      Expect.expect(unicodedString.equals("\u00ef"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("EF");
      Expect.expect(unicodedString.equals("\u00ef"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0EF");
      Expect.expect(unicodedString.equals("\u00ef"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00EF");
      Expect.expect(unicodedString.equals("\u00ef"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("f0");
      Expect.expect(unicodedString.equals("\u00f0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0f0");
      Expect.expect(unicodedString.equals("\u00f0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00f0");
      Expect.expect(unicodedString.equals("\u00f0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("f1");
      Expect.expect(unicodedString.equals("\u00f1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0f1");
      Expect.expect(unicodedString.equals("\u00f1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00f1");
      Expect.expect(unicodedString.equals("\u00f1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("f2");
      Expect.expect(unicodedString.equals("\u00f2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0f2");
      Expect.expect(unicodedString.equals("\u00f2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00f2");
      Expect.expect(unicodedString.equals("\u00f2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("f3");
      Expect.expect(unicodedString.equals("\u00f3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0f3");
      Expect.expect(unicodedString.equals("\u00f3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00f3");
      Expect.expect(unicodedString.equals("\u00f3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("f4");
      Expect.expect(unicodedString.equals("\u00f4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0f4");
      Expect.expect(unicodedString.equals("\u00f4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00f4");
      Expect.expect(unicodedString.equals("\u00f4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("f5");
      Expect.expect(unicodedString.equals("\u00f5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0f5");
      Expect.expect(unicodedString.equals("\u00f5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00f5");
      Expect.expect(unicodedString.equals("\u00f5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("f6");
      Expect.expect(unicodedString.equals("\u00f6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0f6");
      Expect.expect(unicodedString.equals("\u00f6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00f6");
      Expect.expect(unicodedString.equals("\u00f6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("f7");
      Expect.expect(unicodedString.equals("\u00f7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0f7");
      Expect.expect(unicodedString.equals("\u00f7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00f7");
      Expect.expect(unicodedString.equals("\u00f7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("f8");
      Expect.expect(unicodedString.equals("\u00f8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0f8");
      Expect.expect(unicodedString.equals("\u00f8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00f8");
      Expect.expect(unicodedString.equals("\u00f8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("f9");
      Expect.expect(unicodedString.equals("\u00f9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0f9");
      Expect.expect(unicodedString.equals("\u00f9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00f9");
      Expect.expect(unicodedString.equals("\u00f9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("fa");
      Expect.expect(unicodedString.equals("\u00fa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0fa");
      Expect.expect(unicodedString.equals("\u00fa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00fa");
      Expect.expect(unicodedString.equals("\u00fa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("fA");
      Expect.expect(unicodedString.equals("\u00fa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0fA");
      Expect.expect(unicodedString.equals("\u00fa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00fA");
      Expect.expect(unicodedString.equals("\u00fa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("fb");
      Expect.expect(unicodedString.equals("\u00fb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0fb");
      Expect.expect(unicodedString.equals("\u00fb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00fb");
      Expect.expect(unicodedString.equals("\u00fb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("fB");
      Expect.expect(unicodedString.equals("\u00fb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0fB");
      Expect.expect(unicodedString.equals("\u00fb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00fB");
      Expect.expect(unicodedString.equals("\u00fb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("fc");
      Expect.expect(unicodedString.equals("\u00fc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0fc");
      Expect.expect(unicodedString.equals("\u00fc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00fc");
      Expect.expect(unicodedString.equals("\u00fc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("fC");
      Expect.expect(unicodedString.equals("\u00fc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0fC");
      Expect.expect(unicodedString.equals("\u00fc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00fC");
      Expect.expect(unicodedString.equals("\u00fc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("fd");
      Expect.expect(unicodedString.equals("\u00fd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0fd");
      Expect.expect(unicodedString.equals("\u00fd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00fd");
      Expect.expect(unicodedString.equals("\u00fd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("fD");
      Expect.expect(unicodedString.equals("\u00fd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0fD");
      Expect.expect(unicodedString.equals("\u00fd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00fD");
      Expect.expect(unicodedString.equals("\u00fd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("fe");
      Expect.expect(unicodedString.equals("\u00fe"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0fe");
      Expect.expect(unicodedString.equals("\u00fe"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00fe");
      Expect.expect(unicodedString.equals("\u00fe"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("fE");
      Expect.expect(unicodedString.equals("\u00fe"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0fE");
      Expect.expect(unicodedString.equals("\u00fe"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00fE");
      Expect.expect(unicodedString.equals("\u00fe"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("ff");
      Expect.expect(unicodedString.equals("\u00ff"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0ff");
      Expect.expect(unicodedString.equals("\u00ff"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00ff");
      Expect.expect(unicodedString.equals("\u00ff"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("fF");
      Expect.expect(unicodedString.equals("\u00ff"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0fF");
      Expect.expect(unicodedString.equals("\u00ff"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00fF");
      Expect.expect(unicodedString.equals("\u00ff"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("F0");
      Expect.expect(unicodedString.equals("\u00f0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0F0");
      Expect.expect(unicodedString.equals("\u00f0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00F0");
      Expect.expect(unicodedString.equals("\u00f0"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("F1");
      Expect.expect(unicodedString.equals("\u00f1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0F1");
      Expect.expect(unicodedString.equals("\u00f1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00F1");
      Expect.expect(unicodedString.equals("\u00f1"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("F2");
      Expect.expect(unicodedString.equals("\u00f2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0F2");
      Expect.expect(unicodedString.equals("\u00f2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00F2");
      Expect.expect(unicodedString.equals("\u00f2"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("F3");
      Expect.expect(unicodedString.equals("\u00f3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0F3");
      Expect.expect(unicodedString.equals("\u00f3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00F3");
      Expect.expect(unicodedString.equals("\u00f3"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("F4");
      Expect.expect(unicodedString.equals("\u00f4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0F4");
      Expect.expect(unicodedString.equals("\u00f4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00F4");
      Expect.expect(unicodedString.equals("\u00f4"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("F5");
      Expect.expect(unicodedString.equals("\u00f5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0F5");
      Expect.expect(unicodedString.equals("\u00f5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00F5");
      Expect.expect(unicodedString.equals("\u00f5"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("F6");
      Expect.expect(unicodedString.equals("\u00f6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0F6");
      Expect.expect(unicodedString.equals("\u00f6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00F6");
      Expect.expect(unicodedString.equals("\u00f6"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("F7");
      Expect.expect(unicodedString.equals("\u00f7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0F7");
      Expect.expect(unicodedString.equals("\u00f7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00F7");
      Expect.expect(unicodedString.equals("\u00f7"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("F8");
      Expect.expect(unicodedString.equals("\u00f8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0F8");
      Expect.expect(unicodedString.equals("\u00f8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00F8");
      Expect.expect(unicodedString.equals("\u00f8"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("F9");
      Expect.expect(unicodedString.equals("\u00f9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0F9");
      Expect.expect(unicodedString.equals("\u00f9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00F9");
      Expect.expect(unicodedString.equals("\u00f9"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Fa");
      Expect.expect(unicodedString.equals("\u00fa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Fa");
      Expect.expect(unicodedString.equals("\u00fa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Fa");
      Expect.expect(unicodedString.equals("\u00fa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("FA");
      Expect.expect(unicodedString.equals("\u00fa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0FA");
      Expect.expect(unicodedString.equals("\u00fa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00FA");
      Expect.expect(unicodedString.equals("\u00fa"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Fb");
      Expect.expect(unicodedString.equals("\u00fb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Fb");
      Expect.expect(unicodedString.equals("\u00fb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Fb");
      Expect.expect(unicodedString.equals("\u00fb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("FB");
      Expect.expect(unicodedString.equals("\u00fb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0FB");
      Expect.expect(unicodedString.equals("\u00fb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00FB");
      Expect.expect(unicodedString.equals("\u00fb"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Fc");
      Expect.expect(unicodedString.equals("\u00fc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Fc");
      Expect.expect(unicodedString.equals("\u00fc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Fc");
      Expect.expect(unicodedString.equals("\u00fc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("FC");
      Expect.expect(unicodedString.equals("\u00fc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0FC");
      Expect.expect(unicodedString.equals("\u00fc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00FC");
      Expect.expect(unicodedString.equals("\u00fc"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Fd");
      Expect.expect(unicodedString.equals("\u00fd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Fd");
      Expect.expect(unicodedString.equals("\u00fd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Fd");
      Expect.expect(unicodedString.equals("\u00fd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("FD");
      Expect.expect(unicodedString.equals("\u00fd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0FD");
      Expect.expect(unicodedString.equals("\u00fd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00FD");
      Expect.expect(unicodedString.equals("\u00fd"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Fe");
      Expect.expect(unicodedString.equals("\u00fe"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Fe");
      Expect.expect(unicodedString.equals("\u00fe"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Fe");
      Expect.expect(unicodedString.equals("\u00fe"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("FE");
      Expect.expect(unicodedString.equals("\u00fe"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0FE");
      Expect.expect(unicodedString.equals("\u00fe"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00FE");
      Expect.expect(unicodedString.equals("\u00fe"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("Ff");
      Expect.expect(unicodedString.equals("\u00ff"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0Ff");
      Expect.expect(unicodedString.equals("\u00ff"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00Ff");
      Expect.expect(unicodedString.equals("\u00ff"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("FF");
      Expect.expect(unicodedString.equals("\u00ff"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("0FF");
      Expect.expect(unicodedString.equals("\u00ff"));
      unicodedString = StringUtil.convertHexCodedStringIntoUnicodeString("00FF");
      Expect.expect(unicodedString.equals("\u00ff"));
    }
    catch (NumberFormatException nfe)
    {
      os.println("Unexpected NumberFormatException thrown.");
      os.println("NumberFormatException.getMessage(): " + nfe.getMessage());
      Expect.expect(false);
    }
    catch (CharConversionException cce)
    {
      os.println("Unexpected CharConversionException thrown.");
      os.println("CharConversionException.getMessage(): " + cce.getMessage());
      Expect.expect(false);
    }

    // Verify that all illegal characters throw an exception.
    // Verify that all legal characters do not throw an exception.
    final String ASCII_COMMA = ",";
    final String ASCII_DIGITS = "0123456789";
    final String ASCII_HEX_LETTERS = "abcdefABCDEF";
    final String LATIN_1_GEORGIAN_COMMA = "\u00b7";  // MIDDLE DOT
    final String ARMENIAN_COMMA = "\u055d";
    final String ARABIC_COMMA = "\u060c";
    final String ARABIC_INDIC_DIGITS = "\u0660\u0661\u0662\u0663\u0664\u0665\u0666\u0667\u0668\u0669";
    final String EXTENED_ARABIC_INDIC_DIGITS = "\u06f0\u06f1\u06f2\u06f3\u06f4\u06f5\u06f6\u06f7\u06f8\u06f9";
    final String DEVANAGARI_DIGITS = "\u0966\u0967\u0968\u0969\u096a\u096b\u096c\u096d\u096e\u096f";
    final String BENGALI_DIGITS = "\u09e6\u09e7\u09e8\u09e9\u09ea\u09eb\u09ec\u09ed\u09ee\u09ef";
    final String GURMUKHI_DIGITS = "\u0a66\u0a67\u0a68\u0a69\u0a6a\u0a6b\u0a6c\u0a6d\u0a6e\u0a6f";
    final String GUJARATI_DIGITS = "\u0ae6\u0ae7\u0ae8\u0ae9\u0aea\u0aeb\u0aec\u0aed\u0aee\u0aef";
    final String ORIYA_DIGITS = "\u0b66\u0b67\u0b68\u0b69\u0b6a\u0b6b\u0b6c\u0b6d\u0b6e\u0b6f";
    /*
     * The Integer.parseInt() routine currently does not recognize TAMIC DIGIT 0 '\u0be6'.
     * A future move to a newer version of the Integer.parseInt() routine may required including
     * this code point.  Please note that a new version of Integer.parseInt() may add aupport for
     * digits from other languages (or make changes in its existing support).  This will require
     * modifications be made to the contents of 'LEGAL_CHARACTERS'.
     * final String TAMIC_DIGITS = "\u0be6\u0be7\u0be8\u0be9\u0bea\u0beb\u0bec\u0bed\u0bee\u0bef";
     */
    final String TAMIC_DIGITS = /*\u0be6*/"\u0be7\u0be8\u0be9\u0bea\u0beb\u0bec\u0bed\u0bee\u0bef";
    final String TELUGU_DIGITS = "\u0c66\u0c67\u0c68\u0c69\u0c6a\u0c6b\u0c6c\u0c6d\u0c6e\u0c6f";
    final String KANNADA_DIGITS = "\u0ce6\u0ce7\u0ce8\u0ce9\u0cea\u0ceb\u0cec\u0ced\u0cee\u0cef";
    final String MALAYALAM_DIGITS = "\u0d66\u0d67\u0d68\u0d69\u0d6a\u0d6b\u0d6c\u0d6d\u0d6e\u0d6f";
    final String THAI_DIGITS = "\u0e50\u0e51\u0e52\u0e53\u0e54\u0e55\u0e56\u0e57\u0e58\u0e59";
    final String LAO_DIGITS = "\u0ed0\u0ed1\u0ed2\u0ed3\u0ed4\u0ed5\u0ed6\u0ed7\u0ed8\u0ed9";
    final String TIBETAN_DIGITS = "\u0f20\u0f21\u0f22\u0f23\u0f24\u0f25\u0f26\u0f27\u0f28\u0f29";
    final String MYANMAR_DIGITS = "\u1040\u1041\u1042\u1043\u1044\u1045\u1046\u1047\u1048\u1049";
    final String ETHIOPIC_COMMA = "\u1363";
    // There is no 'ETHIOPIC DIGIT ZERO'.
    final String ETHIOPIC_DIGITS = "\u1369\u136a\u136b\u136c\u136d\u136e\u136f\u1370\u1371";
    final String KHMER_DIGITS = "\u17e0\u17e1\u17e2\u17e3\u17e4\u17e5\u17e6\u17e7\u17e8\u17e9";
    final String MONGOLIAN_DIGITS = "\u1810\u1811\u1812\u1813\u1814\u1815\u1816\u1817\u1818\u1819";
    final String LIMBU_DIGITS = "\u1946\u1947\u1948\u1949\u194a\u194b\u194c\u194d\u194e\u194f";
    final String CJK_IDEOGRAPHIC_COMMA = "\u3001";
    final String FULLWIDTH_ASCII_DIGITS = "\uff10\uff11\uff12\uff13\uff14\uff15\uff16\uff17\uff18\uff19";
    final String FULLWIDTH_ASCII_HEX_LETTERS = "\uff21\uff22\uff23\uff24\uff25\uff26\uff41\uff42\uff43\uff44\uff45\uff46";
    final String LEGAL_CHARACTERS = ASCII_DIGITS +
                                    ASCII_HEX_LETTERS +
                                    ARABIC_INDIC_DIGITS +
                                    EXTENED_ARABIC_INDIC_DIGITS +
                                    DEVANAGARI_DIGITS +
                                    BENGALI_DIGITS +
                                    GURMUKHI_DIGITS +
                                    GUJARATI_DIGITS +
                                    ORIYA_DIGITS +
                                    TAMIC_DIGITS +
                                    TELUGU_DIGITS +
                                    KANNADA_DIGITS +
                                    MALAYALAM_DIGITS +
                                    THAI_DIGITS +
                                    LAO_DIGITS +
                                    TIBETAN_DIGITS +
                                    MYANMAR_DIGITS +
                                    ETHIOPIC_DIGITS +
                                    KHMER_DIGITS +
                                    MONGOLIAN_DIGITS +
                                    LIMBU_DIGITS +
                                    FULLWIDTH_ASCII_DIGITS +
                                    FULLWIDTH_ASCII_HEX_LETTERS +
                                    ASCII_COMMA +
                                    LATIN_1_GEORGIAN_COMMA +
                                    ARMENIAN_COMMA +
                                    ARABIC_COMMA +
                                    ETHIOPIC_COMMA +
                                    CJK_IDEOGRAPHIC_COMMA;
    final int MAX_CODE_POINT = 65536;

    for (int codePoint = 0; codePoint < MAX_CODE_POINT; ++codePoint)
    {
      char[] c = { (char)codePoint };
      String s = new String(c);

      if (LEGAL_CHARACTERS.indexOf(c[0]) == -1)
      {
        try
        {
          StringUtil.convertHexCodedStringIntoUnicodeString(s);
          os.println("No (expected) exception thrown for codePoint: " + Integer.toHexString(codePoint) + " (hex).");
          Expect.expect(false);
        }
        catch (NumberFormatException nfe)
        {
          Expect.expect(nfe.getMessage().equals(s.trim()));
        }
        catch (CharConversionException cce)
        {
          os.println("Unexpected 'CharConversionException' thrown for codePoint: " + Integer.toHexString(codePoint) + " (hex).");
          os.println("CharConversionException.getMessage(): " + cce.getMessage());
          Expect.expect(false);
        }
      }
      else
      {
        try
        {
          StringUtil.convertHexCodedStringIntoUnicodeString(s);
        }
        catch (NumberFormatException nfe)
        {
          os.println("Unexpected NumberFormatException thrown for codePoint: " + Integer.toHexString(codePoint) + " (hex).");
          os.println("NumberFormatException.getMessage(): " + nfe.getMessage());
          Expect.expect(false);
        }
        catch (CharConversionException cce)
        {
          os.println("Unexpected CharConversionException thrown for codePoint: " + Integer.toHexString(codePoint) + " (hex).");
          os.println("CharConversionException.getMessage(): " + cce.getMessage());
          Expect.expect(false);
        }
      }
    }

    // Test for null input.
    try
    {
      StringUtil.convertHexCodedStringIntoUnicodeString(null);
      Expect.expect(false);
    }
    catch (NumberFormatException nfe)
    {
      os.println("Unexpected NumberFormatException thrown for 'null' input");
      os.println("NumberFormatException.getMessage(): " + nfe.getMessage());
      Expect.expect(false);
    }
    catch (CharConversionException cce)
    {
      os.println("Unexpected CharConversionException thrown for 'null' input");
      os.println("CharConversionException.getMessage(): " + cce.getMessage());
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // Do nothing.
    }

    // Test for out of Unicode character range.
    try
    {
      StringUtil.convertHexCodedStringIntoUnicodeString("10000");
      Expect.expect(false);
    }
    catch (NumberFormatException nfe)
    {
      os.println("Unexpected NumberFormatException thrown for 'null' input");
      os.println("NumberFormatException.getMessage(): " + nfe.getMessage());
      Expect.expect(false);
    }
    catch (CharConversionException cce)
    {
      Expect.expect(cce.getMessage().equals("10000"));
    }

    // Test for embedded space.
    try
    {
      StringUtil.convertHexCodedStringIntoUnicodeString("44 45");
      Expect.expect(false);
    }
    catch (NumberFormatException nfe)
    {
      Expect.expect(nfe.getMessage().equals("44 45"));
    }
    catch (CharConversionException cce)
    {
      os.println("Unexpected CharConversionException thrown for 'null' input");
      os.println("CharConversionException.getMessage(): " + cce.getMessage());
      Expect.expect(false);
    }

    // Test for embedded tab.
    try
    {
      StringUtil.convertHexCodedStringIntoUnicodeString("44\t45");
      Expect.expect(false);
    }
    catch (NumberFormatException nfe)
    {
      Expect.expect(nfe.getMessage().equals("44\t45"));
    }
    catch (CharConversionException cce)
    {
      os.println("Unexpected CharConversionException thrown for 'null' input");
      os.println("CharConversionException.getMessage(): " + cce.getMessage());
      Expect.expect(false);
    }

    // Test for embedded newline.
    try
    {
      StringUtil.convertHexCodedStringIntoUnicodeString("44\n45");
      Expect.expect(false);
    }
    catch (NumberFormatException nfe)
    {
      Expect.expect(nfe.getMessage().equals("44\n45"));
    }
    catch (CharConversionException cce)
    {
      os.println("Unexpected CharConversionException thrown for 'null' input");
      os.println("CharConversionException.getMessage(): " + cce.getMessage());
      Expect.expect(false);
    }

    // Test for embedded carriage return.
    try
    {
      StringUtil.convertHexCodedStringIntoUnicodeString("44\r45");
      Expect.expect(false);
    }
    catch (NumberFormatException nfe)
    {
      Expect.expect(nfe.getMessage().equals("44\r45"));
    }
    catch (CharConversionException cce)
    {
      os.println("Unexpected CharConversionException thrown for 'null' input");
      os.println("CharConversionException.getMessage(): " + cce.getMessage());
      Expect.expect(false);
    }

    // Test for embedded form feed.
    try
    {
      StringUtil.convertHexCodedStringIntoUnicodeString("44\f45");
      Expect.expect(false);
    }
    catch (NumberFormatException nfe)
    {
      Expect.expect(nfe.getMessage().equals("44\f45"));
    }
    catch (CharConversionException cce)
    {
      os.println("Unexpected CharConversionException thrown for 'null' input");
      os.println("CharConversionException.getMessage(): " + cce.getMessage());
      Expect.expect(false);
    }

    // Test for embedded backspace.
    try
    {
      StringUtil.convertHexCodedStringIntoUnicodeString("44\b45");
      Expect.expect(false);
    }
    catch (NumberFormatException nfe)
    {
      Expect.expect(nfe.getMessage().equals("44\b45"));
    }
    catch (CharConversionException cce)
    {
      os.println("Unexpected CharConversionException thrown for 'null' input");
      os.println("CharConversionException.getMessage(): " + cce.getMessage());
      Expect.expect(false);
    }

    try
    {
      String encoded;

      // Test for minimimum and maximum Unicode characters.
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("0000");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0000');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("ffff");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\uffff');

      // Test for tolerance for extra leading zeros.
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("0000000000000000001");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0001');

      // Test for tolerance for extra delimiters.
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("2,");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0002');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString(",3");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0003');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString(",4,");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0004');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("5,,");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0005');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString(",,6");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0006');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString(",7,,");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0007');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString(",,8,");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0008');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString(",,9,,");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0009');

      // Test for tolerance for white space.
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("a ");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\n');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString(" b");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u000b');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString(" c ");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u000c');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("d  ");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\r');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("  e");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u000e');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString(" f  ");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u000f');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("  10 ");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0010');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("  11  ");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0011');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("12\t");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0012');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("\t13");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0013');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("\t14\t");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0014');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("15\t\t");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0015');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("\t\t16");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0016');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("\t17\t\t");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0017');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("\t\t18\t");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0018');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("\t\t19\t\t");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u0019');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString(" 1a\t");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u001a');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("\t1b ");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u001b');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString(" \t1c");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u001c');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("\t 1d");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u001d');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("1e \t");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u001e');
      encoded = StringUtil.convertHexCodedStringIntoUnicodeString("1f\t ");
      Expect.expect(encoded.length() == 1);
      Expect.expect(encoded.charAt(0) == '\u001f');
    }
    catch (NumberFormatException nfe)
    {
      os.println("Unexpected NumberFormatException thrown.");
      os.println("NumberFormatException.getMessage(): " + nfe.getMessage());
      Expect.expect(false);
    }
    catch (CharConversionException cce)
    {
      os.println("Unexpected CharConversionException thrown.");
      os.println("CharConversionException.getMessage(): " + cce.getMessage());
      Expect.expect(false);
    }
  }

  /**
   * @author Bob Balliew
   */
  private void testConvertUnicodeStringIntoHexCodedString(PrintWriter os)
  {
    String hexString;
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("foo");
    Expect.expect(hexString.equalsIgnoreCase("66,6f,6f"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("66,6f,6f");
    Expect.expect(hexString.equalsIgnoreCase("36,36,2c,36,66,2c,36,66"));

    // The following tests the correct decoding of all possible 8 bit patterns.
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0000");
    Expect.expect(hexString.equalsIgnoreCase("0"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0001");
    Expect.expect(hexString.equalsIgnoreCase("1"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0002");
    Expect.expect(hexString.equalsIgnoreCase("2"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0003");
    Expect.expect(hexString.equalsIgnoreCase("3"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0004");
    Expect.expect(hexString.equalsIgnoreCase("4"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0005");
    Expect.expect(hexString.equalsIgnoreCase("5"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0006");
    Expect.expect(hexString.equalsIgnoreCase("6"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0007");
    Expect.expect(hexString.equalsIgnoreCase("7"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0008");
    Expect.expect(hexString.equalsIgnoreCase("8"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0009");
    Expect.expect(hexString.equalsIgnoreCase("9"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\n");
    Expect.expect(hexString.equalsIgnoreCase("a"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u000b");
    Expect.expect(hexString.equalsIgnoreCase("b"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u000c");
    Expect.expect(hexString.equalsIgnoreCase("c"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\r");
    Expect.expect(hexString.equalsIgnoreCase("d"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u000e");
    Expect.expect(hexString.equalsIgnoreCase("e"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u000f");
    Expect.expect(hexString.equalsIgnoreCase("f"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0010");
    Expect.expect(hexString.equalsIgnoreCase("10"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0011");
    Expect.expect(hexString.equalsIgnoreCase("11"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0012");
    Expect.expect(hexString.equalsIgnoreCase("12"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0013");
    Expect.expect(hexString.equalsIgnoreCase("13"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0014");
    Expect.expect(hexString.equalsIgnoreCase("14"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0015");
    Expect.expect(hexString.equalsIgnoreCase("15"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0016");
    Expect.expect(hexString.equalsIgnoreCase("16"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0017");
    Expect.expect(hexString.equalsIgnoreCase("17"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0018");
    Expect.expect(hexString.equalsIgnoreCase("18"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0019");
    Expect.expect(hexString.equalsIgnoreCase("19"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u001a");
    Expect.expect(hexString.equalsIgnoreCase("1a"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u001b");
    Expect.expect(hexString.equalsIgnoreCase("1b"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u001c");
    Expect.expect(hexString.equalsIgnoreCase("1c"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u001d");
    Expect.expect(hexString.equalsIgnoreCase("1d"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u001e");
    Expect.expect(hexString.equalsIgnoreCase("1e"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u001f");
    Expect.expect(hexString.equalsIgnoreCase("1f"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0020");
    Expect.expect(hexString.equalsIgnoreCase("20"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0021");
    Expect.expect(hexString.equalsIgnoreCase("21"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\"");
    Expect.expect(hexString.equalsIgnoreCase("22"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0023");
    Expect.expect(hexString.equalsIgnoreCase("23"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0024");
    Expect.expect(hexString.equalsIgnoreCase("24"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0025");
    Expect.expect(hexString.equalsIgnoreCase("25"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0026");
    Expect.expect(hexString.equalsIgnoreCase("26"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0027");
    Expect.expect(hexString.equalsIgnoreCase("27"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0028");
    Expect.expect(hexString.equalsIgnoreCase("28"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0029");
    Expect.expect(hexString.equalsIgnoreCase("29"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u002a");
    Expect.expect(hexString.equalsIgnoreCase("2a"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u002b");
    Expect.expect(hexString.equalsIgnoreCase("2b"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u002c");
    Expect.expect(hexString.equalsIgnoreCase("2c"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u002d");
    Expect.expect(hexString.equalsIgnoreCase("2d"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u002e");
    Expect.expect(hexString.equalsIgnoreCase("2e"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u002f");
    Expect.expect(hexString.equalsIgnoreCase("2f"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0030");
    Expect.expect(hexString.equalsIgnoreCase("30"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0031");
    Expect.expect(hexString.equalsIgnoreCase("31"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0032");
    Expect.expect(hexString.equalsIgnoreCase("32"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0033");
    Expect.expect(hexString.equalsIgnoreCase("33"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0034");
    Expect.expect(hexString.equalsIgnoreCase("34"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0035");
    Expect.expect(hexString.equalsIgnoreCase("35"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0036");
    Expect.expect(hexString.equalsIgnoreCase("36"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0037");
    Expect.expect(hexString.equalsIgnoreCase("37"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0038");
    Expect.expect(hexString.equalsIgnoreCase("38"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0039");
    Expect.expect(hexString.equalsIgnoreCase("39"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u003a");
    Expect.expect(hexString.equalsIgnoreCase("3a"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u003b");
    Expect.expect(hexString.equalsIgnoreCase("3b"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u003c");
    Expect.expect(hexString.equalsIgnoreCase("3c"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u003d");
    Expect.expect(hexString.equalsIgnoreCase("3d"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u003e");
    Expect.expect(hexString.equalsIgnoreCase("3e"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u003f");
    Expect.expect(hexString.equalsIgnoreCase("3f"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0040");
    Expect.expect(hexString.equalsIgnoreCase("40"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0041");
    Expect.expect(hexString.equalsIgnoreCase("41"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0042");
    Expect.expect(hexString.equalsIgnoreCase("42"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0043");
    Expect.expect(hexString.equalsIgnoreCase("43"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0044");
    Expect.expect(hexString.equalsIgnoreCase("44"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0045");
    Expect.expect(hexString.equalsIgnoreCase("45"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0046");
    Expect.expect(hexString.equalsIgnoreCase("46"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0047");
    Expect.expect(hexString.equalsIgnoreCase("47"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0048");
    Expect.expect(hexString.equalsIgnoreCase("48"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0049");
    Expect.expect(hexString.equalsIgnoreCase("49"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u004a");
    Expect.expect(hexString.equalsIgnoreCase("4a"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u004b");
    Expect.expect(hexString.equalsIgnoreCase("4b"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u004c");
    Expect.expect(hexString.equalsIgnoreCase("4c"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u004d");
    Expect.expect(hexString.equalsIgnoreCase("4d"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u004e");
    Expect.expect(hexString.equalsIgnoreCase("4e"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u004f");
    Expect.expect(hexString.equalsIgnoreCase("4f"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0050");
    Expect.expect(hexString.equalsIgnoreCase("50"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0051");
    Expect.expect(hexString.equalsIgnoreCase("51"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0052");
    Expect.expect(hexString.equalsIgnoreCase("52"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0053");
    Expect.expect(hexString.equalsIgnoreCase("53"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0054");
    Expect.expect(hexString.equalsIgnoreCase("54"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0055");
    Expect.expect(hexString.equalsIgnoreCase("55"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0056");
    Expect.expect(hexString.equalsIgnoreCase("56"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0057");
    Expect.expect(hexString.equalsIgnoreCase("57"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0058");
    Expect.expect(hexString.equalsIgnoreCase("58"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0059");
    Expect.expect(hexString.equalsIgnoreCase("59"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u005a");
    Expect.expect(hexString.equalsIgnoreCase("5a"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u005b");
    Expect.expect(hexString.equalsIgnoreCase("5b"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\\");
    Expect.expect(hexString.equalsIgnoreCase("5c"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u005d");
    Expect.expect(hexString.equalsIgnoreCase("5d"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u005e");
    Expect.expect(hexString.equalsIgnoreCase("5e"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u005f");
    Expect.expect(hexString.equalsIgnoreCase("5f"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0060");
    Expect.expect(hexString.equalsIgnoreCase("60"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0061");
    Expect.expect(hexString.equalsIgnoreCase("61"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0062");
    Expect.expect(hexString.equalsIgnoreCase("62"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0063");
    Expect.expect(hexString.equalsIgnoreCase("63"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0064");
    Expect.expect(hexString.equalsIgnoreCase("64"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0065");
    Expect.expect(hexString.equalsIgnoreCase("65"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0066");
    Expect.expect(hexString.equalsIgnoreCase("66"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0067");
    Expect.expect(hexString.equalsIgnoreCase("67"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0068");
    Expect.expect(hexString.equalsIgnoreCase("68"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0069");
    Expect.expect(hexString.equalsIgnoreCase("69"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u006a");
    Expect.expect(hexString.equalsIgnoreCase("6a"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u006b");
    Expect.expect(hexString.equalsIgnoreCase("6b"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u006c");
    Expect.expect(hexString.equalsIgnoreCase("6c"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u006d");
    Expect.expect(hexString.equalsIgnoreCase("6d"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u006e");
    Expect.expect(hexString.equalsIgnoreCase("6e"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u006f");
    Expect.expect(hexString.equalsIgnoreCase("6f"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0070");
    Expect.expect(hexString.equalsIgnoreCase("70"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0071");
    Expect.expect(hexString.equalsIgnoreCase("71"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0072");
    Expect.expect(hexString.equalsIgnoreCase("72"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0073");
    Expect.expect(hexString.equalsIgnoreCase("73"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0074");
    Expect.expect(hexString.equalsIgnoreCase("74"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0075");
    Expect.expect(hexString.equalsIgnoreCase("75"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0076");
    Expect.expect(hexString.equalsIgnoreCase("76"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0077");
    Expect.expect(hexString.equalsIgnoreCase("77"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0078");
    Expect.expect(hexString.equalsIgnoreCase("78"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0079");
    Expect.expect(hexString.equalsIgnoreCase("79"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u007a");
    Expect.expect(hexString.equalsIgnoreCase("7a"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u007b");
    Expect.expect(hexString.equalsIgnoreCase("7b"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u007c");
    Expect.expect(hexString.equalsIgnoreCase("7c"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u007d");
    Expect.expect(hexString.equalsIgnoreCase("7d"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u007e");
    Expect.expect(hexString.equalsIgnoreCase("7e"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u007f");
    Expect.expect(hexString.equalsIgnoreCase("7f"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0080");
    Expect.expect(hexString.equalsIgnoreCase("80"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0081");
    Expect.expect(hexString.equalsIgnoreCase("81"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0082");
    Expect.expect(hexString.equalsIgnoreCase("82"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0083");
    Expect.expect(hexString.equalsIgnoreCase("83"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0084");
    Expect.expect(hexString.equalsIgnoreCase("84"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0085");
    Expect.expect(hexString.equalsIgnoreCase("85"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0086");
    Expect.expect(hexString.equalsIgnoreCase("86"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0087");
    Expect.expect(hexString.equalsIgnoreCase("87"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0088");
    Expect.expect(hexString.equalsIgnoreCase("88"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0089");
    Expect.expect(hexString.equalsIgnoreCase("89"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u008a");
    Expect.expect(hexString.equalsIgnoreCase("8a"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u008b");
    Expect.expect(hexString.equalsIgnoreCase("8b"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u008c");
    Expect.expect(hexString.equalsIgnoreCase("8c"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u008d");
    Expect.expect(hexString.equalsIgnoreCase("8d"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u008e");
    Expect.expect(hexString.equalsIgnoreCase("8e"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u008f");
    Expect.expect(hexString.equalsIgnoreCase("8f"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0090");
    Expect.expect(hexString.equalsIgnoreCase("90"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0091");
    Expect.expect(hexString.equalsIgnoreCase("91"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0092");
    Expect.expect(hexString.equalsIgnoreCase("92"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0093");
    Expect.expect(hexString.equalsIgnoreCase("93"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0094");
    Expect.expect(hexString.equalsIgnoreCase("94"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0095");
    Expect.expect(hexString.equalsIgnoreCase("95"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0096");
    Expect.expect(hexString.equalsIgnoreCase("96"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0097");
    Expect.expect(hexString.equalsIgnoreCase("97"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0098");
    Expect.expect(hexString.equalsIgnoreCase("98"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u0099");
    Expect.expect(hexString.equalsIgnoreCase("99"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u009a");
    Expect.expect(hexString.equalsIgnoreCase("9a"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u009b");
    Expect.expect(hexString.equalsIgnoreCase("9b"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u009c");
    Expect.expect(hexString.equalsIgnoreCase("9c"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u009d");
    Expect.expect(hexString.equalsIgnoreCase("9d"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u009e");
    Expect.expect(hexString.equalsIgnoreCase("9e"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u009f");
    Expect.expect(hexString.equalsIgnoreCase("9f"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00a0");
    Expect.expect(hexString.equalsIgnoreCase("a0"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00a1");
    Expect.expect(hexString.equalsIgnoreCase("a1"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00a2");
    Expect.expect(hexString.equalsIgnoreCase("a2"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00a3");
    Expect.expect(hexString.equalsIgnoreCase("a3"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00a4");
    Expect.expect(hexString.equalsIgnoreCase("a4"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00a5");
    Expect.expect(hexString.equalsIgnoreCase("a5"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00a6");
    Expect.expect(hexString.equalsIgnoreCase("a6"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00a7");
    Expect.expect(hexString.equalsIgnoreCase("a7"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00a8");
    Expect.expect(hexString.equalsIgnoreCase("a8"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00a9");
    Expect.expect(hexString.equalsIgnoreCase("a9"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00aa");
    Expect.expect(hexString.equalsIgnoreCase("aa"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00ab");
    Expect.expect(hexString.equalsIgnoreCase("ab"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00ac");
    Expect.expect(hexString.equalsIgnoreCase("ac"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00ad");
    Expect.expect(hexString.equalsIgnoreCase("ad"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00ae");
    Expect.expect(hexString.equalsIgnoreCase("ae"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00af");
    Expect.expect(hexString.equalsIgnoreCase("af"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00b0");
    Expect.expect(hexString.equalsIgnoreCase("b0"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00b1");
    Expect.expect(hexString.equalsIgnoreCase("b1"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00b2");
    Expect.expect(hexString.equalsIgnoreCase("b2"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00b3");
    Expect.expect(hexString.equalsIgnoreCase("b3"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00b4");
    Expect.expect(hexString.equalsIgnoreCase("b4"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00b5");
    Expect.expect(hexString.equalsIgnoreCase("b5"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00b6");
    Expect.expect(hexString.equalsIgnoreCase("b6"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00b7");
    Expect.expect(hexString.equalsIgnoreCase("b7"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00b8");
    Expect.expect(hexString.equalsIgnoreCase("b8"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00b9");
    Expect.expect(hexString.equalsIgnoreCase("b9"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00ba");
    Expect.expect(hexString.equalsIgnoreCase("ba"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00bb");
    Expect.expect(hexString.equalsIgnoreCase("bb"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00bc");
    Expect.expect(hexString.equalsIgnoreCase("bc"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00bd");
    Expect.expect(hexString.equalsIgnoreCase("bd"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00be");
    Expect.expect(hexString.equalsIgnoreCase("be"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00bf");
    Expect.expect(hexString.equalsIgnoreCase("bf"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00c0");
    Expect.expect(hexString.equalsIgnoreCase("c0"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00c1");
    Expect.expect(hexString.equalsIgnoreCase("c1"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00c2");
    Expect.expect(hexString.equalsIgnoreCase("c2"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00c3");
    Expect.expect(hexString.equalsIgnoreCase("c3"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00c4");
    Expect.expect(hexString.equalsIgnoreCase("c4"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00c5");
    Expect.expect(hexString.equalsIgnoreCase("c5"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00c6");
    Expect.expect(hexString.equalsIgnoreCase("c6"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00c7");
    Expect.expect(hexString.equalsIgnoreCase("c7"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00c8");
    Expect.expect(hexString.equalsIgnoreCase("c8"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00c9");
    Expect.expect(hexString.equalsIgnoreCase("c9"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00ca");
    Expect.expect(hexString.equalsIgnoreCase("ca"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00cb");
    Expect.expect(hexString.equalsIgnoreCase("cb"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00cc");
    Expect.expect(hexString.equalsIgnoreCase("cc"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00cd");
    Expect.expect(hexString.equalsIgnoreCase("cd"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00ce");
    Expect.expect(hexString.equalsIgnoreCase("ce"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00cf");
    Expect.expect(hexString.equalsIgnoreCase("cf"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00d0");
    Expect.expect(hexString.equalsIgnoreCase("d0"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00d1");
    Expect.expect(hexString.equalsIgnoreCase("d1"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00d2");
    Expect.expect(hexString.equalsIgnoreCase("d2"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00d3");
    Expect.expect(hexString.equalsIgnoreCase("d3"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00d4");
    Expect.expect(hexString.equalsIgnoreCase("d4"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00d5");
    Expect.expect(hexString.equalsIgnoreCase("d5"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00d6");
    Expect.expect(hexString.equalsIgnoreCase("d6"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00d7");
    Expect.expect(hexString.equalsIgnoreCase("d7"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00d8");
    Expect.expect(hexString.equalsIgnoreCase("d8"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00d9");
    Expect.expect(hexString.equalsIgnoreCase("d9"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00da");
    Expect.expect(hexString.equalsIgnoreCase("da"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00db");
    Expect.expect(hexString.equalsIgnoreCase("db"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00dc");
    Expect.expect(hexString.equalsIgnoreCase("dc"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00dd");
    Expect.expect(hexString.equalsIgnoreCase("dd"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00de");
    Expect.expect(hexString.equalsIgnoreCase("de"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00df");
    Expect.expect(hexString.equalsIgnoreCase("df"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00e0");
    Expect.expect(hexString.equalsIgnoreCase("e0"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00e1");
    Expect.expect(hexString.equalsIgnoreCase("e1"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00e2");
    Expect.expect(hexString.equalsIgnoreCase("e2"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00e3");
    Expect.expect(hexString.equalsIgnoreCase("e3"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00e4");
    Expect.expect(hexString.equalsIgnoreCase("e4"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00e5");
    Expect.expect(hexString.equalsIgnoreCase("e5"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00e6");
    Expect.expect(hexString.equalsIgnoreCase("e6"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00e7");
    Expect.expect(hexString.equalsIgnoreCase("e7"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00e8");
    Expect.expect(hexString.equalsIgnoreCase("e8"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00e9");
    Expect.expect(hexString.equalsIgnoreCase("e9"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00ea");
    Expect.expect(hexString.equalsIgnoreCase("ea"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00eb");
    Expect.expect(hexString.equalsIgnoreCase("eb"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00ec");
    Expect.expect(hexString.equalsIgnoreCase("ec"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00ed");
    Expect.expect(hexString.equalsIgnoreCase("ed"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00ee");
    Expect.expect(hexString.equalsIgnoreCase("ee"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00ef");
    Expect.expect(hexString.equalsIgnoreCase("ef"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00f0");
    Expect.expect(hexString.equalsIgnoreCase("f0"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00f1");
    Expect.expect(hexString.equalsIgnoreCase("f1"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00f2");
    Expect.expect(hexString.equalsIgnoreCase("f2"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00f3");
    Expect.expect(hexString.equalsIgnoreCase("f3"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00f4");
    Expect.expect(hexString.equalsIgnoreCase("f4"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00f5");
    Expect.expect(hexString.equalsIgnoreCase("f5"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00f6");
    Expect.expect(hexString.equalsIgnoreCase("f6"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00f7");
    Expect.expect(hexString.equalsIgnoreCase("f7"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00f8");
    Expect.expect(hexString.equalsIgnoreCase("f8"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00f9");
    Expect.expect(hexString.equalsIgnoreCase("f9"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00fa");
    Expect.expect(hexString.equalsIgnoreCase("fa"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00fb");
    Expect.expect(hexString.equalsIgnoreCase("fb"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00fc");
    Expect.expect(hexString.equalsIgnoreCase("fc"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00fd");
    Expect.expect(hexString.equalsIgnoreCase("fd"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00fe");
    Expect.expect(hexString.equalsIgnoreCase("fe"));
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("\u00ff");
    Expect.expect(hexString.equalsIgnoreCase("ff"));

    // Test for empty string input.
    hexString = StringUtil.convertUnicodeStringIntoHexCodedString("");
    Expect.expect(hexString.equalsIgnoreCase(","));

    // Test for null input.
    try
    {
      StringUtil.convertUnicodeStringIntoHexCodedString(null);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // Do nothing.
    }

    // Test round trip through 'convertUnicodeStringIntoHexCodedString()' and
    // 'convertHexCodedStringIntoUnicodeString()'.
    final int MAX_CODE_POINT = 65536;
    final int EXPECTED_ENCODED_SIZE = 323311;
    StringBuffer originalCodePoints = new StringBuffer(MAX_CODE_POINT);

    for (int codePoint = 0; codePoint < MAX_CODE_POINT; ++codePoint)
      originalCodePoints.append((char) codePoint);

    String encoded = StringUtil.convertUnicodeStringIntoHexCodedString(originalCodePoints.toString());
    Expect.expect(encoded.length() == EXPECTED_ENCODED_SIZE);
    String decoded = null;

    try
    {
      decoded = StringUtil.convertHexCodedStringIntoUnicodeString(encoded);
    }
    catch (NumberFormatException nfe)
    {
      os.println("Unexpected NumberFormatException thrown.");
      os.println("NumberFormatException.getMessage(): " + nfe.getMessage());
      Expect.expect(false);
    }
    catch (CharConversionException cce)
    {
      os.println("Unexpected CharConversionException thrown.");
      os.println("CharConversionException.getMessage(): " + cce.getMessage());
      Expect.expect(false);
    }

    Expect.expect(decoded.length() == MAX_CODE_POINT);
    Expect.expect(decoded.equals(originalCodePoints.toString()));
  }

  /**
   * @author George Booth
   */
  private void testConvertStringToTimeInMilliSeconds()
  {
    // negative time
    boolean exception = false;
    long timeInMils = -120L;
    String timeStamp = null;
    try
    {
      timeStamp = StringUtil.convertMilliSecondsToTimeStamp(timeInMils);
      Expect.expect(false); // Shouldn't ever happen.
    }
    catch (AssertException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    // zero time
    timeInMils = 0;
    long convertedTime = 0;
    timeStamp = StringUtil.convertMilliSecondsToTimeStamp(timeInMils);
//    System.out.println(timeStamp);
    Expect.expect(timeStamp.equals("1970-01-01_00-00-00-00"));
    try
    {
      convertedTime = StringUtil.convertStringToTimeInMilliSeconds(timeStamp);
      Expect.expect(convertedTime == timeInMils);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false); // Shouldn't ever happen.
    }

    // current time
    timeInMils = 1174676934068L;
    timeStamp = StringUtil.convertMilliSecondsToTimeStamp(timeInMils);
//    System.out.println(timeStamp);
    Expect.expect(timeStamp.equals("2007-03-23_19-08-54-68"));
    try
    {
      convertedTime = StringUtil.convertStringToTimeInMilliSeconds(timeStamp);
      Expect.expect(convertedTime == timeInMils);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false); // Shouldn't ever happen.
    }

    // future time
    timeInMils = 3000000000000L;
    timeStamp = StringUtil.convertMilliSecondsToTimeStamp(timeInMils);
//    System.out.println(timeStamp);
    Expect.expect(timeStamp.equals("2065-01-24_05-20-00-00"));
    try
    {
      convertedTime = StringUtil.convertStringToTimeInMilliSeconds(timeStamp);
      Expect.expect(convertedTime == timeInMils);
    }
    catch (BadFormatException bfe)
    {
      Expect.expect(false); // Shouldn't ever happen.
    }

    // null string
    exception = false;
    timeStamp = null;
    try
    {
      convertedTime = StringUtil.convertStringToTimeInMilliSeconds(timeStamp);
      Expect.expect(false); // Shouldn't ever happen.
    }
    catch (AssertException e)
    {
      exception = true;
    }
    catch (BadFormatException e)
    {
      // this exception should not happen
      e.printStackTrace();
    }
    Expect.expect(exception);
    exception = false;

    // empty string
    timeStamp = "";
    try
    {
      convertedTime = StringUtil.convertStringToTimeInMilliSeconds(timeStamp);
      Expect.expect(false); // Shouldn't ever happen.
    }
    catch (BadFormatException bfe)
    {
      // Do nothing.  We expect this to occur.
    }

    // bad string
    timeStamp = "FooBar";
    try
    {
      convertedTime = StringUtil.convertStringToTimeInMilliSeconds(timeStamp);
      Expect.expect(false); // Shouldn't ever happen.
    }
    catch (BadFormatException bfe)
    {
      // Do nothing.  We expect this to occur.
    }
  }
}
