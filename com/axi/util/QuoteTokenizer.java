package com.axi.util;

/**
 * This class takes a line and parses out tokens delimited by quotes(");
 * For example, the following line will be parsed:
 *  abc"def" "ghi""jkl"
 * nextToken() returns def
 * nextToken() returns ghi
 * nextToken() returns jkl
 * nextToken() returns null
 * @author George A. David
 */
public class QuoteTokenizer
{
  private String _line = null;
  private final char _QUOTE = '\"';
  private int _currentPosition = -1;

  /**
   * @author George A. David
   */
  public QuoteTokenizer(String line)
  {
    Assert.expect(line != null);
    _line = line;
    _currentPosition = 0;
  }

  /**
   * @return the next quote delimited token
   * @author George A. David
   */
  public String nextToken()
  {
    String token = null;

    try
    {
      int index = _line.indexOf(_QUOTE,_currentPosition);
      if(index < 0)
        return null;
      int begin = index + 1;
      int end = _line.indexOf(_QUOTE,begin);
      token = _line.substring(begin,end);
      _currentPosition = end + 1;
    }
    catch(IndexOutOfBoundsException e)
    {
      token = null;
      _currentPosition = _line.length();
    }

    return token;
  }

  /**
   * @return true if the line has more tokens, false if it does not
   * @author George A. David
   */
  public boolean hasMoreTokens()
  {
    int currentPosition = _currentPosition;
    String token = null;
    token = nextToken();
    _currentPosition = currentPosition;
    return (token != null);
  }
}