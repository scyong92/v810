package com.axi.util;

/**
 * This class provides memory management functionality.
 *
 * @author Bill Darbie
 */
public class MemoryUtil
{
  private static WorkerThread _workerThread = new WorkerThread("memory minimizer");
  protected static boolean _debug = false;

  /**
   * Try to get the garbage collector to clean up as much memory as possible
   * @author Bill Darbie
   */
  public synchronized static void garbageCollect()
  {
    _workerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        int numLoops = 2;
        for(int i = 0; i < numLoops; ++i)
        {
          // tell the garbage collector to clean up what it can
          System.gc();
          if (i < numLoops - 1)
          {
            try
            {
              // wait a little while before doing it again.
              Thread.sleep(500);
            }
            catch(InterruptedException ie)
            {
              // do nothing
            }
          }
        }
      }
    });
  }
  
  /**
   * @author Wei Chin
   */
  public static void freeMemory()
  {
    long maxMem = Runtime.getRuntime().maxMemory();
    long freeMemAfter = Runtime.getRuntime().freeMemory();
    double percentAvail = (100 - (100 * (maxMem - freeMemAfter) / maxMem));
    if (percentAvail < 0.7)
    {
	  if(_debug)
        displayDebugUtilizationReport();
      garbageCollect();
	}
  }
  
  /**
   * @author Wei Chin
   */
  public static synchronized void displayDebugUtilizationReport()
  {
    double oneMegabyte = 1024.0*1024.0;
    System.out.println("Memory Utilization Report at " + (new java.util.Date(System.currentTimeMillis())).toString());
    double freeMemory = Runtime.getRuntime().freeMemory() / oneMegabyte;
    double maxMemory = Runtime.getRuntime().maxMemory() / oneMegabyte;
    double totalMemory = Runtime.getRuntime().totalMemory() / oneMegabyte;
    System.out.println("JAVA Free Memory : " + freeMemory + " / Max Memory : " + maxMemory + " / Total Memory : " + totalMemory);
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR2183: Standardize all print out when developer debug is true
   */
  public static void setDebug(boolean state)
  {
    _debug = state;
  }
}
