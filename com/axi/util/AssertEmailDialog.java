package com.axi.util;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;

/**
* This class creates a dialog with the information required to allow the user
* to send email to Agilent about an Assert error.
*
* @author Steve Anonson
*/
public class AssertEmailDialog extends JDialog
{
  private static String _title = "Send Email to ViTrox Technologies";
  private static String _instructions = "" +
  "Describe how you were using the system when the error occurred and any other " +
  "information that may be useful to duplicate the error.  " +
  "The error message and stack trace will be added to the email automatically.";

  private String _customerCompanyName = null;
  private String _customerName = null;
  private String _customerEmail = null;
  private String _customerPhoneNumber = null;
  private String _customerSmtpServer = null;
  private String _axiEmailAddress = null;
  private String _emailSubject = null;
  private String _stackTrace = null;
  private EmailSender _emailSender = new EmailSender();

  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelLayout = new BorderLayout();
  private JPanel _northPanel = new JPanel();
  private JPanel _centerPanel = new JPanel();
  private JPanel _southPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private BorderLayout _southPanelLayout = new BorderLayout();
  private BorderLayout _centerPanelLayout = new BorderLayout();
  private BorderLayout _northPanelLayout = new BorderLayout();
  private JPanel _customerInfoPanel = new JPanel();
  private GridLayout _customerInfoPanelLayout = new GridLayout();
  private JPanel _descPanel = new JPanel();
  private BorderLayout _descPanelLayout = new BorderLayout();
  private GridLayout _buttonPanelLayout = new GridLayout();
  private Border _border1 = null;
  private TitledBorder _titledBorder1 = null;
  private Border _border2 = null;
  private JButton _sendButton = new JButton();
  private JButton _cancelButton = new JButton();
  private JPanel _toPanel = new JPanel();
  private JLabel _subjectLabel = new JLabel();
  private JLabel _toLabel = new JLabel();
  private JTextArea _instructionTextArea = new JTextArea();
  private Border _instructionTextAreaBorder = null;
  private JScrollPane _descScrollPane = new JScrollPane();
  private JTextArea _descTextArea = new JTextArea();
  private Border _descTextAreaBorder = null;
  private JLabel _companyNameLabel = new JLabel();
  private JTextField _serverTextF = new JTextField();
  private JLabel _serverLabel = new JLabel();
  private JTextField _phoneTextF = new JTextField();
  private JLabel _phoneLabel = new JLabel();
  private JTextField _emailAddrTextF = new JTextField();
  private JLabel _emailAddrLabel = new JLabel();
  private JTextField _nameTextF = new JTextField();
  private JLabel _nameLabel = new JLabel();
  private JTextField _companyNameTextF = new JTextField();
  private GridLayout _toPanelLayout = new GridLayout();
  private Border _toPanelBorder = null;

  /**
  * This method fills in the dialog data after the contents of the dialog
  * has been created.
  *
  * @author Steve Anonson
  */
  private void init()
  {
    jbInit();
    _instructionTextArea.setText(_instructions);
    _descTextArea.setText("");

    if (_customerCompanyName != null)
      _companyNameTextF.setText(_customerCompanyName);
    else
      _companyNameTextF.setText("");
    if (_customerName != null)
      _nameTextF.setText(_customerName);
    else
      _nameTextF.setText("");
    if (_customerEmail != null)
      _emailAddrTextF.setText(_customerEmail);
    else
      _emailAddrTextF.setText("");
    if (_customerPhoneNumber != null)
      _phoneTextF.setText(_customerPhoneNumber);
    else
      _phoneTextF.setText("");
    if (_customerSmtpServer != null)
      _serverTextF.setText(_customerSmtpServer);
    else
      _serverTextF.setText("");

    if (_axiEmailAddress != null)
      _toLabel.setText("To: " + _axiEmailAddress);
    else
      _toLabel.setText("To: ");
    if (_emailSubject != null)
      _subjectLabel.setText("Subject: " + _emailSubject);
    else
      _subjectLabel.setText("Subject: ");
    pack();
  }

  /**
  * This contructor takes the Assert and customer information and creates a
  * dialog with a Frame parent.
  *
  * @param frame - Frame parent of the dialog, can be null.
  * @param modal - flag to determine if the dialog is modal.
  * @param customerCompanyName - String containing the customer's company name.
  * @param customerName - String containing the technician/operator name.
  * @param customerEmail - String containing the customer's email address.
  * @param customerPhoneNumber - String containing the customer's phone number.
  * @param customerSmtpServer - String containing the customer's SMTP email server.
  * @param axiEmailAddress - String containing the Agilent support email address.
  * @param emailSubject - String containing the subject of the email.
  * @author Steve Anonson
  */
  public AssertEmailDialog(Frame frame, boolean modal,
                            String customerCompanyName,
                            String customerName,
                            String customerEmail,
                            String customerPhoneNumber,
                            String customerSmtpServer,
                            String axiEmailAddress,
                            String emailSubject)
  {
    super(frame, _title, modal);
    _customerCompanyName = customerCompanyName;
    _customerName = customerName;
    _customerEmail = customerEmail;
    _customerPhoneNumber = customerPhoneNumber;
    _customerSmtpServer = customerSmtpServer;
    _axiEmailAddress = axiEmailAddress;
    _emailSubject = emailSubject;
    init();
  }

  /**
  * This contructor takes the Assert and customer information and creates a
  * dialog with a Dialog parent.
  *
  * @param frame - Dialog parent of the dialog, can be null.
  * @param modal - flag to determine if the dialog is modal.
  * @param customerCompanyName - String containing the customer's company name.
  * @param customerName - String containing the technician/operator name.
  * @param customerEmail - String containing the customer's email address.
  * @param customerPhoneNumber - String containing the customer's phone number.
  * @param customerSmtpServer - String containing the customer's SMTP email server.
  * @param axiEmailAddress - String containing the Agilent support email address.
  * @param emailSubject - String containing the subject of the email.
  * @author Steve Anonson
  */
  public AssertEmailDialog(Dialog frame, boolean modal,
                            String customerCompanyName,
                            String customerName,
                            String customerEmail,
                            String customerPhoneNumber,
                            String customerSmtpServer,
                            String axiEmailAddress,
                            String emailSubject)
  {
    super(frame, _title, modal);
    _customerCompanyName = customerCompanyName;
    _customerName = customerName;
    _customerEmail = customerEmail;
    _customerPhoneNumber = customerPhoneNumber;
    _customerSmtpServer = customerSmtpServer;
    _axiEmailAddress = axiEmailAddress;
    _emailSubject = emailSubject;
    init();
  }

  /*
  * This constructor is hidden.
  */
  private AssertEmailDialog()
  {
    /* Don't use this constructor */
  }

  /**
  * This method creates the contents of the dialog.
  *
  * @author Steve Anonson
  */
  private void jbInit()
  {
    _border1 = BorderFactory.createLineBorder(SystemColor.controlText,1);
    _titledBorder1 = new TitledBorder(_border1,"Customer Information (correct if necessary)");
    _border2 = BorderFactory.createCompoundBorder(_titledBorder1,BorderFactory.createEmptyBorder(5,5,5,5));
    _instructionTextAreaBorder = BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.white,1),BorderFactory.createEmptyBorder(10,5,10,5));
    _descTextAreaBorder = BorderFactory.createEmptyBorder(5,5,5,5);
    _toPanelBorder = BorderFactory.createEmptyBorder(5,5,5,5);
    _mainPanel.setLayout(_mainPanelLayout);
    _southPanel.setLayout(_southPanelLayout);
    _centerPanel.setLayout(_centerPanelLayout);
    _northPanel.setLayout(_northPanelLayout);
    _customerInfoPanel.setLayout(_customerInfoPanelLayout);
    _customerInfoPanelLayout.setColumns(2);
    _customerInfoPanelLayout.setRows(5);
    _customerInfoPanelLayout.setVgap(3);
    _descPanel.setLayout(_descPanelLayout);
    _buttonPanel.setLayout(_buttonPanelLayout);
    _northPanel.setFont(new java.awt.Font("SansSerif", 1, 16));
    _northPanel.setBorder(_border2);

    _sendButton.setFont(new java.awt.Font("SansSerif", 0, 16));
    _sendButton.setMnemonic('S');
    _sendButton.setText("Send");
    java.awt.event.ActionListener sendAL = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        sendButton_actionPerformed(e);
      }
    };
    _sendButton.addActionListener( sendAL );
    _sendButton.registerKeyboardAction( sendAL,
                                        KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                                        JComponent.WHEN_FOCUSED);

    _cancelButton.setFont(new java.awt.Font("SansSerif", 0, 16));
    _cancelButton.setMnemonic('C');
    _cancelButton.setText("Cancel");
    java.awt.event.ActionListener cancelAL = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    };
    _cancelButton.addActionListener( cancelAL );
    _cancelButton.registerKeyboardAction( cancelAL,
                                          KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                                          JComponent.WHEN_FOCUSED);

    _toPanel.setLayout(_toPanelLayout);
    _subjectLabel.setFont(new java.awt.Font("SansSerif", 0, 16));
    _subjectLabel.setText("Subject: ");
    _toLabel.setFont(new java.awt.Font("SansSerif", 0, 16));
    _toLabel.setText("To: ");
    _instructionTextArea.setLineWrap(true);
    _instructionTextArea.setColumns(50);
    _instructionTextArea.setWrapStyleWord(true);
    _instructionTextArea.setOpaque(false);
    _instructionTextArea.setBorder(_instructionTextAreaBorder);
    _instructionTextArea.setText("instructions");
    _instructionTextArea.setEditable(false);
    _instructionTextArea.setFont(new java.awt.Font("SansSerif", 1, 16));
    _descTextArea.setLineWrap(true);
    _descTextArea.setColumns(50);
    _descTextArea.setWrapStyleWord(true);
    _descTextArea.setRows(8);
    _descTextArea.setBorder(_descTextAreaBorder);
    _descTextArea.setText("description");
    _descTextArea.setFont(new java.awt.Font("SansSerif", 0, 16));
    _companyNameLabel.setFont(new java.awt.Font("SansSerif", 0, 16));
    _companyNameLabel.setForeground(Color.black);
    _companyNameLabel.setText("Company Name");
    _serverTextF.setFont(new java.awt.Font("SansSerif", 0, 16));
    _serverTextF.setText("server@company.com");
    _serverLabel.setFont(new java.awt.Font("SansSerif", 0, 16));
    _serverLabel.setForeground(Color.black);
    _serverLabel.setText("Site Email Server (SMTP Server)");
    _phoneTextF.setFont(new java.awt.Font("SansSerif", 0, 16));
    _phoneTextF.setText("(888) 555-1212");
    _phoneLabel.setFont(new java.awt.Font("SansSerif", 0, 16));
    _phoneLabel.setForeground(Color.black);
    _phoneLabel.setText("Phone Number");
    _emailAddrTextF.setFont(new java.awt.Font("SansSerif", 0, 16));
    _emailAddrTextF.setText("email address");
    _emailAddrLabel.setFont(new java.awt.Font("SansSerif", 0, 16));
    _emailAddrLabel.setForeground(Color.black);
    _emailAddrLabel.setText("Email Address");
    _nameTextF.setFont(new java.awt.Font("SansSerif", 0, 16));
    _nameTextF.setText("your name here");
    _nameLabel.setFont(new java.awt.Font("SansSerif", 0, 16));
    _nameLabel.setForeground(Color.black);
    _nameLabel.setText("Your Name");
    _companyNameTextF.setFont(new java.awt.Font("SansSerif", 0, 16));
    _companyNameTextF.setText("company name");
    _titledBorder1.setTitleFont(new java.awt.Font("SansSerif", 1, 16));
    _toPanelLayout.setColumns(1);
    _toPanelLayout.setRows(2);
    _toPanel.setBorder(_toPanelBorder);
    getContentPane().add(_mainPanel);
    _mainPanel.add(_northPanel, BorderLayout.NORTH);
    _northPanel.add(_customerInfoPanel, BorderLayout.CENTER);
    _customerInfoPanel.add(_companyNameLabel, null);
    _customerInfoPanel.add(_companyNameTextF, null);
    _customerInfoPanel.add(_nameLabel, null);
    _customerInfoPanel.add(_nameTextF, null);
    _customerInfoPanel.add(_emailAddrLabel, null);
    _customerInfoPanel.add(_emailAddrTextF, null);
    _customerInfoPanel.add(_phoneLabel, null);
    _customerInfoPanel.add(_phoneTextF, null);
    _customerInfoPanel.add(_serverLabel, null);
    _customerInfoPanel.add(_serverTextF, null);
    _mainPanel.add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.add(_descPanel, BorderLayout.CENTER);
    _descPanel.add(_instructionTextArea, BorderLayout.NORTH);
    _descPanel.add(_descScrollPane, BorderLayout.CENTER);
    _descScrollPane.getViewport().add(_descTextArea, null);
    _centerPanel.add(_toPanel, BorderLayout.NORTH);
    _toPanel.add(_toLabel, null);
    _toPanel.add(_subjectLabel, null);
    _mainPanel.add(_southPanel, BorderLayout.SOUTH);
    _southPanel.add(new JSeparator(), BorderLayout.NORTH);
    _southPanel.add(_buttonPanel, BorderLayout.CENTER);
    _buttonPanel.add(_sendButton, null);
    _buttonPanel.add(_cancelButton, null);
  }

  /**
  * This method removes the AssertEmailDialog.
  *
  * @author Steve Anonson
  */
  private void cancelDialog()
  {
    setVisible(false);
    dispose();
  }

  /**
  * This is the method that gets called when the cancel button is pressed.
  *
  * @author Steve Anonson
  */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    cancelDialog();
  }

  /**
  * This method creates an email message which is sent to Agilent.
  *
  * @author Steve Anonson
  */
  private void sendEmail()
  {
    String smtpServer = _serverTextF.getText();
    String customerEmailAddress = _emailAddrTextF.getText();
    StringBuffer emailMessage = new StringBuffer();
    emailMessage.append("This is an automatically generated email for reporting internal software errors.\n\n");
    emailMessage.append("company name: " + _companyNameTextF.getText() + "\n");
    emailMessage.append("contact name: " + _nameTextF.getText() + "\n");
    emailMessage.append("phone number: " + _phoneTextF.getText() + "\n\n");
    emailMessage.append("Customer\'s description of what happened:\n" + _descTextArea.getText() + "\n\n");
    if (_stackTrace != null)
      emailMessage.append("Stack trace information:\n" + _stackTrace);

    // Check that all required information has been provided.
    if (customerEmailAddress == null ||             // check for null
        customerEmailAddress.equals("") ||          // check for empty
        customerEmailAddress.indexOf(" ") != -1 ||  // check for no spaces
        customerEmailAddress.indexOf("@") == -1 ||  // check for @ character
        (customerEmailAddress.lastIndexOf(".") < customerEmailAddress.indexOf("@")) )  // check to make sure that a
                                                                                       // . occurs after a @
                                                                                       // (email@company.com form)
    {
      JOptionPane.showMessageDialog(this,
                                    "Error sending email - check that the customer email address is correct",
                                    "Customer address not valid",
                                    JOptionPane.ERROR_MESSAGE);
      return;
    }

    boolean emailSent = false;
    try
    {
       _emailSender.sendEmail(smtpServer,
                              customerEmailAddress,
                              _axiEmailAddress + " " + customerEmailAddress,
                              _emailSubject,
                              emailMessage.toString());
       emailSent = true;
    }
    catch(final IOException ex)
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          String message = "There was an error connecting to or communicating with the email server " +
                           _customerSmtpServer + "\n" + ex.getMessage();
          JOptionPane.showMessageDialog(AssertEmailDialog.this,
                                        message,
                                        "There was an error connecting to or communicating with the email server " + _customerSmtpServer,
                                        JOptionPane.ERROR_MESSAGE);
        }
      });
    }

    if (emailSent)
    {
      setVisible(false);
      dispose();
    }
  }

  /**
  * This is the method that gets called when the send button is clicked.
  *
  * @author Steve Anonson
  */
  private void sendButton_actionPerformed(ActionEvent e)
  {
    sendEmail();
  }

  /**
  * Set the stack trace string that will be included in the email.
  *
  * @param stackTrace String containing the stack trace.
  * @author Steve Anonson
  */
  public void setStackTrace( String stackTrace )
  {
    _stackTrace = stackTrace;
  }

  /*
  * Main function used only for testing.
  */
  public static void main( String [] args )
  {
    AssertEmailDialog dlg = new AssertEmailDialog((Frame)null, false,
    "Company Name", "My Name", "My Email", "Phone #", "myserver.com", "support@xxx.com", "Internal error subject");
    // Add listener to exit if dialog is closed.
    dlg.addWindowListener( new WindowAdapter()
    {
      public void windowClosing( WindowEvent e )
      {
        System.exit(0);
      }
    } );
    dlg.setVisible(true);
  }
}
