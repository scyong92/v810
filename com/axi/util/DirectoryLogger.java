package com.axi.util;

import java.io.*;
import java.util.*;
import java.text.*;
import com.axi.util.*;


/**
 * The DirectoryLogger class writes log strings into unique
 * files (one log string = one file).  This can be handy when log strings
 * are long and not easy to read or access from one giant file.
 *
 * Timestamps that are part of the file name are used to implement logging
 * frequency.  The timestamp is a 14 digit number of the form
 * <year>.<month>.<day>.<hour>.<minute>.<seconds>.<milliseconds> where year is 4 digits
 * and milliseconds is 3.  All others are 2.  Note, if logging is attempted more frequently
 * than once every millisecond DirectoryLogger will overwrite log files, leaving
 * one for each millisecond period.
 *
 * Localization of the timestamp is not supported and is in fact
 * forced to US English to avoid unexpected behavior.
 *
 * The maximum number of log files allowed in the directory can be specified.
 * When this number is reached the oldest log files are deleted based on the
 * timestamp in the file name.
 *
 * NOTE for AXI do NOT use this class,
 * use com.axi.xRayTest.util.DirectoryLoggerAxi instead.  It is a wrapper for
 * this class that converts all the exceptions thrown here into DatastoreExceptions
 * If you add a method to this class MAKE SURE TO ADD IT to DirectoryLoggerAxi also.
 *
 * @author John Dutton
 */
public class DirectoryLogger
{
  private String _logFilesDirectory;
  private String _logFileNamePrefix;
  private String _logFileNameExtension;
  private int _maxNosOfLogFiles;
  private String _additionalHeaderInfo = "";

  private final SimpleDateFormat _filenameSimpleDateFormat =
      new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss.SSS", new Locale("en", "US"));
  private final SimpleDateFormat _headerSimpleDateFormat =
      new SimpleDateFormat("MM/dd/yyyy h:mm:ss a", new Locale("en", "US"));


  /**
   *
   * @param logFilesDirectory where to create log files
   * @param logFileNamePrefix string to prepend to the timestamp part of log
   *   file names
   * @author John Dutton
   */
  public DirectoryLogger(String logFilesDirectory,
                         String logFileNamePrefix,
                         String logFileNameExtension,
                         int maxNosOfLogFiles)
 {

   Assert.expect(logFilesDirectory != null);
   Assert.expect(logFileNamePrefix != null);
   Assert.expect(logFileNameExtension != null);
   Assert.expect(maxNosOfLogFiles > 0);

   _logFilesDirectory = logFilesDirectory;
   _logFileNamePrefix = logFileNamePrefix;
   _logFileNameExtension = logFileNameExtension;
   _maxNosOfLogFiles = maxNosOfLogFiles;
 }

  /**
   * Creates a log file and writes log string to it.  Then the maximum number of
   * log files is checked and the oldest log files are deleted if necessary.
   *
   * @author John Dutton
   */
  public void log(String logString) throws CouldNotCreateFileException,
                                           CouldNotDeleteFileException,
                                           FileFoundWhereDirectoryWasExpectedException
  {
    Assert.expect(logString != null);
    writeLogString(logString);
  }

  /**
   * Creates a log file and writes log string to it if elapsed time threshold has
   * passed since the last log file was created.  If creation of a log file
   * occurs, then the maximum number of log files is checked and the oldest log files are
   * deleted if necessary.
   *
   * @param elapsedTimeThresholdMillis how much time needs to have elapsed before another log file
   *   will be created.
   * @return true if log file was created, false if not
   * @author John Dutton
   * @author Eddie Williamson
   */
  public boolean logIfTimedOut(String logString,
                               long elapsedTimeThresholdMillis)
      throws CouldNotCreateFileException,
             CouldNotDeleteFileException,
             FileFoundWhereDirectoryWasExpectedException
  {
    Assert.expect(logString != null);
    // 1000*60*60*24*365 is one year in seconds.
    // Eddie -- must calculate as longs or it truncates to the wrong number
    Assert.expect(1000L*60L*60L*24L*365L >= elapsedTimeThresholdMillis && elapsedTimeThresholdMillis > -1);

    if (isLoggingNeeded(elapsedTimeThresholdMillis))
    {
      writeLogString(logString);
      return true;
    }
    else
      return false;
  }


  /**
   * Specify additional information to append to the standard header (date and time), for
   * instance, the serial number of a relevant device.
   *
   * @author John Dutton
   */
  public void setAdditionalHeaderInfo(String additionalHeaderInfo)
  {
    _additionalHeaderInfo = additionalHeaderInfo;
  }

  /**
   * Determine if elapsed time threshold has passed since the last log file was
   * created and written.
   *
   * @return true if a log file should be created and written to.
   * @author John Dutton
   */
  public boolean isLoggingNeeded(long elapsedTimeThresholdMillis)
  {
    // 1000*60*60*24*365 is one year in seconds.
    // Eddie -- must calculate as longs or it truncates to the wrong number
    Assert.expect(1000L * 60L * 60L * 24L * 365L >= elapsedTimeThresholdMillis && elapsedTimeThresholdMillis > -1);

    Date newestKnownDate = getNewestLogFileDate();
    Date currentDate = new Date();
    long elapsedTime = currentDate.getTime() - newestKnownDate.getTime();
    return elapsedTime > elapsedTimeThresholdMillis;
  }

  /**
   * Creates a new log file and writes log data to it.  Deletes log files if more
   * than maximum number of files allowed exist.
   *
   * @author John Dutton
   */
  private void writeLogString(String logString) throws CouldNotCreateFileException,
                                                       CouldNotDeleteFileException,
                                                       FileFoundWhereDirectoryWasExpectedException
  {
    Assert.expect(logString != null);

    // Avoid exception that would be caused if _logFilesDirectory doesn't already exist.
    if (!FileUtil.existsDirectory(_logFilesDirectory))
      FileUtil.mkdirs(_logFilesDirectory);

    FileWriterUtil fwu =
      new FileWriterUtil(_logFilesDirectory + File.separator + createNewFileName(), false);
    fwu.open();
    fwu.write(createNewLogStringHeader() + logString);
    fwu.close();
    cleanupLogDir();
  }

  /**
   * If log directory contains more log files than maximum number of files
   * allowed then log files are deleted to get to that number.   Oldest files
   * according to filename timestamps are deleted first.
   *
   * @author John Dutton
   */
  private void cleanupLogDir() throws CouldNotDeleteFileException
  {
    Set<String> fileNames = getListOfSortedLogFileNames();
    Iterator<String> iter = fileNames.iterator();
    for (int nosToDelete = fileNames.size() - _maxNosOfLogFiles; nosToDelete > 0; nosToDelete--)
    {
      // The first filename is the oldest since the lexicographical sort of
      // timestamps is from oldest to newest.
      String fileName = iter.next();
      String fileNamePath = _logFilesDirectory + File.separator + fileName;
      FileUtil.delete(fileNamePath);
    }
  }

  /**
   * @return Date of the timestamp of the most recent log file.  If none
   * exists the oldest Date possible is returned to force logging.
   * @author John Dutton
   */
  private Date getNewestLogFileDate()
  {
    // Get newest log file name
    String newestKnownLogFileName = null;
    SortedSet<String> fileNames = getListOfSortedLogFileNames();
    if (!fileNames.isEmpty())
      // Most recent file is at bottom of tree set.
      newestKnownLogFileName = fileNames.last();

    Date date;
    if (newestKnownLogFileName == null)
      // If no file (name) return the oldest date possible, 01/01/1970, which
      // will force logging to occur.
      date = new Date(0);
    else
    {
      try
      {
        date = parseDateFromFileName(newestKnownLogFileName);
      }
      catch (ParseException e)
      {
        // This should not happen because getListOfSortedLogFileNames should not return
        // an invalid file name.
        Assert.expect(false);
        date = null; // to avoid compiler error
      }
    }
    return date;
  }

  /**
   * Iterates over all files in the log directory, removing those that aren't
   * valid log file names, and then sorts the filenames from oldest to newest.
   * Valid log file names have the expected prefix and extension, a timestamp of
   * the correct form and a timestamp that has occurred in the past relative to
   * the system clock time.
   *
   * @return a SortedSet<String> of sorted log filenames
   * @author John Dutton
   */
  public SortedSet<String> getListOfSortedLogFileNames()
  {
    Collection<String> files = FileUtil.listAllFilesInDirectory(_logFilesDirectory);

    for (Iterator<String> iter = files.iterator(); iter.hasNext(); )
    {
      String fileName = iter.next();
      if (!fileName.startsWith(_logFileNamePrefix) || !fileName.endsWith(_logFileNameExtension))
        iter.remove();
      else
      {
        try
        {
          Date date = parseDateFromFileName(fileName);

          // Check that time is older (happened earlier) than the system clock.  (Unlikely but possible.)
          if (date.compareTo(new Date()) > 0)
            iter.remove();
        }
        catch (ParseException e)
        {
          // Embedded time stamp wasn't valid because couldn't parse date (again, unlikely).
          iter.remove();
        }
      }
    }

    // Make sure collection is sorted from oldest to newest, i.e. most recent should be at
    // bottom of tree set.
    SortedSet<String> sortedFileNames = new TreeSet<String>(files);
    return sortedFileNames;
  }

  /**
   * Strips off base filename and filename extension and then parses a Java
   * Date from the remaining timestamp.
   *
   * @param fileName is assumed to have a valid file name prefix and extension
   * @return Date parsed from timestamp in filename
   * @author John Dutton
   */
  private Date parseDateFromFileName(String fileName) throws ParseException
  {
    Assert.expect(fileName != null);
    // Remove baseFileName and fileNameExtension
    int startIndex = _logFileNamePrefix.length();
    int endIndex = fileName.length() - _logFileNameExtension.length();
    String timeStamp = fileName.substring(startIndex, endIndex);

    // If for some reason timestamp is invalid, an exception will be thrown.
    Date date = _filenameSimpleDateFormat.parse(timeStamp);
    return date;
  }

  /**
   * Creates a file name using the base filename, the filename extension, and a
   * timestamp based on current system clock.
   *
   * @return file name for a new log file
   * @author John Dutton
   */
  private String createNewFileName()
  {
    String datePartOfFileName = _filenameSimpleDateFormat.format(Calendar.getInstance().getTime());
    return _logFileNamePrefix + datePartOfFileName + _logFileNameExtension;
  }

  /**
   * Creates a timestamped header for a log string.  Header will be on its own line.
   *
   * @author John Dutton
   */
  private String createNewLogStringHeader()
  {
    String newline = System.getProperty("line.separator");
    Date currentDate = new Date();
    return _headerSimpleDateFormat.format(currentDate) + _additionalHeaderInfo + newline;
  }
}
