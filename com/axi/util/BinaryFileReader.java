package com.axi.util;

import java.io.*;
/**
 *
 * @author Ronald Lim
 *
 */
public class BinaryFileReader
{
  private int _BUFFER_SIZE =1024;


  /**
   *
   * @author Ronald Lim
   */
  public byte[] readFileInByte(String filename )throws FileNotFoundException, IOException
  {
    Assert.expect(filename != null);

    File fileToRead = new File (filename);

    FileInputStream in = new FileInputStream(fileToRead);
    byte[] buffer = new byte[_BUFFER_SIZE];
    int num = 0;
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    try
    {
      while( (num = in.read(buffer)) != -1)
      {
        out.write(buffer,0,num);
      }
    }

    catch (IndexOutOfBoundsException e)
    {
       Assert.logException(e);
    }

    return out.toByteArray();
    }


}
