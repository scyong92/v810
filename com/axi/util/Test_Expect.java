package com.axi.util;

import java.io.*;

public class Test_Expect extends UnitTest
{
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_Expect());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    Expect.expect(true);
    Expect.expect(true, "this is true");
    Expect.expect(false);
    Expect.expect(false, "this is false");
    Expect.expect(true);

    // Test out Expect.expectException().
    Expect.expectException(new RunnableWithExceptions()
    {
      public void run() throws Exception
      {
        throw new Exception("you got pwned!");
      }
    }, Exception.class);

    // Test out Expect.expectAssert().
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        Assert.expect(false);
      }
    });
  }
}
