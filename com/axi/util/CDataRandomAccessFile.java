package com.axi.util;

import java.io.*;

/**
 * This class is implemented to read and write binary data compatible with a
 * 32 bit C or Pascal program with random access.
 *
 * <pre>
 * The size of C types are:
 * bool          : 1 byte
 * char          : 1 byte
 * unsigned char : 1 byte
 * short         : 2 bytes
 * unsigned short: 2 bytes
 * int           : 4 bytes
 * unsigned int  : 4 bytes
 * long          : 4 bytes
 * unsigned long : 4 bytes
 * float         : 4 bytes
 * double        : 8 bytes
 * </pre>
 *
 * @see java.io.RandomAccessFile
 * @author Michael Martinez-Schiferl
 */
public abstract class CDataRandomAccessFile implements DataOutput, DataInput
{
  private RandomAccessFile _randomAccessFile;

  /**
   * @see       java.io.RandomAccessFile#RandomAccessFile(String,String)
   * @author    Michael Martinez-Schiferl
   */
  public CDataRandomAccessFile(String name, String mode) throws FileNotFoundException
  {
    Assert.expect(name != null);
    Assert.expect(mode != null);

    _randomAccessFile = new RandomAccessFile(name, mode);
  }

  /**
   * @see       java.io.RandomAccessFile#RandomAccessFile(File,String)
   * @author    Michael Martinez-Schiferl
   */
  public CDataRandomAccessFile(File file, String mode) throws FileNotFoundException
  {
    Assert.expect(file != null);
    Assert.expect(mode != null);

    _randomAccessFile = new RandomAccessFile(file, mode);
  }

  /**
   * @see       java.io.RandomAccessFile#getFD()
   * @author    Michael Martinez-Schiferl
   */
  public FileDescriptor getFileDescriptor() throws IOException
  {
    return _randomAccessFile.getFD();
  }

  /**
   * @see       java.io.RandomAccessFile#read()
   * @author    Michael Martinez-Schiferl
   */
  public int read() throws IOException
  {
    return _randomAccessFile.read();
  }

  /**
   * @see       java.io.RandomAccessFile#read(byte[],int,int)
   * @author    Michael Martinez-Schiferl
   */
  public int read(byte[] byteArray, int offset, int length) throws IOException
  {
    Assert.expect(byteArray != null);
    Assert.expect(offset >= 0);
    Assert.expect(length >= 0);
    Assert.expect(offset + length < byteArray.length);

    return _randomAccessFile.read(byteArray, offset, length);
  }

  /**
   * @see       java.io.RandomAccessFile#read(byte[])
   * @author    Michael Martinez-Schiferl
   */
  public int read(byte[] byteArray) throws IOException
  {
    Assert.expect(byteArray != null);

    return _randomAccessFile.read(byteArray);
  }

  /**
   * @see       java.io.RandomAccessFile#readFully(byte[])
   * @author    Michael Martinez-Schiferl
   */
  public void readFully(byte[] byteArray) throws IOException
  {
    Assert.expect(byteArray != null);
    _randomAccessFile.readFully(byteArray);
  }

  /**
   * @see       java.io.RandomAccessFile#readFully(byte[],int,int)
   * @author    Michael Martinez-Schiferl
   */
  public void readFully(byte[] byteArray, int offset, int length) throws IOException
  {
    Assert.expect(byteArray != null);
    Assert.expect(offset >= 0);
    Assert.expect(length >= 0);
    Assert.expect(offset + length <= byteArray.length);

    _randomAccessFile.readFully(byteArray, offset, length);
  }

  /**
   * @see java.io.RandomAccessFile#skipBytes(int)
   * @author Michael Martinez-Schiferl
   */
  public int skipBytes(int numberOfBytes) throws IOException
  {
    Assert.expect(numberOfBytes >= 0);
    return _randomAccessFile.skipBytes(numberOfBytes);
  }

  /**
   * @see       java.io.RandomAccessFile#write(int)
   * @author    Michael Martinez-Schiferl
   */
  public void write(int aByte) throws IOException
  {
    _randomAccessFile.write(aByte);
  }

  /**
   * @see       java.io.RandomAccessFile#write(byte[])
   * @author    Michael Martinez-Schiferl
   */
  public void write(byte[] byteArray) throws IOException
  {
    Assert.expect(byteArray != null);

    _randomAccessFile.write(byteArray);
  }

  /**
   * @see       java.io.RandomAccessFile#write(byte[],int,int)
   * @author    Michael Martinez-Schiferl
   */
  public void write(byte[] byteArray, int offset, int length) throws IOException
  {
    Assert.expect(byteArray != null);
    Assert.expect(offset >= 0);
    Assert.expect(length >= 0);
    Assert.expect(offset + length < byteArray.length);

    _randomAccessFile.write(byteArray, offset, length);
  }

  /**
   * @see       java.io.RandomAccessFile#getFilePointer()
   * @author    Michael Martinez-Schiferl
   */
  public long getFilePointer() throws IOException
  {
    return _randomAccessFile.getFilePointer();
  }

  /**
   * @see       java.io.RandomAccessFile#seek(long)
   * @author    Michael Martinez-Schiferl
   */
  public void seek(long position) throws IOException
  {
    Assert.expect(position >= 0);

    _randomAccessFile.seek(position);
  }

  /**
   * @see       java.io.RandomAccessFile#length()
   * @author    Michael Martinez-Schiferl
   */
  public long length() throws IOException
  {
    return _randomAccessFile.length();
  }

  /**
   * @see       java.io.RandomAccessFile#setLength(long)
   * @author    Michael Martinez-Schiferl
   */
  public void setLength(long newLength) throws IOException
  {
    Assert.expect(newLength >= 0);

    _randomAccessFile.setLength(newLength);
  }

  /**
   * @see       java.io.RandomAccessFile#close()
   * @author    Michael Martinez-Schiferl
   */
  public void close() throws IOException
  {
    _randomAccessFile.close();
  }

  /**
   * @see       java.io.RandomAccessFile#readBoolean()
   * @author    Michael Martinez-Schiferl
   */
  public boolean readBoolean() throws IOException
  {
    return _randomAccessFile.readBoolean();
  }

  /**
   * @see       java.io.RandomAccessFile#readByte()
   * @author    Michael Martinez-Schiferl
   */
  public byte readByte() throws IOException
  {
    return _randomAccessFile.readByte();
  }

  /**
   * @see       java.io.RandomAccessFile#readUnsignedByte()
   * @author    Michael Martinez-Schiferl
   */
  public int readUnsignedByte() throws IOException
  {
    return _randomAccessFile.readByte();
  }

  /**
   * Reads an unsigned character from the current position of the byte array.
   *
   * @return the next available byte from the byte array as an unsigned character
   * @author Bill Darbie
   */
  public short readUnsignedChar() throws IOException
  {
    return (short)(0x00FF & readUnsignedByte());
  }

  /**
   * @author    Michael Martinez-Schiferl
   */
  public abstract short readShort() throws IOException;

  /**
   * @author    Michael Martinez-Schiferl
   */
  public abstract int readUnsignedShort() throws IOException;

  /**
   * @author    Michael Martinez-Schiferl
   */
  public abstract int readInt() throws IOException;

  /**
   * @author    Michael Martinez-Schiferl
   */
  public char readChar() throws IOException
  {
    Assert.expect(false, "Method readChar() not implemented, C/C++ do not support 2 byte chars.");

    return '0';
  }

  /**
   * Reads an unsigned integer from the current position of the byte array.
   *
   * @return the next 4 bytes from the byte array as an unsigned int
   * @author Bill Darbie
   */
  public abstract long readUnsignedInt() throws IOException;

  /**
   * Reads a four byte C long and places it in an eight byte Java long
   *
   * @author    Michael Martinez-Schiferl
   */
  public long readLong() throws IOException
  {
    return ((long)readInt());
  }

  /**
   * Reads a 4 byte float
   *
   * @author    Michael Martinez-Schiferl
   */
  public float readFloat() throws IOException
  {
    return Float.intBitsToFloat(readInt());
  }

  /**
   * @author    Michael Martinez-Schiferl
   */
  public abstract double readDouble() throws IOException;

  /**
   * @deprecated
   * @author Bill Darbie
   */
  public String readLine()
  {
    Assert.expect(false, "CDataRandomAccessFile.readLine is deprecated");
    return null;
  }

  /**
   * reads a string comprised of single byte chars
   *
   * @author    Michael Martinez-Schiferl
   */
  public String readString(int length) throws IOException
  {
    Assert.expect(length >= 0);
    return readString(length, length);
  }

  /*
   * Read a number of characters from the current position.  The last charater must
   * be '\0'.  The '\0' charater is not returned as part of the string read in.
   *
   * @param length the number of characters to read from the byte array.
   * @param limit the number of characters to put into the string.
   *
   * @return the string read in.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public String readString(int length, int limit) throws IOException, EOFException
  {
    Assert.expect(length >= 0);
    Assert.expect(limit >= 0);
    Assert.expect(limit <= length);

    byte bytes[] = new byte[length];
    for(int i = 0; i < bytes.length; ++i)
    {
      bytes[i] = ((byte)read());
      if (bytes[i] < 0)
        throw new EOFException("CDataRandomAccessFile.readString: EOF reached");
    }

    if (length > 0)
      Assert.expect(bytes[length - 1] == '\0');

    // get rid of the '\0' before creating the string
    if ((limit == length) && (limit > 0))
      limit--;

    return new String(bytes, 0, limit).intern();    //convert to a string
  }

  /**
   * Read a number of characters from the current position and return it as a string.
   * The character string doesn't have to be null terminated.
   *
   * @param length the number of characters to read.
   * @return the String created from the characters.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public String readCharArrayString(int length) throws IOException
  {
    return readCharArrayString(length, length);
  }

  /**
   * Read a number of characters from the current position.  The character string doesn't
   * have to be null terminated.
   *
   * @param length the number of characters to read.
   * @param limit the number of characters to put in the String.
   * @return the String created from the characters.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */

  public String readCharArrayString(int length, int limit) throws IOException
  {
    Assert.expect(length >= 0);
    Assert.expect(limit >= 0);
    Assert.expect(limit >= length);

    byte bytes[] = new byte[length];
    for(int i=0; i<bytes.length; i++)
    {
      bytes[i] = ((byte)read());
      if (bytes[i] < 0)
        throw new EOFException("CDataRandomAccessFile.readString: EOF reached");
    }

    return new String(bytes, 0, limit).intern();    //convert to a string
  }

  /**
   * Read a Pascal style string of characters from the current position.  The total
   * number of bytes to read is passed in through the length prarameter.  This
   * includes the first byte which contains the number of valid characters in
   * the string.  The value in the first byte determines how many of the
   * characters are put into the String to be returned.
   *
   * @param length the maximum number of characters to read
   * @return the String created from the characters.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public String readPascalString(int length) throws IOException, EOFException
  {
    Assert.expect(length >= 0);

    byte[] characters = new byte[length];
    for(int i=0; i < length; i++)
    {
      characters[i] = ((byte)read());
      if (characters[i] < 0)
        throw new EOFException("readString: EOF reached");
    }
    int strLength = (int)characters[0];

    return new String(characters, 1, strLength).intern();
  }

  /**
   * Read in a Pascal style string from the current position.
   *
   * @return the String created from pascal string read
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public String readPascalString() throws IOException
  {
    int strLength = (int)read();
    if (strLength < 0)
        throw new EOFException("readString: EOF reached");

    byte[] characters = new byte[strLength];
    for(int i = 0; i < strLength; ++i)
    {
      characters[i] = ((byte)read());
      if (characters[i] < 0)
        throw new EOFException("readString: EOF reached");
    }

    return new String(characters, 0, strLength).intern();
  }

  /**
   * Read a number of characters from the current position.  The character string doesn't
   * have to be null terminated.
   *
   * @param length the number of characters to read.
   * @return the String created from the characters.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public String readPascalPackedArray(int length) throws IOException
  {
    return readCharArrayString(length);
  }

  /**
   * Read a number of characters from the current position.  The character string doesn't
   * have to be null terminated.
   *
   * @param length the number of characters to read.
   * @param limit the number of characters to put in the String that will be returned.
   * @return the String created from the characters.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public String readPascalPackedArray(int length, int limit) throws IOException
  {
    return readCharArrayString(length,limit);
  }

  /**
   * @author Michael Martinez-Schiferl
   */
  public String readUTF() throws IOException
  {
    Assert.expect(false, "Method writeUTF() not implemented, C/C++ do not support UTF.");
    return null;
  }

  /**
   * @see       java.io.RandomAccessFile#writeBoolean(boolean)
   * @author    Michael Martinez-Schiferl
   */
  public void writeBoolean(boolean aBoolean) throws IOException
  {
    _randomAccessFile.writeBoolean(aBoolean);
  }

  /**
   * @see       java.io.RandomAccessFile#writeByte(int)
   * @author    Michael Martinez-Schiferl
   */
  public void writeByte(int aByte) throws IOException
  {
    _randomAccessFile.writeByte(aByte);
  }

  /**
   * Writes a short (two bytes) to the byte array passed in through this classes contructor
   * The byte values to be written, in the  order
   * shown, are for big endian: <p>
   * <pre><code>
   * (byte)(0xff &amp; (value &gt;&gt; 8))
   * (byte)(0xff &amp; value)
   * </code> </pre> <p>
   * for little endian:
   * <pre><code>
   * (byte)(0xff &amp; value)
   * (byte)(0xff &amp; (value &gt;&gt; 8))
   * </code> </pre> <p>
   *
   * @param aShort the <code>short</code> value to be written to the byte array passed in through this
   *               classes contructor
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public abstract void writeShort(int aShort) throws IOException;

  /**
   * @author    Michael Martinez-Schiferl
   */
  public void writeChar(int aChar) throws IOException
  {
    Assert.expect(false, "Method writeChar() not implemented, C/C++ do not support 2 byte chars.");
  }

  /**
   * Writes an integer value (4 bytes) to the byte array passed in through this classes contructor.
   * The byte values to be written, in the  order
   * shown, are for big endian:
   * <p><pre><code>
   * (byte)(0xff &amp; (value &gt;&gt; 24))
   * (byte)(0xff &amp; (value &gt;&gt; 16))
   * (byte)(0xff &amp; (value &gt;&gt; &#32; &#32;8))
   * (byte)(0xff &amp; value)
   * </code></pre><p>
   * for little endian:
   * <p><pre><code>
   * (byte)(0xff &amp; (value &gt;&gt; 24))
   * (byte)(0xff &amp; (value &gt;&gt; 16))
   * (byte)(0xff &amp; (value &gt;&gt; &#32; &#32;8))
   * (byte)(0xff &amp; value)
   * </code></pre><p>
   *
   * @param anInt the <code>int</code> value to be written.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public abstract void writeInt(int anInt) throws IOException;

  /**
   * Writes a long value (4 bytes) to the byte array passed in to this classes contructor.
   * The byte values to be written, in the  order
   * shown, are:
   * <p><pre><code>
   * (byte)(0xff &amp; (v &gt;&gt; 24))
   * (byte)(0xff &amp; (v &gt;&gt; 16))
   * (byte)(0xff &amp; (v &gt;&gt;  8))
   * (byte)(0xff &amp; v)
   * </code></pre><p>
   * The upper four bytes of the Java 8 byte long are discarded.
   *
   * @param aLong the <code>long</code> value to be written to the byte array passed in to
   *              this classes contructor.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public abstract void writeLong(long aLong) throws IOException;

  /**
   * Writes a float (4 bytes) to the byte array passed into this classes contructor.
   * It does this as if it first converts this
   * float value to an int in exactly the manner of the <code>Float.floatToIntBits</code>
   * method  and then writes the <code>int</code>
   * value in exactly the manner of the  <code>writeInt</code>
   * method.
   *
   * @param aFloat the <code>float</code> value to be written to the byte array passed in to
   *                this classes contructor.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writeFloat(float aFloat) throws IOException
  {
    writeInt(Float.floatToIntBits(aFloat));
  }

  /**
   * Writes a double value (8 bytes)to the byte array passed in to this classes contructor.
   * It does this as if it first converts this
   * <code>double</code> value to a <code>long</code>
   * in exactly the manner of the <code>Double.doubleToLongBits</code>
   * method  and then writes the <code>long</code>
   * value in exactly the manner of the  <code>writeLong</code>
   * method.
   *
   * @param aDouble the <code>double</code> value to be written to the byte array passed into
   *                this classes contructor
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public abstract void writeDouble(double aDouble) throws IOException;

  /**
   * Writes a string to the byte array passed into this classes constructor.
   * For every character in the string
   * <code>aString</code>,  taken in order, one byte
   * is written to byte array passed into this classes contructor.
   *
   * @param aString the string of bytes to be written.
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writeBytes(String aString) throws IOException
  {
    Assert.expect(aString != null);

    _randomAccessFile.writeBytes(aString);
  }

  /**
   * @author    Michael Martinez-Schiferl
   */
  public void writeChars(String aString) throws IOException
  {
    Assert.expect(false, "Method writeChars() not implemented, C/C++ do not support 2 byte chars.");
  }

  /**
   * @author    Michael Martinez-Schiferl
   */
  public void writeUTF(String aString) throws IOException
  {
    Assert.expect(false, "Method writeUTF() not implemented, C/C++ do not support UTF.");
  }

  /**
   * Write a Pascal style string of characters to the byte array passed into this classes constructor.
   * The value in the first byte determines how many of the characters are put
   * into the String to be returned.
   *
   * @param aString is the string to be written
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writePascalString(String aString) throws IOException
  {
    Assert.expect(aString != null);

    int length = aString.length();
    byte[] bytes = aString.getBytes();
    writeByte((byte)(0x000000FF & length));
    write(bytes);
  }

  /**
   * Write a Pascal style packed array of characters to the byte array passed into this classes constructor.
   *
   * @param aString is the string to be written
   * @author Michael Martinez-Schiferl
   * @author Bill Darbie
   */
  public void writePascalPackedArray(String aString) throws IOException
  {
    Assert.expect(aString != null);

    byte[] bytes = aString.getBytes();
    write(bytes);
  }
}

