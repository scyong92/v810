package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * This is the base class for all enumeration classes.
 * This class provides the minimal set of functions needed to make
 * any enumeration class work properly.  It provides an equals()
 * and hashCode() method.  These methods are needed to make any
 * enumeration work properly that uses Java serialization.
 *
 * @author Bill Darbie
 */
public class Enum implements Serializable, Comparable
{
  private final int _id;
  private final int _hashCode;
  private static Map<Class, Map<Integer, Enum>> _enumClassToIdToInstanceMapMap = new HashMap<Class, Map<Integer, Enum>>();

  /**
   * @author Bill Darbie
   * @author Rex Shang
   */
  protected Enum(int id)
  {
    _id = id;
    // Calculate the hashcode once for faster retrival.
    _hashCode = 37 * getClass().hashCode() + id;

    Map<Integer, Enum> instanceMap = _enumClassToIdToInstanceMapMap.get(getClass());
    if (instanceMap == null)
    {
      // Use tree map so the order of values is preserved.
      instanceMap = new TreeMap<Integer, Enum>();
      _enumClassToIdToInstanceMapMap.put(this.getClass(), instanceMap);
    }
    Object my = instanceMap.put(new Integer(id), this);
    Assert.expect(my == null, "The same id cannot be used more than once.  ID " + id + " is already in use.");
  }

  /**
   * @author Bill Darbie
   */
  public int hashCode()
  {
    return _hashCode;
  }

  /**
   * @author Bill Darbie
   */
  public boolean equals(Object rhs)
  {
    if (rhs == null)
      return false;

    if (rhs == this)
      return true;

    // normally in an equals you would do the following line:
    // if (rhs instanceof Enum)
    // but in this case we really want to make sure that we have
    // the exact same Enum class.  If AEnum and BEnum both extend
    // Enum and both have Enums with an _id of 1 we want the equals
    // method to return false.  The getClass() call will cause
    // the right behavior in this case, but the instanceof would not
    if (getClass() == rhs.getClass())
    {
      Enum enumeration = (Enum)rhs;
      if (_id == enumeration._id)
        return true;
    }

    return false;
  }

  /**
   * @author Patrick Lacz
   */
  public int compareTo(Object rhs)
  {
    Assert.expect(getClass() == rhs.getClass());

    Enum enumeration = (Enum) rhs;
    if (_id > enumeration._id)
      return 1;

    if (_id < enumeration._id)
      return -1;

    return 0;
  }

  /**
   * @author Bill Darbie
   */
  public int getId()
  {
    return _id;
  }

  /**
   * @author Bill Darbie
   */
  public static int getNumEnums(Class clazz)
  {
    Assert.expect(clazz != null);
    Map instanceMap = _enumClassToIdToInstanceMapMap.get(clazz);
    Assert.expect(instanceMap != null);
    return instanceMap.size();
  }

  /**
   * Help deserialization to return the Object we've already created so "=="
   * could be used.
   * NOTE: Do not change the scope of this method without extensive testing.
   * We have to make sure all derived class can use it.
   * @author Rex Shang
   */
  protected Object readResolve() throws ObjectStreamException
  {
    Map instanceMap = _enumClassToIdToInstanceMapMap.get(getClass());
    Assert.expect(instanceMap != null);

    Enum myInstance = (Enum) instanceMap.get(new Integer(_id));
    Assert.expect(myInstance != null);

    return myInstance;
  }

  /**
   * @author Rex Shang
   */
  protected static Map<Integer, Enum> getIdToEnumMap(Class clazz)
  {
    Assert.expect(clazz != null);

    Map<Integer, Enum> instanceMap = _enumClassToIdToInstanceMapMap.get(clazz);
    Assert.expect(instanceMap != null);

    // Prevent modification by subclass.
    return Collections.unmodifiableMap(instanceMap);
  }

}
