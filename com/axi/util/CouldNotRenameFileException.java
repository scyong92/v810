package com.axi.util;

import java.io.*;

/**
 * @author Bill Darbie
 */
public class CouldNotRenameFileException extends IOException
{
  private String _fromFileName;
  private String _toFileName;

  /**
   * @author Bill Darbie
   */
  public CouldNotRenameFileException(String fromFileName, String toFileName)
  {
    super("ERROR: Could not rename file " + fromFileName + " to " + toFileName);
    Assert.expect(fromFileName != null);
    Assert.expect(toFileName != null);
    _fromFileName = fromFileName;
    _toFileName = toFileName;
  }

  /**
   * @author Bill Darbie
   */
  public String getFromFileName()
  {
    return _fromFileName;
  }

  /**
   * @author Bill Darbie
   */
  public String getToFileName()
  {
    return _toFileName;
  }
}
