package com.axi.util;

import java.awt.*;
import java.awt.font.*;
import java.awt.print.*;
import java.text.*;
import java.util.*;

import javax.swing.*;

/**
 * This is a general-purpose utility for printing to the printer.
 *
 * @author Jeff Ryer
 */
public class PrinterUtil implements Printable
{
  private static final String _NEW_LINE_DELIMITER = "\n";
  private static Hashtable<TextAttribute, Font> _textAttributeToFontMap = new Hashtable<TextAttribute, Font>();
  private static final int _CONTENT_TYPE_DEFAULT = 0;
  private static final int _CONTENT_TYPE_STRING = 1;
  private static final int _CONTENT_TYPE_COMPONENT = 2;

  private static String _stringToBePrinted;
  private static float _drawPosY;
  private static float _lineSpacing;
  private static Component _componentToBePrinted;
  private static int _contentType = _CONTENT_TYPE_DEFAULT;

  /**
   * @return the string used for separating paragraphs when printing text
   * @author Jeff Ryer
   */
  public static String getNewLine()
  {
    return _NEW_LINE_DELIMITER;
  }

  /**
   * This is the method that will be called to do the printing of text.
   * It prints to the default printer without a dialog prompt.
   * Embed the new line string--getNewLine()--within the string to be printed
   * to start a new paragraph.  New lines at the top of the page are not
   * handled--only after the first paragraph.  Tabs are not handled.
   * Printing is limited to one page.
   *
   * @param stringToBePrinted the text to be printed
   * @author Jeff Ryer
   */
  public static void printText(String stringToBePrinted, Font font)
  {
    Assert.expect(stringToBePrinted != null, "PrinterUtil.printText() -> the string you want to print is null");
    Assert.expect(font != null, "PrinterUtil.printText() - the font is null");
    _stringToBePrinted = stringToBePrinted;
    _contentType = _CONTENT_TYPE_STRING;
    _textAttributeToFontMap.put(TextAttribute.FONT, font);
    PrinterJob printJob = PrinterJob.getPrinterJob();
    printJob.setPrintable(new PrinterUtil());

    try
    {
      printJob.print();
    }
    catch(PrinterException pe)
    {
      pe.printStackTrace();
    }
  }

  /**
   * This is the method that will be called to do the printing for a particular component.
   * The standard print options dialog is displayed prior to printing.
   * @param componentToBePrinted the swing component to be printed
   * @author Erica Wheatcroft, Jeff Ryer
   */
  public static void printComponent(Component componentToBePrinted) throws PrinterException
  {
    Assert.expect(componentToBePrinted != null, "PrintUtilities.printComponent() -> the component you want to print is null");
    _componentToBePrinted = componentToBePrinted;
    _contentType = _CONTENT_TYPE_COMPONENT;
    PrinterJob printJob = PrinterJob.getPrinterJob();
    printJob.setPrintable(new PrinterUtil());

    // show the standard print options dialog.
    if (printJob.printDialog())
    {
      try
      {
        printJob.print();
      }
      catch(PrinterException pe)
      {
        throw pe;
      }
    }
  }

  /**
   * This method has two steps to it. First is to decide what to do for different pages of your print job,
   * since Java repeatedly calls print with higher and higher page indexes until print returns NO_SUCH_PAGE.
   * It returns PAGE_EXISTS for index 0, NO_SUCH_PAGE otherwise. The second step is to start drawing.
   *
   * @param graphics the graphic to print with
   * @param pageFormat the format of the page
   * @param pageIndex the index of the page starting at 0
   * @return whether the page exists or does not exist.
   * @author Jeff Ryer
   */
  public int print(Graphics graphics, PageFormat pageFormat, int pageIndex)
  {
    Assert.expect(graphics != null, "PrinterUtil.print() - Graphics is null");
    Assert.expect(pageFormat != null, "PrinterUtil.print() - PageFormat is null");
    Assert.expect(pageIndex >= -1, "PrinterUtil.print() - PageIndex is negative");
    Assert.expect(_contentType != _CONTENT_TYPE_DEFAULT, "PrinterUtil.print() - _contentType must be set before calling print()");

    if (pageIndex > 0)
      return(NO_SUCH_PAGE);

    Graphics2D graphics2d = (Graphics2D)graphics;
    graphics2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

    if (_contentType == _CONTENT_TYPE_STRING)
    {
      _drawPosY = 0;

      StringTokenizer tokenizedStringToBePrinted = new StringTokenizer(_stringToBePrinted, _NEW_LINE_DELIMITER, true);

      while (tokenizedStringToBePrinted.hasMoreTokens())
      {
        String token = tokenizedStringToBePrinted.nextToken();
        if (token.equals(_NEW_LINE_DELIMITER))
        {
          _drawPosY += _lineSpacing; // add a new line
        }
        else
        {
          AttributedString attributedString = new AttributedString(token,_textAttributeToFontMap);
          drawParagraph(graphics2d, pageFormat, attributedString.getIterator());
        }
      }
    }
    else if (_contentType == _CONTENT_TYPE_COMPONENT)
    {
      // In the specific case of printing Swing components, your drawing should just be a high-resolution version of
      // what the component looks like on the screen. So you cast the Graphics object to Graphics2D, scale the resolution
      // to the printer, and call the component's paint method with this scaled Graphics2D.
      disableDoubleBuffering(_componentToBePrinted);
      _componentToBePrinted.paint(graphics2d);
      enableDoubleBuffering(_componentToBePrinted);
    }

    return PAGE_EXISTS;
  }

  /**
   * This method handles drawing one paragraph, properly breaking up the lines.
   *
   * @param graphics2d the graphic to print with
   * @param pageFormat the format of the page
   * @param paragraph the paragraph to be drawn
   * @author Jeff Ryer
   */
  private void drawParagraph(Graphics2D graphics2d, PageFormat pageFormat, AttributedCharacterIterator paragraph)
  {
    FontRenderContext frc = graphics2d.getFontRenderContext();
    LineBreakMeasurer lineMeasurer = new LineBreakMeasurer(paragraph, frc);

    lineMeasurer.setPosition(paragraph.getBeginIndex());

    // Get lines from lineMeasurer until the entire
    // paragraph has been displayed.
    while (lineMeasurer.getPosition() < paragraph.getEndIndex()) {

        // Retrieve next layout.
        TextLayout layout = lineMeasurer.nextLayout((float)pageFormat.getImageableWidth());

        // Move y-coordinate by the ascent of the layout.
        _drawPosY += layout.getAscent();

        // Draw the TextLayout at (0, drawPosY).
        layout.draw(graphics2d, 0, _drawPosY);

        // Move y-coordinate in preparation for next layout.
        _drawPosY += layout.getDescent() + layout.getLeading();

        _lineSpacing = layout.getAscent() + layout.getDescent() + layout.getLeading(); // used for creating new lines
    }
    _drawPosY -= _lineSpacing; // move y-coordinate back up for next token
  }

  /**
   * This method will disable double buffering for the component specified. This is needed because with Java Swing,
   * almost all components have double buffering turned on by default. In general double buffering is good for making
   * for convenient and efficient paintComponent method. However, in the specific case of printing, it is a huge problem.
   * First, since printing components relies on scaling the coordinate system and then simply calling the component's
   * paint method, if double buffering is enabled printing amounts to little more than scaling up the buffer
   * (off-screen image). This results in ugly low-resolution printing like you already had available. Secondly, sending
   * these huge buffers to the printer results in huge print spooler files which take a very long time to print. Consequently,
   * you need to make sure double buffering is turned off before you print. If you have only a single JPanel or other
   * JComponent, you can call setDoubleBuffered(false) on it before calling the paint method, and setDoubleBuffered(true)
   * afterwards. However, this suffers from the flaw that if you later nest another container inside, you're right back
   * where you started from. So instead I used RepaingManager to disable / enable double buffering for all components.
   *
   * @param component the component to disable double buffering on.
   * @author Erica Wheatcroft
   */
  private void disableDoubleBuffering(Component component)
  {
    Assert.expect(component != null, "ComponentPrintingUtilities.disableDoubleBuffering() - Component is null");

    RepaintManager currentManager = RepaintManager.currentManager(component);
    currentManager.setDoubleBufferingEnabled(false);
  }

  /**
   * @param component the component to enable double buffering on.
   * @author Erica Wheatcroft
   */
  private void enableDoubleBuffering(Component component)
  {
    Assert.expect(component != null, "ComponentPrintingUtilities.enableDoubleBuffering() - Component is null");

    RepaintManager currentManager = RepaintManager.currentManager(component);
    currentManager.setDoubleBufferingEnabled(true);
  }
}
