package com.axi.util;

import java.io.*;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class Test_CommandManager extends UnitTest
{
  private CommandManager _commandManager = new CommandManager();

  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_CommandManager());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      Expect.expect(_commandManager.isUndoAvailable() == false);
      try
      {
        _commandManager.undo();
        Assert.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }

      Expect.expect(_commandManager.isRedoAvailable() == false);
      try
      {
        _commandManager.redo();
        Assert.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }

      // test 1
      StringBuffer buffer = new StringBuffer();
      _commandManager.setUndoAndRedoCommandsGoOnCommandList(true);
      _commandManager.clear();
      System.out.println("test 1");
      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyCommand(1, true));
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyCommand(2, true));
      System.out.println("CMD: execute 3:");
      _commandManager.execute(new MyCommand(3, true));
      System.out.println("CMD: execute 4 with return of false:");
      _commandManager.execute(new MyCommand(4, false));

      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer) == false);

      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      _commandManager.redo();
      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      _commandManager.redo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();

      // test 2
      _commandManager.clear();
      System.out.println();
      System.out.println("test 2");
      Expect.expect(_commandManager.isUndoAvailable(buffer) == false);
      Expect.expect(_commandManager.isRedoAvailable(buffer) == false);

      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyCommand(1, true));
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyCommand(2, true));

      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable() == false);
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      _commandManager.undo();
      System.out.println("CMD: execute 3:");
      _commandManager.execute(new MyCommand(3, true));
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer) == false);

      // test 3
      _commandManager.clear();
      System.out.println();
      System.out.println("test 3");
      Expect.expect(_commandManager.isUndoAvailable() == false);
      Expect.expect(_commandManager.isRedoAvailable() == false);
      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyCommand(1, true));
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable() == false);
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyCommand(2, true));
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      _commandManager.redo();
      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      _commandManager.redo();
      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      _commandManager.redo();

      // test 4
      // now try block commands
      _commandManager.clear();
      System.out.println();
      System.out.println("test 4");
      _commandManager.beginCommandBlock("command block 1");
      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyCommand(1, true));
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyCommand(2, true));
      _commandManager.endCommandBlock();

      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable() == false);
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();

      Expect.expect(_commandManager.isUndoAvailable() == false);
      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      _commandManager.redo();


      // now try the other mode
      // test 5
      try
      {
        _commandManager.setUndoAndRedoCommandsGoOnCommandList(false);
        Expect.expect(false);
      }
      catch (AssertException ae)
      {
        // do nothing
      }
      _commandManager.clear();
      _commandManager.setUndoAndRedoCommandsGoOnCommandList(false);
      System.out.println("test 5");
      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyCommand(1, true));
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyCommand(2, true));
      System.out.println("CMD: execute 3:");
      _commandManager.execute(new MyCommand(3, true));
      System.out.println("CMD: execute 4 with return of false: ");
      _commandManager.execute(new MyCommand(4, false));

      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer) == false);

      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      _commandManager.redo();
      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      _commandManager.redo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();

      // test 6
      _commandManager.clear();
      System.out.println();
      System.out.println("test 6");
      Expect.expect(_commandManager.isUndoAvailable(buffer) == false);
      Expect.expect(_commandManager.isRedoAvailable(buffer) == false);

      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyCommand(1, true));
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyCommand(2, true));
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer) == false);
      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      _commandManager.redo();
      Expect.expect(_commandManager.isUndoAvailable());
      Expect.expect(_commandManager.isRedoAvailable());
      System.out.println("CMD: execute 3:");
      _commandManager.execute(new MyCommand(3, true));
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer) == false);
      Expect.expect(_commandManager.isRedoAvailable(buffer));

      // test 7
      _commandManager.clear();
      System.out.println();
      System.out.println("test 7");
      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyCommand(1, true));
      Expect.expect(_commandManager.isRedoAvailable() == false);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable() == false);
      Expect.expect(_commandManager.isRedoAvailable());
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyCommand(2, true));
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable() == false);
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable() == false);
      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      _commandManager.redo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable(buffer) == false);

      // test 8
      // now try block commands
      _commandManager.clear();
      System.out.println();
      System.out.println("test 8");
      _commandManager.beginCommandBlock("command block 1");
      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyCommand(1, true));
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyCommand(2, true));
      _commandManager.endCommandBlock();

      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable() == false);

      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();

      Expect.expect(_commandManager.isUndoAvailable() == false);
      Expect.expect(_commandManager.isRedoAvailable());
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}

/**
 * @author Bill Darbie
 */
class MyCommand extends Command
{
  private int _commandNumber;
  private boolean _stateChange;

  /**
   * @author Bill Darbie
   */
  MyCommand(int commandNumber, boolean stateChange)
  {
    _commandNumber = commandNumber;
    _stateChange = stateChange;
  }

  /**
   * @author Bill Darbie
   */
  public boolean execute()
  {
    if (_stateChange)
      System.out.println("  executing command " + _commandNumber);

    return _stateChange;
  }

  /**
   * @author Bill Darbie
   */
  public void undo()
  {
    System.out.println("  undo command " + _commandNumber);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    return Integer.toString(_commandNumber);
  }
}
