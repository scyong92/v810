package com.axi.util;

import java.lang.*;

/**
 * A socket client will throw this exception whenever a socket server cannot complete the client's request.
 *
 * @author Eugene Kim-Leighton
 */
public class SocketMessageFailedException extends Exception
{
  public SocketMessageFailedException(String errorMessage)
  {
    super(errorMessage);
    Assert.expect(errorMessage!=null);
  }
}