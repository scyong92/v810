package com.axi.util;

import java.io.*;

/**
 * This data input class provides an interface to read in binary
 * data from a file that was created by a 32 bit C or C++ application.<p>
 * The class is like the DataInputStream.  Changes are made to cope with the
 * big endian, little endian differences and data sizes.
 * This class is used with a InputStream.
 * Checks for a null stream are not done so a runtime error is thrown if access
 * is attempted from a closed stream.
 *
 * PE:  You should probably use java.nio.ByteBuffers instead . . .
 *
 * @author Steve Anonson
 *
 * @see LittleEndianCDataInputStream
 * @see BigEndianCDataInputStream
 */
public abstract class CDataInputStream extends FilterInputStream implements DataInput
{
  /**
   * Saves the InputStream from which the bytes are read.
   *
   * @param   is the InputStream to be read.
   * @author Steve Anonson
   */
  public CDataInputStream(InputStream is)
  {
    super(is);
    Assert.expect(in != null);
  }

  /**
   * Skips over a number of bytes from the input stream.
   *
   * @param numBytes the number of bytes to skip.
   * @return the number of bytes skipped.
   * @author Steve Anonson
   */
  public int skipBytes(int numBytes) throws IOException
  {
    int total = 0;
    int cur = 0;
    while ((total < numBytes) && ((cur = (int) in.skip(numBytes - total)) > 0))
    {
      total += cur;
    }
    return total;
  }

  /**
   * Reads an array of bytes from the input stream.
   *
   * @param byteArray the array to put the bytes into.
   * @exception EOFException if end of input stream before reading all the bytes.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public void readFully(byte byteArray[]) throws EOFException, IOException
  {
    readFully(byteArray, 0, byteArray.length);
  }

  /**
   * Reads an array of bytes from the input stream.
   *
   * @param byteArray the array to put the bytes into.
   * @param offset the position in the array to start entering bytes.
   * @param length the number of bytes to read.
   * @exception EOFException if end of input stream before reading all the bytes.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public void readFully(byte byteArray[], int offset, int length) throws EOFException, IOException
  {
    Assert.expect(byteArray != null);
    Assert.expect(offset >= 0);
    Assert.expect(length >= 0);
    Assert.expect(offset + length <= byteArray.length);

    int n = 0;
    while (n < length)
    {
        int count = in.read(byteArray, offset + n, length - n);
        if (count < 0)
          throw new EOFException();
        n += count;
    }
  }

  /**
   * Reads a boolean type from the input stream.  Assumes that a C++ bool type
   * is 1 byte.
   *
   * @return true if the byte is not zero.
   * @exception EOFException if at the end of input stream.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public boolean readBoolean() throws EOFException, IOException
  {
    int ch = read();  // read a byte from the stream
    if (ch < 0)
      throw new EOFException();
    return (ch != 0);
  }

  /**
   * Reads one byte from the input stream.
   *
   * @return the next byte from the input stream.
   * @exception EOFException if at the end of input stream.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public byte readByte() throws EOFException, IOException
  {
    int ch = read();  // read a byte from the stream
    if (ch < 0)
      throw new EOFException();
    return (byte)(ch);
  }

  /**
   * Reads unsigned byte from the input file.
   *
   * @return a non-negative value of the byte.
   * @exception EOFException  if at the end of input stream.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public int readUnsignedByte() throws EOFException, IOException
  {
    int ch = read();  // read a byte from the stream
    if (ch < 0)
      throw new EOFException();
    return ch;
  }

  /**
   * Reads a char type from the input file.  Assumes that a C++ char
   * is 1 byte.
   *
   * @return value of the byte.
   * @exception EOFException  if at the end of input stream.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public char readChar() throws EOFException, IOException
  {
    int ch = read();  // read a byte from the stream
    if (ch < 0)
      throw new EOFException();
    return (char)ch;
  }

  /**
   * Reads a unsigned char type from the input file.  Assumes that a C++ char
   * is 1 byte.
   *
   * @return non-negative value of the byte.
   * @exception EOFException if at the end of input stream.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public short readUnsignedChar() throws EOFException, IOException
  {
    return (short)readUnsignedByte();
  }

  /**
   * Reads a short type from the input file.  Assumes that a C++ short
   * is 2 bytes.  Least Significant Byte is first.
   *
   * @return the value of the bytes.
   * @exception EOFException if at the end of input stream before reading 2 bytes.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public abstract short readShort() throws EOFException, IOException;

  /**
   * Reads a unsigned short type from the input file.  Assumes that a C++ short
   * is 2 bytes.  Least Significant Byte is first.
   *
   * @return non-negative value of the bytes.
   * @exception EOFException if at the end of input stream before reading 2 bytes.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public abstract int readUnsignedShort() throws EOFException, IOException;

  /**
   * Reads a int type from the input file.  Assumes that a C++ int
   * is 4 bytes.  Least Significant Byte is first.
   *
   * @return  value of the bytes.
   * @exception EOFException if at the end of input stream before reading 4 bytes.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public abstract int readInt() throws EOFException, IOException;

  /**
   * Reads a long type from the input file.  Assumes that a C++ long
   * is 4 bytes.  Least Significant Byte is first.
   *
   * @return value of the bytes.
   * @exception EOFException - if at the end of input stream before reading 4 bytes.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public long readLong() throws EOFException, IOException
  {
    return (long)readInt();
  }

  /**
   * Reads a float type from the input file.  Assumes that a C++ float
   * is 4 bytes.  Least Significant Byte is first.
   *
   * @return float - value of the bytes.
   * @exception EOFException if at the end of input stream before reading four bytes.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public float readFloat() throws EOFException, IOException
  {
    return Float.intBitsToFloat(readInt());
  }

  /**
   * Reads a double type from the input file.  Assumes that a C++ double
   * is 8 bytes.  Least Significant Byte is first.
   *
   * @return value of the bytes.
   * @exception EOFException  if at the end of input stream before reading eight bytes.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public abstract double readDouble() throws EOFException, IOException;

  /**
   * Read a number of characters from the input stream.
   *
   * @param length the number of characters to read.
   * @return  the String created from the characters.
   * @exception EOFException if at the end of input stream before reading 'len' bytes.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public String readString(int length) throws EOFException, IOException
  {
    return readString(length, length);
  }

  /**
   * Read a number of characters from the input stream.  The last charater must
   * be '\0'.  The null charater is not returned.
   *
   * @param length the number of characters to read.
   * @param limit the number of valid characters to put in the String.
   * @return String created from the characters.
   * @exception EOFException  if at the end of input stream before reading 'len' bytes.
   * @exception IOException if the limit parameter is greater than the number
   * of characters to read, the read string is not null terminated or there is an IO error.
   * @author Steve Anonson
   */
  public String readString(int length, int limit) throws EOFException, IOException
  {
    Assert.expect(length >= 0);
    Assert.expect(limit >= 0);
    Assert.expect(limit <= length);

    byte[] myBytes = new byte[length];
    readFully(myBytes);  // get the bytes from the stream

    if (length > 0)
      Assert.expect(myBytes[length - 1] == '\0');

    if ((limit == length) && (limit > 0))
      limit--;

    return new String(myBytes, 0, limit).intern();  // convert to a string
  }

  /**
   * Read a number of characters from the input stream.  The character string doesn't
   * have to be null terminated.
   *
   * @param length the number of characters to read.
   * @return  the String created from the characters.
   * @exception EOFException if at the end of input stream before reading 'len' bytes.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public String readCharArrayString(int length) throws EOFException, IOException
  {
    return readCharArrayString(length, length);
  }

  /**
   * Read a number of characters from the input stream.  The character string doesn't
   * have to be null terminated.
   *
   * @param length - number of characters to read.
   * @param limit - number of valid characters to put in the String.
   * @return the String created from the characters.
   * @exception EOFException  if at the end of input stream before reading 'len' bytes.
   * @exception IOException if the limit parameter is greater than the number
   * of characters to read or there is an IO error.
   * @author Steve Anonson
   */
  public String readCharArrayString(int length, int limit) throws EOFException, IOException
  {
    Assert.expect(length >= 0);
    Assert.expect(limit >= 0);
    Assert.expect(limit <= length);

    byte[] myBytes = new byte[length];
    readFully(myBytes);  // get the bytes from the stream
    return new String(myBytes, 0, limit).intern();  // convert to a string
  }

  /**
   * Read a Pascal style string of characters from a stream.  The total number
   * of bytes to read is passed in through the len prarameter.  This includes
   * the first byte which contains the number of valid characters in the string.
   * The value in the first byte determines how many of the characters are put
   * into the String to be returned.
   *
   * @param length the- maximum number of characters to read.
   * @return a String created from the characters.
   * @exception EOFException if at the end of input stream before reading 'len' bytes.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public String readPascalString(int length) throws EOFException, IOException
  {
    Assert.expect(length >= 0);
    byte[] myBytes = new byte[length];
    readFully(myBytes);  // get the bytes from the stream
    return new String( myBytes, 1, (int)myBytes[0] ).intern();  // convert to a string
  }

  /**
   * Read in a Pascal style string.
   *
   * @author Bill Darbie
   */
  public String readPascalString() throws EOFException, IOException
  {
    int numCharacters = in.read();
    byte[] characters = new byte[numCharacters];
    int numCharactersRead = in.read(characters);
    Assert.expect(numCharacters == numCharactersRead);

    return new String(characters, 0, numCharacters).intern();
  }

  /**
   * Read a number of characters from the input stream.  The character string doesn't
   * have to be null terminated.
   *
   * @param length the number of characters to read.
   * @return the String created from the characters.
   * @exception EOFException  if at the end of input stream before reading 'len' bytes.
   * @exception IOException if there is an IO error.
   * @author Steve Anonson
   */
  public String readPascalPackedArray(int length) throws EOFException, IOException
  {
    return readCharArrayString(length);
  }

  /**
   * Read a number of characters from the input stream.  The character string doesn't
   * have to be null terminated.
   *
   * @param length the number of characters to read.
   * @param limit the number of valid characters to put in the String.
   * @return the String created from the characters.
   * @exception EOFException if at the end of input stream before reading 'len' bytes.
   * @exception IOException if the limit parameter is greater than the number
   * of characters to read or there is an IO error.
   * @author Steve Anonson
   */
  public String readPascalPackedArray(int length, int limit) throws EOFException, IOException
  {
    return readCharArrayString( length, limit);
  }

  /**
   * @author Steve Anonson
   */
  public String readLine() throws IOException
  {
    // Deprecated function so I won't complete it.
    Assert.expect(false, "Deprecated method, not implemented.");

    return null;
  }

  /**
   * @author Steve Anonson
   */
  public String readUTF() throws IOException
  {
    // C/C++ doesn't have this type so I won't complete this method.
    Assert.expect(false, "No corresponding C/C++ type, not implemented.");

    return null;
  }
}
