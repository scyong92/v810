package com.axi.util;

import java.util.regex.*;

/**
 * This is a checked exception replacement for invalid regular expressions. It replaces Java's unchecked
 * PatternSyntaxException, and provides get methods for the error details
 *
 * @author Laura Cormos
 */
public class InvalidRegularExpressionException extends Exception
{
  private String _errorMessage;
  private String _pattern;
  private int _index = -1;

  /**
   * @author Laura Cormos
   */
  public InvalidRegularExpressionException(PatternSyntaxException pse)
  {
    super("The regular expression \"" + pse.getPattern() + "\" is not valid.\n" + pse.getMessage());
    Assert.expect(pse != null);

    _index = pse.getIndex();
    _errorMessage = pse.getMessage();
    _pattern = pse.getPattern();
  }

  /**
   * @author Laura Cormos
   */
  public String getErrorMessage()
  {
    Assert.expect(_errorMessage != null);

    return _errorMessage;
  }

  /**
   * @author Laura Cormos
   */
  public String getPattern()
  {
    Assert.expect(_pattern != null);

    return _pattern;
  }

  /**
   * @author Laura Cormos
   */
  public int getIndex()
  {
    Assert.expect(_index != -1);

    return _index;
  }
}
