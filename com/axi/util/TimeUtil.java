package com.axi.util;

import java.util.*;

/**
 * @author Bill Darbie
 */
public class TimeUtil
{
  /**
   * @author Bill Darbie
   */
  public static String getTimeStamp()
  {
    Calendar calendar = Calendar.getInstance();
    int month = calendar.get(Calendar.DAY_OF_MONTH);
    int day = calendar.get(Calendar.DATE);
    int year = calendar.get(Calendar.YEAR);
    int hour = calendar.get(Calendar.HOUR);
    int minute = calendar.get(Calendar.MINUTE);
    int second = calendar.get(Calendar.SECOND);
    String timeStamp = month + "/" + day + "/" + year + " " + hour + ":" + minute + ":" + second;

    return timeStamp;
  }

}