package com.axi.util;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * This class provides a generic socket server implementation.
 * It accepts a connection from a client that initiates the connection.
 * It then receives the input or request through streams,
 * and processes it with the method, "processInput()".
 * If the input from the client is not in a proper format, it sends the message back to client
 * informing the invalid input.
 *
 * @author Eugene Kim-Leighton
 */
public class SocketServer
{
  private static int _port = -1;
  private SocketServerMessageHandlerInt _messageHandlerInt = null;

  private ServerSocket _serverSocket = null;
  private Socket _acceptSocket = null;
  private BufferedReader _inputStream = null;
  private PrintWriter _outputStream = null;

  private String _input = null;
  private volatile boolean _connected = false;
  private volatile boolean _serverRunning = true;
  private List<String> _responses = null;

  /**
   * Create a socket server that will accept connections from client sockets.
   *
   * @param port is the port that server is listening and accepting any connection from any client.
   * @param messageHandlerInt is the interface that the user (the specific purposed server) of this class must provide.
   *                          It provides the mechanism that processes the client's messages and generates the proper responses.
   *
   * @author Eugene Kim-Leighton
   */
  public SocketServer(int port, SocketServerMessageHandlerInt messageHandlerInt) throws IOException
  {
    // assert if the port number is not in valid range
    Assert.expect(port >= 0 && port <= 65535);
    // assert if user of this server does not provide processInput implementation.
    Assert.expect(messageHandlerInt != null);

    _port = port;
    _messageHandlerInt = messageHandlerInt;

    // create a serverSocket at the specified port
    _serverSocket = new ServerSocket(_port);
  }

  /**
   * This function accepts a client's connection.
   * While the connection is alive, the server will do the following:
   *       1) Receive the request from the client
   *       2) Handle client's requests and send the result back to the client
   *       3) If necessary, the server sends out the error message back to the client for the invalid requests
   *
   * @author Eugene Kim-Leighton
   */
  public void acceptConnectionAndProcessMessage() throws IOException
  {
    try
    {
      _serverRunning = true;
      while(_serverRunning)
      {
        // server is listening to any client that tries to connect to this server
        // note that this call blocks until a connection is made
        _connected    = false;
        _acceptSocket = _serverSocket.accept();
        _connected    = true;

        openStreams();
        while(_serverRunning && _connected)
        {
          _input = _inputStream.readLine();

          // if _input is null then the client has already disconnected so have the server disconnect
          if (_input == null)
            _connected = false;

          if(_serverRunning && _connected)
          {
            _responses = _messageHandlerInt.processMessage(_input);
            sendResponse();
          }
        }
        closeStreams();
      }
      // close the socket and exit
      _serverSocket.close();
    }
    catch(IOException ioe)
    {
      if(_acceptSocket != null)
        closeStreams();
      if(_serverSocket != null)
        _serverSocket.close();
    }
  }

  /**
   * This opens the streams for receiving the client's request and send response to the client.
   * @throws IOException if there is an error opening streams.
   * @author Eugene Kim-Leighton
   */
  private void openStreams() throws IOException
  {
    Assert.expect(_acceptSocket != null);

    // get the input stream
    InputStream inputStream        = _acceptSocket.getInputStream();
    InputStreamReader streamReader = new InputStreamReader(inputStream);
    _inputStream                   = new BufferedReader(streamReader);

    // get the output stream
    OutputStream outputStream      = _acceptSocket.getOutputStream();
    _outputStream                  = new PrintWriter(outputStream);
  }

  /**
   * This sends out the reponse to the client.
   * This server supports the "\n"(Line Feeder) as only end of the string character,
   * instead of "\r\n" (Carrage Return and Line Feeder).
   * @author Eugene Kim-Leighton
   */
  private void sendResponse()
  {
    for (String message : _responses)
    {
      message = message + "\n";
      _outputStream.print(message);
    }
    _outputStream.flush();
  }

  /**
   * This closes the socket and streams that are used for a connection with a client, if the client disconnects from
   * this server.
   * @author Eugene Kim-Leighton
   */
  private void closeStreams()
  {
    try
    {
      if (_inputStream != null)
        _inputStream.close();
    }
    catch(IOException ioe)
    {
      ioe.printStackTrace();
    }

    if (_outputStream != null)
    _outputStream.close();

    try
    {
      if (_acceptSocket != null)
      _acceptSocket.close();
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
  }

  /**
   * The SocketServerMessageHandlerInt.processMessage method should call this method to tell the Server
   * that the client is disconnecting.  The Server will disconnect and then wait for another
   * client to connect after this call is made.
   *
   * @author Bill Darbie
   */
  public void disconnectFromClient()
  {
    _connected = false;
  }

  /**
   * The SocketServerMessageHandlerInt.processMessage() method should call this method to tell
   * the Server that the client is terminating server program.
   *
   * @author Bill Darbie
   */
  public void killServer()
  {
    _serverRunning = false;
  }


  /**
   * Returns the ServerSocket bound to this SocketServer.
   *
   * @author Matt Wharton
   */
  public ServerSocket getServerSocket()
  {
    return _serverSocket;
  }
}
