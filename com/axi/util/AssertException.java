package com.axi.util;

/**
 * The AssertException class is responible for throwing a RunTimeException.
 * The RunTimeException will contatin all stack trace information that will
 * be displayed when an assert condition failes.
 *
 * @author Bill Darbie
 */
public class AssertException extends RuntimeException
{
  AssertException()
  {
    super();
  }

  AssertException(String s)
  {
    super(s);
  }
};
