package com.axi.util;

/**
 * @author Cheah Lee Herng
 */
public class ThinPlateSpline
{
    private int _totalNumberOfPoints = 0;
    private String _regionPositionName = null;
    
    protected static boolean _debug = false;
    private boolean _isSimulation = false;
    
    static
    {      
        System.loadLibrary("nativeAxiUtil");
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public ThinPlateSpline(String regionPositionName)
    {
        Assert.expect(regionPositionName != null);
        _regionPositionName = regionPositionName;
    }

    /**
     * @author Cheah, Lee Herng
     */
    public synchronized void reset()
    {
        if (isSimulation())
            return;
        
        nativeReset(_regionPositionName);
    }

    /**
     * @author Cheah, Lee Herng
     */
    public synchronized void addPoint(float focusRegionCenterXCoordinateInNanometers,
                                      float focusRegionCenterYCoordinateInNanometers,
                                      float zHeight_FiducialRelativeNanometers)
    {
        if (isSimulation())
            return;
        
        nativeAddPoint(_regionPositionName, focusRegionCenterXCoordinateInNanometers, focusRegionCenterYCoordinateInNanometers, zHeight_FiducialRelativeNanometers);        
    }

    /**
     * @author Cheah, Lee Herng
     */
    public synchronized void update()
    {
        if (isSimulation())
            return;

        if (isDebug())
            System.out.println("Total points added into Thin Plate Spline model = " + _totalNumberOfPoints);

        nativeUpdate(_regionPositionName);
    }

    /**
     * @author Cheah, Lee Herng
     */
    public synchronized boolean isSufficientSurfaceModelDataPoint()
    {
        return nativeIsSufficientSurfaceModelDataPoint(_regionPositionName);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public synchronized int getNumberOfControlPoints()
    {
        return nativeGetNumberOfControlPoints(_regionPositionName);
    }

    /**
     * @author Cheah Lee Herng
     */
    public synchronized double interpolateZHeightInNanometers(float focusRegionCenterXCoordinateInNanometers, float focusRegionCenterYCoordinateInNanometers)
    {
        Assert.expect(isSufficientSurfaceModelDataPoint());        
        return nativeInterpolateZHeightInNanoMeters(_regionPositionName, focusRegionCenterXCoordinateInNanometers, focusRegionCenterYCoordinateInNanometers);
    }

//    /**
//     * @author Cheah Lee Herng
//     */
//    private Point2D getReconstructedImagesSystemFiducialRectangleCenterPoint2D(ReconstructedImages reconstructedImages)
//    {
//        Assert.expect(reconstructedImages != null);
//        ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
//        PanelRectangle panelRectangle = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
//        SystemFiducialRectangle systemFiducialRectangle = MechanicalConversions.convertPanelRectangleToSystemFiducialRectangle(panelRectangle,
//                                                                                                                               reconstructionRegion.getManualAlignmentMatrix());
//        Point2D point2D = new Point2D.Double(systemFiducialRectangle.getCenterX(), systemFiducialRectangle.getCenterY());
//        return point2D;
//    }
//
//    /**
//     * @author Cheah Lee Herng
//     */
//    private SliceNameEnum getSliceNameEnumForPadSlice(JointInspectionData jointInspectionData)
//    {
//        Assert.expect(jointInspectionData != null);
//
//        // PAD is the default
//        SliceNameEnum padSliceNameEnum = SliceNameEnum.PAD;
//
//        JointTypeEnum jointTypeEnum = jointInspectionData.getJointTypeEnum();
//
//        // if this is a cap, then we need to figure out if we should use the clear or opaque slice
//        if (jointTypeEnum.equals(JointTypeEnum.CAPACITOR))
//        {
//          if (ChipMeasurementAlgorithm.testAsOpaque(jointInspectionData))
//          {
//            padSliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
//          }
//          else
//          {
//            padSliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
//          }
//        }
//        // resistors always use the clear slice
//        if (jointTypeEnum.equals(JointTypeEnum.RESISTOR))
//        {
//          padSliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
//        }
//        // for throughhole, let's use the pin side slice
//        // Note that this is on the "wrong: side, so will require special handling!
//        if (jointTypeEnum.equals(JointTypeEnum.THROUGH_HOLE))
//        {
//          padSliceNameEnum = SliceNameEnum.THROUGHHOLE_PIN_SIDE;
//          //padSliceNameEnum = SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE;
//        }
//        // for CSP, we have to use the midball and actually, it should be close enough
//        if (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
//        {
//          padSliceNameEnum = SliceNameEnum.MIDBALL;
//        }
//        // For pressfit, use the component side slice
//        if (jointTypeEnum.equals(JointTypeEnum.PRESSFIT))
//        {
//          padSliceNameEnum = SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE;
//        }
//        return padSliceNameEnum;
//    }    
//
//    /**
//     * Add new surface point into Thin Plate Spline model
//     *
//     * @param jointInspectionDataList
//     * @param reconstructedImages
//     * @param actualPadZHeight
//     */
//    public synchronized void addDataPoint(List<JointInspectionData> jointInspectionDataList,
//                                          ReconstructedImages reconstructedImages)
//    {
//        Assert.expect(jointInspectionDataList != null);
//        Assert.expect(reconstructedImages != null);
//
//        // No points are added if we disable ThinPlateSpline model or we are in simulation mode
//        if (XrayTester.getInstance().isSimulationModeOn())
//          return;
//
//        boolean allJointsPassed = true;
//        boolean isTopSide = false;
//        int padZHeight = 0;        
//        boolean determinedPadSlice = false;
//        
//        ReconstructionRegion region = reconstructedImages.getReconstructionRegion();        
//        for (JointInspectionData jointInspectionData : jointInspectionDataList)
//        {
//          if (determinedPadSlice == false)
//          {
//            // Check to see if thie joint type should be used to build the limited surface model
//            JointTypeEnum jointType = jointInspectionData.getJointTypeEnum();
//            if (jointType.isUseToBuildLimitedSurfaceModel() == false)
//            {
//              return;
//            }
//            SliceNameEnum padSliceNameEnum = getSliceNameEnumForPadSlice(jointInspectionData);
//            isTopSide = jointInspectionData.isTopSideSurfaceModel();
//
//            boolean foundPadSlice = false;
//            for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
//            {
//              if (reconstructedSlice.getSliceNameEnum().equals(padSliceNameEnum))
//              {
//                padZHeight = reconstructedSlice.getHeightInNanometers();
//                foundPadSlice = true;
//              }
//            }
//            Assert.expect(foundPadSlice, "no pad slice for " + jointInspectionData.getFullyQualifiedPadName());
//            determinedPadSlice = true;
//          }
//
//          // check if the joint failed or its component failed
//          JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
//          boolean jointPassed = jointInspectionResult.passed();
//          ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
//          ComponentInspectionResult componentInspectionResult = componentInspectionData.getComponentInspectionResult();
//          boolean componentPassed = componentInspectionResult.passed();
//          boolean jointAndComponentPassed = jointPassed && componentPassed;
//
//          if (jointAndComponentPassed == false)
//          {
//              allJointsPassed = false;
//              break;
//          }
//        }
//
//        Assert.expect(determinedPadSlice);
//
//        if (allJointsPassed && isTopSide)
//        {
//            Point2D point2D = getReconstructedImagesSystemFiducialRectangleCenterPoint2D(reconstructedImages);
//            addPoint((float)point2D.getX(), (float)point2D.getY(), (float)padZHeight);
//
//            System.out.println("LEEHERNG: RRID: " + region.getRegionId() + ", isTopSide = " + isTopSide + ", New pad slice points added into ThinPlateSpline model (" + point2D.getX() + "," + point2D.getY() + "," + padZHeight + ")");
//
//            ++_totalNumberOfPoints;
//        }
//    }


    // Native methods
    private native void nativeReset(String regionPositionName);
    private native void nativeAddPoint(String regionPositionName, float focusRegionCenterXCoordinateInNanometers, float focusRegionCenterYCoordinateInNanometers, float zHeight_FiducialRelativeNanometers);
    private native void nativeUpdate(String regionPositionName);
    private native boolean nativeIsSufficientSurfaceModelDataPoint(String regionPositionName);
    private native double nativeInterpolateZHeightInNanoMeters(String regionPositionName, float focusRegionCenterXCoordinateInNanometers, float focusRegionCenterYCoordinateInNanometers);
    private native int nativeGetNumberOfControlPoints(String regionPositionName);

  /**
   * @return the _debug
   */
  public boolean isDebug()
  {
    return _debug;
  }

  /**
   * @author Khaw Chek Hau
   * XCR2183: Standardize all print out when developer debug is true
   */
  public static void setDebug(boolean state)
  {
    _debug = state;
  }

  /**
   * @return the _isSimulation
   */
  public boolean isSimulation()
  {
    return _isSimulation;
  }

  /**
   * @param isSimulation the _isSimulation to set
   */
  public void setSimulation(boolean isSimulation)
  {
    _isSimulation = isSimulation;
  }
}
