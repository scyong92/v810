package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * @author George A. David
 */
public class Test_RangeUtil extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_RangeUtil());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    // tests ranges with end value less than begin value
    try
    {
      new RangeUtil(5, 4);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }
    RangeUtil rangeUtil = new RangeUtil();
    try
    {
      rangeUtil.add(5, 4);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      rangeUtil.subtract(5, 4);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    // start with a range 1-100
    rangeUtil.add(1, 100);
    List<Pair<Integer, Integer>> ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 1);
    Pair<Integer, Integer> pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 100);

    // no subtract 10-20, we should get 1-9 and 21 - 100
    rangeUtil.subtract(10, 20);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 2);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 9);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 21);
    Expect.expect(pair.getSecond() == 100);

    // subract 90-110, we should get 1-9, 21-89
    rangeUtil.subtract(90, 110);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 2);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 9);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 21);
    Expect.expect(pair.getSecond() == 89);

    // subract 95-100, nothing should change, 1-9, 21-89
    rangeUtil.subtract(95, 100);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 2);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 9);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 21);
    Expect.expect(pair.getSecond() == 89);

    // subtract 45-67 => 1-9, 21-44, 68-89
    rangeUtil.subtract(45, 67);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 3);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 9);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 21);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 68);
    Expect.expect(pair.getSecond() == 89);

    // subtract 5-23 => 1-4, 24-44, 68-89
    rangeUtil.subtract(5, 23);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 3);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 4);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 24);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 68);
    Expect.expect(pair.getSecond() == 89);

    // subtract 74-81 => 1-4, 24-44, 68-73, 82-89
    rangeUtil.subtract(74, 81);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 4);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 4);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 24);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 68);
    Expect.expect(pair.getSecond() == 73);

    pair = ranges.get(3);
    Expect.expect(pair.getFirst() == 82);
    Expect.expect(pair.getSecond() == 89);

    // subtract 29-31 => 1-4, 24-28, 32-44, 68-73, 82-89
    rangeUtil.subtract(29, 31);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 5);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 4);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 24);
    Expect.expect(pair.getSecond() == 28);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 32);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(3);
    Expect.expect(pair.getFirst() == 68);
    Expect.expect(pair.getSecond() == 73);

    pair = ranges.get(4);
    Expect.expect(pair.getFirst() == 82);
    Expect.expect(pair.getSecond() == 89);

    // subtract 20-30 => 1-4, 32-44, 68-73, 82-89
    rangeUtil.subtract(20, 30);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 4);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 4);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 32);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 68);
    Expect.expect(pair.getSecond() == 73);

    pair = ranges.get(3);
    Expect.expect(pair.getFirst() == 82);
    Expect.expect(pair.getSecond() == 89);

    //subtract 1-4 => 32-44, 68-73, 82-89
    rangeUtil.subtract(1, 4);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 3);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 32);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 68);
    Expect.expect(pair.getSecond() == 73);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 82);
    Expect.expect(pair.getSecond() == 89);

    //subtract 1-4 => no change: 32-44, 68-73, 82-89
    rangeUtil.subtract(1, 4);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 3);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 32);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 68);
    Expect.expect(pair.getSecond() == 73);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 82);
    Expect.expect(pair.getSecond() == 89);

    // add 1-4 => 1-4, 32-44, 68-73, 82-89
    rangeUtil.add(1, 4);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 4);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 4);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 32);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 68);
    Expect.expect(pair.getSecond() == 73);

    pair = ranges.get(3);
    Expect.expect(pair.getFirst() == 82);
    Expect.expect(pair.getSecond() == 89);

    // add 1-4 => no change: 1-4, 32-44, 68-73, 82-89
    rangeUtil.add(1, 4);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 4);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 4);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 32);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 68);
    Expect.expect(pair.getSecond() == 73);

    pair = ranges.get(3);
    Expect.expect(pair.getFirst() == 82);
    Expect.expect(pair.getSecond() == 89);

    // add 5-7 => 1-7, 32-44, 68-73, 82-89
    rangeUtil.add(5, 7);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 4);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 7);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 32);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 68);
    Expect.expect(pair.getSecond() == 73);

    pair = ranges.get(3);
    Expect.expect(pair.getFirst() == 82);
    Expect.expect(pair.getSecond() == 89);

    // add 25-30 => 1-7, 25-30, 32-44, 68-73, 82-89
    rangeUtil.add(25, 30);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 5);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 7);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 25);
    Expect.expect(pair.getSecond() == 30);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 32);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(3);
    Expect.expect(pair.getFirst() == 68);
    Expect.expect(pair.getSecond() == 73);

    pair = ranges.get(4);
    Expect.expect(pair.getFirst() == 82);
    Expect.expect(pair.getSecond() == 89);

    // add 20-24 => 1-7, 20-30, 32-44, 68-73, 82-89
    rangeUtil.add(20, 24);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 5);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 7);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 20);
    Expect.expect(pair.getSecond() == 30);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 32);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(3);
    Expect.expect(pair.getFirst() == 68);
    Expect.expect(pair.getSecond() == 73);

    pair = ranges.get(4);
    Expect.expect(pair.getFirst() == 82);
    Expect.expect(pair.getSecond() == 89);

    // add 65-75 =>  1-7, 20-30, 32-44, 65-75, 82-89
    rangeUtil.add(65, 75);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 5);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 7);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 20);
    Expect.expect(pair.getSecond() == 30);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 32);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(3);
    Expect.expect(pair.getFirst() == 65);
    Expect.expect(pair.getSecond() == 75);

    pair = ranges.get(4);
    Expect.expect(pair.getFirst() == 82);
    Expect.expect(pair.getSecond() == 89);

    // add 5-10 =>  1-10, 20-30, 32-44, 65-75, 82-89
    rangeUtil.add(5, 10);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 5);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 10);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 20);
    Expect.expect(pair.getSecond() == 30);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 32);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(3);
    Expect.expect(pair.getFirst() == 65);
    Expect.expect(pair.getSecond() == 75);

    pair = ranges.get(4);
    Expect.expect(pair.getFirst() == 82);
    Expect.expect(pair.getSecond() == 89);

    // add 60-70 =>  1-10, 20-30, 32-44, 60-75, 82-89
    rangeUtil.add(60, 70);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 5);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 10);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 20);
    Expect.expect(pair.getSecond() == 30);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 32);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(3);
    Expect.expect(pair.getFirst() == 60);
    Expect.expect(pair.getSecond() == 75);

    pair = ranges.get(4);
    Expect.expect(pair.getFirst() == 82);
    Expect.expect(pair.getSecond() == 89);

    // add 95-100 =>  1-10, 20-30, 32-44, 60-75, 82-89, 95-100
    rangeUtil.add(95, 100);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 6);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 10);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 20);
    Expect.expect(pair.getSecond() == 30);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 32);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(3);
    Expect.expect(pair.getFirst() == 60);
    Expect.expect(pair.getSecond() == 75);

    pair = ranges.get(4);
    Expect.expect(pair.getFirst() == 82);
    Expect.expect(pair.getSecond() == 89);

    pair = ranges.get(5);
    Expect.expect(pair.getFirst() == 95);
    Expect.expect(pair.getSecond() == 100);

    // add 25-35 =>  1-10, 20-44, 60-75, 82-89, 95-100
    rangeUtil.add(25, 35);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 5);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 1);
    Expect.expect(pair.getSecond() == 10);

    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 20);
    Expect.expect(pair.getSecond() == 44);

    pair = ranges.get(2);
    Expect.expect(pair.getFirst() == 60);
    Expect.expect(pair.getSecond() == 75);

    pair = ranges.get(3);
    Expect.expect(pair.getFirst() == 82);
    Expect.expect(pair.getSecond() == 89);

    pair = ranges.get(4);
    Expect.expect(pair.getFirst() == 95);
    Expect.expect(pair.getSecond() == 100);

    // add 0-200 => 0-200
    rangeUtil.add(0, 200);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 1);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 0);
    Expect.expect(pair.getSecond() == 200);

    //subtract 5-5 -> 0-4, 6-200
    rangeUtil.subtract(5, 5);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 2);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 0);
    Expect.expect(pair.getSecond() == 4);
    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 6);
    Expect.expect(pair.getSecond() == 200);

    // add 5-5 => 0-200
    rangeUtil.add(5, 5);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 1);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 0);
    Expect.expect(pair.getSecond() == 200);

    //subtract 5 -> 0-4, 6-200
    rangeUtil.subtract(5);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 2);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 0);
    Expect.expect(pair.getSecond() == 4);
    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 6);
    Expect.expect(pair.getSecond() == 200);

    // add 5 => 0-200
    rangeUtil.add(5);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 1);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 0);
    Expect.expect(pair.getSecond() == 200);

    // subtract 9-37 => 0-8, 38-200
    RangeUtil newRangeUtil = new RangeUtil(9, 37);
    rangeUtil.subtract(newRangeUtil);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 2);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 0);
    Expect.expect(pair.getSecond() == 8);
    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 38);
    Expect.expect(pair.getSecond() == 200);

    // add 9-37 => 0-200
    rangeUtil.add(newRangeUtil);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 1);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 0);
    Expect.expect(pair.getSecond() == 200);

    // subtract -10-300 => empty
    rangeUtil.subtract(-10, 300);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.isEmpty());

    // start with 1 - 100
    rangeUtil.add(1, 100);

    // try intersect with 9 - 37 => 9 - 37
    rangeUtil = RangeUtil.intersect(rangeUtil, newRangeUtil);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 1);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 9);
    Expect.expect(pair.getSecond() == 37);

    // intersect with 1 - 20 => 9 - 20
    newRangeUtil = new RangeUtil(1, 20);
    rangeUtil = RangeUtil.intersect(rangeUtil, newRangeUtil);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 1);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 9);
    Expect.expect(pair.getSecond() == 20);

    // intersect with 1 - 8 => empty
    newRangeUtil = new RangeUtil(1, 8);
    rangeUtil = RangeUtil.intersect(rangeUtil, newRangeUtil);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.isEmpty());

    // add 1- 10 and 15 - 25
    rangeUtil.add(1, 10);
    rangeUtil.add(15, 25);

    // intersect with 5 - 20 => 5 - 10, 15 - 20
    newRangeUtil = new RangeUtil(5, 20);
    rangeUtil = RangeUtil.intersect(rangeUtil, newRangeUtil);
    ranges = rangeUtil.getRanges();
    Assert.expect(ranges.size() == 2);
    pair = ranges.get(0);
    Expect.expect(pair.getFirst() == 5);
    Expect.expect(pair.getSecond() == 10);
    pair = ranges.get(1);
    Expect.expect(pair.getFirst() == 15);
    Expect.expect(pair.getSecond() == 20);
  }
}
