package com.axi.util;

/**
 * Helper class for testing native asserts (i.e. C++ sptAssert that invokes Assert.expect(false)).

 * @author Matt Wharton
 */
class NativeAssertFalse
{
  /**
   * @author Matt Wharton
   */
  private NativeAssertFalse()
  {
    // Should never need to instantiate.
  }

  /**
   * Native stub whose underlying implementation does a "sptAssert(false)".
   *
   * @author Matt Wharton
   */
  static native void nativeAssertFalseSimple();

  /**
   * Native stub whose underlying implementation calls a function in a seperate DLL which
   * does a "sptAssert(false)".
   *
   * @author Matt Wharton
   */
  static native void nativeAssertFalseNested();
}
