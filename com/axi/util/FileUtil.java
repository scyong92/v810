package com.axi.util;

import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.util.zip.*;
import java.nio.channels.*;

/**
 * This class provides common file operations that are not included in
 * the standard java.io.File class.  NOTE for the 5dx do NOT use this class,
 * use com.axi.xRayTest.util.FileUtil5dx instead.  It is a wrapper for
 * this class that converts all the exceptions thrown here into DatastoreExceptions
 * If you add a method to this class MAKE SURE TO ADD IT to FileUtil5dx also.
 *
 * @author Bill Darbie
 */
public class FileUtil
{
  private static int _FLOPPY_SIZE = 1450000; // actual size 1457664
  private static int _ZIP_BUFFER = 2048;
  private static int _COPY_BUFFER = 8192;
  private static boolean _DONT_PRESERVE_TIMESTAMP = false;
  private static boolean _PRESERVE_TIMESTAMP = true;
  private static String _BACKUP_EXTENSION = ".backup";
  private static String _TEMP_EXTENSION = ".temp";

  private static boolean _cancelZip = false;
  private static boolean _cancelDirectoryCopy = false;
  private static boolean _excludeHiddenFiles = false;
 
  /**
   * @author Bill Darbie
   */
  static
  {
    System.loadLibrary("nativeAxiUtil");
  }

  /**
   * @author Bill Darbie
   */
  public static boolean exists(String fileOrDirectoryName)
  {
    Assert.expect(fileOrDirectoryName != null);

    File file = new File(fileOrDirectoryName);

    return file.exists();
  }

  /**
   * @author John Dutton
   */
  public static boolean existsDirectory(String directoryName)
  {
    Assert.expect(directoryName != null);

    File file = new File(directoryName);

    return file.exists() && file.isDirectory() == true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static boolean existsDirectoryAndAbleToReadWrite(String directoryName)
  {
    Assert.expect(directoryName != null);

    File file = new File(directoryName);

    return file.exists() && file.isDirectory() && file.canRead() && file.canWrite();
  }

  /**
   * Split one file into many smaller ones.  Each smaller file will be no larger than sizeInBytesPerFile.
   * The output file name will match the original file with _1, _2, ... appended.
   * @return a List of output files
   * @author Bill Darbie
   */
  public static List<String> splitFile(String fileName, int maxOutFileSizeInBytes) throws CouldNotCreateFileException,
                                                                                          CouldNotReadFileException,
                                                                                          CouldNotWriteFileException,
                                                                                          CouldNotCloseFileException
  {
    Assert.expect(fileName != null);
    boolean exception = false;
    BufferedInputStream is = null;
    BufferedOutputStream os = null;
    String outFileName = "";
    List<String> fileNames = new ArrayList<String>();

    try
    {
      FileInputStream fis = null;
      try
      {
        fis = new FileInputStream(fileName);
      }
      catch (FileNotFoundException fnfe)
      {
        CouldNotCreateFileException ex = new CouldNotCreateFileException(fileName);
        ex.initCause(fnfe);
        throw ex;
      }
      is = new BufferedInputStream(fis);

      File file = new File(fileName);
      if (file.length() == 0)
        return fileNames;

      int fileNumber = 1;
      outFileName = fileName + "_" + Integer.toString(fileNumber);
      FileOutputStream fos = null;
      try
      {
        fos = new FileOutputStream(outFileName);
      }
      catch (FileNotFoundException fnfe)
      {
        CouldNotCreateFileException ex = new CouldNotCreateFileException(fileName);
        ex.initCause(fnfe);
        throw ex;
      }

      os = new BufferedOutputStream(fos);
      fileNames.add(outFileName);

      byte[] b = new byte[4096];
      int totalBytesRead = 0;
      int totalBytesWritten = 0;
      int numBytesRead = 0;
      do
      {
        try
        {
          numBytesRead = is.read(b);
        }
        catch (IOException ioe)
        {
          CouldNotReadFileException ex = new CouldNotReadFileException(fileName);
          ex.initCause(ioe);
          throw ex;
        }
        if (numBytesRead == -1)
          break;
        totalBytesRead += numBytesRead;

        if (totalBytesRead <= maxOutFileSizeInBytes)
        {
          try
          {
            os.write(b, 0, numBytesRead);
          }
          catch (IOException ioe)
          {
            CouldNotWriteFileException ex = new CouldNotWriteFileException(outFileName);
            ex.initCause(ioe);
            throw ex;
          }
          totalBytesWritten += numBytesRead;
        }
        else
        {
          // write out to the max size allowed only for the current file
          int numBytesToWrite = maxOutFileSizeInBytes - totalBytesWritten;
          try
          {
            os.write(b, 0, numBytesToWrite);
          }
          catch (IOException ioe)
          {
            CouldNotWriteFileException ex = new CouldNotWriteFileException(outFileName);
            ex.initCause(ioe);
            throw ex;
          }
          int numBytesWrittenFromBuffer = numBytesToWrite;

          // write out all remaining bytes in the next file(s)
          while (numBytesWrittenFromBuffer < numBytesRead)
          {
            // now open a new file and begin writing to it
            totalBytesWritten = 0;
            try
            {
              os.close();
            }
            catch (IOException ioe)
            {
              CouldNotCloseFileException ex = new CouldNotCloseFileException(outFileName);
              ex.initCause(ioe);
              throw ex;
            }
            ++fileNumber;
            outFileName = fileName + "_" + Integer.toString(fileNumber);
            fileNames.add(outFileName);
            try
            {
              os = new BufferedOutputStream(new FileOutputStream(outFileName));
            }
            catch (FileNotFoundException fnfe)
            {
              CouldNotCreateFileException ex = new CouldNotCreateFileException(outFileName);
              ex.initCause(fnfe);
              throw ex;
            }

            numBytesToWrite = maxOutFileSizeInBytes - totalBytesWritten;
            try
            {
              os.write(b, numBytesWrittenFromBuffer, numBytesToWrite);
            }
            catch (IOException ioe)
            {
              CouldNotWriteFileException ex = new CouldNotWriteFileException(outFileName);
              ex.initCause(ioe);
              throw ex;
            }

            numBytesWrittenFromBuffer += numBytesToWrite;
            totalBytesWritten += numBytesToWrite;
          }
        }
      }
      while (numBytesRead != -1);
    }
    finally
    {
      try
      {
        if (is != null)
          is.close();
      }
      catch (IOException ioe)
      {
        if (exception == false)
        {
          CouldNotCloseFileException ex = new CouldNotCloseFileException(outFileName);
          ex.initCause(ioe);
          throw ex;
        }
        else
          ioe.printStackTrace();
      }

      try
      {
        if (os != null)
          os.close();
      }
      catch (IOException ioe)
      {
        if (exception == false)
        {
          CouldNotCloseFileException ex = new CouldNotCloseFileException(outFileName);
          ex.initCause(ioe);
          throw ex;
        }
        else
          ioe.printStackTrace();
      }
    }

    return fileNames;
  }

  /**
   * concatenate the contents of inFile1 and inFile2 to create the concatFile.
   * @author Bill Darbie
   */
  public static void concat(String inFile1, String inFile2, String concatFile) throws FileDoesNotExistException,
      CouldNotCreateFileException
  {
    Assert.expect(inFile1 != null);
    Assert.expect(inFile2 != null);
    Assert.expect(concatFile != null);

    FileChannel fcIn1 = null;
    FileChannel fcIn2 = null;
    FileChannel fcOut = null;
    try
    {
      File file1 = new File(inFile1);
      File file2 = new File(inFile2);

      String fileName = "";
      FileInputStream fis1;
      FileInputStream fis2;
      FileOutputStream fos;
      try
      {
        fileName = inFile1;
        fis1 = new FileInputStream(fileName);
        fileName = inFile2;
        fis2 = new FileInputStream(fileName);
        fileName = concatFile;
        fos = new FileOutputStream(fileName);
      }
      catch (FileNotFoundException fnfe)
      {
        FileDoesNotExistException ex = new FileDoesNotExistException(fileName);
        ex.initCause(fnfe);
        throw ex;
      }

      fcIn1 = fis1.getChannel();
      fcIn2 = fis2.getChannel();
      fcOut = fos.getChannel();
      int maxCount = 32 * 1024 * 1024;

      try
      {
//        fileName = inFile1;
        long size1 = file1.length();
//        long numBytesTransfered = fcOut.transferFrom(fcIn1, 0, size1);
        long position = 0;
        while (size1 > 0) {
            fcOut.transferFrom(fcIn1, position, (size1>=maxCount ? maxCount : size1));
            size1 -= maxCount;
            position += maxCount;
        }
        fcOut.position(file1.length());
//        Assert.expect(size1 == numBytesTransfered);
        long size2 = file2.length();
//        fileName = inFile2;
//        numBytesTransfered = fcOut.transferFrom(fcIn2, size1, size2);
//        Assert.expect(size2 == numBytesTransfered);
        position = file1.length();
        while (size2 > 0) {
            fcOut.transferFrom(fcIn2, position, (size2>=maxCount ? maxCount : size2));
            size2 -= maxCount;
            position += maxCount;
        }
      }
      catch (IOException ioe)
      {
        // try to close the output file and delete it, then throw the exception
        try
        {
          if (fcOut != null)
            fcOut.close();

          if (FileUtil.exists(concatFile))
            FileUtil.delete(concatFile);
        }
        catch (IOException ioe2)
        {
          ioe2.printStackTrace();
        }

        CouldNotCreateFileException ex = new CouldNotCreateFileException(concatFile);
        ex.initCause(ioe);
        throw ex;
      }
    }
    finally
    {
      try
      {
        if (fcIn1 != null)
          fcIn1.close();
      }
      catch (IOException ioe)
      {
        ioe.printStackTrace();
      }

      try
      {
        if (fcIn2 != null)
          fcIn2.close();
      }
      catch (IOException ioe)
      {
        ioe.printStackTrace();
      }

      try
      {
        if (fcOut != null)
          fcOut.close();
      }
      catch (IOException ioe)
      {
        ioe.printStackTrace();
      }
    }

// wpd - the code below works, but the file channel code above is supposed to be faster
//    BufferedInputStream is1;
//    BufferedInputStream is2;
//    BufferedOutputStream os;
//
//    String fileName = "";
//    byte[] bytes = new byte[_COPY_BUFFER];
//    try
//    {
//      fileName = inFile1;
//      is1 = new BufferedInputStream(new FileInputStream(fileName));
//      fileName = inFile2;
//      is2 = new BufferedInputStream(new FileInputStream(fileName));
//      fileName = concatFile;
//      os = new BufferedOutputStream(new FileOutputStream(fileName));
//    }
//    catch (FileNotFoundException fnfe)
//    {
//      FileDoesNotExistException ex = new FileDoesNotExistException(fileName);
//      ex.initCause(fnfe);
//      throw ex;
//    }
//
//    try
//    {
//      // read in file1
//      try
//      {
//        int numBytes = is1.read(bytes);
//        while (numBytes != -1)
//        {
//          os.write(bytes, 0, numBytes);
//          numBytes = is1.read(bytes);
//        }
//      }
//      catch (IOException ioe)
//      {
//        try
//        {
//          if (os != null)
//            os.close();
//        }
//        catch (IOException ioe2)
//        {
//          ioe.printStackTrace();
//        }
//
//        try
//        {
//          if (FileUtil.exists(concatFile))
//            FileUtil.delete(concatFile);
//        }
//        catch (CouldNotDeleteFileException fe)
//        {
//          fe.printStackTrace();
//        }
//
//        CouldNotReadFileException ex = new CouldNotReadFileException(inFile1);
//        ex.initCause(ioe);
//        throw ex;
//      }
//
//      try
//      {
//        // read in file2
//        int numBytes = is2.read(bytes);
//        while (numBytes != -1)
//        {
//          os.write(bytes, 0, numBytes);
//          numBytes = is2.read(bytes);
//        }
//      }
//      catch (IOException ioe)
//      {
//        try
//        {
//          if (os != null)
//            os.close();
//        }
//        catch (IOException ioe2)
//        {
//          ioe.printStackTrace();
//        }
//
//        try
//        {
//          if (FileUtil.exists(concatFile))
//            FileUtil.delete(concatFile);
//        }
//        catch (CouldNotDeleteFileException fe)
//        {
//          fe.printStackTrace();
//        }
//
//        CouldNotReadFileException ex = new CouldNotReadFileException(inFile2);
//        ex.initCause(ioe);
//        throw ex;
//      }
//    }
//    finally
//    {
//      try
//      {
//        if (is1 != null)
//          is1.close();
//      }
//      catch (IOException ioe)
//      {
//        ioe.printStackTrace();
//      }
//      try
//      {
//        if (is2 != null)
//          is2.close();
//      }
//      catch (IOException ioe)
//      {
//        ioe.printStackTrace();
//      }
//      try
//      {
//        if (os != null)
//          os.close();
//      }
//      catch (IOException ioe)
//      {
//        ioe.printStackTrace();
//      }
//    }
  }


  /**
   * Checks for a proper separator at the end of the path before appending the filename.
   * @author Patrick Lacz
   */
  public static String concatFileNameToPath(String path, String fileName)
  {
    Assert.expect(path != null);
    Assert.expect(fileName != null);

    if (path.endsWith(File.separator) == false)
      return path + File.separator + fileName;

    return path + fileName;
  }

  /**
   * Copy from fromFile to toFile.
   * @author Bill Darbie
   */
  public static void copy(String fromFile, String toFile) throws FileDoesNotExistException, CouldNotCopyFileException
  {
    copy(fromFile, toFile, _DONT_PRESERVE_TIMESTAMP);
  }

  /**
   * Copy from fromFile to toFile and preserve the time stamp
   * @author Bill Darbie
   */
  public static void copyPreserveTimeStamp(String fromFile, String toFile) throws FileDoesNotExistException,
                                                                                  CouldNotCopyFileException
  {
    copy(fromFile, toFile, _PRESERVE_TIMESTAMP);
  }

  /**
   * @author Bill Darbie
   */
  private static void copy(String fromFileName,
                           String toFileName,
                           boolean preserveTimeStamp) throws FileDoesNotExistException, CouldNotCopyFileException
  {
    Assert.expect(fromFileName != null);
    Assert.expect(toFileName != null);

    File fromFile = new File(fromFileName);
    File toFile = new File(toFileName);

    BufferedInputStream is = null;
    BufferedOutputStream os = null;

    boolean exception = false;
    try
    {
      FileInputStream fis = null;
      try
      {
        fis = new FileInputStream(fromFileName);
      }
      catch(FileNotFoundException fnfe)
      {
        exception = true;

        FileDoesNotExistException ex = new FileDoesNotExistException(fromFileName);
        ex.initCause(fnfe);
        throw ex;
      }
      is = new BufferedInputStream(fis, _COPY_BUFFER);

      FileOutputStream fos = null;
      try
      {
        fos = new FileOutputStream(toFileName);
      }
      catch(FileNotFoundException fnfe)
      {        
        exception = true;
        CouldNotCopyFileException ex = new CouldNotCopyFileException(fromFileName, toFileName);

        ex.initCause(fnfe);
        throw ex;
      }
      if (exception == false)
      {
        os = new BufferedOutputStream(fos, _COPY_BUFFER);

        byte[] bytes = new byte[_COPY_BUFFER];
        try
        {
          int numBytes = is.read(bytes, 0, _COPY_BUFFER);
          while(numBytes != -1)
          {
            os.write(bytes, 0, numBytes);
            numBytes = is.read(bytes, 0, _COPY_BUFFER);
          }
        }
        catch(IOException ioe)
        {
          exception = true;

          CouldNotCopyFileException ex = new CouldNotCopyFileException(fromFileName, toFileName);
          ex.initCause(ioe);
          throw ex;
        }
      }
    }
    finally
    {
      try
      {
        if (is != null)
          is.close();
      }
      catch(IOException ioe)
      {
        ioe.printStackTrace();
      }
      try
      {
        if (os != null)
          os.close();
      }
      catch(IOException ioe)
      {
        ioe.printStackTrace();
      }

      if ((exception) && (os != null))
      {
        // if os is null then the output stream was never used
        // and there is no need to delete the file we are copying to
        toFile.delete();
      }
    }

    // ok - now the copy is done - preserve time stamp if required
    if (preserveTimeStamp)
      Assert.expect(toFile.setLastModified(fromFile.lastModified()));

// the code below was supposed to be faster, but we ran accross issues
// transfering large file > 1GB. So we have re-instated the code above.
//    FileChannel fcIn = null;
//    FileChannel fcOut = null;
//    try
//    {
//      File file = new File(fromFileName);
//
//      String fileName = "";
//      FileInputStream fis;
//      FileOutputStream fos;
//      try
//      {
//        fileName = fromFileName;
//        fis = new FileInputStream(fileName);
//        fileName = toFileName;
//        fos = new FileOutputStream(fileName);
//      }
//      catch (FileNotFoundException fnfe)
//      {
//        FileDoesNotExistException ex = new FileDoesNotExistException(fileName);
//        ex.initCause(fnfe);
//        throw ex;
//      }
//
//      fcIn = fis.getChannel();
//      fcOut = fos.getChannel();
//
//      try
//      {
//        fileName = fromFileName;
//        long size1 = file.length();
//        long totalBytesTransfered = 0;
//        long startingFilePosition = 0;
//        long numBytesToTransfer = size1;
//        long maxBytesToTransfer = 10000000;
//        if (numBytesToTransfer > maxBytesToTransfer)
//          numBytesToTransfer = maxBytesToTransfer;
//        while(totalBytesTransfered != size1)
//        {
//          long numBytesTransfered = fcOut.transferFrom(fcIn, startingFilePosition, numBytesToTransfer);
//          Assert.expect(numBytesToTransfer == numBytesTransfered);
//          totalBytesTransfered += numBytesTransfered;
//          if (size1 - totalBytesTransfered < numBytesToTransfer)
//            numBytesToTransfer = size1 - totalBytesTransfered;
//          startingFilePosition += numBytesTransfered;
//        }
//      }
//      catch (IOException ioe)
//      {
//        // try to close the output file and delete it, then throw the exception
//        try
//        {
//          if (fcOut != null)
//            fcOut.close();
//
//          if (FileUtil.exists(toFileName))
//            FileUtil.delete(toFileName);
//        }
//        catch (IOException ioe2)
//        {
//          ioe2.printStackTrace();
//        }
//
//        CouldNotCopyFileException ex = new CouldNotCopyFileException(fromFileName, toFileName);
//        ex.initCause(ioe);
//        throw ex;
//      }
//    }
//    finally
//    {
//      try
//      {
//        if (fcIn != null)
//          fcIn.close();
//      }
//      catch (IOException ioe)
//      {
//        ioe.printStackTrace();
//      }
//
//      try
//      {
//        if (fcOut != null)
//          fcOut.close();
//      }
//      catch (IOException ioe)
//      {
//        ioe.printStackTrace();
//      }
//    }
//
//    // ok - now the copy is done - preserve time stamp if required
//    File fromFile = new File(fromFileName);
//    File toFile = new File(toFileName);
//    if (preserveTimeStamp)
//      Assert.expect(toFile.setLastModified(fromFile.lastModified()));
  }

  /**
   * Copy all the files contained in fromDirectory over to toDirectory.  Subdirectories
   * are not copied.  Both fromDirectory and toDirectory must already exist.
   *
   * @author Bill Darbie
   */
  public static void copyDirectory(String fromDirectory,
                                   String toDirectory) throws FileDoesNotExistException,
                                                              FileFoundWhereDirectoryWasExpectedException,
                                                              CouldNotCopyFileException
  {
    copyDirectory(fromDirectory, toDirectory, _DONT_PRESERVE_TIMESTAMP);
  }

  /**
   * Copy all the files contained in fromDirectory over to toDirectory without changing any time stamps.  Subdirectories
   * are not copied.  Both fromDirectory and toDirectory must already exist.
   *
   * @author Bill Darbie
   */
  public static void copyDirectoryPreserveTimeStamp(String fromDirectory,
                                                    String toDirectory)  throws FileDoesNotExistException,
                                                                                FileFoundWhereDirectoryWasExpectedException,
                                                                                CouldNotCopyFileException
  {
    copyDirectory(fromDirectory, toDirectory, _PRESERVE_TIMESTAMP);
  }

  /**
   * @author Bill Darbie
   */
  private static void copyDirectory(String fromDirectory,
                                    String toDirectory,
                                    boolean preserveTimeStamp) throws FileDoesNotExistException,
                                                                      FileFoundWhereDirectoryWasExpectedException,
                                                                      CouldNotCopyFileException
  {
    Assert.expect(fromDirectory != null);
    Assert.expect(toDirectory != null);

    _cancelDirectoryCopy = false;

    File fromDir = new File(fromDirectory);
    File toDir = new File(toDirectory);

    if (fromDir.exists() == false)
      throw new FileDoesNotExistException(fromDirectory);
    if (toDir.exists() == false)
      throw new FileDoesNotExistException(toDirectory);
    if (fromDir.isDirectory() == false)
      throw new FileFoundWhereDirectoryWasExpectedException(fromDirectory);
    if (toDir.isDirectory() == false)
      throw new FileFoundWhereDirectoryWasExpectedException(toDirectory);

    File[] files = fromDir.listFiles();
    if (files != null)
    {
      for (int i = 0; i < files.length; ++i)
      {
        if (_cancelDirectoryCopy)
        {
          break;
        }
        File file = files[i];
        if (file.isDirectory() == false)
        {
          String toFileName = toDirectory + File.separator + file.getName();
          copy(file.getPath(), toFileName, preserveTimeStamp);
        }
      }
    }
  }

  /**
   * Copy all files and directories in fromDirectory to toDirectory recursively.
   * Both fromDirectory and toDirectory must already exist.
   * @author Bill Darbie
   */
  public static void copyDirectoryRecursively(String fromDirectory,
                                              String toDirectory) throws FileDoesNotExistException,
                                                                         FileFoundWhereDirectoryWasExpectedException,
                                                                         CouldNotCopyFileException,
                                                                         CouldNotCreateFileException
  {
    _excludeHiddenFiles = true;
    try
    {
      copyDirectoryRecursively(fromDirectory, toDirectory, _DONT_PRESERVE_TIMESTAMP);
    }
    finally
    {
      _excludeHiddenFiles = false;  // revert to defaults
    }
  }

  /**
   * Copy all files and directories in fromDirectory to toDirectory recursively without changing any time stamps.
   * Both fromDirectory and toDirectory must already exist.
   * @author Bill Darbie
   */
  public static void copyDirectoryRecursivelyPreserveTimeStamp(String fromDirectory,
                                                               String toDirectory) throws FileDoesNotExistException,
                                                                                          FileFoundWhereDirectoryWasExpectedException,
                                                                                          CouldNotCopyFileException,
                                                                                          CouldNotCreateFileException
  {
    copyDirectoryRecursively(fromDirectory, toDirectory, _PRESERVE_TIMESTAMP);
  }

  /**
   * @author Bill Darbie
   */
  private static void copyDirectoryRecursively(String fromDirectory,
                                               String toDirectory,
                                               boolean preserveTimeStamp) throws FileDoesNotExistException,
                                                                                 FileFoundWhereDirectoryWasExpectedException,
                                                                                 CouldNotCopyFileException,
                                                                                 CouldNotCreateFileException
  {
    List<String> dontCopyList = new ArrayList<String>();
    copyDirectoryRecursively(fromDirectory, toDirectory, dontCopyList, preserveTimeStamp);
  }


  /**
   * Copy all files and directories in fromDirectory to toDirectory recursively except for the files
   * or directory listed in the dontCopyFilesOrDirectories List.
   * Both fromDirectory and toDirectory must already exist.
   * @author Bill Darbie
   */
  public static void copyDirectoryRecursively(String fromDirectory,
                                              String toDirectory,
                                              List<String> dontCopyFilesOrDirectories) throws FileDoesNotExistException,
                                                                                      FileFoundWhereDirectoryWasExpectedException,
                                                                                      CouldNotCreateFileException,
                                                                                      CouldNotCopyFileException
  {
    copyDirectoryRecursively(fromDirectory, toDirectory, dontCopyFilesOrDirectories, _DONT_PRESERVE_TIMESTAMP);
  }

  /**
   * Copy all files and directories in fromDirectory to toDirectory recursively without changing
   * any time stamps except for the files or directory listed in the dontCopyFilesOrDirectories List.
   * Both fromDirectory and toDirectory must already exist.
   * @author Bill Darbie
   */
  public static void copyDirectoryRecursivelyPreserveTimeStamp(String fromDirectory,
                                                               String toDirectory,
                                                               List<String> dontCopyFilesOrDirectories) throws FileDoesNotExistException,
                                                                                                       FileFoundWhereDirectoryWasExpectedException,
                                                                                                       CouldNotCreateFileException,
                                                                                                       CouldNotCopyFileException
  {
    copyDirectoryRecursively(fromDirectory, toDirectory, dontCopyFilesOrDirectories, _PRESERVE_TIMESTAMP);
  }

  /**
   * @author Bill Darbie
   */
  private static void copyDirectoryRecursively(String fromDirectory,
                                               String toDirectory,
                                               List<String> dontCopyFilesOrDirectories,
                                               boolean preserveTimeStamp) throws FileDoesNotExistException,
                                                                                 FileFoundWhereDirectoryWasExpectedException,
                                                                                 CouldNotCreateFileException,
                                                                                 CouldNotCopyFileException
  {
    Assert.expect(fromDirectory != null);
    Assert.expect(toDirectory != null);
    Assert.expect(dontCopyFilesOrDirectories != null);

    _cancelDirectoryCopy = false;

    File fromDir = new File(fromDirectory);
    File toDir = new File(toDirectory);

    if (fromDir.exists() == false)
      throw new FileDoesNotExistException(fromDirectory);
    if (toDir.exists() == false)
      throw new FileDoesNotExistException(toDirectory);
    if (fromDir.isDirectory() == false)
      throw new FileFoundWhereDirectoryWasExpectedException(fromDirectory);
    if (toDir.isDirectory() == false)
      throw new FileFoundWhereDirectoryWasExpectedException(toDirectory);

    Set<String> dontCopySet = new HashSet<String>(dontCopyFilesOrDirectories);

    File[] files = fromDir.listFiles();
    if (files != null)
    {
      for (int i = 0; i < files.length; ++i)
      {
        if (_cancelDirectoryCopy)
        {
          break;
        }
        File file = files[i];
        String fileName = file.getAbsolutePath();

        if (dontCopySet.contains(fileName))
        {
          // do not copy this file, continue back with the for loop
          continue;
        }

        if (file.isDirectory())
        {
          if (file.equals(toDir) == false)
          {
            File newDir = new File(toDirectory + File.separator + file.getName());
            if ((newDir.exists() == false) && (newDir.mkdir() == false))
              throw new CouldNotCreateFileException(newDir.getPath());

            // XCR-3735 Failed to open v810 application after patch installation due to thumbs.db lock it
            copyDirectoryRecursively(file.getPath(), newDir.getPath(), dontCopyFilesOrDirectories, preserveTimeStamp);
          }
        }
        else
        {
          if (_excludeHiddenFiles && file.isHidden())
            return;
          copy(file.getPath(), toDirectory + File.separator + file.getName(), preserveTimeStamp);
        }
      }
    }
  }

  /**
   * Stop copying the directory contents when this is called.  The files that were already copied
   * will be left where they are.
   * @author Bill Darbie
   */
  public static void cancelDirectoryCopy()
  {
    _cancelDirectoryCopy = true;
  }

  /**
   * Move all the files and directories in the fromDirectory to the toDirectory.
   * Both the fromDirectory and the toDirectory must exist before this method is called.
   * @author Bill Darbie
   */
  public static void moveDirectoryContents(String fromDirectory, String toDirectory) throws FileDoesNotExistException,
                                                                                            FileFoundWhereDirectoryWasExpectedException,
                                                                                            CouldNotRenameFileException
  {
    List<String> dontMoveFiles = new ArrayList<String>();
    moveDirectoryContents(fromDirectory, toDirectory, dontMoveFiles);
  }

  /**
   * Move all the files and directories in the fromDirectory to the toDirectory except for
   * the files listed in dontMoveFiles.
   * Both the fromDirectory and the toDirectory must exist before this method is called.
   * @author Bill Darbie
   */
  public static void moveDirectoryContents(String fromDirectory,
                                           String toDirectory,
                                           List<String> dontMoveDirOrFiles) throws FileDoesNotExistException,
                                                                                   FileFoundWhereDirectoryWasExpectedException,
                                                                                   CouldNotRenameFileException
  {
    Assert.expect(fromDirectory != null);
    Assert.expect(toDirectory != null);
    Assert.expect(dontMoveDirOrFiles!= null);

    File fromDir = new File(fromDirectory);
    File toDir = new File(toDirectory);

    if (fromDir.exists() == false)
      throw new FileDoesNotExistException(fromDirectory);
    if (toDir.exists() == false)
      throw new FileDoesNotExistException(toDirectory);
    if (fromDir.isDirectory() == false)
      throw new FileFoundWhereDirectoryWasExpectedException(fromDirectory);
    if (toDir.isDirectory() == false)
      throw new FileFoundWhereDirectoryWasExpectedException(toDirectory);

    File[] files = fromDir.listFiles();
    if (files != null)
    {
      for (int i = 0; i < files.length; ++i)
      {
        File fromFile = files[i];
        File toFile = new File(toDirectory + File.separator + fromFile.getName());
        // do not move the backup directory into itself!
        if (fromFile.equals(toDir) == false)
        {
          boolean rename = true;
          for (String dontMoveFileName : dontMoveDirOrFiles)
          {
            File dontMoveFile = new File(dontMoveFileName);
            if (fromFile.equals(dontMoveFile))
            {
              rename = false;
              break;
            }
          }
          if (rename)
          {
            if (fromFile.renameTo(toFile) == false)
              throw new CouldNotRenameFileException(fromFile.getPath(), toFile.getPath());
          }
        }
      }
    }
  }

  /**
   * @return a List of Strings of all files and directories in the directory passed in. It does not
   *         search subdirectories
   * @param directoryName the name of the directory to search
   * @author George A. David
   */
  public static List<String> listFiles(String directoryName)
  {
    Assert.expect(directoryName != null);

    List<String> filePaths = new ArrayList<String>();
    File file = new File(directoryName);
    if (file.exists())
    {
      File[] files = file.listFiles();
      if (files != null)
      {
        for (int i = 0; i < files.length; ++i)
        {
          filePaths.add(files[i].getPath());
        }
      }
    }

    return filePaths;
  }

  /**
   * @return a List of String of all files and directories in the directory passed in. including
   *         all subdirectories.
   * @author Bill Darbie
   */
  public static List<String> listAllFilesAndDirectoriesFullPath(String directoryName)
  {
    Assert.expect(directoryName != null);

    List<String> allFiles = new ArrayList<String>();
    listFilesRecursively(directoryName, allFiles);

    return allFiles;
  }

  /**
   * @return a List of String of all files in the directory passed in.  Subdirectories will
   * not be listed.  The file names (not path and name) will be returned.
   * @author Bill Darbie
   */
  public static Collection<String> listAllFilesInDirectory(String directoryName)
  {
    Collection<String> files = new ArrayList<String>();

    Collection<String> filesFullPath = listAllFilesFullPathInDirectory(directoryName);
    for (String fileFullPath : filesFullPath)
    {
      int index = fileFullPath.lastIndexOf(File.separator);
      String file = fileFullPath;
      if (index != -1)
        file = fileFullPath.substring(index + 1, fileFullPath.length());
      files.add(file);
    }

    return files;
  }

  /**
   * @return a List of String of all files in the directory passed in.  Subdirectories will
   * not be listed.  The file names (not path and name) will be returned.
   * @author Wei Chin
   */
  public static Collection<String> listAllFileNamesWithoutExtensionInDirectory(String directoryName)
  {
    Collection<String> files = new ArrayList<String>();

    Collection<String> filesFullPath = listAllFilesFullPathInDirectory(directoryName);
    for (String fileFullPath : filesFullPath)
    {
      int index = fileFullPath.lastIndexOf(File.separator);
      int lastIndex = fileFullPath.lastIndexOf(".");

      if(lastIndex == -1)
        lastIndex = fileFullPath.length();
      
      String file = fileFullPath;
      if (index != -1)
        file = fileFullPath.substring(index + 1, lastIndex);
      files.add(file);
    }

    return files;
  }
  /**
   * @return a List of String of all files in the directory passed in.  Subdirectories will
   * not be listed.  The file names (not path and name) will be returned.
   * @author Bill Darbie
   */
  public static Collection<String> listAllFilesFullPathInDirectory(String directoryName)
  {
    Collection<String> allFiles = new ArrayList<String>();

    File file = new File(directoryName);
    if (file.exists() && file.isDirectory())
    {
      File[] files = file.listFiles();
      if (files != null)
      {
        for (int i = 0; i < files.length; ++i)
        {
          file = files[i];
          if (file.isFile())
            allFiles.add(file.getPath());
        }
      }
    }

    return allFiles;
  }

  /**
   * @return a List of String of all subdirectories in the directory passed in.
   * The file names (not path and name) will be returned.
   * @author Bill Darbie
   */
  public static Collection<String> listAllSubDirectoriesInDirectory(String directoryName)
  {
    Collection<String> files = new ArrayList<String>();

    if (FileUtil.exists(directoryName))
    {
      Collection<String> filesFullPath = listAllSubDirectoriesFullPathInDirectory(directoryName);
      for (String fileFullPath : filesFullPath)
      {
        int index = fileFullPath.lastIndexOf(File.separator);
        String file = fileFullPath;
        if (index != -1)
          file = fileFullPath.substring(index + 1, fileFullPath.length());
        files.add(file);
      }
    }

    return files;
  }

  /**
   * @return a List of String of all subdirectories in the directory passed in.
   * The file names (not path and name) will be returned.
   * @author Bill Darbie
   */
  public static Collection<String> listAllSubDirectoriesFullPathInDirectory(String directoryName)
  {
    Collection<String> allDirs = new ArrayList<String>();

    File file = new File(directoryName);
    if (file.exists() && file.isDirectory())
    {
      File[] files = file.listFiles();
      if (files != null)
      {
        if (files != null)
        {
          for (int i = 0; i < files.length; ++i)
          {
            file = files[i];
            if (file.isDirectory())
              allDirs.add(file.getPath());
          }
        }
      }
    }

    return allDirs;
  }

  /**
   * @author Bill Darbie
   */
  private static void listFilesRecursively(String fileName, List<String> allFiles)
  {
    Assert.expect(fileName != null);
    Assert.expect(allFiles != null);

    File file = new File(fileName);
    if (file.exists())
    {
      if (file.isDirectory() == false)
      {
        allFiles.add(file.getPath());
      }
      else
      {
        File[] files = file.listFiles();
        if (files != null)
        {
          for (int i = 0; i < files.length; ++i)
          {
            file = files[i];
            if (file.isDirectory())
              listFilesRecursively(file.getPath(), allFiles);

            allFiles.add(file.getPath());
          }
        }
      }
    }
  }

  /**
   * create the directory
   * @param path the path to create
   * @author George A. David
   * @author Matt Wharton
   */
  public static void createDirectory(String path) throws CouldNotCreateFileException
  {
    Assert.expect(path != null);

    // Only try to create the directory if it isn't already there.
    if (exists(path) == false)
    {
      File file = new File(path);

      if (file.mkdirs() == false)
      {
        throw new CouldNotCreateFileException(path);
      }
    }
  }

  /**
   * @author George A. David
   */
  public static void delete(List<String> paths) throws CouldNotDeleteFileException
  {
    Assert.expect(paths != null);

    for(String path : paths)
      delete(path);
  }

  /**
   * Delete the file or directory.  Note that java.io.File.delete() only works if a file is passed
   * to it.  This method works on either files or directories.
   * @author Bill Darbie
   */
  public static void delete(String fileOrDirectoryName) throws CouldNotDeleteFileException
  {
    Assert.expect(fileOrDirectoryName != null);

    File mainFile = new File(fileOrDirectoryName);

    if (mainFile.exists())
    {
      if (mainFile.isDirectory() == false)
      {
        if (mainFile.delete() == false)
          throw new CouldNotDeleteFileException(mainFile.getPath());
      }
      else
      {
        List<String> allFiles = listAllFilesAndDirectoriesFullPath(fileOrDirectoryName);
        for (String fName : allFiles)
        {
          File file = new File(fName);
          if (file.isDirectory())
            delete(file.getPath());
          else
          {
            if (file.delete() == false)
              throw new CouldNotDeleteFileException(file.getPath());
          }
        }
        // delete the directory now that it is empty
        if (mainFile.delete() == false)
          throw new CouldNotDeleteFileException(mainFile.getPath());
      }
    }
  }
  
    /**
   * Delete the file or directory.  Note that java.io.File.delete() only works if a file is passed
   * to it.  This method works on either files or directories.
   * - To make sure the production will not prompt the file is locked,
   *   this new function will be handle for this.
   * @author Chin Seong, KEe
   */
  public static void deleteFileOrDirectory(String fileOrDirectoryName) throws CouldNotDeleteFileException
  {
    Assert.expect(fileOrDirectoryName != null);

    File mainFile = new File(fileOrDirectoryName);

    if (mainFile.exists())
    {
      if (mainFile.isDirectory() == false)
      {
        if (mainFile.delete() == false)
          throw new CouldNotDeleteFileException(mainFile.getPath());
      }
      else
      {
        List<String> allFiles = listAllFilesAndDirectoriesFullPath(fileOrDirectoryName);
        for (String fName : allFiles)
        {
          File file = new File(fName);
          if (file.isDirectory())
            delete(file.getPath());
          else
          {
            //if (file.delete() == false)
            //  throw new CouldNotDeleteFileException(file.getPath());
            deleteMappedByteBufferFile(fName);
          }
        }
        // delete the directory now that it is empty
        if (mainFile.delete() == false)
          throw new CouldNotDeleteFileException(mainFile.getPath());
      }
    }
  }

  /**
   * This method will delete a file that is a MappedByteBuffer.
   * A MappedByteBuffer may not be immediately available to be deleted.  see Sun bug id 4715154.
   * This method will try several times to delete before throwing an exception to get around
   * the problem.
   *
   * NOTE: If your code is holding on to a reference that will not allow the MappedByteBuffer to be
   *       garbage collected, this call will always fail.
   *
   * @author Bill Darbie
   */
  public static void deleteMappedByteBufferFile(String fileName) throws CouldNotDeleteFileException
  {
    Assert.expect(fileName != null);

    File file = new File(fileName);

    // make sure this is a file and not a directory
    if (file.exists())
      Assert.expect(file.isFile());

    CouldNotDeleteFileException ex = null;
    int numAttempts = 0;
    do
    {
      try
      {
        // the garbage collector is what ultimately frees up a MappedByteBuffer (Sun bug id 4715154) - so call it first
        // then try to delete the file.
        MemoryUtil.garbageCollect();

        ex = null;
        delete(fileName);
      }
      catch (CouldNotDeleteFileException dex)
      {
        // this could fail if the MappedByteBuffer was not freed yet, so sleep for a little while and try again.
        ex = dex;
        ++numAttempts;
        try
        {
          Thread.sleep(200);
        }
        catch (InterruptedException iex)
        {
          // do nothing
        }
      }
    }
    while ((ex != null) && (numAttempts < 3));

    if (ex != null)
      throw ex;
  }

  /**
   * Delete the contents of the directory but not the directory itself.
   * See also delete().
   * @author Vincent Wong
   */
  public static void deleteDirectoryContents(String dirName) throws CouldNotDeleteFileException
  {
    Assert.expect(dirName != null);

    deleteDirectoryContents(dirName, new ArrayList<String>(0));
  }

  /**
   * Delete all contents of the directory passed in with the exception of the
   * files or directories in the dontDeleteFilesOrDirectories List.
   * @author Bill Darbie
   */
  public static void deleteDirectoryContents(String dirName,
                                             List<String> dontDeleteFilesOrDirectories) throws CouldNotDeleteFileException
  {
    Assert.expect(dirName != null);

    File dir = new File(dirName);
    if (dir.exists() && dir.isDirectory())
    {
      Set<File> dontDeleteSet = new HashSet<File>();
      for (String dontDeleteDirOrFile : dontDeleteFilesOrDirectories)
      {
        File file = new File(dontDeleteDirOrFile);
        dontDeleteSet.add(file);
        // if it is a directory then include all of the files and directories in it so they are not deleted
        if (file.isDirectory())
        {
          for (String theFile : FileUtil.listAllFilesAndDirectoriesFullPath(dontDeleteDirOrFile))
            dontDeleteSet.add(new File(theFile));
        }
      }

      List<String> allFiles = listAllFilesAndDirectoriesFullPath(dirName);
      for (String fName : allFiles)
      {
        File file = new File(fName);

        if (file.isDirectory())
        {
          // delete the directory
          if (dontDeleteSet.contains(file) == false)
            delete(file.getPath());
        }
        else
        {
          // delete the file
          if (dontDeleteSet.contains(file) == false)
          {
            if (file.delete() == false)
              throw new CouldNotDeleteFileException(file.getPath());
          }
        }
      }
    }
  }

  /**
   * Rename the file to the new name.  If a file with the new name already exists it will
   * be replaced.  Note that java.io.File.renameTo() will fail rather then replace an existing
   * file.
   *
   * @author Bill Darbie
   */
  public static void rename(String fromFileOrDir, String toFileOrDir) throws CouldNotRenameFileException, CouldNotDeleteFileException, FileNotFoundException
  {
    Assert.expect(fromFileOrDir != null);
    Assert.expect(toFileOrDir != null);

    File from = new File(fromFileOrDir);
    // Assert.expect(from.exists());
    if (from.exists() == false)
      throw new FileNotFoundException(fromFileOrDir);
    File to = new File(toFileOrDir);
    if (to.exists())
      delete(toFileOrDir);

    if (from.renameTo(to) == false)
      throw new CouldNotRenameFileException(fromFileOrDir, toFileOrDir);
  }

  /**
   * This method will cancel the currently running zip.  The incomplete zip file will be left with
   * whatever files it already had in it.
   * @author Erica Wheatcroft
   */
  public static void cancelZip()
  {
    _cancelZip = true;
  }

  /**
   * See the method description below - this one is the same except it uses the default compression setting.
   * @author Bill Darbie
   */
  public static void zip(String inputFileOrDirectoryName,
                         String zipName,
                         String relativePath) throws FileDoesNotExistException,
                                                     CouldNotCreateFileException,
                                                     NotEnoughDiskSpaceException
  {
    zip(inputFileOrDirectoryName, zipName, relativePath, Deflater.DEFAULT_COMPRESSION);
  }

  /**
   * Zip the contents of fileOrDirectoryName and put the result into zipName.  If fileOrDirectoryName
   * is a file then just that file will be zipped.  If fileOrDirectoryName is a directory then
   * all the files and directories (recursively) will be zipped.  The zip will contain the path
   * of the files relative to the relativePath parameter.
   *
   * @param inputFileOrDirectoryName is the name of the file or directory of what should be zipped
   * @param zipName is the zip file that will be created
   * @param relativePath is the relativePath that the contents of the zip will be relative to.  This
   *                     must be the path contained by inputFileOrDirectoryName.  An empty string will
   *                     cause the full path to be put into the zip (not recommended)
   * @param compressionLevel is the level of compression (1-9) 1 is the least compressed and fastest, 9 is
   *                         the best compression and slowest
   *
   * @author Bill Darbie
   */
  public static void zip(String inputFileOrDirectoryName,
                         String zipName,
                         String relativePath,
                         int compressionLevel) throws FileDoesNotExistException,
                                                      CouldNotCreateFileException,
                                                      NotEnoughDiskSpaceException
  {
    Assert.expect(inputFileOrDirectoryName != null);
    Assert.expect(zipName != null);
    Assert.expect(relativePath != null);
    Assert.expect(((compressionLevel > 0) && (compressionLevel < 10)) || (compressionLevel == Deflater.DEFAULT_COMPRESSION));

    _cancelZip = false;
    inputFileOrDirectoryName = inputFileOrDirectoryName.replace('\\', '/');

    if (relativePath.length() > 0)
    {
      relativePath = relativePath.replace('\\', '/');
      String upperRelativePath = relativePath.toUpperCase();
      String upperName = inputFileOrDirectoryName.toUpperCase();
      Assert.expect(upperName.startsWith(upperRelativePath));
      char lastChar = relativePath.charAt(relativePath.length() - 1);
      if (lastChar != '/')
        relativePath = relativePath + '/';
    }

    boolean exception = false;
    File inputFile = new File(inputFileOrDirectoryName);
    if (inputFile.exists() == false)
      throw new FileDoesNotExistException(inputFileOrDirectoryName);
    File outFile = new File(zipName);
    if ((outFile.exists()) && (outFile.isFile() == false))
      throw new CouldNotCreateFileException(zipName);

    // check write permission on parent directory so we can display a more
    // accurate error message when a FileNotFoundException is caught below
    boolean canWriteToParentDir = false;
    File outFileDir = new File(outFile.getParent());
    if (outFileDir != null)
      if (outFileDir.canWrite() == true)
        canWriteToParentDir = true;

    ZipOutputStream os = null;
    try
    {
      FileOutputStream fos = null;
      try
      {
        fos = new FileOutputStream(outFile);
      }
      catch(FileNotFoundException ex)
      {
        exception = true;

        if (canWriteToParentDir == false && outFileDir != null)
        {
          CouldNotCreateFileException cex = new CouldNotCreateFileException(zipName);
          cex.initCause(ex);
          throw cex;
        }
        else
        {
          FileDoesNotExistException exc = new FileDoesNotExistException(zipName);
          exc.initCause(ex);
          throw exc;
        }
      }
      os = new ZipOutputStream(new BufferedOutputStream(fos, _ZIP_BUFFER));
      os.setLevel(compressionLevel);

      if (_cancelZip)
        return;

      zip(inputFileOrDirectoryName, zipName, os, relativePath, false);
    }

    finally
    {
      try
      {
        if (os != null)
          os.close();
        if ((exception) || (_cancelZip))
        {
          if (FileUtil.exists(zipName))
            delete(zipName);
        }
      }
      catch(IOException ioe)
      {
        // throw NotEnoughDiskSpaceException for all IOExceptions for now,
        // until a new reason to throw a different exception arises
        if (exception == false)
            throw new NotEnoughDiskSpaceException(outFile.getParent(), outFile.getName());
        else
          ioe.printStackTrace();
      }
    }
  }

  /**
   * See the method description below - this one is the same except it uses the default compression setting.
   * @author Andy Mechtenberg
   */
  public static void zip(List<String> fileOrDirectoryNames, String zipName, String relativePath, boolean excludeHiddenFiles) throws CouldNotCreateFileException
  {
    _excludeHiddenFiles = excludeHiddenFiles;
    try
    {
      zip(fileOrDirectoryNames, zipName, relativePath, Deflater.DEFAULT_COMPRESSION);
    }
    finally
    {
      _excludeHiddenFiles = false;  // revert to defaults
    }
  }

  /**
   * See the method description below - this one is the same except it uses the default compression setting.
   * @author Bill Darbie
   */
  public static void zip(List<String> fileOrDirectoryNames, String zipName, String relativePath) throws CouldNotCreateFileException
  {
    zip(fileOrDirectoryNames, zipName, relativePath, Deflater.DEFAULT_COMPRESSION);
  }

  /**
   * Zip the contents of fileOrDirectoryNames passed in through the List of Strings
   * and put the result into zipName.  If fileOrDirectoryName
   * is a file then just that file will be zipped.  If fileOrDirectoryName is a directory then
   * all the files and directories (recursively) will be zipped.  The files in the zip file will
   * all be relative to the path specified in relativePath.  The path specified by relativePath must
   * be in the name of all files and directories in the fileOrDirectoryNames List.
   *
   * @param fileOrDirectoryNames is the list of names of the files or directories that will be zipped
   * @param zipName is the zip file that will be created
   * @param relativePath is the relativePath that the contents of the zip will be relative to.  This
   *                     must be the path contained by inputFileOrDirectoryName.  An empty string will
   *                     cause the full path to be put into the zip (not recommended)
   * @param compressionLevel is the level of compression (1-9) 1 is the least compressed and fastest, 9 is
   *                         the best compression and slowest
   *
   * @author Bill Darbie
   */
  public static void zip(List<String> fileOrDirectoryNames, String zipName, String relativePath, int compressionLevel) throws CouldNotCreateFileException
  {
    Assert.expect(fileOrDirectoryNames != null);
    Assert.expect(zipName != null);
    Assert.expect(relativePath != null);
    Assert.expect(((compressionLevel > 0) && (compressionLevel < 10)) || (compressionLevel == Deflater.DEFAULT_COMPRESSION));

    _cancelZip = false;
    relativePath = relativePath.replace('\\', '/');
    char lastChar = relativePath.charAt(relativePath.length() - 1);
    if (lastChar != '/')
      relativePath += '/';

    boolean exception = false;
    File outFile = new File(zipName);
    if ((outFile.exists()) && (outFile.isFile() == false))
      throw new CouldNotCreateFileException(zipName);

    ZipOutputStream os = null;
    try
    {
      FileOutputStream fos = new FileOutputStream(zipName);
      os = new ZipOutputStream(new BufferedOutputStream(fos, _ZIP_BUFFER));
      // 1 - 9, 1 is fast with worst compression, 9 is slow with best compression
      os.setLevel(compressionLevel);

      for (String fileOrDirectoryName : fileOrDirectoryNames)
      {
        if (_cancelZip)
          return;

        File inputFile = new File(fileOrDirectoryName);
        if (inputFile.exists() == false)
          throw new FileDoesNotExistException(fileOrDirectoryName);

        fileOrDirectoryName = fileOrDirectoryName.replace('\\', '/');

        if (inputFile.isDirectory())
        {
          // if its a directory make sure that there exists a '/' at the end of the file path.
          lastChar = fileOrDirectoryName.charAt(fileOrDirectoryName.length() - 1);
          if (lastChar != '/')
            fileOrDirectoryName += '/';
        }

        String upperName = fileOrDirectoryName.toUpperCase();
        String upperRelativePath = relativePath.toUpperCase();
        Assert.expect(upperName.startsWith(upperRelativePath));

        zip(fileOrDirectoryName, zipName, os, relativePath, false);

      }
    }
    catch(IOException ioe)
    {
      exception = true;
      CouldNotCreateFileException ex = new CouldNotCreateFileException(zipName);
      ex.initCause(ioe);
      throw ex;
    }
    finally
    {
      try
      {
        if (os != null)
          os.close();
      }
      catch(ZipException ze)
      {
        ze.printStackTrace();
      }
      catch(IOException ioe)
      {
        ioe.printStackTrace();
      }
      try
      {
        if ((exception) || (_cancelZip))
        {
          if (FileUtil.exists(zipName))
            delete(zipName);
        }
      }
      catch(CouldNotDeleteFileException ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   * This method can be called recursively.  The zip method about cannot because it handles opening
   * and closing the zipped file.  So it does that and calls this method to do the work.
   * @author Bill Darbie
   */
  private static void zip(String inputFileOrDirectoryName,
                          String zipName,
                          ZipOutputStream os,
                          String relativePath,
                          boolean isRelativePathNew) throws CouldNotCreateFileException,
                                                            FileDoesNotExistException
  {
    Assert.expect(inputFileOrDirectoryName != null);
    Assert.expect(os != null);
    Assert.expect(relativePath != null);

    File inputFile = new File(inputFileOrDirectoryName);

    if ((inputFile.isFile()) && (zipName.equals(inputFileOrDirectoryName) == false))// && ((_excludeHiddenFiles == false) || (inputFile.isHidden() == false)))
      writeZipFile(inputFileOrDirectoryName, zipName, os, relativePath, isRelativePathNew);
    else if (inputFile.isDirectory())
    {
      File[] files = inputFile.listFiles();
      if (files != null)
      {
        for (int i = 0; i < files.length; ++i)
        {
          if (_cancelZip)
            return;

          File file = files[i];
          String fileName = file.getAbsolutePath();
          fileName = fileName.replace('\\', '/');
          if (file.isDirectory())
          {
            if (_cancelZip)
              return;

            // recursively call this method
            zip(fileName, zipName, os, relativePath, isRelativePathNew);
          }

          if ((fileName.equals(zipName) == false))// && ((_excludeHiddenFiles == false) || (file.isHidden() == false)))
          {
            writeZipFile(fileName, zipName, os, relativePath, isRelativePathNew);
          }
        }
      }
    }
  }

  /**
   * adds files to a zip file with a new relative path.
   * for instance it can add c:/temp/temp.txt to a zip file with a
   * relative path of new/relative/path/ so when the file is unzipped it
   * be placed in <current dir>/new/relative/path/temp.txt
   * @author George A. David
   */
  public static void zipWithNewRelativePath(Map<String, List<String>> newRelativePathToPathsMap, String zipName, boolean append) throws CouldNotCreateFileException
  {
    Assert.expect(newRelativePathToPathsMap != null);
    Assert.expect(zipName != null);

    _cancelZip = false;

    boolean exception = false;
    File outFile = new File(zipName);
    if ((outFile.exists()) && (outFile.isFile() == false))
      throw new CouldNotCreateFileException(zipName);

    ZipOutputStream os = null;
    try
    {
      FileOutputStream fos = new FileOutputStream(zipName, append);
      os = new ZipOutputStream(new BufferedOutputStream(fos, _ZIP_BUFFER));
      // 1 - 9, 1 is fast with worst compression, 9 is slow with best compression
      os.setLevel(Deflater.DEFAULT_COMPRESSION);

      for (Map.Entry<String, List<String>> mapEntry : newRelativePathToPathsMap.entrySet())
      {
        String relativePath = mapEntry.getKey();
        // the relative path could be empty, this just means to place
        // the files in the zip without a path.
        if(relativePath.length() > 0)
        {
          relativePath = relativePath.replace('\\', '/');
          char lastChar = relativePath.charAt(relativePath.length() - 1);
          if (lastChar != '/')
            relativePath += '/';
        }
        List<String> fileOrDirectoryNames = mapEntry.getValue();

        for (String fileOrDirectoryName : fileOrDirectoryNames)
        {
          if (_cancelZip)
            return;

          File inputFile = new File(fileOrDirectoryName);
          if (inputFile.exists() == false)
            throw new FileDoesNotExistException(fileOrDirectoryName);

          fileOrDirectoryName = fileOrDirectoryName.replace('\\', '/');

          if (inputFile.isDirectory())
          {
            // if its a directory make sure that there exists a '/' at the end of the file path.
            char lastChar = fileOrDirectoryName.charAt(fileOrDirectoryName.length() - 1);
            if (lastChar != '/')
              fileOrDirectoryName += '/';
          }

          zip(fileOrDirectoryName, zipName, os, relativePath, true);
        }
      }
    }
    catch(IOException ioe)
    {
      exception = true;
      CouldNotCreateFileException ex = new CouldNotCreateFileException(zipName);
      ex.initCause(ioe);
      throw ex;
    }
    finally
    {
      try
      {
        if (os != null)
          os.close();
      }
      catch(ZipException ze)
      {
        ze.printStackTrace();
      }
      catch(IOException ioe)
      {
        ioe.printStackTrace();
      }
      try
      {
        if ((exception) || (_cancelZip))
        {
          if (FileUtil.exists(zipName))
            delete(zipName);
        }
      }
      catch(CouldNotDeleteFileException ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   * Write fileName into the os stream in a zipped format.
   * @author Bill Darbie
   */
  private static void writeZipFile(String fileOrDirectoryName,
                                   String zipFileName,
                                   ZipOutputStream os,
                                   String relativePath,
                                   boolean isRelativePathNew) throws CouldNotCreateFileException,
                                                                     FileDoesNotExistException
  {
    Assert.expect(fileOrDirectoryName != null);
    Assert.expect(os != null);
    Assert.expect(relativePath != null);

    BufferedInputStream is = null;
    byte data[] = new byte[_ZIP_BUFFER];

    if (_cancelZip)
      return;

    try
    {
      File file = new File(fileOrDirectoryName);
      if (file.isFile())
      {
        if (_excludeHiddenFiles && file.isHidden())
          return;
        FileInputStream fis = null;
        try
        {
          fis = new FileInputStream(fileOrDirectoryName);
        }
        catch(FileNotFoundException ex)
        {
          FileDoesNotExistException fdne = new FileDoesNotExistException(fileOrDirectoryName);
          fdne.initCause(ex);
          throw fdne;
        }
        is = new BufferedInputStream(fis, _ZIP_BUFFER);
      }

      String relativeName = null;
      if(isRelativePathNew)
        relativeName = relativePath + file.getName();
      else
      {
        String upperName = fileOrDirectoryName.toUpperCase();
        String upperRelativePath = relativePath.toUpperCase();
        Assert.expect(upperName.startsWith(upperRelativePath));
        relativeName = fileOrDirectoryName.substring(relativePath.length(), fileOrDirectoryName.length());
      }
      Assert.expect(relativeName != null);
      if ((file.isDirectory()) && (relativeName.charAt(relativeName.length() - 1) != '/'))
        relativeName += '/';

      ZipEntry entry = new ZipEntry(relativeName);
      // now preserve the file time stamp in the zip entry. This functionality was added to fix CR12587
      long zipFileTimeStamp = file.lastModified();
      entry.setTime(zipFileTimeStamp);
      try
      {
        os.putNextEntry(entry);
      }
      catch(IOException ex)
      {
        CouldNotCreateFileException cncfe = new CouldNotCreateFileException(zipFileName);
        cncfe.initCause(ex);
        throw cncfe;
      }
      if (file.isFile())
      {
        try
        {
          int count = is.read(data, 0, _ZIP_BUFFER);
          while(count != -1)
          {
            os.write(data, 0, count);
            count = is.read(data, 0, _ZIP_BUFFER);
          }
        }
        catch(IOException ex)
        {
          CouldNotCreateFileException cncfe = new  CouldNotCreateFileException(zipFileName);
          cncfe.initCause(ex);
          throw cncfe;
        }
      }
    }
    finally
    {
      try
      {
        if (is != null)
          is.close();
      }
      catch(IOException ioe)
      {
        ioe.printStackTrace();
      }
    }
  }

  /**
   * Unzip the zipFile into unzipDirectory.  The unzip will create any directories that it needs to.
   * If a file already exists that is in the zipped file, the unzip will overwrite it.
   *
   * @param zipFile is the file to be unzipped
   * @param unzipDirectory is the directory to unzip the file into
   *
   * @author Andy Mechtenberg
   */
  public static void unZip(String zipFile,
                           String unzipDirectory,
                           boolean retainOriginalFileTimeStamps) throws CouldNotCreateFileException,
                                                                        FileDoesNotExistException,
                                                                        CouldNotCreateDirectoryException,
                                                                        BadFileFormatException,
                                                                        FileFoundWhereDirectoryWasExpectedException
  {
    Assert.expect(zipFile != null);
    Assert.expect(unzipDirectory != null);

    long startTime = System.currentTimeMillis();

    unzipDirectory = unzipDirectory.replace('\\', '/');

    File inFile = new File(zipFile);
    if (inFile.exists() == false)
      throw new FileDoesNotExistException(zipFile);
    File outFile = new File(unzipDirectory);
    if (outFile.exists() && outFile.isDirectory() == false)
      throw new FileFoundWhereDirectoryWasExpectedException(unzipDirectory);

    if (unzipDirectory.length() > 0)
    {
      char lastChar = unzipDirectory.charAt(unzipDirectory.length() - 1);
      if (lastChar != '/')
        unzipDirectory = unzipDirectory + '/';
    }

    // new code using ZipInputStream
    ZipInputStream zis = null;
    try
    {
      BufferedOutputStream dest = null;
      FileInputStream fis = null;
      try
      {
        fis = new FileInputStream(zipFile);
      }
      catch (FileNotFoundException ex)
      {
        CouldNotCreateFileException cncfe = new CouldNotCreateFileException(zipFile);
        cncfe.initCause(ex);
        throw cncfe;
      }

      zis = new ZipInputStream(new BufferedInputStream(fis));
      ZipEntry entry;
      try
      {
        entry = zis.getNextEntry();
      }
      catch (IOException ex)
      {
        CouldNotCreateFileException cncfe = new CouldNotCreateFileException(zipFile);
        cncfe.initCause(ex);
        throw cncfe;
      }

      while (entry != null)
      { 
        if(entry.getName().matches("algorithmLearning/Temp/"))
          System.out.println("" + entry.getName());
         //Jack Hwee - fix to unzip multiple directories
        while (entry != null && entry.isDirectory() == true )
        {
           try
           {
             entry = zis.getNextEntry();
           }
           catch (IOException ex)
           {
             CouldNotCreateFileException cncfe = new CouldNotCreateFileException(zipFile);
             cncfe.initCause(ex);
             throw cncfe;
           }
        }
        
        if(entry != null)
        {
          int count;
          byte data[] = new byte[_ZIP_BUFFER];
          // write the files to the disk
          long zipFileTimeStamp = entry.getTime();
          String fullName = unzipDirectory + entry.getName();
          int index = fullName.lastIndexOf('/');

          if (index != -1)
          {
            String path = fullName.substring(0, index);
            File pathFile = new File(path);
            if (pathFile.exists() == false)
            {
              if (pathFile.mkdirs() == false)
                throw new CouldNotCreateDirectoryException(path);
            }
          }

          FileOutputStream fos = null;
          try
          {
            fos = new FileOutputStream(fullName);
          }
          catch (FileNotFoundException ex)
          {
            FileDoesNotExistException fdne = new FileDoesNotExistException(fullName);
            fdne.initCause(ex);
            throw fdne;
          }

          dest = new BufferedOutputStream(fos, _ZIP_BUFFER);
          try
          {
            count = zis.read(data, 0, _ZIP_BUFFER);
          }
          catch (IOException ex2)
          {
            CouldNotCreateFileException cncfe = new CouldNotCreateFileException(zipFile);
            cncfe.initCause(ex2);
            throw cncfe;
          }
          try
          {
            while (count != -1)
            {
              try
              {
                dest.write(data, 0, count);
              }
              catch (IOException ex3)
              {
                CouldNotCreateFileException cncfe = new CouldNotCreateFileException(zipFile);
                cncfe.initCause(ex3);
                throw cncfe;
              }
              try
              {
                count = zis.read(data, 0, _ZIP_BUFFER);
              }
              catch (IOException ex2)
              {
                CouldNotCreateFileException cncfe = new CouldNotCreateFileException(zipFile);
                cncfe.initCause(ex2);
                throw cncfe;
              }
            }
          }
          finally
          {
            try
            {
              dest.flush();
              dest.close();
            }
            catch (IOException ex)
            {
              ex.printStackTrace();
            }
          }
          // now restore the file to the time that the file had when the zip was created.
          if (retainOriginalFileTimeStamps)
          {
            // now set the time. This needs to be done after the stream has been closed or the file time stamp still
            // gets updated with the time the file stream was closed. - EEW
            File file = new File(fullName);
            file.setLastModified(zipFileTimeStamp);
          }

          try
          {
            entry = zis.getNextEntry();
          }
          catch (IOException ex)
          {
            CouldNotCreateFileException cncfe = new CouldNotCreateFileException(zipFile);
            cncfe.initCause(ex);
            throw cncfe;
          }
        }
      }
    }
    finally
    {
      try
      {
        if (zis != null)
          zis.close();
      }
      catch (IOException ioe)
      {
        ioe.printStackTrace();
      }
      long endTime = System.currentTimeMillis();
//      System.out.println("Unzip Time (new method): " + (endTime - startTime));
    }
  }


  /**
   * Get the list of file names and directory names zipped in the zip file.
   *
   * @param zipFile is the file to be examined.
   * @author Vincent Wong
   */
  public static List<String> getZipFileContents(String zipFile) throws FileDoesNotExistException,
                                                               BadFileFormatException
  {
    Assert.expect(zipFile != null);

    List<String> content = new ArrayList<String>();

    File inFile = new File(zipFile);

    if (inFile.exists() == false)
      throw new FileDoesNotExistException(zipFile);

    ZipEntry entry = null;
    ZipFile zipfile = null;
    try
    {
      zipfile = new ZipFile(zipFile);
    }
    catch(ZipException ze)
    {
      BadFileFormatException ex = new BadFileFormatException(zipFile);
      ex.initCause(ze);
      throw ex;
    }
    catch(IOException ex)
    {
      FileDoesNotExistException fdne = new FileDoesNotExistException(zipFile);
      fdne.initCause(ex);
      throw fdne;
    }
    Enumeration enumeration = zipfile.entries();

    while (enumeration.hasMoreElements())
    {
      entry = (ZipEntry)enumeration.nextElement();
      content.add(entry.getName());
    }

    try
    {
      zipfile.close();
    }
    catch(IOException ex)
    {
      ex.printStackTrace();
    }

    return content;
  }

  /**
   * Return the extension with the dot.
   * @author Vincent Wong
   */
  public static String getExtension(String fileName)
  {
    Assert.expect(fileName != null);

    File file = new File(fileName);
    String fileNameOnly = file.getName();
    int index = fileNameOnly.lastIndexOf('.');

    if ( index == -1 )
      return ""; // no extension
    else
      return fileNameOnly.substring(index, fileNameOnly.length());
  }

  /**
   * @author Patrick Lacz
   */
  public static String getNameWithoutPath(String fileNameWithPath)
  {
    Assert.expect(fileNameWithPath != null);

    int index = fileNameWithPath.lastIndexOf(File.separator);
    String file = fileNameWithPath;
    if (index != -1)
      file = fileNameWithPath.substring(index + 1, fileNameWithPath.length());

    return file;
  }

  /**
   * @author Patrick Lacz
   */
  public static String getPathWithoutFileName(String fileNameWithPath)
  {
    Assert.expect(fileNameWithPath != null);

    int index = fileNameWithPath.lastIndexOf(File.separator);
    String path = fileNameWithPath;
    if (index != -1)
      path = fileNameWithPath.substring(0, index);

    return path;
  }

  /**
   * Return the base name of the file (without the extension).
   * @author Vincent Wong
   */
  public static String getNameWithoutExtension(String fileName)
  {
    Assert.expect(fileName != null);

    File file = new File(fileName);
    String fileNameOnly = file.getName();
    int index = fileNameOnly.lastIndexOf('.');

    if ( index == -1 )
      return fileNameOnly;
    else
      return fileNameOnly.substring(0, index);
  }

  /**
   * Return the base name with the leading path name of the file
   * (without the extension).
   * @author Vincent Wong
   */
  public static String getNameWithoutExtensionWithPath(String fileName)
  {
    Assert.expect(fileName != null);

    File file = new File(fileName);
    String fileNameOnly = file.getName();
    int index = fileNameOnly.lastIndexOf('.');
    String parent = file.getParent();

    if ( parent == null ) // e.g. fileName = "vincent.tmp"
    {
      if ( index == -1 ) // e.g. fileName = "c:/temp/vincent"
        return fileNameOnly; // fileNameOnly = "c:/temp/vincent"
      else // e.g. fileName = "vincent.tmp"
        return fileNameOnly.substring(0, index); // fileNameOnly = "vincent"
    }
    else // e.g. fileName = "c:/temp/vincent.tmp"
    {
      if ( parent.endsWith(File.separator) ) // e.g. parent = "c:/"
        parent = parent.substring(0, parent.length()-1); // e.g. parent = "c:"

      if ( index == -1 )
        return parent + File.separator + fileNameOnly;
      else
        return parent + File.separator + fileNameOnly.substring(0, index);
    }
  }

  /**
   * Return the absolute path of fileName.
   *
   * @author Vincent Wong
   */
  public static String getAbsolutePath(String fileName)
  {
    Assert.expect(fileName != null);

    File file = new File(fileName);

    return file.getAbsolutePath();
  }

  /**
   * Return the parent directory name of fileName. If the input fileName is a
   * relative path, the output parent directory will be a relative path, too.
   * The method returns an empty string if the fileName does not have a parent.
   *
   * @author Vincent Wong
   */
  public static String getParent(String fileName)
  {
    Assert.expect(fileName != null);

    File file = new File(fileName);
    String parent = file.getParent();

    if (parent == null)
      return "";
    else
      return parent;
  }

  /**
   * Return the absolute path of the parent directory of fileName.
   * The method returns an empty string if the fileName does not have a parent.
   *
   * @author Vincent Wong
   */
  public static String getAbsoluteParent(String fileName)
  {
    Assert.expect(fileName != null);

    File file = new File(fileName);
    String absolutePath = file.getAbsolutePath();

    file = new File(absolutePath);
    String parent = file.getParent();

    if (parent == null)
      return "";
    else
      return parent;
  }

  /**
   * Return the fileName without a file separator at the end, if any.
   *
   * @author Vincent Wong
   */
  public static String removeEndFileSeparator(String fileName)
  {
    Assert.expect(fileName != null);

    String outputFileName = null;

    if (fileName.endsWith("/"))
      outputFileName = fileName.substring(0, fileName.length()-1);
    else if (fileName.endsWith("\\"))
      outputFileName = fileName.substring(0, fileName.length()-1);
    else
      outputFileName = fileName;

    return outputFileName;
  }

  /**
   * @author Bill Darbie
   */
  public static int getFloppySizeInBytes()
  {
    return _FLOPPY_SIZE;
  }

  /**
   * Creates the directory named by this abstract pathname.
   * @author Bill Darbie
   */
  public static void mkdir(String dirName) throws FileFoundWhereDirectoryWasExpectedException, CouldNotCreateFileException
  {
    File file = new File(dirName);
    if (file.exists() && (file.isDirectory() == false))
      throw new FileFoundWhereDirectoryWasExpectedException(dirName);

    boolean createdDir = file.mkdir();
    if (createdDir == false)
      throw new CouldNotCreateFileException(dirName);
  }


  /**
   * Creates the directory named by this abstract pathname, including any
   * necessary but nonexistent parent directories.
   * @author Bill Darbie
   */
  public static void mkdirs(String dirName) throws FileFoundWhereDirectoryWasExpectedException, CouldNotCreateFileException
  {
    File file = new File(dirName);
    if (file.exists() && (file.isDirectory() == false))
      throw new FileFoundWhereDirectoryWasExpectedException(dirName);

    boolean createdDir = file.mkdirs();
    if ((createdDir == false) && (FileUtil.exists(dirName) == false))
      throw new CouldNotCreateFileException(dirName);
  }

  /**
   * Reads an entire file into a byte array
   * @return the byte array read.
   * @author George A. David
   */
  public static byte[] readBytes(String filePath) throws FileNotFoundException, IOException
  {
    Assert.expect(filePath != null);

    File file = new File(filePath);
    byte[] fileBytes = null;

    BufferedInputStream inputStream = null;
    FileInputStream fileInputStream = fileInputStream = new FileInputStream(file);

    try
    {
      inputStream = new BufferedInputStream(fileInputStream);

      fileBytes = new byte[(int)file.length()];

      int numberOfBytesRead = inputStream.read(fileBytes);
      Assert.expect((int)file.length() == numberOfBytesRead);
    }
    finally
    {
      if (inputStream != null)
      {
        try
        {
          inputStream.close();
        }
        catch(IOException ioe)
        {
          // do nothing
        }
      }
    }

    Assert.expect(fileBytes != null);
    return fileBytes;
  }

  /**
   * Given a zip file path, calculate the size of the uncompressed data.
   *
   * @param zipFilePath the path to the zip file.
   * @author George A. David
   */
  public static long getUncompressedZipFileSizeInBytes(String zipFilePath) throws BadFileFormatException,
                                                                                  FileDoesNotExistException
  {
    Assert.expect(zipFilePath != null);

    ZipEntry entry = null;
    ZipFile zipfile = null;

    long sizeInBytes = 0;
    try
    {
      zipfile = new ZipFile(zipFilePath);
    }
    catch(ZipException ze)
    {
      BadFileFormatException ex = new BadFileFormatException(zipFilePath);
      ex.initCause(ze);
      throw ex;
    }
    catch(IOException ex)
    {
      FileDoesNotExistException fdne = new FileDoesNotExistException(zipFilePath);
      fdne.initCause(ex);
      throw fdne;
    }
    Enumeration enumeration = zipfile.entries();

    while (enumeration.hasMoreElements())
    {
      entry = (ZipEntry)enumeration.nextElement();
      long size = entry.getSize();
      if (size == -1)
      {
        throw new BadFileFormatException(zipFilePath);
      }
      sizeInBytes += size;
    }

    try
    {
      zipfile.close();
    }
    catch(IOException ex)
    {
      ex.printStackTrace();
    }

    return sizeInBytes;
  }

  /**
   * List the directories directly under the specified path. This does not search subdirectories.
   *
   * @param path the path to read the directories from
   * @author George A. David
   */
  public static List<String> listDirectories(String path)
  {
    Assert.expect(path != null);

    List<String> directories = new ArrayList<String>();
    File directory = new File(path);
    File[] files = directory.listFiles();

    if (files != null)
    {
      for (int i = 0; i < files.length; ++i)
      {
        if (files[i].isDirectory())
        {
          directories.add(files[i].getAbsolutePath());
        }
      }
    }

    return directories;
  }

  /**
   * get the size of the file in bytes
   * @param filePath the path to the file to calculate the size
   * @return the size of the file in bytes
   * @author George A. David
   */
  public static long getFileSizeInBytes(String filePath)
  {
    Assert.expect(filePath != null);

    File file = new File(filePath);
    Assert.expect(file.exists());

    return file.length();
  }


  /**
   * Calculate the size of the contents of the path recursively
   * @param path the path to calculate the size
   * @return the size of the path in bytes
   * @author George A. David
   */
  public static long getSizeOfDirectoryContentsInBytes(String path)
  {
    Assert.expect(path != null);

    List<String> allFiles = new ArrayList<String>();
    listFilesRecursively(path, allFiles);

    int size = 0;
    for (String file : allFiles)
    {
      size += getFileSizeInBytes(file);
    }

    return size;
  }


  /**
   * Determines the available disk space. It is windows specific This uses JNI to call into
   * the win32 api.
   *
   * @author George A. David
   */
  public static long getAvailableDiskSpaceInBytes(String path)
  {
    Assert.expect(path != null);

    return nativeGetAvailableDiskSpaceInBytes(path);
  }

  /**
   * @author George A. David
   */
  private native static long nativeGetAvailableDiskSpaceInBytes(String path);


  /**
   * This method will create a temporary file name from the fileName passed in.
   * If a file called hello.txt was passed in, this method will return hello.txt.temp1
   * assuming a file does not already exist with that name.  If one does already exist,
   * the number at the end of temp will be incremented to a value that yields a new filename
   * that does not already exist.
   *
   * @return the name of a temporary file that does not already exist.  The name will
   * be the same as the one passed in, with some unique extension added on.
   * @author Bill Darbie
   */
  public static String getTempFileName(String fileName)
  {
    Assert.expect(fileName != null);
    String extension = ".temp";
    int i = 1;
    String tempFileName = fileName + extension + Integer.toString(i);
    File file = new File(tempFileName);
    while (file.exists())
    {
      ++i;
      tempFileName = fileName + extension + Integer.toString(i);
      file = new File(fileName + extension + Integer.toString(i));
    }

    return tempFileName;
  }

  /**
   * Save the object passed in as a serialized file with the name passed in.  If this method fails
   * to write the new file, any pre-existing file will be left in its original state.
   * @author Bill Darbie
   */
  public static void saveObjectToSerializedFile(Object object, String fileName) throws CouldNotRenameFileException,
                                                                                       CouldNotDeleteFileException,
                                                                                       CouldNotCreateFileException,
                                                                                       FileNotFoundException
  {
    Assert.expect(object != null);
    Assert.expect(fileName != null);

    // save original file to a temporary file first, in case the save fails

    String tempFileName = getTempFileName(fileName);
    if (FileUtil.exists(fileName))
      FileUtil.rename(fileName, tempFileName);

    ObjectOutput os = null;
    try
    {
      FileOutputStream fos = new FileOutputStream(fileName);
      BufferedOutputStream bos = new BufferedOutputStream(fos);
      os = new ObjectOutputStream(bos);

      // wpd - timing
      //      long startTime = System.currentTimeMillis();
      os.writeObject(object);
      //      long stopTime = System.currentTimeMillis();
      //      long time = stopTime - startTime;
      //      if (UnitTest.unitTesting() == false)
      //      {
      //        System.out.println("time to save serialized file in milliseconds: " + time);
      //      }
    }
    catch (IOException ioe)
    {
      // remove the partially written file, but must close the stream first
      try
      {
        if (os != null)
          os.close();
        if (FileUtil.exists(fileName))
          FileUtil.delete(fileName);
        if (FileUtil.exists(tempFileName))
          FileUtil.rename(tempFileName, fileName);
      }
      catch(IOException fioe)
      {
        fioe.printStackTrace();
      }
      CouldNotCreateFileException ex = new CouldNotCreateFileException(fileName);
      ex.initCause(ioe);
      throw ex;
    }
    finally
    {
      try
      {
        if (os != null)
          os.close();
      }
      catch(IOException ioe)
      {
        ioe.printStackTrace();
      }
    }

    if (FileUtil.exists(tempFileName))
      FileUtil.delete(tempFileName);
  }

  /**
   * @author Bill Darbie
   * @author Ying-Huan.Chu
   */
  public static Object loadObjectFromSerializedFile(String fileName) throws FileNotFoundException, ClassNotFoundException, IOException
  {
    Assert.expect(fileName != null);

    Object object = null;
    FileInputStream fis = null;
    BufferedInputStream bis = null;
    ObjectInput is = null;
    try
    {
      fis = new FileInputStream(fileName);
      bis = new BufferedInputStream(fis);
      is = new ObjectInputStream(bis);

//      long startTime = System.currentTimeMillis();
      object = is.readObject();
//      long stopTime = System.currentTimeMillis();
//      long time = stopTime - startTime;
    }
    finally
    {
      try
      {
        if (is != null)
          is.close();
        // Ying-Huan.Chu [XCR1661]
        // Leaving FileInputStream and BufferedInputStream unclosed will cause the file to be locked.
        if (bis != null)
          bis.close();
        if (fis != null)
          fis.close();
      }
      catch (IOException ioe)
      {
        // do nothing
      }
    }

    Assert.expect(object != null);
    return object;
  }

  /**
   * Find all files in the directory specified by directoryName with the extension specified by
   * the extension parameter.  Return the list in the files variable
   * @author George A. David
   */
  public static void findAllFullPaths(String directoryName,
                                      String extension,
                                      List<String> files,
                                      boolean searchSubDirectories)
  {
    Assert.expect(directoryName != null);
    Assert.expect(files != null);
    Assert.expect(extension != null);
    String ext = extension.toLowerCase();

    File directory = new File(directoryName);
    File[] theFiles = directory.listFiles();
    if (theFiles != null)
    {
      for (int i = 0; i < theFiles.length; ++i)
      {
        File file = theFiles[i];
        if (file.isDirectory() && searchSubDirectories)
          findAllFullPaths(file.getPath(), ext, files, true);
        else
        {
          String fileName = file.getName();
          if (fileName.toLowerCase().endsWith(ext))
            files.add(file.getPath());
        }
      }
    }
  }

  /**
   * Creates a backup file name by changing the extension to .backup
   * @author George A. David
   */
  public static String changeExtensionToBackup(String fileName)
  {
    Assert.expect(fileName != null);

    String backupFileName = null;
    int index = fileName.lastIndexOf(".");
    if (index == -1)
    {
      backupFileName = fileName;
    }
    else
    {
      backupFileName = fileName.substring(0, index);
    }
    backupFileName += _BACKUP_EXTENSION;

    return backupFileName;
  }

  /**
   * Creates a temorary file name by changing the extension to .temp
   * @author George A. David
   */
  public static String changeExtensionToTemp(String fileName)
  {
    Assert.expect(fileName != null);

    String tempFileName = null;
    int index = fileName.lastIndexOf(".");
    if (index == -1)
    {
      tempFileName = fileName;
    }
    else
    {
      tempFileName = fileName.substring(0, index);
    }
    tempFileName += _TEMP_EXTENSION;

    return tempFileName;
  }

  /**
   * Creates a duplicate of the file passed in by
   * copying it and changing its extension to .backup
   * @author George A. David
   */
  public static void backupFile(String fileName) throws FileDoesNotExistException, CouldNotCopyFileException
  {
    String backupFileName = changeExtensionToBackup(fileName);
    FileUtil.copy(fileName, backupFileName);
  }

  /**
   * Restore a backup file. The file name is the name prior to backing up.
   * @author George A. David
   */
  public static void restoreBackupFile(String fileName) throws FileDoesNotExistException, CouldNotCopyFileException
  {
    String backupFileName = changeExtensionToBackup(fileName);
    File backupFile = new File(backupFileName);
    if (backupFile.exists())
    {
      FileUtil.copy(backupFileName, fileName);
    }
  }

  /**
   * This method compares text or binary files to determine if they are identical or not.
   * @return true if the two files are identical, false otherwise.
   * @author Bill Darbie
   */
  public static boolean areFilesIdentical(String file1, String file2) throws FileDoesNotExistException,
      CouldNotReadFileException
  {
    Assert.expect(file1 != null);
    Assert.expect(file2 != null);

    FileInputStream fis1 = null;
    FileInputStream fis2 = null;
    BufferedInputStream bif1 = null;
    BufferedInputStream bif2 = null;
    try
    {
      try
      {
        fis1 = new FileInputStream(file1);
      }
      catch (FileNotFoundException ex)
      {
        FileDoesNotExistException ex2 = new FileDoesNotExistException(file1);
        ex2.initCause(ex);
        throw ex2;
      }
      try
      {
        fis2 = new FileInputStream(file2);
      }
      catch (FileNotFoundException ex)
      {
        FileDoesNotExistException ex2 = new FileDoesNotExistException(file2);
        ex2.initCause(ex);
        throw ex2;
      }

      File theFile1 = new File(file1);
      File theFile2 = new File(file2);

      if (theFile1.length() != theFile2.length())
        return false;

      bif1 = new BufferedInputStream(fis1);
      bif2 = new BufferedInputStream(fis2);

      int byte1 = 0;
      int byte2 = 0;
      while (byte1 != -1)
      {
        if (byte1 != byte2)
          return false;

        try
        {
          byte1 = bif1.read();
        }
        catch (IOException ioe)
        {
          CouldNotReadFileException ex = new CouldNotReadFileException(file1);
          ex.initCause(ioe);
          throw ex;
        }
        try
        {
          byte2 = bif2.read();
        }
        catch (IOException ioe)
        {
          CouldNotReadFileException ex = new CouldNotReadFileException(file2);
          ex.initCause(ioe);
          throw ex;
        }
      }
    }
    finally
    {
      try
      {
        if (bif1 != null)
          bif1.close();
      }
      catch (IOException ex)
      {
        ex.printStackTrace();
      }

      try
      {
        if (bif2 != null)
          bif2.close();
      }
      catch (IOException ex)
      {
        ex.printStackTrace();
      }
    }

    return true;
  }

  /**
   * Create a unique temporary directory in the directory specified.
   * @author Bill Darbie
   */
  public static String createTempDir(String dirName) throws FileFoundWhereDirectoryWasExpectedException, CouldNotCreateFileException
  {
    Assert.expect(dirName != null);

    String newDirName = null;

    int i = 1;
    while (true)
    {
      newDirName = dirName + File.separator + "temp" + Integer.toString(i);
      if (exists(newDirName) == false)
        break;
      i++;
    }

    FileUtil.mkdir(newDirName);
    return newDirName;
  }

  /**
   * @author Bill Darbie
   */
  public static String getUniqueTempFile(String fileName)
  {
    Assert.expect(fileName != null);

    String newFileName = null;

    int i = 1;
    while (true)
    {
      newFileName = fileName + ".temp" + Integer.toString(i);
      if (exists(newFileName) == false)
        break;
      i++;
    }

    return newFileName;
  }

  /**
   * Returns the absolute path root of the path provided (e.g. 'c:\temp\images' returns 'c:\').
   * If the path is a UNC path, returns the computer name and share name
   * (e.g. '\\server\share\someDir' returns '\\server\share\').
   * If the path doesn't match either of the above patterns, an empty string is returned.
   *
   * @author Matt Wharton
   */
  public static String getAbsolutePathRoot(String absolutePath)
  {
    Assert.expect(absolutePath != null);

    String absolutePathRoot = "";

    // Try a standard path pattern first.
    final String standardAbsolutePathRegex = "^([a-z]:).*$";
    Pattern standardAbsolutePathPattern = Pattern.compile(standardAbsolutePathRegex, Pattern.CASE_INSENSITIVE);
    Matcher matcher = standardAbsolutePathPattern.matcher(absolutePath);
    if (matcher.matches())
    {
      absolutePathRoot = matcher.group(1) + File.separator;
    }
    else
    {
      // Standard path didn't match, let's try UNC.
      final String uncAbsolutePathRegex = "^(\\\\\\\\[^/\\\\\\]\\[\":;|<>+=,?* _]+\\\\[^/\\\\\\]\\[\":;|<>+=,?*]+).*$";
      Pattern uncAbsolutePathPattern = Pattern.compile(uncAbsolutePathRegex, Pattern.CASE_INSENSITIVE);
      matcher = uncAbsolutePathPattern.matcher(absolutePath);
      if (matcher.matches())
      {
        absolutePathRoot = matcher.group(1) + File.separator;
      }
      else
      {
        absolutePathRoot = "";
      }
    }

    return absolutePathRoot;
  }

  /**
   * @author Laura Cormos
   */
  public static boolean isDirectoryPathFileSystemLegal(String fileOrDirectoryName)
  {
    Assert.expect(fileOrDirectoryName != null);

    // match an optional letter drive (e.g. C: followed by anything but one or more entry of any of: / : * ? " < > or |)
    String legalFileNameChar = "^([A-Za-z]:)?[^/:\\*\\?\"\\<\\>\\|]*$";
    if (Pattern.matches(legalFileNameChar, fileOrDirectoryName))
      return true;
    return false;
  }

  /**
   * @author Bill Darbie
   */
  public static void createEmtpyFile(String fileName) throws IOException
  {
    Assert.expect(fileName != null);
    File file = new File(fileName);
    file.createNewFile();
  }

  /**
   * @author Laura Cormos
   */
  public static String replaceIllegaFileSystemCharsWithSpace(String path)
  {
    Assert.expect(path != null);

    Matcher matcher = Pattern.compile("^([A-Za-z]:)?(.*$)").matcher(path);
    String driveLetter = "";
    String filePath = "";
    if (matcher.matches())
    {
      driveLetter = matcher.group(1);
      filePath = matcher.group(2);
    }
    String newPath = filePath.replaceAll("[/:\\*\\?\"\\<\\>\\|]", " ");
    if (driveLetter != null)
      return driveLetter + newPath;
    else
      return newPath;
  }

   /**
   * @author Bill Darbie
   * @author Sham
   */
  private static void copyDirectoryRecursivelyWithExcludeList(String fromDirectory,
                                               String toDirectory,
                                               boolean preserveTimeStamp, List<String> dontCopyList) throws FileDoesNotExistException,
                                                                                 FileFoundWhereDirectoryWasExpectedException,
                                                                                 CouldNotCopyFileException,
                                                                                 CouldNotCreateFileException
  {
    copyDirectoryRecursively(fromDirectory, toDirectory, dontCopyList, preserveTimeStamp);
  }

    /**
   * Copy all files and directories in fromDirectory to toDirectory recursively.
   * Both fromDirectory and toDirectory must already exist.
   * @author Bill Darbie
   * @author Sham
   */
  public static void copyDirectoryRecursivelyWithExcludeList(String fromDirectory,
                                              String toDirectory, List<String>dontCopyList) throws FileDoesNotExistException,
                                                                         FileFoundWhereDirectoryWasExpectedException,
                                                                         CouldNotCopyFileException,
                                                                         CouldNotCreateFileException
  {
    _excludeHiddenFiles = true;
    try
    {
      copyDirectoryRecursivelyWithExcludeList(fromDirectory, toDirectory, _DONT_PRESERVE_TIMESTAMP, dontCopyList);
    }
    finally
    {
      _excludeHiddenFiles = false;  // revert to defaults
    }
  }
  
  /**
   * @return a List of Strings of all files that matches with the file name regular expression in the directory passed in. It does not
   *         search subdirectories
   * @param directoryName the name of the directory to search
   * @author Siew Yeng
   */
  public static List<String> listFiles(String directoryName, final String fileNameRegEx)
  {
    Assert.expect(directoryName != null);
    Assert.expect(fileNameRegEx != null);

    File file = new File(directoryName);
    File[] fileNames = file.listFiles(new RegExFileNameFilter(fileNameRegEx));
    
    List<String> filePaths = new ArrayList();
    if(fileNames != null)
    {
      for(File fileName: fileNames)
      {
       filePaths.add(fileName.getPath());
      }
    }
    return filePaths;
  }
}

