package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * Helper class for reading data from a CSV formatted file.
 *
 * @author khang-wah.chnee
 */
public class CSVFileReader 
{
  public static final char _COMMA = ','; // Consistent with CSVParser
  public static final char _SPACE = ' ';
  public static final char _QUOTE = '"'; // Consistent with CSVParser
  private static CSVReader _reader;
  
  /**
   * @author khang-wah.chnee
   */
  public CSVFileReader()
  {
  }
  
  public List<String []> readFile(String fileName) throws IOException
  {
    Assert.expect(fileName != null);
    
    _reader = new CSVReader(new FileReader(fileName), _COMMA, _QUOTE, _SPACE);
    
    List<String []> data = _reader.readAll();
    
    return data;
  }
  
  /**
   * @author Bee Hoon
   */
  public void closeFile() throws IOException
  { 
    _reader.close();
  }
}
