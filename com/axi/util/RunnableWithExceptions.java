package com.axi.util;

/**
 * This interface is similar to the java.lang.Runnable interface except that
 * it allows Exceptions to be thrown from the run method.  This class is
 * used by the WorkerThread class.
 *
 * @author Bill Darbie
 */
public interface RunnableWithExceptions
{
  public void run() throws Exception;
}