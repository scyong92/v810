package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * This class performs a Unit Test on the MathUtil class.
 *
 * @author Keith Lee
 */
public class Test_MathUtil extends UnitTest
{
  private static final double _DEFAULT_VALUE = 100;

  // these constants should exactly equal those from MathUtil.java
  private static final double _MILS_TO_INCHES = 0.001;
  private static final double _INCHES_TO_MILS = 1000.0;
  private static final double _MILS_TO_MILLIMETERS = 0.0254;
  private static final double _MILLIMETERS_TO_MILS = 1.0/0.0254;
  private static final double _INCHES_TO_MILLIMETERS = 25.4;
  private static final double _MILLIMETERS_TO_INCHES = 1.0/25.4;
  private static final double _MILS_TO_METERS = 0.0000254;
  private static final double _METERS_TO_MILS = 1.0/0.0000254;
  private static final double _INCHES_TO_METERS = 0.0254;
  private static final double _METERS_TO_INCHES = 1.0/0.0254;
  private static final double _MILLIMETERS_TO_METERS = 0.001;
  private static final double _METERS_TO_MILLIMETERS = 1000.0;
  private static final double _NANOMETERS_TO_MILS = 1.0/25400.0;
  private static final double _MILS_TO_NANOMETERS = 25400.0;
  private static final double _NANOMETERS_TO_INCHES= 1.0/25400000.0;
  private static final double _INCHES_TO_NANOMETERS = 25400000.0;
  private static final double _NANOMETERS_TO_MILLIMETERS = 0.000001;
  private static final double _MILLIMETERS_TO_NANOMETERS = 1000000.0;
  private static final double _NANOMETERS_TO_METERS = 0.000000001;
  private static final double _METERS_TO_NANOMETERS = 1000000000.0;

  private static final double _MILS_TO_INCHES_VALUE = _DEFAULT_VALUE * _MILS_TO_INCHES;
  private static final double _MILS_TO_MILLIMETERS_VALUE = _DEFAULT_VALUE * _MILS_TO_MILLIMETERS;
  private static final double _MILS_TO_METERS_VALUE = _DEFAULT_VALUE * _MILS_TO_METERS;
  private static final double _MILS_TO_NANOMETERS_VALUE = _DEFAULT_VALUE * _MILS_TO_NANOMETERS;
  private static final double _INCHES_TO_MILS_VALUE = _DEFAULT_VALUE * _INCHES_TO_MILS;
  private static final double _INCHES_TO_MILLIMETERS_VALUE = _DEFAULT_VALUE * _INCHES_TO_MILLIMETERS;
  private static final double _INCHES_TO_METERS_VALUE = _DEFAULT_VALUE * _INCHES_TO_METERS;
  private static final double _INCHES_TO_NANOMETERS_VALUE = _DEFAULT_VALUE * _INCHES_TO_NANOMETERS;
  private static final double _MILLIMETERS_TO_MILS_VALUE = _DEFAULT_VALUE * _MILLIMETERS_TO_MILS;
  private static final double _MILLIMETERS_TO_INCHES_VALUE = _DEFAULT_VALUE * _MILLIMETERS_TO_INCHES;
  private static final double _MILLIMETERS_TO_METERS_VALUE = _DEFAULT_VALUE * _MILLIMETERS_TO_METERS;
  private static final double _MILLIMETERS_TO_NANOMETERS_VALUE = _DEFAULT_VALUE * _MILLIMETERS_TO_NANOMETERS;
  private static final double _METERS_TO_MILS_VALUE = _DEFAULT_VALUE * _METERS_TO_MILS;
  private static final double _METERS_TO_INCHES_VALUE = _DEFAULT_VALUE * _METERS_TO_INCHES;
  private static final double _METERS_TO_MILLIMETERS_VALUE = _DEFAULT_VALUE * _METERS_TO_MILLIMETERS;
  private static final double _METERS_TO_NANOMETERS_VALUE = _DEFAULT_VALUE * _METERS_TO_NANOMETERS;
  private static final double _NANOMETERS_TO_MILS_VALUE = _DEFAULT_VALUE * _NANOMETERS_TO_MILS;
  private static final double _NANOMETERS_TO_INCHES_VALUE = _DEFAULT_VALUE * _NANOMETERS_TO_INCHES;
  private static final double _NANOMETERS_TO_MILLIMETERS_VALUE = _DEFAULT_VALUE * _NANOMETERS_TO_MILLIMETERS;
  private static final double _NANOMETERS_TO_METERS_VALUE = _DEFAULT_VALUE * _NANOMETERS_TO_METERS;

  private static final int _ZERO_INT = 0;
  private static final int _POSITIVE_ONE_INT = 1;
  private static final int _NEGATIVE_ONE_INT = -1;
  private static final int _POSITIVE_FIVE_INT = 5;
  private static final int _NEGATIVE_FIVE_INT = -5;
  private static final int _POSITIVE_TWO_INT = 2;
  private static final int _NEGATIVE_TWO_INT = -2;
  private static final int _POSITIVE_SIX_INT = 6;
  private static final int _NEGATIVE_SIX_INT = -6;
  private static final int _POSITIVE_SEVEN_INT = 7;
  private static final int _NEGATIVE_SEVEN_INT = -7;

  private static final double _NANOMETERS_PER_PIXEL = 100;

  private static BufferedReader _is = null;
  private static PrintWriter _os = null;

  public static void main(String[] args)
  {
    UnitTest.execute(new Test_MathUtil());
  }

  /**
   * @author Keith Lee
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Assert.expect(is != null);
    Assert.expect(os != null);

    _is = is;
    _os = os;

    testConvertUnits();
    testConvertDoubleToInt();
    testBankersRoundDoubleToInt();
    testIsColinear();
    testConvexHull();
    testConvertNanoMetersToPixels();
    testGetDeterminant();
    testConvertByteToHex(os);
//    testSwap();
  }

  private void testConvertUnits()
  {
    boolean exception = false;
    double value = _DEFAULT_VALUE;
    double convertedValue = _DEFAULT_VALUE;
    MathUtilEnum mils = MathUtilEnum.MILS;
    MathUtilEnum inches = MathUtilEnum.INCHES;
    MathUtilEnum millimeters = MathUtilEnum.MILLIMETERS;
    MathUtilEnum meters = MathUtilEnum.METERS;
    MathUtilEnum nanometers = MathUtilEnum.NANOMETERS;

    try
    {
      convertedValue = MathUtil.convertUnits(value, null, mils);
    }
    catch(AssertException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      convertedValue = MathUtil.convertUnits(value, mils, null);
    }
    catch(AssertException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    convertedValue = MathUtil.convertUnits(value, mils, mils);
    Expect.expect(convertedValue == value);

    convertedValue = MathUtil.convertUnits(value, mils, inches);
    Expect.expect(convertedValue == _MILS_TO_INCHES_VALUE);

    convertedValue = MathUtil.convertUnits(value, mils, millimeters);
    Expect.expect(convertedValue == _MILS_TO_MILLIMETERS_VALUE);

    convertedValue = MathUtil.convertUnits(value, mils, meters);
    Expect.expect(convertedValue == _MILS_TO_METERS_VALUE);

    convertedValue = MathUtil.convertUnits(value, mils, nanometers);
    Expect.expect(convertedValue == _MILS_TO_NANOMETERS_VALUE);

    convertedValue = MathUtil.convertUnits(value, inches, mils);
    Expect.expect(convertedValue == _INCHES_TO_MILS_VALUE);

    convertedValue = MathUtil.convertUnits(value, inches, millimeters);
    Expect.expect(convertedValue == _INCHES_TO_MILLIMETERS_VALUE);

    convertedValue = MathUtil.convertUnits(value, inches, meters);
    Expect.expect(convertedValue == _INCHES_TO_METERS_VALUE);

    convertedValue = MathUtil.convertUnits(value, inches, nanometers);
    Expect.expect(convertedValue == _INCHES_TO_NANOMETERS_VALUE);

    convertedValue = MathUtil.convertUnits(value, millimeters, mils);
    Expect.expect(convertedValue == _MILLIMETERS_TO_MILS_VALUE);

    convertedValue = MathUtil.convertUnits(value, millimeters, inches);
    Expect.expect(convertedValue == _MILLIMETERS_TO_INCHES_VALUE);

    convertedValue = MathUtil.convertUnits(value, millimeters, meters);
    Expect.expect(convertedValue == _MILLIMETERS_TO_METERS_VALUE);

    convertedValue = MathUtil.convertUnits(value, millimeters, nanometers);
    Expect.expect(convertedValue == _MILLIMETERS_TO_NANOMETERS_VALUE);

    convertedValue = MathUtil.convertUnits(value, meters, mils);
    Expect.expect(convertedValue == _METERS_TO_MILS_VALUE);

    convertedValue = MathUtil.convertUnits(value, meters, inches);
    Expect.expect(convertedValue == _METERS_TO_INCHES_VALUE);

    convertedValue = MathUtil.convertUnits(value, meters, millimeters);
    Expect.expect(convertedValue == _METERS_TO_MILLIMETERS_VALUE);

    convertedValue = MathUtil.convertUnits(value, meters, nanometers);
    Expect.expect(convertedValue == _METERS_TO_NANOMETERS_VALUE);

    convertedValue = MathUtil.convertUnits(value, nanometers, mils);
    Expect.expect(convertedValue == _NANOMETERS_TO_MILS_VALUE);

    convertedValue = MathUtil.convertUnits(value, nanometers, inches);
    Expect.expect(convertedValue == _NANOMETERS_TO_INCHES_VALUE);

    convertedValue = MathUtil.convertUnits(value, nanometers, millimeters);
    Expect.expect(convertedValue == _NANOMETERS_TO_MILLIMETERS_VALUE);

    convertedValue = MathUtil.convertUnits(value, nanometers, meters);
    Expect.expect(convertedValue == _NANOMETERS_TO_METERS_VALUE);
  }

  void testConvertDoubleToInt()
  {
    int convertedValue = 0;
    boolean exception = false;

    try
    {
      convertedValue = MathUtil.convertDoubleToInt(Double.MAX_VALUE);
    }
    catch(ValueOutOfRangeException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;
    try
    {
      convertedValue = MathUtil.convertDoubleToInt(-Double.MAX_VALUE);
    }
    catch(ValueOutOfRangeException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      convertedValue = MathUtil.convertDoubleToInt(0.0);
      Expect.expect(convertedValue == _ZERO_INT);
      convertedValue = MathUtil.convertDoubleToInt(-0.0);
      Expect.expect(convertedValue == _ZERO_INT);
      convertedValue = MathUtil.convertDoubleToInt(0.1);
      Expect.expect(convertedValue == _ZERO_INT);
      convertedValue = MathUtil.convertDoubleToInt(-0.1);
      Expect.expect(convertedValue == _ZERO_INT);
      // round to more positive number
      convertedValue = MathUtil.convertDoubleToInt(0.5);
      Expect.expect(convertedValue == _POSITIVE_ONE_INT);
      // round to more postive number
      convertedValue = MathUtil.convertDoubleToInt(-0.5);
      Expect.expect(convertedValue == _ZERO_INT);
      convertedValue = MathUtil.convertDoubleToInt(0.9);
      Expect.expect(convertedValue == _POSITIVE_ONE_INT);
      convertedValue = MathUtil.convertDoubleToInt(-0.9);
      Expect.expect(convertedValue == _NEGATIVE_ONE_INT);
      convertedValue = MathUtil.convertDoubleToInt(5.0);
      Expect.expect(convertedValue == _POSITIVE_FIVE_INT);
      convertedValue = MathUtil.convertDoubleToInt(-5.0);
      Expect.expect(convertedValue == _NEGATIVE_FIVE_INT);
      convertedValue = MathUtil.convertDoubleToInt(5.1);
      Expect.expect(convertedValue == _POSITIVE_FIVE_INT);
      convertedValue = MathUtil.convertDoubleToInt(-5.1);
      Expect.expect(convertedValue == _NEGATIVE_FIVE_INT);
      // round to more positive number
      convertedValue = MathUtil.convertDoubleToInt(5.5);
      Expect.expect(convertedValue == _POSITIVE_SIX_INT);
      // round to more positive number
      convertedValue = MathUtil.convertDoubleToInt(-5.5);
      Expect.expect(convertedValue == _NEGATIVE_FIVE_INT);
      convertedValue = MathUtil.convertDoubleToInt(5.9);
      Expect.expect(convertedValue == _POSITIVE_SIX_INT);
      convertedValue = MathUtil.convertDoubleToInt(-5.9);
      Expect.expect(convertedValue == _NEGATIVE_SIX_INT);
    }
    catch(ValueOutOfRangeException voore)
    {
      voore.printStackTrace();
    }
  }

  /**
   * @author Peter Esbensen
   */
  private void testGetDeterminant()
  {
    // get the determinant of this matrix:
    //  1 2 3
    //  4 5 6
    //  7 8 9

    double determinant = MathUtil.getDeterminantOfThreeByThreeMatrix(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0);
    Expect.expect(MathUtil.fuzzyEquals(determinant, 0.0));
  }

  private void testBankersRoundDoubleToInt()
  {
    int convertedValue = 0;
    boolean exception = false;

    try
    {
      convertedValue = MathUtil.legacyBankersRoundDoubleToInt(Double.MAX_VALUE);
    }
    catch(AssertException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;
    try
    {
      convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-Double.MAX_VALUE);
    }
    catch(AssertException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(0.0);
    Expect.expect(convertedValue == _ZERO_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-0.0);
    Expect.expect(convertedValue == _ZERO_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(0.1);
    Expect.expect(convertedValue == _ZERO_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-0.1);
    Expect.expect(convertedValue == _ZERO_INT);
    // round to even number
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(0.5);
    Expect.expect(convertedValue == _ZERO_INT);
    // round to even number
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-0.5);
    Expect.expect(convertedValue == _ZERO_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(0.9);
    Expect.expect(convertedValue == _POSITIVE_ONE_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-0.9);
    Expect.expect(convertedValue == _NEGATIVE_ONE_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(1.0);
    Expect.expect(convertedValue == _POSITIVE_ONE_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-1.0);
    Expect.expect(convertedValue == _NEGATIVE_ONE_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(1.1);
    Expect.expect(convertedValue == _POSITIVE_ONE_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-1.1);
    Expect.expect(convertedValue == _NEGATIVE_ONE_INT);
    // round to even number
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(1.5);
    Expect.expect(convertedValue == _POSITIVE_TWO_INT);
    // round to even number
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-1.5);
    Expect.expect(convertedValue == _NEGATIVE_TWO_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(1.9);
    Expect.expect(convertedValue == _POSITIVE_TWO_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-1.9);
    Expect.expect(convertedValue == _NEGATIVE_TWO_INT);

    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(5.0);
    Expect.expect(convertedValue == _POSITIVE_FIVE_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-5.0);
    Expect.expect(convertedValue == _NEGATIVE_FIVE_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(5.1);
    Expect.expect(convertedValue == _POSITIVE_FIVE_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-5.1);
    Expect.expect(convertedValue == _NEGATIVE_FIVE_INT);
    // round to even number
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(5.5);
    Expect.expect(convertedValue == _POSITIVE_SIX_INT);
    // round to even number
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-5.5);
    Expect.expect(convertedValue == _NEGATIVE_SIX_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(5.9);
    Expect.expect(convertedValue == _POSITIVE_SIX_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-5.9);
    Expect.expect(convertedValue == _NEGATIVE_SIX_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(6.0);
    Expect.expect(convertedValue == _POSITIVE_SIX_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-6.0);
    Expect.expect(convertedValue == _NEGATIVE_SIX_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(6.1);
    Expect.expect(convertedValue == _POSITIVE_SIX_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-6.1);
    Expect.expect(convertedValue == _NEGATIVE_SIX_INT);
    // round to even number
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(6.5);
    Expect.expect(convertedValue == _POSITIVE_SIX_INT);
    // round to even number
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-6.5);
    Expect.expect(convertedValue == _NEGATIVE_SIX_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(6.9);
    Expect.expect(convertedValue == _POSITIVE_SEVEN_INT);
    convertedValue = MathUtil.legacyBankersRoundDoubleToInt(-6.9);
    Expect.expect(convertedValue == _NEGATIVE_SEVEN_INT);
  }

  /**
   * @author George A. David
   */
  private void testIsColinear(double x1, double y1,
                              double x2, double y2,
                              double x3, double y3,
                              boolean isColinear)
  {
    Expect.expect(MathUtil.isFuzzyColinear(x1, y1, x2, y2, x3, y3) == isColinear);
    Expect.expect(MathUtil.isFuzzyColinear(x1, y1, x3, y3, x2, y2) == isColinear);
    Expect.expect(MathUtil.isFuzzyColinear(x2, y2, x1, y1, x3, y3) == isColinear);
    Expect.expect(MathUtil.isFuzzyColinear(x2, y2, x3, y3, x1, y1) == isColinear);
    Expect.expect(MathUtil.isFuzzyColinear(x3, y3, x1, y1, x2, y2) == isColinear);
    Expect.expect(MathUtil.isFuzzyColinear(x3, y3, x2, y2, x1, y1) == isColinear);

    DoubleCoordinate point1 = new DoubleCoordinate(x1, y1);
    DoubleCoordinate point2 = new DoubleCoordinate(x2, y2);
    DoubleCoordinate point3 = new DoubleCoordinate(x3, y3);

    List<DoubleCoordinate> points = new ArrayList<DoubleCoordinate>(3);
    points.add(point1);
    points.add(point2);
    points.add(point3);

    Expect.expect(MathUtil.isFuzzyColinear(points) == isColinear);

    points.clear();
    points.add(point1);
    points.add(point3);
    points.add(point2);
    Expect.expect(MathUtil.isFuzzyColinear(points) == isColinear);

    points.clear();
    points.add(point2);
    points.add(point1);
    points.add(point3);
    Expect.expect(MathUtil.isFuzzyColinear(points) == isColinear);

    points.clear();
    points.add(point2);
    points.add(point3);
    points.add(point1);
    Expect.expect(MathUtil.isFuzzyColinear(points) == isColinear);

    points.clear();
    points.add(point3);
    points.add(point2);
    points.add(point1);
    Expect.expect(MathUtil.isFuzzyColinear(points) == isColinear);

    points.clear();
    points.add(point3);
    points.add(point1);
    points.add(point2);
    Expect.expect(MathUtil.isFuzzyColinear(points) == isColinear);
  }

  /**
   * @author George A. David
   */
  private void testIsColinear()
  {
    double x1 = 1.0;
    double y1 = 3.0;
    double x2 = 2.0;
    double y2 = 6.0;
    double x3 = 3.0;
    double y3 = 9.0;

    testIsColinear(x1, y1, x2, y2, x3, y3, true);

    x1 = 2;

    testIsColinear(x1, y1, x2, y2, x3, y3, false);

  }

  /**
   * @author George A. David
   */
  public void testConvexHull()
  {
    DoubleCoordinate referenceCoord = new DoubleCoordinate(2, 1);
    List<DoubleCoordinate> coords = new LinkedList<DoubleCoordinate>();

    DoubleCoordinate coord1 = new DoubleCoordinate(-5, -1);
    DoubleCoordinate coord2 = new DoubleCoordinate(-4, 3);
    DoubleCoordinate coord3 = new DoubleCoordinate(-2, 1);
    DoubleCoordinate coord4 = new DoubleCoordinate(-2, -5);
    DoubleCoordinate coord5 = new DoubleCoordinate(-1, -1);
    DoubleCoordinate coord6 = new DoubleCoordinate(2, -3);
    DoubleCoordinate coord7 = new DoubleCoordinate(1, 2);
    DoubleCoordinate coord8 = new DoubleCoordinate(2, 5);
    DoubleCoordinate coord9 = new DoubleCoordinate(3, 2);
    DoubleCoordinate coord10 = new DoubleCoordinate(4, -1);

    coords.add(coord1);
    coords.add(coord2);
    coords.add(coord3);
    coords.add(coord4);
    coords.add(coord5);
    coords.add(coord6);
    coords.add(coord7);
    coords.add(coord8);
    coords.add(coord9);
    coords.add(coord10);

    List<DoubleCoordinate> convexHull = MathUtil.getConvexHull(coords);
    Assert.expect(convexHull.size() == 7);
    Expect.expect(convexHull.get(6) == coord4);
    Expect.expect(convexHull.get(5) == coord6);
    Expect.expect(convexHull.get(4) == coord10);
    Expect.expect(convexHull.get(3) == coord9);
    Expect.expect(convexHull.get(2) == coord8);
    Expect.expect(convexHull.get(1) == coord2);
    Expect.expect(convexHull.get(0) == coord1);

    Collections.sort(coords, new DoubleCoordinateDistanceComparator(referenceCoord));
    convexHull = MathUtil.getSmallestConvexHull(coords, referenceCoord);
    Assert.expect(convexHull.size() == 3);
    Expect.expect(convexHull.contains(coord10));
    Expect.expect(convexHull.contains(coord7));
    Expect.expect(convexHull.contains(coord9));

//    Expect.expect(convexHull.get(0) == coord4);
//    Expect.expect(convexHull.get(1) == coord6);
//    Expect.expect(convexHull.get(2) == coord1);
//    Expect.expect(convexHull.get(3) == coord10);
//    Expect.expect(convexHull.get(4) == coord9);
//    Expect.expect(convexHull.get(5) == coord2);
//    Expect.expect(convexHull.get(6) == coord1);
  }

  /**
   * @author Matt Wharton
   */
  private void testConvertNanoMetersToPixels()
  {

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        MathUtil.convertNanoMetersToPixelsUsingRound(100, -_NANOMETERS_PER_PIXEL);
      }
    });

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        MathUtil.convertNanoMetersToPixelsUsingCeil(100, -_NANOMETERS_PER_PIXEL);
      }
    });

    // Test normal cases.
    int expectedNumberOfPixels = 2;
    int numberOfPixels = MathUtil.convertNanoMetersToPixelsUsingRound(125, _NANOMETERS_PER_PIXEL);
    Expect.expect(numberOfPixels == expectedNumberOfPixels);

    expectedNumberOfPixels = 3;
    numberOfPixels = MathUtil.convertNanoMetersToPixelsUsingCeil(125, _NANOMETERS_PER_PIXEL);
    Expect.expect(numberOfPixels == expectedNumberOfPixels);
  }

  /**
   * @author Matt Wharton
   */
  private void testSwap()
  {
    int a = 1;
    int b = 2;

    MathUtil.swap(a, b);

    Expect.expect(a == 2);
    Expect.expect(b == 1);
  }

  /**
   * @author Poh Kheng
   */
  private void testCorrelation()
  {
    double x[] = {6,16,7,7,8,8,8,8};
    double y[] = {9,21,6,6,6,6,6,6};
//    double y[] = {6,16,7,7,8,8,8,8};

    double correlation = MathUtil.correlation(x, y);

    System.out.println("Correlation: "+correlation);

  }


  /**
   * @author Ronald Lim
   */

  private void testConvertByteToHex(PrintWriter os)
  {
    int inputValue = 10;
    byte byteInputValue = (byte) inputValue;
    String hexString = MathUtil.convertByteToHex(byteInputValue);

    os.println("HexString = " + hexString);
  }

}
