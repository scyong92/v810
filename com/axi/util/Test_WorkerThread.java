package com.axi.util;

import java.io.*;

/**
 * @author Bill Darbie
 */
class Test_WorkerThread extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_WorkerThread());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    test1();
//    testPhantomReference();
  }

  /**
   * @author Bill Darbie
   */
  private void testPhantomReference()
  {
    WorkerThread workerThread = new WorkerThread("test2");
    int i = 0;
    while (i < 10)
    {
      ++i;
      try
      {
        Thread.sleep(1000);
      }
      catch (InterruptedException ex)
      {
        // do nothing
      }
      workerThread = null;
      System.gc();
    }
  }


  /**
   * @author Bill Darbie
   */
  private void test1()
  {
    try
    {
      WorkerThread workerThread = new WorkerThread(null);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    WorkerThread workerThread = new WorkerThread("test");

    Run run = new Run();
    RunOnWorkerThread runOnWorkerThread = new RunOnWorkerThread(workerThread);
    RunException runEx = new RunException();
    RunExceptionOnWorkerThread runExOnWorkerThread = new RunExceptionOnWorkerThread(workerThread);

    System.out.println("calling invokeLater with run 1");
    workerThread.invokeLater(run);
    System.out.println("calling invokeLater with runOnWorkerThread 1");
    workerThread.invokeLater(runOnWorkerThread);
    System.out.println("calling invokeAndWait with run 2");
    workerThread.invokeAndWait(run);
    System.out.println("calling invokeLater with run 3");
    workerThread.invokeLater(run);
    System.out.println("calling invokeAndWait with runOnWorkerThread 2");
    workerThread.invokeAndWait(runOnWorkerThread);
    System.out.println("calling invokeAndWait with run 4");
    workerThread.invokeAndWait(run);
    System.out.println("calling invokeLater with run 5");
    workerThread.invokeLater(run);
    try
    {
      System.out.println("calling invokeAndWait with runEx 1");
      workerThread.invokeAndWait(runEx);
      Expect.expect(false);
    }
    catch(Exception ex)
    {
      // do nothing
    }
    try
    {
      System.out.println("calling invokeAndWait with runExOnWorkerThread 1");
      workerThread.invokeAndWait(runExOnWorkerThread);
      Expect.expect(false);
    }
    catch(Exception ex)
    {
      // do nothing
    }
    System.out.println("calling invokeAndWait with run 6");
    workerThread.invokeAndWait(run);

    // wpd - could not get this test to work consistently (because of a race condition) so I have commented it out
    // now test the clearPendingTask() method
    // put 3 tasks on, then clear, only the first task
    // should print and the other 2 should be cleared out
    // before they execute
    //System.out.println("testing clearPendingTasks() - only invokeLater with run 7 should print");
    //workerThread.invokeLater(run);
    //workerThread.invokeLater(run);
    //workerThread.invokeLater(run);
    //workerThread.clearPendingTasks();

    // we have to have an invokeAndWait() as the last call to prevent the virtual machine
    // from exiting
    //System.out.println("calling invokeAndWait with run 7 ");
    //workerThread.invokeAndWait(run);
    //System.out.println("NOTE: 8 should be the last number printed or there is a bug with clearPendingTasks()");
  }
}

/**
 * @author Bill Darbie
 */
class Run implements Runnable
{
  private int _i;

  public void run()
  {
    try
    {
      Thread.sleep(1000);
    }
    catch(InterruptedException ie)
    {
      // do nothing
    }
    ++_i;
    System.out.println("run: " + _i);
  }
}

/**
 * @author Bill Darbie
 */
class RunOnWorkerThread implements Runnable
{
  private int _i;
  private WorkerThread _workerThread;

  RunOnWorkerThread(WorkerThread workerThread)
  {
    _workerThread = workerThread;
  }

  public void run()
  {
    _workerThread.invokeAndWait(new Runnable()
    {
      public void run()
      {
        try
        {
          Thread.sleep(1000);
        }
        catch(InterruptedException ie)
        {
          // do nothing
        }
        ++_i;
        System.out.println("run on WorkerThread: " + _i);
      }
    });
  }
}

/**
 * @author Bill Darbie
 */
class RunException implements RunnableWithExceptions
{
  private int _i;

  public void run() throws Exception
  {
    try
    {
      Thread.sleep(1000);
    }
    catch(InterruptedException ie)
    {
      // do nothing
    }
    ++_i;
    System.out.println("runException: " + _i);
    throw new Exception("Exception: " + _i);
  }
}

/**
 * @author Bill Darbie
 */
class RunExceptionOnWorkerThread implements RunnableWithExceptions
{
  private int _i;
  private WorkerThread _workerThread;

  RunExceptionOnWorkerThread(WorkerThread workerThread)
  {
    _workerThread = workerThread;
  }

  public void run() throws Exception
  {
    _workerThread.invokeAndWait(new RunnableWithExceptions()
    {
      public void run() throws Exception
      {
        try
        {
          Thread.sleep(1000);
        }
        catch(InterruptedException ie)
        {
          // do nothing
        }
        ++_i;
        System.out.println("runException on WorkerThread: " + _i);
        throw new Exception("Exception: " + _i);
      }
    });
  }
}


