package com.axi.util;

import java.awt.geom.*;
import java.util.*;

/**
* This class provides additional math functionality.
*
* @author Matt Snook, Keith Lee
*/
public class MathUtil
{
  // Constants
  // _X_TO_Y where 1.0 * X = _X_TO_Y * Y
  // for example, 1.0 mils = 0.001 inches
  private static final double _MILS_TO_INCHES = 0.001;
  private static final double _INCHES_TO_MILS = 1000.0;
  private static final double _MILS_TO_MILLIMETERS = 0.0254;
  private static final double _MILLIMETERS_TO_MILS = 1.0/0.0254;
  private static final double _INCHES_TO_MILLIMETERS = 25.4;
  private static final double _MILLIMETERS_TO_INCHES = 1.0/25.4;
  private static final double _MILS_TO_METERS = 0.0000254;
  private static final double _METERS_TO_MILS = 1.0/0.0000254;
  private static final double _INCHES_TO_METERS = 0.0254;
  private static final double _METERS_TO_INCHES = 1.0/0.0254;
  private static final double _MILLIMETERS_TO_METERS = 0.001;
  private static final double _METERS_TO_MILLIMETERS = 1000.0;
  private static final double _NANOMETERS_TO_MILS = 1.0/25400.0;
  private static final double _MILS_TO_NANOMETERS = 25400.0;
  private static final double _NANOMETERS_TO_INCHES= 1.0/25400000.0;
  private static final double _INCHES_TO_NANOMETERS = 25400000.0;
  private static final double _NANOMETERS_TO_MILLIMETERS = 0.000001;
  private static final double _MILLIMETERS_TO_NANOMETERS = 1000000.0;
  private static final double _NANOMETERS_TO_MICRONS = .001;
  private static final double _MICRONS_TO_NANOMETERS = 1000;
  private static final double _NANOMETERS_TO_METERS = 0.000000001;
  private static final double _METERS_TO_NANOMETERS = 1000000000.0;
  private static final double _DOUBLE_EPSILON = 2.2204460492503131e-016;
  private static final double _ROUND_ERROR_LIMIT = 1.0e9;
  private static final double _FUZZY_LIMIT = _DOUBLE_EPSILON * _ROUND_ERROR_LIMIT;
  private static final char[] _HEX_ARRAY = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

  public static final double INCHES_PER_MIL = 0.001;
  public static final double MILS_PER_INCH = 1000.0;
  public static final double MILLIMETERS_PER_MIL = 0.0254;
  public static final double MILS_PER_MILLIMETER = 1.0/0.0254;
  public static final double MILLIMETERS_PER_INCH = 25.4;
  public static final double INCHES_PER_MILLIMETER = 1.0/25.4;
  public static final double METERS_PER_MIL = 0.0000254;
  public static final double MILS_PER_METER = 1.0/0.0000254;
  public static final double METERS_PER_INCH = 0.0254;
  public static final double INCHES_PER_METER = 1.0/0.0254;
  public static final double METERS_PER_MILLIMETER = 0.001;
  public static final double MILLIMETERS_PER_METER = 1000.0;
  public static final double MILS_PER_NANOMETER = 1.0/25400.0;
  public static final double NANOMETERS_PER_MIL = 25400.0;
  public static final double INCHES_PER_NANOMETER= 1.0/25400000.0;
  public static final double NANOMETERS_PER_INCH = 25400000.0;
  public static final double MILLIMETERS_PER_NANOMETER = 0.000001;
  public static final double NANOMETERS_PER_MILLIMETER = 1000000.0;
  public static final double MICRONS_PER_NANOMETER = 0.001;
  public static final double NANOMETERS_PER_MICRON = 1000.0;
  public static final double METERS_PER_NANOMETER = 0.000000001;
  public static final double NANOMETERS_PER_METER = 1000000000.0;
  public static final double _UNIT_CONVERSION_NANOMETER_LIMIT = 1000.0;
  public static final double _CORRELATION_MAX_VALUE = 1.0000;
  public static final double _CORRELATION_MIN_VALUE = -1.0000;


  /**
   * Rounds a number using Banker's Rounding. This is necessary since Pascal
   * rounding uses Banker's Rounding. In Banker's Rounding, numbers with a 0.5
   * fraction are affected. In these cases, it always rounds to the nearest
   * EVEN integer.
   * Look at the example below to get a better understanding of how it works.
   *        Number : Rounding : Banker's Rounding
   *         10.0  :    10    : 10
   *         10.5  :    11    : 10 <- This is what is different.
   *         11.0  :    11    : 11
   *         11.5  :    12    : 12
   *        -10.0  :   -10    : -10
   *        -10.5  :   -10    : -10 <- Math.round()
   *        -11.0  :   -11    : -11
   *        -11.5  :   -11    : -11 <- Math.round()
   * @author Keith Lee
   */
  public static int legacyBankersRoundDoubleToInt(double value)
  {
    Assert.expect(value <= Integer.MAX_VALUE);
    Assert.expect(value >= -Integer.MAX_VALUE);

    double floorDoubleValue = Math.floor(value);
    int floorIntValue = (int)value;
    double abs = Math.abs(value - floorDoubleValue);

    if(abs == 0.5) // if number is exactly on 0.5 line
    {
      if(Math.abs(floorIntValue % 2) == 1) // if number is odd, increment
      {
        if(value > 0) // increment positive number (add +1)
          floorIntValue++;
        else // increment negative number (add -1)
          floorIntValue--;
      }
    }
    else
    {
      if(value < 0)
      {
        floorIntValue = (int)(value - 0.5);
      }
      else
      {
        floorIntValue = (int)(value + 0.5);
      }
    }

    return floorIntValue;
  }

  /**
   * This rounds a number based on a #define in c-code used
   * by the compiler.
   * We must duplicate this rounding technique because it's used
   * to generate the Board RTF directory name, which is then
   * hashed.  If we get this Board RTF directory name wrong, the
   * hash name will be wrong and we won't be able to load CAD.
   *
   * The ONLY place this round should be used in where the Board RTF
   * directory name is being determined.
   *
   * Essentially, this will round -0.5 to -1.0 and 0.5 to 1.0,
   * which is backwards from the Bankers Round above.
   *
   * @author Andy Mechtenberg
   */
  public static int legacyFourPiRoundDoubleToInt(double value)
  {
    Assert.expect(value <= Integer.MAX_VALUE);
    Assert.expect(value >= -Integer.MAX_VALUE);

    int intValue;

    if (value < 0.0)
    {
      intValue = (int)(value - 0.5);
    }
    else
      intValue = (int)(value + 0.5);

    return intValue;
  }


  /**
   * Converts a double value to an int value. The double must be less than or
   * equal to the max positive integer value and greater than or equal to the
   * max negative integer value before converting.
   *
   * @param value the value to convert.
   * @return an int equal to value.
   * @author Keith Lee
   */
  public static int convertDoubleToInt(double value) throws ValueOutOfRangeException
  {
    if ((value > Integer.MAX_VALUE) || (value < Integer.MIN_VALUE))
      throw new ValueOutOfRangeException(value, Integer.MIN_VALUE, Integer.MAX_VALUE);

    // casting to an int merely drops the decimal point. we want to round to the
    // closest whole number. Math.round() uses the expression:
    // Math.floor(value + 0.5)
    // it rounds up (more positive) at the half-way point for positive and negative numbers.
    return (int)Math.round(value);
  }

  /**
   * Converts a double value from one unit type to another unit type.
   *
   * @param value the value to convert.
   * @param fromUnits MathUtilEnum for the units 'value' is currently in.
   * @param toUnits MathUtilEnum for the units to convert 'value' to.
   * @return a double equal to value, but converted into the new units.
   * @author Keith Lee
   */
  public static double convertUnits(double value, MathUtilEnum fromUnits, MathUtilEnum toUnits)
  {
    Assert.expect(fromUnits != null);
    Assert.expect(toUnits != null);

    double returnVal = value;
    if(fromUnits.equals(toUnits))
    {
      // do nothing
    }
    // mils to inches
    else if( (fromUnits.equals(MathUtilEnum.MILS)) && (toUnits.equals(MathUtilEnum.INCHES)) )
    {
      returnVal = (_MILS_TO_INCHES * value);
    }
    // mils to millimeters
    else if( (fromUnits.equals(MathUtilEnum.MILS)) && (toUnits.equals(MathUtilEnum.MILLIMETERS)) )
    {
      returnVal = (_MILS_TO_MILLIMETERS * value);
    }
    // mils to meters
    else if( (fromUnits.equals(MathUtilEnum.MILS)) && (toUnits.equals(MathUtilEnum.METERS)) )
    {
      returnVal = (_MILS_TO_METERS * value);
    }
    // mils to nanometers
    else if( (fromUnits.equals(MathUtilEnum.MILS)) && (toUnits.equals(MathUtilEnum.NANOMETERS)) )
    {
      returnVal = (_MILS_TO_NANOMETERS * value);
    }
    // inches to mils
    else if( (fromUnits.equals(MathUtilEnum.INCHES)) && (toUnits.equals(MathUtilEnum.MILS)) )
    {
      returnVal = (_INCHES_TO_MILS * value);
    }
    // inches to millimeters
    else if( (fromUnits.equals(MathUtilEnum.INCHES)) && (toUnits.equals(MathUtilEnum.MILLIMETERS)) )
    {
      returnVal = (_INCHES_TO_MILLIMETERS * value);
    }
    // inches to meters
    else if( (fromUnits.equals(MathUtilEnum.INCHES)) && (toUnits.equals(MathUtilEnum.METERS)) )
    {
      returnVal = (_INCHES_TO_METERS * value);
    }
    // inches to meters
    else if( (fromUnits.equals(MathUtilEnum.INCHES)) && (toUnits.equals(MathUtilEnum.NANOMETERS)) )
    {
      returnVal = (_INCHES_TO_NANOMETERS * value);
    }
    // millimeters to mils
    else if( (fromUnits.equals(MathUtilEnum.MILLIMETERS)) && (toUnits.equals(MathUtilEnum.MILS)) )
    {
      returnVal = (_MILLIMETERS_TO_MILS * value);
    }
    // millimeters to inches
    else if( (fromUnits.equals(MathUtilEnum.MILLIMETERS)) && (toUnits.equals(MathUtilEnum.INCHES)) )
    {
      returnVal = (_MILLIMETERS_TO_INCHES * value);
    }
    // millimeters to meters
    else if( (fromUnits.equals(MathUtilEnum.MILLIMETERS)) && (toUnits.equals(MathUtilEnum.METERS)) )
    {
      returnVal = (_MILLIMETERS_TO_METERS * value);
    }
    // millimeters to nanometers
    else if( (fromUnits.equals(MathUtilEnum.MILLIMETERS)) && (toUnits.equals(MathUtilEnum.NANOMETERS)) )
    {
      returnVal = (_MILLIMETERS_TO_NANOMETERS * value);
    }
    // meters to mils
    else if( (fromUnits.equals(MathUtilEnum.METERS)) && (toUnits.equals(MathUtilEnum.MILS)) )
    {
      returnVal = (_METERS_TO_MILS * value);
    }
    // meters to inches
    else if( (fromUnits.equals(MathUtilEnum.METERS)) && (toUnits.equals(MathUtilEnum.INCHES)) )
    {
      returnVal = (_METERS_TO_INCHES * value);
    }
    // meters to millimeters
    else if( (fromUnits.equals(MathUtilEnum.METERS)) && (toUnits.equals(MathUtilEnum.MILLIMETERS)) )
    {
      returnVal = (_METERS_TO_MILLIMETERS * value);
    }
    // meters to nanometers
    else if( (fromUnits.equals(MathUtilEnum.METERS)) && (toUnits.equals(MathUtilEnum.NANOMETERS)) )
    {
      returnVal = (_METERS_TO_NANOMETERS * value);
    }
    // nanometers to mils
    else if( (fromUnits.equals(MathUtilEnum.NANOMETERS)) && (toUnits.equals(MathUtilEnum.MILS)) )
    {
      returnVal = (_NANOMETERS_TO_MILS * value);
    }
    // nanometers to inches
    else if( (fromUnits.equals(MathUtilEnum.NANOMETERS)) && (toUnits.equals(MathUtilEnum.INCHES)) )
    {
      returnVal = (_NANOMETERS_TO_INCHES * value);
    }
    // nanometers to millimeters
    else if( (fromUnits.equals(MathUtilEnum.NANOMETERS)) && (toUnits.equals(MathUtilEnum.MILLIMETERS)) )
    {
      returnVal = (_NANOMETERS_TO_MILLIMETERS * value);
    }
    // nanometers to meters
    else if( (fromUnits.equals(MathUtilEnum.NANOMETERS)) && (toUnits.equals(MathUtilEnum.METERS)) )
    {
      returnVal = (_NANOMETERS_TO_METERS * value);
    }
    else if (fromUnits.equals(MathUtilEnum.MICRONS) && toUnits.equals(MathUtilEnum.NANOMETERS))
    {
      returnVal = _MICRONS_TO_NANOMETERS * value;
    }
    // else throw some type of exception
    else
    {
      Assert.expect(false);
    }

    return returnVal;
  }

  /**
   * Converts a float value from one unit type to another unit type.
   *
   * @param value the value to convert.
   * @param fromUnits MathUtilEnum for the units 'value' is currently in.
   * @param toUnits MathUtilEnum for the units to convert 'value' to.
   * @return a float equal to value, but converted into the new units.
   * @author Matt Wharton
   */
  public static float convertUnits(float value, MathUtilEnum fromUnits, MathUtilEnum toUnits)
  {
    Assert.expect(fromUnits != null);
    Assert.expect(toUnits != null);

    return (float)convertUnits((double)value, fromUnits, toUnits);
  }

  /**
   * @author Bill Darbie
   */
  public static double convertMilsToMeters(double mils)
  {
    return mils * _MILS_TO_NANOMETERS * _NANOMETERS_TO_METERS;
  }

  /**
   * @author Patrick Lacz
   */
  public static float convertMilsToMillimeters(float mils)
  {
    return mils * (float)_MILS_TO_MILLIMETERS;
  }

  /**
   * @author Patrick Lacz
   */
  public static double convertMilsToMillimeters(double mils)
  {
    return mils * _MILS_TO_MILLIMETERS;
  }

  /**
   * @author George Booth
   */
  public static float convertSquareMilsToSquareMillimeters(float mils)
  {
    return mils * (float)_MILS_TO_MILLIMETERS * (float)_MILS_TO_MILLIMETERS;
  }

  /**
   * @author George Booth
   */
  public static double convertSquareMilsToSquareMillimeters(double mils)
  {
    return mils * _MILS_TO_MILLIMETERS * _MILS_TO_MILLIMETERS;
  }

  /**
   * @author George Booth
   */
  public static float convertCubicMilsToCubicMillimeters(float mils)
  {
    return mils * (float)_MILS_TO_MILLIMETERS * (float)_MILS_TO_MILLIMETERS * (float)_MILS_TO_MILLIMETERS;
  }

  /**
   * @author George Booth
   */
  public static double convertCubicMilsToCubicMillimeters(double mils)
  {
    return mils * _MILS_TO_MILLIMETERS * _MILS_TO_MILLIMETERS * _MILS_TO_MILLIMETERS;
  }

  /**
   * @author Sunit Bhalla
   */
  public static float convertMilsToNanoMeters(float mils)
  {
    return mils * (float)_MILS_TO_NANOMETERS;
  }

  /**
  * @author Sunit Bhalla
  */
  public static double convertMilsToNanoMeters(double mils)
  {
    return mils * _MILS_TO_NANOMETERS;
  }

  /**
  * @author Bill Darbie
  */
  public static int convertMilsToNanoMetersInteger(double mils)
  {
    final int MILS_TO_NANOMETERS_CONSTANT = (int)_MILS_TO_NANOMETERS;

    Assert.expect (mils < Integer.MAX_VALUE/MILS_TO_NANOMETERS_CONSTANT);
    Assert.expect(mils > Integer.MIN_VALUE/MILS_TO_NANOMETERS_CONSTANT);

    return (int)Math.round(mils * 25400.0);
  }

  /**
   * @author Sunit Bhalla
   */
  public static float convertMillimetersToMils(float millimeters)
  {
    return millimeters * (float)_MILLIMETERS_TO_MILS;
  }

  /**
   * @author George A. David
   */
  public static double convertMillimetersToMils(double millimeters)
  {
    return millimeters * _MILLIMETERS_TO_MILS;
  }

  /**
   * @author George Booth
   */
  public static float convertSquareMillimetersToSquareMils(float millimeters)
  {
    return millimeters * (float)_MILLIMETERS_TO_MILS * (float)_MILLIMETERS_TO_MILS;
  }

  /**
   * @author George Booth
   */
  public static double convertSquareMillimetersToSquareMils(double millimeters)
  {
    return millimeters * _MILLIMETERS_TO_MILS * _MILLIMETERS_TO_MILS;
  }

  /**
   * @author George Booth
   */
  public static float convertCubicMillimetersToCubicMils(float millimeters)
  {
    return millimeters * (float)_MILLIMETERS_TO_MILS * (float)_MILLIMETERS_TO_MILS * (float)_MILLIMETERS_TO_MILS;
  }

  /**
   * @author George Booth
   */
  public static double convertCubicMillimetersToCubicMils(double millimeters)
  {
    return millimeters * _MILLIMETERS_TO_MILS * _MILLIMETERS_TO_MILS * _MILLIMETERS_TO_MILS;
  }

  /**
   * @author Sunit Bhalla
   */
  public static float convertNanoMetersToMils(float nanoMeters)
  {
    return nanoMeters / (float)(_MILS_TO_NANOMETERS);
  }

  /**
   * @author George A. David
   */
  public static double convertNanoMetersToMils(double nanoMeters)
  {
    return nanoMeters / _MILS_TO_NANOMETERS;
  }

  /**
   * @author Patrick Lacz
   */
  public static float convertNanometersToMillimeters(float nanometers)
  {
    return nanometers * (float)_NANOMETERS_TO_MILLIMETERS;
  }

  /**
   * @author Patrick Lacz
   */
  public static double convertNanometersToMillimeters(double nanometers)
  {
    return nanometers * _NANOMETERS_TO_MILLIMETERS;
  }

  /**
   * @author Patrick Lacz
   */
  public static double convertMillimetersToNanometers(double millimeters)
  {
    return millimeters * _MILLIMETERS_TO_NANOMETERS;
  }

  /**
   * @author Patrick Lacz
   */
  public static float convertMillimetersToNanometers(float millimeters)
  {
    return millimeters * (float)_MILLIMETERS_TO_NANOMETERS;
  }


  /**
  * Rounds a double to the specified number of decimal places, for display formatting purposes.
  * It calls Math.round which returns a long, so this will not work for values approaching
  *  the min/max values of a long (after multiplying -- see the last line).
  * Also, if the client uses Double.toString(result), this will format based on value ranges,
  *  (this works best if the range is 10e-3 to 10e7).
  *
  * @param value  The double value to be rounded.
  * @param decimalPlaces  The number of decimal places desired (must be >=0).
  * @return  The rounded double value.
  * @author  Matt Snook
  */
  public static double roundToPlaces(double value, int decimalPlaces)
  {
    Assert.expect(decimalPlaces >= 0);
    // preserve value (toString="NaN"), otherwise it would return 0.0 due to Math.round()...
    if (Double.isNaN(value))
      return value;

    double powerOfTen = 1.0;
    while (decimalPlaces > 0)
    {
      powerOfTen *= 10.0;
      decimalPlaces--;
    }

    return Math.round(value * powerOfTen) / powerOfTen;
  }

  /**
   * @author Bill Darbie
   */
  public static int roundDoubleToInt(double value)
  {
    long longValue = Math.round(value);

    if (longValue > Integer.MAX_VALUE)
      longValue = Integer.MAX_VALUE;
    if (longValue < Integer.MIN_VALUE)
      longValue = Integer.MIN_VALUE;

    return (int)longValue;
  }

  /**
  * Rounds a float to the specified number of decimal places, for display formatting purposes.
  * It calls Math.round which returns an int, so this will not work for values approaching
  *  the min/max values of an int (after multiplying -- see the last line).
  * Also, if the client uses Float.toString(result), this will format based on value ranges,
  *  (this works best if the range is 10e-3 to 10e7).
  *
  * @param value  The float value to be rounded.
  * @param decimalPlaces  The number of decimal places desired (must be >=0).
  * @return  The rounded float value.
  * @author  Matt Snook
  */
  public static float roundToPlaces(float value, int decimalPlaces)
  {
    Assert.expect(decimalPlaces >= 0);
    if (Float.isNaN(value))  // preserve value (toString="NaN"), otherwise it would return 0.0 due to Math.round()...
      return value;
    float powerOfTen = 1.0F;

    while (decimalPlaces > 0)
    {
      powerOfTen *= 10.0F;
      decimalPlaces--;
    }

    return Math.round(value * powerOfTen) / powerOfTen;
  }

  /**
   * The convex hull is a list of points that make up the
   * smallest polygon that encompasses all the points.
   * In this case however, we want the smallest convex hull
   * that will encompass the reference point. NOTE:: the coordinates
   * passed in must be sorted by distance to the reference point for
   * this algorithm to work properly.
   * @author George A. David
   */
  public static List<DoubleCoordinate> getSmallestConvexHull(Collection<DoubleCoordinate> coordinatesSortedByDistanceToReference, DoubleCoordinate referenceCoordinate)
  {
    Assert.expect(coordinatesSortedByDistanceToReference != null);
    Assert.expect(referenceCoordinate != null);

    List<DoubleCoordinate> convexHull = new LinkedList<DoubleCoordinate>();
    List<DoubleCoordinate> candidatePoints = new LinkedList<DoubleCoordinate>();
    for(DoubleCoordinate coord : coordinatesSortedByDistanceToReference)
    {
      candidatePoints.add(coord);
      if(candidatePoints.size() >= 3)
      {
        convexHull = getConvexHull(candidatePoints);

        // check to see if the reference coordinate is contianed
        // within the current convex hull, if so, we are done.
        GeneralPath path = null;
        for(DoubleCoordinate convexPoint : convexHull)
        {
          if(path == null)
          {
            path = new GeneralPath();
            path.moveTo((float)convexPoint.getX(), (float)convexPoint.getY());
          }
          else
            path.lineTo((float)convexPoint.getX(), (float)convexPoint.getY());
        }
        path.closePath();
        if(path.contains(referenceCoordinate.getX(), referenceCoordinate.getY()))
          break;
      }
    }

    return convexHull;
  }

  /**
   * The convex hull is a list of points that make up the
   * smallest polygon that encompasses all the points.
   * @author George A. David
   */
  public static List<DoubleCoordinate> getConvexHull(Collection<DoubleCoordinate> coordinates)
  {
    // first find the point with the min y, and then min x.
    DoubleCoordinate lowestCoord = null;
    for(DoubleCoordinate coord : coordinates)
    {
      if(lowestCoord == null ||
         (coord.getY() < lowestCoord.getY()) ||
         (fuzzyEquals(coord.getY(), lowestCoord.getY()) && coord.getX() < lowestCoord.getX()))
      {
        lowestCoord = coord;
      }
    }

    Assert.expect(lowestCoord != null);
    List<DoubleCoordinate> sortedCoords = new LinkedList<DoubleCoordinate>(coordinates);
    // ok, now sort the coordinates based on the angle they make with the lowest coordinate and the horizon
    Collections.sort(sortedCoords, new DoubleCoordinateAngleComparator(lowestCoord));

    LinkedList<DoubleCoordinate> convexHull = new LinkedList<DoubleCoordinate>();
    for(DoubleCoordinate coord : sortedCoords)
    {
      if(convexHull.size() < 3)
        convexHull.addFirst(coord);
      else
      {
        // check the angle
        DoubleCoordinate lastCoord = convexHull.get(0);
        DoubleCoordinate prevLastCoord = convexHull.get(1);
        boolean done = false;;
        while(done == false)
        {
          if (MathUtil.isFuzzyColinear(prevLastCoord.getX(), prevLastCoord.getY(),
                                       lastCoord.getX(), lastCoord.getY(),
                                       coord.getX(), coord.getY()) == false)
          {
            int relativeCCW = Line2D.relativeCCW(prevLastCoord.getX(), prevLastCoord.getY(),
                                                 lastCoord.getX(), lastCoord.getY(),
                                                 coord.getX(), coord.getY());
            if (relativeCCW == 1) // the line has to turn clockwise to add this point to the convex hull
            // keep in mind, the documentation says -1 is clockwise, but that's for
            // the screen coodinate system, where y up is negative, when y up is positive,
            // 1 is actually clockwise!
            {
              convexHull.removeFirst();
              if (convexHull.size() < 3)
              {
                convexHull.addFirst(coord);
                done = true;
              }
              else
              {
                lastCoord = prevLastCoord;
                prevLastCoord = convexHull.get(1);
              }
            }
            else // line turns counter-clockwise or not at all
            {
              convexHull.addFirst(coord);
              done = true;
            }
          }
          else // the points are colinear
          {
            convexHull.addFirst(coord);
            done = true;
          }
        }
      }
    }

    return convexHull;
  }

  /**
   * @author George A. David
   */
  public static boolean isFuzzyColinear(List<DoubleCoordinate> coords)
  {
    Assert.expect(coords != null);
    Assert.expect(coords.size() > 2);
    boolean isColinear = false;

    DoubleCoordinate firstCoord = coords.get(0);
    DoubleCoordinate secondCoord = coords.get(1);

    for(DoubleCoordinate coord : coords)
    {
      if(isFuzzyColinear(firstCoord.getX(), firstCoord.getY(),
                         secondCoord.getX(), secondCoord.getY(),
                         coord.getX(), coord.getY()))
        isColinear = true;
      else
      {
        isColinear = false;
        break;
      }
    }
    return isColinear;
  }

  /**
   * PE:  Note that this function really just sees how close the third
   * point is to the line defined by the first two points.  In my mind, this
   * is different than seeing how colinear three points are.  This method
   * does NOT fit a line to all three points and measure how much the points
   * deviate from that best-fit line, which is what I'd expect.
   *
   * @author George A. David
   */
  public static boolean isFuzzyColinear(double x1, double y1,
                                      double x2, double y2,
                                      double x3, double y3)
  {
    double distance = Line2D.ptLineDist(x1, y1, x2, y2, x3, y3);
    return MathUtil.fuzzyEquals(0.0, distance);
  }

  /**
   * This method compares to doubles to see if they are equal.  It uses
   * the values of the doubles themselves to determine how close they need to
   * be to be considered equal.
   *
   * @return true if the values are equal enough.
   * @author Bill Darbie
   */
  public static boolean fuzzyEquals(double lhs, double rhs)
  {
    double diff = Math.abs(lhs - rhs);

    // find the maximium double between lhs and rhs
    double maxDouble = Math.max(Math.abs(lhs), Math.abs(rhs));

    if (diff <= _FUZZY_LIMIT * maxDouble)
      return true;
    else
      return false;
  }

  /**
   * This method compares to doubles to see if they are equal.  It uses
   * the value passed in to determine how close they need to
   * be to be considered equal.  Only use this method if the 2 parameter version
   * of this method will not work for you.
   * @return true if the values are equal enough.
   * @author Bill Darbie
   */
  public static boolean fuzzyEquals(double lhs, double rhs, double differenceAllowedToBeConsideredEqual)
  {
    double diff = Math.abs(lhs - rhs);

    if (diff <= Math.abs(differenceAllowedToBeConsideredEqual))
      return true;
    else
      return false;
  }

  /**
   * @author George A. David
   */
  public static boolean fuzzyContains(java.awt.Shape outerShape, java.awt.Shape innerShape)
  {
    return fuzzyContains(outerShape, innerShape, _FUZZY_LIMIT);
  }

  /**
   * @author Bill Darbie
   */
  public static boolean fuzzyContains(java.awt.Shape outerShape, java.awt.Shape innerShape, double differenceAllowedToBeConsideredEqual)
  {
    Assert.expect(outerShape != null);
    Assert.expect(innerShape != null);
    Assert.expect(differenceAllowedToBeConsideredEqual >= 0.0);

    Rectangle2D outerBounds = outerShape.getBounds2D();
    Rectangle2D innerBounds = innerShape.getBounds2D();

    // expand the outer bounds to do a fuzzy compare!
    outerBounds.setRect(outerBounds.getMinX() - differenceAllowedToBeConsideredEqual,
                        outerBounds.getMinY() - differenceAllowedToBeConsideredEqual,
                        outerBounds.getWidth() + 2 * differenceAllowedToBeConsideredEqual,
                        outerBounds.getHeight() + 2 * differenceAllowedToBeConsideredEqual);

    return outerBounds.contains(innerBounds);
  }

  /**
   * Determines if the one double is greater than another double
   * taking into account the _FUZZY_LIMIT to determine if they are
   * equal.
   * @author George A. David
   */
  public static boolean fuzzyGreaterThan(double lhs, double rhs)
  {
    return (lhs > rhs && fuzzyEquals(lhs, rhs) == false);
  }

  /**
   * @author Erica Wheatcroft
   */
  public static boolean fuzzyGreaterThan(double lhs, double rhs, double differenceAllowedToBeConsideredEqual)
  {
    return (lhs > rhs && fuzzyEquals(lhs, rhs, differenceAllowedToBeConsideredEqual) == false);
  }


  /**
   * Determines if the one double is greater than or equal to another double
   * taking into account the _FUZZY_LIMIT to determine if they are
   * equal.
   *
   * @author Matt Wharton
   */
  public static boolean fuzzyGreaterThanOrEquals(double lhs, double rhs)
  {
    return (lhs > rhs || fuzzyEquals(lhs, rhs));
  }

  /**
   * @author Erica Wheatcroft
   */
  public static boolean fuzzyGreaterThanOrEquals(double lhs, double rhs, double differenceAllowedToBeConsideredEqual)
  {
    return (lhs > rhs || fuzzyEquals(lhs, rhs, differenceAllowedToBeConsideredEqual));
  }

  /**
   * Determines if the one double is less than another double
   * taking into account the _FUZZY_LIMIT to determine if they are
   * equal.
   * @author George A. David
   */
  public static boolean fuzzyLessThan(double lhs, double rhs)
  {
    return (lhs < rhs && fuzzyEquals(lhs, rhs) == false);
  }

  /**
   * @author Erica Wheatcroft
   */
  public static boolean fuzzyLessThan(double lhs, double rhs, double differenceAllowedToBeConsideredEqual)
  {
    return (lhs < rhs && fuzzyEquals(lhs, rhs, differenceAllowedToBeConsideredEqual) == false);
  }

  /**
   * Determines if the one double is less than or equal to another double
   * taking into account the _FUZZY_LIMIT to determine if they are
   * equal.
   * @author George A. David
   */
  public static boolean fuzzyLessThanOrEquals(double lhs, double rhs)
  {
    return (lhs < rhs || fuzzyEquals(lhs, rhs));
  }

  /**
   * @author Erica Wheatcroft
   */
  public static boolean fuzzyLessThanOrEquals(double lhs, double rhs, double differenceAllowedToBeConsideredEqual)
  {
    return (lhs < rhs || fuzzyEquals(lhs, rhs, differenceAllowedToBeConsideredEqual));
  }

  /**
   * For review, a quadratic equation is the following:
   * ax^2 + bx + c = y
   * Given three points, calculate the cooeficients a, b, and c
   * for the quadratic equation.
   * @author George A. David
   */
  public static void calculateQuadraticEquationCoefficients(DoubleCoordinate[] coordinates,
                                                            DoubleRef coefficientA,
                                                            DoubleRef coefficientB,
                                                            DoubleRef coefficientC) throws DivideByZeroException
  {
    Assert.expect(coordinates != null);
    Assert.expect(coordinates.length == 3);

    double x1 = coordinates[0].getX();
    double y1 = coordinates[0].getY();

    double x2 = coordinates[1].getX();
    double y2 = coordinates[1].getY();

    double x3 = coordinates[2].getX();
    double y3 = coordinates[2].getY();

    calculateQuadraticEquationCoefficients(x1, y1,
                                           x2, y2,
                                           x3, y3,
                                           coefficientA,
                                           coefficientB,
                                           coefficientC);
  }

  /**
   * For review, a quadratic equation is the following:
   * ax^2 + bx + c = y
   * Given three points, calculate the cooeficients a, b, and c
   * for the quadratic equation.
   * @author George A. David
   */
  public static void calculateQuadraticEquationCoefficients(double x1, double y1,
                                                            double x2, double y2,
                                                            double x3, double y3,
                                                            DoubleRef coefficientA,
                                                            DoubleRef coefficientB,
                                                            DoubleRef coefficientC) throws DivideByZeroException
  {
    if(x1 == x2 && x2 == x3)
    {
      // this is a horizontal line that will
      // lead to a divide by zero
      coefficientA.setValue(0.0);
      coefficientB.setValue(0.0);
      coefficientC.setValue(0.0);
      throw new DivideByZeroException();
    }
    double a = y1/((x1 - x2) * (x1 - x3)) + y2/((x2 - x1) * (x2 - x3)) + y3/((x3 - x1) * (x3 - x2));
    double b = -( (y1 * (x2 + x3)) / ((x1 - x2) * (x1 - x3)) +
                  (y2 * (x1 + x3)) / ((x2 - x1) * (x2 - x3)) +
                  (y3 * (x1 + x2)) / ((x3 - x1) * (x3 - x2)) );
    double c = (y1 * x2 * x3) / ((x1 - x2) * (x1 - x3)) +
               (y2 * x1 * x3) / ((x2 - x1) * (x2 - x3)) +
               (y3 * x1 * x2) / ((x3 - x1) * (x3 - x2));

    coefficientA.setValue(a);
    coefficientB.setValue(b);
    coefficientC.setValue(c);
  }


  /**
   * given 3 2-dimensional coordinates, calculate
   * the cross product of all three.
   * @author George A. David
   */
  public static double calculateCrossProductOf2dCoordinates(IntCoordinate point1,
                                                            IntCoordinate point2,
                                                            IntCoordinate point3)
  {
    Assert.expect(point1 != null);
    Assert.expect(point2 != null);
    Assert.expect(point3 != null);

    double x1 = point1.getX();
    double y1 = point1.getY();

    double x2 = point2.getX();
    double y2 = point2.getY();

    double x3 = point3.getX();
    double y3 = point3.getY();

    return calculateCrossProductOf2dCoordinates(x1, y1,
                                                x2, y2,
                                                x3, y3);
  }

  /**
   * given 3 2-dimensional coordinates, calculate
   * the cross product of all three.
   * @author George A. David
   */
  public static double calculateCrossProductOf2dCoordinates(double x1, double y1,
                                                            double x2, double y2,
                                                            double x3, double y3)
  {
    double deltaX1;
    double deltaY1;
    double deltaX2;
    double deltaY2;

    deltaX1 = x2 - x1;
    deltaY1 = y2 - y1;
    deltaX2 = x3 - x1;
    deltaY2 = y3 - y1;

    return (deltaX1 * deltaY2) - (deltaX2 * deltaY1);
  }

  /**
   * Given 2 2d coordinate, we calculate the distance squared.
   * This is useful when trying to determine which two points are
   * closer together. In this case, you don't need to know the
   * exact distance. This omits the unecessary step of calculating
   * the square root of the final value.
   * @author George A. David
   */
  public static double calculateDistanceSquared(double x1, double y1,
                                                double x2, double y2)
  {
    double dx = x1 - x2;
    double dy = y1 - y2;
    double distanceSquared = dx * dx + dy * dy;

    return distanceSquared;
  }

  /**
   * Return the centroid of the given IntCoordinates.
   *
   * @author Peter Esbensen
   */
  public static IntCoordinate calculateCentroid(Collection<IntCoordinate> intCoordinates)
  {
    Assert.expect(intCoordinates != null);

    double x = 0.0;
    double y = 0.0;
    int numCoordinates = intCoordinates.size();
    Assert.expect(numCoordinates > 0);

    for (IntCoordinate intCoordinate : intCoordinates)
    {
      x += intCoordinate.getX();
      y += intCoordinate.getY();
    }
    return (new IntCoordinate( (int)Math.round(x/numCoordinates),
                               (int)Math.round(y/numCoordinates) ));
  }

  /**
   * Given 2 2d coordinates, we will calculate the distance
   * only use this if you need to now the exact distance between
   * to points, if you just need to determine which points are
   * closer together, then use calculateDistanceSquared() instead.
   * That function is faster because it doesn't do the unecessary
   * step of calculating the square root.
   * @author George A. David
   */
  public static double calculateDistance(double x1, double y1,
                                         double x2, double y2)
  {
    return Math.sqrt(calculateDistanceSquared(x1, y1, x2, y2));
  }

  /**
   * @author Bill Darbie
   */
  public static double getDegreesWithin0To359(double degrees)
  {
    degrees = degrees % 360;

    if (degrees < 0)
      degrees += 360;

    return degrees;
  }

  /**
   * @author Bill Darbie
   */
  public static int getDegreesWithin0To359(int degrees)
  {
    degrees = degrees % 360;
    if (degrees < 0)
      degrees += 360;

    return degrees;
  }

  /**
   * @author Peter Esbensen
   */
  public static double getDeterminantOfThreeByThreeMatrix(
      double row1col1, double row1col2, double row1col3,
      double row2col1, double row2col2, double row2col3,
      double row3col1, double row3col2, double row3col3)
  {
    double determinant = (row1col1 * row2col2 * row3col3) +
                         (row1col3 * row2col1 * row3col2) +
                         (row1col2 * row2col3 * row3col1) -
                         (row1col3 * row2col2 * row3col1) -
                         (row1col1 * row2col3 * row3col2) -
                         (row1col2 * row2col1 * row3col3);
    return determinant;
  }

  /**
   * Fit a quadratic curve to three given points.  The results are returned by
   * reference as the three coeffecients in the following quadratic equation:
   *
   * y = Ax^2 + Bx + C
   *
   * @param x1 is the x coordinate for the first point
   * @param y1 is the y coordinate for the first point
   * @param x2 is the x coordinate for the second point
   * @param y2 is the y coordinate for the second point
   * @param x3 is the x coordinate for the third point
   * @param y3 is the y coordinate for the third point
   * @param A will be the computed value of A in the equation above
   * @param B will be the computed value of B in the equation above
   * @param C will be the computed value of C in the equation above
   *
   * @author Peter Esbensen
   */
  public static void fitQuadraticToCoordinates(float x1, float y1,
                                               float x2, float y2,
                                               float x3, float y3,
                                               DoubleRef A,
                                               DoubleRef B,
                                               DoubleRef C)
  {
    Assert.expect(A != null);
    Assert.expect(B != null);
    Assert.expect(C != null);

    Assert.expect( ((x1 != x2) && (x1 != x3) && (x2 != x3)), "do not try to fit quadratic to points with the same x!" );

    // for a fun exercise, try writing down the three equations with the three points in them,
    // solve for A, B, C, and you should get the following formulas:
    double computedA =  y1/((x1-x2)*(x1-x3)) + y2/((x2-x1)*(x2-x3)) + y3/((x3-x1)*(x3-x2));
    double computedB =  -( (y1*(x2+x3)) / ((x1-x2)*(x1-x3)) +
           (y2*(x1+x3)) / ((x2-x1)*(x2-x3)) +
           (y3*(x1+x2)) / ((x3-x1)*(x3-x2)) );
    double computedC =  (y1*x2*x3)/((x1-x2)*(x1-x3)) + (y2*x1*x3)/((x2-x1)*(x2-x3))+(y3*x1*x2)/((x3-x1)*(x3-x2));
    A.setValue(computedA);
    B.setValue(computedB);
    C.setValue(computedC);
  }

  /**
   * Converts the specified size in nanometers to pixels using the following formula:
   *
   * sizeInPixels = 1 + round(sizeInNanoMeters / nanoMetersPerPixel)
   *
   * The rationale for this as per John Heumann:
   *
   *  1) While region of length D could fall on exactly ceil (D / P) pixels, it can do
   *     so only in a very restricted number of positions. (One side of the image of D
   *     must lie exactly on a pixel boundary). Conversely, there are an infinite
   *     number of positions in which neither edge corresponds with a pixel boundary.
   *  2) The image of any physical region, no matter how small, will always project
   *     onto at least one pixel.
   *  3) This is the convention used by almost all image processing packages. A 1x1 pixel
   *     image has no physical extent, so rotation is a no-op.
   *  4) Whether to use ceil or round is somewhat debatable, but I suggest we standardize on
   *     round, since it's statistically more likely to be correct. (I.e. a D / P = 4.1 region can be
   *     aligned to fall on parts of 6 pixels, but its much more likely to fall on parts of 5). The
   *     only exception might be in situtations when you want to compute largest possible size.
   *
   * @author Matt Wharton
   */
  public static int convertNanoMetersToPixelsUsingRound(int sizeInNanoMeters, double nanoMetersPerPixel)
  {
    Assert.expect(nanoMetersPerPixel > 0);

    if (sizeInNanoMeters > 0)
      return 1 + (int)Math.round((double)sizeInNanoMeters / nanoMetersPerPixel);
    else if (sizeInNanoMeters < 0)
      return -1 + (int)Math.round((double)sizeInNanoMeters / nanoMetersPerPixel);
    else return 1;
  }

  /**
   * Converts the specified size in nanometers to pixels using the following formula:
   *
   * distanceInPixels = 1 + ceil(sizeInNanoMeters / nanoMetersPerPixel)
   *
   * The rationale for this as per John Heumann:
   *
   *  1) While region of length D could fall on exactly ceil (D / P) pixels, it can do
   *     so only in a very restricted number of positions. (One side of the image of D
   *     must lie exactly on a pixel boundary). Conversely, there are an infinite
   *     number of positions in which neither edge corresponds with a pixel boundary.
   *  2) The image of any physical region, no matter how small, will always project
   *     onto at least one pixel.
   *  3) This is the convention used by almost all image processing packages. A 1x1 pixel
   *     image has no physical extent, so rotation is a no-op.
   *  4) Whether to use ceil or round is somewhat debatable, but I suggest we standardize on
   *     round, since it's statistically more likely to be correct. (I.e. a D / P = 4.1 region can be
   *     aligned to fall on parts of 6 pixels, but its much more likely to fall on parts of 5). The
   *     only exception might be in situtations when you want to compute largest possible size.
   *
   * @author Matt Wharton
   */
  public static int convertNanoMetersToPixelsUsingCeil(int sizeInNanoMeters, double nanoMetersPerPixel)
  {
    Assert.expect(sizeInNanoMeters >= 0);
    Assert.expect(nanoMetersPerPixel > 0);

    return 1 + (int)Math.ceil((double)sizeInNanoMeters / nanoMetersPerPixel);
  }

  /**
   * @author Sunit Bhalla
   */
  public static int convertMillimetersToPixelsUsingCeil(float millimeters, double nanoMetersPerPixel)
  {
    Assert.expect(millimeters >= 0.0);
    Assert.expect(nanoMetersPerPixel > 0);

    float mils = MathUtil.convertMillimetersToMils(millimeters);
    int nanometers = MathUtil.convertMilsToNanoMetersInteger(mils);
    int pixels = MathUtil.convertNanoMetersToPixelsUsingCeil(nanometers, nanoMetersPerPixel);
    return pixels;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static double convertBytesToMegabytes(int bytes)
  {
    return (double)bytes / (1024 * 1024);
  }

  /**
   * @author George A. David
   */
  public static void swap(int rhs, int lhs)
  {
    int temp = rhs;
    rhs = lhs;
    lhs = temp;
  }

  /**
   * Use Law of Cosine to figure out angle where...
   *
   *      c^2 = a^2 + b^2 - 2ab�cos(theta)
   *
   * @author Roy Williams
   *
   * @return the angle theta (in degrees) for vertex opposite of point c in degrees.
   */
  public static double lawOfCosines(double distanceOppositeVertex, double d1, double d2)
  {
    return Math.toDegrees(Math.acos((d1 * d1 + d2 * d2 - distanceOppositeVertex * distanceOppositeVertex) / (2 * d1 * d2)));
  }


  /**
   * This is to calculate the correlation score between 2 set of data.
   * The formula is as
   *
   *                                         (n * E(x*y)) - (Ex * Ey)
   *  Correlation Score =  ----------------------------------------------------------------
   *                       SqrtRoot ( (n Ex2 - Sqrt(Ex))) * SqrtRoot ( (n Ey2 - Sqrt(Ey)) )
   *
   * This value range from -1 to 1.
   * 1 means positive linear match and
   * 0 mean totally non linear match.
   * -1 means perfect negative linear match
   *
   * @author Poh Kheng
   */
  public static double correlation(double x[], double y[])
  {
    Assert.expect(x.length > 0);
    Assert.expect(y.length > 0);


    double totalX = sumTotal(x);
    double totalY = sumTotal(y);
    double totalXX = sumPowerOfTwoTotal(x);
    double totalYY = sumPowerOfTwoTotal(y);
    double totalXY = sumMultipleTotal(x,y);
    int totalSample = x.length;

    double correlation = (((totalSample*totalXY) - (totalX*totalY)) /
            (Math.sqrt(((totalSample*totalXX) - Math.pow(totalX,2))) * Math.sqrt(((totalSample*totalYY) - Math.pow(totalY,2)))));

    double returnCorrelation = MathUtil.roundToPlaces(correlation, 4);

    Assert.expect(returnCorrelation >= MathUtil._CORRELATION_MIN_VALUE);
    Assert.expect(returnCorrelation <= MathUtil._CORRELATION_MAX_VALUE);

    return returnCorrelation;

  }

  /**
   * This would sum up all the value in the array
   * @author Poh Kheng
   */
  public static double sumTotal(double x[])
  {
    Assert.expect(x.length > 0);
    double total = 0;

    for(int i=0; i<x.length; i++)
      total += x[i];

    return total;
  }

  /**
   * This would sum up all the multiple value x * y in the array
   * @author Poh Kheng
   */
  public static double sumMultipleTotal(double x[], double y[])
  {
    Assert.expect(x.length > 0);
    Assert.expect(y.length > 0);
    double total = 0;

    for(int i=0; i<x.length; i++)
      total += x[i]*y[i];

    return total;
  }

  /**
   * @author Poh Kheng
   */
  public static double sumPowerOfTwoTotal(double x[])
  {
    Assert.expect(x.length > 0);
    double total = 0;

    for(int i=0; i<x.length; i++)
      total += Math.pow(x[i], 2);

    return total;
  }

  /**
   * @author Poh Kheng
   */
  public static double sumSquareRootRectangleArea(int width, int length)
  {
    Assert.expect(width > 0);
    Assert.expect(length > 0);

    return Math.sqrt( ((double)width * (double)length) );
  }

  /**
   * @author Poh Kheng
   *
   *          . -------------- .      --
   *        /                   \
   *       |                     |    SemiCircleDiameter
   *        \                   /
   *          ` -------------- `      --
   *           |  rectLength  |
   *
   */
  public static double sumSquareRootAbroundArea(int width, int length)
  {
    Assert.expect(width > 0);
    Assert.expect(length > 0);
    double totalArea = -1;
    double semiCircleDiameter = -1;
    double rectLength = -1;

    // This is for perfect circle
    if(width == length)
    {
      totalArea = Math.PI * (double)width * (double)length;
    }
    else
    {
      // This if for Abround case where we need to calculate the semi circle and the rect
      if (width < length)
      {
        semiCircleDiameter = width;
        rectLength = length - width;
      }
      else
      {
        semiCircleDiameter = length;
        rectLength = width - length;
      }

      // total area for 2 semi circle
      totalArea = Math.PI * semiCircleDiameter/2 * semiCircleDiameter/2;

      // plus the rect area
      totalArea = totalArea + (rectLength * semiCircleDiameter);

    }



    return Math.sqrt(totalArea);
  }

  /**
   * This would remove the factorial member in the array
   * @author Poh Kheng
   */
  public static double[] removeMemberBasedOnFactorial(double x[], int factorial)
  {
    Assert.expect(x.length > 0);
    Assert.expect(factorial > 0);
    double[] returnValueArray;
    int count = 0;

    double numberOfArray = (x.length) * ((double)(factorial-1)/(double)factorial);
    returnValueArray =  new double[(int)numberOfArray];

    for(int i=0; i<x.length; i++) {

      if((i+1) % factorial != 0)
      {
        returnValueArray[count] = x[i];
        count++;
      }

    }

    return returnValueArray;
  }

  /**
   * This would get the factorial member in the array
   * @author Poh Kheng
   */
  public static double[] getMemberBasedOnFactorial(double x[], int factorial)
  {
    Assert.expect(x.length > 0);
    Assert.expect(factorial > 0);
    double[] returnValueArray;
    int count = 0;

    double numberOfArray = (x.length) * ((double)(1)/(double)factorial);
    returnValueArray =  new double[(int)numberOfArray];

    for(int i=0; i<x.length; i++) {
      if((i+1) % factorial == 0)
      {
        returnValueArray[count] = x[i];
        count++;
      }
    }

    return returnValueArray;
  }

  /**
   * This would return the percentage match between the 2 set of data
   *
   * @author Poh Kheng
   */
  public static double numberOfMatchPecentage(double x[], double y[])
  {
    Assert.expect(x.length > 0);
    Assert.expect(y.length > 0);
    Assert.expect(x.length == y.length);
    double numberOfMatch = 0;

    for(int i=0; i < x.length; i++)
    {
      if(x[i] == y[i])
        numberOfMatch++;
    }

    return numberOfMatch/x.length * 100 ;
  }


  /**
   * @author Ronald Lim
   */
  public static String convertByteToHex (byte b)
  {
    char[] nibbles ={'0','0'};

    nibbles[1] = convertNibbleToHex ((int)(0x0F & b));
    nibbles[0] = convertNibbleToHex ((int)(0x0F & (b>>4)));

    return new String (nibbles);

  }

  /**
   * @author Ronald Lim
   */
  private static char convertNibbleToHex (int n)
  {
    return _HEX_ARRAY[n];
  }

  /**
   * Convert a base ten integer to a hex character
   * @param	baseTenInteger the base ten integer to convert.
   * @author Wei Chin, Chong   
   */
  public static char baseTenIntegerToHex(int baseTenInteger)
  {
    return _hexDigit[(baseTenInteger & 0xF)];
  }

  /** A table of hex digits */
  private static final char[] _hexDigit =
      {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
}
