package com.axi.util.math;

import com.axi.util.*;

/**
 * @author Ying-Huan.Chu
 */
public class GaussianCurveFunction implements DifferentiableFunction
{

  public int numberOfParameters()
  {
    return 4;
  }

  // Following 2 functions define a Gaussian and its gradient
  // with respect to its parameters/
  // params a[] are amplitude, mean, standard deviation, and background
  public double evalFunction(double[] parameters, double x)
  {
    assert parameters.length == numberOfParameters();

    double a0 = parameters[0];
    double a1 = parameters[1];
    double a2 = parameters[2];
    double a3 = parameters[3];

    double delta = x - a1;
    return a0 * Math.exp(-delta * delta / (2 * a2 * a2)) + a3;

  }

  public void evalGradient(double[] parameters, double x, double[] result)
  {

    double a0 = parameters[0];
    double a1 = parameters[1];
    double a2 = parameters[2];
    double a3 = parameters[3];

    double sigmaInv = 1.0 / a2;
    double delta = (x - a1) * sigmaInv;
    double deltaSq = delta * delta;
    double t = Math.exp(-0.5 * deltaSq);

    result[0] = t;             // gradient wrt amplitude
    t *= a0 * sigmaInv;
    result[1] = delta * t;     // gradient wrt mean
    result[2] = deltaSq * t;   // gradient wrt sigma
    result[3] = 1.0;           // gradient wrt background
  }
}
