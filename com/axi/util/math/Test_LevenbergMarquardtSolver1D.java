package com.axi.util.math;

import com.axi.util.UnitTest;

import java.io.BufferedReader;
import java.io.PrintWriter;

/**
 * v810 unit test for the 1D Levenberg-Marquardt solver
 *
 * @author Rick Gaudette
 */
public class Test_LevenbergMarquardtSolver1D extends UnitTest
{

  public void test(BufferedReader is, PrintWriter os)
  {
    testErfEdge();
    testLaplaceEdge();
    testGaussianCurve();
    System.out.println("Test_LevenbergMarquardtSolver1D complete successfully");
  }

  public void testErfEdge()
  {
    int n_elem = 31;
    int half = n_elem / 2;
    double[] x = new double[n_elem];
    double[] y = new double[n_elem];

    //ErfEdgeFunction edgeFunction = new ErfEdgeFunction();
    ErfEdgeFunction edgeFunction = new ErfEdgeFunction();
    double[] a = new double[4];
    // amplitude = parameters[0];
    // mu = parameters[1];
    // sigma = parameters[2];
    // offset = parameters[3];
    a[0] = 2.0;
    a[1] = 0.0;
    a[2] = 1.0;
    a[3] = -1.0;

    for (int i = 0; i < n_elem; i++)
    {
      x[i] = (i - half) / 3.0;
      y[i] = edgeFunction.evalFunction(a, x[i]);
    }

    LevenbergMarquardtSolver1D lm_solver = new LevenbergMarquardtSolver1D(edgeFunction);
    lm_solver.setConvergenceCriteria(1E-10, 0.0, 1E-3, 30);

    double[] init_a = new double[4];

    for (double amplitude = 1.8; amplitude <= 2.2; amplitude += 0.1)
    {
      init_a[0] = amplitude;

      for (double mu = -0.7; mu <= 0.7; mu += 0.1)
      {
        init_a[1] = mu;

        for (double sigma = 0.4; sigma <= 1.6; sigma += 0.1)
        {
          init_a[2] = sigma;

          for (double offset = -1.5; offset <= -0.5; offset += 0.1)
          {
            init_a[3] = offset;

            lm_solver.solve(x, y, init_a);
            double[] a_est = lm_solver.get_a();

            if (!arrays_equal(a, a_est, 1E-3))
            {
              System.out.println("Initial estimate:");
              print_array(init_a);

              System.out.println("Final estimate:");
              print_array(a_est);

              System.out.printf("Iterations: %d\n", lm_solver.get_nIter());
              System.out.printf("Chi squared: %f\n", lm_solver.get_chiSq());
            }
          }
        }
      }
    }
  }

  public void testLaplaceEdge()
  {
    int n_elem = 31;
    int half = n_elem / 2;
    double[] x = new double[n_elem];
    double[] y = new double[n_elem];

    //ErfEdgeFunction edgeFunction = new ErfEdgeFunction();
    LaplaceEdgeFunction edgeFunction = new LaplaceEdgeFunction();
    double[] a = new double[4];
    a[0] = 2.0;
    a[1] = 0.0;
    a[2] = 1.0;
    a[3] = -1.0;

    for (int i = 0; i < n_elem; i++)
    {
      x[i] = (i - half) / 3.0;
      y[i] = edgeFunction.evalFunction(a, x[i]);
    }

    LevenbergMarquardtSolver1D lm_solver = new LevenbergMarquardtSolver1D(edgeFunction);
    lm_solver.setConvergenceCriteria(1E-10, 0.0, 1E-3, 30);

    double[] init_a = new double[4];

    for (double amplitude = 1.8; amplitude <= 2.2; amplitude += 0.1)
    {
      init_a[0] = amplitude;

      for (double mu = -0.7; mu <= 0.7; mu += 0.1)
      {
        init_a[1] = mu;

        for (double beta = 0.4; beta <= 1.6; beta += 0.1)
        {
          init_a[2] = beta;

          for (double offset = -1.5; offset <= -0.5; offset += 0.1)
          {
            init_a[3] = offset;

            lm_solver.solve(x, y, init_a);
            double[] a_est = lm_solver.get_a();

            if (!arrays_equal(a, a_est, 1E-3))
            {
              System.out.println("Initial estimate:");
              print_array(init_a);

              System.out.println("Final estimate:");
              print_array(a_est);

              System.out.printf("Iterations: %d\n", lm_solver.get_nIter());
              System.out.printf("Chi squared: %f\n", lm_solver.get_chiSq());
            }
          }
        }
      }
    }
  }

  public void testGaussianCurve()
  {
    int n_elem = 31;
    int half = n_elem / 2;
    double[] x = new double[n_elem];
    double[] y = new double[n_elem];

    GaussianCurveFunction gaussianCurveFunction = new GaussianCurveFunction();
    double[] a = new double[4];

    a[1] = 0.0; // mu
    a[2] = 1.0; // sigma    
    //a[0] = 1 / (a[2] * Math.sqrt(2 * Math.PI)); // k
    a[0] = 1; //Amplitude
    a[3] = 0; // background offset


    for (int i = 0; i < n_elem; i++)
    {
      x[i] = (i - half) / 3.0;
      y[i] = gaussianCurveFunction.evalFunction(a, x[i]);
    }

    LevenbergMarquardtSolver1D lm_solver = new LevenbergMarquardtSolver1D(gaussianCurveFunction);
    lm_solver.setConvergenceCriteria(1E-10, 0.0, 1E-3, 30000);

    double[] init_a = new double[4];

    int count = 0;
    int passCount = 0;
    int failCount = 0;

    for (double b = 0.0; b <= 1.0; b += 0.1)
    {
      init_a[3] = b;

      for (double k = 0.2; k <= 1.0; k += 0.1)
      {
        init_a[0] = k;

        for (double mu = -0.7; mu <= 0.7; mu += 0.1)
        {
          init_a[1] = mu;

          for (double sigma = 0.4; sigma <= 1.6; sigma += 0.1)
          {
            init_a[2] = sigma;
            count++;
            lm_solver.solve(x, y, init_a);
            double[] a_est = lm_solver.get_a();

            //if (arrays_equal(a, a_est, 1E-3))
            if (arrays_equal(a, a_est, 0.1))
            {
              System.out.println("Initial estimate:");
              System.out.printf("Counter: %d\n", count);

              print_array(a);

              System.out.println("Final estimate:");
              print_array(a_est);

              System.out.printf("Iterations: %d\n", lm_solver.get_nIter());
              System.out.printf("Chi squared: %f\n", lm_solver.get_chiSq());

              passCount++;
            }
            else
            {
              failCount++;
            }
          }
        }
      }
    }
    System.out.println("Total Pass = " + passCount);
    System.out.println("Total Fail = " + failCount);
  }

  public void print_array(double[] a)
  {
    for (double v : a)
    {
      System.out.printf("%f, ", v);
    }
    System.out.println("");
  }

  public boolean arrays_equal(double[] a, double[] b, double tol)
  {
    assert a.length == b.length;
    for (int i = 0; i < a.length; i++)
    {
      double absDiff = Math.abs(b[i] - a[i]);
      if (absDiff > tol)
      {
        return false;
      }
    }
    return true;
  }

  public static void main(String[] args)
  {
    UnitTest.execute(new Test_LevenbergMarquardtSolver1D());
  }
}
