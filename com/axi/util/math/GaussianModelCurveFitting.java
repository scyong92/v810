/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.util.math;

import com.axi.util.*;

/**
 * Curve Fitting with Levenberg-Marquardt Algorithm using Gaussian Model
 *
 * @author Anthony Fong
 * @author Ying-Huan.Chu
 */
public class GaussianModelCurveFitting
{

  private static double _min = Double.MAX_VALUE;
  private static double _max = Double.MIN_VALUE;
  private static float _gaussianFitMean = 0;
  private static float _gaussianFitSigma = 0;
  private static float _gaussianFitAmplitude = 0;
  private static float _gaussianFitBackgroundLevel = 0;
  private static float _gaussianFitChiSquaredValue = 0;
  private static double _firstCurveFitMean = 0;
  private static double _gaussianFitMin = Double.MAX_VALUE;
  private static double _gaussianFitMax = Double.MIN_VALUE; 
  /**
   * @author Ying-Huan.Chu
   */
  public static double[] normalize(double[] y)
  {
    _min = Double.MAX_VALUE;
    _max = Double.MIN_VALUE;
    double[] yNormalized = new double[y.length];
    for (int i = 0; i < y.length; i++)
    {
      if (y[i] < _min)
      {
        _min = y[i];
      }
      if (y[i] > _max)
      {
        _max = y[i];
      }
    }

    for (int i = 0; i < y.length; i++)
    {
      yNormalized[i] = (y[i] - _min) / (_max - _min);
    }

    return yNormalized;
  }

  /**
   * @author Anthony Fong
   * @author Ying-Huan.Chu
   */
  public static double[] GaussianCurve(double[] x, double[] y, double maxSharpnessMag)
  {
    int arraySize = x.length;
    int halfArraySize = x.length / 2;
    GaussianCurveFunction gaussianCurveFunction = new GaussianCurveFunction();

    double[] a = new double[4];

    a[1] = maxSharpnessMag; // mu
    a[2] = 0.01; // sigma
    a[0] = 1 / (a[2] * Math.sqrt(2 * Math.PI)); // k
    a[3] = 0;

    y = normalize(y);
    LevenbergMarquardtSolver1D lm_solver = new LevenbergMarquardtSolver1D(gaussianCurveFunction);
    //lm_solver.setConvergenceCriteria(1E-10, 0.0, 1E-3, 30);
    lm_solver.setConvergenceCriteria(1E-10, 0.0, 1E-3, 300000);
    lm_solver.solve(x, y, a);
    double[] a_est = lm_solver.get_a();
    //if (arrays_equal(a, a_est, 1E-3))
    //if (arrays_equal(a, a_est, 0.1))
    {
      System.out.println("Initial estimate:");
      print_array(a);

      System.out.println("Final estimate:");
      print_array(a_est);

      System.out.printf("Iterations: %d\n", lm_solver.get_nIter());
      System.out.printf("Chi squared: %f\n", lm_solver.get_chiSq());
    }

    double[] y_result = new double[x.length];
    for (int i = 0; i < x.length; i++)
    {
      y_result[i] = _min + (_max - _min) * gaussianCurveFunction.evalFunction(a_est, x[i]);
    }

    _firstCurveFitMean = a_est[1];

    //calculateGaussianFitMean(x);
    //calculateGaussianFitSigma(y_result);
    normalize(y);
    _gaussianFitMin = _min;
    _gaussianFitMax = _max;
    setGaussianFitAmplitude((float) a_est[0]);
    setGaussianFitMean((float) a_est[1]);
    setGaussianFitSigma((float) a_est[2]);
    setGaussianFitBackgroundLevel((float) a_est[3]);

    calculateChiSquaredValue(y, y_result);
    return y_result;
  }

  /**
   * @author Anthony Fong
   * @author Ying-Huan.Chu
   */
  public static double getGaussianFitPeakX(double[] x, double[] y)
  {
    //*******************************
    // Do curve fit for the 2nd time
    //*******************************
    GaussianCurveFunction gaussianCurveFunction = new GaussianCurveFunction();

    double maxY = y[0];
    double maxSharpnessMag = x[0];
    int maxSharpnessIndex = 0;
    for (int i = 0; i < y.length; i++)
    {
      if (maxY < y[i])
      {
        maxY = y[i];
        maxSharpnessMag = x[i];
        maxSharpnessIndex = i;
      }
    }

    int r1 = 0, r2 = 0;
    for (int i = 0; i < x.length; ++i)
    {
      if (x[i] <= maxSharpnessMag)
      {
        r1 += 1;
      }
      else
      {
        r2 += 1;
      }
    }

    double[] a = new double[4];

    a[1] = _firstCurveFitMean; // mu
    a[2] = 0.01; // sigma
    a[0] = 1 / (a[2] * Math.sqrt(2 * Math.PI)); // k
    a[3] = 0;

    int arraySize = 0;
    if (r1 <= r2)
    {
      arraySize = r1;
    }
    else
    {
      arraySize = r2;
    }
    if (arraySize < 30)
    {
      arraySize = 30;
    }
    double[] x2 = new double[arraySize];
    double[] y2 = new double[arraySize];

    int count = 0;
    double rangeScale = 10;
    int fineStart = maxSharpnessIndex - (int) (arraySize / rangeScale);
    int fineEnd = maxSharpnessIndex + (int) (arraySize / rangeScale);
    
    //XCR1772 -Magnification Calibration Crash by Anthony FOng
    if((fineStart>0 && fineEnd>0 && maxSharpnessIndex>3)&&(fineStart<y.length && fineEnd<y.length && maxSharpnessIndex<y.length-3))
    {
        for (int i = fineStart; i < fineEnd; i++)
        {
          x2[count] = x[i];
          y2[count] = y[i];
          count++;
        }

        y2 = normalize(y2);
        LevenbergMarquardtSolver1D lm_solver = new LevenbergMarquardtSolver1D(gaussianCurveFunction);
        lm_solver.setConvergenceCriteria(1E-10, 0.0, 1E-3, 300000);
        lm_solver.solve(x2, y2, a);
        double[] a_est = lm_solver.get_a();
        double[] y_result = new double[y.length];
        for (int i = 0; i < arraySize; i++)
        {
          y_result[i] = _min + (_max - _min) * gaussianCurveFunction.evalFunction(a_est, x2[i]);
        }

        return a_est[1];
    }
    else
    { 
      return maxSharpnessMag;
    }
  }

  /**
   * @author Anthony Fong
   * @author Ying-Huan.Chu
   */
  public static void print_array(double[] a)
  {
    for (double v : a)
    {
      System.out.printf("%f, ", v);
    }
    System.out.println("");
  }

  /**
   * @author Anthony Fong
   * @author Ying-Huan.Chu
   */
  public static boolean arrays_equal(double[] a, double[] b, double tol)
  {
    assert a.length == b.length;
    for (int i = 0; i < a.length; i++)
    {
      double absDiff = Math.abs(b[i] - a[i]);
      if (absDiff > tol)
      {
        return false;
      }
    }
    return true;
  }

  /**
   * @author Ying-Huan.Chu
   */
  public static double[] convertListToArray(java.util.List<Double> list)
  {
    double[] array = new double[list.size()];
    for (int i = 0; i < list.size(); ++i)
    {
      array[i] = list.get(i);
    }
    return array;
  }

  /**
   * @author Ying-Huan.Chu
   */
  private static void calculateGaussianFitMean(double[] magnificationArray)
  {
    float[] array = new float[magnificationArray.length];
    for (int i = 0; i < magnificationArray.length; ++i)
    {
      array[i] = (float) magnificationArray[i];
    }
    _gaussianFitMean = StatisticsUtil.mean(array);
  }

  /**
   * @author Ying-Huan.Chu
   */
  private static void calculateGaussianFitSigma(double[] sharpnessArray)
  {
    float[] array = new float[sharpnessArray.length];
    for (int i = 0; i < sharpnessArray.length; ++i)
    {
      array[i] = (float) sharpnessArray[i];
    }
    _gaussianFitSigma = StatisticsUtil.standardDeviation(array);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public static float getGaussianFitMean()
  {
    return _gaussianFitMean;
  }

  /**
   * @author Anthony Fong
   */
  private static void setGaussianFitMean(float fitMean)
  {
    _gaussianFitMean = fitMean;
  }

  /**
   * @author Ying-Huan.Chu
   */
  public static float getGaussianFitSigma()
  {
    return _gaussianFitSigma;
  }

  /**
   * @author Anthony Fong
   */
  private static void setGaussianFitSigma(float fitSigma)
  {
    _gaussianFitSigma = fitSigma;
  }

  /**
   * @author Anthony Fong
   */
  public static float getGaussianFitAmplitude()
  {
    return _gaussianFitAmplitude;
  }

  /**
   * @author Anthony Fong
   */
  private static void setGaussianFitAmplitude(float fitAmplitude)
  {
    _gaussianFitAmplitude = fitAmplitude;
  }

  /**
   * @author Anthony Fong
   */
  public static float getGaussianFitBackgroundLevel()
  {
    return _gaussianFitBackgroundLevel;
  }

  /**
   * @author Anthony Fong
   */
  private static void setGaussianFitBackgroundLevel(float fitBackgroundLevel)
  {
    _gaussianFitBackgroundLevel = fitBackgroundLevel;
  }

  /**
   * @author Anthony Fong
   */
  public static double getGaussianFitMin()
  {
    return _gaussianFitMin;
  }

  /**
   * @author Anthony Fong
   */
  public static double getGaussianFitMax()
  {
    return _gaussianFitMax;
  }

  /**
   * @author Anthony Fong
   */
  public static double calculateChiSquaredValue(double[] observedValue, double[] expectedValue)
  {
    _gaussianFitChiSquaredValue = 0;
    assert observedValue.length == expectedValue.length;
    for (int i = 0; i < observedValue.length; i++)
    {
      _gaussianFitChiSquaredValue += Math.pow((observedValue[i] - expectedValue[i]), 2) / expectedValue[i];
    }
    return _gaussianFitChiSquaredValue;
  }

  /**
   * @author Anthony Fong
   */
  public static float getGaussianFitChiSquaredValue()
  {
    return _gaussianFitChiSquaredValue;
  }
}
