package com.axi.util.math;
import com.axi.util.*;

/**
 * An Laplace Distribution function implementation implementing a DifferentiableFunction interface
 * @author Rick Gaudette
 * @since 5.4
 */

public class LaplaceEdgeFunction implements DifferentiableFunction
{
  public int numberOfParameters()
  {
    return 4;
  }

  //
  public double evalFunction(double[] parameters, double x)
  {
    assert parameters.length == numberOfParameters();

    double amplitude = parameters[0];
    double mu = parameters[1];
    double beta = parameters[2];
    double offset = parameters[3];

    assert beta != 0.0;

    double x_shift = x - mu;
    return 0.5 * amplitude * (1.0 + Math.signum(x_shift) * (1.0 - Math.exp(- Math.abs(x_shift) / beta))) + offset;
  }

  public void evalGradient(double[] parameters, double x, double[] result)
  {
    assert parameters.length == 4;

    double amplitude = parameters[0];
    double mu = parameters[1];
    double beta = parameters[2];
    // double offset = parameters[3];

    assert beta != 0.0;

    double exp_arg = -1.0 * Math.abs(x - mu) / beta;

    result[0] = 0.5 * (1.0 + Math.signum(x - mu) * (1.0 - Math.exp(exp_arg)));
    result[1] = -1.0 * amplitude / (2.0 * beta) * Math.exp(exp_arg);
    result[2] = amplitude * (mu - x) / (2.0 * beta * beta) * Math.exp(exp_arg);
    result[3] = 1.0;

  }

  public double[] fitModel(double[] x, double[] y)
  {
    Assert.expect(y != null);
    Assert.expect(x.length == y.length);

    LevenbergMarquardtSolver1D lm_solver = new LevenbergMarquardtSolver1D(this);
    lm_solver.setConvergenceCriteria(1E-10, 0.0, 1E-3, 30);
    double[] initialParameterEstimate = coarseParameterEstimate(x, y);
    int nIter = lm_solver.solve(x, y, initialParameterEstimate);
    double[] parameterEstimate = lm_solver.get_a();

//    System.out.printf("iterations: %d\n", nIter);
//    System.out.printf("Amplitude: %.4G -> %.4G\n", initialParameterEstimate[0], parameterEstimate[0]);
//    System.out.printf("mu: %.4G -> %.4G\n", initialParameterEstimate[1], parameterEstimate[1]);
//    System.out.printf("beta: %.4G -> %.4G\n", initialParameterEstimate[2], parameterEstimate[2]);
//    System.out.printf("offset: %.4G -> %.4G\n", initialParameterEstimate[3], parameterEstimate[3]);

    return parameterEstimate;
  }

  public double[] coarseParameterEstimate(double[] x, double[] y)
  {
    double[] initialParameterEstimate = new double[4];

    double diffSign = estimateConstantRegions(y, initialParameterEstimate);

    FirstDifference firstDifference = new FirstDifference(x, y, diffSign);
    initialParameterEstimate[1] = firstDifference.argmax;

    double halfMaxWidth = estimateHalfMaxWidth(x, firstDifference);
    initialParameterEstimate[2] = halfMaxWidth / (-2.0 * Math.log(0.5));

    return initialParameterEstimate;
  }

  private double estimateHalfMaxWidth(double[] x, FirstDifference firstDifference)
  {
    double halfMax = firstDifference.max / 2.0;
    int leftIndex = 0;
    for (int i = 0; i < firstDifference.diff.length; i++)
    {
      if (firstDifference.diff[i] > halfMax)
      {
        leftIndex = i;
        break;
      }
    }

    int rightIndex = firstDifference.diff.length;
    for (int i = firstDifference.diff.length - 1; i >= 0; i--)
    {
      if (firstDifference.diff[i] > halfMax)
      {
        rightIndex = i;
        break;
      }
    }
    return x[rightIndex] - x[leftIndex];
  }


  private double estimateConstantRegions(double[] y, double[] initialParameterEstimate)
  {
    // Compute a course estimate of the parameters
    int iFirstQuarter = y.length / 4;
    double sum = 0.0;
    for (int i = 0; i < iFirstQuarter; i++)
    {
      sum += y[i];
    }
    double firstQuarterAvg = sum / iFirstQuarter;

    int iLastQuarter = iFirstQuarter + y.length / 2;

    sum = 0.0;
    for (int i = iLastQuarter; i < y.length; i++)
    {
      sum += y[i];
    }
    double lastQuarterAvg = sum / (y.length - iLastQuarter);

    double diffSign = 1.0;
    if (firstQuarterAvg > lastQuarterAvg)
    {
      initialParameterEstimate[0] = firstQuarterAvg - lastQuarterAvg;
      initialParameterEstimate[3] = lastQuarterAvg;
      diffSign = -1.0;
    }
    else
    {
      initialParameterEstimate[0] = lastQuarterAvg - firstQuarterAvg;
      initialParameterEstimate[3] = firstQuarterAvg;
    }
    return diffSign;
  }

  // Compute the first difference function of the given sequence, extracting the max and argmax
  private class FirstDifference
  {
    FirstDifference(double[] x, double y[], double scale)
    {
      assert x.length == y.length;
      diff = new double[y.length - 1];
      for (int i = 0; i < diff.length; i++)
      {
        diff[i] = scale * (y[i + 1] - y[i]) / (x[i + 1] - x[i]);
        if (diff[i] > max)
        {
          max = diff[i];
          argmax = x[i + 1];
        }
      }
    }

    double[] diff;
    double max = 0.0;
    double argmax = 0.0;
  }


}
