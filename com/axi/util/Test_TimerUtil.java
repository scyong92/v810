package com.axi.util;

import java.io.*;

/**
 * This class performs a Unit Test on the TimerUtil class.
 *
 * @author Eugene Kim-Leighton
 */
public class Test_TimerUtil extends UnitTest
{
  /**
   * @author Eugene Kim-Leighton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_TimerUtil());
  }

  /**
   * @author Eugene Kim-Leighton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    TimerUtil timer = new TimerUtil();
    long elapsedTime = timer.getElapsedTimeInMillis();
    Expect.expect(elapsedTime == 0);

    // using a timer in the right order, that is start - stop - (reset),
    timer.start();
    elapsedTime = timer.getElapsedTimeInMillis();
    Expect.expect(elapsedTime == 0);

    timer.stop();
    elapsedTime = timer.getElapsedTimeInMillis();
    Expect.expect(elapsedTime >= 0);

    timer.reset();
    elapsedTime = timer.getElapsedTimeInMillis();
    Expect.expect(elapsedTime == 0);

    // using a timer in out of order
    timer.stop();
    elapsedTime = timer.getElapsedTimeInMillis();
    Expect.expect(elapsedTime == 0);

    timer.reset();
    timer.start();
    elapsedTime = timer.getElapsedTimeInMillis();
    Expect.expect(elapsedTime == 0);

    timer.stop();
    elapsedTime = timer.getElapsedTimeInMillis();
    Expect.expect(elapsedTime >= 0);

    // test if the elapsed time is more than 0.5 second.
    timer.reset();
    timer.start();

    try
    {
      Thread.sleep(500);
    }
    catch(InterruptedException ie)
    {
      Expect.expect(false);
    }

    timer.stop();
    elapsedTime = timer.getElapsedTimeInMillis();
    // getElapsedTime is accurate to +/- 10 ms
    Expect.expect(elapsedTime >= 500 - (2 * 10));
  }
}
