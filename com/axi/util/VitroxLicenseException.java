package com.axi.util;

/**
 * @author wei-chin.chong
 */
public class VitroxLicenseException extends LocalizedException
{
  /**
   * @author Chong, Wei Chin
   */
  public VitroxLicenseException(String key, String exception)
  {
    super(new LocalizedString(key,new Object[]{exception}));
  }

  /**
   * License Error or invalid DLL
   * @author Chong, Wei Chin
   */
  static VitroxLicenseException getLicenseFailedToInitializeException(String message)
  {
    return new VitroxLicenseException("LICENSE_INITIALIZE_EXCEPTION_KEY", message);
  }
  
  /**
   * Invalid HWID or Version
   * @author Chong, Wei Chin
   */
  static VitroxLicenseException getInvalidLicenseException(String message)
  {

    return new VitroxLicenseException("LICENSE_INVALID_EXCEPTION_KEY", message);
  }

  /**
   * @author Chong, Wei Chin
   */
  static VitroxLicenseException getLicenseExceedMaxUserException(String message)
  {
    return new VitroxLicenseException("LICENSE_EXCEED_MAX_USER_EXCEPTION_KEY", message);
  }

  /**
   * @author Chong, Wei Chin
   */
  static VitroxLicenseException getNoLicenseException()
  {
    return new VitroxLicenseException("LICENSE_NOT_FOUND_EXCEPTION_KEY", "");
  }

  /**
   * @author Chong, Wei Chin
   */
  static VitroxLicenseException getLicenseExpiredException(String message)
  {
    return new VitroxLicenseException("LICENSE_EXPIRED_EXCEPTION_KEY", message);
  }
}
 