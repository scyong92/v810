package com.axi.util;

/**
 * This interface is used to support undo operations.
 * @see CommandManager
 *
 * @author Bill Darbie
 */
public abstract class Command
{
  /**
   * Execute the command.  If the command will cause a change of state it should return true.
   * If the commands execution will not cause a state change, it should return false.
   *
   * @author Bill Darbie
   */
  public abstract boolean execute() throws Exception;

  /**
   * @author Bill Darbie
   */
  public abstract void undo() throws Exception;

  /**
   * Supply a String description of the Command here
   * @author Bill Darbie
   */
  public abstract String getDescription();

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return getDescription();
  }

  /**
   * @return the number of Commands that this Command will execute() when it's execute() or undo()
   * methods are called.
   *
   * @author Bill Darbie
   */
  public int getNumCommands()
  {
    return 1;
  }
}
