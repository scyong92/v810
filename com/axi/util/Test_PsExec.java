package com.axi.util;

import java.io.*;

/**
 * @author Chong, Wei Chin
 */
public class Test_PsExec extends UnitTest
{
  private PsExec _psExec;
  /**
   * Constructor.
   *
   * @author Chong, Wei Chin
   */
  private Test_PsExec()
  {
    _psExec = PsExec.getInstance();
    // Do nothing...
  }

  /**
   * Main unit test method.
   *
   * @author Chong, Wei Chin
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testConfiguration();
    testValidExecution();
    testInvalidExecution();
  }

  /**
   * @author Chong, Wei Chin
   */
  private void testValidExecution()
  {
  }

  /**
   * @author Chong, Wei Chin
   */
  private void testInvalidExecution()
  {
    ExitCodeEnum exitCode= ExitCodeEnum.NO_ERRORS;
    _psExec.setupConnection("127.0.0.1", "", "");
    try
    {
      exitCode = _psExec.execProcess("test");
    }
    catch(IOException ioe)
    {
      exitCode = ExitCodeEnum.GENERAL_ERRORS;
    }

    if(exitCode == ExitCodeEnum.NO_ERRORS)
      Assert.expect(false);
  }

  /**
   * @author Chong, Wei Chin
   */
  private void testConfiguration()
  {
    
  }

  /**
   * Unit test entry point.
   *
   * @author Chong, Wei Chin
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_PsExec());
  }
}
