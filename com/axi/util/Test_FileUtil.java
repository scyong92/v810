package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * Tests the FileUtil class
 * @author George A. David
 */
public class Test_FileUtil extends UnitTest
{
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_FileUtil());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    String sourcePath = "source.txt";
    String destinationPath = "destination.txt";
    File sourceFile = new File(sourcePath);
    File destinationFile = new File(destinationPath);
    try
    {
      sourceFile.createNewFile();
      if(destinationFile.exists())
        destinationFile.delete();

      FileUtil.copy(sourcePath,destinationPath);
      Expect.expect(destinationFile.exists());
    }
    catch(IOException e)
    {
      e.printStackTrace();
    }

    //----------------------------------------
    // Test the zip methods.
    //
    String inputDirName = getTestDataDir() + File.separator + "testdir";
    Expect.expect( FileUtil.exists( inputDirName ) );

    String zipFile = inputDirName + File.separator + "testdir.zip";
    Expect.expect( !FileUtil.exists( zipFile ) );

    try
    {
      FileUtil.zip( FileUtil.getAbsolutePath( inputDirName ),
                    zipFile,
                    FileUtil.getAbsolutePath( inputDirName ) );
    }
    catch( FileDoesNotExistException fdnee )
    {
      fdnee.printStackTrace();
    }
    catch( CouldNotCreateFileException cncfe )
    {
      cncfe.printStackTrace();
    }
    catch( NotEnoughDiskSpaceException nedse)
    {
      nedse.printStackTrace();
    }

    Expect.expect( FileUtil.exists( zipFile ) );

    try
    {
      FileUtil.delete( zipFile );
    }
    catch( CouldNotDeleteFileException cndfe )
    {
      cndfe.printStackTrace();
    }

    //
    // Try unzipping a bogus zip file.
    //
    try
    {
      FileUtil.unZip( getTestDataDir() + File.separator + "bogus.zip", "outputDir", false);
    }
    catch( FileDoesNotExistException fdnee )
    {
      fdnee.printStackTrace();
    }
    catch( BadFileFormatException bffe )
    {
      Expect.expect( true );
    }
    catch( CouldNotCreateFileException cncfe )
    {
      cncfe.printStackTrace();
    }
    catch( CouldNotCreateDirectoryException cncfe )
    {
      cncfe.printStackTrace();
    }
    catch (FileFoundWhereDirectoryWasExpectedException ex)
    {
      ex.printStackTrace();
    }

    testReadBytes(os);
//    testGetAvailableDiskSpace(os);

    testDeleteDirectoryContents();

    testMoveDirectoryContents();

    testGetAbsolutePathRoot();
  }

  /**
   * @author Bill Darbie
   */
  private void testMoveDirectoryContents()
  {
    try
    {
      // create the following file structure:
      //  newDir/file1
      //  newDir/file2
      //  newDir/subDir/file3
      String testDataDir = getTestDataDir();
      String newDir = FileUtil.createTempDir(testDataDir);

      String newSubDir = newDir + File.separator + "subDir";
      FileUtil.createDirectory(newSubDir);

      String file1 = newDir + File.separator + "file1";
      String file2 = newDir + File.separator + "file2";
      String file3 = newSubDir + File.separator + "file3";

      Set<String> fileSet = new HashSet<String>();
      fileSet.add(file1);
      fileSet.add(file2);
      fileSet.add(file3);
      for (String file : fileSet)
      {
        File aFile = new File(file);
        aFile.createNewFile();
      }

      // now check that all files exist
      File dir = new File(newDir);
      Expect.expect(dir.listFiles().length == 3);

      // move everything from the newDir to newDir2
      String newDir2 = FileUtil.createTempDir(testDataDir);
      FileUtil.moveDirectoryContents(newDir, newDir2);

      File dir2 = new File(newDir2);
      Expect.expect(dir.listFiles().length == 0);
      Expect.expect(dir2.listFiles().length == 3);

      FileUtil.delete(newDir);
      FileUtil.delete(newDir2);

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void testDeleteDirectoryContents()
  {
    try
    {
      // create the following file structure:
      //  newDir/file1
      //  newDir/file2
      //  newDir/subDir/file3
      String testDataDir = getTestDataDir();
      String newDir = FileUtil.createTempDir(testDataDir);

      String newSubDir = newDir + File.separator + "subDir";
      FileUtil.createDirectory(newSubDir);

      String file1 = newDir + File.separator + "file1";
      String file2 = newDir + File.separator + "file2";
      String file3 = newSubDir + File.separator + "file3";

      Set<String> fileSet = new HashSet<String>();
      fileSet.add(file1);
      fileSet.add(file2);
      fileSet.add(file3);
      for (String file : fileSet)
      {
        File aFile = new File(file);
        aFile.createNewFile();
      }

      // now check that all files exist
      File file = new File(newDir);
      Expect.expect(file.listFiles().length == 3);

      // delete all by one directory and one file
      List<String> dontDelete = new ArrayList<String>();
      dontDelete.add(newSubDir);
      dontDelete.add(file1);
      FileUtil.deleteDirectoryContents(newDir, dontDelete);

      // now check that all files but 2 were deleted
      file = new File(newDir);
      Expect.expect(file.listFiles().length == 2);

      FileUtil.deleteDirectoryContents(newDir);

      // now check that all files were deleted
      file = new File(newDir);
      Expect.expect(file.listFiles().length == 0);

      // delete the temp directory
      FileUtil.delete(newDir);

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author George A. David
   */
  private void testReadBytes(PrintWriter os)
  {
    Assert.expect(os != null);

    String filePath = getTestDataDir() + File.separator + "Test_FileUtil" + File.separator + "xrayCameraFirmware.ace";
    byte[] bytes = null;
    try
    {
      bytes = FileUtil.readBytes(filePath);
    }
    catch (Exception ex)
    {
      Expect.expect(false);
      ex.printStackTrace(os);
    }

    os.println("The following is the binary contents of the file " + UnitTest.stripOffViewName(filePath));
    os.println("The length of the file is " + bytes.length + " bytes.");
    for(int i = 0; i < bytes.length; ++i)
    {
      os.println("bytes[" + i + "] = " + bytes[i]);
    }
  }

  /**
   * @author George A. David
   */
  private void testGetAvailableDiskSpace(PrintWriter os)
  {
    System.loadLibrary("nativeAxiUtil");

    String[] path = new String[]{"c:/apps","d:/","f:/","h:","//mtddarbi/install"};

    for(int i = 0; i < path.length; ++i)
    {
      long freeSpace = FileUtil.getAvailableDiskSpaceInBytes(path[i]);
      os.println("The available disk space on " + path[i] + " is: " + freeSpace + " bytes (" + freeSpace/1000000 + "Mb, " + (freeSpace)/1000000000 + "Gb)");
    }
  }

  /**
   * @author Matt Wharton
   */
  private void testGetAbsolutePathRoot()
  {
    // Make sure we assert when passed a null String.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        FileUtil.getAbsolutePathRoot(null);
      }
    });

    // Test some standard paths.
    String pathRoot = FileUtil.getAbsolutePathRoot("c:\\temp");
    Expect.expect(pathRoot.equalsIgnoreCase("c:\\"));

    pathRoot = FileUtil.getAbsolutePathRoot("d:");
    Expect.expect(pathRoot.equalsIgnoreCase("d:\\"));

    // Test UNC paths.
    pathRoot = FileUtil.getAbsolutePathRoot("\\\\mtd-mattw\\coolguy\\junk");
    Expect.expect(pathRoot.equalsIgnoreCase("\\\\mtd-mattw\\coolguy\\"));

    // Test a bogus path.
    pathRoot = FileUtil.getAbsolutePathRoot("1:\\23\\badPath");
    Expect.expect(pathRoot.equals(""));
  }
}
