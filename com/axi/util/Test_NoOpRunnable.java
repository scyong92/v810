package com.axi.util;

import java.io.*;


/**
 * Test class for NoOpRunnable (duh).
 *
 * @author Matt Wharton
 */
public class Test_NoOpRunnable extends UnitTest
{
  /**
   * @author Matt Wharton
   */
  public Test_NoOpRunnable()
  {
    // Do nothing...
  }


  /**
   * @author Matt Wharton
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    Thread testThread1 = new Thread( new NoOpRunnable( 5000 ), "Test Thread 1" );
    Thread testThread2 = new Thread( new NoOpRunnable( 3000 ), "Test Thread 2" );
    Thread testThread3 = new Thread( new NoOpRunnable(), "Test Thread 3" );

    testThread1.start();
    testThread2.start();
    testThread3.start();

    try
    {
      testThread1.join();
    }
    catch ( InterruptedException iex )
    {
      Expect.expect( false );
    }
    try
    {
      testThread2.join();
    }
    catch ( InterruptedException iex )
    {
      Expect.expect( false );
    }
    try
    {
      testThread3.join();
    }
    catch ( InterruptedException iex )
    {
      Expect.expect( false );
    }

    try
    {
      Runnable badRunnable = new NoOpRunnable( -123 );
      Expect.expect( false );
    }
    catch ( AssertException aex )
    {
      // Do nothing...
    }
  }


  /**
   * @author Matt Wharton
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_NoOpRunnable() );
  }
}