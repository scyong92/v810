package com.axi.util;

/**
 * Boolean flag designed to be shared among threads.
 *
 * If you need a boolean flag that is to be used by multiple threads, this
 * is the class for you!  Threads can test and set the internal value and
 * wait for it to change.  The wait/notify mechanism is used internally to
 * support waiting for the value to change, and frees external classes from
 * the error-prone complexity of properly implementing this mechanism.
 *
 * The wait methods all can throw InterruptedExceptions.  This will happen
 * if somebody calls interrupt() on the waiting thread.
 *
 * This is adapted from Paul Hyde's BooleanLock class as described in his
 * book _Java Thread Programming_ .
 *
 * @author Peter Esbensen
 */
public class BooleanLock extends Object
{
  private boolean _value;

  /**
   * Create a BooleanLock with the given initial value.
   *
   * @author Peter Esbensen
   */
  public BooleanLock(boolean initialValue)
  {
    _value = initialValue;
  }

  /**
   * Create a BooleanLock with a default initial value of false.
   *
   * @author Peter Esbensen
   */
  public BooleanLock()
  {
    this(false);
  }

  /**
   * @author Peter Esbensen
   */
  public synchronized void setValue(boolean newValue)
  {
    if (newValue != _value)
    {
      _value = newValue;
      notifyAll();
    }
  }

  /**
   * @author Peter Esbensen
   */
  public synchronized boolean isTrue()
  {
    return _value;
  }

  /**
   * @author Peter Esbensen
   */
  public synchronized boolean isFalse()
  {
    return !_value;
  }

  /**
   * Wait for specified time for value to be set to true.
   *
   * @param timeoutMillisec is the most time you want to wait - in milliseconds
   * @return false if timeout occurred before value was set to true, otherwise true
   * @throws InterruptedException when somebody calls interrupt on your waiting thread
   * @author Peter Esbensen
   */
  public synchronized boolean waitUntilTrue(long timeoutMillisec) throws InterruptedException
  {
    return waitUntilStateIs(true, timeoutMillisec);
  }

  /**
   * Wait for specified time for value to be set to false.
   *
   * @param timeoutMillisec is the most time you want to wait - in milliseconds
   * @return false if timeout occurred before value was set to false, otherwise true
   * @throws InterruptedException when somebody calls interrupt on your waiting thread
   * @author Peter Esbensen
   */
  public synchronized boolean waitUntilFalse(long timeoutMillisec) throws InterruptedException
  {
    return waitUntilStateIs(false, timeoutMillisec);
  }

  /**
   * Wait indefinitely for value to be set to true.
   *
   * @throws InterruptedException when somebody calls interrupt on your waiting thread
   * @author Peter Esbensen
   */
  public synchronized void waitUntilTrue() throws InterruptedException
  {
    boolean noTimeoutOccurred = waitUntilStateIs(true, 0);
    Assert.expect(noTimeoutOccurred);
  }

  /**
   * Wait indefinitely for value to be set to false.
   *
   * @throws InterruptedException when somebody calls interrupt on your waiting threa
   * @author Peter Esbensen
   */
  public synchronized void waitUntilFalse() throws InterruptedException
  {
    boolean noTimeoutOccurred = waitUntilStateIs(false, 0);
    Assert.expect(noTimeoutOccurred);
  }

  /**
   * Wait for specified time for specified value.
   *
   * @param state is the value you are waiting for
   * @param timeoutMillisec is the most time you want to wait - in milliseconds
   * @throws InterruptedException when somebody calls interrupt on your waiting thread
   * @author Peter Esbensen
   */
  public synchronized boolean waitUntilStateIs(boolean state, long timeoutMillisec) throws InterruptedException
  {
    if ( timeoutMillisec == 0L)
    {
      while (_value != state)
      {
        wait(); // wait indefinitely until notified
      }

      // condition has finally been met
      return true;
    }

    // only wait for the specified amount of time
    long endTime = System.currentTimeMillis() + timeoutMillisec;
    long msRemaining = timeoutMillisec;

    while ((_value != state) && (msRemaining > 0L))
    {
      wait(msRemaining);
      msRemaining = endTime - System.currentTimeMillis();
    }

    // May have timed out, or may have met value,
    // calculate return value.
    return (_value == state);
  }
}
