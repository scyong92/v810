package com.axi.util;

import java.io.*;
import java.util.regex.*;

/**
 * Filter filenames based on a regular expression.
 * @author Patrick Lacz
 */
public class RegExFileNameFilter implements FilenameFilter
{
  private Pattern _regExPattern = null;

  /**
   * @author Patrick Lacz
   */
  public RegExFileNameFilter(String patternString)
  {
    Assert.expect(patternString != null);

    _regExPattern = Pattern.compile(patternString, Pattern.CASE_INSENSITIVE);
  }

  /**
   * Get access to the regular expression used. This can be useful to identifiy the groups.
   * (eg. "image(\\d+).jpg", group(1) is the number.)
   * @author Patrick Lacz
   */
  public Pattern getRegExPattern()
  {
    Assert.expect(_regExPattern != null);

    return _regExPattern;
  }

  /**
   * Tests whether or not the specified abstract pathname should be included in
   * a pathname list.
   *
   * In this case, the name should match the given regular expression.
   *
   * @author Patrick Lacz
   */
  public boolean accept(File path, String filenameString)
  {
    Assert.expect(path != null);
    Assert.expect(filenameString != null);
    Assert.expect(_regExPattern != null);

    Matcher matcher = _regExPattern.matcher(filenameString);
    return matcher.matches();
  }
}
