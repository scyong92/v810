package com.axi.util;

import java.io.*;

/**
 * @author Greg Esparza
 */
public class CommunicationTypeEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;

  public static final CommunicationTypeEnum TCPIP = new CommunicationTypeEnum(++_index);

  /**
   * @author Greg Esparza
   */
  private CommunicationTypeEnum(int id)
  {
    super(id);
  }
}
