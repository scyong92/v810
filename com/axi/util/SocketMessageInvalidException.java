package com.axi.util;

import java.lang.*;

/**
 * A socket client will throw this exception whenever there is a socket server cannot understand the client's request.
 *
 * @author Eugene Kim-Leighton
 */
public class SocketMessageInvalidException extends Exception
{
  public SocketMessageInvalidException(String errorMessage)
  {
    super(errorMessage);
    Assert.expect(errorMessage != null);
  }
}