package com.axi.util;

import java.io.*;
import java.nio.*;
import java.nio.channels.*;

public class Test_CDataByteBufferUtil extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_CDataByteBufferUtil());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String tempFile = "temp";
    FileChannel fc = null;
    try
    {
      RandomAccessFile raf = new RandomAccessFile(tempFile, "rw");
      fc = raf.getChannel();
      ByteBuffer bb = ByteBuffer.allocate(1000);
      bb.order(ByteOrder.LITTLE_ENDIAN);
      CDataByteBufferUtil buffUtil = new CDataByteBufferUtil(bb);

      // test getUnsignedByte
      bb.clear();
      // -0x0b = 0x00FB in 2's complement
      byte byteValue = -0x0b;
      bb.put(byteValue);
      bb.flip();
      Expect.expect(bb.get() == -0x0b);
      bb.rewind();
      Expect.expect(buffUtil.getUnsignedByte() == 0xf5);

      // test readUnsignedChar
      bb.rewind();
      Expect.expect(buffUtil.getUnsignedChar() == 0xf5);

      // test writeBoolean
      bb.clear();
      buffUtil.putBoolean(true);
      bb.flip();
      Expect.expect(buffUtil.getBoolean() == true);
      bb.rewind();
      buffUtil.putBoolean(false);
      bb.flip();
      Expect.expect(buffUtil.getBoolean() == false);

      // test writeShort();
      bb.clear();
      short shortValue = -2;
      bb.putShort(shortValue);
      bb.flip();
      Expect.expect(bb.getShort() == shortValue);

      // test readUnsignedShort
      bb.rewind();
      // 0xfffe is the 2's complement representation of -2
      Expect.expect(buffUtil.getUnsignedShort() == 0xfffe);

      // test writeInt()
      bb.clear();
      bb.putInt(-77);
      bb.flip();
      Expect.expect(bb.getInt() == -77);

      // test readUnsignedInt()
      bb.rewind();
      Expect.expect(buffUtil.getUnsignedInt() == 0xffffffffffffffb3L);

      // test writePascalString
      String str = "hi";
      bb.clear();
      buffUtil.putPascalString(str);
      bb.flip();
      Expect.expect(buffUtil.getPascalString().equals(str));

      // test writePascalPackedArray();
      str = "abc";
      bb.clear();
      buffUtil.putPascalPackedArray(str);
      bb.flip();
      Expect.expect(buffUtil.getPascalPackedArray(str.length()).equals(str));


      // now test the absolute positioning calls


      // test getUnsignedByte
      // -0x0b = 0x00FB in 2's complement
      bb.limit(1000);
      byteValue = -0x0b;
      bb.put(100, byteValue);
      Expect.expect(bb.get(100) == -0x0b);
      Expect.expect(buffUtil.getUnsignedByte(100) == 0xf5);

      // test readUnsignedChar
      Expect.expect(buffUtil.getUnsignedChar(100) == 0xf5);

      // test writeBoolean
      buffUtil.putBoolean(100, true);
      Expect.expect(buffUtil.getBoolean(100) == true);
      buffUtil.putBoolean(100, false);
      Expect.expect(buffUtil.getBoolean(100) == false);

      // test writeShort();
      shortValue = -2;
      bb.putShort(100, shortValue);
      Expect.expect(bb.getShort(100) == shortValue);

      // test readUnsignedShort
      // 0xfffe is the 2's complement representation of -2
      Expect.expect(buffUtil.getUnsignedShort(100) == 0xfffe);

      // test writeInt()
      bb.putInt(100, -77);
      Expect.expect(bb.getInt(100) == -77);

      // test readUnsignedInt()
      Expect.expect(buffUtil.getUnsignedInt(100) == 0xffffffffffffffb3L);

      // test writePascalString
      str = "hi";
      buffUtil.putPascalString(100, str);
      Expect.expect(buffUtil.getPascalString(100, str.length() + 1).equals(str));

      // test writePascalPackedArray();
      str = "abc";
      buffUtil.putPascalPackedArray(100, str);
      Expect.expect(buffUtil.getPascalPackedArray(100, str.length(), str.length()).equals(str));
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    finally
    {
      try
      {
        if (fc != null)
          fc.close();
      }
      catch(IOException ioe)
      {
        ioe.printStackTrace();
      }
      try
      {
        FileUtil.delete(tempFile);
      }
      catch(CouldNotDeleteFileException ex)
      {
        ex.printStackTrace();
      }

    }
  }
}