package com.axi.util;

import java.io.*;

/**
 * @author John Dutton
 */
public class CouldNotCloseFileException extends IOException
{
  private String _fileName;

  /**
   * @author John Dutton
   */
  public CouldNotCloseFileException(String fileName)
  {
    super("ERROR: Could not close file " + fileName);
    Assert.expect(fileName != null);
    _fileName = fileName;
  }

  /**
   * @author John Dutton
   */
  public String getFileName()
  {
    return _fileName;
  }
}
