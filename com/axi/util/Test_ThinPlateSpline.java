package com.axi.util;

import java.io.*;
import java.util.Properties;

/**
 * @author Cheah Lee Herng
 */
public class Test_ThinPlateSpline extends UnitTest
{
    /**
     * @author Cheah Lee Herng
     */
    public Test_ThinPlateSpline()
    {
        // Do nothing
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void test(BufferedReader is, PrintWriter os) 
    {
        testAdd4ModelDataPoints();
        testAdd3ModelDataPoints();
        testInterpolate_1();
        testInterpolate_2();
        testReset();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void testAdd4ModelDataPoints()
    {
        ThinPlateSpline thinPlateSpline = new ThinPlateSpline("123_567");
        
        thinPlateSpline.addPoint(10, 20, 30);
        thinPlateSpline.addPoint(30, 25, 60);
        thinPlateSpline.addPoint(55, 60, 35);
        thinPlateSpline.addPoint(85, 45, 90);
        
        Expect.expect(thinPlateSpline.isSufficientSurfaceModelDataPoint());
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void testAdd3ModelDataPoints()
    {
        ThinPlateSpline thinPlateSpline = new ThinPlateSpline("567_695");
        
        thinPlateSpline.addPoint(10, 20, 30);
        thinPlateSpline.addPoint(30, 25, 60);
        thinPlateSpline.addPoint(55, 60, 35);
                
        Expect.expect(thinPlateSpline.isSufficientSurfaceModelDataPoint() == false);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void testReset()
    {
        ThinPlateSpline thinPlateSpline1 = new ThinPlateSpline("123_567");
        thinPlateSpline1.reset();        
        Expect.expect(thinPlateSpline1.isSufficientSurfaceModelDataPoint() == false);
        
        ThinPlateSpline thinPlateSpline2 = new ThinPlateSpline("567_695");
        thinPlateSpline2.addPoint(65, 45, 12);
        Expect.expect(thinPlateSpline2.isSufficientSurfaceModelDataPoint());
        thinPlateSpline2.reset();
        Expect.expect(thinPlateSpline2.isSufficientSurfaceModelDataPoint() == false);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void testInterpolate_1()
    {
        ThinPlateSpline thinPlateSpline = new ThinPlateSpline("123_567");                
        thinPlateSpline.update();
        
        double interpolatedZHeight = thinPlateSpline.interpolateZHeightInNanometers(10, 20);
        Expect.expect(interpolatedZHeight == 31.886871868123034);
        
        interpolatedZHeight = thinPlateSpline.interpolateZHeightInNanometers(55, 60);
        Expect.expect(interpolatedZHeight == 34.83448492391897);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void testInterpolate_2()
    {
        ThinPlateSpline thinPlateSpline = new ThinPlateSpline("567_695");        
        thinPlateSpline.addPoint(105, 65, 89);        
        Expect.expect(thinPlateSpline.isSufficientSurfaceModelDataPoint());
        
        thinPlateSpline.update();
        
        double interpolatedZHeight = thinPlateSpline.interpolateZHeightInNanometers(30, 25);
        Expect.expect(interpolatedZHeight == 57.859992878490026);
        
        thinPlateSpline.addPoint(65, 45, 12);
        thinPlateSpline.addPoint(55, 60, 35);
        
        thinPlateSpline.update();
        
        interpolatedZHeight = thinPlateSpline.interpolateZHeightInNanometers(65, 45);
        Expect.expect(interpolatedZHeight == 24.049432155145066);
    }
    
    /**
     * Main method.
     *
     * @author Cheah Lee Herng
    */
    public static void main(String[] args)
    {
        UnitTest.execute(new Test_ThinPlateSpline());
    }
}
