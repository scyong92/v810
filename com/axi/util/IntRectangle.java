package com.axi.util;

import java.awt.*;
import java.awt.geom.*;
import java.io.*;

/**
 * This class is a representation of a rectangle.
 * Since this can be used for any type of rectangle, it is not necessarily
 * saved in nanometers. The variable name of an instance of this class should
 * be descriptive enough to determine what units are being used (e.g. pixels).
 *
 * IMPORTANT: This class assumes a standard origin where x grows right and y grows up.
 * The underlying Rectangle2D uses "screen coordinates" (y grows down).
 * Hence, some of the wrapper methods do y inversions to provide the expected "y grows up" behavior.
 * If you're looking to use screen coordinates, use the ImageRectangle class.
 *
 *
 * @author Keith Lee
 * @author Matt Wharton
 */
public class IntRectangle implements Serializable
{
  // This class really just wraps a java.awt.geom.Rectangle2D.Double.
  // NOTE: This IS a serializable class and the Rectangle2D is supposed to be transient.  This is handled
  // via the overridden readObject/writeObject methods below.
  protected transient Rectangle2D _rectangle = null;

  // Outcode constants
  public static final int OUT_BOTTOM = Rectangle2D.OUT_BOTTOM;
  public static final int OUT_TOP = Rectangle2D.OUT_TOP;
  public static final int OUT_LEFT = Rectangle2D.OUT_LEFT;
  public static final int OUT_RIGHT = Rectangle2D.OUT_RIGHT;

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  public IntRectangle( IntRectangle rhs )
  {
    Assert.expect(rhs != null);

    _rectangle = (Rectangle2D)rhs._rectangle.clone();
  }

  /**
   * @author Peter Esbensen
   */
  public IntRectangle(Rectangle2D rectangle2D)
  {
    Assert.expect(rectangle2D != null);

    _rectangle = (Rectangle2D)rectangle2D.clone();
  }

  /**
   * @author George A. David
   * @author Matt Wharton
   */
  public IntRectangle(Shape shape)
  {
    Assert.expect(shape != null);

    Rectangle2D boundingBox = shape.getBounds2D();
    _rectangle = new Rectangle2D.Double(Math.round(boundingBox.getMinX()),
                                        Math.round(boundingBox.getMinY()),
                                        Math.round(boundingBox.getWidth()),
                                        Math.round(boundingBox.getHeight()));
  }

  /**
   * @author Matt Wharton
   */
  public IntRectangle(int bottomLeftX, int bottomLeftY, int width, int length)
  {
    Assert.expect(width >= 0);
    Assert.expect(length >= 0);

    _rectangle = new Rectangle2D.Double(bottomLeftX, bottomLeftY , width, length);
  }

  /**
   * @author Matt Wharton
   */
  public void setRect(int bottomLeftX, int bottomLeftY, int width, int height)
  {
    _rectangle.setRect(bottomLeftX, bottomLeftY, width, height);
  }

  /**
   * @author George A. David
   */
  public void setRect(double bottomLeftX, double bottomLeftY, double width, double height)
  {
    _rectangle.setRect(bottomLeftX, bottomLeftY, width, height);
  }


  /**
   * @author Matt Wharton
   */
  public void setRect(IntRectangle rectangle)
  {
    Assert.expect(rectangle != null);

    _rectangle.setRect((int)rectangle._rectangle.getX(),
                       (int)rectangle._rectangle.getY(),
                       (int)rectangle._rectangle.getWidth(),
                       (int)rectangle._rectangle.getHeight());
  }


  /**
   * @author Matt Wharton
   */
  public int getMinX()
  {
    Assert.expect(_rectangle != null);

    return (int)_rectangle.getMinX();
  }

  /**
   * @author Matt Wharton
   */
  public int getMinY()
  {
    Assert.expect(_rectangle != null);

    return (int)_rectangle.getMinY();
  }

  /**
   * @author Patrick Lacz
   */
  public void setMinXY(int x, int y)
  {
    Assert.expect(_rectangle != null);

    _rectangle.setRect((double)x, (double)y, _rectangle.getWidth(), _rectangle.getHeight());
  }

  /**
   * @author Matt Wharton
   */
  public int getMaxX()
  {
    Assert.expect(_rectangle != null);

    return (int)_rectangle.getMaxX();
  }


  /**
   * @author Matt Wharton
   */
  public int getMaxY()
  {
    Assert.expect(_rectangle != null);

    return (int)_rectangle.getMaxY();
  }


  /**
   * @author Matt Wharton
   */
  public int getCenterX()
  {
    return (int)_rectangle.getCenterX();
  }

  /**
   * @author Matt Wharton
   */
  public int getCenterY()
  {
    return (int)_rectangle.getCenterY();
  }


  /**
   * @author Matt Wharton
   */
  public int getWidth()
  {
    Assert.expect(_rectangle != null);

    return (int)_rectangle.getWidth();
  }


  /**
   * @author Matt Wharton
   */
  public int getHeight()
  {
    Assert.expect(_rectangle != null);

    return (int)_rectangle.getHeight();
  }

  /**
   * @author Peter Esbensen
   */
  public Rectangle2D getRectangle2D()
  {
    Assert.expect(_rectangle != null);
    return (Rectangle2D)_rectangle.clone();
  }

  /**
   * Create the smallest rectangle containing both the current rectangle and the point (x,y).
   *
   * After adding a point, a call to contains with the added point as an argument does not necessarily return true.
   * The contains method does not return true for points on the right or bottom edges of a rectangle. Therefore, if
   * the added point falls on the left or bottom edge of the enlarged rectangle, contains returns false for that point.
   *
   * @author Kay Lannen
   */
  public void add(int x, int y)
  {
    _rectangle.add( x, y );
  }

  /**
   * Create the union of this rectangle and the one passed in.
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public void add(IntRectangle rect)
  {
    Assert.expect(rect != null);

    _rectangle.add( rect._rectangle );
  }

  /**
   * Create the union of this rectangle and the one passed in.
   * @author George A. David
   */
  public void add(Rectangle2D rect)
  {
    Assert.expect(rect != null);

    _rectangle.add(rect);
  }

  /**
   * Create the union of this rectangle and the shape passed in.
   * @author George A. David
   */
  public void addShape(Shape shape)
  {
    // NOTE: keep this function named different
    // from add, otherwise the function
    // add(Rectangle2D ) will never be
    // called. And since this function calls get
    // bounds, it will add a small unecessary
    // delay.
    Assert.expect(shape != null);

    _rectangle.add(shape.getBounds2D());
  }

  /**
   * @author Matt Wharton
   */
  public boolean contains(IntRectangle innerRectangle)
  {
    Assert.expect( innerRectangle != null );
    Assert.expect( _rectangle != null );

    return _rectangle.contains(innerRectangle._rectangle);
  }

  /**
   * @author George A. David
   */
  public boolean contains(Rectangle2D rectangle)
  {
    Assert.expect(rectangle != null);
    Assert.expect(_rectangle != null);

    return _rectangle.contains(rectangle);
  }

  /**
   *
   * @author George A. David
   */
  public boolean containsShape(Shape shape)
  {
    // NOTE: keep this function named different
    // from contains, otherwise the function
    // contains(Rectangle2D ) will never be
    // called. And since this function calls get
    // bounds, it will add a small unecessary
    // delay.
    Assert.expect(shape != null);
    Assert.expect( _rectangle != null );

    return _rectangle.contains(shape.getBounds2D());
  }


  /**
   * @author Matt Wharton
   */
  public boolean contains(int x, int y)
  {
    Assert.expect( _rectangle != null );

    return _rectangle.contains( x, y );
  }


  /**
   * Determines where the specified point is with respect to this IntRectangle.
   *
   * @param x the x coordinate of the point in question.
   * @param y the y coordinate of the point in question.
   * @return the 'outcode' representing the positional relationship (see the Rectangle2D documentation for more detail).
   * @author Matt Wharton
   */
  public int outcode(int x, int y)
  {
    Assert.expect( _rectangle != null );

    return _rectangle.outcode( x, y );
  }

  /**
   * Determines if the specified rectangle intersects this IntRectangle
   *
   * @author Dave Ferguson
   */
  public boolean intersects(IntRectangle rect)
  {
    Assert.expect(_rectangle != null);
    Assert.expect(rect != null);

    return _rectangle.intersects(rect.getRectangle2D());
  }

  /**
   * @author George A. David
   */
  public boolean intersects(Rectangle2D rect)
  {
    Assert.expect(_rectangle != null);
    Assert.expect(rect != null);

    return _rectangle.intersects(rect);
  }

  /**
   * @author Roy Williams
   */
  public Rectangle2D createIntersection(IntRectangle rect)
  {
    Assert.expect(_rectangle != null);
    Assert.expect(rect != null);

    return _rectangle.createIntersection(rect._rectangle);
  }

  /**
   * @author George A. David
   */
  public boolean intersectsShape(Shape shape)
  {
    // NOTE: keep this function named different
    // from intersects, otherwise the function
    // intersects(Rectangle2D ) will never be
    // called. And since this function calls get
    // bounds, it will add a small unecessary
    // delay.
    Assert.expect(shape != null);
    Assert.expect( _rectangle != null );

    return _rectangle.intersects(shape.getBounds2D());
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public boolean intersectsX(IntRectangle rect)
  {
    double diff1 = _rectangle.getMaxX()-rect._rectangle.getMinX();
    double diff2 = rect._rectangle.getMaxX() - _rectangle.getMinX();
    
    return Math.min(diff1, diff2) > 0;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public boolean intersectsY(IntRectangle rect)
  {
    double diff1 = _rectangle.getMaxY()-rect._rectangle.getMinY();
    double diff2 = rect._rectangle.getMaxY() - _rectangle.getMinY();
    
    return Math.min(diff1, diff2) > 0;
  }


  /**
   * @author Peter Esbensen
   */
  public boolean equals(Object object)
  {
    boolean equal = false;

    if ((object != null) && (object instanceof IntRectangle))
    {
      IntRectangle intRectangle = (IntRectangle)object;
      if ((intRectangle.getMinX() == (int)_rectangle.getMinX()) &&
          (intRectangle.getMinY() == (int)_rectangle.getMinY()) &&
          (intRectangle.getWidth() == (int)_rectangle.getWidth()) &&
          (intRectangle.getHeight() == (int)_rectangle.getHeight()))
        equal = true;
    }

    return equal;
  }

  /**
   * Determines where the specified IntCoordinate is with respect to this IntRectangle.
   *
   * @param coordinate the IntCoordinate in question.
   * @return the 'outcode' representing the positional relationship (see the Rectangle2D documentation for more detail).
   * @author Matt Wharton
   */
  public int outcode(IntCoordinate coordinate)
  {
    Assert.expect( _rectangle != null );
    Assert.expect( coordinate != null );

    return _rectangle.outcode( coordinate.getX(), coordinate.getY() );
  }

  /**
   * @author Matt Wharton
   */
  protected void deserializeRectangle(ObjectInputStream is) throws IOException
  {
    Assert.expect(is != null);

    double tempX = is.readDouble();
    double tempY = is.readDouble();
    double tempWidth = is.readDouble();
    double tempHeight = is.readDouble();
    _rectangle = new Rectangle2D.Double(tempX, tempY, tempWidth, tempHeight);
  }

  /**
   * @author Matt Wharton
   */
  protected void serializeRectangle(ObjectOutputStream os) throws IOException
  {
    Assert.expect(os != null);
    Assert.expect(_rectangle != null);

    os.writeDouble(_rectangle.getX());
    os.writeDouble(_rectangle.getY());
    os.writeDouble(_rectangle.getWidth());
    os.writeDouble(_rectangle.getHeight());
  }

  /**
   * @author Matt Wharton
   */
  private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
  {
    is.defaultReadObject();
    deserializeRectangle(is);
  }

  /**
   * @author Matt Wharton
   */
  private void writeObject(ObjectOutputStream os) throws IOException
  {
    os.defaultWriteObject();
    serializeRectangle(os);
  }

  /**
   * @author Roy Williams
   */
  public String toString()
  {
    String space = " ";
    return new String(space + (int)_rectangle.getX()     +
                      space + (int)_rectangle.getY()     +
                      space + (int)_rectangle.getWidth() +
                      space + (int)_rectangle.getHeight());
  }

}
