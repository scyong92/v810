package com.axi.util;

/**
 * <p>Title: XMLWriterStrinUtil: </p>
 *
 * <p>Description: This file keeps a number of strings that are commonly used when generating and XML file programmatically</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Laura Cormos
 * @version 1.0
 */
public class XMLWriterStringUtil
{
  public static final String ESCAPED_DOUBLE_QUOTE = "\"";
  public static final String GREATER_THEN_TAG = ">";
  public static final String ELEMENT_END_TAG = "/>";
  public static final String BEGIN_COMMENT_TAG = "<!--";
  public static final String END_COMMENT_TAG = "-->";
  public static final String XML_TAG = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
  public static final int NUM_INDENT_SPACES = 2;
  public static final String GREATER_THAN_ENTITY_REF = "&lt;";
  public static final String AMPERSAND_ENTITY_REF = "&amp;";
  public static final char _GREATER_THAN_CHAR = '<';
  public static final char _AMPERSAND_CHAR = '&';

  public static final String _GREATER_THAN_STRING = "<";
  public static final String _AMPERSAND_STRING = "&";
  public static final String _ALIASE_STRING = "@";

  /**
   * @author Laura Cormos
   */
  public static String replaceIllegalStringChars(String originalString)
  {
    Assert.expect(originalString != null);

    String newString = originalString;
    newString.replace(new String(_GREATER_THAN_STRING), GREATER_THAN_ENTITY_REF);
    newString.replace(new String(_AMPERSAND_STRING), AMPERSAND_ENTITY_REF);

    originalString = newString;
    return originalString;
  }

//  /**
//   * @author Laura Cormos
//   */
//  public static String getEscapedDoubleQuote()
//  {
//    return _ESCAPED_DOUBLE_QUOTE;
//  }
//
//  /**
//   * @author Laura Cormos
//   */
//  public static String getGreaterThanTag()
//  {
//    return _GREATER_THEN_TAG;
//  }
//
//  /**
//   * @author Laura Cormos
//   */
//  public static String getElementEndTag()
//  {
//    return _ELEMENT_END_TAG;
//  }
//
//  /**
//   * @author Laura Cormos
//   */
//  public static String getBeginCommentTag()
//  {
//    return _BEGIN_COMMENT_TAG;
//  }
//
//  /**
//   * @author Laura Cormos
//   */
//  public static String getEndCommentTag()
//  {
//    return _END_COMMENT_TAG;
//  }
//
//  /**
//   * @author Laura Cormos
//   */
//  public static int getNumIndentSpaces()
//  {
//    return _NUM_INDENT_SPACES;
//  }
}
