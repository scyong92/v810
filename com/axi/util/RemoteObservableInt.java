package com.axi.util;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
* This interface allows clients to call these methods through RMI calls.
* Any class you have that extends RemoteObservable must also implement
* this interface so that Observers are allowed to call these methods
* remotely.
* @see RemoteObservable
* @see java.util.Observable
*  @author Bill Darbie
*/
public interface RemoteObservableInt extends Remote
{
  void addObserver(RemoteObserverInt remoteObserver) throws RemoteException;
  void deleteObserver(RemoteObserverInt remoteObserver) throws RemoteException;
  void deleteObservers() throws RemoteException;
}
