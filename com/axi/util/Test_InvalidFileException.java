package com.axi.util;

import java.io.*;

/**
 * This class performs a Unit Test on the InvalidFileException class.
 *
 * @author Eugene Kim-Leighton
 */
public class Test_InvalidFileException extends UnitTest
{
  /**
   * @author Eugene Kim-Leighton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_InvalidFileException());
  }

  /**
   * @author Eugene Kim-Leighton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String tempFileName = "fileName";
    InvalidFileException ife = new InvalidFileException(tempFileName);

    String fileName = ife.getFileNameWithFullPath();
    Expect.expect(fileName.equals(tempFileName));

    String tempMessage = "The file " + fileName + " is not valid.";
    String message = ife.getMessage();
    Expect.expect(message.equals(tempMessage));
  }
}