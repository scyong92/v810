package com.axi.util;

/**
 * Provides identifiers for a variety of norms/distances to use with the
 * StatisticsUtil class.
 *
 * Each method has two names: a mathematical name and a more user-friendly name.
 *
 * @author  Patrick Lacz
 */
public class StatisticsNormEnum extends Enum
{
  /**
   * The Infinity norm is computed as the maximal magnitude of any dimension.
   *
   * result = max(|x0|, ..., |xn|)
   */
  public static final StatisticsNormEnum INFINITY = new StatisticsNormEnum(0);
  /**
   * The maximum magnitude of any dimention.
   * Same as INFINITY.
   *
   * result = max(|x0|, ..., |xn|)
   */
  public static final StatisticsNormEnum MAXIMUM = INFINITY;

  /**
   * The L1 norm is the sum of the magnitudes of each dimension.
   * Also referred to as Manhattan Distance. (navigating city blocks)
   *
   * result = sum(|x0|, ..., |xn|)
   */
  public static final StatisticsNormEnum L1 = new StatisticsNormEnum(1);

  /**
   * The sum of the magnitudes of each dimension.
   * Same as L1.
   *
   * result = sum(|x0|, ..., |xn|)
   */
  public static final StatisticsNormEnum MANHATTAN = L1;

  /**
   * The L2 norm is the usual distance equation (Euclidian), or the direct
   * 'as the crow flies' distance.
   *
   * result = sqrt(sum(x0^2, ..., xn^2))
   */
  public static final StatisticsNormEnum L2 = new StatisticsNormEnum(2);
  /**
   * The Euclidian distance from the point to the origin.
   * Same as L2.
   *
   * result = sqrt(sum(x0^2, ..., xn^2))
   */
  public static final StatisticsNormEnum EUCLIDIAN = L2;

  /**
   * Mahalanobis distance is Euclidian distance scaled by the certainty or standard
   * deviation of each dimension.
   *
   * Useful when operating on dimensions that have different distributions:
   *  - angle, magnitude
   *  - area, perimeter
   *  - etc.
   *
   * When using MAHALANOBIS, your 'distanceThreshold' is really in standard
   * deviations, not units.
   *
   * result = sqrt(sum((x0/s0)^2, ..., (xn/sn)^2))
   */
  public static final StatisticsNormEnum MAHALANOBIS = new StatisticsNormEnum(3);
  /**
   * The Euclidian Distance normalized so that a distance of 1 is 1 standard deviation.
   * Same as MAHALANOBIS.
   *
   * result = sqrt(sum((x0/s0)^2, ..., (xn/sn)^2))
   */
  public static final StatisticsNormEnum DEVIATION = MAHALANOBIS;

  /**
   * @author Patrick Lacz
   */
  private StatisticsNormEnum(int id)
  {
    super(id);
  }
}
