package com.axi.util;

import java.io.*;

/**
 * Unit test for the native "JNI mode" C++ sptAssert functionality.
 *
 * @author Matt Wharton
 */
public class Test_NativeAssert extends UnitTest
{
  // Static constructor
  static
  {
    System.loadLibrary("nativeAxiUtil");
  }

  /**
   * @author Matt Wharton
   */
  public Test_NativeAssert()
  {
    // Do nothing...
  }

  /**
   * Top level test method.
   *
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    // Validate that a failing sptAssert called straight from the JNI method causes
    // Assert.expect(false) to get called.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        NativeAssertFalse.nativeAssertFalseSimple();
      }
    });

    // Validate that a failing sptAssert called by a method in a seperate DLL which,
    // in turn, is called from the JNI method causes Assert.expect(false) to get called.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        NativeAssertFalse.nativeAssertFalseNested();
      }
    });
  }

  /**
   * Main method.
   *
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_NativeAssert());
  }
}
