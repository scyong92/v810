package com.axi.util;

import java.io.*;
import java.net.*;
import java.rmi.server.*;

/**
 * @author Chong Wei Chin
 */
public class TimeOutFactory extends RMISocketFactory
{

  private int timeout;

  /**
   * @param timeout
   * @author Chong Wei Chin
   */
  public TimeOutFactory(int timeout)
  {
    this.timeout = timeout;
  }

  /**
   * @param host
   * @param port
   * @return
   * @throws IOException
   */
  public Socket createSocket(String host, int port) throws IOException
  {
    Socket ret = getDefaultSocketFactory().createSocket(host, port);
    ret.setSoTimeout(timeout * 1000);
    return ret;
  }

  /*
   * @author Chong Wei Chin
   */
  public ServerSocket createServerSocket(int port) throws IOException
  {
    return getDefaultSocketFactory().createServerSocket(port);
  }
}
