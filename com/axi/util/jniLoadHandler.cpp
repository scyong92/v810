/**
 * Contains an implementation of JNI_OnLoad which wires the the JVM to the C++ sptAssert.
 * This enables sptAssert to actually invoke Assert.expect whenever the JVM is running.
 *
 * @author Matt Wharton
 */

#include <jni.h>

#include "cpp/util/src/sptAssert.h"


/**
 * Custom JNI_OnLoad implementation.  Wires up the JVM to the C++ sptAssert code.
 *
 * @author Matt Wharton
 */
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* pVirtualMachine, void* pReserved)
{
  // Pass the VM pointer to the sptAssert code.
  setupJniAssertMode(pVirtualMachine);

  return JNI_VERSION_1_4;
}
