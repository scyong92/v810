package com.axi.util;

import java.awt.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

/**
* The Assert class provides a way to check that assumptions made in your
* code are really correct.  If an Assert.expect(condition) fails because
* a condition is false, the Assert class will print a message to standard
* error and also log the information to a log file.  In addition the
* class will pull up a dialog box asking the customer if they want to send
* the information back to Agilent Technologies for them to investigate.  To use this class
* you must call setLogFile() in your main.
*
* @author Bill Darbie
*/
public class Assert
{
  // _logFileSet is true once a log file has been set using the
  // setLogFile() method
  private static boolean _logFileSet = false;  // set to true after the setLogFile() is called
  private static String _logFileName = null;  // the name to log the stack trace info to
  private static String _softwareBuildVersion = null;
  private static String _lineSeparator = System.getProperty("line.separator");
  private static boolean _sendEmail = false;
  private static Frame _frame = null;

  private static String _description = null;
  private static String _machineDescription = null;
  private static String _customerCompanyName = null;
  private static String _customerName = null;
  private static String _customerEmailAddress = null;
  private static String _customerPhoneNumber = null;
  private static String _smtpServer = null;
  private static String _axiSupportEmailAddress = null;
  private static String _emailSubject = null;

  private static boolean _exitOnAssert = true;
  private static boolean _assertHandlingInProgress = false;
  
  private static AssertObservable _assertObservable = AssertObservable.getInstance();
  
  private static boolean _useErrorHandler = true;
  
  //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
  private static String _assertTimeLog = "errorTime";  // the name to log the stack trace assert time
  private static String _assertTimeFileName = null;  // the name to log the stack trace assert time
  
  /**
  * This method sets the file name that the current executable will
  * write assert information to.  It is recommended that the
  * log file name be the fully qualified package name of the class with the main in it.
  * @param logfile is the name of the file where asserts will be logged
  *                to.  This should be called in your main() method for
  *                applications and in your init() method for applets.
  *                It should be the same as your fully qualified main
  *                class name.  The extension .log will automatically
  *                be added to whatever name you pass in.  For example
  *                for this class you would make a call like this:
  *                Assert.setLogFile(MyClass.class.getClass().getName());
  *                Use -DassertLogDir=logDir to
  *                tell this class what directory to put the log files.
  * @param versionAndSerialNumber is a string representing the current version of the software.
  *        This helps identify what version of code a customer was running
  *        when the assertion occurred.
  * @author Bill Darbie
  */
  public static void setLogFile(String logfile, String versionAndSerialNumber)
  {
    _softwareBuildVersion = versionAndSerialNumber;

    // set _logFileSet to true to indicate the the setLogFile() method was called
    _logFileSet = true;

    // get the directory where the log file should go
    String logDir = "";
    try
    {
      logDir = System.getProperty("assertLogDir", null);
      if (logDir == null)
      {
        String userDir = System.getProperty("user.dir");
        int index = userDir.indexOf("java");
        if (index > 0)
          logDir = userDir.substring(0, index) + "custRel" + File.separator
                                               + "v810" + File.separator + "x.xx" + File.separator + "log";
        else
          logDir = userDir + File.separator + "log";
      }
    }
    catch(SecurityException se)
    {
      // do nothing
    }
    _logFileName = new String();
    //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
    _assertTimeFileName = new String();
    if (logDir.equals(""))
      _logFileName = logfile;
    else
    {
      _logFileName = logDir + File.separator + logfile;
      _assertTimeFileName = logDir + File.separator + _assertTimeLog;
    }
  }

  /**
   * @author Bill Darbie
   */
  public static void setExitOnAssert(boolean exitOnAssert)
  {
    _exitOnAssert = exitOnAssert;
  }

  /**
  * Turn off the automatic sending of the stack trace to Agilent technologies using email.
  *
  * @author Bill Darbie
  */
  public static void doNotSendEmail()
  {
    _sendEmail = false;
  }

  /**
  * Turn on the automatic sending of the stack trace to Agilent technologies using email.
  *
  * @author Bill Darbie
  */
 public static void sendEmail(Frame frame,
                              String description,
                              String machineDescription,
                              String customerCompanyName,
                              String customerName,
                              String customerEmailAddress,
                              String customerPhoneNumber,
                              String smtpServer,
                              String axiSupportEmailAddress,
                              String emailSubject)
 {
    // put this in so in JBuilder, we don't send email and this crashes the GUI designer
    if (java.beans.Beans.isDesignTime())
      _sendEmail = false;
    else
      _sendEmail = true;

    _frame = frame;
    _description = description;
    _machineDescription = machineDescription;
    _customerCompanyName = customerCompanyName;
    _customerName = customerName;
    _customerEmailAddress = customerEmailAddress;
    _customerPhoneNumber = customerPhoneNumber;
    _smtpServer = smtpServer; 
    _axiSupportEmailAddress = axiSupportEmailAddress;
    _emailSubject = emailSubject;
  }


  /**
  * The message and the stack trace will be sent to standard error and to the
  * log file specified by the setLogFile() method.  A run time exception
  * will be thrown if the condition is false.  If -DASSERT=off used
  * the exception will not be thrown and nothing will be reported to
  * standard error.  Information will still go to the log file.
  * @param condition the condition to check, if it is true execution
  *                  continues, if it is false an AssertException is
  *                  thrown or the exception is logged and execution continues
  *                  depending on the -DASSERT=on|off
  * @author Bill Darbie
  */
  public static void expect(boolean condition)
  {
    expect(condition, "");
  }

  /**
  * The message and the stack trace will be sent to standard error and to the
  * log file specified by the setLogFile() method.  A run time exception
  * will be thrown if the condition is false.  If -DASSERT=off used
  * the exception will not be thrown and nothing will be reported to
  * standard error.  Information will still go to the log file.
  *
  * NOTE: The native sptAssert macro depends on this signature.  If you change
  * the signature of this method, be sure to change sptAssert.cpp to reflect it!
  * Test_NativeAssert should catch this if it happens.
  *
  * @param condition the condition to check, if it is true execution
  *                  continues, if it is false an AssertException is
  *                  thrown or the exception is logged and execution continues
  *                  depending on the -DASSERT=on|off
  * @param message the message that will be reported if condition if false
  *
  * @author Bill Darbie
  */
  public static void expect(boolean condition, String message)
  {
    if ((_assertHandlingInProgress) && (UnitTest.unitTesting() == false))
      return;

    if (_logFileSet == false)
    {
      System.out.println("WARNING: setLogFile() was not called in your main() or init() method");
      _logFileSet = true;
    }

    if (condition == false)
    {
        _assertHandlingInProgress = true;
        AssertException assertException = new AssertException(message);

        logData(assertException);

        throw assertException;
      }    
  }
  
  /**
   * @author swee-yee.wong
   */
  public static boolean expectWithErrorPrompt(boolean condition, final ErrorHandlerEnum errorHandlerEnum)
  {
    String[] exceptionParameters = new String[]{};
    return expectWithErrorPrompt(condition, errorHandlerEnum, exceptionParameters);
  }

  /**
   * @author swee-yee.wong
   */
  public static boolean expectWithErrorPrompt(boolean condition, final ErrorHandlerEnum errorHandlerEnum, final String[] exceptionParameters)
  {
    if ((_assertHandlingInProgress) && (UnitTest.unitTesting() == false))
      return false;

    if (_logFileSet == false)
    {
      System.out.println("WARNING: setLogFile() was not called in your main() or init() method");
      _logFileSet = true;
    }

    if (condition == false)
    {
      _assertHandlingInProgress = true;
      com.axi.v810.util.XrayTesterException xrayTesterException = new com.axi.v810.util.XrayTesterException(new LocalizedString(errorHandlerEnum.toString(), null));

      logErrorHandlerData(xrayTesterException);
      
      try
      {
        com.axi.v810.datastore.ErrorHandlerLogUtil.getInstance().log(errorHandlerEnum.toString());
      }
      catch(com.axi.v810.util.XrayTesterException e)
      {
        System.out.println("Failed to get machine descriptions. \n" + e.getMessage());
      }

      com.axi.v810.util.InfoHandlerPanel.getInstance().executeCommand(com.axi.v810.gui.mainMenu.MainMenuGui.getInstance(), "DISPLAY-ERROR|" + errorHandlerEnum.toString(), exceptionParameters);

      _assertHandlingInProgress = false;
      return true;
    } 
    else
    {
      return false;
    }
  }

  /**
   * @author Bill Darbie
   */
  private static synchronized void logData(Throwable exception)
  {
    final StringWriter stringWriter = new StringWriter();
    final PrintWriter printStream = new PrintWriter(stringWriter);

    Date date = new Date();
    String headerString = "##############################################################################" + _lineSeparator +
                          "AXI System v810 Internal Error Stack Trace" + _lineSeparator +
                          "Software Version: " + _softwareBuildVersion + _lineSeparator +
                          "Machine Description: " + _machineDescription + _lineSeparator +
                          date + _lineSeparator + _lineSeparator;

    if (_logFileName != null)
      logExceptionToFile(exception, headerString, _logFileName, true);
    
    //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
    if (_assertTimeFileName != null)
      logExceptionToFile(exception, headerString, _assertTimeFileName, false);

    // If running from the IDE, we want to get a stack trace out so we can click on the file/line numbers
    // regression tests will often pass in a null parameter and expect an Assert.expect() to fail
    // in this case we do not want to print out a stack trace to the expect file
    if (System.getProperty("RUNNING_IN_IDE") != null && UnitTest.unitTesting() == false)
      exception.printStackTrace();

    if ((_sendEmail) && (UnitTest.unitTesting() == false))
    {
      // put information into the stringStream for the Assert Dialog
      stringWriter.write("AXI System v810 Internal Error Stack Trace");
      stringWriter.write(_lineSeparator);
      stringWriter.write("Software Version: " + _softwareBuildVersion);
      stringWriter.write(_lineSeparator);
      stringWriter.write("Machine Description: " + _machineDescription);
      stringWriter.write(_lineSeparator);
      stringWriter.write(date.toString());
      stringWriter.write(_lineSeparator);
      exception.printStackTrace(printStream);
      stringWriter.write(_lineSeparator);
      printStream.close();

      StringBuffer des = new StringBuffer();
      des.append("An internal software error occurred.  <br><br>");
      if (exception.getMessage() != null && exception.getMessage() != "")
        des.append("<b>" + exception.getMessage() + "</b><br><br>");
      des.append("To send this information to AXI System click the Send Email button.  <br>");
/**
 * @todo change below to the correct internet address
 */
//      des.append("You can check <A HREF=http://www.vitrox.com/key/boardtest>www.vitrox.com/key/boardtest</A> to see if there is a fix for this problem.  <br>");
      des.append("This error information can also be found in the log file: <br>" + _logFileName + ".log");
      _description = des.toString();

      if (EventQueue.isDispatchThread())
      {
        // we are already on the swing thread
        exitDialog(stringWriter);
      }
      else
      {
        SwingUtilities.invokeLater(new Runnable()
        {
          public void run()
          {
            exitDialog(stringWriter);
          }
        });
      }
    }
  }

  /**
   * @author swee-yee.wong
   */
  private static synchronized void logErrorHandlerData(Throwable exception)
  {
    final StringWriter stringWriter = new StringWriter();
    final PrintWriter printStream = new PrintWriter(stringWriter);

    Date date = new Date();
    String headerString = "##############################################################################" + _lineSeparator +
                          "AXI System v810 Internal Error Stack Trace" + _lineSeparator +
                          "Software Version: " + _softwareBuildVersion + _lineSeparator +
                          "Machine Description: " + _machineDescription + _lineSeparator +
                          date + _lineSeparator + _lineSeparator;

    if (_logFileName != null)
    {
      logExceptionToFile(exception, headerString, _logFileName, true);
    }

    // If running from the IDE, we want to get a stack trace out so we can click on the file/line numbers
    // regression tests will often pass in a null parameter and expect an Assert.expect() to fail
    // in this case we do not want to print out a stack trace to the expect file
    if (System.getProperty("RUNNING_IN_IDE") != null && UnitTest.unitTesting() == false)
      exception.printStackTrace();

    if ((_sendEmail) && (UnitTest.unitTesting() == false))
    {
      // put information into the stringStream for the Assert Dialog
      stringWriter.write("AXI System v810 Internal Error Stack Trace");
      stringWriter.write(_lineSeparator);
      stringWriter.write("Software Version: " + _softwareBuildVersion);
      stringWriter.write(_lineSeparator);
      stringWriter.write("Machine Description: " + _machineDescription);
      stringWriter.write(_lineSeparator);
      stringWriter.write(date.toString());
      stringWriter.write(_lineSeparator);
      exception.printStackTrace(printStream);
      stringWriter.write(_lineSeparator);
      printStream.close();

      StringBuffer des = new StringBuffer();
      des.append("An internal software error occurred.  <br><br>");
      if (exception.getMessage() != null && exception.getMessage() != "")
        des.append("<b>" + exception.getMessage() + "</b><br><br>");
      des.append("To send this information to AXI System click the Send Email button.  <br>");
/**
 * @todo change below to the correct internet address
 */
//      des.append("You can check <A HREF=http://www.vitrox.com/key/boardtest>www.vitrox.com/key/boardtest</A> to see if there is a fix for this problem.  <br>");
      des.append("This error information can also be found in the log file: <br>" + _logFileName + ".log");
      _description = des.toString();

    }
  }
    
  /**
   * @author Bill Darbie
   */
  private static void exitDialog(StringWriter stringWriter)
  {
    AssertDialog dialog = new AssertDialog(_frame,
                                           true,
                                           _description,
                                           _customerCompanyName,
                                           _customerName,
                                           _customerEmailAddress,
                                           _customerPhoneNumber,
                                           stringWriter.toString(),
                                           _smtpServer,
                                           _axiSupportEmailAddress,
                                           _emailSubject);
    dialog.setVisible(true);
    dialog.toFront();
    if (_exitOnAssert)
    {
      if (_assertObservable.countObservers() > 0)
      {
        _frame.dispose();      
        _assertObservable.sendEvent();
      }
      else
        System.exit(1);
    }
  }

  /**
   * @author Bill Darbie
   * @author Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
   */
   private static synchronized void logExceptionToFile(Throwable exception, String headerString, String fileName, boolean append)
   {
     fileName = fileName + ".log";

     FileOutputStream fileStream = null;
     PrintWriter fileWriter = null;

     try
     {
       // open in append mode
       fileStream = new FileOutputStream(fileName, append);
     }
     catch (IOException ioe)
     {
       System.err.println("ERROR: com.axi.util.LogUtil.logExceptionToFile()");
       System.err.println("       Could not open assert log file: " + fileName);
       fileStream = null;
       // let the program keep running anyway
     }
     catch (SecurityException se)
     {
       // must be in an applet - do not write the file
       fileStream = null;
     }

     if (fileStream != null)
       fileWriter = new PrintWriter(fileStream);

     // print out information to the log file
     if(fileWriter != null)
     {
       fileWriter.println(headerString);
       exception.printStackTrace(fileWriter);
       fileWriter.println();
       fileWriter.flush();
       fileWriter.close();
     }
   }

  /**
  * Call this if you want to log any exception that might get thrown.  The advantage of calling this
  * method is that it will log the stack trace to the log file and it will ask the customer if
  * they want to email the trace to Agilent Technologies.
  *
  * @param exception is the Exception that was caught
  * @author Bill Darbie
  */
  public static void logException(Throwable exception)
  {
    if (_assertHandlingInProgress)
      return;

    if (exception instanceof AssertException)
      throw (AssertException)exception;
    else
    {
      exception.printStackTrace();
      // We have an exception that is not an AssertException so it needs to be logged
      logData(exception);
    }

    AssertException assertException = new AssertException();
    assertException.initCause(exception);
    throw assertException;
  }
};


