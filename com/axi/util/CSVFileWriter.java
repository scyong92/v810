package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * Helper class for writing data out to a CSV formatted file.
 *
 * @author Matt Wharton
 */
public class CSVFileWriter
{
  public static final char _COMMA = ',';

  private PrintWriter _filePrintWriter;

  private Collection<String> _columnHeaders = null;

  /**
   * @author Matt Wharton
   */
  public CSVFileWriter()
  {
  }

  /**
   * @author Matt Wharton
   */
  public CSVFileWriter(Collection<String> columnHeaders)
  {
    Assert.expect(columnHeaders != null);

    _columnHeaders = columnHeaders;
  }

  /**
   * Opens the specified file for writing.
   *
   * @author Matt Wharton
   */
  public void open(String fileName, boolean append) throws FileNotFoundException
  {
    Assert.expect(fileName != null);

    boolean fileAlreadyExists = FileUtil.exists(fileName);
    File file = new File(fileName);
    FileOutputStream fileOutputStream = new FileOutputStream(file, append);
    _filePrintWriter = new PrintWriter(fileOutputStream, true);

    if ((_columnHeaders != null) && ((append == false) || (fileAlreadyExists == false)))
    {
      if (_columnHeaders.isEmpty() == false)
      {
        String columnHeaderLine = StringUtil.join(_columnHeaders, _COMMA);
        _filePrintWriter.println(columnHeaderLine);
      }
    }
  }

  /**
   * Opens the specfied file for writing.  Assumes we're NOT appending.
   *
   * @author Matt Wharton
   */
  public void open(String fileName) throws FileNotFoundException
  {
    Assert.expect(fileName != null);

    open(fileName, false);
  }

  /**
   * @author Matt Wharton
   */
  public void close()
  {
    if (_filePrintWriter != null)
    {
      _filePrintWriter.close();
      _filePrintWriter = null;
    }
  }

  /**
   * @author Matt Wharton
   */
  public void writeDataLine(Collection<String> dataValues)
  {
    Assert.expect(dataValues != null);
    Assert.expect(_filePrintWriter != null);

    // If column headers were specified, make sure that the number of data values matches the number of column headers.
    if (_columnHeaders != null)
    {
      Assert.expect(_columnHeaders.size() == dataValues.size());
    }

    // Make a comma-delimited line and write it.
    String dataLine = StringUtil.join(dataValues, _COMMA);
    _filePrintWriter.println(dataLine);
  }
}
