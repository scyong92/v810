package com.axi.util;

import java.io.*;

/**
 * @author Bill Darbie
 */
public class CouldNotDeleteFileException extends IOException
{
  private String _fileName;

  /**
   * @author Bill Darbie
   */
  public CouldNotDeleteFileException(String fileName)
  {
    super("ERROR: Could not delete directory " + fileName);
    Assert.expect(fileName != null);
    _fileName = fileName;
  }

  /**
   * @author Bill Darbie
   */
  public String getFileName()
  {
    return _fileName;
  }
}
