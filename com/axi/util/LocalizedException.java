package com.axi.util;

import com.axi.util.*;

/**
 * All Exceptions that inherit from this class will support localized messages.
 *
 * @author Bill Darbie
 */
public class LocalizedException extends Exception
{
  private LocalizedString _localizedString;

  /**
   * Create this exception with a localized string.
   * @author Bill Darbie
   */
  public LocalizedException(LocalizedString localizedString)
  {
    Assert.expect(localizedString != null);

    _localizedString = localizedString;
  }

  /**
   * @author Bill Darbie
   */
  public LocalizedString getLocalizedString()
  {
    Assert.expect(_localizedString != null);

    return _localizedString;
  }
}
