package com.axi.util;

import java.io.*;

/**
 * Class to hold a pair of objects.  This is intended to be similar to std::pair in C++.  I'd like to
 * make 'first' and 'second' public in that respect, but Herr Darbie says nein.
 *
 * Note: This class does NOT make defensive copies of its fields (either inbound or outbound).
 *
 * @author Matt Wharton
 */
public class Pair<F, S> implements Serializable
{
  private F _first = null;
  private S _second = null;

  /**
   * Default constructor.
   *
   * @author Matt Wharton
   */
  public Pair()
  {
    _first = null;
    _second = null;
  }

  /**
   * Constructor.  Sets up a pair with the specified objects.
   *
   * @author Matt Wharton
   */
  public Pair(F first, S second)
  {
    // No asserts.  There are some cases where it would make sense to have 'null' values.
    _first = first;
    _second = second;
  }


  /**
   * Copy Constructor.
   *
   * @author Matt Wharton
   */
  public Pair(Pair<F, S> rhs)
  {
    Assert.expect(rhs != null);

    _first = rhs._first;
    _second = rhs._second;
  }


  /**
   * Gets the first element of the pair.
   *
   * @author Matt Wharton
   */
  public F getFirst()
  {
    // No asserts.  There are some cases where it would make sense to have 'null' values.
    return _first;
  }


  /**
   * Gets the second element of the pair.
   *
   * @author Matt Wharton
   */
  public S getSecond()
  {
    // No asserts.  There are some cases where it would make sense to have 'null' values.
    return _second;
  }


  /**
   * Sets the first element of the Pair.
   *
   * @author Matt Wharton
   */
  public void setFirst(F first)
  {
    // No asserts.  There are some cases where it would make sense to have 'null' values.
    _first = first;
  }


  /**
   * Sets the second element of the Pair.
   *
   * @author Matt Wharton
   */
  public void setSecond(S second)
  {
    // No asserts.  There are some cases where it would make sense to have 'null' values.
    _second = second;
  }


  /**
   * Sets the Pair to the specified values.
   *
   * @author Matt Wharton
   */
  public void setPair(F first, S second)
  {
    // No asserts.  There are some cases where it would make sense to have 'null' values.
    _first = first;
    _second = second;
  }


  /**
   * Sets this Pair to the values contained in the specified Pair.
   *
   * @author Matt Wharton
   */
  public void setPair(Pair<F, S> rhs)
  {
    Assert.expect(rhs != null);

    _first = rhs._first;
    _second = rhs._second;
  }

  /**
   * Override of Object.equals().  Performs an equality test of both pair elements.
   *
   * This is based on the algortihm discussed in "Effective Java" Item 7.
   *
   * @author Matt Wharton
   */
  public boolean equals(Object rhs)
  {
    // Check for simple reference equality.
    if (this == rhs)
    {
      return true;
    }

    // Make sure that the object were comparing against is actually a Pair object.
    if ((rhs instanceof Pair) == false)
    {
      return false;
    }

    Pair rhsPair = (Pair)rhs;

    return ((_first == null ? rhsPair._first == null : _first.equals(rhsPair._first))
            && (_second == null ? rhsPair._second == null : _second.equals(rhsPair._second)));
  }

  /**
   * Override of Object.hashCode().
   *
   * This is based on the algortihm discussed in "Effective Java" item 8.
   *
   * @author Matt Wharton
   */
  public int hashCode()
  {
    // Initial hashcode seed.
    int finalHashcode = 17;

    // Apply the 'first' field to the hashcode.
    int hashCodeForCurrentField =(_first != null ? _first.hashCode() : 0);
    finalHashcode = (37 * finalHashcode) + hashCodeForCurrentField;

    // Apply the 'second' field to the hashcode.
    hashCodeForCurrentField =(_second != null ? _second.hashCode() : 0);
    finalHashcode = (37 * finalHashcode) + hashCodeForCurrentField;

    return finalHashcode;
  }
}
