/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.util;

import java.util.*;

/**
 *
 * @author weng-jian.eoh
 */
public class SoftwareRevisionEnum extends Enum
{

  private static int _index = -1;
  private String _name;
  public static List <SoftwareRevisionEnum> _softwareRevisionEnumFullList = new ArrayList<SoftwareRevisionEnum>();
  public static List<String> _softwareRevisionNameFullList = new ArrayList<>();
  
  public static final SoftwareRevisionEnum _5_11 = new SoftwareRevisionEnum(++_index, "5.11");
  public static final SoftwareRevisionEnum _5_10 = new SoftwareRevisionEnum(++_index, "5.10");
  public static final SoftwareRevisionEnum _5_9 = new SoftwareRevisionEnum(++_index, "5.9");  
  public static final SoftwareRevisionEnum _5_8 = new SoftwareRevisionEnum(++_index, "5.8");  
  public static final SoftwareRevisionEnum _5_7 = new SoftwareRevisionEnum(++_index, "5.7");  
  public static final SoftwareRevisionEnum _5_6 = new SoftwareRevisionEnum(++_index, "5.6");  
//  public static final SoftwareRevisionEnum FIVE_FIVE = new SoftwareRevisionEnum(++_index, "5.5"); 

  private SoftwareRevisionEnum(int id, String name)
  {
    super(id);
    _name = name.intern();
    _softwareRevisionEnumFullList.add(this);
    _softwareRevisionNameFullList.add(_name);
  }

  public String toString()
  {
    return _name;
  }
  public static List <SoftwareRevisionEnum> getFullEnumList()
  {
    return _softwareRevisionEnumFullList;
  }
  public static List<String> getFullStringList()
  {
    return _softwareRevisionNameFullList;
  }
}
