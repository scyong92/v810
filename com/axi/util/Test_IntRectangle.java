package com.axi.util;

import java.io.*;

/**
 * @author Keith Lee
 */
class Test_IntRectangle extends UnitTest
{
  public static void main(String args[])
  {
    UnitTest.execute(new Test_IntRectangle());
  }

  /**
   * @author Keith Lee
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    boolean exception = false;

    IntCoordinate topRightCoordinate = new IntCoordinate(300, 400);
    IntCoordinate bottomLeftCoordinate = new IntCoordinate(100, 200);
    int width = Math.abs( topRightCoordinate.getX() - bottomLeftCoordinate.getX() );
    int height = Math.abs( topRightCoordinate.getY() - bottomLeftCoordinate.getY() );

    IntRectangle rectangle = new IntRectangle(bottomLeftCoordinate.getX(), bottomLeftCoordinate.getY(), width, height);
    Expect.expect( rectangle.getMinX() == bottomLeftCoordinate.getX() );
    Expect.expect( rectangle.getMinY() == bottomLeftCoordinate.getY() );
    Expect.expect( rectangle.getMaxX() == topRightCoordinate.getX() );
    Expect.expect( rectangle.getMaxY() == topRightCoordinate.getY() );

    rectangle.setRect(0, 0, 10, 10);
    IntRectangle rectangle2 = new IntRectangle(5, 5, 10, 20);
    rectangle.add(rectangle2);
    // rectangle should now be the union of the two rectangles
    Expect.expect(rectangle.getMinX() == 0);
    Expect.expect(rectangle.getMinY() == 0);
    Expect.expect(rectangle.getHeight() == 25);
    Expect.expect(rectangle.getWidth() == 15);

    rectangle.setRect(0, 0, 10, 10);
    rectangle2.setRect(15, 15, 5, 5);
    rectangle.add(rectangle2);
    // rectangle should now be the union of the two rectangles
    Expect.expect(rectangle.getMinX() == 0);
    Expect.expect(rectangle.getMinY() == 0);
    Expect.expect(rectangle.getHeight() == 20);
    Expect.expect(rectangle.getWidth() == 20);

  }
}
