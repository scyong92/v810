package com.axi.util;

import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.charset.*;
import java.util.*;
import java.net.*;

/**
 * ******************************************************************************************************* *                                                                                                        *
 * This class is finished with its implemetation, but is not throughly reviewed yet.                       *
 * The following is the notes for Eugene Kim-Leighton.                                                     *
 * Things to be done:                                                                                      *
 *     1) Find the right way to handle the exceptions thrown in putFile().                                 *
 *                                                                                                         *
 * ******************************************************************************************************* *
 * This class provides an interface to handle common FTP client control.  All the control is managed in the
 * background, not through a console or graphical user interface.  When the connection is established between
 * the FTP client (Client) and FTP server (Server), the Client must wait for the Server's response indicating
 * that the Server is ready to receive any request or command from the Client.  The server will send a reply
 * for every client's command.  This will act as a handshake; veryfing the command completed.  Server sends
 * the text reply that starts with three-digit numeric code, and there are 5 different types of replies which
 * are distingished by the first digit.
 *
 * There is a set of reply codes to expect for the each command.  In this class, we just check the valid range
 * of the reply code, instead of checking whether or not the reply code is one in the set, for now.
 *
 * For more details about the FTP protocol, please refer RFC (Request For Comment) 765.  RFCs are standard
 * documents for Internet that are developed by the Internet Engineering Task Force.  RFC 765 states standard
 * for FTP.  (A possible web site to find this document: www.wu-ftpd.org/rfc/rfc0959.txt)
 *
 * Most of the methods provided in this class are corresponding to the FTP commands that are defined in RFC 765.
 * Please read this documents before changing or adding any method in this class.
 *
 * @author Eugene Kim-Leighton
 */
public class FtpClient
{
  // Access Control Commands
  private static final String _USER_NAME                  = "USER";
  private static final String _PASSWORD                   = "PASS";
  private static final String _ACCOUNT                    = "ACCT";
  private static final String _CHANGE_DIRECTORY           = "CWD";
  private static final String _CHANGE_TO_PARENT           = "CDUP";
  private static final String _STRUCTURE_MOUNT            = "SMNT";
  private static final String _REINITIALIZE               = "REIN";
  private static final String _LOGOUT                     = "QUIT";

  // Transfer Parameter Commands
  private static final String _DATA_PORT                  = "PORT";
  private static final String _PASSIVE                    = "PASV";
  private static final String _REPRESENT_TYPE             = "TYPE";// by default: ASCII Non-print
  private static final String _FILE_STRUCTURE             = "STRU";// by default: File
  private static final String _TRANSFER_MODE              = "MODE";// by default: Stream

  // FTP Service Commands - define the file transfer or file system function requested by user
  private static final String _RETRIEVE                   = "RETR";// via data connection to user
  private static final String _STORE                      = "STOR";// via data connection to server
  private static final String _STORE_UNIQUE               = "STOU";
  private static final String _APPEND                     = "APPE";
  private static final String _ALLOCATE                   = "ALLO";
  private static final String _RESTART                    = "REST";
  private static final String _RENAME_FROM                = "RNFR";
  private static final String _RENAME_TO                  = "RNTO";
  private static final String _ABORT                      = "ABOR";
  private static final String _DELETE                     = "DELE";
  private static final String _REMOVE_DIR                 = "RMD";
  private static final String _MAKE_DIR                   = "MKD";
  private static final String _PRINT_WORKING_DIR          = "PWD";
  private static final String _LIST                       = "LIST";
  private static final String _NAME_LIST                  = "NLST";
  private static final String _SITE                       = "SITE";
  private static final String _SYSTEM                     = "SYST";
  private static final String _STATUS                     = "STAT";
  private static final String _HELP                       = "HELP";
  private static final String _NOOP                       = "NOOP";

  private static final String _SPACE_STR                  = " ";
  private static final String _END_OF_STRING              = "\r\n";
  private static final String _GOOD_BYE                   = "221 Goodbye.";
  private static final int    _READY_FOR_LOGIN            = 220;
  private static final int    _WAIT_FOR_LOGIN             = 120;
  private static final int    _PASSWORD_REQUIRED          = 331;
  private static final int    _SUCESSFUL_CODE_LOWER_RANGE = 200;
  private static final int    _SUCESSFUL_CODE_UPPER_RANGE = 399;
  private static final int    _WAIT_CODE_LOWER_RANGE      = 100;
  private static final int    _WAIT_CODE_UPPER_RANGE      = 199;
  private static final int    _ERROR_CODE_LOWER_RANGE     = 400;
  private static final int    _ERROR_CODE_UPPER_RANGE     = 599;
  private static final int    _FILE_DATA_SIZE             = 1024;

  private static final int    _FTP_PORT                   = 21;   // This is a well known port for the FTP

  private SocketChannel       _dataSocket                 = null;
  private InetAddress         _dataSocketLocalIpAddress   = null;
  private InetAddress         _dataSocketRemoteIPAddress  = null;
  private int                 _dataSocketRemotePort       = -1;
  private int                 _dataSocketPort             = -1;

//  private BufferedReader      _dataSocketInputStream      = null;
//  private PrintWriter         _dataSocketOutputStream     = null;

  // prepare the encoder and decoder for char outputs and outputs via nio
  private static Charset charset = Charset.forName("ISO-8859-1"); // ISO Latin Alphabet #1
  private static CharsetEncoder encoder = charset.newEncoder();
  private static CharsetDecoder decoder = charset.newDecoder();

  private ByteBuffer buffer = ByteBuffer.allocateDirect(_FILE_DATA_SIZE);
  private CharBuffer charBuffer = CharBuffer.allocate(_FILE_DATA_SIZE);

  private List _replies = null;

  private SocketClient _socketClient = null;
  private String _serverIPAddress = null;
  private String _ipAddress = null;

// * The syntax for the rest of FTP commands that are not implemented in this class
// * ACCT <SP> <account-information> <CRLF>
// * CDUP <CRLF>
// * PORT <SP> <host-port> <CRLF>
// * SMNT <SP> <pathname> <CRLF>
// * STRU <SP> <structure-code> <CRLF>
// * MODE <SP> <mode-code> <CRLF>
// * STOU <CRLF>
// * APPE <SP> <pathname> <CRLF>
// * ALLO <SP> <decimal-integer> [<SP> R <SP> <decimal-integer>] <CRLF>
// * REST <SP> <marker> <CRLF>
// * ABOR <CRLF>
// * NLST [<SP> <pathname>] <CRLF>
// * SITE <SP> <string> <CRLF>
// * SYST <CRLF>
// * STAT [<SP> <pathname>] <CRLF>
// * HELP [<SP> <string>] <CRLF>
// * NOOP <CRLF>

  /**
   * @param ipAddress is the address of the FTP server to connect
   * @author Eugene Kim-Leighton
   */
  public FtpClient(String ipAddress)
  {
    Assert.expect(ipAddress != null);
    _socketClient = new SocketClient(ipAddress, _FTP_PORT);
    _ipAddress = ipAddress;
  }

  /**
   * In FTP, two socket connections are established, one for commands and the other for any data or infomation
   * needs to be transfered between client and server.  This method creates a socket for the data connection.
   * Whenever, it requires to transfer data other than a request and replies between the client and server,
   * data connection needs to be open until transfer is done.  Any data is sent via this connection.
   *
   * @throws SocketConnectionFailedException if there is an error opening the data control connection.
   * @author Eugene Kim-Leighton
   */
  private void openDataConnection() throws SocketConnectionFailedException
  {
    _dataSocketRemoteIPAddress = _socketClient.getInetAddress();
    _dataSocketRemotePort = _socketClient.getPort();
    _dataSocketLocalIpAddress = _socketClient.getLocalAddress();
    Assert.expect(_dataSocketPort > 0);
    try
    {
      _dataSocket = SocketChannel.open();
      _dataSocket.connect(new InetSocketAddress(_dataSocketRemoteIPAddress, _dataSocketRemotePort));
    }
    catch(IOException ioe)
    {
      SocketConnectionFailedException ex = new SocketConnectionFailedException(_dataSocketLocalIpAddress.getHostName(), _dataSocketPort, ioe.getMessage());
      ex.initCause(ioe);
      throw ex;
    }
  }

  /**
   * This opens the streams for sending data/file to the FTP server and receiving data/file from the FTP server.
   *
   * @throws IOException if there is an error opening streams.
   * @author Eugene Kim-Leighton
   */
/*
 private void openDataStreams() throws IOException
  {
    Assert.expect(_dataSocket != null);

    // get the input stream
    InputStream inputStream         = _dataSocket.getInputStream();
    InputStreamReader streamReader  = new InputStreamReader(inputStream);
    _dataSocketInputStream          = new BufferedReader(streamReader);

    // get the output stream
    OutputStream outputStream       = _dataSocket.getOutputStream();
    _dataSocketOutputStream         = new PrintWriter(outputStream);
  }
*/

  /**
   * Whenever the clien is done with sending data to and receiving data from the FTP server, it needs to
   * close the data connection until the next transfer.
   *
   * @throws SocketConnectionFailedException if there is an error closing the connection from FTP server.
   * @author Eugene Kim-Leighton
   */
  private void closeDataConnection() throws SocketConnectionFailedException
  {
    try
    {
      if (_dataSocket != null)
      {
        _dataSocket.close();
        _dataSocket = null;
        _dataSocketRemoteIPAddress = null;
      }

/*
      if(_dataSocketInputStream != null)
      {
        _dataSocketInputStream.close();
        _dataSocketInputStream = null;
      }

      if(_dataSocketOutputStream != null)
      {
        _dataSocketOutputStream.close();
        _dataSocketOutputStream = null;
      }
*/
    }
    catch(IOException ioe)
    {
      SocketConnectionFailedException ex = new SocketConnectionFailedException(_dataSocketLocalIpAddress.getHostName(), _dataSocketPort, ioe.getMessage());
      ex.initCause(ioe);
      throw ex;
    }
  }

  /**
   * This establishes the control connection to the FTP server.
   * The control connection is used for sending a request/command to and receiving responses from the FTP server.
   *
   * @throws SocketConnectionFaileedException if there is an error establishing a connection.
   * @author Eugenen Kim-Leighton
   */
  private void openControlConnection() throws SocketConnectionFailedException
  {
    try
    {
      _socketClient.connect();
      receiveConnectionReplies();
    }
    catch(IOException ioe)
    {
      SocketConnectionFailedException ex = new SocketConnectionFailedException(_dataSocketLocalIpAddress.getHostName(), _dataSocketPort, ioe.getMessage());
      ex.initCause(ioe);
      throw ex;
    }
  }

  /**
   * This receives the response list that contains one line of response from the server.
   * This function then converts the response into a string that is returned.
   *
   * @return a string representing the server's response.
   * @throws IOException if there is an error receiving a response from the server.
   * @author Eugene Kim-Leighton
   */
  private String receiveReply() throws IOException
  {
    boolean matchEntireLine = false;
    boolean isHandshake = false;
    List<String> list = _socketClient.receiveReplies(_END_OF_STRING, matchEntireLine, isHandshake);
    Assert.expect(list.size() == 1);
    String reply = list.get(0);
    return reply;
  }

  /**
   * This method checks if there is a response indicates an error from the FTP server.  A reply from the
   * server is defined to contain the 3-digit code, followed by space, followed by one line of text, and
   * terminated by the Telnet end-of-line code (CR LF).  There will be cases however, where the text is
   * longer than a single line.  The format for multi-line replies is that the first line will begin with
   * the exact required reply code, followed immediately by a Hyphen, "-" (also known as Minus), followed
   * by text.  The last line will begin with the same code, followed immediately by Space, optionally some
   * text, and the Telnet end-of-line code.  For example:
   *   123-First line
   *   Second line
   *   123 The last line
   *
   * There are 5 different meanings with 3 digit code:
   *  1) a code between 100 and 199: The requested action is being initiated by the server.
   *                                 The client must wait for another eply before sending a new command.
   *  2) a code between 200 and 299: The requested action has been successfully completed by the server.
   *                                 A new request may be initiated by the client.
   *  3) a code between 300 and 399: The command has been accepted by the server,
   *                                 but the requested action is being held in abeyance, pending receipt of further information.
   *                                 The Client should send another command specifying this information.
   *                                 This reply is used in command sequence groups such as "USER, PASS" and "RNFR, RNTO".
   *                                 In this class, since it is known what commands are sequence group,
   *                                 this range of code is considered as sucessfully completed.
   *  4) a code between 400 and 499: The command was not accepted and the requested action did not take place,
   *                                 but the error condition is temporary and the action may be requested again.
   *                                 However, in this class, we just throw the exception with the error message
   *                                 when we receive this kind of code.
   *  5) a code between 500 and 599: The command was not accepted and the requested action did not take place
   *                                 and the error condition is permanent.
   *
   * @param reply is the server's response with 3 digit code.
   * @throws SocketMessageInvalidException if the FTP server's response indicates that the FTP client's request is not valid for server's system.
   * @throws SocketMessageFailedException if the FTP server's response indicates that there was an error perfoming the FTP client's request.
   * @author Eugene Kim-Leighton
   */
  private void determineRepliesAreValid(String reply)
          throws SocketMessageInvalidException, SocketMessageFailedException
  {
    Assert.expect(reply != null);
    try
    {
      // server sends the message indicating it is unknown command
      if(reply.startsWith("?"))
        throw new SocketMessageInvalidException(reply);

      StringTokenizer tokens = new StringTokenizer(reply);
      int code = StringUtil.convertStringToInt(tokens.nextToken());

      // If the three digit code in the server's response is followed by hyphen,
      // it implies that this response is a multiple string message.
      // It then should keep reading the response until it receives the response with the same three digit code
      // as the code that first line this response started with is.
      if((reply.charAt(3)) == '-')
      {
        reply = receiveReply();
        while(reply.startsWith(Integer.toString(code)) == false)
          reply = receiveReply();
      }

      if(code < _SUCESSFUL_CODE_LOWER_RANGE  || code > _SUCESSFUL_CODE_UPPER_RANGE)
      {
        if (code >= _WAIT_CODE_LOWER_RANGE && code <= _WAIT_CODE_UPPER_RANGE)
        {
          // do nothing, like the function comment states, this just indicates
          // that the server is waiting for the client to reply. For instance,
          // when transferring over a file, the server will accept the command
          // and return this code indicating that it is waitng for the file transfer.
        }
        else if (code >= _ERROR_CODE_LOWER_RANGE && code <= _ERROR_CODE_UPPER_RANGE)
        {
          throw new SocketMessageFailedException(reply);
        }
      }
    }
    catch(BadFormatException bfe)
    {
      SocketMessageFailedException ex = new SocketMessageFailedException(bfe.getMessage());
      ex.initCause(bfe);
      throw ex;
    }
    catch(IOException ioe)
    {
      SocketMessageFailedException ex = new SocketMessageFailedException(ioe.getMessage());
      ex.initCause(ioe);
      throw ex;
    }
  }

  /**
   * Once connected to ftp server, the user needs to provide a login name and a password to the server.
   * When the connection is established, the FTP server sends the reply back to the FTP user (client).
   * The user needs to wait for a reply before sending other commands.  A positive reply from the server
   * starts with 220.  If the user recives 150 before 220, the server needs some more time to get ready
   * for the session.  Other replies starts with a number that ranges from 400 to 599 are considered to be
   * an error.
   *
   * @author Eugene Kim-Leighton
   */
  public void login(String loginName, String password) throws SocketConnectionFailedException,
                                                              SocketMessageFailedException,
                                                              SocketMessageInvalidException
  {
    Assert.expect(loginName != null);
    Assert.expect(password != null);

    try
    {
      openControlConnection();
      String command = _USER_NAME + _SPACE_STR + loginName + _END_OF_STRING;
      sendRequestAndReceiveReplies(command);

      command = _PASSWORD + _SPACE_STR + password + _END_OF_STRING;
      sendRequestAndReceiveReplies(command);
    }
    catch (IOException ioe)
    {
      SocketConnectionFailedException ex = new SocketConnectionFailedException(_ipAddress, _FTP_PORT, ioe.getMessage());
      ex.initCause(ioe);
      throw ex;
    }
  }

  /**
   * This sends out the request for logging out, waits for the response, that is "221 goodbye." from the server,
   * and close the connection from the FTP server.
   *
   * @throws SocketMessageFailedException if there is an error for completing logout request in either server or client side.
   * @author Eugene Kim-Leighton
   */
  public void logout() throws SocketMessageFailedException
  {
    try
    {
      String command = _LOGOUT + _END_OF_STRING;
      boolean matchEntireLine = false;
      boolean isHandshake = false;
      _socketClient.disconnectFromServer(command, _END_OF_STRING, matchEntireLine, isHandshake);
    }
    catch(IOException ioe)
    {
      SocketMessageFailedException ex = new SocketMessageFailedException(ioe.getMessage());
      ex.initCause(ioe);
      throw ex;
    }
  }

  /**
   * This sets the transfer mode to binary.
   *
   * @throws SocketMessageInvalidException if the FTP server's response indicates that the FTP client's request is not valid for server's system.
   * @throws SocketMessageFailedException if the FTP server's response indicates that there was an error perfoming the FTP client's request.
   * @author Eugene Kim-Leighton
   */
  public void setBinaryTransferMode() throws SocketMessageFailedException, SocketMessageInvalidException
  {
    String type = "I";
    boolean matchEntireLine = false;
    String command = _REPRESENT_TYPE + _SPACE_STR + type + _END_OF_STRING;
    sendRequestAndReceiveReplies(command);
  }

  /**
   * This sets the transfer mode to ascii.
   *
   * @throws SocketMessageInvalidException if the FTP server's response indicates that the FTP client's request is not valid for server's system.
   * @throws SocketMessageFailedException if the FTP server's response indicates that there was an error perfoming the FTP client's request.
   * @author Eugene Kim-Leighton
   */
  public void setAsciiTransferMode() throws SocketMessageFailedException, SocketMessageInvalidException
  {
    String type = "A";
    String command = _REPRESENT_TYPE + _SPACE_STR + type + _END_OF_STRING;
    sendRequestAndReceiveReplies(command);
  }

  /**
   * This sends the command "PASV" to the server, when the client wants the server to listen on its data
   * connection port before the client sends the file to the server through the port.
   *
   * @author Eugene Kim-Leighton
   */
  public void setServerPassiveMode() throws SocketMessageFailedException, SocketMessageInvalidException
  {
    String command = _PASSIVE + _END_OF_STRING;
    findServerDataPort(command);
  }

  /**
   * When the commmand "PASV" is executed, the server sends the information about the IP address and port
   * number that the server is listening to for the data connection.  The information comes in a format such
   * that "(X,X,X,X,Y,Y)", where Xs reperesent the IP address and Ys represent the port number that server
   * is listeng in.  (ie. (10,20,30,41,199,122)).  Each X and Y is between 0 and 255 which is the ranger for
   * the byte.  Since the client knows the IP address from the control connection, it is the port number
   * that client should find out in order to send the file or data.
   *
   * @throws SocketMessageInvalidException if the FTP server's response indicates that the FTP client's request is not valid for server's system.
   * @throws SocketMessageFailedException if the FTP server's response indicates that there was an error perfoming the FTP client's request.
   * @author Eugene Kim-Leighton
   */
  private void findServerDataPort(String request)
          throws SocketMessageFailedException, SocketMessageInvalidException
  {
    try
    {
      boolean matchEntireLine = false;
      boolean isHandshake = false;
      List<String> list = _socketClient.sendRequestAndReceiveReplies(request, _END_OF_STRING, matchEntireLine, isHandshake);
      Assert.expect(list.size() == 1);
      String reply = list.get(0);

      determineRepliesAreValid(reply);
      StringTokenizer tokens = new StringTokenizer(reply);
      String token = "";
      while(tokens.hasMoreTokens())
      {
        token = tokens.nextToken();
        if(token.startsWith("(") && token.endsWith(")"))
        {
          int length = token.length();
          token = token.substring(1, length - 1);
          break;
        }
      }
      tokens = new StringTokenizer(token);
      for(int i = 0; i < 4; i++)
        tokens.nextToken(",");
      int dataPort = StringUtil.convertStringToInt(tokens.nextToken(","));
      dataPort <<=  8;
      dataPort += StringUtil.convertStringToInt(tokens.nextToken());

      _dataSocketPort = dataPort;
    }
    catch(BadFormatException bfe)
    {
      SocketMessageFailedException ex = new SocketMessageFailedException(bfe.getMessage());
      ex.initCause(bfe);
      throw ex;
    }
    catch(IOException ioe)
    {
      SocketMessageFailedException ex = new SocketMessageFailedException(ioe.getMessage());
      ex.initCause(ioe);
      throw ex;
    }
  }
  /**
   * This sends out the file, srcPathAndFileName to the FTP server to put as destPathAndFileName.
   * It is very important, even after any exception is thrown, to make sure to close the any data connection
   * that is open.
   *
   * @param srcPathAndFileName is the local file and its location that is about to be transferred from.
   * @param destPathAndFileName is the file and its locations in the remote systems, that a FTP server is running, that a file from the client side gets transferred to.
   * @throws socketConnectionFailedException if there is an error opening and closing the data connection for sending file to the FTP server.
   * @throws SocketMessageInvalidException if the FTP server's response indicates that the FTP client's request is not valid for server's system.
   * @throws SocketMessageFailedException if the FTP server's response indicates that there was an error perfoming the FTP client's request.
   * @author Eugene Kim-Leighton
   */
  public void sendFile(String srcPathAndFileName, String destPathAndFileName)
        throws SocketConnectionFailedException, SocketMessageInvalidException, SocketMessageFailedException
  {
    Assert.expect(srcPathAndFileName != null);
    Assert.expect(destPathAndFileName != null);

    try
    {
      // tell the ftp server that you want to transfer a file
      String command = _STORE + _SPACE_STR + destPathAndFileName + _END_OF_STRING;
      _socketClient.sendRequest(command);
      openDataConnection();
      String reply = receiveReply();
      determineRepliesAreValid(reply);

      // now transfer the file
      putFile(srcPathAndFileName, destPathAndFileName);
      closeDataConnection();
      reply = receiveReply();
      determineRepliesAreValid(reply);
    }
    catch(IOException ioe)
    {
      SocketConnectionFailedException ex = new SocketConnectionFailedException(_dataSocketLocalIpAddress.getHostName(), _dataSocketPort, ioe.getMessage());
      ex.initCause(ioe);
      throw ex;
    }
    finally
    {
      closeDataConnection();
    }
  }

  /**
   * This function sends out the request and receives the list of replies from the FTP server,
   * and checks whether or not an error condition is contatined in the reply.
   *
   * @param request is the client's request to the server. It cannot be a null.
   * @throws SocketMessageInvalidException if the FTP server's response indicates that the FTP client's request is not valid for server's system.
   * @throws SocketMessageFailedException if the FTP server's response indicates that there was an error perfoming the FTP client's request.
   *
   * @author Eugene Kim-Leighton
   */
  public void sendRequestAndReceiveReplies(String request)
         throws SocketMessageInvalidException, SocketMessageFailedException
  {
    try
    {
      boolean matchEntireLine = false;
      boolean isHandshake = false;
      List<String> list = _socketClient.sendRequestAndReceiveReplies(request, _END_OF_STRING, matchEntireLine, isHandshake);
      Assert.expect(list.size() == 1);
      String reply = list.get(0);
      determineRepliesAreValid(reply);
    }
    catch(IOException ioe)
    {
      SocketMessageFailedException ex = new SocketMessageFailedException(ioe.getMessage());
      ex.initCause(ioe);
      throw ex;
    }
  }

  /**
   * This function receives the connection replies from the FTP server when the client makes the connection.
   * If the connection is accepted by the FTP server, then it sends out message with 220 code.  If server
   * sends a reply with 220 code, after connection is made, then everything is OK and FTP client can login
   * any time.  If server sends a reply with 150 code, after connection is made, then it means that server
   * needs some more time to be ready for client's login process.  Client should wait for login until it
   * receives 220 from the server.  If server sends a reply other than 220 and 150, then there is an error
   * setting up the session.
   *
   * @throws SocketConnectionFailedException if there is an error while establishing the connection between the client and server
   * @author Eugene Kim-Leighton
   * @author George A. David
   */
  private void receiveConnectionReplies() throws SocketConnectionFailedException
  {
    try
    {
      String reply = receiveReply();
      while(reply.startsWith(String.valueOf(_READY_FOR_LOGIN)) == false)
      {
        if(reply.startsWith(String.valueOf(_WAIT_FOR_LOGIN)))
        {
          reply = receiveReply();
        }
        else
          throw new SocketConnectionFailedException(_dataSocketLocalIpAddress.getHostName(), _dataSocketPort,  reply);
      }
    }
    catch(IOException ioe)
    {
      SocketConnectionFailedException ex = new SocketConnectionFailedException(_dataSocketLocalIpAddress.getHostName(), _dataSocketPort, ioe.getMessage());
      ex.initCause(ioe);
      throw ex;
    }
  }

  /**
   * This is a helper method for the sendFile().  This send file to the ftp server via data connection.
   *
   * @throws IOException if there is an error opening, reading or closing FileReader, or if there is an error writing out to OutputStream.
   * @throws SocketMessageFailedException if there is an error with file to send.
   *
   * @author Eugene Kim-Leighton
   */
  private void putFile(String srcPathAndFileName, String destPathAndFileName)
          throws IOException, SocketMessageFailedException
  {
    Assert.expect(srcPathAndFileName != null);
    Assert.expect(destPathAndFileName != null);

    File file = new File(srcPathAndFileName);

    if (file.exists()==false)
      throw new SocketMessageFailedException("File " + srcPathAndFileName + " does not exist.");
    else if(file.canRead() == false)
      throw new SocketMessageFailedException("Access to the file " + srcPathAndFileName + " denied.");
    else if (file.isDirectory())
      throw new SocketMessageFailedException(srcPathAndFileName + " is a directory.");
    else
    {
      FileReader fileReader = null;
      try
      {
        fileReader = new FileReader(file);

        if(fileReader == null)
          throw new SocketMessageFailedException("Can't read from file " + srcPathAndFileName);
        long fileLength = file.length();
        long fileStreamOffset = 0;
        char fileData[]  = new char [_FILE_DATA_SIZE];

        while (fileStreamOffset != fileLength)
        {
          int read = fileReader.read(fileData);
          if (read <= 0)
            break;
//          _dataSocketOutputStream.write(fileData, 0, read);
          _dataSocket.write(encoder.encode(CharBuffer.wrap(fileData)));
          fileStreamOffset += read;
        }
      }
      catch (FileNotFoundException fnfe)
      {
        fnfe.printStackTrace();
      }
      catch (IOException ioe)
      {
        ioe.printStackTrace();
      }
      finally
      {
        if (fileReader != null)
          fileReader.close();
      }
    }
  }

//****************************************************************************//
// The codes below this are the one that is implemented but not yet tested.   //
//****************************************************************************//
//  /**
//   * This receives the file, srcPathAndFileName that the FTP server sends to the client to put
//   * as a destPathAndFileName in the client side location.
//   *
//   * It is very important, even after any exception is thrown,
//   * to make sure to close the any data connection that is open.
//   *
//   * @param srcPathAndFileName is the file and its location in a remote system, that the FTP server is runnign,
//   *                           that gets transfered from the FTP server side.
//   * @param destPathAndFileName is the file and its locations in the FTP client's systems.
//   * @throws socketConnectionFailedException if there is an error opening and closing the data connection
//   *                                         for sending file to the FTP server.
//   * @throws SocketMessageInvalidException if the FTP server's response indicates that
//   *                                      the FTP client's request is not valid for server's system.
//   * @throws SocketMessageFailedException if the FTP server's response indicates that
//   *                                      there was an error perfoming the FTP client's request.
//   *
//   * @author Eugene Kim-Leighton
//   */
//  public void receiveFile(String srcPathAndFileName, String destPathAndFileName)
//         throws SocketConnectionFailedException, SocketMessageInvalidException, SocketMessageFailedException
//  {
//    Assert.expect(srcPathAndFileName != null);
//    Assert.expect(destPathAndFileName != null);
//
//    try
//    {
//      openDataConnection();
//      String command = _RETRIEVE + _SPACE_STR + srcPathAndFileName + _END_OF_STRING;
//      sendRequestAndReceiveReplies(command);
//      getFile(srcPathAndFileName, destPathAndFileName);
//    }
//    catch(IOException ioe)
//    {
//      SocketConnectionFailedException ex = new SocketConnectionFailedException(ioe.getMessage());
//      ex.initCause(ioe);
//      throw ex;
//    }
//    finally
//    {
//      closeDataConnection();
//    }
//  }
//
//  /**
//   * This method will receive the data from the server through data connection
//   *
//   * @author Eugene Kim-Leighton
//   */
//  private void getFile(String srcPathAndFileName, String destPathAndFileName)
//          throws IOException, SocketMessageFailedException
//  {
//    Assert.expect(srcPathAndFileName != null);
//    Assert.expect(destPathAndFileName != null);
//
//    File file = new File(destPathAndFileName);
//    if (file.exists() && file.canWrite() == false)
//      throw new SocketMessageFailedException("Access to the file " + destPathAndFileName + " is denied");
//    else
//    {
//      FileWriter fileWriter = null;
//      try
//      {
//        fileWriter = new FileWriter(file);
//        if (fileWriter == null)
//          throw new SocketMessageFailedException("can't write to file " + destPathAndFileName);
//
//        while (_dataSocket.read(buffer) != -1)
//        {
//          buffer.flip();
//          decoder.decode(buffer, charBuffer, false);
//          fileWriter.write(charBuffer, 0, read);
//        }
//      }
//      catch (IOException ioe)
//      {
//        // do nothing?
//      }
//      finally
//      {
//        if (fileWriter != null)
//          fileWriter.close();
//      }
//    }
//  }
//  /**
//   *
//   * @author Eugene Kim-Leighton
//   */
//  private List receiveDataList() throws IOException
//  {
//    List list = new ArrayList();
//    String contents = _dataSocketInputStream.readLine();
//    while(contents != null)
//    {
//      list.add(contents);
//      contents = _dataSocketInputStream.readLine();
//    }
//    return list;
//  }
//
//  /**
//   * @author Eugene Kim-Leighton
//   */
//  public String getWorkingDirectory() throws SocketMessageInvalidException, SocketMessageFailedException
//  {
//    //command syntax: PWD  <CRLF>
//
//    String str = "";
//    try
//    {
//      String command = _PRINT_WORKING_DIR + _END_OF_STRING;
//      sendRequestAndReceiveReplies(command);
//      str = getDirectory(_replies);
//    }
//    catch(IOException ioe)
//    {
//      FTPException ex = new FTPException(ioe.getMessage());
//      ex.initCause(ioe);
//      throw ex;
//    }
//    return str;
//  }
//
//  /**
//   * @author Eugene Kim-Leighton
//   */
//  private String getDirectory(List list) throws SocketMessageInvalidException, SocketMessageFailedException
//  {
//    String line = (String)list.get(0);
//    StringTokenizer tokens = new StringTokenizer(line);
//    String retStr = "";
//    while(tokens.hasMoreTokens())
//    {
//      String str = tokens.nextToken();
//      if(str.startsWith("\""))
//      {
//        retStr = str;
//        break;
//      }
//    }
//    if(retStr == "")
//      throw new FTPException("invalid reply for the command \"list\"");
//    return retStr;
//  }
//
//  /**
//   * @author Eugene Kim-Leighton
//   */
//  public List listDirectoryContents(String pathName) throws SocketMessageInvalidException, SocketMessageFailedException
//  {
//    //command syntax: LIST [<SP> <pathname>] <CRLF>
//
//    //path name can be null
//    List replies = null;
//    try
//    {
//      String command;
//      if (pathName == null)
//        command = _LIST + _END_OF_STRING;
//      else
//        command = _LIST + _SPACE_STR + pathName + _END_OF_STRING;
//
//      sendRequestAndReceiveReplies(command);
//
//      openDataConnection();
//      closeDataConnection();
//    }
//    catch(IOException ioe)
//    {
//      FTPException ex = new FTPException(ioe.getMessage());
//      ex.initCause(ioe);
//      throw ex;
//    }
//    return replies;
//  }
//
//  /**
//   * @author Eugene Kim-Leighton
//   */
//  public void changeDirectory(String pathName) throws SocketMessageInvalidException, SocketMessageFailedException
//  {
//    //command syntax: CWD  <SP> <pathname> <CRLF>
//
//    Assert.expect(pathName != null);
//
//    try
//    {
//      String command = _CHANGE_DIRECTORY + _SPACE_STR + pathName + _END_OF_STRING;
//      sendRequestAndReceiveReplies(command);
//    }
//    catch(IOException ioe)
//    {
//      FTPException ex = new FTPException(ioe.getMessage());
//      ex.initCause(ioe);
//      throw ex;
//    }
//  }
//
//  /**
//   * @author Eugene Kim-Leighton
//   */
//  public void makeDirectory(String pathName) throws SocketMessageInvalidException, SocketMessageFailedException
//  {
//    //command syntax: MKD  <SP> <pathname> <CRLF>
//
//    Assert.expect(pathName != null);
//
//    try
//    {
//      String command = _MAKE_DIR + _SPACE_STR + pathName + _END_OF_STRING;
//      sendRequestAndReceiveReplies(command);
//    }
//    catch(IOException ioe)
//    {
//      FTPException ex = new FTPException(ioe.getMessage());
//      ex.initCause(ioe);
//      throw ex;
//    }
//  }
//
//  /**
//   * @author Eugene Kim-Leighton
//   */
//  public void removeDirectory(String pathName) throws SocketMessageInvalidException, SocketMessageFailedException
//  {
//    // command syntax: RMD  <SP> <pathname> <CRLF>
//
//    Assert.expect(pathName != null);
//
//    try
//    {
//      String command = _REMOVE_DIR + _SPACE_STR + pathName + _END_OF_STRING;
//      sendRequestAndReceiveReplies(command);
//    }
//    catch(IOException ioe)
//    {
//      FTPException ex = new FTPException(ioe.getMessage());
//      ex.initCause(ioe);
//      throw ex;
//    }
//  }
//
//  /**
//   * @author Eugene Kim-Leighton
//   */
//  public void deleteFile(String pathAndFileName) throws SocketMessageInvalidException, SocketMessageFailedException
//  {
//    // command syntax: DELE <SP> <pathname> <CRLF>
//
//    Assert.expect(pathAndFileName != null);
//
//    try
//    {
//      String command = _DELETE + _SPACE_STR + pathAndFileName + _END_OF_STRING;
//      sendRequestAndReceiveReplies(command);
//    }
//    catch(IOException ioe)
//    {
//      FTPException ex = new FTPException(ioe.getMessage());
//      ex.initCause(ioe);
//      throw ex;
//    }
//  }
//
//  /**
//   * @author Eugene Kim-Leighton
//   */
//  public void renameFile(String oldPathAndFileName, String newPathAndFileName) throws SocketMessageInvalidException, SocketMessageFailedException
//  {
//    // command syntax:    RNFR <SP> <pathname> <CRLF>
//    //                    RNTO <SP> <pathname> <CRLF>
//
//    Assert.expect(oldPathAndFileName != null);
//    Assert.expect(newPathAndFileName != null);
//
//    try
//    {
//      String command = _RENAME_FROM + _SPACE_STR + oldPathAndFileName + _END_OF_STRING;
//      sendRequestAndReceiveReplies(command);
//
//      command = _RENAME_TO + _SPACE_STR + newPathAndFileName + _END_OF_STRING;
//      sendRequestAndReceiveReplies(command);
//    }
//    catch(IOException ioe)
//    {
//      FTPException ex = new FTPException(ioe.getMessage());
//      ex.initCause(ioe);
//      throw ex;
//    }
//  }
}
