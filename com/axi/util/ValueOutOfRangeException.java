package com.axi.util;

import java.io.*;

/**
 * This exception is used to indicate that a value was out of the allowable range.
 * @author Bill Darbie
 */
public class ValueOutOfRangeException extends Exception
{
  private double _actualValue;
  private double _minValue;
  private double _maxValue;

  /**
   * Construct an exception that occurred because the actualValue was not within the range
   * specified by the minValue and maxValue.
   * @author Bill Darbie
   */
  public ValueOutOfRangeException(double actualValue, double minValue, double maxValue)
  {
    super("ERROR: The value " + actualValue + " was not within the allowable range of " + minValue + " to " + maxValue);
    _actualValue = actualValue;
    _minValue = minValue;
    _maxValue = maxValue;
  }

  /**
   * @author Bill Darbie
   */
  public double getActualValue()
  {
    return _actualValue;
  }

  /**
   * @author Bill Darbie
   */
  public double getMinValue()
  {
    return _minValue;
  }

  /**
   * @author Bill Darbie
   */
  public double getMaxValue()
  {
    return _maxValue;
  }
}
