:: -----------------------------------------------------------------------------
:: (C) Copyright 2010 ViTrox Technologies. All rights reserved
:: -----------------------------------------------------------------------------
:: @author Wei Chin, Chong
:: compile JNI and package it into nativeImageEnhancer.dll
::

@echo off

SET MODE=Release
IF "%1"=="Debug" set MODE=Debug
IF "%1"=="Release" set MODE=Release

set OLDPATH=%PATH%
set VSTUDIO_DIR=C:/apps/vStudio2008
set JDK_PATH=C:/apps/java/jdk1.7.0
set WINSDK_PATH=../../../../../thirdParty/microsoft/WINSDK/v7.1
set IPPROOT=C:/apps/Intel/IPP/5.1/em64t
set OPENCVROOT=../../../../../thirdParty/OpenCV
set VCVARS_PATH=%VSTUDIO_DIR%/VC/bin/amd64
set LIB_CLASSPATH=../../../../jre/1.7.0/lib/ext/comm.jar;../../../../jre/1.7.0/lib/ext/j3daudio.jar;../../../../jre/1.7.0/lib/ext/j3dcore.jar;../../../../jre/1.7.0/lib/ext/j3dutils.jar;../../../../jre/1.7.0/lib/ext/vecmath.jar;../../../../jre/1.7.0/lib/ext/Jama-1.0.2.jar;../../../../jre/1.7.0/lib/ext/jcommon-1.0.0.jar;../../../../jre/1.7.0/lib/ext/jfreechart-1.0.1.jar
set INCLUDE_VC=-I%VSTUDIO_DIR%/VC/include -I"%WINSDK_PATH%/Include" -I%IPPROOT%/include -I%OPENCVROOT%/include -I%JDK_PATH%/include -I%JDK_PATH%/include/win32 -I.. -I../../../.. -I../../../../.. -I../../../../../cpp -I../../../../../cpp/util/src -I../../../../../thirdParty/zlib -I../../../../../thirdParty/libpng/lpng140 -I../../../../../thirdParty/VitroxImageEnhancer/Include
IF "%MODE%"=="Release" set VC_FLAG=-D_WINDOWS -DWIN32 -DWIN64 -D_WIN64 -D_USRDLL -D_WINDLL -D_CRT_SECURE_NO_DEPRECATE -D_AFXDLL -D_MBCS -DBOOST_THREAD_USE_DLL -EHsc -O2 -MD
IF "%MODE%"=="Debug" set VC_FLAG=-D_WINDOWS -DWIN32 -DWIN64 -D_WIN64 -D_USRDLL -D_WINDLL -D_CRT_SECURE_NO_DEPRECATE -D_AFXDLL -D_MBCS -DBOOST_THREAD_USE_DLL -D_Debug  -EHsc -O2 -MD
set DEBUG_FLAG=-DEBUG
IF "%MODE%"=="Release" set DEBUG_FLAG= 
set OBJ_PATH=../../../../buildFiles/jniMscpp/jniLoadHandler.obj  ../../../../buildFiles/jniMscpp/jniImageUtil.obj ../../../../buildFiles/jniMscpp/com_axi_util_image_ImageEnhancer.obj
set PATH=%PATH%;%JDK_PATH%\bin;%VCVARS_PATH%
set TMP=%TEMP%

echo "#### generating JNI header file from com.axi.util.image.ImageEnhancer ####"
@echo on
javah  -classpath ../../../..;../../../../classes;%LIB_CLASSPATH% com.axi.util.image.ImageEnhancer
@echo off

echo "#### building nativeImageEnhancer.dll ####"

@echo on

cl  -c %VC_FLAG% %INCLUDE_VC% com_axi_util_image_ImageEnhancer.cpp -Fo../../../../buildFiles/jniMscpp/com_axi_util_image_ImageEnhancer.obj

%VCVARS_PATH%/link %OBJ_PATH% -ignore:4089 -out:../../../../jniRelease/bin/nativeImageEnhancer.dll -LIBPATH:../../../../../cpp/release/bin -LIBPATH:../../../../../cpp/bin/src -LIBPATH:%VSTUDIO_DIR%/VC/lib/amd64 -LIBPATH:"%WINSDK_PATH%/Lib/x64" -DLL %DEBUG_FLAG% ../../../../../cpp/release/bin/AgtAxiIPP.lib ../../../../../cpp/release/bin/axiUtil.lib ../../../../jniRelease/bin/nativeAxiUtil.lib ../../../../../cpp/bin/src/png14.lib ../../../../../cpp/bin/src/zlib1.lib ../../../../../cpp/release/bin/opencv_core231.lib ../../../../../cpp/release/bin/opencv_highgui231.lib ../../../../../cpp/release/bin/opencv_imgproc231.lib ../../../../../cpp/release/bin/VfImageEnhancer_x64.lib
"%WINSDK_PATH%\Bin\x64\mt" -manifest ../../../../jniRelease/bin/nativeImageEnhancer.dll.manifest -outputresource:../../../../jniRelease/bin/nativeImageEnhancer.dll;2

@echo off
:: clean up environment space
set PATH= %OLDPATH%
set OLDPATH=
set VCVARS_PATH=
set JDK_PATH=
