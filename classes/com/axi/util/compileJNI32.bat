:: -----------------------------------------------------------------------------
:: (C) Copyright 2010 ViTrox Technologies. All rights reserved
:: -----------------------------------------------------------------------------
:: @author Wei Chin, Chong
:: compile JNI and package it into nativeAxiUtil32.dll
::

@echo off

SET MODE=Debug
IF "%1"=="Debug" set MODE=Debug
IF "%1"=="Release" set MODE=Release

set OLDPATH=%PATH%
set VSTUDIO_DIR=C:/apps/vStudioNet2003
set VSTUDIO2008_DIR=C:/apps/vStudio2008
set JDK_PATH=C:/apps/java/jdk1.7.0
set WINSDK_PATH=../../../../thirdParty/microsoft/WINSDK/v7.1
set IPPROOT=C:/apps/Intel/IPP/5.1/ia32
set BOOSTROOT=../../../../thirdParty/boost/boost_1_34_0
set VCVARS_PATH=%VSTUDIO_DIR%/VC7/bin
set LIB_CLASSPATH=../../../jre/1.7.0/lib/ext/comm.jar;../../../jre/1.7.0/lib/ext/j3daudio.jar;../../../jre/1.7.0/lib/ext/j3dcore.jar;../../../java/jre/1.7.0/lib/ext/j3dutils.jar;../../../jre/1.7.0/lib/ext/vecmath.jar;../../../jre/1.7.0/lib/ext/Jama-1.0.2.jar;../../../jre/1.7.0/lib/ext/jcommon-1.0.0.jar;../../../jre/1.7.0/lib/ext/jfreechart-1.0.1.jar
set INCLUDE_VC=-I%VSTUDIO_DIR%/VC7/include -I"%WINSDK_PATH%/include" -I"%VSTUDIO2008_DIR%/VC/include" -I%JDK_PATH%/include -I%JDK_PATH%/include/win32 -I../../../java -I../../../.. -I../../../../cpp -I%BOOSTROOT%
IF "%MODE%"=="Release" set VC_FLAG=-D_WINDOWS -DWIN32 -D_USRDLL -D_WINDLL -D_CRT_SECURE_NO_DEPRECATE -D_AFXDLL -D_MBCS -DBOOST_THREAD_USE_DLL -EHsc -O2 -MD
IF "%MODE%"=="Debug" set VC_FLAG=-D_WINDOWS -DWIN32 -D_USRDLL -D_WINDLL -D_CRT_SECURE_NO_DEPRECATE -D_AFXDLL -D_MBCS -DBOOST_THREAD_USE_DLL -D_Debug -EHsc -O2 -MD
set DEBUG_FLAG=-DEBUG
IF "%MODE%"=="Release" set DEBUG_FLAG= 
set OBJ_PATH=../../../buildFiles/jniMscpp/jniLoadHandler32.obj ../../../buildFiles/jniMscpp/com_axi_util_ICMPPacketUtil32.obj ../../../buildFiles/jniMscpp/com_axi_util_FileUtil32.obj ../../../buildFiles/jniMscpp/com_axi_util_NativeAssertFalse32.obj ../../../buildFiles/jniMscpp/com_axi_util_NetworkUtil32.obj ../../../buildFiles/jniMscpp/com_axi_util_UsbUtil32.obj ../../../buildFiles/jniMscpp/com_axi_util_ThinPlateSpline32.obj ../../../buildFiles/jniMscpp/jniUtil32.obj
set PATH=%PATH%;%JDK_PATH%\bin;%VCVARS_PATH%
set TMP=%TEMP%

echo "#### generating JNI header file from com.axi.util.ICMPPacketUtil ####"
@echo on
javah  -classpath ../../..;../../../classes;%LIB_CLASSPATH% com.axi.util.ICMPPacketUtil

@echo off
echo "#### generating JNI header file from com.axi.util.FileUtil ####"
@echo on
javah  -classpath ../../..;../../../classes;%LIB_CLASSPATH% com.axi.util.FileUtil
@echo off

::echo "#### generating JNI header file from com.axi.util.LicenseManagerUtil ####"
::javah  -classpath ../../..;../../../classes;%LIB_CLASSPATH% com.axi.util.LicenseManagerUtil

echo "#### generating JNI header file from com.axi.util.NativeAssertFalse ####"
@echo on
javah  -classpath ../../..;../../../classes;%LIB_CLASSPATH% com.axi.util.NativeAssertFalse
echo "#### generating JNI header file from com.axi.util.NetworkUtil ####"

@echo on
javah  -classpath ../../..;../../../classes;%LIB_CLASSPATH% com.axi.util.NetworkUtil
@echo off
echo "#### generating JNI header file from com.axi.util.UsbUtil ####"

@echo on
javah  -classpath ../../..;../../../classes;%LIB_CLASSPATH% com.axi.util.UsbUtil
@echo off

echo "#### generating JNI header file from com.axi.util.ThinPlateSpline ####"
@echo on
javah  -classpath ../../..;../../../classes;%LIB_CLASSPATH% com.axi.util.ThinPlateSpline
@echo off

echo "#### building nativeAxiUtil32.dll ####"
@echo on
%VCVARS_PATH%/cl  -c -DNATIVE_AXI_UTIL_DLL %VC_FLAG% %INCLUDE_VC% jniLoadHandler.cpp -Fo../../../buildFiles/jniMscpp/jniLoadHandler32.obj

%VCVARS_PATH%/cl  -c -DNATIVE_AXI_UTIL_DLL %VC_FLAG% %INCLUDE_VC% com_axi_util_ICMPPacketUtil.cpp -Fo../../../buildFiles/jniMscpp/com_axi_util_ICMPPacketUtil32.obj

%VCVARS_PATH%/cl  -c -DNATIVE_AXI_UTIL_DLL %VC_FLAG% %INCLUDE_VC% com_axi_util_FileUtil.cpp -Fo../../../buildFiles/jniMscpp/com_axi_util_FileUtil32.obj

%VCVARS_PATH%/cl  -c -DNATIVE_AXI_UTIL_DLL %VC_FLAG% %INCLUDE_VC% com_axi_util_NativeAssertFalse.cpp -Fo../../../buildFiles/jniMscpp/com_axi_util_NativeAssertFalse32.obj

%VCVARS_PATH%/cl  -c -DNATIVE_AXI_UTIL_DLL %VC_FLAG% %INCLUDE_VC% com_axi_util_NetworkUtil.cpp -Fo../../../buildFiles/jniMscpp/com_axi_util_NetworkUtil32.obj

%VCVARS_PATH%/cl  -c -DNATIVE_AXI_UTIL_DLL %VC_FLAG% %INCLUDE_VC% com_axi_util_UsbUtil.cpp -Fo../../../buildFiles/jniMscpp/com_axi_util_UsbUtil32.obj

%VCVARS_PATH%/cl  -c -DNATIVE_AXI_UTIL_DLL %VC_FLAG% %INCLUDE_VC% com_axi_util_ThinPlateSpline.cpp -Fo../../../buildFiles/jniMscpp/com_axi_util_ThinPlateSpline32.obj

%VCVARS_PATH%/cl  -c -DNATIVE_AXI_UTIL_DLL %VC_FLAG% %INCLUDE_VC% jniUtil.cpp -Fo../../../buildFiles/jniMscpp/jniUtil32.obj

%VCVARS_PATH%/link %OBJ_PATH% -ignore:4089 -out:../../../jniRelease/bin/nativeAxiUtil32.dll -LIBPATH:../../../../cpp/release/bin -LIBPATH:%VSTUDIO_DIR%/VC7/lib -LIBPATH:"%WINSDK_PATH%/Lib" -DLL %DEBUG_FLAG% ../../../../cpp/release/bin/axiUtil32.lib "%WINSDK_PATH%/Lib/netapi32.lib"

"%WINSDK_PATH%\Bin\mt" -manifest ../../../jniRelease/bin/nativeAxiUtil32.dll.manifest -outputresource:../../../jniRelease/bin/nativeAxiUtil32.dll;2

@echo off
:: clean up environment space
set PATH= %OLDPATH%
set OLDPATH=
set VCVARS_PATH=
set JDK_PATH=
