/**
 * Title:        ndfTreeSetup Class<p>
 * Description:  This class defines a few methods which make setting up a tree easier<p>
 * Copyright:    Copyright (c) <p>
 * Company:      Agilent Technologies<p>
 * @Michael Martinez-Schiferl
 * @version 1.0
 */
package ComponentLibrary;

import javax.swing.tree.DefaultMutableTreeNode;
import java.io.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class ndfTreeSetup extends TreeSetup{

  private int numberOfDataSetsInFile = 13;  // 13 comes from the input file data2.txt, its a Magic Constant
  private Threshold[] threshy = new Threshold[numberOfDataSetsInFile];
  private int count;

  public ndfTreeSetup() {
  }

  public void myNDFTreeInit(DefaultMutableTreeNode top) {
    //start of initial data input

  try {
    //categories Lead Types
    //this creates the basic nodes with which we will sort the rest
    DefaultMutableTreeNode BGA        = new DefaultMutableTreeNode("BGA");
    DefaultMutableTreeNode CAP        = new DefaultMutableTreeNode("CAP");
    DefaultMutableTreeNode FPGULLWING = new DefaultMutableTreeNode("FPGULLWING");
    DefaultMutableTreeNode GULLWING   = new DefaultMutableTreeNode("GULLWING");
    DefaultMutableTreeNode JLEAD      = new DefaultMutableTreeNode("JLEAD");
    DefaultMutableTreeNode MELFRES    = new DefaultMutableTreeNode("MELFRES");
    DefaultMutableTreeNode RES        = new DefaultMutableTreeNode("RES");
    DefaultMutableTreeNode component  = null;

    //adds basic nodes as children to top
    top.add(BGA);
    top.add(CAP);
    top.add(FPGULLWING);
    top.add(GULLWING);
    top.add(JLEAD);
    top.add(MELFRES);
    top.add(RES);

    //readDataIn
    //this the System.getProperty() didn't return the correct directory because JBuilder goes about compiling
    //and running the programs in a different way
//    String prefix = "file:" + System.getProperty("user.dir") + System.getProperty("file.separator");
//    BufferedReader in = new BufferedReader(new FileReader(prefix + "data.txt"));
    BufferedReader in = new BufferedReader(new FileReader("C:\\CL\\src\\ComponentLibrary\\data\\data.txt"));

    boolean eof = false;
    count = 0;

    while (!eof) {

      String part_name    = in.readLine();
      String comp_name    = in.readLine();
      String pan_name     = in.readLine();
      String board_name   = in.readLine();
      String foot_ID      = in.readLine();
      String pack_name    = in.readLine();
      String joint_t      = in.readLine();
      String num_pins     = in.readLine();
      String sub_ID       = in.readLine();
      String _fov         = in.readLine();
      String _pitch       = in.readLine();
      String alg_slice    = in.readLine();
      String _units       = in.readLine();
      String _description = in.readLine();
      String _alg         = in.readLine();
      String _thresh      = in.readLine();
      String thresh_val   = in.readLine();



      if (comp_name == null)
        eof = true;
      else {

      //calls the constuctor for a new threshold variable
      threshy[count] = new Threshold(part_name, comp_name, pan_name, board_name, foot_ID, pack_name,
                                                        joint_t, num_pins, sub_ID, _fov, _pitch, alg_slice,
                                                          _units, _description, _alg, _thresh, thresh_val);

        // tree organization
        // places component under the correct joint type
        switch (threshy[count].getCase(joint_t)) {
            case 1:         BGA.add(threshy[count].getNode());  break;
            case 2:         CAP.add(threshy[count].getNode());  break;
            case 3:  FPGULLWING.add(threshy[count].getNode());  break;
            case 4:    GULLWING.add(threshy[count].getNode());  break;
            case 5:       JLEAD.add(threshy[count].getNode());  break;
            case 6:     MELFRES.add(threshy[count].getNode());  break;
            case 7:         RES.add(threshy[count].getNode());  break;
        }  // end switch
      count++;
      }  // end else
    }  // end while
      in.close();

      //These two lines write out a new library file
//      TreeDataManagement _dataManager = new TreeDataManagement();
//      _dataManager.writeData(threshy);
    }
  catch (Exception e)
    {
       e.printStackTrace();
    }
  } // end myTreeInit()

  public Threshold[] getArray() {
  return threshy;
  }
}