/**
 * Title:        PadLayout Class<p>
 * Description:  for CAD preview<p>
 * Copyright:    Copyright (c) <p>
 * Company:      Agilent Technolgies<p>
 * @Michael Martinez-Schiferl
 * @version 1.0
 */
/*
 * Note: This class and its functions are not currently used in the first demo of this program
 */
package ComponentLibrary;

public class PadLayout {

  private boolean top;
  private char shape;
  private float x;
  private float y;
  private float dx;
  private float dy;
  private float xhole;
  private float yhole;

  public PadLayout(/* parameter list */) {
  // read data into variables
  }

/*  public PadLayout () {
  }
*/
  public void generateCADPreview () {
  }
}
