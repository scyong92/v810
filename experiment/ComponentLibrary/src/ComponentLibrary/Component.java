/**
 * Title:        Component Class<p>
 * Description:  class that stores String variables identifying a component<p>
 * Copyright:    Copyright (c) <p>
 * Company:      Agilent Technologies<p>
 * @Michael Martinez-Schiferl
 * @version 1.0
 */
package ComponentLibrary;

import javax.swing.tree.DefaultTreeModel;
import java.io.Serializable;

public class Component implements Serializable{

  private String partName;
  private String componentName;
  private String panelName;
  private String boardName;
  private String footprintID;
  private String packageName;
  private String jointType;
  private String numberOfPins;

  public Component() {
  }

  public void initializeComponent(String part_name, String comp_name, String pan_name, String board_name, String foot_ID, String pack_name,  String joint_t, String num_pins) {
  // make the varible assignments
  partName = part_name;
  componentName = comp_name;
  panelName = pan_name;
  boardName = board_name;
  footprintID = foot_ID;
  packageName = pack_name;
  jointType = joint_t;
  numberOfPins = num_pins;
  }

  public String getComponentName() {
  return this.componentName;
  }

  public String getPanelName() {
  return this.panelName;
  }

  public String getBoardName() {
  return this.boardName;
  }

  public String getFootprintID() {
  return this.footprintID;
  }

  public String getPackageName() {
  return this.packageName;
  }

  public String getjointType() {
  return this.jointType;
  }

  public String getNumberOfPins() {
  return this.numberOfPins;
  }
}

