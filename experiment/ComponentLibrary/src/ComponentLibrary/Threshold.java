/**
 * Title:        Threshold Class<p>
 * Description:  extends Subtype Class, this is the last of the three classes<p>
 * Copyright:    Copyright (c) <p>
 * Company:      Agilent Technologies<p>
 * @Michael Martinez-Schiferl
 * @version 1.0
 */
package ComponentLibrary;

import javax.swing.tree.TreeNode;
import javax.swing.tree.DefaultMutableTreeNode;
import java.text.Collator;

public class Threshold extends Subtype{

  private String algorithm;
  private String threshold;
  private String thresholdValue;
  private DefaultMutableTreeNode _node = null;

  public Threshold(String part_name, String comp_name, String pan_name, String board_name, String foot_ID, String pack_name,  String joint_t, String num_pins, String sub_ID, String _fov, String _pitch, String alg_slice, String _units, String _description, String _alg, String _thresh, String thresh_val) {
  // take data passed in from text file and store it in the variable
  initializeComponent(part_name, comp_name, pan_name, board_name, foot_ID, pack_name, joint_t, num_pins); //initializes the Component variables
  initializeSubtype(sub_ID, _fov, _pitch, alg_slice, _units, _description); //initializes the subtype variables
  algorithm = _alg;
  threshold = _thresh;
  thresholdValue = thresh_val;
  _node = new DefaultMutableTreeNode("             " + part_name +
                                       "                         " + comp_name);
  }

  public Threshold() {
  }

  /*  getCase(String joint_t)
   *  Note : This function is used to find the case and return the number that corresponds
   *         with that case.  This could be replaced with an enum type emulation.
   *  Precondition : the function is passed a valid String argument that is a valid Joint Type
   *  Postcondition : the corresponding number for the joint type is returned
   */
  public int getCase(String joint_t) {
  try {
  Collator myCollator = Collator.getInstance();
    if (myCollator.compare( joint_t, "BGA") == 0)
      return 1;
      else if (myCollator.compare( joint_t, "CAP") == 0)
        return 2;
        else if (myCollator.compare( joint_t, "FPGULLWING") == 0)
          return 3;
          else if (myCollator.compare( joint_t, "GULLWING") == 0)
            return 4;
            else if (myCollator.compare( joint_t, "JLEAD") == 0)
              return 5;
              else if (myCollator.compare( joint_t, "MELFRES") == 0)
                return 6;
                else return 7;
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  return 0;
  }

  public void readDataIn() {
  }

  public String getAlgorithm() {
  return this.algorithm;
  }

  public String getThreshold() {
  return this.threshold;
  }

  public DefaultMutableTreeNode getNode() {
  return this._node;
  }

  public String getThresholdValue() {
  return this.thresholdValue;
  }
}
