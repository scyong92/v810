/**
 * Title:        myCenterInfoTableModel Class<p>
 * Description:  This is the table model for the center table on the left side of the info panel<p>
 * Copyright:    Copyright (c) <p>
 * Company:      Agilent Technologies<p>
 * @Michael Martinez-Schiferl
 * @version 1.0
 */
package ComponentLibrary;

import javax.swing.table.AbstractTableModel;

public class myCenterInfoTableModel extends AbstractTableModel {

   private int i = 0;
   private String[] columnNamesCenter = {"Footprint ID", "Pitch", "# of Pins"};
   private Object[][] dataCenter = {
                   {"", "", ""},          // initializes the table as blank
                   };

    public void setCenterData(Threshold[] thresh) {
    if (i!=0){
      dataCenter[0][0] = thresh[i].getFootprintID();  // it wouldn't work unless I changed the array one at a time
      dataCenter[0][1] = thresh[i].getPitch();
      dataCenter[0][2] = thresh[i].getNumberOfPins();
      if(i > 11) i = 0; // the data in array[0] will be skipped
      }
    i++;
    }


    public myCenterInfoTableModel() {
    }


    public int getColumnCount() {
         return columnNamesCenter.length;
    }

    public int getRowCount() {
         return dataCenter.length;
    }

    public String getColumnName(int col) {
         return columnNamesCenter[col];
    }

    public Object getValueAt(int row, int col) {
        return dataCenter[row][col];
    }

    public boolean isCellEditable(int row, int col) {
    // there are only three columns so this makes all of the cells uneditable
    if (col < 3) {
        return false;
    } else {
        return true;
      }
  }
}