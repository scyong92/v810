/**
 * Title:        MainFrame_AboutBox<p>
 * Description:  about box for Component Library auto-generated by Jbuilder 3.5<p>
 * Copyright:    Copyright (c) <p>
 * Company:      Agilent Technologies<p>
 * @Michael Martinez-Schiferl
 * @version 1.0
 */
//Note:  The greater part of this file was auto-generated by JBuilder 3.5's
//       Design utility.  I made the class variables private.
package ComponentLibrary;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import com.borland.jbcl.layout.*;

public class MainFrame_AboutBox extends JDialog implements ActionListener {

  private JPanel panel1 = new JPanel();
  private JPanel panel2 = new JPanel();
  private JPanel insetsPanel1 = new JPanel();
  private JPanel insetsPanel2 = new JPanel();
  private JPanel insetsPanel3 = new JPanel();
  private JButton button1 = new JButton();
  private JLabel imageControl1 = new JLabel();
  private ImageIcon imageIcon;
  private JLabel label1 = new JLabel();
  private JLabel label2 = new JLabel();
  private JLabel label3 = new JLabel();
  private JLabel label4 = new JLabel();
  private FlowLayout flowLayout1 = new FlowLayout();
  private FlowLayout flowLayout2 = new FlowLayout();
  private String product = "";
  private String version = "1.0";
  private String copyright = "Copyright (c) ";
  private String comments = "";
  private JLabel jLabel1 = new JLabel();
  private JLabel jLabel2 = new JLabel();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private BorderLayout borderLayout1 = new BorderLayout();
  private BorderLayout borderLayout2 = new BorderLayout();
  private BorderLayout borderLayout3 = new BorderLayout();

  public MainFrame_AboutBox(Frame parent) {
    super(parent);
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
    //imageControl1.setIcon(imageIcon);
    pack();
  }

  private void jbInit() throws Exception  {
    //imageIcon = new ImageIcon(getClass().getResource("[Your Image]"));
    this.setTitle("About");
    setResizable(false);
    this.getContentPane().setLayout(borderLayout3);
    panel1.setLayout(borderLayout2);
    panel2.setLayout(borderLayout1);
    insetsPanel1.setLayout(flowLayout1);
    insetsPanel2.setLayout(flowLayout1);
    insetsPanel2.setBorder(new EmptyBorder(10, 10, 10, 10));
    label1.setText("Agilent Technologies");
    label2.setText("Component Library Prototype 1.0");
    label3.setText(copyright);
    label4.setText("Michael Martinez-Schiferl");
    insetsPanel3.setLayout(gridBagLayout1);
    insetsPanel3.setBorder(new EmptyBorder(10, 60, 10, 10));
    button1.setText("Ok");
    button1.addActionListener(this);
    jLabel1.setText("mmartine@mines.edu");
    insetsPanel2.add(imageControl1, null);
    panel2.add(insetsPanel2, BorderLayout.WEST);
    this.getContentPane().add(panel1, BorderLayout.CENTER);
    insetsPanel3.add(label1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 182, 2));
    insetsPanel3.add(label2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 114, 2));
    insetsPanel3.add(label3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 225, 2));
    insetsPanel3.add(label4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 156, 2));
    insetsPanel3.add(jLabel1, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 62), 112, 6));
    insetsPanel3.add(jLabel2, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 62), 21, 6));
    panel1.add(insetsPanel1, BorderLayout.SOUTH);
    insetsPanel1.add(button1, null);
    panel2.add(insetsPanel3, BorderLayout.CENTER);
    panel1.add(panel2, BorderLayout.CENTER);
  }

  protected void processWindowEvent(WindowEvent e) {
    if (e.getID() == WindowEvent.WINDOW_CLOSING) {
      cancel();
    }
    super.processWindowEvent(e);
  }

  void cancel() {
    dispose();
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == button1) {
      cancel();
    }
  }
}