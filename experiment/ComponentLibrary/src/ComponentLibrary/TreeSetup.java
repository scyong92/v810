/**
 * Title:        TreeSetup Class<p>
 * Description:  foundation for the ndfTreeSetup and libraryTreeSetup Classes<p>
 * Copyright:    Copyright (c) <p>
 * Company:      Agilent Technologies<p>
 * @Michael Martinez-Schiferl
 * @version 1.0
 */
package ComponentLibrary;

import javax.swing.tree.DefaultMutableTreeNode;
import java.io.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class TreeSetup {

  private DefaultMutableTreeNode top = new DefaultMutableTreeNode("Joint Type");
  private DefaultTreeModel _treeModel = new DefaultTreeModel(top);


  public TreeSetup() {
  }

  public DefaultTreeModel getTreeModel() {
  return this._treeModel;
  }

  public DefaultMutableTreeNode getTreeNode() {
  return this.top;
  }
}