/**
 * Title:        myThreshInfoTableModel Class<p>
 * Description:  This is the table model for the center table on the left side of the info panel<p>
 * Copyright:    Copyright (c) <p>
 * Company:      Agilent Technologies<p>
 * @Michael Martinez-Schiferl
 * @version 1.0
 */
package ComponentLibrary;

import javax.swing.table.AbstractTableModel;

public class myThreshInfoTableModel extends AbstractTableModel {

   private int i = 0;
   private String[] columnNamesThresh = {"Algorithm", "Threshold", "Value"};
   private Object[][] dataThresh = {
                   {"", "", ""},          // initializes the table as blank
                   {"", "", ""},
                   {"", "", ""},
                   {"", "", ""},
                   {"", "", ""},
                   {"", "", ""},
                   {"", "", ""},
                   {"", "", ""},
                   };

    public void setThreshData(Threshold[] thresh) {
    if (i!=0){
      dataThresh[0][0] = thresh[i].getAlgorithm();      // it wouldn't work unless I changed the array one at a time
      dataThresh[0][1] = thresh[i].getThreshold();
      dataThresh[0][2] = thresh[i].getThresholdValue();
      dataThresh[1][0] = thresh[0].getAlgorithm();
      dataThresh[1][1] = thresh[0].getThreshold();
      dataThresh[1][2] = thresh[0].getThresholdValue();
      dataThresh[2][0] = thresh[1].getAlgorithm();
      dataThresh[2][1] = thresh[1].getThreshold();
      dataThresh[2][2] = thresh[1].getThresholdValue();
      dataThresh[3][0] = thresh[2].getAlgorithm();
      dataThresh[3][1] = thresh[2].getThreshold();
      dataThresh[3][2] = thresh[2].getThresholdValue();
      dataThresh[4][0] = thresh[3].getAlgorithm();
      dataThresh[4][1] = thresh[3].getThreshold();
      dataThresh[4][2] = thresh[3].getThresholdValue();
      dataThresh[5][0] = thresh[4].getAlgorithm();
      dataThresh[5][1] = thresh[4].getThreshold();
      dataThresh[5][2] = thresh[4].getThresholdValue();
      dataThresh[6][0] = thresh[5].getAlgorithm();
      dataThresh[6][1] = thresh[5].getThreshold();
      dataThresh[6][2] = thresh[5].getThresholdValue();
      dataThresh[7][0] = thresh[6].getAlgorithm();
      dataThresh[7][1] = thresh[6].getThreshold();
      dataThresh[7][2] = thresh[6].getThresholdValue();

      if(i > 11) i = 0;   // the data in array[0] will be skipped
      }
    i++;
    }

    public myThreshInfoTableModel() {
    }


    public int getColumnCount() {
         return columnNamesThresh.length;
    }

    public int getRowCount() {
         return dataThresh.length;
    }

    public String getColumnName(int col) {
         return columnNamesThresh[col];
    }

    public Object getValueAt(int row, int col) {
        return dataThresh[row][col];
    }

    public boolean isCellEditable(int row, int col) {
    // there are only three columns so this makes all of the cells uneditable
    if (col < 3) {
        return false;
    } else {
        return true;
      }
  }
}