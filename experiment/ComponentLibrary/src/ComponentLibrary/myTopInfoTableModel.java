/**
 * Title:        myTopInfoTableModel Class<p>
 * Description:  This is the Table Model for the top table on the left side of the info panel<p>
 * Copyright:    Copyright (c) <p>
 * Company:      Agilent Technologies<p>
 * @Michael Martinez-Schiferl
 * @version 1.0
 */
package ComponentLibrary;

import javax.swing.table.AbstractTableModel;
import java.awt.Color;

public class myTopInfoTableModel extends AbstractTableModel {

   private int i = 0;
   private String[] columnNamesTop =    {"Component Name", "Panel Name", "Board Name"};
   private Object[][] dataTop = {
                   {"", "", ""},        // initializes the table as blank
                   };

    public void setTopData(Threshold[] thresh) {
    if (i!=0){
      dataTop[0][0] = thresh[i].getComponentName();   // it wouldn't work unless I changed the array one at a time
      dataTop[0][1] = thresh[i].getPanelName();
      dataTop[0][2] = thresh[i].getBoardName();
      if(i > 11) i = 0; // the data in array[0] will be skipped
    }
    i++;
  }

    public myTopInfoTableModel() {
    }

    public int getColumnCount() {
         return columnNamesTop.length;
    }

    public int getRowCount() {
         return dataTop.length;
    }

    public String getColumnName(int col) {
         return columnNamesTop[col];
    }

    public Object getValueAt(int row, int col) {
        return dataTop[row][col];
    }

    public boolean isCellEditable(int row, int col) {
    // there are only three columns so this makes all of the cells uneditable
    if (col < 3) {
        return false;
    } else {
        return true;
      }
  }
}