/**
 * Title:        Subtype Class<p>
 * Description:  extends Component Class<p>
 * Copyright:    Copyright (c) <p>
 * Company:      Agilent Technologies<p>
 * @Michael Martinez-Schiferl
 * @version 1.0
 */
package ComponentLibrary;

public class Subtype extends Component{

  private String subtypeID;
  private String fov;
  private String pitch;
  private String algSliceHeights;
  private String units;
  private String description;

  public Subtype() {
  }

  public void initializeSubtype(String sub_ID, String _fov, String _pitch, String alg_slice, String _units, String _description) {
  // make the assignments
  subtypeID = sub_ID;
  fov = _fov;
  pitch = _pitch;
  algSliceHeights = alg_slice;
  units = _units;
  description = _description;
  }

  public String getSubtypeID() {
  return this.subtypeID;
  }

  public String getFOV() {
  return this.fov;
  }

  public String getPitch() {
  return this.pitch;
  }

  public String getAlgSliceHeights() {
  return this.algSliceHeights;
  }

  public String getUnits() {
  return this.units;
  }

  public String getDescription () {
  return this.description;
  }
}

