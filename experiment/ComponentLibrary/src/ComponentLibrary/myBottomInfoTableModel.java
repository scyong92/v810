/**
 * Title:        myBottomInfoTableModel Class<p>
 * Description:  This is the table model for the bottom table on the left side of the info panel<p>
 * Copyright:    Copyright (c) <p>
 * Company:      Agilent Technologies<p>
 * @Michael Martinez-Schiferl
 * @version 1.0
 */
package ComponentLibrary;

import javax.swing.table.AbstractTableModel;

public class myBottomInfoTableModel extends AbstractTableModel {

   private int i = 0;
   private String[] columnNamesBottom = {"Subtype ID", "FOV", "Alg Slice Height"};
   private Object[][] dataBottom = {
                   {"", "", ""},        // initializes the table as blank
                   };

    public void setBottomData(Threshold[] thresh) {
    if (i!=0){
      dataBottom[0][0] = thresh[i].getSubtypeID();      // it wouldn't work unless I changed the array one at a time
      dataBottom[0][1] = thresh[i].getFOV();
      dataBottom[0][2] = thresh[i].getAlgSliceHeights();
      if(i > 11) i = 0; // the data in array[0] will be skipped
      }
    i++;
    }

    public myBottomInfoTableModel() {
    }


    public int getColumnCount() {
         return columnNamesBottom.length;
    }

    public int getRowCount() {
         return dataBottom.length;
    }

    public String getColumnName(int col) {
         return columnNamesBottom[col];
    }

    public Object getValueAt(int row, int col) {
        return dataBottom[row][col];
    }

    public boolean isCellEditable(int row, int col) {
    // there are only three columns so this makes all of the cells uneditable
    if (col < 3) {
        return false;
    } else {
        return true;
      }
  }
}