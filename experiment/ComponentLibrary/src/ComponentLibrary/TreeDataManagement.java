/**
 * Title:        TreeDataManagement Class<p>
 * Description:  reads in tree data from <p>
 * Copyright:    Copyright (c) <p>
 * Company:      Agilent Technologies<p>
 * @Michael Martinez-Schiferl
 * @version 1.0
 */
package ComponentLibrary;

import java.io.*;

public class TreeDataManagement {

  private String prefix = "file:" + System.getProperty("user.dir") + System.getProperty("file.separator");

  public TreeDataManagement() {
  }

  /**
   * WriteData(Object unwrittenObject)
   * Precondition : This function is passed a valid object to be written
   * Postcondition : The object is written
   */
  public void writeData(Object unwrittenObject) {
    try {
//        ObjectOutputStream objOut = new ObjectOutputStream(new FileOutputStream(prefix + "Library.dat"));
        ObjectOutputStream objOut = new ObjectOutputStream(new FileOutputStream("C:\\CL\\src\\ComponentLibrary\\Library.dat"));
        objOut.writeObject(unwrittenObject);
    }
    catch (Exception e){
        e.printStackTrace();
    }
  }

  /**
   * readData()
   * Precondition : There must be a Library.dat file in the directory specified below
   * Postcondition : The file is read in and the Threshold[] array is returned
   */
  public Threshold[] readData() {
    try {
//        ObjectInputStream  objIn  = new ObjectInputStream(new  FileInputStream(prefix + "Library.dat"));
        ObjectInputStream  objIn  = new ObjectInputStream(new  FileInputStream("C:\\CL\\src\\ComponentLibrary\\Library.dat"));
        Threshold[] threshy2 = (Threshold[])objIn.readObject();
        return threshy2;
    }
    catch (Exception e){
        e.printStackTrace();
    }
  Threshold[] voidThresh = null;
  return voidThresh;
  }
}
