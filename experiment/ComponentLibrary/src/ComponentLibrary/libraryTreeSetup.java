/**
 * Title:        libraryTreeSetup Class<p>
 * Description:  initialization of library tree<p>
 * Copyright:    Copyright (c) <p>
 * Company:      Agilent Technologies<p>
 * @Michael Martinez-Schiferl
 * @version 1.0
 */
package ComponentLibrary;

import javax.swing.tree.DefaultMutableTreeNode;
import java.io.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class libraryTreeSetup extends TreeSetup {

  private int numberOfDataSetsInFile = 13;  // this comes from the input file data.txt
  private Threshold[] threshy = new Threshold[numberOfDataSetsInFile];

  public libraryTreeSetup() {
  }

///Library Tree Initialization/////////////////////////////////////////////////

  public void libraryTreeInit(DefaultMutableTreeNode top) {

  try {

    TreeDataManagement _newTree = new TreeDataManagement();
    threshy = _newTree.readData();

    //creates the basic joint type tree nodes for the library tree
    //categories Lead Types
    DefaultMutableTreeNode BGA        = new DefaultMutableTreeNode("BGA");
    DefaultMutableTreeNode CAP        = new DefaultMutableTreeNode("CAP");
    DefaultMutableTreeNode FPGULLWING = new DefaultMutableTreeNode("FPGULLWING");
    DefaultMutableTreeNode GULLWING   = new DefaultMutableTreeNode("GULLWING");
    DefaultMutableTreeNode JLEAD      = new DefaultMutableTreeNode("JLEAD");
    DefaultMutableTreeNode MELFRES    = new DefaultMutableTreeNode("MELFRES");
    DefaultMutableTreeNode RES        = new DefaultMutableTreeNode("RES");
    DefaultMutableTreeNode component  = null;

    // adds the basic tree nodes to the top node
    top.add(BGA);
    top.add(CAP);
    top.add(FPGULLWING);
    top.add(GULLWING);
    top.add(JLEAD);
    top.add(MELFRES);
    top.add(RES);


    // this switch statement uses a function Threshold.getCase("Joint_Type")
    // it could be replaced with an enum type emulation
    for(int countAgain = 0; countAgain < 13; countAgain++) {
        switch (threshy[countAgain].getCase(threshy[countAgain].getjointType())) {
            case 1:         BGA.add(threshy[countAgain].getNode());  break;
            case 2:         CAP.add(threshy[countAgain].getNode());  break;
            case 3:  FPGULLWING.add(threshy[countAgain].getNode());  break;
            case 4:    GULLWING.add(threshy[countAgain].getNode());  break;
            case 5:       JLEAD.add(threshy[countAgain].getNode());  break;
            case 6:     MELFRES.add(threshy[countAgain].getNode());  break;
            case 7:         RES.add(threshy[countAgain].getNode());  break;
        } // end switch
    } // end while
  }
  catch (Exception e)
  {
    e.printStackTrace();
  }
  }

  public Threshold[] getArray() {
  return threshy;
  }
}