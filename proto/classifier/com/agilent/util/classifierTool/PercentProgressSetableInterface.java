package proto.classifier.com.agilent.util.classifierTool;

/**
 * @author George A. David
 */
public interface PercentProgressSetableInterface
{
  /**
   * @param percentProgress the percent progress.
   * @author George A. David
   */
  public abstract void setPercentProgress(double percentProgress);
}