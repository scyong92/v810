package proto.classifier.com.agilent.guiUtil.classifierGuiUtil;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import com.agilent.xRayTest.gui.algoTuner.*;
import com.agilent.guiUtil.*;
import com.agilent.util.*;

/**
 * @author George A. David
 */
public class CalendarChooserDialog extends JDialog
{
  public static int OK_OPTION = 1;
  public static int CANCEL_OPTION = 2;
  private JPanel _mainPanel = new JPanel();
  private JPanel _monthAndYearChooserPanel = new JPanel();
  private JButton _day7Button = new JButton();
  private JPanel _dayChooserPanel = new JPanel();
  private JButton _day8Button = new JButton();
  private JPanel _centerPanel = new JPanel();
  private JButton _day21Button = new JButton();
  private JButton _day9Button = new JButton();
  private JButton _day10Button = new JButton();
  private JButton _day1Button = new JButton();
  private JButton _tuesdayButton = new JButton();
  private JButton _saturdayButton = new JButton();
  private JButton _fridayButton = new JButton();
  private JButton _thursdayButton = new JButton();
  private JButton _wednesdayButton = new JButton();
  private JButton _mondayButton = new JButton();
  private JButton _sundayButton = new JButton();
  private FlowLayout _centerPanelFlowLayout = new FlowLayout();
  private JButton _day11Button = new JButton();
  private JButton _day12Button = new JButton();
  private JButton _day13Button = new JButton();
  private JButton _day14Button = new JButton();
  private JButton _day15Button = new JButton();
  private JButton _day16Button = new JButton();
  private JButton _day17Button = new JButton();
  private GridLayout _dayChooserPanelGridLayout = new GridLayout();
  private JButton _day18Button = new JButton();
  private JButton _day19Button = new JButton();
  private JButton _day20Button = new JButton();
  private JButton _day2Button = new JButton();
  private JButton _day3Button = new JButton();
  private JButton _day4Button = new JButton();
  private JButton _day5Button = new JButton();
  private JButton _day6Button = new JButton();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private JPanel _southPanel = new JPanel();
  private JPanel jPanel3 = new JPanel();
  private JPanel jPanel4 = new JPanel();
  private JPanel jPanel10 = new JPanel();
  private GridLayout _monthAndYearChooserPanelGridLayout = new GridLayout();
  private JButton _day22Button = new JButton();
  private JButton _day31Button = new JButton();
  private JButton _day30Button = new JButton();
  private JButton _day29Button = new JButton();
  private JButton _day28Button = new JButton();
  private JButton _day27Button = new JButton();
  private JButton _day26Button = new JButton();
  private JButton _day25Button = new JButton();
  private JButton _day24Button = new JButton();
  private JButton _day23Button = new JButton();
  private JPanel _buttonPanel = new JPanel();
  private GridLayout _buttonPanelGridLayout = new GridLayout();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private Map _numberToDayButtonMap = new HashMap();
  private JButton _selectedDayButton = null;
  private Calendar _todaysDate = new GregorianCalendar();
  private Calendar _selectedDate = new GregorianCalendar();
  private boolean _okButtonPressed = false;
  private Spinner _yearSpinner = new Spinner();
  private JComboBox _monthComboBox = new JComboBox();
  private Spinner _monthSpinner = new Spinner();
  private JTextField _yearTextField = new IntegerNumberField(2000, 0);
  private JPanel _yearChooserPanel = new JPanel();
  private BorderLayout _monthChooserPanelBorderLayout = new BorderLayout();
  private BorderLayout _yearChooserPanelBorderLayout = new BorderLayout();
  private JPanel _monthChooserPanel = new JPanel();
  private FlowLayout _southPanelFlowLayout = new FlowLayout();

  /**
   * @author George A. David
   */
  public CalendarChooserDialog(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author George A. David
   */
  public CalendarChooserDialog()
  {
    this(null, "", false);
  }

  /**
   * @author George A. David
   */
  private void jbInit() throws Exception
  {
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _monthAndYearChooserPanel.setBorder(BorderFactory.createEtchedBorder());
    _monthAndYearChooserPanel.setLayout(_monthAndYearChooserPanelGridLayout);
    _day7Button.setText("7");
    _dayChooserPanel.setLayout(_dayChooserPanelGridLayout);
    _day8Button.setText("8");
    _centerPanel.setLayout(_centerPanelFlowLayout);
    _day21Button.setText("21");
    _day9Button.setText("9");
    _day10Button.setText("10");
    _day1Button.setText("1");
    _tuesdayButton.setRequestFocusEnabled(false);
    _tuesdayButton.setText("Tue");
    _saturdayButton.setRequestFocusEnabled(false);
    _saturdayButton.setText("Sat");
    _fridayButton.setRequestFocusEnabled(false);
    _fridayButton.setToolTipText("");
    _fridayButton.setText("Fri");
    _thursdayButton.setRequestFocusEnabled(false);
    _thursdayButton.setText("Thu");
    _wednesdayButton.setRequestFocusEnabled(false);
    _wednesdayButton.setText("Wed");
    _mondayButton.setRequestFocusEnabled(false);
    _mondayButton.setText("Mon");
    _sundayButton.setRequestFocusEnabled(false);
    _sundayButton.setText("Sun");
    _day11Button.setText("11");
    _day12Button.setText("12");
    _day13Button.setText("13");
    _day14Button.setToolTipText("");
    _day14Button.setText("14");
    _day15Button.setText("15");
    _day16Button.setText("16");
    _day17Button.setText("17");
    _dayChooserPanelGridLayout.setColumns(7);
    _dayChooserPanelGridLayout.setRows(0);
    _day18Button.setText("18");
    _day19Button.setText("19");
    _day20Button.setText("20");
    _day2Button.setText("2");
    _day3Button.setText("3");
    _day4Button.setText("4");
    _day5Button.setText("5");
    _day6Button.setText("6");
    _day22Button.setText("22");
    _day31Button.setText("31");
    _day30Button.setText("30");
    _day29Button.setText("29");
    _day28Button.setText("28");
    _day27Button.setText("27");
    _day26Button.setText("26");
    _day25Button.setText("25");
    _day24Button.setText("24");
    _day23Button.setText("23");
    _buttonPanel.setLayout(_buttonPanelGridLayout);
    _okButton.setText("Ok");
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _okButton_actionPerformed(e);
      }
    });
    _cancelButton.setText("Cancel");
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _cancelButton_actionPerformed(e);
      }
    });
    _buttonPanelGridLayout.setHgap(5);
    _yearTextField.setPreferredSize(new Dimension(100, 21));
    _yearChooserPanel.setLayout(_yearChooserPanelBorderLayout);
    _monthChooserPanel.setLayout(_monthChooserPanelBorderLayout);
    _southPanel.setLayout(_southPanelFlowLayout);
    _southPanel.add(_buttonPanel, null);
    _buttonPanel.add(_okButton, null);
    _buttonPanel.add(_cancelButton, null);
    getContentPane().add(_mainPanel);
    _mainPanel.add(_monthAndYearChooserPanel, BorderLayout.NORTH);
    _mainPanel.add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.add(_dayChooserPanel, null);
    _dayChooserPanel.add(_sundayButton, null);
    _dayChooserPanel.add(_mondayButton, null);
    _dayChooserPanel.add(_tuesdayButton, null);
    _dayChooserPanel.add(_wednesdayButton, null);
    _dayChooserPanel.add(_thursdayButton, null);
    _dayChooserPanel.add(_fridayButton, null);
    _dayChooserPanel.add(_saturdayButton, null);
    _dayChooserPanel.add(jPanel3, null);
    _dayChooserPanel.add(jPanel4, null);
    _dayChooserPanel.add(jPanel10, null);
    _dayChooserPanel.add(_day1Button, null);
    _dayChooserPanel.add(_day2Button, null);
    _dayChooserPanel.add(_day3Button, null);
    _dayChooserPanel.add(_day4Button, null);
    _dayChooserPanel.add(_day5Button, null);
    _dayChooserPanel.add(_day6Button, null);
    _dayChooserPanel.add(_day7Button, null);
    _dayChooserPanel.add(_day8Button, null);
    _dayChooserPanel.add(_day9Button, null);
    _dayChooserPanel.add(_day10Button, null);
    _dayChooserPanel.add(_day11Button, null);
    _dayChooserPanel.add(_day12Button, null);
    _dayChooserPanel.add(_day13Button, null);
    _dayChooserPanel.add(_day14Button, null);
    _dayChooserPanel.add(_day15Button, null);
    _dayChooserPanel.add(_day16Button, null);
    _dayChooserPanel.add(_day17Button, null);
    _dayChooserPanel.add(_day18Button, null);
    _dayChooserPanel.add(_day19Button, null);
    _dayChooserPanel.add(_day20Button, null);
    _dayChooserPanel.add(_day21Button, null);
    _dayChooserPanel.add(_day22Button, null);
    _dayChooserPanel.add(_day23Button, null);
    _dayChooserPanel.add(_day24Button, null);
    _dayChooserPanel.add(_day25Button, null);
    _dayChooserPanel.add(_day26Button, null);
    _dayChooserPanel.add(_day27Button, null);
    _dayChooserPanel.add(_day28Button, null);
    _dayChooserPanel.add(_day29Button, null);
    _dayChooserPanel.add(_day30Button, null);
    _dayChooserPanel.add(_day31Button, null);
    _mainPanel.add(_southPanel,  BorderLayout.SOUTH);

    setUpDayButtons();
    setUpMonthComboBox();
    setUpYearTextField();
    setUpDayChooserPanel();
    setUpSpinners();
    _yearChooserPanel.add(_yearTextField, BorderLayout.CENTER);
    _yearChooserPanel.add(_yearSpinner, BorderLayout.EAST);
    _monthAndYearChooserPanel.add(_monthChooserPanel, null);
    _monthChooserPanel.add(_monthComboBox, BorderLayout.CENTER);
    _monthChooserPanel.add(_monthSpinner, BorderLayout.EAST);
    _monthAndYearChooserPanel.add(_yearChooserPanel, null);
  }

  /**
   * @author George A. David
   */
  private void updateSelectedDate()
  {
    updateMonthComboBox();
    updateYearTextField();
    setUpDayChooserPanel();
    updateDayButton();
    repaint();
  }

  /**
   * @author George A. David
   */
  private void setUpSpinners()
  {
    JButton decrementButton = _monthSpinner.getDecrementButton();
    decrementButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        decrementMonth();
      }
    });
    JButton incrementButton = _monthSpinner.getIncrementButton();
    incrementButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        incrementMonth();
      }

    });

    decrementButton = _yearSpinner.getDecrementButton();
    decrementButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        decrementYear();
      }
    });
    incrementButton = _yearSpinner.getIncrementButton();
    incrementButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        incrementYear();
      }

    });

  }

  /**
   * @author George A. David
   */
  private void incrementYear()
  {
    _selectedDate.add(Calendar.YEAR, 1);
    updateSelectedDate();
  }

  /**
   * @author George A. David
   */
  private void decrementYear()
  {
    _selectedDate.add(Calendar.YEAR, -1);
    updateSelectedDate();
  }

  /**
   * @author George A. David
   */
  public void decrementMonth()
  {
    _selectedDate.add(Calendar.MONTH, -1);
    updateSelectedDate();
  }

  /**
   * @author George A. David
   */
  public void incrementMonth()
  {
    _selectedDate.add(Calendar.MONTH, 1);
    updateSelectedDate();
  }

  /**
   * @author George A. David
   */
  private void setUpDayButtons()
  {
    _numberToDayButtonMap.clear();
    _numberToDayButtonMap.put(new Integer(1), _day1Button);
    _numberToDayButtonMap.put(new Integer(2), _day2Button);
    _numberToDayButtonMap.put(new Integer(3), _day3Button);
    _numberToDayButtonMap.put(new Integer(4), _day4Button);
    _numberToDayButtonMap.put(new Integer(5), _day5Button);
    _numberToDayButtonMap.put(new Integer(6), _day6Button);
    _numberToDayButtonMap.put(new Integer(7), _day7Button);
    _numberToDayButtonMap.put(new Integer(8), _day8Button);
    _numberToDayButtonMap.put(new Integer(9), _day9Button);
    _numberToDayButtonMap.put(new Integer(10), _day10Button);
    _numberToDayButtonMap.put(new Integer(11), _day11Button);
    _numberToDayButtonMap.put(new Integer(12), _day12Button);
    _numberToDayButtonMap.put(new Integer(13), _day13Button);
    _numberToDayButtonMap.put(new Integer(14), _day14Button);
    _numberToDayButtonMap.put(new Integer(15), _day15Button);
    _numberToDayButtonMap.put(new Integer(16), _day16Button);
    _numberToDayButtonMap.put(new Integer(17), _day17Button);
    _numberToDayButtonMap.put(new Integer(18), _day18Button);
    _numberToDayButtonMap.put(new Integer(19), _day19Button);
    _numberToDayButtonMap.put(new Integer(20), _day20Button);
    _numberToDayButtonMap.put(new Integer(21), _day21Button);
    _numberToDayButtonMap.put(new Integer(22), _day22Button);
    _numberToDayButtonMap.put(new Integer(23), _day23Button);
    _numberToDayButtonMap.put(new Integer(24), _day24Button);
    _numberToDayButtonMap.put(new Integer(25), _day25Button);
    _numberToDayButtonMap.put(new Integer(26), _day26Button);
    _numberToDayButtonMap.put(new Integer(27), _day27Button);
    _numberToDayButtonMap.put(new Integer(28), _day28Button);
    _numberToDayButtonMap.put(new Integer(29), _day29Button);
    _numberToDayButtonMap.put(new Integer(30), _day30Button);
    _numberToDayButtonMap.put(new Integer(31), _day31Button);

    int todaysDayOfMonth = _todaysDate.get(Calendar.DAY_OF_MONTH);
    JButton dayButton = (JButton)_numberToDayButtonMap.get(new Integer(todaysDayOfMonth));
    dayButton.setBackground(Color.GRAY);
    _selectedDayButton = dayButton;

    Collection buttons = _numberToDayButtonMap.values();
    Iterator it = buttons.iterator();
    while(it.hasNext())
    {
      dayButton = (JButton)it.next();
      dayButton.setFocusable(false);
      dayButton.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Assert.expect(e != null);
          if(e.getSource() instanceof JButton)
          {
            JButton button = (JButton)e.getSource();
            if(_selectedDayButton != button)
            {
              if(_selectedDayButton != null)
              {
                _selectedDayButton.setBackground(button.getBackground());
              }
              _selectedDayButton = button;
              _selectedDayButton.setBackground(Color.GRAY);
              int dayOfMonth = (new Integer(_selectedDayButton.getText())).intValue();
              _selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            }

          }
        }
      });
    }
  }

  /**
   * @author George A. David
   */
  private void updateDayButton()
  {
    int dayOfMonth = _selectedDate.get(Calendar.DAY_OF_MONTH);
    JButton dayButton = (JButton)_numberToDayButtonMap.get(new Integer(dayOfMonth));
    if(_selectedDayButton != null)
    {
      _selectedDayButton.setBackground(dayButton.getBackground());
    }
    dayButton.setBackground(Color.GRAY);
    _selectedDayButton = dayButton;
  }

  /**
   * @author George A. David
   */
  private void setUpMonthComboBox()
  {
    _monthComboBox.addItem("January");
    _monthComboBox.addItem("February");
    _monthComboBox.addItem("March");
    _monthComboBox.addItem("April");
    _monthComboBox.addItem("May");
    _monthComboBox.addItem("June");
    _monthComboBox.addItem("July");
    _monthComboBox.addItem("August");
    _monthComboBox.addItem("September");
    _monthComboBox.addItem("October");
    _monthComboBox.addItem("November");
    _monthComboBox.addItem("December");
    _monthComboBox.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        int selectedMonth = _monthComboBox.getSelectedIndex();
        if(selectedMonth != _selectedDate.get(Calendar.MONTH))
        {
          _selectedDate.set(Calendar.MONTH, selectedMonth);
          updateSelectedDate();
        }
      }
    });
    updateMonthComboBox();
  }

  /**
   * @author George A. David
   */
  private void updateMonthComboBox()
  {
    int month = _selectedDate.get(Calendar.MONTH);
    _monthComboBox.setSelectedIndex(month);
  }

  /**
   * @author George A. David
   */
  private void setUpYearTextField()
  {
    _yearTextField.addKeyListener(new KeyListener()
    {
      public void keyTyped(KeyEvent e){}
      public void keyPressed(KeyEvent e)
      {
        if(e.getKeyCode() == KeyEvent.VK_ENTER)
        {
          String yearText = _yearTextField.getText();
          if(yearText.length() < 1 ||
             yearText.length() > 4)
          {
            _yearTextField.setText(Integer.toString(_selectedDate.get(Calendar.YEAR)));
          }
          else
          {
            int year = 0;
            try
            {
              year = Integer.parseInt(yearText);
              if(year < 0)
              {
                _yearTextField.setText(Integer.toString(_selectedDate.get(Calendar.YEAR)));
              }
              else if(year != _selectedDate.get(Calendar.YEAR))
              {
                _selectedDate.set(Calendar.YEAR, year);
                updateSelectedDate();
              }
            }
            catch (NumberFormatException ex)
            {
              _yearTextField.setText(Integer.toString(_selectedDate.get(Calendar.YEAR)));
            }

          }
        }
      }
      public void keyReleased(KeyEvent e){}
    });

    updateYearTextField();
  }

  /**
   * @author George A. David
   */
  private void updateYearTextField()
  {
    _yearTextField.setText(Integer.toString(_selectedDate.get(Calendar.YEAR)));
  }



  /**
   * @author George A. David
   */
  private void setUpDayChooserPanel()
  {
    int currentDayOfMonth = _selectedDate.get(Calendar.DAY_OF_MONTH);
    int currentDayOfWeekInMonth = _selectedDate.get(Calendar.DAY_OF_WEEK_IN_MONTH);
    _selectedDate.set(Calendar.DAY_OF_WEEK_IN_MONTH, 1);
    _selectedDate.set(Calendar.DAY_OF_MONTH, 1);
    int firstDayOfMonth = _selectedDate.get(Calendar.DAY_OF_WEEK);
    _dayChooserPanel.removeAll();
    _dayChooserPanel.add(_sundayButton);
    _dayChooserPanel.add(_mondayButton);
    _dayChooserPanel.add(_tuesdayButton);
    _dayChooserPanel.add(_wednesdayButton);
    _dayChooserPanel.add(_thursdayButton);
    _dayChooserPanel.add(_fridayButton);
    _dayChooserPanel.add(_saturdayButton);
    //add empty JPanels
    for(int i = 1; i < firstDayOfMonth; ++i)
    {
      JPanel emptyPanel = new JPanel();
      _dayChooserPanel.add(emptyPanel);
    }

    int numberOfDaysInMonth = _selectedDate.getActualMaximum(Calendar.DAY_OF_MONTH);
    for(int i = 1; i <= numberOfDaysInMonth; ++i)
    {
      JButton dayButton = (JButton)_numberToDayButtonMap.get(new Integer(i));
      _dayChooserPanel.add(dayButton);
    }
    _dayChooserPanel.validate();
    _selectedDate.set(Calendar.DAY_OF_WEEK_IN_MONTH, currentDayOfWeekInMonth);
    _selectedDate.set(Calendar.DAY_OF_MONTH, currentDayOfMonth);
  }

  /**
   * @author George A. David
   */
  void _okButton_actionPerformed(ActionEvent e)
  {
    _okButtonPressed = true;
    dispose();
  }

  /**
   * @author George A. David
   */
  void _cancelButton_actionPerformed(ActionEvent e)
  {
    _okButtonPressed = false;
    dispose();
  }

  /**
   * @author George A. David
   */
  public boolean hasSelectedDate()
  {
    return _selectedDate != null;
  }

  /**
   * @author George A. David
   */
  public Calendar getCalendar()
  {
    Assert.expect(_selectedDate != null);

    Calendar calendar = new GregorianCalendar();
    calendar.setTime(_selectedDate.getTime());

    return calendar;
  }

  /**
   * @author George A. David
   */
  public void setCalendar(Calendar calendar)
  {
    Assert.expect(calendar != null);

    _selectedDate.setTime(calendar.getTime());
    updateSelectedDate();
  }

  /**
   * This version of show returns a response depending on whether OK or CANCEL
   * was clicked.
   * @author George A. David
   */
  public int showAndGetResponse()
  {
    super.show();
    int response = CANCEL_OPTION;
    if(_okButtonPressed)
    {
      response = OK_OPTION;
    }

    return response;
  }
}