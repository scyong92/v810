package proto.classifier.com.agilent.xRayTest.datastore.classifierTool;

import java.util.*;
import java.io.*;
import java.text.*;

import com.agilent.util.*;
import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.datastore.panelDesc.*;

/**
 * @author George A. David
 */
public class ClassifiedJoint
{
  private JointClassificationEnum _systemClassification = null;
  private JointClassificationEnum _trueClassification = null;
  private boolean _lastSaveReviewNeeded;
  private boolean _reviewNeeded;
  private int _sortedJointNumber = -1;
  private String _resultFileName = null;
  private List _measurements = null;
  private int _subtypeNumber = -1;
  private String _panelName = null;
  private int _boardNumber = -1;
  private String _panelSerialNumber = null;
  private String _boardSerialNumber = null;
  private Calendar _inspectionStartDateAndTime = null;
  private String _algorithmFamilyName = null;
  private IntCoordinate _coordinate = null;
  private int _width = -1;
  private int _length = -1;
  private ShapeEnum _padShape = null;
  private int _viewNumber = -1;
  private int _numberOfTimesReviewed = -1;
  private String _referenceDesignator = null;
  private String _pinName = null;

  /**
   * @author George A. David
   */
  void setReferenceDesignator(String referenceDesignator)
  {
    Assert.expect(referenceDesignator != null);

    _referenceDesignator = referenceDesignator;
  }

  /**
   * @author George A. David
   */
  void setPinName(String pinName)
  {
    Assert.expect(pinName != null);

    _pinName = pinName;
  }

  /**
   * @author George A. David
   */
  void setSystemClassification(JointClassificationEnum jointClassification)
  {
    Assert.expect(jointClassification != null);

    _systemClassification = jointClassification;
  }

  /**
   * @author George A. David
   */
  public void setTrueClassification(JointClassificationEnum jointClassification)
  {
    Assert.expect(jointClassification != null);

    _trueClassification = jointClassification;
  }

  /**
   * @author George A. David
   */
  void setMeasurments(List measurements)
  {
    Assert.expect(measurements != null && measurements.size() == 27);

    _measurements = measurements;
  }

  /**
   * @author George A. David
   */
  void setSubtypeNumber(int subtypeNumber)
  {
    Assert.expect(subtypeNumber >= 0);

    _subtypeNumber = subtypeNumber;
  }

  /**
   * @author George A. David
   */
  void setSortedJointNumber(int sortedJointNumber)
  {
    Assert.expect(sortedJointNumber >= 0);

    _sortedJointNumber = sortedJointNumber;
  }

  /**
   * @author George A. David
   */
  void setResultFileName(String resultFileName)
  {
    Assert.expect(resultFileName != null);

    _resultFileName = resultFileName;
  }

  /**
   * @author George A. David
   */
  void setPanelName(String panelName)
  {
    Assert.expect(panelName != null);

    _panelName = panelName;
  }

  /**
   * @author George A. David
   */
  void setBoardNumber(int boardNumber)
  {
    Assert.expect(boardNumber > 0);

    _boardNumber = boardNumber;
  }

  /**
   * @author George A. David
   */
  void setPanelSerialNumber(String serialNumber)
  {
    Assert.expect(serialNumber != null);

    _panelSerialNumber = serialNumber;
  }

  /**
   * @author George A. David
   */
  void setBoardSerialNumber(String serialNumber)
  {
    Assert.expect(serialNumber != null);

    _boardSerialNumber = serialNumber;
  }

  /**
   * @author George A. David
   */
  void setInspectionStartDateAndTime(Calendar dateAndTime)
  {
    Assert.expect(dateAndTime != null);
    _inspectionStartDateAndTime = dateAndTime;

  }

  /**
   * @author George A. David
   */
  void setAlgorithmFamilyName(String algorithmFamilyName)
  {
    Assert.expect(algorithmFamilyName != null);

    _algorithmFamilyName = algorithmFamilyName;
  }

  /**
   * @author George A. David
   */
  void setCoordinate(IntCoordinate coordinate)
  {
    Assert.expect(coordinate != null);

    _coordinate = coordinate;
  }

 /**
  * @author George A. David
  */
  public IntCoordinate getCoordinate()
  {
    Assert.expect(_coordinate != null);

    return _coordinate;
  }

  /**
   * @author George A. David
   */
  void setWidth(int width)
  {
    Assert.expect(width > 0);

    _width = width;
  }

  /**
   * @author Erica Wheatcroft
   */
  void setLastSaveReviewNeeded(boolean lastSaveReviewNeededValue)
  {
    _lastSaveReviewNeeded = lastSaveReviewNeededValue;
  }

  /**
   * @author George A. David
   */
  public int getWidth()
  {
    Assert.expect(_width > 0);

    return _width;
  }

  /**
   * @author George A. David
   */
  void setLength(int length)
  {
    Assert.expect(length > 0);

    _length = length;
  }

  /**
   * @author George A. David
   */
  public int getLength()
  {
    Assert.expect(_length > 0);

    return _length;
  }

  /**
   * @author George A. David
   */
  void setPadShape(ShapeEnum padShape)
  {
    Assert.expect(padShape != null);

    _padShape = padShape;
  }

  /**
   * @author George A. David
   */
  void setViewNumber(int viewNumber)
  {
    Assert.expect(viewNumber >= 0);

    _viewNumber = viewNumber;
  }

  /**
   * @author George A. David
   */
  public void setReviewNeeded(boolean reviewNeeded)
  {
    _reviewNeeded = reviewNeeded;
  }

  /**
   * @author George A. David
   */
  void setNumberOfTimeReviewed(int numberOfTimesReviewed)
  {
    Assert.expect(numberOfTimesReviewed >= 0);

    _numberOfTimesReviewed = numberOfTimesReviewed;
  }
  /**
   * @author George A. David
   */
  void incrementNumberOfTimesReviewed()
  {
    ++_numberOfTimesReviewed;
  }

  /**
   * @author George A. David
   */
  public String getReferenceDesignator()
  {
    Assert.expect(_referenceDesignator != null);

    return _referenceDesignator;
  }

  /**
   * @author George A. David
   */
  public String getPinName()
  {
    Assert.expect(_pinName != null);

    return _pinName;
  }

  /**
   * @author George A. David
   */
  public String getResultFileName()
  {
    Assert.expect(_resultFileName != null);

    return _resultFileName;
  }

  /**
   * @author George A. David
   */
  public int getBoardNumber()
  {
    Assert.expect(_boardNumber > 0);

    return _boardNumber;
  }

  /**
   * @author George A. David
   */
  public String getPanelSerialNumber()
  {
    Assert.expect(_panelSerialNumber != null);

    return _panelSerialNumber;
  }

  /**
   * @author George A. David
   */
  public String getBoardSerialNumber()
  {
    Assert.expect(_boardSerialNumber != null);

    return _boardSerialNumber;
  }

  /**
   * @author George A. David
   */
  public String getPanelName()
  {
    Assert.expect(_panelName != null);

    return _panelName;
  }

  /**
   * @author George A. David
   */
  public JointClassificationEnum getSystemClassification()
  {
    Assert.expect(_systemClassification != null);

    return _systemClassification;
  }

  /**
   * @author George A. David
   */
  public JointClassificationEnum getTrueClassification()
  {
    Assert.expect(_trueClassification != null);

    return _trueClassification;
  }

  /**
   * @author George A. David
   */
  public int getSubtypeNumber()
  {
    Assert.expect(_subtypeNumber >= 0);

    return _subtypeNumber;
  }

  /**
   * @author George A. David
   */
  public int getSortedJointNumber()
  {
    Assert.expect(_sortedJointNumber >= 0);

    return _sortedJointNumber;
  }

  /**
   * @author George A. David
   */
  public Calendar getInspectionStartDateAndTime()
  {
    Assert.expect(_inspectionStartDateAndTime  != null);

    return _inspectionStartDateAndTime;
  }

  /**
   * @author George A. David
   */
  public String getAlgorithmFamilyName()
  {
    Assert.expect(_algorithmFamilyName != null);

    return _algorithmFamilyName;
  }

  /**
   * @author George A. David
   */
  public ShapeEnum getPadShape()
  {
    Assert.expect(_padShape != null);

    return _padShape;
  }

  public String getPadShapeString()
  {
    if(_padShape.equals(ShapeEnum.CIRCLE))
      return "Circle";
    else if(_padShape.equals(ShapeEnum.RECTANGLE))
      return "Rectangle";
    return null; // should never happen
  }

  /**
   * @author George A. David
   */
  public int getViewNumber()
  {
    Assert.expect(_viewNumber >= 0);

    return _viewNumber;
  }

  /**
   * The view file name is a combination of board number,
   * view number, and slice number.
   * bbvvvvss.jpg
   * bb is the board number
   * vvvv is the view number
   * ss is the slice number.
   * 01009601.jpg is an image file for board 1, view 96, slice 1.
   * @author George A. David
   */
  public List getViewFileNames()
  {
    Assert.expect(_boardNumber > 0);
    Assert.expect(_viewNumber >= 0);

    List fileNames = new ArrayList();

    //start with the slice number 0
    String boardNumber = Integer.toString(_boardNumber);
    int padding = 2 - boardNumber.length();
    Assert.expect(padding >= 0);
    for(int i = 0; i < padding; ++i)
    {
      boardNumber = "0" + boardNumber;
    }

    String viewNumber = Integer.toString(_viewNumber);
    int length = viewNumber.length();
    padding = 4 - viewNumber.length();
    Assert.expect(padding >= 0, "View Number is " + viewNumber);
    for(int i = 0; i < padding; ++i)
    {
      viewNumber = "0" + viewNumber;
    }

    for(int i = 0; i < 100; ++i)
    {
      String classifierResultsDirectory = System.getProperty("AGT5DX_STAGE2_DIR", "") + File.separator + "res";
      String imageFileName = classifierResultsDirectory + File.separator +
                             _resultFileName + File.separator + boardNumber + viewNumber;
      if(i < 10)
      {
        imageFileName += "0" + i + ".jpg";
      }
      else
      {
        imageFileName += i + ".jpg";
      }
      if(Directory.doesFileExist(imageFileName))
      {
        fileNames.add(imageFileName);
      }
      else
      {
        break;
      }
    }

    return fileNames;

  }
  /**
   * @author George A. David
   */
  public boolean isReviewNeeded()
  {
    return _reviewNeeded;
  }

  /**
   * This will increment the review count if and only if the lastSaveReviewNeeded flag was set to 1
   * on load and the reviewNeeded flag has been set to 0 at runtime. On save this will result in the
   * review count being updated.
   *
   * @author Erica Wheatcroft
   */
  public void incrementReviewCountIfNeeded()
  {
    if(_lastSaveReviewNeeded == true && _reviewNeeded == false)
    {
      // a review is needed so increment the the review count
      _numberOfTimesReviewed++;
      _lastSaveReviewNeeded = _reviewNeeded;
    }

  }

  /**
   * @author George A. David
   */
  public int getNumberOfTimesReviewed()
  {
    Assert.expect(_numberOfTimesReviewed >= 0);

    return _numberOfTimesReviewed;
  }

  public Date getInspectionStartTime()
  {
    Assert.expect(_inspectionStartDateAndTime != null);

    return _inspectionStartDateAndTime.getTime();
  }

  /**
   * @author George A. David
   */
  public String getInspectionStartDateAndTimeString()
  {
    Assert.expect(_inspectionStartDateAndTime != null);

    DateFormat df = DateFormat.getDateInstance(DateFormat.LONG);
    String dateAndTime = df.format(_inspectionStartDateAndTime.getTime()) + " ";
    int hour = _inspectionStartDateAndTime.get(Calendar.HOUR);

    //hack because calendar api is crap
    if(hour == 0)
     hour = 12;

    dateAndTime += hour + ":";

    int minutes = _inspectionStartDateAndTime.get(Calendar.MINUTE);
    if(minutes < 10)
    {
      dateAndTime += "0";
    }
    dateAndTime += minutes + ":";
    int seconds = _inspectionStartDateAndTime.get(Calendar.SECOND);
    if(seconds < 10)
    {
      dateAndTime += "0";
    }
    dateAndTime += seconds + " ";
    int amOrPm = _inspectionStartDateAndTime.get(Calendar.AM_PM);
    if(amOrPm == Calendar.AM)
    {
      dateAndTime += "AM";
    }
    else if(amOrPm == Calendar.PM)
    {
      dateAndTime += "PM";
    }
    else
    {
      Assert.expect(false);
    }

    if(_referenceDesignator.equalsIgnoreCase("u11") && dateAndTime.startsWith("March 7, 2002"))
    {
      boolean value = true;
    }

    return dateAndTime;
  }

  /**
   * @author George A. David
   */
  List getMeasurements()
  {
    Assert.expect(_measurements != null);

    return _measurements;
  }
}
