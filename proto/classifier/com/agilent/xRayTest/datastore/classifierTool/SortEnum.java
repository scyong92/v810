package proto.classifier.com.agilent.xRayTest.datastore.classifierTool;

import com.agilent.util.*;

/**
 * @author George A. David
 */
public class SortEnum extends Enum
{
  private static int _index = -1;
  public final static SortEnum NONE = new SortEnum(++_index);
  public final static SortEnum ASCENDING = new SortEnum(++_index);
  public final static SortEnum DESCENDING = new SortEnum(++_index);

  /**
   * @author George A. David
   */
  private SortEnum(int id)
  {
    super(id);
  }

  /**
   * @author George A. David
   */
  public String toString()
  {
    String str = "";
    if (this.equals(NONE))
      str = "None";
    else if (this.equals(ASCENDING))
      str = "Ascending";
    else if (this.equals(DESCENDING))
      str = "Descending";
    else
      Assert.expect(false);

    return str;
  }
}