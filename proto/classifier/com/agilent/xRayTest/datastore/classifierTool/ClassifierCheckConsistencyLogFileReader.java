package proto.classifier.com.agilent.xRayTest.datastore.classifierTool;

import java.io.*;

import com.agilent.xRayTest.datastore.*;
import com.agilent.util.*;

import proto.classifier.com.agilent.util.classifierTool.*;

public class ClassifierCheckConsistencyLogFileReader
{
  /**
   * @author George A. David
   */
  public String read(String logFileName) throws DatastoreException
  {

    String results = "-1";
    final WorkerThread workerThread = new WorkerThread("log file reader");
    while(results.equalsIgnoreCase("-1"))
    {
      workerThread.invokeAndWait(new Runnable()
      {
        public void run()
        {
          try
          {
            workerThread.sleep(1000);
          }
          catch(InterruptedException interruptedException)
          {
            // do nothing
          }
        }
      });
      results = readFile(logFileName);
    }

    return results;
  }

  public String readFile(String logFileName) throws DatastoreException
  {
    Assert.expect(logFileName != null);

    String results = "-1";

    LineNumberReader is = null;
    try
    {
      is = ParseUtil.openFile(logFileName, 1);
      String line = ParseUtil.readNextLine(is, logFileName);
      while(line != null)
      {
        //looking for line x out of y joints flagged for review!
        int index = line.indexOf("joints flagged for review");
        if(index != -1)
        {
          results = line;
        }
        line = ParseUtil.readNextLine(is, logFileName);
      }
    }
    finally
    {
      if(is != null)
      {
        try
        {
          is.close();
        }
        catch (IOException ex)
        {
          // do nothing
        }
      }
    }

    return results;
  }

}