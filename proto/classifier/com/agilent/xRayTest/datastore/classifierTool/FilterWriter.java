package proto.classifier.com.agilent.xRayTest.datastore.classifierTool;


import java.io.*;
import java.util.*;

import com.agilent.util.*;
import com.agilent.xRayTest.datastore.*;

/**
 * @author George A. David
 */
class FilterWriter
{
  /**
   * @author George A. David
   */
  void writerFilter(Filter filter) throws DatastoreException
  {
    Assert.expect(filter != null);

    String filterFilePath = filter.getFilterFilePath();

    File filterFile = new File(filterFilePath);
    File backupFilterFile = null;
    if(filterFile.exists())
    {
      backupFilterFile = new File(filterFilePath + FileName.getBackupExtension());
      filterFile.renameTo(backupFilterFile);
    }
    ObjectOutput os = null;
    try
    {
      FileOutputStream fos = new FileOutputStream(filterFile);
      BufferedOutputStream bos = new BufferedOutputStream(fos);
      os = new ObjectOutputStream(bos);

      os.writeObject(filter);
    }
    catch(IOException e)
    {
      try
      {
        if (os != null)
          os.close();
      }
      catch(IOException ioe)
      {
        //do nothing
      }

      // delete the partially witten file
      filterFile.delete();

      //restore the backup file
      if(backupFilterFile != null)
      {
        backupFilterFile.renameTo(filterFile);
      }

      CannotWriteDatastoreException dex = new CannotWriteDatastoreException(filterFilePath);
      dex.initCause(e);
      throw dex;
    }
    finally
    {
      try
      {
        if (os != null)
          os.close();
      }
      catch(IOException ioe)
      {
        // do nothing
      }
    }
  }
}