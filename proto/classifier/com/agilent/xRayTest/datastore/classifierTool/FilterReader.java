package proto.classifier.com.agilent.xRayTest.datastore.classifierTool;

import java.io.*;

import com.agilent.xRayTest.datastore.*;
import com.agilent.util.*;
/**
 * @author George A. David
 */
class FilterReader
{
  /**
   * @author George A. David
   */
  Filter readFilter(String filterFilePath) throws DatastoreException
  {
    Assert.expect(filterFilePath != null);
    Filter filter = null;
    ObjectInput is = null;
    try
    {
      FileInputStream fis = new FileInputStream(filterFilePath);
      BufferedInputStream bis = new BufferedInputStream(fis);
      is = new ObjectInputStream(bis);

      filter = (Filter)is.readObject();
    }
    catch(InvalidClassException ice)
    {
      throw new FileCorruptDatastoreException(filterFilePath);
    }
    catch(FileNotFoundException fnfe)
    {
      throw new FileNotFoundDatastoreException(filterFilePath);
    }
    catch(StreamCorruptedException sce)
    {
      throw new FileCorruptDatastoreException(filterFilePath);
    }
    catch(IOException ioe)
    {
      throw new DatastoreException(filterFilePath);
    }
    catch(ClassNotFoundException cnfe)
    {
      throw new FileCorruptDatastoreException(filterFilePath);
    }
    finally
    {
      try
      {
        if (is != null)
          is.close();
      }
      catch(IOException ioe)
      {
        // do nothing
      }
    }

    Assert.expect(filter != null);

    return filter;
  }
}
