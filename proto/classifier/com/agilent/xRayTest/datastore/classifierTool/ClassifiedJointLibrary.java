package proto.classifier.com.agilent.xRayTest.datastore.classifierTool;

import java.util.*;

import com.agilent.xRayTest.datastore.*;
import com.agilent.util.*;

/**
 * @author George A. David
 */
public class ClassifiedJointLibrary
{
  private static Map _classifiedJointNameToClassifiedJointMap = new HashMap();
  private final static String _KEY_SPACER = "_";

  /**
   * @author George A. David
   */
  public static boolean addClassifiedJoint(ClassifiedJoint classifiedJoint)
  {
    Assert.expect(classifiedJoint != null);

    String key = createClassifiedJointNameToClassifiedJointMapKey(classifiedJoint);
    Object previous = _classifiedJointNameToClassifiedJointMap.put(key, classifiedJoint);
    Assert.expect(previous == null, "Duplicate classified joint, call containsClassifiedJoint() first");
    return true;
  }

  /**
   * @author George A. David
   */
  public static ClassifiedJoint getClassifiedJoint(String resultFileName,
                                                   String panelSerialNumber,
                                                   String boardSerialNumber,
                                                   String referenceDesignator,
                                                   String pinName)
  {
    String key = createClassifiedJointNameToClassifiedJointMapKey(resultFileName,
                                                                  panelSerialNumber,
                                                                  boardSerialNumber,
                                                                  referenceDesignator,
                                                                  pinName);
    ClassifiedJoint classifiedJoint = (ClassifiedJoint)_classifiedJointNameToClassifiedJointMap.get(key);

    Assert.expect(classifiedJoint != null, "Classified Joint not found, call containsClassifiedJoint() first");

    return classifiedJoint;
  }

  /**
   * @author George A. David
   */
  public static boolean containsClassifiedJoint(ClassifiedJoint classifiedJoint)
  {
    String key = createClassifiedJointNameToClassifiedJointMapKey(classifiedJoint);

    return _classifiedJointNameToClassifiedJointMap.containsKey(key);
  }

  /**
   * @author George A. David
   */
  public static boolean containsClassifiedJoint(String resultFileName,
                                                String panelSerialNumber,
                                                String boardSerialNumber,
                                                String referenceDesignator,
                                                String pinName)

  {
    String key = createClassifiedJointNameToClassifiedJointMapKey(resultFileName,
                                                                  panelSerialNumber,
                                                                  boardSerialNumber,
                                                                  referenceDesignator,
                                                                  pinName);

    return _classifiedJointNameToClassifiedJointMap.containsKey(key);
  }

  /**
   * @author George A. David
   */
  private static String createClassifiedJointNameToClassifiedJointMapKey(ClassifiedJoint classifiedJoint)
  {
    Assert.expect(classifiedJoint != null);

    return createClassifiedJointNameToClassifiedJointMapKey(classifiedJoint.getResultFileName(),
                                                            classifiedJoint.getPanelSerialNumber(),
                                                            classifiedJoint.getBoardSerialNumber(),
                                                            classifiedJoint.getReferenceDesignator(),
                                                            classifiedJoint.getPinName());
  }

  /**
   * @author George A. David
   */
  public static String createClassifiedJointNameToClassifiedJointMapKey(String resultFileName,
                                                                       String panelSerialNumber,
                                                                       String boardSerialNumber,
                                                                       String referenceDesignator,
                                                                       String pinName)

  {
    Assert.expect(resultFileName != null);
    Assert.expect(panelSerialNumber !=null);
    Assert.expect(boardSerialNumber != null);
    Assert.expect(referenceDesignator != null);
    Assert.expect(pinName != null);

    String key = resultFileName + _KEY_SPACER +
                 panelSerialNumber + _KEY_SPACER +
                 boardSerialNumber + _KEY_SPACER +
                 referenceDesignator + _KEY_SPACER +
                 pinName;
    return key.toUpperCase();
  }

  /**
   * @author George A. David
   */
  public static List getClassifiedJoints()
  {
    return new ArrayList(_classifiedJointNameToClassifiedJointMap.values());
  }

  /**
   * @author George A. David
   */
  public static void load(String fileName) throws DatastoreException
  {
    _classifiedJointNameToClassifiedJointMap.clear();
    AsciiDatabaseReader adbReader = new AsciiDatabaseReader();
    adbReader.parseFile(fileName);
  }

  /**
   * @author George A. David
   */
  public static void save(String fileName) throws DatastoreException
  {
    AsciiDatabaseWriter adbWriter = new AsciiDatabaseWriter();
    adbWriter.write(fileName);
  }

  /**
   * @author George A. David
   */
  public static void delete(ClassifiedJoint classifiedJoint)
  {
    Object key = createClassifiedJointNameToClassifiedJointMapKey(classifiedJoint);

    Object value = _classifiedJointNameToClassifiedJointMap.remove(key);
    Assert.expect(value != null, "The classified joint does not exist, call containsClassifiedJoint() first.");
  }

  /**
   * @author George A. David
   */
  public static void delete(List classifiedJoints)
  {
    Assert.expect(classifiedJoints != null);
    Iterator it = classifiedJoints.iterator();
    while(it.hasNext())
    {
      delete((ClassifiedJoint)it.next());
    }
  }
}
