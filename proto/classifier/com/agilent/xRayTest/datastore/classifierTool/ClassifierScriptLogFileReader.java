package proto.classifier.com.agilent.xRayTest.datastore.classifierTool;

import java.io.*;

import com.agilent.xRayTest.datastore.*;
import com.agilent.util.*;

import proto.classifier.com.agilent.util.classifierTool.*;
/**
 * Reads the log files created by the classifer perl scripts.
 *
 * @author George A. David
 */
public class ClassifierScriptLogFileReader
{
  /**
   * @author George A. David
   */
  public int read(String logFileName, PercentProgressSetableInterface percentProgressSetable) throws DatastoreException
  {
    Assert.expect(percentProgressSetable != null);
    final WorkerThread workerThread = new WorkerThread("log file reader");
    int percentProgress = 0;
    while(percentProgress < 100)
    {
      workerThread.invokeAndWait(new Runnable()
      {
        public void run()
        {
          try
          {
            workerThread.sleep(1000);
          }
          catch(InterruptedException interruptedException)
          {
            // do nothing
          }
        }
      });
      percentProgress = readFile(logFileName);
      percentProgressSetable.setPercentProgress(percentProgress);
    }

    return percentProgress;
  }

  /**
   * @author George A. David
   */
  private int readFile(String logFileName) throws DatastoreException
  {
    Assert.expect(logFileName != null);

    int percentProgress = 0;
    LineNumberReader is = null;
    try
    {
      is = ParseUtil.openFile(logFileName, 1);
      String line = ParseUtil.readNextLine(is, logFileName);

      while(line != null)
      {
        int index = line.indexOf("% done!");
        if(index != -1)
        {
          try
          {
            percentProgress = StringUtil.convertStringToInt(line.substring(0, index), 0 , 100);
          }
          catch(BadFormatException bfe)
          {
            throw new FileCorruptDatastoreException(logFileName, is.getLineNumber(), "a precent value", line);
          }
        }
        line = ParseUtil.readNextLine(is, logFileName);
      }
    }
    finally
    {
      if(is != null)
      {
        try
        {
          is.close();
        }
        catch (IOException ex)
        {
          // do nothing
        }
      }
    }

    return percentProgress;
  }
}