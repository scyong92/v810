package proto.classifier.com.agilent.xRayTest.datastore.classifierTool;

import com.agilent.util.*;

/**
 * @author George A. David
 */
public class JointClassificationEnum extends Enum
{
  private static int _index = -1;
  public final static JointClassificationEnum GOOD = new JointClassificationEnum(++_index);
  public final static JointClassificationEnum BAD = new JointClassificationEnum(++_index);

  /**
   * @author George A. David
   */
  private JointClassificationEnum(int id)
  {
    super(id);
  }

  /**
   * @author George A. David
   */
  public String toString()
  {
    String str = "";
    if (this.equals(GOOD))
      str = "Good";
    else if (this.equals(BAD))
      str = "Bad";
    else
      Assert.expect(false);

    return str;
  }
}
