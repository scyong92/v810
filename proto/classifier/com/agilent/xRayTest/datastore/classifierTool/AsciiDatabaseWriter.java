package proto.classifier.com.agilent.xRayTest.datastore.classifierTool;

import java.io.*;
import java.util.*;

import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.datastore.panelDesc.*;
import com.agilent.util.*;
/**
 * This file writes out the ascii database file The format of
 * the file is as follows:
 * <reference designator> <pin number> <system classification> <true classification> <f1> ... <fn>
 * <subtype number> <sorted joint number> <result file name> <panel name> <board number>
 * <panel serial number> <board serial number> <date and time> <algorithm family name>
 * <x coordinate> <y coordinate> <width> <length> <pad shape> <view number> <review needed> <review count>
 *
 * f1 - fn are measurments. For this proto code, we will only be dealing with the FPGullwing algoirthm family.
 * This algorithm contains 27 measurments. This number will change depending on the algorithm family.
 * @author George A. David
 */
class AsciiDatabaseWriter
{
  private AsciiDatabaseWriter _instance = null;
  private PrintWriter _os = null;

  /**
   * @author George A. David
   */
  public synchronized AsciiDatabaseWriter getInstance()
  {
    if(_instance == null)
    {
      _instance = new AsciiDatabaseWriter();
    }

    return _instance;
  }

  /**
   * @author George A. David
   */
  public void write(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);

    File file = new File(fileName);
    File backupFile = new File(fileName + FileName.getBackupExtension());
    if(file.exists())
    {
      file.renameTo(backupFile);
    }

    try
    {
      FileWriter fw = new FileWriter(fileName);
      BufferedWriter bw = new BufferedWriter(fw);
      _os = new PrintWriter(bw);
      writeFile();
    }
    catch(IOException ioe)
    {
      // delete the partially witten file
      file.delete();

      //restore the backup file
      if(backupFile.exists())
      {
        backupFile.renameTo(file);
      }

      CannotWriteDatastoreException dex = new CannotWriteDatastoreException(fileName);
      dex.initCause(ioe);
      throw dex;

    }
    finally
    {
      if(_os != null)
        _os.close();
    }
  }

  /**
   * @author George A. David
   */
  private void writeFile()
  {
    Assert.expect(_os != null);

    List classifiedJoints = ClassifiedJointLibrary.getClassifiedJoints();
    Iterator it = classifiedJoints.iterator();
    while(it.hasNext())
    {
      ClassifiedJoint classifiedJoint = (ClassifiedJoint)it.next();
      String line = classifiedJoint.getReferenceDesignator() + " " +
                    classifiedJoint.getPinName() + " ";
      if(classifiedJoint.getSystemClassification().equals(JointClassificationEnum.BAD))
      {
        line += 1 + " ";
      }
      else if(classifiedJoint.getSystemClassification().equals(JointClassificationEnum.GOOD))
      {
        line += 0 + " ";
      }
      else
      {
        Assert.expect(false);
      }

      if(classifiedJoint.getTrueClassification().equals(JointClassificationEnum.BAD))
      {
        line += 1 + " ";
      }
      else if(classifiedJoint.getTrueClassification().equals(JointClassificationEnum.GOOD))
      {
        line += 0 + " ";
      }
      else
      {
        Assert.expect(false);
      }

      List measurements = classifiedJoint.getMeasurements();
      Iterator mit = measurements.iterator();
      while(mit.hasNext())
      {
        Double measurement = (Double)mit.next();
        line += measurement + " ";
      }

      line += classifiedJoint.getSubtypeNumber() + " " +
              classifiedJoint.getSortedJointNumber() + " " +
              classifiedJoint.getResultFileName() + " " +
              classifiedJoint.getPanelName() + " " +
              classifiedJoint.getBoardNumber() + " " +
              classifiedJoint.getPanelSerialNumber() + " " +
              classifiedJoint.getBoardSerialNumber() + " ";

      Calendar calendar = classifiedJoint.getInspectionStartDateAndTime();
      line += Integer.toString(calendar.get(Calendar.YEAR));
      int month = calendar.get(Calendar.MONTH) + 1;
      if(month < 10)
      {
        line += "0";
      }
      line += Integer.toString(month);
      int day = calendar.get(Calendar.DAY_OF_MONTH);
      if(day < 10)
      {
        line += "0";
      }
      line += Integer.toString(day);
      int hour = calendar.get(Calendar.HOUR_OF_DAY);
      if(hour < 10)
      {
        line += "0";
      }
      line += Integer.toString(hour);
      int minute = calendar.get(Calendar.MINUTE);
      if(minute < 10)
      {
        line += "0";
      }
      line += Integer.toString(minute);
      int second = calendar.get(Calendar.SECOND);
      if(second < 10)
      {
        line += "0";
      }
      line += Integer.toString(second) + " " +
              classifiedJoint.getAlgorithmFamilyName() + " " +
              classifiedJoint.getCoordinate().getX() + " " +
              classifiedJoint.getCoordinate().getY() + " " +
              classifiedJoint.getWidth() + " " +
              classifiedJoint.getLength() + " ";
      if(classifiedJoint.getPadShape().equals(ShapeEnum.RECTANGLE))
      {
        line += "0" + " ";
      }
      else if(classifiedJoint.getPadShape().equals(ShapeEnum.CIRCLE))
      {
        line += "1" + " ";
      }
      line += classifiedJoint.getViewNumber() + " ";
      if(classifiedJoint.isReviewNeeded())
      {
        line += "1" + " ";
      }
      else
      {
        line += "0" + " ";
      }
      classifiedJoint.incrementReviewCountIfNeeded();
      line += classifiedJoint.getNumberOfTimesReviewed();
      _os.println(line);
    }
  }
}