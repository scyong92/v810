package proto.classifier.com.agilent.xRayTest.datastore.classifierTool;

import java.io.*;
import java.util.*;

import com.agilent.xRayTest.datastore.panelDesc.*;
import com.agilent.util.*;
import com.agilent.xRayTest.datastore.*;
/**
 * @author George A. David
 */
public class Filter implements Serializable
{
  private String _name = null;
  private String _filterFilePath = null;
  private List _filterDataList = new ArrayList();
  public static final String _PANEL_NAME = "Panel Name";
  public static final String _PANEL_SERIAL_NUMBER = "Panel Serial Number";
  public static final String _REFERENCE_DESIGNATOR = "Reference Designator";
  public static final String _PIN_NAME = "Pin Name";
  public static final String _SYSTEM_CLASSIFICATION = "System Classification";
  public static final String _TRUE_CLASSIFICATION = "True Classification";
  public static final String _SUBTYPE_NUMBER = "Subtype Number";
  public static final String _SORTED_JOINT_NUMBER = "Sorted Joint Number";
  public static final String _RESULT_FILE_NAME = "Result File Name";
  public static final String _BOARD_NUMBER = "Board Number";
  public static final String _BOARD_SERIAL_NUMBER = "Board Serial Number";
  public static final String _INSPECTION_START_TIME = "Inspection Start Time";
  public static final String _ALGORITHM_FAMILY_NAME = "Algorithm Family Name";
  public static final String _PAD_SHAPE = "Pad Shape";
  public static final String _VIEW_NUMBER = "View Number";
  public static final String _REVIEW_NEEDED = "Review Needed";
  public static final String _NUMBER_OF_TIMES_REVIEWED = "Number Of Times Reviewed";

  /**
   * @author George A. David
   */
  public void setName(String name)
  {
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author George A. David
   */
  public boolean hasName()
  {
    return _name != null;
  }

  /**
   * @author George A. David
   */
  public String getName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @author George A. David
   */
  public void setFilterFilePath(String filterFilePath)
  {
    Assert.expect(filterFilePath != null);

    _filterFilePath = filterFilePath;
  }

  /**
   * @author George A. David
   */
  public String getFilterFilePath()
  {
    Assert.expect(_filterFilePath != null);

    return _filterFilePath;
  }

  /**
   * @author George A. David
   */
  public boolean containsFilterData(FilterData filterData)
  {
    Assert.expect(filterData != null);

    return _filterDataList.contains(filterData);
  }

  /**
   * @author Erica Wheatcroft
   */
  public FilterData getFilterData(FilterData filterData)
  {
    Assert.expect(filterData != null);

    return (FilterData)_filterDataList.get(_filterDataList.indexOf(filterData));
  }

  /**
   * @author George A. David
   */
  public boolean addFilterData(FilterData filterData)
  {
    Assert.expect(filterData != null);
    Assert.expect(_filterDataList.contains(filterData) == false);

    int length = _filterDataList.size();

    _filterDataList.add(length, filterData);

    return true;
  }

  /**
   * @author George A. David
   */
  public List getFilterDataList()
  {
    return _filterDataList;
  }

  /**
   * @author George A. David
   */
  public void save() throws DatastoreException
  {
    FilterWriter filterWriter = new FilterWriter();
    filterWriter.writerFilter(this);
  }

  /**
   * @author George A. David
   */
  public boolean accept(ClassifiedJoint classifiedJoint)
  {
    Assert.expect(classifiedJoint != null);
    boolean accept = true;
    Iterator it = _filterDataList.iterator();
    while(it.hasNext())
    {
      FilterData filterData = (FilterData)it.next();
      String filterName = filterData.getFilterName();
      String stringToCheck = null;
      if(filterName.equalsIgnoreCase(_REFERENCE_DESIGNATOR))
      {
        stringToCheck = classifiedJoint.getReferenceDesignator();
      }
      else if(filterName.equalsIgnoreCase(_PANEL_NAME))
      {
        stringToCheck = classifiedJoint.getPanelName();
      }
      else if(filterName.equalsIgnoreCase(_PANEL_SERIAL_NUMBER))
      {
        stringToCheck = classifiedJoint.getPanelSerialNumber();
      }
      else if(filterName.equalsIgnoreCase(_PIN_NAME))
      {
        stringToCheck = classifiedJoint.getPinName();
      }
      else if(filterName.equalsIgnoreCase(_SYSTEM_CLASSIFICATION))
      {
        stringToCheck = classifiedJoint.getSystemClassification().toString();
      }
      else if(filterName.equalsIgnoreCase(_TRUE_CLASSIFICATION))
      {
        stringToCheck = classifiedJoint.getTrueClassification().toString();
      }
      else if(filterName.equalsIgnoreCase(_SUBTYPE_NUMBER))
      {
        stringToCheck = Integer.toString(classifiedJoint.getSubtypeNumber());
      }
      else if(filterName.equalsIgnoreCase(_SORTED_JOINT_NUMBER))
      {
        stringToCheck = Integer.toBinaryString(classifiedJoint.getSortedJointNumber());
      }
      else if(filterName.equalsIgnoreCase(_RESULT_FILE_NAME))
      {
        stringToCheck = classifiedJoint.getResultFileName();
      }
      else if(filterName.equalsIgnoreCase(_BOARD_NUMBER))
      {
        stringToCheck = Integer.toString(classifiedJoint.getBoardNumber());
      }
      else if(filterName.equalsIgnoreCase(_BOARD_SERIAL_NUMBER))
      {
        stringToCheck = classifiedJoint.getBoardSerialNumber();
      }
      else if(filterName.equalsIgnoreCase(_INSPECTION_START_TIME))
      {
        stringToCheck = classifiedJoint.getInspectionStartDateAndTime().toString();
      }
      else if(filterName.equals(_ALGORITHM_FAMILY_NAME))
      {
        stringToCheck = classifiedJoint.getAlgorithmFamilyName();
      }
      else if(filterName.equals(_PAD_SHAPE))
      {
        ShapeEnum padShape = classifiedJoint.getPadShape();
        if(padShape.equals(ShapeEnum.CIRCLE))
        {
          stringToCheck = "Circle";
        }
        else if(padShape.equals(ShapeEnum.RECTANGLE))
        {
          stringToCheck = "Rectangle";
        }
      }
      else if(filterName.equals(_VIEW_NUMBER))
      {
        stringToCheck = Integer.toString(classifiedJoint.getViewNumber());
      }
      else if(filterName.equalsIgnoreCase(_REVIEW_NEEDED))
      {
        if(classifiedJoint.isReviewNeeded())
        {
          stringToCheck = "yes";
        }
        else
        {
          stringToCheck = "no";
        }
      }
      else if(filterName.equalsIgnoreCase(_NUMBER_OF_TIMES_REVIEWED))
      {
        stringToCheck = Integer.toString(classifiedJoint.getNumberOfTimesReviewed());
      }
      else
      {
        //this should not happen
        Assert.expect(false);
      }

      accept = accept && filterData.accept(stringToCheck);
    }

    return accept;
  }

  /**
   * @author George A. David
   */
  public static Filter load(String filterFilePath) throws DatastoreException
  {
    FilterReader filterReader = new FilterReader();
    return filterReader.readFilter(filterFilePath);
  }

  /**
   * @author Erica Wheatcroft
   */
  public static Filter getDefaultFilter()
  {
    Filter defaultFilter = new Filter();

    FilterData trueClassificationFilterData = new FilterData();
    trueClassificationFilterData.setFilterName(_TRUE_CLASSIFICATION);
    trueClassificationFilterData.setSortEnum(SortEnum.ASCENDING);
    trueClassificationFilterData.setFilterString("");
    defaultFilter.addFilterData(trueClassificationFilterData);

    FilterData panelNameFilterData = new FilterData();
    panelNameFilterData.setFilterName(_PANEL_NAME);
    panelNameFilterData.setSortEnum(SortEnum.NONE);
    panelNameFilterData.setFilterString("");
    defaultFilter.addFilterData(panelNameFilterData);

    FilterData resultFileNameFilterData = new FilterData();
    resultFileNameFilterData.setFilterName(_RESULT_FILE_NAME);
    resultFileNameFilterData.setSortEnum(SortEnum.NONE);
    resultFileNameFilterData.setFilterString("");
    defaultFilter.addFilterData(resultFileNameFilterData);

    FilterData referenceDesignatorFilterData = new FilterData();
    referenceDesignatorFilterData.setFilterName(_REFERENCE_DESIGNATOR);
    referenceDesignatorFilterData.setSortEnum(SortEnum.ASCENDING);
    referenceDesignatorFilterData.setFilterString("");
    defaultFilter.addFilterData(referenceDesignatorFilterData);

    FilterData pinNameFilterData = new FilterData();
    pinNameFilterData.setFilterName(_PIN_NAME);
    pinNameFilterData.setSortEnum(SortEnum.NONE);
    pinNameFilterData.setFilterString("");
    defaultFilter.addFilterData(pinNameFilterData);

    FilterData subtypeNumberFilterData = new FilterData();
    subtypeNumberFilterData.setFilterName(_SUBTYPE_NUMBER);
    subtypeNumberFilterData.setSortEnum(SortEnum.NONE);
    subtypeNumberFilterData.setFilterString("");
    defaultFilter.addFilterData(subtypeNumberFilterData);

    FilterData reviewNeededFilterData = new FilterData();
    reviewNeededFilterData.setFilterName(_REVIEW_NEEDED);
    reviewNeededFilterData.setFilterString("Yes");
    reviewNeededFilterData.setSortEnum(SortEnum.NONE);
    defaultFilter.addFilterData(reviewNeededFilterData);

    return defaultFilter;
  }

  public static List getDefaultFilterFieldList()
  {
    List defaultFilterFieldList = new ArrayList();

    defaultFilterFieldList.add(_TRUE_CLASSIFICATION);
    defaultFilterFieldList.add(_PANEL_NAME);
    defaultFilterFieldList.add(_RESULT_FILE_NAME);
    defaultFilterFieldList.add(_REFERENCE_DESIGNATOR);
    defaultFilterFieldList.add(_PIN_NAME);
    defaultFilterFieldList.add(_SUBTYPE_NUMBER);
    defaultFilterFieldList.add(_REVIEW_NEEDED);

    return defaultFilterFieldList;
  }

  public static Set getAvailableFilters()
  {
    Set availableFilters = new TreeSet(new CaseInsensitiveStringComparator());
    availableFilters.add(_PANEL_NAME);
    availableFilters.add(_PANEL_SERIAL_NUMBER);
    availableFilters.add(_REFERENCE_DESIGNATOR);
    availableFilters.add(_PIN_NAME);
    availableFilters.add(_SYSTEM_CLASSIFICATION);
    availableFilters.add(_TRUE_CLASSIFICATION);
    availableFilters.add(_SUBTYPE_NUMBER);
    availableFilters.add(_SORTED_JOINT_NUMBER);
    availableFilters.add(_RESULT_FILE_NAME);
    availableFilters.add(_BOARD_NUMBER);
    availableFilters.add(_BOARD_SERIAL_NUMBER);
    availableFilters.add(_INSPECTION_START_TIME);
    availableFilters.add(_ALGORITHM_FAMILY_NAME);
    availableFilters.add(_PAD_SHAPE);
    availableFilters.add(_VIEW_NUMBER);
    availableFilters.add(_REVIEW_NEEDED);
    availableFilters.add(_NUMBER_OF_TIMES_REVIEWED);

    return availableFilters;
  }
}