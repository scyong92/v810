package proto.classifier.com.agilent.xRayTest.datastore.classifierTool;

import java.io.*;
import java.util.*;

import com.agilent.util.*;
import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.datastore.ndfReaders.*;
import com.agilent.xRayTest.datastore.panelDesc.*;


/**
 * This class reads an ascii database file. The file has the follwoing format:
 * <reference designator> <pin number> <system classification> <true classification> <f1> ... <fn>
 * <subtype number> <sorted joint number> <result file name> <panel name> <board number>
 * <panel serial number> <board serial number> <date and time> <algorithm family name>
 * <x coordinate> <y coordinate> <width> <length> <pad shape> <view number> <review needed> <review count>
 *
 * f1 - fn are measurments. For this proto code, we will only be dealing with the FPGullwing algoirthm family.
 * This algorithm contains 27 measurments. This number will change depending on the algorithm family.
 * @author George A. David
 */
class AsciiDatabaseReader
{
  private AsciiDatabaseReader _instance;
  private int _FPGULLWING_NUMBER_OF_MEASUREMENTS = 27;
  private String _currentFileName = null;
  private LineNumberReader _is = null;
  private String _referenceDesignator = null;

  /**
   * @author George A. David
   */
  void parseFile(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);

    _currentFileName = fileName;

    try
    {
      _is = ParseUtil.openFile(_currentFileName);
      String line = ParseUtil.readNextLine(_is, _currentFileName);

      while(line != null)
      {
        StringTokenizer st = new StringTokenizer(line, " \t\n\r\f", false);

        String token = ParseUtil.getNextToken(st);
        String referenceDesignator = parseReferenceDesignator(token);

        token = ParseUtil.getNextToken(st);
        String pinName = parsePinName(token);

        token = ParseUtil.getNextToken(st);
        JointClassificationEnum systemClassification = parseSystemClassification(token);

        token = ParseUtil.getNextToken(st);
        JointClassificationEnum trueClassification = parseTrueClassification(token);

        List measurements = parseMeasurements(st);

        token = ParseUtil.getNextToken(st);
        int subtypeNumber = parseSubtypeNumber(token);

        token = ParseUtil.getNextToken(st);
        int sortedJointNumber = parseSortedJointNumber(token);

        token = ParseUtil.getNextToken(st);
        String resultFileName = parseResutlFileName(token);

        token = ParseUtil.getNextToken(st);
        String panelName = parsePanelName(token);

        token = ParseUtil.getNextToken(st);
        int boardNumber = parseBoardNumber(token);

        token = ParseUtil.getNextToken(st);
        String panelSerialNumber = parsePanelSerialNumber(token);

        token = ParseUtil.getNextToken(st);
        String boardSerialNumber = parseBoardSerialNumber(token);

        token = ParseUtil.getNextToken(st);
        Calendar inspectionStartDateAndTime = parseInspectionStartDateAndTime(token);

        token = ParseUtil.getNextToken(st);
        String algorithmFamilyName = parseAlgorithmFamilyName(token);

        IntCoordinate coordinate = parseCoordinate(st);

        token = ParseUtil.getNextToken(st);
        int width = parseWidth(token);

        token = ParseUtil.getNextToken(st);
        int length = parseLength(token);

        token = ParseUtil.getNextToken(st);
        ShapeEnum padShape = parsePadShape(token);

        token = ParseUtil.getNextToken(st);
        int viewNumber = parseViewNumber(token);

        token = ParseUtil.getNextToken(st);
        boolean reviewNeeded = parseReviewNeeded(token);

        token = ParseUtil.getNextToken(st);
        int numberOfTimesReviewed = parseNumberOfTimesReviewed(token);

        ClassifiedJoint classifiedJoint = new ClassifiedJoint();
        classifiedJoint.setAlgorithmFamilyName(algorithmFamilyName);
        classifiedJoint.setBoardNumber(boardNumber);
        classifiedJoint.setBoardSerialNumber(boardSerialNumber);
        classifiedJoint.setCoordinate(coordinate);
        classifiedJoint.setInspectionStartDateAndTime(inspectionStartDateAndTime);
        classifiedJoint.setLength(length);
        classifiedJoint.setMeasurments(measurements);
        classifiedJoint.setNumberOfTimeReviewed(numberOfTimesReviewed);
        classifiedJoint.setPanelName(panelName);
        classifiedJoint.setPanelSerialNumber(panelSerialNumber);
        classifiedJoint.setPinName(pinName);
        classifiedJoint.setReferenceDesignator(referenceDesignator);
        classifiedJoint.setResultFileName(resultFileName);
        classifiedJoint.setReviewNeeded(reviewNeeded);
        classifiedJoint.setLastSaveReviewNeeded(reviewNeeded);
        classifiedJoint.setPadShape(padShape);
        classifiedJoint.setSortedJointNumber(sortedJointNumber);
        classifiedJoint.setSubtypeNumber(subtypeNumber);
        classifiedJoint.setSystemClassification(systemClassification);
        classifiedJoint.setTrueClassification(trueClassification);
        classifiedJoint.setViewNumber(viewNumber);
        classifiedJoint.setWidth(width);
        classifiedJoint.getInspectionStartDateAndTimeString();

        if(ClassifiedJointLibrary.containsClassifiedJoint(classifiedJoint))
        {
          throw new DuplicateClassifiedJointDatastoreException(_currentFileName,
                                                               _is.getLineNumber(),
                                                               classifiedJoint.getReferenceDesignator(),
                                                               classifiedJoint.getPinName(),
                                                               classifiedJoint.getResultFileName());
        }

        ClassifiedJointLibrary.addClassifiedJoint(classifiedJoint);

        line = ParseUtil.readNextLine(_is, _currentFileName);
      }
    }
    finally
    {
      if(_is != null)
      {
        try
        {
          _is.close();
        }
        catch (IOException ex)
        {
          // do nothing
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  private String parseReferenceDesignator(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<reference designator>");
    }
    _referenceDesignator = token;
    return token;
  }

  /**
   * @author George A. David
   */
  private String parsePinName(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<pin number>");
    }

    return token;
  }

  /**
   * @author George A. David
   */
  private JointClassificationEnum parseSystemClassification(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<system classification>");
    }

    int systemClassification = -1;
    try
    {
      systemClassification = StringUtil.convertStringToInt(token, 0, 1);
    }
    catch(BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "<system classification>",
                                              token);
    }

    JointClassificationEnum jointClassification = null;
    if(systemClassification == 0)
    {
      jointClassification = JointClassificationEnum.GOOD;
    }
    else if(systemClassification == 1)
    {
      jointClassification = JointClassificationEnum.BAD;
    }
    else
    {
      //this should not happen
      Assert.expect(false, "Bad system classification specified: " + systemClassification);
    }

    return jointClassification;
  }

  /**
   * @author George A. David
   */
  private JointClassificationEnum parseTrueClassification(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<true classification>");
    }

    int trueClassification = -1;
    try
    {
      trueClassification = StringUtil.convertStringToInt(token, 0, 1);
    }
    catch(BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "<true classification>",
                                              token);
    }

    JointClassificationEnum jointClassification = null;
    if(trueClassification == 0)
    {
      jointClassification = JointClassificationEnum.GOOD;
    }
    else if(trueClassification == 1)
    {
      jointClassification = JointClassificationEnum.BAD;
    }
    else
    {
      //this should not happen
      Assert.expect(false, "Bad true classification specified: " + trueClassification);
    }

    return jointClassification;
  }

  /**
   * @author George A. David
   */
  private List parseMeasurements(StringTokenizer st) throws DatastoreException
  {
    Assert.expect(st != null);
    List measurements = new ArrayList();

    for(int i = 0; i < _FPGULLWING_NUMBER_OF_MEASUREMENTS; i++)
    {
      String token = ParseUtil.getNextToken(st);
      double measurement = parseMeasurement(token);

      measurements.add(i, new Double(measurement));
    }

    return measurements;
  }

  /**
   * @author George A. David
   */
  private int parseSubtypeNumber(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<subtype number>");
    }

    int subtypeNumber = -1;
    try
    {
      subtypeNumber = StringUtil.convertStringToInt(token, 0, 255);
    }
    catch(BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "<subtype number (0 - 255)>",
                                              token);
    }


    return subtypeNumber;
  }

  /**
   * @author George A. David
   */
  private int parseSortedJointNumber(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<sorted joint number>");
    }

    int sortedJointNumber = -1;
    try
    {
      sortedJointNumber = StringUtil.convertStringToPositiveInt(token);
    }
    catch(BadFormatException bfe)
    {
      /**@todo should this be ExpectedPositiveIntException??? */
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "<sorted joint number>",
                                              token);
    }
    return sortedJointNumber;
  }

  /**
   * @author George A. David
   */
  private String parseResutlFileName(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<result file name>");
    }

    return token;
  }

  /**
   * @author George A. David
   */
  private String parsePanelName(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<panel name>");
    }

    return token;
  }

  /**
   * @author George A. David
   */
  private int parseBoardNumber(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<board number>");
    }

    int boardNumber = -1;
    try
    {
      boardNumber = StringUtil.convertStringToPositiveInt(token);
    }
    catch(BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "<board number>",
                                              token);
    }

    return boardNumber;
  }

  /**
   * @author George A. David
   */
  private String parsePanelSerialNumber(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<panel serial number>");
    }

    return token;
  }

  /**
   * @author George A. David
   */
  private String parseBoardSerialNumber(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<board serial number>");
    }

    return token;
  }

  /**
   * @author George A. David
   */
  private Calendar parseInspectionStartDateAndTime(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<date and time>");
    }
    else if(token.length() != 14)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "<date and time>",
                                              token);
    }

    String subString = token.substring(0, 4);
    int year = parseYear(subString);

    subString = token.substring(4, 6);
    int month = parseMonth(subString);

    subString = token.substring(6, 8);
    int day = parseDay(subString);

    subString = token.substring(8, 10);
    int hourOfDay= parseHour(subString);

    subString = token.substring(10, 12);
    int minutes = parseMinutes(subString);

    subString = token.substring(12);
    int seconds = parseSeconds(subString);

    Calendar dateAndTime = new GregorianCalendar(year, month - 1, day, hourOfDay, minutes, seconds);

    return dateAndTime;
  }

  /**
   * @author George A. David
   */
  private String parseAlgorithmFamilyName(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<algorithm family name>");
    }
    else if(token.equalsIgnoreCase("FPGULLWING") == false)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "FPGULLWING",
                                              token);
    }

    return token;
  }

  /**
   * @author George A. David
   */
  private IntCoordinate parseCoordinate(StringTokenizer st) throws DatastoreException
  {
    Assert.expect(st != null);

    String token = ParseUtil.getNextToken(st);
    int xCoordinate = parseXCoordinate(token);

    token = ParseUtil.getNextToken(st);
    int yCoordinate = parseYCoordinate(token);

    IntCoordinate coordinate = new IntCoordinate(xCoordinate, yCoordinate);

    return coordinate;
  }

  /**
   * @author George A. David
   */
  private int parseXCoordinate(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<x coordinate>");
    }

    int xCoordinate = -1;
    try
    {
      xCoordinate = StringUtil.convertStringToInt(token);
    }
    catch(BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "<x coordinate>",
                                              token);
    }

    return xCoordinate;
  }

  /**
   * @author George A. David
   */
  private int parseYCoordinate(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<y coordinate>");
    }

    int yCoordinate = -1;
    try
    {
      yCoordinate = StringUtil.convertStringToInt(token);
    }
    catch(BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "<y coordinate>",
                                              token);
    }

    return yCoordinate;
  }

  /**
   * @author George A. David
   */
  private int parseWidth(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<width>");
    }

    int width = -1;
    try
    {
      width = StringUtil.convertStringToPositiveInt(token);
    }
    catch(BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "<width>",
                                              token);
    }

    return width;
  }

  /**
   * @author George A. David
   */
  private int parseLength(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<length>");
    }

    int length = -1;
    try
    {
      length = StringUtil.convertStringToPositiveInt(token);
    }
    catch (BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "<length>",
                                              token);
    }

    return length;
  }

  /**
   * @author George A. David
   */
  private ShapeEnum parsePadShape(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<pad shape>");
    }

    int shapeInteger = -1;
    try
    {
      shapeInteger = StringUtil.convertStringToInt(token, 0, 1);
    }
    catch (BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "<pad shape>",
                                              token);
    }

    ShapeEnum padShape = null;
    if(shapeInteger == 0)
    {
      padShape = ShapeEnum.RECTANGLE;
    }
    else if(shapeInteger == 1)
    {
      padShape = ShapeEnum.CIRCLE;
    }
    else
    {
      // this should not happen
      Assert.expect(false);
    }

    return padShape;
  }

  /**
   * @author George A. David
   */
  private int parseViewNumber(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<view number>");
    }

    int viewNumber = -1;
    try
    {
      viewNumber = StringUtil.convertStringToPositiveInt(token);
    }
    catch(BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "<view number>",
                                              token);
    }

    return viewNumber;
  }

  /**
   * @author George A. David
   */
  private boolean parseReviewNeeded(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<review needed>");
    }

    int reviewNeededInteger = -1;
    try
    {
      reviewNeededInteger = StringUtil.convertStringToInt(token, 0, 1);
    }
    catch(BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "<review needed>",
                                              token);
    }

    boolean reviewNeeded = false;
    if(reviewNeededInteger == 0)
    {
      reviewNeeded = false;
    }
    else if(reviewNeededInteger == 1)
    {
      reviewNeeded = true;
    }
    else
    {
      //this should not happen
      Assert.expect(false);
    }

    return reviewNeeded;
  }

  /**
   * @author George A. David
   */
  private int parseNumberOfTimesReviewed(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<review count>");
    }

    int reviewCount = -1;
    try
    {
      reviewCount = StringUtil.convertStringToPositiveInt(token);
    }
    catch(BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "<review count>",
                                              token);
    }

    return reviewCount;
  }

  /**
   * @author George A. David
   */
  AsciiDatabaseReader getInstance()
  {
    if(_instance == null)
      _instance = new AsciiDatabaseReader();

    return _instance;
  }

  /**
   * @author George A. David
   */
  private double parseMeasurement(String token) throws DatastoreException
  {
    if(token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFileName,
                                                _is.getLineNumber(),
                                                "<f1> ... <fn>");
    }

    double measurement = -1;
    try
    {
      measurement = StringUtil.convertStringToDouble(token);
    }
    catch(BadFormatException bfe)
    {
      throw new ExpectedNumberDatastoreException(_currentFileName,
                                                 _is.getLineNumber(),
                                                 token);
    }
    return measurement;
  }

  /**
   * @author George A. David
   */
  private int parseYear(String token) throws DatastoreException
  {
    Assert.expect(token != null && token.length() == 4);

    int year = -1;
    try
    {
      /** @todo should I check that this is greater than 2001? */
      year = StringUtil.convertStringToPositiveInt(token);
    }
    catch(BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "4 digit year",
                                              token);
    }

    return year;
  }

  /**
   * @author George A. David
   */
  private int parseMonth(String token) throws DatastoreException
  {
    Assert.expect(token != null && token.length() == 2);

    int month = -1;
    try
    {
      month = StringUtil.convertStringToInt(token, 1, 12);
    }
    catch(BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "2 digit month",
                                              token);
    }

    return month;
  }

  /**
   * @author George A. David
   */
  private int parseDay(String token) throws DatastoreException
  {
    Assert.expect(token != null && token.length() == 2);

    int day = -1;
    try
    {
      day = StringUtil.convertStringToInt(token, 1, 31);
    }
    catch(BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "2 digit day",
                                              token);
    }

    return day;
  }

  /**
   * @author George A. David
   */
  private int parseHour(String token) throws DatastoreException
  {
    Assert.expect(token != null && token.length() == 2);

    int hour = -1;
    try
    {
      hour = StringUtil.convertStringToInt(token, 0, 23);
    }
    catch(BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "2 digit hour",
                                              token);
    }

    return hour;
  }

  /**
   * @author George A. David
   */
  private int parseMinutes(String token) throws DatastoreException
  {
    Assert.expect(token != null && token.length() == 2);

    int minutes = -1;
    try
    {
      minutes = StringUtil.convertStringToInt(token, 0, 59);
    }
    catch(BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "2 digit minuted",
                                              token);
    }

    return minutes;
  }

  /**
   * @author George A. David
   */
  private int parseSeconds(String token) throws DatastoreException
  {
    Assert.expect(token != null && token.length() == 2);

    int seconds = -1;
    try
    {
      seconds = StringUtil.convertStringToInt(token, 0, 59);
    }
    catch(BadFormatException bfe)
    {
      throw new FileCorruptDatastoreException(_currentFileName,
                                              _is.getLineNumber(),
                                              "2 digit second",
                                              token);
    }

    return seconds;
  }
}
