package proto.classifier.com.agilent.xRayTest.datastore.classifierTool;

import com.agilent.xRayTest.datastore.*;
import com.agilent.util.*;
/**
 * This class get thrown when duplicate classified joints are found.
 * @author George A. David
 */
public class DuplicateClassifiedJointDatastoreException extends DatastoreException
{
  /**
   * @author George A. David
   */
  public DuplicateClassifiedJointDatastoreException(String fileName,
                                                    int lineNumber,
                                                    String componentName,
                                                    String pinName,
                                                    String resultFileName)
  {
    super(new LocalizedString("DS_ERROR_DUPLICATE_CLASSIFIED_JOINT_KEY",
          new Object[]{fileName,
                       new Integer(lineNumber),
                       componentName,
                       pinName,
                       resultFileName}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(componentName != null);
    Assert.expect(pinName != null);
    Assert.expect(resultFileName != null);
  }
}
