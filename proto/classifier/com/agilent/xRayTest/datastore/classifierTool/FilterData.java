package proto.classifier.com.agilent.xRayTest.datastore.classifierTool;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.agilent.util.*;

/**
 * @author George A. David
 */
public class FilterData implements Serializable
{
  private String _filterName = null;
  private String _filterString = null;
  private SortEnum _sortEnum = null;

  /**
   * @author George A. David
   */
  public void setFilterName(String filterName)
  {
    Assert.expect(filterName != null);

    _filterName = filterName;
  }

  /**
   * @author George A. David
   */
  public String getFilterName()
  {
    Assert.expect(_filterName != null);

    return _filterName;
  }

  /**
   * @author George A. David
   */
  public void setFilterString(String filterString)
  {
    Assert.expect(filterString != null);

    _filterString = filterString;
  }

  /**
   * @author George A. David
   */
  public String getFilterString()
  {
    Assert.expect(_filterString != null);

    return _filterString;
  }

  /**
   * @author George A. David
   */
  public void setSortEnum(SortEnum sortEnum)
  {
    Assert.expect(sortEnum != null);

    _sortEnum = sortEnum;
  }

  /**
   * @author George A. David
   */
  public SortEnum getSortEnum()
  {
    Assert.expect(_sortEnum != null);

    return _sortEnum;
  }

  /**
   * @author George A. David
   */
  public boolean accept(String stringToCheck)
  {
    Assert.expect(stringToCheck != null);
    //this code should probably be moved to StringUtil
    String regularExpression = "";
    if(_filterString.length() == 0)
    {
      regularExpression = ".*";
    }
    else
    {
      for(int index = 0; index < _filterString.length(); ++index)
      {
        String subString = _filterString.substring(index, index + 1);
        if(subString.equals("*"))
        {
          subString = ".*";
        }
        else if(subString.equals("?"))
        {
          subString = ".";
        }
        regularExpression += subString;
      }
    }

    Pattern pattern = Pattern.compile(regularExpression, Pattern.CASE_INSENSITIVE);
    Matcher matcher = pattern.matcher(stringToCheck);

    return matcher.matches();
  }

}