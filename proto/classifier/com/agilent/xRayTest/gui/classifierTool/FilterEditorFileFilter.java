package proto.classifier.com.agilent.xRayTest.gui.classifierTool;

import java.io.*;
import javax.swing.filechooser.*;

import com.agilent.xRayTest.datastore.*;
import com.agilent.util.*;

/**
 * @author George A. David
 */
class FilterEditorFileFilter extends javax.swing.filechooser.FileFilter
{
  /**
   * @author George A. David
   */
  public boolean accept(File file)
  {
    Assert.expect(file != null);

    boolean accept = false;

    if(file.isDirectory() == false)
    {
      String fileName = file.getName().toLowerCase();
      accept = fileName.endsWith(".filter");
    }
    else
      accept = true;

    return accept;
  }

  /**
   * @author George A. David
   */
  public String getDescription()
  {
    return "Filter Editor Filter Files (*.filter)";
  }
}