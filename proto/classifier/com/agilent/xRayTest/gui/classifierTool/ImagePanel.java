package proto.classifier.com.agilent.xRayTest.gui.classifierTool;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.util.*;

import javax.swing.*;


import com.agilent.xRayTest.datastore.panelDesc.*;
import com.agilent.guiUtil.*;
import com.agilent.util.*;
import proto.classifier.com.agilent.xRayTest.datastore.classifierTool.*;

/**
 * @author George A. David
 */
public class ImagePanel extends JPanel
{
  private JScrollPane _scrollPanel = new JScrollPane();
  private JLabel _imageLabel = new JLabel();
  private boolean _drawGraphics = true;
  private java.awt.Shape _shape = new Rectangle2D.Double();
  boolean _isImageLoaded = true;
  boolean _isZoomedIn = false;
  private Color _jointBoxColor = null;
  private ImageIcon _image = null;
  private static int _WIDTH = 512;
  private static int _HEIGHT = 512;
  private static int _ZOOM_FACTOR = 2;
  private static int _IMAGE_WIDTH = 1024;
  private static int _IMAGE_HEIGHT = 1024;
  private static int _VISIBLE_RECTANGLE_WIDTH = 274;
  private static int _VISIBLE_RECTANGLE_HEIGHT = 274;

  /**
   * @author George A. David
   */
  public ImagePanel(ClassifiedJoint classifiedJoint, int currentImageIndex)
  {
    Assert.expect(classifiedJoint != null);
    Assert.expect(currentImageIndex >= 0);

    JointClassificationEnum jointClassification = classifiedJoint.getTrueClassification();
    if(jointClassification.equals(JointClassificationEnum.BAD))
    {
      _jointBoxColor = ImageIconPanel._BAD_JOINT_COLOR;
    }
    else if(jointClassification.equals(JointClassificationEnum.GOOD))
    {
      _jointBoxColor = ImageIconPanel._GOOD_JOINT_COLOR;
    }
    else
    {
      Assert.expect(false);
    }

    java.util.List imageFileNames = classifiedJoint.getViewFileNames();
    Assert.expect(currentImageIndex < imageFileNames.size());
    _image = new ImageIcon((String)imageFileNames.get(currentImageIndex));
    _imageLabel.setIcon(_image);
    add(_imageLabel);
    setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
    setUpGraphics(classifiedJoint);
  }

  /**
   * @author George A. David
   */
  private void setUpGraphics(ClassifiedJoint classifiedJoint)
  {
    Assert.expect(classifiedJoint != null);

    ShapeEnum padShape = classifiedJoint.getPadShape();
    IntCoordinate coordinate = classifiedJoint.getCoordinate();
    Point2D topLeft = new Point2D.Double(coordinate.getX() - (classifiedJoint.getWidth() / 2.0),
                                         coordinate.getY() - (classifiedJoint.getLength() / 2.0));
    if(padShape.equals(ShapeEnum.CIRCLE))
    {
      _shape = new Ellipse2D.Double(topLeft.getX(),
                                    topLeft.getY(),
                                    classifiedJoint.getWidth(),
                                    classifiedJoint.getLength());
    }
    else if(padShape.equals(ShapeEnum.RECTANGLE))
    {
      _shape = new Rectangle2D.Double(topLeft.getX(),
                                      topLeft.getY(),
                                      classifiedJoint.getWidth(),
                                      classifiedJoint.getLength());
    }
    else
    {
      Assert.expect(false);
    }
  }

  /**
   * @author George A. David
   */
  public void setDrawGraphics(boolean drawGraphics)
  {
    _drawGraphics = drawGraphics;
    repaint();
  }


  /**
   * @author George A. David
   */
  public void paint(Graphics graphics)
  {
    super.paint(graphics);
    Assert.expect(graphics != null);

    if(_isImageLoaded)
    {
      if(ImageIconPanel._drawJointGraphics)
      {
        Graphics2D graphics2D = (Graphics2D)graphics;
        Color previousColor = graphics2D.getColor();
        graphics2D.setColor(_jointBoxColor);

        if(_isZoomedIn)
        {
          graphics2D.scale(_ZOOM_FACTOR, _ZOOM_FACTOR);
        }
        graphics2D.draw(_shape);
        graphics2D.setColor(previousColor);
      }
    }
  }

  /**
   * @author George A. David
   */
  public void zoomOut()
  {
    if(_isZoomedIn)
    {
      _imageLabel.setIcon(_image);
      _isZoomedIn = false;
      repaint();
    }
  }

  /**
   * @author George A. David
   */
  public void zoomIn()
  {
    if(_isZoomedIn == false)
    {
      BufferedImage bufferedImage = new BufferedImage(_IMAGE_WIDTH * _ZOOM_FACTOR,
                                                      _IMAGE_HEIGHT * _ZOOM_FACTOR,
                                                      BufferedImage.TYPE_BYTE_GRAY);
      Graphics2D graphics = (Graphics2D)bufferedImage.getGraphics();
      graphics.scale(_ZOOM_FACTOR, _ZOOM_FACTOR);
      _image.paintIcon(null, graphics, 0, 0);
      _imageLabel.setIcon(new ImageIcon(bufferedImage));
      _isZoomedIn = true;
      repaint();
    }
  }

  /**
   * @author George A. David
   */
  public void showFullImage(JFrame frame)
  {
    zoomOut();
    Rectangle visibleRectangle = _shape.getBounds();
    visibleRectangle.setSize( _VISIBLE_RECTANGLE_WIDTH,
                              _VISIBLE_RECTANGLE_HEIGHT);
    showImage(frame, visibleRectangle);
  }

  /**
   * @author George A. David
   */
  public void showZoomedImage(JFrame frame)
  {
    zoomIn();
    Rectangle bounds = _shape.getBounds();
    Rectangle visibleRectangle = new Rectangle(bounds.x * _ZOOM_FACTOR,
                                               bounds.y * _ZOOM_FACTOR,
                                               _VISIBLE_RECTANGLE_WIDTH,
                                               _VISIBLE_RECTANGLE_HEIGHT);
    showImage(frame, visibleRectangle);
  }

  /**
   * @author George A. David
   */
  private void showImage(JFrame frame, Rectangle visibleRectangle)
  {
    Assert.expect(frame != null);
    Assert.expect(visibleRectangle != null);

    JScrollPane scrollPane = new JScrollPane(this);
    Dimension dimension = new Dimension(_WIDTH, _HEIGHT);
    scrollPane.setPreferredSize(dimension);
    final JDialog dialog = new JDialog(frame, "", true);
    dialog.getContentPane().add(scrollPane);
    dialog.addKeyListener(new KeyListener()
    {
      public void keyPressed(KeyEvent e)
      {
        if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
          dialog.dispose();
        }
      }
      public void keyReleased(KeyEvent e){}
      public void keyTyped(KeyEvent e){}

    });
    dialog.pack();
    scrollRectToVisible(visibleRectangle);
    SwingUtils.centerOnComponent(dialog, frame);
    dialog.show();
  }
}
