package proto.classifier.com.agilent.xRayTest.gui.classifierTool;

import java.awt.*;

import javax.swing.UIManager;

import com.agilent.xRayTest.util.*;

/**
 * @author George A. David
 */
public class ClassifierToolGUI
{
  private boolean packFrame = false;

  /**
   * @author George A. David
   */
  public ClassifierToolGUI()
  {
    ClassifierToolFrame frame = ClassifierToolFrame.getInstance();
    AssertUtil.setUpAssert("classifierTool", frame);

    //Validate frames that have preset sizes
    //Pack frames that have useful preferred size info, e.g. from their layout
    if (packFrame)
    {
      frame.pack();
    }
    else
    {
      frame.validate();
    }
    //Center the window
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = frame.getSize();
    if (frameSize.height > screenSize.height)
    {
      frameSize.height = screenSize.height;
    }
    if (frameSize.width > screenSize.width)
    {
      frameSize.width = screenSize.width;
    }
    frame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
    frame.setVisible(true);
  }

  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    try
    {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    new ClassifierToolGUI();
  }
}