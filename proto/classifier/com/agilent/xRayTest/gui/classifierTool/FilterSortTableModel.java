package proto.classifier.com.agilent.xRayTest.gui.classifierTool;

import java.util.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;

import com.agilent.xRayTest.datastore.panelDesc.*;
import com.agilent.guiUtil.*;
import com.agilent.util.*;
import proto.classifier.com.agilent.xRayTest.datastore.classifierTool.*;

/**
 * @author George A. David
 */
class FilterSortTableModel extends DefaultSortTableModel
{
  private JTable _table = null;

  /**
   * @author George A. David
   */
  public FilterSortTableModel(Object[][]data, Object[] names)
  {
    super(data, names);
    Assert.expect(data != null);
    Assert.expect(names != null);
  }

  /**
   * @author George A. David
   */
  void setTable(JTable table)
  {
    Assert.expect(table != null);

    _table = table;
  }

  /**
   * @author George A. David
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0 && rowIndex < _table.getRowCount());
    Assert.expect(columnIndex >= 0 && columnIndex < _table.getColumnCount());
    return false;
  }

  /**
   * @author George A. David
   */
  public void sortColumn(int column, boolean ascending)
  {
    Assert.expect(_table != null);
    Assert.expect(column >= 0 && column < _table.getColumnCount());

    List selectedJoints = getSelectedClassifiedJoints();
    Collections.sort(getDataVector(),
                     new AlphaNumericColumnComparator(column, ascending, _table.getColumnName(column)));
    selectRows(selectedJoints, true);
  }

  /**
   * @author George A. David
   */
  public Object getValueAt(int row, int column)
  {
    Assert.expect(_table != null);
    Assert.expect(row >= 0 && row < _table.getRowCount());
    Assert.expect(column >= 0 && column < _table.getColumnCount());

    Vector rowVector = (Vector)dataVector.elementAt(row);
    String columnName = getColumnName(column);
    ClassifiedJoint classifiedJoint = (ClassifiedJoint)rowVector.elementAt(column);
    Object value = null;
    if(columnName.equalsIgnoreCase(Filter._REFERENCE_DESIGNATOR))
    {
      value = classifiedJoint.getReferenceDesignator();
    }
    else if(columnName.equalsIgnoreCase(Filter._PANEL_NAME))
    {
      value = classifiedJoint.getPanelName();
    }
    else if(columnName.equalsIgnoreCase(Filter._PANEL_SERIAL_NUMBER))
    {
      value = classifiedJoint.getPanelSerialNumber();
    }
    else if(columnName.equalsIgnoreCase(Filter._PIN_NAME))
    {
      value = classifiedJoint.getPinName();
    }
    else if(columnName.equalsIgnoreCase(Filter._SYSTEM_CLASSIFICATION))
    {
      value = classifiedJoint.getSystemClassification().toString();
    }
    else if(columnName.equalsIgnoreCase(Filter._TRUE_CLASSIFICATION))
    {
      value = classifiedJoint.getTrueClassification().toString();
    }
    else if(columnName.equalsIgnoreCase(Filter._SUBTYPE_NUMBER))
    {
      value = Integer.toString(classifiedJoint.getSubtypeNumber());
    }
    else if(columnName.equalsIgnoreCase(Filter._SORTED_JOINT_NUMBER))
    {
      value = Integer.toBinaryString(classifiedJoint.getSortedJointNumber());
    }
    else if(columnName.equalsIgnoreCase(Filter._RESULT_FILE_NAME))
    {
      value = classifiedJoint.getResultFileName();
    }
    else if(columnName.equalsIgnoreCase(Filter._BOARD_NUMBER))
    {
      value = Integer.toString(classifiedJoint.getBoardNumber());
    }
    else if(columnName.equalsIgnoreCase(Filter._BOARD_SERIAL_NUMBER))
    {
      value = classifiedJoint.getBoardSerialNumber();
    }
    else if(columnName.equalsIgnoreCase(Filter._INSPECTION_START_TIME))
    {
      value = classifiedJoint.getInspectionStartDateAndTimeString();
    }
    else if(columnName.equals(Filter._ALGORITHM_FAMILY_NAME))
    {
      value = classifiedJoint.getAlgorithmFamilyName();
    }
    else if(columnName.equals(Filter._PAD_SHAPE))
    {
      ShapeEnum padShape = classifiedJoint.getPadShape();
      if(padShape.equals(ShapeEnum.CIRCLE))
      {
        value = "Circle";
      }
      else if(padShape.equals(ShapeEnum.RECTANGLE))
      {
        value = "Rectangle";
      }
    }
    else if(columnName.equals(Filter._VIEW_NUMBER))
    {
      value = Integer.toString(classifiedJoint.getViewNumber());
    }
    else if(columnName.equalsIgnoreCase(Filter._REVIEW_NEEDED))
    {
      if(classifiedJoint.isReviewNeeded())
        value = "yes";
      else
        value = "no";
    }
    else if(columnName.equalsIgnoreCase(Filter._NUMBER_OF_TIMES_REVIEWED))
    {
      value = Integer.toString(classifiedJoint.getNumberOfTimesReviewed());
    }
    else
    {
      //this should not happen
      Assert.expect(false);
    }
    return value;
  }

  /**
   * @author George A. David
   */
  List getSortedClassifiedJoints()
  {
    List sortedClassifiedJoints = new ArrayList();
    Iterator it = dataVector.iterator();
    while(it.hasNext())
    {
      Vector vector = (Vector)it.next();
      sortedClassifiedJoints.add(vector.get(0));
    }

    return sortedClassifiedJoints;
  }

  /**
   * @author George A. David
   */
  ClassifiedJoint getClassifiedJoint(int rowIndex)
  {
    Assert.expect(rowIndex >= 0 && rowIndex < _table.getRowCount());

    Vector rowVector = (Vector)dataVector.elementAt(rowIndex);
    ClassifiedJoint classifiedJoint = (ClassifiedJoint)rowVector.elementAt(0);

    return classifiedJoint;

  }

  /**
   * @author George A. David
   */
  List getClassifiedJoints(int[] rows)
  {
    Assert.expect(rows != null);

    List classifiedJoints = new ArrayList();
    for(int i = 0; i < rows.length; ++i)
    {
      ClassifiedJoint classifiedJoint = getClassifiedJoint(rows[i]);
      classifiedJoints.add(classifiedJoint);
    }

    return classifiedJoints;
  }

  /**
   * @author George A. David
   */
  List getSelectedClassifiedJoints()
  {
    int[] rows = _table.getSelectedRows();
    return getClassifiedJoints(rows);
  }

  /**
   * @author George A. David
   */
  boolean containsClassifiedJoint(ClassifiedJoint classifiedJoint)
  {
    Assert.expect(classifiedJoint != null);

    boolean foundClassifiedJoint = false;

    for(int rowIndex = 0; rowIndex < dataVector.size(); ++rowIndex)
    {
      Vector rowVector = (Vector)dataVector.elementAt(rowIndex);
      if(classifiedJoint == rowVector.elementAt(0))
      {
        foundClassifiedJoint = true;
        break;
      }
    }

    return foundClassifiedJoint;
  }

  /**
   * @author George A. David
   */
  void selectRow(ClassifiedJoint classifiedJoint, boolean clearPreviousSelections)
  {
    Assert.expect(classifiedJoint != null);

    int rowIndex = 0;
    for(rowIndex = 0; rowIndex < dataVector.size(); ++rowIndex)
    {
      Vector rowVector = (Vector)dataVector.elementAt(rowIndex);
      if(classifiedJoint == rowVector.elementAt(0))
        break;
    }

    if(clearPreviousSelections)
    {
      _table.setRowSelectionInterval(rowIndex, rowIndex);
    }
    else
    {
      _table.addRowSelectionInterval(rowIndex, rowIndex);
    }
    _table.changeSelection(rowIndex, 0, true, true);
  }

  /**
   * @author George A. David
   */
  private int getRowIndex(ClassifiedJoint classifiedJoint)
  {
    Assert.expect(classifiedJoint != null);

    int rowIndex = -1;
    for(rowIndex = 0; rowIndex < dataVector.size(); ++rowIndex)
    {
      Vector rowVector = (Vector)dataVector.elementAt(rowIndex);
      if(classifiedJoint == rowVector.elementAt(0))
        break;
    }
    Assert.expect(rowIndex < dataVector.size());

    return rowIndex;
  }

  /**
   * Given the classified joints, select them and any classified joints in between them
   * in the table.
   * @author George A. David
   */
  void selectRows(ClassifiedJoint firstClassifiedJoint, ClassifiedJoint secondClassifiedJoint)
  {
    Assert.expect(firstClassifiedJoint != null);
    Assert.expect(secondClassifiedJoint != null);
    int firstRowIndex = getRowIndex(firstClassifiedJoint);
    int secondRowIndex = getRowIndex(secondClassifiedJoint);
    if(firstRowIndex > secondRowIndex)
    {
      selectRows(secondRowIndex, firstRowIndex);
    }
    else
    {
      selectRows(firstRowIndex, secondRowIndex);
    }
  }

  /**
   * Give the row indicies, select them and the rows in between them
   * @author George A. David
   */
  private void selectRows(int beginIndex, int endIndex)
  {
    Assert.expect(beginIndex >= 0 &&
                  beginIndex < getRowCount());
    Assert.expect(endIndex >= 0 &&
                  endIndex < getRowCount());

    _table.clearSelection();
    _table.addRowSelectionInterval(beginIndex, endIndex);
    _table.changeSelection(endIndex, 0, true, true);
  }


  /**
   * @author George A. David
   */
  void selectRows(List classifiedJoints, boolean clearPreviousSelections)
  {
    Assert.expect(classifiedJoints != null);

    if(clearPreviousSelections)
    {
      _table.clearSelection();
    }

    Iterator it = classifiedJoints.iterator();
    while(it.hasNext())
    {
      ClassifiedJoint classifiedJoint = (ClassifiedJoint)it.next();
      selectRow(classifiedJoint, false);
    }
  }

  /**
   * @author George A. David
   */
  void clearSelectedRow(ClassifiedJoint classifiedJoint)
  {
    Assert.expect(classifiedJoint != null);

    int rowIndex = 0;
    for(rowIndex = 0; rowIndex < dataVector.size(); ++rowIndex)
    {
      Vector rowVector = (Vector)dataVector.elementAt(rowIndex);
      if(classifiedJoint == rowVector.elementAt(0))
        break;
    }
    _table.removeRowSelectionInterval(rowIndex, rowIndex);
    _table.changeSelection(rowIndex, 0, true, true);
  }

  /**
   * @author George A. David
   */
  void deleteSelectedRows()
  {
    List selectedJoints = getSelectedClassifiedJoints();
    Iterator it = selectedJoints.iterator();
    int rowIndex = -1;
    while(it.hasNext())
    {
      ClassifiedJoint classifiedJoint = (ClassifiedJoint)it.next();
      rowIndex = getRowIndex(classifiedJoint);
      dataVector.remove(rowIndex);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  void addColumn(String addedColumnName)
  {
    addColumn(addedColumnName);
  }

  /**
   * @author Erica Wheatcroft
   */
  void deleteColumn(String deletedColumnName)
  {
    Iterator columnIdentifiersIt = columnIdentifiers.iterator();
    while(columnIdentifiersIt.hasNext())
    {
      if(columnIdentifiersIt.next().toString().equalsIgnoreCase(deletedColumnName))
      {
        columnIdentifiersIt.remove();
        break;
      }
    }
  }
}