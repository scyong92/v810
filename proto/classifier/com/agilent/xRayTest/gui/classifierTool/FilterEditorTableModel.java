package proto.classifier.com.agilent.xRayTest.gui.classifierTool;

import java.util.*;

import javax.swing.table.*;

import com.agilent.util.*;
import proto.classifier.com.agilent.xRayTest.datastore.classifierTool.*;

/**
 * @author George A. David
 */
class FilterEditorTableModel extends AbstractTableModel
{
  private List _columnNames = new LinkedList();
  private Map _rowNumberToDisplayDataList = new HashMap();
  private final int _NUMBER_OF_ROWS = 2;

  /**
   * @author George A. David
   */
  FilterEditorTableModel()
  {
  }

  /**
   * @author George A. David
   */
  FilterEditorTableModel(List columnNames)
  {
    Assert.expect(columnNames != null);

    _columnNames = new ArrayList(columnNames);
  }

  /**
   * @author George A. David
   */
  public int getColumnCount()
  {
    return _columnNames.size();
  }

  /**
   * @author George A. David
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());

    return (String)_columnNames.get(columnIndex);
  }

  /**
   * @author George A. David
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0 && rowIndex < _NUMBER_OF_ROWS);
    Assert.expect(columnIndex >= 0 && columnIndex < _columnNames.size());

    Object value = null;
    Integer key = new Integer(rowIndex);
    if(_rowNumberToDisplayDataList.containsKey(key))
    {
      List displayDataList = (List)_rowNumberToDisplayDataList.get(new Integer(rowIndex));
      value = displayDataList.get(columnIndex);
      if(value instanceof String && ((String)value).equalsIgnoreCase("Not Specified"))
      {
        value = "";
      }
    }
    else
    {
      if(rowIndex == 0)
      {
        value = "";
      }
      else if (rowIndex == 1)
      {
        value = SortEnum.NONE;
      }
      else
      {
        //this should not happen
        Assert.expect(false);
      }
    }
    return value;
  }

  /**
   * This table only contains two rows, one to tell it how to filter the data,
   * another to tell it how to sort the data.
   * @author George A. David
   */
  public int getRowCount()
  {
    return _NUMBER_OF_ROWS;
  }

  /**
   * @author George A. David
   */
  public boolean containsColumnName(String columnName)
  {
    return _columnNames.contains(columnName);
  }

  /**
   * Adds the column name at the end of the list
   * @author George A. David
   */
  public void addColumnName(String columnName)
  {
    Assert.expect(columnName != null);
    Assert.expect(_columnNames.contains(columnName) == false,
                  "Attempted to create a duplicate column name, " +
                  "please use containsColumnName() prior to calling addColumnName()");

    addColumnName(columnName, "", SortEnum.NONE);

  }

  /**
   * Adds the column name at the end of the list
   * @author George A. David
   */
  public void addColumnName(String columnName, String filterString, SortEnum sortEnum)
  {
    Assert.expect(columnName != null);
    Assert.expect(filterString != null);
    Assert.expect(sortEnum != null);
    Assert.expect(_columnNames.contains(columnName) == false,
                  "Attempted to create a duplicate column name, " +
                  "please use containsColumnName() prior to calling addColumnName()");

    _columnNames.add(columnName);
    int columnIndex = _columnNames.indexOf(columnName);
    Object key = new Integer(0);
    List displayData = (List)_rowNumberToDisplayDataList.get(key);
    if(displayData == null)
    {
      displayData = new LinkedList();
      for(int index = 0; index < _columnNames.size(); ++ index)
      {
        displayData.add("");
      }
      _rowNumberToDisplayDataList.put(key, displayData);
    }
    else if(columnIndex >= displayData.size())
    {
      for(int index = displayData.size(); index < _columnNames.size(); ++index)
      {
        displayData.add("");
      }
    }
    displayData.set(columnIndex, filterString);
    key = new Integer(1);
    displayData = (List)_rowNumberToDisplayDataList.get(key);
    if(displayData == null)
    {
      displayData = new LinkedList();
      for(int index = 0; index < _columnNames.size(); ++ index)
      {
        displayData.add(SortEnum.NONE);
      }
      _rowNumberToDisplayDataList.put(key, displayData);
    }
    else if(columnIndex >= displayData.size())
    {
      for(int index = displayData.size(); index < _columnNames.size(); ++index)
      {
        displayData.add(SortEnum.NONE);
      }
    }
    displayData.set(columnIndex, sortEnum);
  }

  /**
   * @author George A. David
   */
  public void removeColumnName(String columnName)
  {
    Assert.expect(columnName != null);
    Assert.expect(_columnNames.contains(columnName) == true,
                  "Attempted to delete a column name that does not exist, " +
                  "please use containsColumnName() prior to calling remvoeColumnName()");

    int columnIndex = _columnNames.indexOf(columnName);
    _columnNames.remove(columnName);
    Set keys = _rowNumberToDisplayDataList.keySet();
    Iterator it = keys.iterator();
    while(it.hasNext())
    {
      Object key = it.next();
      List displayData = (List)_rowNumberToDisplayDataList.get(key);
      displayData.remove(columnIndex);
    }
  }

  /**
   * @author George A. David
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0 && rowIndex < _NUMBER_OF_ROWS);
    Assert.expect(columnIndex >= 0 && columnIndex < _columnNames.size());
    return true;
  }


  /**
   * @author George A. David
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    Assert.expect(aValue != null);
    Assert.expect(rowIndex >= 0 && rowIndex < _NUMBER_OF_ROWS);
    Assert.expect(columnIndex >= 0 && columnIndex < _columnNames.size());

    Integer key = new Integer(rowIndex);
    List displayDataList = null;
    if(_rowNumberToDisplayDataList.containsKey(key))
    {
      displayDataList = (List)_rowNumberToDisplayDataList.get(new Integer(rowIndex));
    }
    else
    {
      displayDataList = new LinkedList();
      for(int i = 0; i < _columnNames.size(); ++i)
      {
        if(rowIndex == 0)
        {
          displayDataList.add("");
        }
        else if (rowIndex == 1)
        {
          displayDataList.add(SortEnum.NONE);
        }
        else
        {
          //this should not happen
          Assert.expect(false);
        }
      }
      _rowNumberToDisplayDataList.put(key, displayDataList);
    }
    if(rowIndex == 1)
    {
      String valueString = (String)aValue;
      SortEnum sortEnum = null;
      if(valueString.equalsIgnoreCase(SortEnum.NONE.toString()))
      {
        sortEnum = SortEnum.NONE;
      }
      else if(valueString.equalsIgnoreCase(SortEnum.ASCENDING.toString()))
      {
        sortEnum = SortEnum.ASCENDING;
      }
      else if(valueString.equalsIgnoreCase(SortEnum.DESCENDING.toString()))
      {
        sortEnum = SortEnum.DESCENDING;
      }
      else
      {
        //this shoud not happen
        Assert.expect(false);
      }

      aValue = sortEnum;
    }

    displayDataList.set(columnIndex, aValue);
  }

  /**
   * @author George A. David
   */
  public int getColumnIndex(String columnName)
  {
    Assert.expect(columnName != null);
    Assert.expect(_columnNames.contains(columnName),
                  "Attempted to access a column name that does not exist, " +
                  "please use containsColumnName() prior to calling getColumnIndex()");

    return _columnNames.indexOf(columnName);
  }
}
