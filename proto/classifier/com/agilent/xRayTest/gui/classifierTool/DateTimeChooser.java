package proto.classifier.com.agilent.xRayTest.gui.classifierTool;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import com.agilent.guiUtil.*;

import com.agilent.util.*;

import proto.classifier.com.agilent.guiUtil.classifierGuiUtil.*;;

/**
 * @author George A. David
 */
public class DateTimeChooser extends JDialog
{
  private JPanel _mainPanel = new JPanel();
  private JPanel _lablePanel = new JPanel();
  private JPanel _dateTimeInformationPanel = new JPanel();
  private JPanel _editDateButtonPanel = new JPanel();
  private JLabel _dateLabel = new JLabel();
  private JLabel _timeLabel = new JLabel();
  private JTextField _dateTextField = new JTextField();
  private JComboBox _minuteComboBox = new JComboBox();
  private JComboBox _amPmComboBox = new JComboBox();
  private JComboBox _hourComboBox = new JComboBox();
  private JPanel _timeInformationPanel = new JPanel();
  private GridLayout _dateTimeInformationPanelGridLayout = new GridLayout();
  private GridLayout _labelPanelGridLayout = new GridLayout();
  private JPanel _dateTimeChooserInnerPanel = new JPanel();
  private BorderLayout _dateTimeChooserInnerPanelBorderLayout = new BorderLayout();
  private JPanel _dateTimeChooserPanel = new JPanel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private FlowLayout _dateTimeChooserPanelFlowLayout = new FlowLayout();
  private JPanel _okCancelButtonPanel = new JPanel();
  private JButton _okButton = new JButton();
  private JPanel _okCancelButtonInnerPanel = new JPanel();
  private GridLayout _okCancelButtonInnerPanelGridLayout = new GridLayout();
  private JButton _cancelButton = new JButton();
  private FlowLayout _okCancelButtonPanelFlowLayout = new FlowLayout();
  private JLabel _colonLabel = new JLabel();
  private FlowLayout flowLayout1 = new FlowLayout();
  private Border _empty5555SpaceBorder;
  private Border _compoundBorder;
  private JPanel _editButtonInnerPanel = new JPanel();
  private JButton _editDateButton = new JButton();
  private GridLayout _editButtonInnerPanelGridLayout = new GridLayout();
  private FlowLayout _editButtonPanelFlowLayout = new FlowLayout();
  private Calendar _calendar = null;
  private Calendar _previousCalendar = null;
  private int _previousHourSelectedIndex = 0;
  private int _previousMinuteSelectedIndex = 0;
  private int _previousAmPmSelectedIndex = 0;

  /**
   * @author George A. David
   */
  public DateTimeChooser(JFrame frame, String title, boolean modal)
  {
    super(frame , title, modal);
    Assert.expect(title != null);

    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }


  /**
   * @author George A. David
   */
  private void jbInit() throws Exception
  {
    _empty5555SpaceBorder = BorderFactory.createEmptyBorder(5,5,5,5);
    _compoundBorder = BorderFactory.createCompoundBorder(_empty5555SpaceBorder,
                      BorderFactory.createBevelBorder(BevelBorder.LOWERED));
    _mainPanel.setLayout(borderLayout2);
    _lablePanel.setLayout(_labelPanelGridLayout);
    _dateTimeInformationPanel.setLayout(_dateTimeInformationPanelGridLayout);
    _dateLabel.setText("Date:");
    _timeLabel.setToolTipText("");
    _timeLabel.setText("Time:");
    _timeInformationPanel.setLayout(flowLayout1);
    _dateTimeInformationPanelGridLayout.setRows(2);
    _labelPanelGridLayout.setColumns(1);
    _labelPanelGridLayout.setRows(2);
    _dateTimeChooserInnerPanel.setLayout(_dateTimeChooserInnerPanelBorderLayout);
    _hourComboBox.setMinimumSize(new Dimension(45, 26));
    _hourComboBox.setPreferredSize(new Dimension(45, 26));
    _minuteComboBox.setMinimumSize(new Dimension(50, 26));
    _minuteComboBox.setPreferredSize(new Dimension(50, 26));
    _amPmComboBox.setMinimumSize(new Dimension(60, 26));
    _amPmComboBox.setPreferredSize(new Dimension(60, 26));
    _dateTimeChooserPanel.setLayout(_dateTimeChooserPanelFlowLayout);
    _dateTextField.setBorder(_compoundBorder);
    _dateTextField.setPreferredSize(new Dimension(14, 35));
    _dateTextField.setEditable(false);
    _okButton.setToolTipText("");
    _okButton.setText("Ok");
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _okButton_actionPerformed(e);
      }
    });
    _okCancelButtonInnerPanel.setLayout(_okCancelButtonInnerPanelGridLayout);
    _cancelButton.setText("Cancel");
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _cancelButton_actionPerformed(e);
      }
    });
    _okCancelButtonPanel.setLayout(_okCancelButtonPanelFlowLayout);
    _okCancelButtonInnerPanelGridLayout.setHgap(10);
    _dateTimeChooserInnerPanelBorderLayout.setHgap(5);
    _okCancelButtonPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _dateTimeChooserPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _colonLabel.setText(":");
    flowLayout1.setAlignment(FlowLayout.LEFT);
    flowLayout1.setVgap(0);
    _editDateButton.setText("Edit Date");
    _editDateButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _editDateButton_actionPerformed(e);
      }
    });
    _editButtonInnerPanel.setLayout(_editButtonInnerPanelGridLayout);
    _editDateButtonPanel.setLayout(_editButtonPanelFlowLayout);
    _editButtonPanelFlowLayout.setHgap(0);
    _editButtonPanelFlowLayout.setVgap(0);
    _editDateButtonPanel.setBorder(_empty5555SpaceBorder);
    getContentPane().add(_mainPanel);
    _mainPanel.add(_dateTimeChooserPanel,  BorderLayout.NORTH);
    _dateTimeChooserPanel.add(_dateTimeChooserInnerPanel, null);
    _lablePanel.add(_dateLabel, null);
    _lablePanel.add(_timeLabel, null);
    _dateTimeChooserInnerPanel.add(_editDateButtonPanel, BorderLayout.EAST);
    _editDateButtonPanel.add(_editButtonInnerPanel, null);
    _editButtonInnerPanel.add(_editDateButton, null);
    _dateTimeChooserInnerPanel.add(_dateTimeInformationPanel, BorderLayout.CENTER);
    _dateTimeInformationPanel.add(_dateTextField, null);
    _dateTimeInformationPanel.add(_timeInformationPanel, null);
    _timeInformationPanel.add(_hourComboBox, null);
    _timeInformationPanel.add(_colonLabel, null);
    _timeInformationPanel.add(_minuteComboBox, null);
    _timeInformationPanel.add(_amPmComboBox, null);
    _dateTimeChooserInnerPanel.add(_lablePanel, BorderLayout.WEST);
    _mainPanel.add(_okCancelButtonPanel,  BorderLayout.CENTER);
    _okCancelButtonPanel.add(_okCancelButtonInnerPanel, null);
    _okCancelButtonInnerPanel.add(_okButton, null);
    _okCancelButtonInnerPanel.add(_cancelButton, null);

    _calendar = Calendar.getInstance();
    DateFormat df = DateFormat.getDateInstance(DateFormat.LONG);
    _dateTextField.setText(df.format(_calendar.getTime()));

    populateComboBoxes();
  }

  /**
   * @author George A. David
   */
  void populateComboBoxes()
  {
    for(int i = 1; i < 13; i++)
    {
      _hourComboBox.addItem(new Integer(i));
    }

    for(int i = 0; i < 60; i++)
    {
      Object item;
      if(i < 10)
      {
        item = "0" + i;

      }
      else
      {
        item = new Integer(i);
      }
      _minuteComboBox.addItem(item);
    }

    _amPmComboBox.addItem("A.M.");
    _amPmComboBox.addItem("P.M.");
  }

  /**
   * @author George A. David
   * @author Erica Wheatcroft
   */
  void _cancelButton_actionPerformed(ActionEvent e)
  {
    hide();
  }

  /**
   * @author George A. David
   */
  void _editDateButton_actionPerformed(ActionEvent e)
  {
    CalendarChooserDialog calendarChooser = new CalendarChooserDialog(null, "Choose a date", true);
    calendarChooser.setCalendar(_calendar);
    SwingUtils.centerOnComponent(calendarChooser, this);
    int response = calendarChooser.showAndGetResponse();
    if(response == CalendarChooserDialog.OK_OPTION)
    {
      _calendar = calendarChooser.getCalendar();
      DateFormat df = DateFormat.getDateInstance(DateFormat.LONG);
      _dateTextField.setText(df.format(_calendar.getTime()));
    }
  }

  /**
   * @author George A. David
   */
  void _okButton_actionPerformed(ActionEvent e)
  {
    _previousCalendar = _calendar;
    _previousHourSelectedIndex = _hourComboBox.getSelectedIndex();
    _previousMinuteSelectedIndex = _minuteComboBox.getSelectedIndex();
    _previousAmPmSelectedIndex = _amPmComboBox.getSelectedIndex();
    hide();
  }


  /**
   * @author George A. David
   */
  String getDateTimeString()
  {
    String dateTimeString;

    if(_previousCalendar == null)
      dateTimeString = "";
    else
      dateTimeString = _dateTextField.getText() + " " +
                              _hourComboBox.getSelectedItem() + ":" +
                              _minuteComboBox.getSelectedItem() + " " +
                              _amPmComboBox.getSelectedItem();

    return dateTimeString;
  }
}