package proto.classifier.com.agilent.xRayTest.gui.classifierTool;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.agilent.util.*;
/**
 * @author George A. David
 */
class DateButtonEditor extends DefaultCellEditor
{
  private JButton _button = null;
  private boolean _isPushed = false;
  private Point _editLocationPoint;
  private DateTimeChooser _dateTimeChooser = null;
  private String _label = null;

  /**
   * @author George A. David
   */
  DateButtonEditor(JCheckBox checkBox)
  {
    super(checkBox);
    Assert.expect(checkBox != null);

    _dateTimeChooser = new DateTimeChooser(null, "Edit Date and Time", true);
    _button = new JButton();
    _button.setOpaque(true);
    _button.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        fireEditingStopped();
      }
    });
  }

  /**
   * @author George A. David
   */
  public Component getTableCellEditorComponent(JTable table, Object value,
       boolean isSelected, int row, int column)
  {
    if (isSelected)
    {
      _button.setForeground(table.getSelectionForeground());
      _button.setBackground(table.getSelectionBackground());
    }
    else
    {
      _button.setForeground(table.getForeground());
      _button.setBackground(table.getBackground());
    }
    _label = (value ==null) ? "" : value.toString();
    _button.setText( _dateTimeChooser.getDateTimeString() );
    _button.setBorder(BorderFactory.createEmptyBorder());
    _isPushed = true;
    Point tp = table.getLocationOnScreen();
    Point cp = table.getCellRect(row, column, true).getLocation();
    _editLocationPoint = new Point(tp.x + cp.x, tp.y + cp.y);

    return _button;
  }

  /**
   * @author George A. David
   */
  public Object getCellEditorValue()
  {
    if (_isPushed)
    {
      _dateTimeChooser.setLocation(_editLocationPoint);
      _dateTimeChooser.setResizable(false);

      _dateTimeChooser.show();
    }
    _isPushed = false;

    // if not pushed, then return the current settings
    return _dateTimeChooser.getDateTimeString();
  }

  /**
   * @author George A. David
   */
  public boolean stopCellEditing()
  {
    _isPushed = false;
    return super.stopCellEditing();
  }

  /**
   * @author George A. David
   */
  protected void fireEditingStopped()
  {
    super.fireEditingStopped();
  }
}