package proto.classifier.com.agilent.xRayTest.gui.classifierTool;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;

import com.agilent.util.*;
import proto.classifier.com.agilent.xRayTest.datastore.classifierTool.*;

/**
 * @author George A. David
 */
public class ImageScrollPanel extends JPanel implements Scrollable
{
  private Map _classifiedJointToImageIconPanel = new HashMap();
  private JPanel _gridLayoutPanel = null;
  private Rectangle2D _selectionBox = null;
  private int _columnCount = 5;
  private static final int _GAP = 10;
  private Rectangle _visibleRectangle = new Rectangle();
  private Rectangle _pageUpRectangle = new Rectangle();
  private Rectangle _pageDownRectangle = new Rectangle();
  private java.util.List _panelsWithImagesLoaded = new ArrayList();
  private java.util.List _imagesToBeCleared = new ArrayList();
  private java.util.List _visiblePanels = new ArrayList();
  private java.util.List _pageUpPanels = new ArrayList();
  private java.util.List _pageDownPanels = new ArrayList();
  private WorkerThread _workerThread = new WorkerThread("image loader");
  private boolean _stopLoadingImages = false;
  private boolean _finishedLoadingImages = false;


  /**
   * @author George A. David
   */
  ImageScrollPanel()
  {
    FlowLayout flowLayout = new FlowLayout(FlowLayout.LEFT, _GAP, _GAP);
    setLayout(flowLayout);
    GridLayout gridLayout = new GridLayout(0, _columnCount, _GAP, _GAP);
    _gridLayoutPanel = new JPanel(gridLayout);
    add(_gridLayoutPanel);
    resize();
  }

  /**
   * @author George A. David
   */
  void resize()
  {
    Container parent = getParent();
    if(parent instanceof JViewport)
    {
      int width = ((JViewport)parent).getViewRect().width;
      _workerThread.clearPendingTasks();
      _stopLoadingImages = true;

      _columnCount = (int)(width / 160.0);
      if(_columnCount < 4)
      {
        _columnCount = 4;
      }
      GridLayout gridLayout = new GridLayout(0, _columnCount, _GAP, _GAP);
      _gridLayoutPanel.setLayout(gridLayout);
      _gridLayoutPanel.revalidate();
    }
  }

  /**
   * @author George A. David
   */
  void add(ImageIconPanel imageIconPanel)
  {
    Assert.expect(imageIconPanel != null);

    _gridLayoutPanel.add(imageIconPanel);
    Object previous = _classifiedJointToImageIconPanel.put(imageIconPanel.getClassifiedJoint(), imageIconPanel);
    Assert.expect(previous == null, "Duplicate classified joint found!");
  }

  /**
   * @author George A. David
   */
  public void paint(Graphics g)
  {
    super.paint(g);
    Assert.expect(g != null);

    if(_selectionBox != null)
    {
      Graphics2D g2 = (Graphics2D)g;
      g2.draw(_selectionBox);
    }
  }

  /**
   * @author George A. David
   */
  void setNumberOfImages(int numberOfImages)
  {
    Assert.expect(numberOfImages >= 0);

    _gridLayoutPanel.setLayout(new GridLayout(0, _columnCount, _GAP, _GAP));
  }

  /**
   * @author George A. David
   */
  public Dimension getPreferredScrollableViewportSize()
  {
    return getPreferredSize();
  }

  /**
   * @author George A. David
   */
  public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction)
  {
    Assert.expect(visibleRect != null);
    return 160;
  }

  /**
   * @author George A. David
   */
  public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction)
  {
    Assert.expect(visibleRect != null);
    return visibleRect.height;
  }

  /**
   * @author George A. David
   */
  public boolean getScrollableTracksViewportWidth()
  {
    return false;
  }

  /**
   * @author George A. David
   */
  public boolean getScrollableTracksViewportHeight()
  {
    return false;
  }

  /**
   * @author George A. David
   */
  void selectImage(ClassifiedJoint classifiedJoint, boolean clearPreviousSelections)
  {
    Assert.expect(classifiedJoint != null);

    if(clearPreviousSelections)
    {
      ImageIconPanel.clearSelectedPanels();
    }

    ImageIconPanel imagePanel = (ImageIconPanel)_classifiedJointToImageIconPanel.get(classifiedJoint);
    Assert.expect(imagePanel != null);
    imagePanel.select();
  }

  /**
   * @author George A. David
   */
  void selectImages(java.util.List classifiedJoints, boolean clearSelectedRows)
  {
    Assert.expect(classifiedJoints != null);

    if(clearSelectedRows)
    {
      ImageIconPanel.clearSelectedPanels();
    }


    Iterator it = classifiedJoints.iterator();
    while(it.hasNext())
    {
      ClassifiedJoint classifiedJoint = (ClassifiedJoint)it.next();
      ImageIconPanel imagePanel = (ImageIconPanel)_classifiedJointToImageIconPanel.get(classifiedJoint);
      Assert.expect(imagePanel != null);
      imagePanel.select();
    }
  }

  /**
   * Given the end panels, select them and all images in between.
   * @author George A. David
   */
  void selectImages(ImageIconPanel firstEndImagePanel, ImageIconPanel secondEndImagePanel)
  {
    Assert.expect(firstEndImagePanel != null);
    Assert.expect(secondEndImagePanel != null);

    int firstEndPanelIndex = getImageIndex(firstEndImagePanel);
    int secondEndPanelIndex = getImageIndex(secondEndImagePanel);

    if(firstEndPanelIndex > secondEndPanelIndex)
    {
      selectImages(secondEndPanelIndex, firstEndPanelIndex);
    }
    else
    {
      selectImages(firstEndPanelIndex, secondEndPanelIndex);
    }
  }

  /**
   * Given the indicies, select those panels and all the panels in between.
   * @author George A. David
   */
  private void selectImages(int beginIndex, int endIndex)
  {
    Assert.expect(beginIndex < _gridLayoutPanel.getComponentCount() &&
                  beginIndex >= 0);
    Assert.expect(endIndex < _gridLayoutPanel.getComponentCount() &&
                  endIndex >= 0);

    ImageIconPanel.clearSelectedPanels();
    Component[] components = _gridLayoutPanel.getComponents();
    for(int i = beginIndex; i < endIndex + 1; ++i)
    {
      ImageIconPanel iconPanel = (ImageIconPanel)components[i];
      iconPanel.select();
    }
  }

  /**
   * @author George A. David
   */
  void selectAllImages()
  {
    Component[] components = _gridLayoutPanel.getComponents();
    for(int i = 0; i < components.length; ++i)
    {
      ImageIconPanel imagePanel = (ImageIconPanel)components[i];
      imagePanel.select();
    }
  }

  /**
   * @author George A. David
   */
  void clearSelectedImages()
  {
    ImageIconPanel.clearSelectedPanels();
  }

  /**
   * @author George A. David
   */
  void reorderImages(java.util.List classifiedJoints)
  {
    Assert.expect(classifiedJoints != null);

    _gridLayoutPanel.removeAll();
    Iterator it = classifiedJoints.iterator();
    while(it.hasNext())
    {
      ImageIconPanel imagePanel = (ImageIconPanel)_classifiedJointToImageIconPanel.get(it.next());
      _gridLayoutPanel.add(imagePanel);
    }
    _gridLayoutPanel.validate();
    updateImages();
    scrollSelectedPanelToVisible();
  }

  /**
   * @author George A. David
   */
  private int getImageIndex(Component component)
  {
    Assert.expect(component != null);

    int index = -1;
    Component[] components = _gridLayoutPanel.getComponents();
    for(int i = 0; i < components.length; ++i)
    {
      if(component == components[i])
      {
        index = i;
        break;
      }
    }
    Assert.expect(index != -1);

    return index;
  }

  /**
   * @author George A. David
   */
  boolean areAllImagesSelected()
  {
    boolean areAllImagesSelected = true;
    Component[] components = _gridLayoutPanel.getComponents();
    for(int i = 0; i < components.length; ++i)
    {
      ImageIconPanel iconPanel = (ImageIconPanel)components[i];
      if(iconPanel.isSelected() == false)
      {
        areAllImagesSelected = false;
        break;
      }
    }

    return areAllImagesSelected;
  }

  /**
   * @author George A. David
   */
  ImageIconPanel getNextUnselectedPanel()
  {
    Component[] components = _gridLayoutPanel.getComponents();
    int currentIndex = -1;
    for(int i = 0; i < components.length; ++i)
    {
      ImageIconPanel iconPanel = (ImageIconPanel)components[i];
      if(iconPanel.isSelected())
      {
        currentIndex = i;
      }
    }
    int nextIndex = 0;
    // if no panels were selected then the next selected panel is the first panel
    // otherwise calculate the next index.
    if(currentIndex != -1)
    {
      nextIndex = getNextIndex(currentIndex);
    }

    return (ImageIconPanel)components[nextIndex];
  }

  /**
   * @author George A. David
   */
  private int getNextIndex(int currentIndex)
  {
    Assert.expect(currentIndex >= 0 &&
                  currentIndex < _gridLayoutPanel.getComponentCount());

    int nextIndex = currentIndex + 1;
    if(nextIndex >= _gridLayoutPanel.getComponentCount())
    {
      nextIndex = 0;
    }

    return nextIndex;
  }

  /**
   * @author George A. David
   */
  ImageIconPanel getNextImage(ImageIconPanel currentPanel)
  {
    Assert.expect(currentPanel != null);
    int currentIndex = getImageIndex(currentPanel);
    int nextIndex = currentIndex + 1;
    if(nextIndex >= _gridLayoutPanel.getComponentCount())
    {
      nextIndex = 0;
    }
    ImageIconPanel imagePanel = (ImageIconPanel)_gridLayoutPanel.getComponent(nextIndex);

    return imagePanel;
  }

  /**
   * @author George A. David
   */
  void selectNextImage(ImageIconPanel currentPanel)
  {
    Assert.expect(currentPanel != null);

    ImageIconPanel imagePanel = getNextImage(currentPanel);
    ImageIconPanel.clearSelectedPanels();
    imagePanel.select();
    imagePanel.requestFocus();
    Rectangle rectToScrollTo = imagePanel.getBounds();
    rectToScrollTo.grow(15, 15);
    scrollRectToVisible(rectToScrollTo);
  }

  /**
   * @author George A. David
   */
  void selectPreviousImage(ImageIconPanel currentPanel)
  {
    Assert.expect(currentPanel != null);

    int currentIndex = getImageIndex(currentPanel);
    int previousIndex = currentIndex - 1;
    if(previousIndex < 0)
    {
      previousIndex = _gridLayoutPanel.getComponentCount() - 1;
    }
    ImageIconPanel imagePanel = (ImageIconPanel)_gridLayoutPanel.getComponent(previousIndex);
    ImageIconPanel.clearSelectedPanels();
    imagePanel.select();
    imagePanel.requestFocus();
    Rectangle rectToScrollTo = imagePanel.getBounds();
    rectToScrollTo.grow(15, 15);
    scrollRectToVisible(rectToScrollTo);
  }

  /**
   * @author George A. David
   */
  void selectNextImageAbove(ImageIconPanel currentPanel)
  {
    Assert.expect(currentPanel != null);

    int currentIndex = getImageIndex(currentPanel);
    int aboveIndex = currentIndex - _columnCount;
    if(aboveIndex < 0)
    {
      int index = _gridLayoutPanel.getComponentCount() - 1;
      while(index % _columnCount != currentIndex)
      {
        --index;
      }
      Assert.expect(index >= 0);
      aboveIndex = index;
    }
    ImageIconPanel imagePanel = (ImageIconPanel)_gridLayoutPanel.getComponent(aboveIndex);
    ImageIconPanel.clearSelectedPanels();
    imagePanel.select();
    imagePanel.requestFocus();
    Rectangle rectToScrollTo = imagePanel.getBounds();
    rectToScrollTo.grow(15, 15);
    scrollRectToVisible(rectToScrollTo);
  }

  /**
   * @author George A. David
   */
  void selectNextImageBelow(ImageIconPanel currentPanel)
  {
    Assert.expect(currentPanel != null);

    int currentIndex = getImageIndex(currentPanel);
    int belowIndex = currentIndex + _columnCount;
    if(belowIndex >= _gridLayoutPanel.getComponentCount())
    {
      belowIndex = currentIndex % _columnCount;
    }
    ImageIconPanel imagePanel = (ImageIconPanel)_gridLayoutPanel.getComponent(belowIndex);
    ImageIconPanel.clearSelectedPanels();
    imagePanel.select();
    imagePanel.requestFocus();
    Rectangle rectToScrollTo = imagePanel.getBounds();
    rectToScrollTo.grow(15, 15);
    scrollRectToVisible(rectToScrollTo);
  }

  /**
   * Selects the first image on the next page that is not completely visible
   * on the current page.
   * @author George A. David
   */
  void selectPageDownImage(ImageIconPanel currentPanel)
  {
    Assert.expect(currentPanel != null);

    ImageIconPanel imagePanel = null;
    int belowIndex = getImageIndex(currentPanel);
    boolean done = false;
    while(done == false)
    {
      belowIndex += _columnCount;
      if(belowIndex < _gridLayoutPanel.getComponentCount())
      {
        imagePanel = (ImageIconPanel)_gridLayoutPanel.getComponent(belowIndex);
        if(_visibleRectangle.contains(imagePanel.getBounds()))
        {
          continue;
        }
        else
        {
          done = true;
        }
      }
      else
      {
        imagePanel = (ImageIconPanel)_gridLayoutPanel.getComponent(_gridLayoutPanel.getComponentCount() - 1);
        done = true;
      }
    }

    ImageIconPanel.clearSelectedPanels();
    imagePanel.select();
    imagePanel.requestFocus();
    Rectangle rectToScrollTo = imagePanel.getBounds();
    rectToScrollTo.setSize(rectToScrollTo.width, _visibleRectangle.height + 500);
    scrollRectToVisible(rectToScrollTo);
  }

  /**
   * Selects the first image on the previous page that is completely visible
   * @author George A. David
   */
  void selectPageUpImage(ImageIconPanel currentPanel)
  {
    Assert.expect(currentPanel != null);

    ImageIconPanel pageUpImagePanel = (ImageIconPanel)_gridLayoutPanel.getComponent(0);
    ImageIconPanel imagePanel = null;
    int aboveIndex = getImageIndex(currentPanel);
    boolean done = false;
    while(done == false)
    {
      aboveIndex -= _columnCount;
      if(aboveIndex >= 0)
      {
        imagePanel = (ImageIconPanel)_gridLayoutPanel.getComponent(aboveIndex);
        if(_pageUpRectangle.contains(imagePanel.getBounds()) ||
           _visibleRectangle.contains(imagePanel.getBounds()) ||
           _visibleRectangle.intersects(imagePanel.getBounds()))
        {
          pageUpImagePanel = imagePanel;
          continue;
        }
        else
        {
          done = true;
        }
      }
      else
      {
        done = true;
      }
    }
    ImageIconPanel.clearSelectedPanels();
    pageUpImagePanel.select();
    pageUpImagePanel.requestFocus();
    Rectangle rectToScrollTo = pageUpImagePanel.getBounds();
//    rectToScrollTo.setLocation(rectToScrollTo.x, rectToScrollTo.y - _visibleRectangle.height + 160 + 15);
//    rectToScrollTo.grow(15, 15);
    scrollRectToVisible(rectToScrollTo);
  }

  /**
   * @author George A. David
   */
  void deleteImages(java.util.List classifiedJoints)
  {
    Assert.expect(classifiedJoints != null);

    ImageIconPanel.clearSelectedPanels();

    Iterator it = classifiedJoints.iterator();
    while(it.hasNext())
    {
      ImageIconPanel imagePanel = (ImageIconPanel)_classifiedJointToImageIconPanel.remove(it.next());
      _gridLayoutPanel.remove(imagePanel);
    }
    _gridLayoutPanel.revalidate();
  }

  /**
   * @author George A. David
   */
  void delectSelectedImages()
  {
    java.util.List selectedPanels = ImageIconPanel.getSelectedPanels();
    Iterator it = selectedPanels.iterator();
    while(it.hasNext())
    {
      ImageIconPanel imagePanel = (ImageIconPanel)it.next();
      _classifiedJointToImageIconPanel.remove(imagePanel.getClassifiedJoint());
      _gridLayoutPanel.remove(imagePanel);
    }
    ImageIconPanel.clearSelectedPanels();
    _gridLayoutPanel.revalidate();
    _gridLayoutPanel.repaint();
  }

  /**
   * @author George A. David
   */
  public ImageIconPanel getImageIconPanel(ClassifiedJoint classifiedJoint)
  {
    Assert.expect(classifiedJoint != null);
    Assert.expect(_classifiedJointToImageIconPanel.containsKey(classifiedJoint));

    return (ImageIconPanel)_classifiedJointToImageIconPanel.get(classifiedJoint);

  }

  /**
   * @author George A. David
   */
  public java.util.List getPanelsInRectangle(Rectangle rectangle, boolean includeIntersections)
  {
    Assert.expect(rectangle != null);
    java.util.List componentsInRectangle = new ArrayList();
    Component[] components = _gridLayoutPanel.getComponents();
    for(int i = 0; i < components.length; ++i)
    {
      Component component = components[i];
      Rectangle bounds = component.getBounds();
      if(rectangle.contains(bounds) ||
         (includeIntersections && rectangle.intersects(bounds)))
      {
        componentsInRectangle.add(component);
      }
    }

    return componentsInRectangle;
  }

  /**
   * @author George A. David
   */
  public void scrollSelectedPanelToVisible()
  {
    if(ImageIconPanel.getSelectedPanelCount() > 0)
    {
      Rectangle bounds = ImageIconPanel.getSelectedPanel().getBounds();
      bounds.grow(15, 15);
      scrollRectToVisible(bounds);
    }
  }

  /**
   * @author George A. David
   */
  public void loadImages(java.util.List imagePanels)
  {
    Assert.expect(imagePanels != null);

    Iterator it = imagePanels.iterator();
    while(it.hasNext())
    {
      final ImageIconPanel imagePanel = (ImageIconPanel)it.next();
      if(_panelsWithImagesLoaded.contains(imagePanel) == false)
      {
        _panelsWithImagesLoaded.add(imagePanel);
      }
      if(imagePanel.areImagesLoaded() == false)
      {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            if(_stopLoadingImages == false)
            {
              imagePanel.loadImages();

              SwingUtilities.invokeLater(new Runnable()
              {
                public void run()
                {
                  imagePanel.setAreImagesLoaded(true);
                }
              });
            }
            else
            {
              return;
            }
          }

        });
      }
    }
  }

  /**
   * @author George A. David
   */
  private java.util.List loadVisibleImages()
  {
      java.util.List imagePanels = getPanelsInRectangle(_visibleRectangle, true);
      loadImages(imagePanels);

      return imagePanels;
  }

  /**
   * @author George A. David
   */
  private java.util.List loadPageUpImages()
  {
    java.util.List imagePanels = getPanelsInRectangle(_pageUpRectangle, true);
    loadImages(imagePanels);

    return imagePanels;
  }

  /**
   * @author George A. David
   */
  private java.util.List loadPageDownImages()
  {
    java.util.List imagePanels = getPanelsInRectangle(_pageDownRectangle, true);
    loadImages(imagePanels);

    return imagePanels;
  }

  /**
   * @author George A. David
   */
  public void loadImages()
  {
    _workerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        _stopLoadingImages = false;
        java.util.List imagesToLoad = new ArrayList();

        imagesToLoad.addAll(loadVisibleImages());
        imagesToLoad.addAll(loadPageDownImages());
        imagesToLoad.addAll(loadPageUpImages());


        _imagesToBeCleared.addAll(_panelsWithImagesLoaded);
        _imagesToBeCleared.removeAll(imagesToLoad);
        clearImages();
        _panelsWithImagesLoaded.retainAll(imagesToLoad);
        MemoryUtil.garbageCollect();
      }
    });
  }

  /**
   * @author George A. David
   */
  public void clearImages()
  {
     Iterator it = _imagesToBeCleared.iterator();
     while(it.hasNext())
     {
       ImageIconPanel imagePanel = (ImageIconPanel)it.next();
       imagePanel.clearImages();
     }
     _imagesToBeCleared.clear();
  }

  /**
   * @author George A. David
   */
  public void updateImages()
  {
    Container parent = getParent();
    if(parent instanceof JViewport)
    {
      _workerThread.clearPendingTasks();
      _stopLoadingImages = true;

      Dimension dim = getPreferredSize();
      Rectangle viewRect = ((JViewport)parent).getViewRect();

      _visibleRectangle = new Rectangle(0, viewRect.y, dim.width, viewRect.height);

      int pageUpY = viewRect.y - viewRect.height;
      _pageUpRectangle = new Rectangle(0, pageUpY, dim.width, viewRect.height);

      int pageDownY = viewRect.y + viewRect.height;
      _pageDownRectangle = new Rectangle(0, pageDownY, dim.width, viewRect.height);
      loadImages();
    }
  }

  /**
   * Clears all other selections and selects the first image.
   * @author George A. David
   */
  public void selectFirstImage()
  {
    Assert.expect(_gridLayoutPanel.getComponentCount() > 0);

    ImageIconPanel.clearSelectedPanels();
    ImageIconPanel iconPanel = (ImageIconPanel)_gridLayoutPanel.getComponent(0);
    iconPanel.select();
    iconPanel.requestFocus();
  }

  /**
   * @author George A. David
   */
  public boolean containsImages()
  {
    return _gridLayoutPanel.getComponentCount() > 0;
  }
}