package proto.classifier.com.agilent.xRayTest.gui.classifierTool;

import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import javax.swing.event.*;

import com.agilent.util.*;
import proto.classifier.com.agilent.xRayTest.datastore.classifierTool.*;

/**
 * @author George A. David
 */
class FilterEditorTableCellRenderer extends JPanel implements TableCellRenderer
{

  /**
   * @author George A. David
   */
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int rowIndex, int columnIndex)
  {
    setOpaque(true);
   if (isSelected)
   {
     setForeground(table.getSelectionForeground());
     setBackground(table.getSelectionBackground());
   }
   else
   {
     setForeground(table.getForeground());
     setBackground(UIManager.getColor("Button.background"));
   }
   return this;
  }
}