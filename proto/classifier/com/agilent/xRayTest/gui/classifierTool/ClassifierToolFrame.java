package proto.classifier.com.agilent.xRayTest.gui.classifierTool;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.image.renderable.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

import com.agilent.xRayTest.business.server.*;
import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.images.*;
import com.agilent.xRayTest.util.*;
import com.agilent.guiUtil.*;
import com.agilent.util.*;

import proto.classifier.com.agilent.xRayTest.datastore.classifierTool.*;
import proto.classifier.com.agilent.guiUtil.classifierGuiUtil.*;
import proto.classifier.com.agilent.util.classifierTool.*;
/**
 * @author George A. David
 */
public class ClassifierToolFrame extends JFrame implements PercentProgressSetableInterface
{
  private static ClassifierToolFrame _instance = null;
  private final ServerDosShell _dosShell = ServerDosShell.getInstance();
  private JPanel _contentPane;
  private BorderLayout _contentPaneBorderLayout = new BorderLayout();
  private JPanel _mainPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JButton _exitButton = new JButton();
  private JButton _consistencyCheckButton = new JButton();
  private JButton _trainClassifierButton = new JButton();
  private JButton _manualReviewOfJointsButton = new JButton();
  private GridLayout _buttonPanelGridLayout = new GridLayout();
  private FlowLayout _mainPanelFlowLayout = new FlowLayout();
  private Border _empty15_150_15_150SpaceBorder;
  private JPanel _manualReviewPanel = new JPanel();
  private JMenuBar _manualReviewMenuBar = new JMenuBar();
  private JMenu _fileMenu = new JMenu();
  private JMenuItem _loadMenuItem = new JMenuItem();
  private JMenuItem _saveMenuItem = new JMenuItem();
  private JMenuItem _saveAsMenuItem = new JMenuItem();
  private JMenu _editMenu = new JMenu();
  private JMenuItem _deleteMenuItem = new JMenuItem();
  private JMenuItem _selectAllMenuItem = new JMenuItem();
  private JMenuItem _editFilterMenuItem = new JMenuItem();
  private JMenuItem _markAsGoodMenuItem = new JMenuItem();
  private JMenuItem _markAsBadMenuItem = new JMenuItem();
  private JMenuItem _markForReviewMenuItem = new JMenuItem();
  private JMenuItem _markAsReviewMenuItem = new JMenuItem();
  private JMenu _viewMenu = new JMenu();
  private JMenuItem _showJointTableMenuItem = new JMenuItem();
  private JToolBar _manualReviewToolBar = new JToolBar();
  private BorderLayout _manualReviewPanelBorderLayout = new BorderLayout();
  private JButton _openToolBarButton = new JButton();
  private boolean _mainPanelIsVisible = true;
  private final String _CLASSIFIER_UTILITY_TITLE = "Classifier Utility";
  private final String _MANUAL_REVIEW_TITLE = "Review Classifications";
  private JButton _saveToolBarButton = new JButton();
  private JButton _zoomInToolBarButton = new JButton();
  private JButton _fullImageToolBarButton = new JButton();
  private JButton _previousJointToolBarButton = new JButton();
  private JButton _nextJointToolBarButton = new JButton();
  private JButton _goodJointToolBarButton = new JButton();
  private JButton _badJointToolBarButton = new JButton();
  private JButton _runClassifierButton = new JButton();
  private JPanel _manualReviewMainPanel = new JPanel();
  private JPanel _changeSlicePanel = new JPanel();
  private BorderLayout _manualReviewMainPanelBorderLayout = new BorderLayout();
  private JButton _nextSliceButton = new JButton();
  private JLabel _sliceLabel = new JLabel();
  private JPanel _jointReviewPanel = new JPanel();
  private BorderLayout _jointReviewBorderLayout = new BorderLayout();
  private boolean _tableIsVisible = true;
  private FlowLayout _changeSlicePanelFlowLayout = new FlowLayout();
  private Filter _filter = new Filter();
  private FilterEditor _filterEditor = new FilterEditor(this, "Filter Editor", true);
  private JSplitPane _jointReviewSplitPane = new JSplitPane();
  private JScrollPane _imageScrollPane = new JScrollPane();
  private FlowLayout _manualReviewStatusPanelFlowLayout = new FlowLayout();
  private JPanel _manualReviewStatusPanel = new JPanel();
  private JLabel _statusLabel = new JLabel();
  private JPanel _filterTableButtonPanel = new JPanel();
  private JPanel _filterAppliedLabelPanel = new JPanel();
  private FlowLayout _filterAppliedLabelPanelFlowLayout = new FlowLayout();
  private BorderLayout _filterTableButtonPanelBorderLayout = new BorderLayout();
  private JLabel _filterAppliedLabel = new JLabel();
  private JButton _editFilterButton = new JButton();
  private GridLayout _editFilterButtonInnerPanelGridLayout = new GridLayout();
  private JPanel _editFilterButtonPanel = new JPanel();
  private FlowLayout _editFilterButtonPanelFlowLayout = new FlowLayout();
  private JPanel _editFilterButtonInnerPanel = new JPanel();
  private JScrollPane _filterTableScrollPane = new JScrollPane();
  private JSortTable _filterTable = new JSortTable();
  private ImageScrollPanel _imageScrollPanel = new ImageScrollPanel();
  private ImageIconPanel _selectedImagePanel = null;
  private JToggleButton _graphicsToggleToolBarButton = new JToggleButton();
  private JButton _markAsReviewedToolBarButton = new JButton();
  private String _currentFileName = null;
  private WorkerThread _workerThread = new WorkerThread("file loader");
  private boolean _arrowKeyPressed = false;
  private String _adbFileDirectory = System.getProperty("AGT5DX_STAGE2_DIR", "");
  private JButton _predictPerformanceButton = new JButton();
  private boolean _classifierIsRunning = false;
  private JPanel _scriptProgressBarPanel = new JPanel();
  private JProgressBar _scriptProgressBar = new JProgressBar();
  private boolean _isDatabaseDirty = false;
  private boolean _isAtLeastOneJointMarkedForReview = false;
  private JButton _selectAllToolBarButton = new JButton();
  private JButton _markForReviewToolBarButton = new JButton();
  private JMenuItem _exitMenuItem = new JMenuItem();
  private JMenuItem _nextImageMenuItem = new JMenuItem();
  private JMenuItem _previousImageMenuItem = new JMenuItem();
  private JMenuItem _showFullImageMenuItem = new JMenuItem();
  private JMenuItem _showZoomedInImageMenuItem = new JMenuItem();
  private JMenuItem _toggleGraphicsMenuItem = new JMenuItem();
  private Point _oldPosn = null;

  /**
   * @author George A. David
   */
  public ClassifierToolFrame()
  {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try
    {
      jbInit();
      pack();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * @author George A. David
   */
  private void jbInit() throws Exception
  {
    _saveToolBarButton.setToolTipText("Save");
    _openToolBarButton.setToolTipText("Open");
    //setIconImage(Toolkit.getDefaultToolkit().createImage(ClassifierToolFrame.class.getResource("[Your Icon]")));
    _empty15_150_15_150SpaceBorder = BorderFactory.createEmptyBorder(15,150,15,150);
    _contentPane = (JPanel) this.getContentPane();
    _contentPane.setLayout(_contentPaneBorderLayout);
    this.setSize(new Dimension(487, 420));
    this.setTitle(_CLASSIFIER_UTILITY_TITLE);
    _exitButton.setText("Exit");
    _exitButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _exitButton_actionPerformed(e);
      }
    });
    _consistencyCheckButton.setText("Check Consistency");
    _consistencyCheckButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _consistencyCheckButton_actionPerformed(e);
      }
    });
    _trainClassifierButton.setText("Train Classifier");
    _trainClassifierButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _trainClassifierButton_actionPerformed(e);
      }
    });
    _manualReviewOfJointsButton.setMaximumSize(new Dimension(159, 27));
    _manualReviewOfJointsButton.setPreferredSize(new Dimension(200, 50));
    _manualReviewOfJointsButton.setText("Review Classifications");
    _manualReviewOfJointsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _manualReviewOfJointsButton_actionPerformed(e);
      }
    });
    _buttonPanel.setLayout(_buttonPanelGridLayout);
    _buttonPanelGridLayout.setColumns(1);
    _buttonPanelGridLayout.setRows(0);
    _buttonPanelGridLayout.setVgap(15);
    _mainPanel.setLayout(_mainPanelFlowLayout);
    _fileMenu.setText("File");
    _loadMenuItem.setText("Load");
    _loadMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _loadMenuItem_actionPerformed(e);
      }
    });
    _saveMenuItem.setText("Save");
    _saveMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _saveMenuItem_actionPerformed(e);
      }
    });
    _saveAsMenuItem.setText("Save As");
    _saveAsMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _saveAsMenuItem_actionPerformed(e);
      }
    });
    _editMenu.setText("Edit");
    _deleteMenuItem.setText("Delete");
    _deleteMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _deleteMenuItem_actionPerformed(e);
      }
    });
    _selectAllMenuItem.setText("Select All");
    _selectAllMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectAllMenuItem_actionPerformed(e);
      }
    });
    _editFilterMenuItem.setText("Edit Filter");
    _editFilterMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _editFilterMenuItem_actionPerformed(e);
      }
    });
    _markAsGoodMenuItem.setText("Mark As Good Joint");
    _markAsGoodMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _markAsGoodMenuItem_actionPerformed(e);
      }
    });
    _markAsBadMenuItem.setText("Mark As Bad Joint");
    _markAsBadMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _markAsBadMenuItem_actionPerformed(e);
      }
    });
    _markForReviewMenuItem.setText("Mark For Review");
    _markForReviewMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _markForReviewMenuItem_actionPerformed(e);
      }
    });
    _markAsReviewMenuItem.setText("Mark As Reviewed");
    _markAsReviewMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _markAsReviewMenuItem_actionPerformed(e);
      }
    });
    _viewMenu.setText("View");
    _showJointTableMenuItem.setText("Toggle Joint Data Table");
    _showJointTableMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _showJointTableMenuItem_actionPerformed(e);
      }
    });
    _manualReviewPanel.setLayout(_manualReviewPanelBorderLayout);
    _openToolBarButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _openToolBarButton_actionPerformed(e);
      }
    });
    _saveToolBarButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _saveToolBarButton_actionPerformed(e);
      }
    });
    _manualReviewToolBar.setFloatable(false);
    _manualReviewToolBar.setRollover(true);
    _zoomInToolBarButton.setEnabled(false);
    _zoomInToolBarButton.setToolTipText("Zoom in");
    _zoomInToolBarButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _zoomInToolBarButton_actionPerformed(e);
      }
    });
    _fullImageToolBarButton.setEnabled(false);
    _fullImageToolBarButton.setToolTipText("Show full image");
    _fullImageToolBarButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _fullImageToolBarButton_actionPerformed(e);
      }
    });
    _previousJointToolBarButton.setEnabled(false);
    _previousJointToolBarButton.setToolTipText("Go to previous image");
    _previousJointToolBarButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _previousJointToolBarButton_actionPerformed(e);
      }
    });
    _nextJointToolBarButton.setEnabled(false);
    _nextJointToolBarButton.setToolTipText("Go to next image");
    _nextJointToolBarButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _nextJointToolBarButton_actionPerformed(e);
      }
    });
    _goodJointToolBarButton.setEnabled(false);
    _goodJointToolBarButton.setToolTipText("Mark as good joint");
    _goodJointToolBarButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _goodJointToolBarButton_actionPerformed(e);
      }
    });
    _badJointToolBarButton.setEnabled(false);
    _badJointToolBarButton.setToolTipText("Mark as bad joint");
    _badJointToolBarButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _badJointToolBarButton_actionPerformed(e);
      }
    });
    _buttonPanel.setBorder(_empty15_150_15_150SpaceBorder);
    _runClassifierButton.setText("Run Classifier");
    _runClassifierButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _runClassifierButton_actionPerformed(e);
      }
    });
    _manualReviewMainPanel.setLayout(_manualReviewMainPanelBorderLayout);
    _nextSliceButton.setEnabled(false);
    _nextSliceButton.setText("Next Slice");
    _nextSliceButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _nextSliceButton_actionPerformed(e);
      }
    });
    _jointReviewPanel.setLayout(_jointReviewBorderLayout);
    _changeSlicePanel.setLayout(_changeSlicePanelFlowLayout);
    _jointReviewSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
    _jointReviewSplitPane.setName("");
    _jointReviewSplitPane.setDividerSize(20);
    _jointReviewSplitPane.setOneTouchExpandable(true);
    _jointReviewSplitPane.setResizeWeight(1.0);
    _manualReviewStatusPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _manualReviewStatusPanel.setBorder(BorderFactory.createLoweredBevelBorder());
    _manualReviewStatusPanel.setLayout(_manualReviewStatusPanelFlowLayout);
    _statusLabel.setText("Status: ");
    _filterTableButtonPanel.setLayout(_filterTableButtonPanelBorderLayout);
    _filterAppliedLabelPanel.setLayout(_filterAppliedLabelPanelFlowLayout);
    _editFilterButton.setText("Edit Filter");
    _editFilterButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _editFilterButton_actionPerformed(e);
      }
    });
    _editFilterButtonPanel.setLayout(_editFilterButtonPanelFlowLayout);
    _editFilterButtonInnerPanel.setLayout(_editFilterButtonInnerPanelGridLayout);
    _filterTableScrollPane.setMinimumSize(new Dimension(0, 0));
    _filterTableScrollPane.setPreferredSize(new Dimension(100, 100));
    _filterTable.setPreferredSize(new Dimension(3, 3));
    _graphicsToggleToolBarButton.setBorderPainted(false);
    _graphicsToggleToolBarButton.setToolTipText("Toggle graphics");
    _graphicsToggleToolBarButton.setText("Graphics");
    _graphicsToggleToolBarButton.setSelected(true);
    _graphicsToggleToolBarButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _graphicsToggleToolBarButton_actionPerformed(e);
      }
    });
    _markAsReviewedToolBarButton.setEnabled(false);
    _markAsReviewedToolBarButton.setText("Mark As Reviewed");
    _markAsReviewedToolBarButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _markAsReviewedToolBarButton_actionPerformed(e);
      }
    });
    _predictPerformanceButton.setText("Predict Performance");
    _predictPerformanceButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _predictPerformanceButton_actionPerformed(e);
      }
    });
    _filterAppliedLabel.setRequestFocusEnabled(false);
    _scriptProgressBar.setPreferredSize(new Dimension(248, 24));
    _selectAllToolBarButton.setText("Select All");
    _selectAllToolBarButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectAllToolBarButton_actionPerformed(e);
      }
    });
    _markForReviewToolBarButton.setText("Mark For Review");
    _markForReviewToolBarButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _markForReviewToolBarButton_actionPerformed(e);
      }
    });
    _exitMenuItem.setText("Exit");
    _exitMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _exitMenuItem_actionPerformed(e);
      }
    });
    _nextImageMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _nextImageMenuItem_actionPerformed(e);
      }
    });
    _nextImageMenuItem.setText("Go to Next Image");
    _previousImageMenuItem.setText("Go to Previous Image");
    _previousImageMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _previousImageMenuItem_actionPerformed(e);
      }
    });
    _showFullImageMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _showFullImageMenuItem_actionPerformed(e);
      }
    });
    _showFullImageMenuItem.setText("Show full image");
    _showZoomedInImageMenuItem.setText("Show Zoomed Image");
    _showZoomedInImageMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _showZoomedInImageMenuItem_actionPerformed(e);
      }
    });
    _toggleGraphicsMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _toggleGraphicsMenuItem_actionPerformed(e);
      }
    });
    _toggleGraphicsMenuItem.setText("Toggle Graphics");
    _contentPane.add(_mainPanel, BorderLayout.CENTER);
    _mainPanel.add(_buttonPanel, null);
    _buttonPanel.add(_manualReviewOfJointsButton, null);
    _buttonPanel.add(_trainClassifierButton, null);
    _buttonPanel.add(_predictPerformanceButton, null);
    _buttonPanel.add(_consistencyCheckButton, null);
    _buttonPanel.add(_runClassifierButton, null);
    _buttonPanel.add(_exitButton, null);
    _manualReviewMenuBar.add(_fileMenu);
    _manualReviewMenuBar.add(_editMenu);
    _manualReviewMenuBar.add(_viewMenu);
    _fileMenu.add(_loadMenuItem);
    _fileMenu.add(_saveMenuItem);
    _fileMenu.add(_saveAsMenuItem);
    _fileMenu.add(_exitMenuItem);
    _editMenu.add(_deleteMenuItem);
    _editMenu.add(_selectAllMenuItem);
    _editMenu.add(_editFilterMenuItem);
    _editMenu.add(_markAsGoodMenuItem);
    _editMenu.add(_markAsBadMenuItem);
    _editMenu.add(_markForReviewMenuItem);
    _editMenu.add(_markAsReviewMenuItem);
    _viewMenu.add(_showJointTableMenuItem);
    _viewMenu.add(_nextImageMenuItem);
    _viewMenu.add(_previousImageMenuItem);
    _viewMenu.add(_showFullImageMenuItem);
    _viewMenu.add(_showZoomedInImageMenuItem);
    _viewMenu.add(_toggleGraphicsMenuItem);
    _manualReviewPanel.add(_manualReviewToolBar, BorderLayout.NORTH);
    _manualReviewToolBar.add(_openToolBarButton, null);
    _manualReviewToolBar.add(_saveToolBarButton, null);
    _manualReviewToolBar.add(_zoomInToolBarButton, null);
    _manualReviewToolBar.add(_fullImageToolBarButton, null);
    _manualReviewToolBar.add(_previousJointToolBarButton, null);
    _manualReviewToolBar.add(_nextJointToolBarButton, null);
    _manualReviewToolBar.add(_goodJointToolBarButton, null);
    _manualReviewToolBar.add(_badJointToolBarButton, null);
    _manualReviewToolBar.add(_graphicsToggleToolBarButton, null);
    _manualReviewToolBar.add(_selectAllToolBarButton, null);
    _manualReviewToolBar.addSeparator();
    _manualReviewToolBar.add(_markAsReviewedToolBarButton, null);
    _manualReviewToolBar.add(_markForReviewToolBarButton, null);
    _manualReviewPanel.add(_manualReviewMainPanel, BorderLayout.CENTER);
    _manualReviewMainPanel.add(_changeSlicePanel, BorderLayout.NORTH);
    _changeSlicePanel.add(_nextSliceButton, null);
    _changeSlicePanel.add(_sliceLabel, null);
    _manualReviewMainPanel.add(_jointReviewPanel, BorderLayout.CENTER);
    _jointReviewPanel.add(_jointReviewSplitPane, BorderLayout.CENTER);
    _manualReviewMainPanel.add(_manualReviewStatusPanel,  BorderLayout.SOUTH);
    _manualReviewStatusPanel.add(_statusLabel, null);
    _jointReviewSplitPane.add(_imageScrollPane, JSplitPane.TOP);
    _jointReviewSplitPane.add(_filterTableScrollPane, JSplitPane.BOTTOM);
    _filterTableScrollPane.add(_filterTable, null);
    _jointReviewPanel.add(_filterTableButtonPanel,  BorderLayout.SOUTH);
    _filterTableButtonPanel.add(_editFilterButtonPanel, BorderLayout.WEST);
    _editFilterButtonPanel.add(_editFilterButtonInnerPanel, null);
    _editFilterButtonInnerPanel.add(_editFilterButton, null);
    _filterTableButtonPanel.add(_filterAppliedLabelPanel, BorderLayout.CENTER);
    _filterAppliedLabelPanel.add(_filterAppliedLabel, null);
    _scriptProgressBarPanel.add(_scriptProgressBar, null);

    _jointReviewSplitPane.setDividerLocation(495);

    addComponentListener(new ComponentListener()
    {
      public void componentHidden(ComponentEvent e){}
      public void componentMoved(ComponentEvent e) {}
      public void componentShown(ComponentEvent e){}
      public void componentResized(ComponentEvent e)
      {
        _imageScrollPanel.resize();
      }
    });

    FocusManager.setCurrentManager(new FocusManager()
    {
      private boolean isRightArrowKey(int keyCode)
      {
        return (keyCode == KeyEvent.VK_RIGHT ||
                keyCode == KeyEvent.VK_KP_RIGHT);
      }
      private boolean isLeftArrowKey(int keyCode)
      {
        return (keyCode == KeyEvent.VK_LEFT ||
                keyCode == KeyEvent.VK_KP_LEFT);
      }
      private boolean isDownArrowKey(int keyCode)
      {
        return (keyCode == KeyEvent.VK_DOWN ||
                keyCode == KeyEvent.VK_KP_DOWN);
      }
      private boolean isUpArrowKey(int keyCode)
      {
        return (keyCode == KeyEvent.VK_UP ||
                keyCode == KeyEvent.VK_KP_UP);
      }
      private boolean isArrowKey(int keyCode)
      {
        return (isRightArrowKey(keyCode) ||
                isLeftArrowKey(keyCode) ||
                isDownArrowKey(keyCode) ||
                isUpArrowKey(keyCode));
      }
      public void processKeyEvent(java.awt.Component focusedComponent, KeyEvent anEvent)
      {
        if(anEvent.getSource() instanceof ImageIconPanel)
        {
          ImageIconPanel source = (ImageIconPanel)anEvent.getSource();
          if(anEvent.getID() == anEvent.KEY_PRESSED)
          {
            int keyCode = anEvent.getKeyCode();
            if(keyCode == KeyEvent.VK_TAB || isArrowKey(keyCode))
            {
              if(isDownArrowKey(keyCode))
              {
                _imageScrollPanel.selectNextImageBelow(source);
              }
              else if(isUpArrowKey(keyCode))
              {
                _imageScrollPanel.selectNextImageAbove(source);
              }
              else if(anEvent.isShiftDown() || isLeftArrowKey(keyCode))
              {
                _imageScrollPanel.selectPreviousImage(source);
              }
              else
              {
                _imageScrollPanel.selectNextImage(source);
              }
              _arrowKeyPressed = true;
              updateGUI();
              FilterSortTableModel tableModel = (FilterSortTableModel)_filterTable.getModel();
              tableModel.selectRow(_selectedImagePanel.getClassifiedJoint(), true);
              anEvent.consume();
              return;
            }
            else if(keyCode == KeyEvent.VK_A && anEvent.isControlDown())
            {
              selectAll();
              return;
            }
            else if(keyCode == KeyEvent.VK_G ||
                    keyCode == KeyEvent.VK_ADD)
            {
              /* changeClassificationOnSelectedJoints(JointClassificationEnum.GOOD); */
              classifyJointAsGood();
              return;
            }
            else if(keyCode == KeyEvent.VK_B ||
                    keyCode == KeyEvent.VK_SUBTRACT)
            {
              /* changeClassificationOnSelectedJoints(JointClassificationEnum.BAD); */
              classifyJointAsBad();
              return;
            }
            else if(keyCode == KeyEvent.VK_PAGE_UP)
            {
              _imageScrollPanel.selectPageUpImage(source);
              updateGUI();
              FilterSortTableModel tableModel = (FilterSortTableModel)_filterTable.getModel();
              tableModel.selectRow(_selectedImagePanel.getClassifiedJoint(), true);
              anEvent.consume();
              return;
            }
            else if(keyCode == KeyEvent.VK_PAGE_DOWN)
            {
              _imageScrollPanel.selectPageDownImage(source);
              updateGUI();
              FilterSortTableModel tableModel = (FilterSortTableModel)_filterTable.getModel();
              tableModel.selectRow(_selectedImagePanel.getClassifiedJoint(), true);
              anEvent.consume();
              return;
            }
            else if(keyCode == KeyEvent.VK_DELETE)
            {
              deleteImages();
            }

          }
        }

        super.processKeyEvent(focusedComponent,anEvent);
      }
    });

    setUpToolBarImages();
  }

  /**
   * @author George A. David
   */
  private void setUpToolBarImages()
  {
    _openToolBarButton.setIcon(Image5DX.getImageIcon(Image5DX.OPEN_FILE));
    _saveToolBarButton.setIcon(Image5DX.getImageIcon(Image5DX.AT_SAVE_PANEL_PROGRAM));
    _zoomInToolBarButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_ZOOM_IN));
    _fullImageToolBarButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_FULL_IMAGE));
    _previousJointToolBarButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_LEFT_ARROW));
    _nextJointToolBarButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_RIGH_ARROW));
    _goodJointToolBarButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_THUMBS_UP));
    _badJointToolBarButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_THUMBS_DOWN));
  }

  /**
   * @author George A. David
   */
  private void selectAll()
  {
    _imageScrollPanel.selectAllImages();
    updateGUI();
    _filterTable.selectAll();
  }

  /**
   * @author George A. David
   */
  protected void processWindowEvent(WindowEvent e)
  {
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      if(_mainPanelIsVisible)
      {
        if(_classifierIsRunning)
        {
          stopClassifier();
        }
        System.exit(0);
      }
      else
      {
        closeManualReviewOfJoints();
      }
    }
    else
    {
      super.processWindowEvent(e);
    }
  }

  /**
   * @author George A. David
   */
  private void closeManualReviewOfJoints()
  {
    if(doesDatabaseNeedSaving())
    {
      promptUserToSaveDatabase();
    }
    boolean continueClosing = true;
    if(_isAtLeastOneJointMarkedForReview)
    {
      continueClosing = confirmDatabaseClosureWithJointsMarkedForReview();
    }
    if(continueClosing)
    {
      showMainScreen();
    }
  }

  /**
   * @author George A. David
   */
  private boolean doesDatabaseNeedSaving()
  {
    return _isDatabaseDirty;
  }

  /**
   * @author George A. David
   */
  private void promptUserToSaveDatabase()
  {
    int response = JOptionPane.showConfirmDialog(this,
                                                 "The current database has been modified.\n" +
                                                 "Would you like to save your changes?",
                                                 "Classifier Tool",
                                                 JOptionPane.YES_NO_OPTION);
    if(response == JOptionPane.YES_OPTION)
    {
      saveDatabase();
    }
  }

  /**
   * @author George A. David
   */
  private void showMainScreen()
  {
    setJMenuBar(null);
    setContentPane(_contentPane);
    _contentPane.removeAll();
    _contentPane.add(_mainPanel);
    if (_oldPosn != null)
      setLocation(_oldPosn.x, _oldPosn.y);
    setVisible(true);
    pack();
    validate();
    repaint();
    setTitle(_CLASSIFIER_UTILITY_TITLE);
    _mainPanelIsVisible = true;
  }

  /**
   * @author George A. David
   */
  void _exitButton_actionPerformed(ActionEvent e)
  {
    if(_classifierIsRunning)
    {
      stopClassifier();
    }
    System.exit(0);
  }

  /**
   * @author George A. David
   */
  private boolean load()
  {
    boolean fileSelected = false;
    java.util.List files = new ArrayList();
    FileName.findAllFullPaths(_adbFileDirectory, ".adb", files, false);
    if(files.size() > 0)
    {
      if(files.size() > 1)
      {
        JFileChooser adbFileChooser = getAsciiDataBaseFileChooser();
        if(adbFileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
        {
          _currentFileName = adbFileChooser.getSelectedFile().getPath();
          fileSelected = true;
        }
        else
        {
          fileSelected = false;
        }
      }
      else if(files.size() == 1)
      {
        _currentFileName = (String)files.get(0);
        fileSelected = true;
      }

      if(fileSelected)
      {
        load(_currentFileName);
      }
    }
    else
    {
      JOptionPane.showMessageDialog(this,
                                    "No database files were found in " + _adbFileDirectory,
                                    "Classifier Tool",
                                    JOptionPane.INFORMATION_MESSAGE);

    }

    return fileSelected;
  }

  /**
   * @author George A. David
   */
  private void load(final String fileName)
  {
    Assert.expect(fileName != null);

    setCursor(new Cursor(Cursor.WAIT_CURSOR));
    _workerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          ClassifiedJointLibrary.load(fileName);
          _isDatabaseDirty = false;
        }
        catch(DatastoreException dex)
        {
          final String errorMessage = dex.getLocalizedMessage();
          SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              JOptionPane.showMessageDialog(getParent(), errorMessage, "Load Error", JOptionPane.ERROR_MESSAGE);
              showMainScreen();
              return;
            }
          });
        }

        SwingUtilities.invokeLater(new Runnable()
        {
          public void run()
          {
            setUpJointReviewInformation();
          }

        });
      }
    });
  }

  /**
   * @author George A. David
   */
  private void setUpJointReviewInformation()
  {
    createDefaultTable();
    setUpFilterTable();
    setUpImageTable();
    updateGUI();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  /**
   * @author George A. David
   */
  void _manualReviewOfJointsButton_actionPerformed(ActionEvent e)
  {
    if(_classifierIsRunning)
    {
      stopClassifier();
    }
    if(load())
    {
      _oldPosn = getLocation();
      _contentPane.removeAll();
      _contentPane.add(_manualReviewPanel, BorderLayout.CENTER);
      setJMenuBar(_manualReviewMenuBar);
      setTitle(_MANUAL_REVIEW_TITLE);
      _mainPanelIsVisible = false;
      setSize(850, 880);
      SwingUtils.centerOnScreen(this);
      repaint();
    }
  }

  /**
   * @author George A. David
   */
  void _showHideTableButton_actionPerformed(ActionEvent e)
  {
    toggleTable();
    _selectedImagePanel.requestFocus();
  }

  /**
   * @author George A. David
   */
  void toggleTable()
  {
    int maximumDividerLocation = _jointReviewSplitPane.getMaximumDividerLocation();
    int dividerLocation = _jointReviewSplitPane.getDividerLocation();
    int lastDividerLocation = _jointReviewSplitPane.getLastDividerLocation();

    if(dividerLocation == maximumDividerLocation) //if table is hidden
    {
      _jointReviewSplitPane.setDividerLocation(lastDividerLocation);
      _jointReviewSplitPane.setLastDividerLocation(dividerLocation);
      validate();
      repaint();
    }
    else
    {
      _jointReviewSplitPane.setDividerLocation(maximumDividerLocation);
      _jointReviewSplitPane.setLastDividerLocation(dividerLocation);
      validate();
      repaint();
    }
  }

  /**
   * @author George A. David
   */
  void editFilter()
  {
    _filterEditor.show();
    if(_filterEditor.getResponse() == FilterEditor.UPDATE)
    {
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      _workerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          //get the filter
          Filter previousFilter = _filter;
          _filter = _filterEditor.getFilter();
          boolean displayDataChanged = false;

          //check to see if the filter warrants a display change.
          java.util.List previousFilterList = previousFilter.getFilterDataList();
          java.util.List previousFilterListCopy = new ArrayList(previousFilterList);

          java.util.List currentFilterList = _filter.getFilterDataList();
          java.util.List currentFilterListCopy = new ArrayList(currentFilterList);

          Iterator pflcIt = previousFilterListCopy.iterator();
          Iterator cflcIt = currentFilterListCopy.iterator();
          while(pflcIt.hasNext())
          {
            FilterData previousFilterData = (FilterData) pflcIt.next();
            while(cflcIt.hasNext())
            {
              FilterData currentFilterData = (FilterData) cflcIt.next();
              if( previousFilterData.getFilterName().equalsIgnoreCase(currentFilterData.getFilterName()) )
              {
                if((previousFilterData.getFilterString().equalsIgnoreCase(currentFilterData.getFilterString())) &&
                   (previousFilterData.getSortEnum().toString().equalsIgnoreCase(currentFilterData.getSortEnum().toString())))
                {
                  //remove the filterdata from teh previous and current filter data lists
                  pflcIt.remove();
                  cflcIt.remove();
                }
              }
            }
          }

          //now that the lists are populated with only differnt data filters. check to see if they warrent a display change

          //check #1 check the PreviousFilterListCopy if items still exist in this list, check to make sure the filter
          //string is not * or "". If not then these data filters will be the columns we remove.
          pflcIt = previousFilterListCopy.iterator();
          while(pflcIt.hasNext())
          {
            FilterData previousFilterData = (FilterData) pflcIt.next();
            if( (! previousFilterData.getFilterString().equals("*") ) || ( ! previousFilterData.getFilterString().equals("")))
            {
              displayDataChanged = true;
              break;
            }
          }

          //check #2 check that the currentFilterListCopy if items still exist, check to make sure that the filter strings are
          // * or "" if not a display change will be needed.
          cflcIt = currentFilterListCopy.iterator();
          while(cflcIt.hasNext())
          {
            FilterData currentFilterData = (FilterData) cflcIt.next();
            if( (! currentFilterData.getFilterString().equals("*") ) || (! currentFilterData.getFilterString().equals("")))
            {
              displayDataChanged = true;
              break;
            }
          }

          if(displayDataChanged == false)
          {
            //since the display data has not changed all we need to do is update the table and we are ready to go.
            FilterSortTableModel tableModel = (FilterSortTableModel)_filterTable.getModel();

            // now remove the column names from previous filter list copy if any exist.
            Iterator dataFilterIt = previousFilterListCopy.iterator();
            while(dataFilterIt.hasNext())
            {
              FilterData filterData = (FilterData) dataFilterIt.next();
              tableModel.deleteColumn(filterData.getFilterName());
            }

            //now add the column names from teh current filter list if any exist
            dataFilterIt = currentFilterListCopy.iterator();
            while(dataFilterIt.hasNext())
            {
              FilterData filterData = (FilterData) dataFilterIt.next();
              tableModel.addColumn(filterData.getFilterName());
            }
          }
          else
          {
            // reload the new filter data due to display image change
            createTable();
            setUpFilterTable();
            setUpImageTable();
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
          }
        }
      });
    }
    else
    {
      if(_selectedImagePanel != null)
      {
        _selectedImagePanel.requestFocus();
      }
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createTable()
  {
    java.util.List classifiedJoints = ClassifiedJointLibrary.getClassifiedJoints();
    Iterator cjit = classifiedJoints.iterator();
    while(cjit.hasNext())
    {
      ClassifiedJoint classifiedJoint = (ClassifiedJoint)cjit.next();
      if(_filter.accept(classifiedJoint) == false)
      {
        cjit.remove();
      }
    }
    java.util.List filterDataList = _filter.getFilterDataList();
    java.util.List columnNames = new ArrayList();
    Object[][] dataList = new Object[classifiedJoints.size()][filterDataList.size()];

    Map columnNameToSortEnum = new HashMap();
    Iterator it = filterDataList.iterator();
    while(it.hasNext())
    {
      FilterData filterData = (FilterData)it.next();
      String filterName = filterData.getFilterName();
      SortEnum sortEnum = filterData.getSortEnum();
      columnNameToSortEnum.put(filterName, sortEnum);
      columnNames.add(filterName);
      int columnIndex = filterDataList.indexOf(filterData);
      cjit = classifiedJoints.iterator();
      while(cjit.hasNext())
      {
        ClassifiedJoint classifiedJoint = (ClassifiedJoint)cjit.next();
        int rowIndex = classifiedJoints.indexOf(classifiedJoint);
        dataList[rowIndex][columnIndex] = classifiedJoint;
      }
    }

    FilterSortTableModel tableModel = new FilterSortTableModel(dataList, columnNames.toArray());
    _filterTable = new JSortTable(tableModel);
    tableModel.setTable(_filterTable);
    int columnCount = _filterTable.getColumnCount();
    for(int i = columnCount - 1; i >= 0; --i)
    {
      TableColumn tableColumn = _filterTable.getColumnModel().getColumn(i);
      if(columnNameToSortEnum.containsKey(tableColumn.getHeaderValue()))
      {
        SortEnum sortEnum = (SortEnum)columnNameToSortEnum.get(tableColumn.getHeaderValue());
        Assert.expect(sortEnum != null);
        int index = tableColumn.getModelIndex();
        tableModel = (FilterSortTableModel)_filterTable.getModel();
        if(sortEnum.equals(SortEnum.ASCENDING))
        {
          tableModel.sortColumn(index, true);
        }
        else if(sortEnum.equals(SortEnum.DESCENDING))
        {
          tableModel.sortColumn(index, false);
        }
        else if(sortEnum.equals(SortEnum.NONE))
        {
          //do nothing
        }
        else
        {
          //this should not happen
          Assert.expect(false);
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  void _editFilterButton_actionPerformed(ActionEvent e)
  {
    editFilter();
  }

  /**
   * @author George A. David
   */
  void createDefaultTable()
  {

    _filter = Filter.getDefaultFilter();
    createTable();

    setUpFilterTable();
  }

  /**
   * @author George A. David
   */
  private void setUpFilterTable()
  {
    FontMetrics fontMetrics = _filterTable.getFontMetrics(_filterTable.getFont());
    _filterTable.setRowHeight(fontMetrics.getMaxAscent() + fontMetrics.getMaxDescent() + fontMetrics.getLeading());
    Enumeration enumeration = _filterTable.getColumnModel().getColumns();
    while(enumeration.hasMoreElements())
    {
      TableColumn tableColumn = (TableColumn)enumeration.nextElement();
      String columnName = (String)tableColumn.getHeaderValue();
      int stringWidth = (int)Math.rint(fontMetrics.stringWidth(columnName) * 1.5);
      tableColumn.setPreferredWidth(stringWidth);
    }
    _filterTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        FilterSortTableModel tableModel = (FilterSortTableModel)_filterTable.getModel();
        if(_filterTable.getSelectedRowCount() == 0)
        {
          _imageScrollPanel.clearSelectedImages();
        }
        else if(_filterTable.getSelectedRowCount() == 1)
        {
          int rowIndex = _filterTable.getSelectedRow();
          ClassifiedJoint classifiedJoint = tableModel.getClassifiedJoint(rowIndex);
          if(_selectedImagePanel == null)
          {
            _imageScrollPanel.selectFirstImage();
            _selectedImagePanel = ImageIconPanel.getSelectedPanel();
          }
          if(classifiedJoint != _selectedImagePanel.getClassifiedJoint())
          {
            _imageScrollPanel.selectImage(classifiedJoint, true);
            _imageScrollPanel.scrollSelectedPanelToVisible();
          }
        }
        else
        {
          int[] rows = _filterTable.getSelectedRows();
          java.util.List classifiedJoints = tableModel.getClassifiedJoints(rows);
          _imageScrollPanel.selectImages(classifiedJoints, true);
        }

        updateGUI();
//        _selectedImagePanel.requestFocus();
      }

    });

    _filterTable.addMouseListener(new MouseListener()
    {
      public void mouseReleased(MouseEvent event)
      {
        _selectedImagePanel.requestFocus();
      }

      public void mousePressed(MouseEvent event) {}
      public void mouseClicked(MouseEvent event) {}
      public void mouseEntered(MouseEvent event) {}
      public void mouseExited(MouseEvent event) {}
    });

    _filterTable.getTableHeader().addMouseListener(new MouseListener()
    {
      public void mouseReleased(MouseEvent event)
      {
        TableColumnModel colModel = _filterTable.getColumnModel();
        int index = colModel.getColumnIndexAtX(event.getX());
        int modelIndex = colModel.getColumn(index).getModelIndex();

        FilterSortTableModel model = (FilterSortTableModel)_filterTable.getModel();
        if (model.isSortable(modelIndex))
        {
          setCursor(new Cursor(Cursor.WAIT_CURSOR));
          java.util.List classifiedJoints = model.getSortedClassifiedJoints();
          _imageScrollPanel.reorderImages(classifiedJoints);
          setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
        _selectedImagePanel.requestFocus();
      }

      public void mousePressed(MouseEvent event) {}
      public void mouseClicked(MouseEvent event) {}
      public void mouseEntered(MouseEvent event) {}
      public void mouseExited(MouseEvent event) {}
    });


    _filterTable.setAutoResizeMode(JSortTable.AUTO_RESIZE_OFF);
    _filterTableScrollPane.getViewport().removeAll();
    _filterTableScrollPane.getViewport().add(_filterTable);
  }

  /**
   * @author George A. David
   */
  void _editFilterMenuItem_actionPerformed(ActionEvent e)
  {
    editFilter();
  }

  /**
   * @author George A. David
   */
  void _showJointTableMenuItem_actionPerformed(ActionEvent e)
  {
    toggleTable();
    _selectedImagePanel.requestFocus();
  }

  /**
   * @author George A. David
   */
  private void setUpImageTable()
  {
    _imageScrollPanel = new ImageScrollPanel();
    ImageIconPanel.reset();
    FilterSortTableModel tableModel = (FilterSortTableModel)_filterTable.getModel();
    java.util.List classifiedJoints = tableModel.getSortedClassifiedJoints();
//    _imageScrollPanel.setNumberOfImages(classifiedJoints.size());
    Iterator it = classifiedJoints.iterator();
    while(it.hasNext())
    {
      final ClassifiedJoint classifiedJoint = (ClassifiedJoint)it.next();
      final ImageIconPanel iconPanel = new ImageIconPanel(classifiedJoint, _filterTable, _imageScrollPanel);
      iconPanel.addMouseListener(new MouseListener()
      {
        public void mouseEntered(MouseEvent e){}
        public void mouseExited(MouseEvent e){}
        public void mouseReleased(MouseEvent e){}
        public void mouseClicked(MouseEvent e){}
        public void mousePressed(MouseEvent e)
        {
          updateGUI();
        }
      });
      _imageScrollPanel.add(iconPanel);
    }

    _imageScrollPane.getViewport().removeAll();
    _imageScrollPane.getViewport().add(_imageScrollPanel);
    _imageScrollPane.setWheelScrollingEnabled(true);
    _imageScrollPane.getViewport().addChangeListener(new ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        _imageScrollPanel.updateImages();
      }

    });
    _imageScrollPanel.resize();
    if(_imageScrollPanel.containsImages())
    {
      _imageScrollPanel.selectFirstImage();
      _selectedImagePanel = ImageIconPanel.getSelectedPanel();
      _selectedImagePanel.setAsAnchorPanel();
      tableModel.selectRow(_selectedImagePanel.getClassifiedJoint(), true);
    }
  }

  /**
   * @author George A. David
   */
  void setImageControlButtonsEnabled(boolean enabled)
  {
    _nextJointToolBarButton.setEnabled(enabled);
    _previousJointToolBarButton.setEnabled(enabled);
    _badJointToolBarButton.setEnabled(enabled);
    _markAsReviewedToolBarButton.setEnabled(enabled);
    _goodJointToolBarButton.setEnabled(enabled);
    _zoomInToolBarButton.setEnabled(enabled);
    _fullImageToolBarButton.setEnabled(enabled);
  }

  /**
   * @author George A. David
   */
  void updateGUI()
  {
    int selectedPanelCount = ImageIconPanel.getSelectedPanelCount();
    if(selectedPanelCount == 1)
    {
      setImageControlButtonsEnabled(true);
      _selectedImagePanel = ImageIconPanel.getSelectedPanel();
      int imageCount = _selectedImagePanel.getImageCount();
      if(imageCount > 0)
      {
        int imageNumber= _selectedImagePanel.getCurrentImageNumber() + 1;
        _sliceLabel.setText("Slice " + imageNumber + " of " + imageCount);
        _nextSliceButton.setEnabled(true);
        if(imageCount == 1)
        {
          _nextSliceButton.setEnabled(false);
        }
      }
      else
      {
        _nextSliceButton.setEnabled(false);
        _sliceLabel.setText("");
      }
      if(_selectedImagePanel.areImagesLoaded())
      {
        _fullImageToolBarButton.setEnabled(true);
        _zoomInToolBarButton.setEnabled(true);
      }
      else
      {
        _fullImageToolBarButton.setEnabled(false);
        _zoomInToolBarButton.setEnabled(false);
      }

    }
    else if(selectedPanelCount > 1)
    {
//      _selectedImagePanel = null;
      setImageControlButtonsEnabled(true);
      _nextSliceButton.setEnabled(false);
      _sliceLabel.setText("");
      _zoomInToolBarButton.setEnabled(false);
      _fullImageToolBarButton.setEnabled(false);
    }
    else if(selectedPanelCount == 0)
    {
      _selectedImagePanel = null;
      _nextSliceButton.setEnabled(false);
      _sliceLabel.setText("");
      setImageControlButtonsEnabled(false);
    }

    updateStatusLabel();
  }

  /**
   * @author George A. David
   */
  private void updateStatusLabel()
  {
    int goodJoints = 0;
    int badJoints = 0;
    int markedForReview = 0;
    java.util.List classifiedJoints = ClassifiedJointLibrary.getClassifiedJoints();
    Iterator it = classifiedJoints.iterator();
    while(it.hasNext())
    {
      ClassifiedJoint classifiedJoint = (ClassifiedJoint)it.next();
      JointClassificationEnum jointClassification = classifiedJoint.getTrueClassification();
      if(jointClassification.equals(JointClassificationEnum.GOOD))
      {
        ++goodJoints;
      }
      else if(jointClassification.equals(JointClassificationEnum.BAD))
      {
        ++badJoints;
      }
      else
      {
        Assert.expect(false);
      }
      if(classifiedJoint.isReviewNeeded())
      {
        ++markedForReview;
      }
    }

    if(markedForReview > 0)
    {
      _isAtLeastOneJointMarkedForReview = true;
    }
    else
    {
      _isAtLeastOneJointMarkedForReview = false;
    }

    _statusLabel.setText("Status: " + goodJoints + " good joints, " +
                         badJoints + " bad joints, " +
                         markedForReview + " marked for review.");
  }

  /**
   * @author George A. David
   */
  void _nextSliceButton_actionPerformed(ActionEvent e)
  {
    Assert.expect(_selectedImagePanel != null);

    _selectedImagePanel.nextImage();
    int imageCount = _selectedImagePanel.getImageCount();
    int imageNumber= _selectedImagePanel.getCurrentImageNumber() + 1;
    _sliceLabel.setText("Slice " + imageNumber + " of " + imageCount);
    _nextSliceButton.setEnabled(true);
    _selectedImagePanel.requestFocus();
  }

  /**
   * @author George A. David
   */
  void _graphicsToggleToolBarButton_actionPerformed(ActionEvent e)
  {
    toggleGraphicsForImage();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void toggleGraphicsForImage()
  {
    ImageIconPanel.toggleGrahics();
    focusSelectedImage();
    _imageScrollPanel.repaint();
  }

  /**
   * @author George A. David
   */
  void _nextJointToolBarButton_actionPerformed(ActionEvent e)
  {
    goToNextJoint();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void goToNextJoint()
  {
    ImageIconPanel imagePanel = ImageIconPanel.getSelectedPanel();
    _imageScrollPanel.selectNextImage(imagePanel);
    updateGUI();
    FilterSortTableModel tableModel = (FilterSortTableModel)_filterTable.getModel();
    tableModel.selectRow(_selectedImagePanel.getClassifiedJoint(), true);
  }

  /**
   * @author George A. David
   */
  void _previousJointToolBarButton_actionPerformed(ActionEvent e)
  {
    goToPreviousJoint();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void goToPreviousJoint()
  {
    ImageIconPanel imagePanel = ImageIconPanel.getSelectedPanel();
    _imageScrollPanel.selectPreviousImage(imagePanel);
    updateGUI();
    FilterSortTableModel tableModel = (FilterSortTableModel)_filterTable.getModel();
    tableModel.selectRow(_selectedImagePanel.getClassifiedJoint(), true);
  }

  /**
   * @author George A. David
   */
  void _markAsReviewedToolBarButton_actionPerformed(ActionEvent e)
  {
    markJointsAsReviewed();
 }

 /**
  * @author Erica Wheatcroft
  */
 void  _markAsReviewMenuItem_actionPerformed(ActionEvent e)
 {
   markJointsAsReviewed();
 }

 /**
  * @author Erica Wheatcroft
  */
 void  _markAsGoodMenuItem_actionPerformed(ActionEvent e)
 {
   classifyJointAsGood();
 }

 /**
  * @author Erica Wheatcroft
  */
 void  _markAsBadMenuItem_actionPerformed(ActionEvent e)
 {
   classifyJointAsBad();
 }

 /**
  * @author Erica Wheatcroft
  */
 void  _markForReviewMenuItem_actionPerformed(ActionEvent e)
 {
   markJointsForReview();
 }

 /**
  * @author Erica Wheatcroft
  */
  private void markJointsAsReviewed()
  {
    _isDatabaseDirty = true;
    FilterSortTableModel tableModel = (FilterSortTableModel)_filterTable.getModel();
    java.util.List selectedJoints = tableModel.getSelectedClassifiedJoints();
    Iterator it = selectedJoints.iterator();
    while(it.hasNext())
    {
      ClassifiedJoint selectedJoint = (ClassifiedJoint)it.next();
      selectedJoint.setReviewNeeded(false);
    }
    _filterTable.repaint();
    updateStatusLabel();
    focusSelectedImage();

  }

  /**
   * @author Erica Wheatcroft
   */
  private void focusSelectedImage()
  {
    if(ImageIconPanel.getSelectedPanelCount() > 0)
      ImageIconPanel.getSelectedPanel();
    else
      _selectedImagePanel.requestFocus();
  }

  /**
   * @author George A. David
   */
  void _badJointToolBarButton_actionPerformed(ActionEvent e)
  {
    classifyJointAsBad();
  }

  private void classifyJointAsBad()
  {
    _isDatabaseDirty = true;
    changeClassificationOnSelectedJoints(JointClassificationEnum.BAD);
    focusSelectedImage();
  }

  /**
   * @author George A. David
   */
  void changeClassificationOnSelectedJoints(JointClassificationEnum jointClassification)
  {
    Assert.expect(jointClassification != null);

    FilterSortTableModel tableModel = (FilterSortTableModel)_filterTable.getModel();
    java.util.List selectedJoints = tableModel.getSelectedClassifiedJoints();
    Iterator it = selectedJoints.iterator();
    while(it.hasNext())
    {
      ClassifiedJoint selectedJoint = (ClassifiedJoint)it.next();
      selectedJoint.setTrueClassification(jointClassification);
    }
    _filterTable.repaint();
    _imageScrollPanel.repaint();
    updateStatusLabel();
  }

  /**
   * @author George A. David
   */
  void _deleteMenuItem_actionPerformed(ActionEvent e)
  {
    deleteImages();
  }

  /**
   * This function will delete the selected Images.
   * @author Erica Wheatcroft
   */
  private void deleteImages()
  {
    String message = "Are you sure you want to delete the selected joints?";
    int response = JOptionPane.showConfirmDialog(this, message);
    if(response == JOptionPane.YES_OPTION)
    {
      _isDatabaseDirty = true;
      FilterSortTableModel tableModel = (FilterSortTableModel)_filterTable.getModel();
      java.util.List deletedClassifiedJoints = tableModel.getSelectedClassifiedJoints();
      tableModel.deleteSelectedRows();
      _filterTable = new JSortTable(tableModel);
      tableModel.setTable(_filterTable);
      setUpFilterTable();
      ImageIconPanel nextImagePanel = null;
      if(_imageScrollPanel.areAllImagesSelected() == false)
      {
        nextImagePanel = _imageScrollPanel.getNextUnselectedPanel();
      }
      _imageScrollPanel.delectSelectedImages();
      if(nextImagePanel != null)
      {
        nextImagePanel.select();
        nextImagePanel.setAsAnchorPanel();
        tableModel.selectRow(nextImagePanel.getClassifiedJoint(), true);
        nextImagePanel.requestFocus();
      }
      ClassifiedJointLibrary.delete(deletedClassifiedJoints);
      _imageScrollPanel.scrollSelectedPanelToVisible();
      updateStatusLabel();
    }
  }

  /**
   * @author George A. David
   */
  void _selectAllToolBarButton_actionPerformed(ActionEvent e)
  {
    selectAll();
    focusSelectedImage();
  }

  /**
   * @author George A. David
   */
  void _markForReviewToolBarButton_actionPerformed(ActionEvent e)
  {
    markJointsForReview();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void markJointsForReview()
  {
    _isDatabaseDirty = true;
    FilterSortTableModel tableModel = (FilterSortTableModel)_filterTable.getModel();
    java.util.List selectedJoints = tableModel.getSelectedClassifiedJoints();
    Iterator it = selectedJoints.iterator();
    while(it.hasNext())
    {
      ClassifiedJoint selectedJoint = (ClassifiedJoint)it.next();
      selectedJoint.setReviewNeeded(true);
    }
    _filterTable.repaint();
    updateStatusLabel();
    focusSelectedImage();
  }

  /**
   * @author George A. David
   */
  void _goodJointToolBarButton_actionPerformed(ActionEvent e)
  {
    classifyJointAsGood();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void classifyJointAsGood()
  {
    _isDatabaseDirty = true;
    changeClassificationOnSelectedJoints(JointClassificationEnum.GOOD);
    focusSelectedImage();
  }

  /**
   * @author George A. David
   */
  private void openDatabase()
  {
    if(doesDatabaseNeedSaving())
    {
      promptUserToSaveDatabase();
    }

    boolean openNewDatabase = true;
    if(_isAtLeastOneJointMarkedForReview)
    {
      openNewDatabase = confirmDatabaseClosureWithJointsMarkedForReview();
    }

    if(openNewDatabase)
    {
      JFileChooser adbFileChooser = getAsciiDataBaseFileChooser();
      if(adbFileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
      {
        _currentFileName = adbFileChooser.getSelectedFile().getPath();
        load(_currentFileName);
      }
      else
      {
        if(_selectedImagePanel != null)
        {
          _selectedImagePanel.requestFocus();
        }
      }
      _isDatabaseDirty = false;
    }
  }

  /**
   * @author George A. David
   */
  private boolean confirmDatabaseClosureWithJointsMarkedForReview()
  {
    boolean confirmClosure = false;
    String message = "There are one or more joints still marked for review.\n" +
                     "Do you wish to continue closing the current database?";
    int response = JOptionPane.showConfirmDialog(this,
                                                 message,
                                                 "Classifier Tool",
                                                 JOptionPane.YES_NO_OPTION);
    if(response == JOptionPane.YES_OPTION)
    {
      confirmClosure = true;
    }

    return confirmClosure;
  }

  /**
   * @author George A. David
   */
  void _openToolBarButton_actionPerformed(ActionEvent e)
  {
    openDatabase();
  }

  /**
   * @author George A. David
   */
  private JFileChooser getAsciiDataBaseFileChooser()
  {
    JFileChooser adbFileChooser = new JFileChooser(_adbFileDirectory);
    adbFileChooser.setFileFilter(new AsciiDatabaseFileFilter());
    adbFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    adbFileChooser.setMultiSelectionEnabled(false);

    return adbFileChooser;
  }

  /**
   * @author George A. David
   */
  private void saveDatabase()
  {
    // we only want to save if there has been a change to the database.
    if( _isDatabaseDirty == true)
    {
      /***** JMH omit "can't undo" prompt ****************************
      int response = JOptionPane.showConfirmDialog(this,
                                                 "You will not be able to reverse your changes after you save.\n Do you wish to continue?",
                                                 "Classifier Tool",
                                                 JOptionPane.YES_NO_OPTION,
                                                 JOptionPane.INFORMATION_MESSAGE);
      if(response == JOptionPane.YES_OPTION)
      {
      ****************************************************************/
        try
        {
          setCursor(new Cursor(Cursor.WAIT_CURSOR));
          ClassifiedJointLibrary.save(_currentFileName);
          setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
          _isDatabaseDirty = false;
          _filterTable.repaint();
          updateStatusLabel();
        }
        catch(DatastoreException dex)
        {
          //this should not happen
          dex.printStackTrace();
          Assert.expect(false);
        }
      /*****
      }
       *****/
    }
    /***** JMH silently do nothing, omit prompt **********************
    else
    {
      JOptionPane.showMessageDialog(this, "You have not made any changes to the database, the save will not be performed.",
                                    "Save will not be performed", JOptionPane.OK_OPTION);
    }
    ******************************************************************/
  }

  /**
   * @author Erica Wheatcroft
   */
  private void saveDatabaseAs()
  {
    // open a file dialog to $agt5dx_Stage2_Dir/fpgw.adb
    JFileChooser adbFileChooser = getAsciiDataBaseFileChooser();
    if(adbFileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION)
    {
      _currentFileName = adbFileChooser.getSelectedFile().getPath();
    }

    try
    {
      ClassifiedJointLibrary.save(_currentFileName);
      _isDatabaseDirty = false;
      updateStatusLabel();
    }
    catch(DatastoreException dex)
    {
      //this should not happen
      dex.printStackTrace();
      Assert.expect(false);
    }

  }

  /**
   * @author George A. David
   */
  void _saveMenuItem_actionPerformed(ActionEvent e)
  {
    saveDatabase();
  }

 /**
  * @author Erica Wheatcroft
  */
 void _saveAsMenuItem_actionPerformed(ActionEvent e)
 {
   saveDatabaseAs();
   focusSelectedImage();
 }


  /**
   * @author George A. David
   */
  void _loadMenuItem_actionPerformed(ActionEvent e)
  {
    openDatabase();
  }

  /**
   * @author George A. David
   */
  void _saveToolBarButton_actionPerformed(ActionEvent e)
  {
    saveDatabase();
    focusSelectedImage();
  }

  /**
   * @author George A. David
   */
  void _selectAllMenuItem_actionPerformed(ActionEvent e)
  {
    selectAll();
    focusSelectedImage();
  }

  /**
   * @author George A. David
   */
  void _zoomInToolBarButton_actionPerformed(ActionEvent e)
  {
    showZoomedInImage();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void showZoomedInImage()
  {
    Assert.expect(_selectedImagePanel != null);

    _selectedImagePanel.showZoomedImage(this);
    _selectedImagePanel.requestFocus();
  }

  /**
   * @author George A. David
   */
  void _fullImageToolBarButton_actionPerformed(ActionEvent e)
  {
    showFullImage();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void showFullImage()
  {
    Assert.expect(_selectedImagePanel != null);

    _selectedImagePanel.showFullImage(this);
    _selectedImagePanel.requestFocus();
  }

  /**
   * @author George A. David
   */
  private void stopClassifier()
  {
    String killFilePath = Directory. getLegacyResultsDir() + File.separator +
                          "killstg2.sig";

    File killFile = new File(killFilePath);
    try
    {
      killFile.createNewFile();
      _runClassifierButton.setText("Run Classifier");
      _classifierIsRunning = false;
    }
    catch(IOException ioe)
    {
      String errorMessage =  new CannotCreateFileDatastoreException(killFilePath).getLocalizedMessage();
      JOptionPane.showMessageDialog(getParent(), errorMessage, "Stop Classifier Error", JOptionPane.ERROR_MESSAGE);
    }
  }


  /**
   * @author George A. David
   */
  void _runClassifierButton_actionPerformed(ActionEvent e)
  {
    if(_classifierIsRunning)
    {
      stopClassifier();
    }
    else
    {
      _classifierIsRunning = true;
      _runClassifierButton.setText("Stop Classifier");

      String command = "perl " + Directory.get5dxReleaseDir() + File.separator +
                       "classify" + File.separator + "stage2.pl fpgw";
      runCommand(command);

    }
  }

  /**
   * @author George A. David
   */
  public static ClassifierToolFrame getInstance()
  {
    if(_instance == null)
    {
      _instance = new ClassifierToolFrame();
    }

    return _instance;
  }

  /**
   * @author George A. David
   */
  public void showScriptProgress(final String dialogTitle, final String logFileName, final boolean consistencyCheckScript)
  {
    Assert.expect(dialogTitle != null);
    Assert.expect(logFileName != null);
    _scriptProgressBar.setValue(0);
    final JDialog progressDialog = new JDialog(this, dialogTitle, true);
    progressDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    progressDialog.getContentPane().add(_scriptProgressBarPanel);
    progressDialog.setCursor(new Cursor(Cursor.WAIT_CURSOR));
    progressDialog.pack();
    SwingUtils.centerOnComponent(progressDialog, this);
    final PercentProgressSetableInterface percentProgressSetable = this;
    _workerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        ClassifierScriptLogFileReader logFileReader = new ClassifierScriptLogFileReader();
        int numberOfAttempts = 0;
        boolean finishedReading = false;
        while(numberOfAttempts < 3 && finishedReading == false)
        try
        {
          try
          {
            _workerThread.sleep(3000);
          }
          catch(InterruptedException ie)
          {
            //do nothing
          }
          logFileReader.read(logFileName, percentProgressSetable);
          if(consistencyCheckScript)
          {
            SwingUtilities.invokeLater(new Runnable()
            {
              public void run()
              {
                try
                {
                  ClassifierCheckConsistencyLogFileReader consistencyLogFileReader = new ClassifierCheckConsistencyLogFileReader();
                  String results = consistencyLogFileReader.read(logFileName);
                  JOptionPane.showMessageDialog(getParent(), results, dialogTitle, JOptionPane.INFORMATION_MESSAGE);
                }
                catch(DatastoreException de)
                {
                  JOptionPane.showMessageDialog(getParent(), de.getLocalizedMessage(), dialogTitle, JOptionPane.ERROR_MESSAGE);
                }
              }
            });
          }

          finishedReading = true;
        }
        catch(DatastoreException dex)
        {
          ++numberOfAttempts;
          if(numberOfAttempts >= 3)
          {
            final String errorMessage = dex.getLocalizedMessage();
            SwingUtilities.invokeLater(new Runnable()
            {
              public void run()
              {
                JOptionPane.showMessageDialog(getParent(), errorMessage, dialogTitle, JOptionPane.ERROR_MESSAGE);
              }
            });
          }
          else
          {
            continue;
          }
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        SwingUtilities.invokeLater(new Runnable()
        {
          public void run()
          {
            progressDialog.dispose();
          }
        });
      }
    });
    progressDialog.show();
  }

  /**
   * @author George A. David
   */
  private void runCommand(final String command)
  {
    new WorkerThread("Run PerlScripts").invokeLater(new Runnable()
    {
      public void run()
      {
        _dosShell.sendCommand(command);
      }
    });
  }


  /**
   * @author George A. David
   */
  void _trainClassifierButton_actionPerformed(ActionEvent e)
  {
    if(_classifierIsRunning)
    {
      stopClassifier();
    }

    String logFileName = Directory.get5dxReleaseDir() + File.separator +
                         "classify" + File.separator + "trainfpgw.log";
    //if the log file exists, its from the last time it was run. we need to delete it.
    File file = new File(logFileName);
    if(file.exists())
    {
      file.delete();
    }

    String command = "perl " + Directory.get5dxReleaseDir() + File.separator +
                     "classify" + File.separator + "train.pl fpgw";

    _runClassifierButton.setEnabled(false);
    _predictPerformanceButton.setEnabled(false);
    _manualReviewOfJointsButton.setEnabled(false);
    _consistencyCheckButton.setEnabled(false);
    _exitButton.setEnabled(false);

    runCommand(command);
    showScriptProgress(_trainClassifierButton.getText(), logFileName, false);

    _trainClassifierButton.setEnabled(true);
    _runClassifierButton.setEnabled(true);
    _predictPerformanceButton.setEnabled(true);
    _manualReviewOfJointsButton.setEnabled(true);
    _consistencyCheckButton.setEnabled(true);
    _exitButton.setEnabled(true);
  }

  /**
   * @author George A. David
   */
  void _predictPerformanceButton_actionPerformed(ActionEvent e)
  {
    if(_classifierIsRunning)
    {
      stopClassifier();
    }

    String logFileName = Directory.get5dxReleaseDir() + File.separator +
                         "classify" + File.separator + "predictfpgw.log";
    //if the log file exists, its from the last time it was run. we need to delete it.
    File file = new File(logFileName);
    if(file.exists())
    {
      file.delete();
    }

    _consistencyCheckButton.setEnabled(false);
    _trainClassifierButton.setEnabled(false);
    _runClassifierButton.setEnabled(false);
    _manualReviewOfJointsButton.setEnabled(false);
    _exitButton.setEnabled(false);

    String command = "perl " + Directory.get5dxReleaseDir() + File.separator +
                   "classify" + File.separator + "predict.pl fpgw";
    runCommand(command);
    showScriptProgress(_predictPerformanceButton.getText(), logFileName, false);
    command = Directory.get5dxReleaseDir() + File.separator +
              "classify" + File.separator + "predictfpgw.pdf";
    runCommand(command);

    _consistencyCheckButton.setEnabled(true);
    _trainClassifierButton.setEnabled(true);
    _runClassifierButton.setEnabled(true);
    _manualReviewOfJointsButton.setEnabled(true);
    _exitButton.setEnabled(true);
  }

  /**
   * @author George A. David
   */
  void _consistencyCheckButton_actionPerformed(ActionEvent e)
  {
    if(_classifierIsRunning)
    {
      stopClassifier();
    }
    String logFileName = Directory.get5dxReleaseDir() + File.separator +
                         "classify" + File.separator + "checkfpgw.log";
    //if the log file exists, its from the last time it was run. we need to delete it.
    File file = new File(logFileName);
    if(file.exists())
    {
      file.delete();
    }
    String command = "perl.exe " + Directory.get5dxReleaseDir() + File.separator +
                   "classify" + File.separator + "check.pl fpgw";

    _trainClassifierButton.setEnabled(false);
    _runClassifierButton.setEnabled(false);
    _manualReviewOfJointsButton.setEnabled(false);
    _exitButton.setEnabled(false);
    _predictPerformanceButton.setEnabled(false);

    runCommand(command);
    showScriptProgress(_consistencyCheckButton.getText(), logFileName, true);

    _trainClassifierButton.setEnabled(true);
    _runClassifierButton.setEnabled(true);
    _manualReviewOfJointsButton.setEnabled(true);
    _exitButton.setEnabled(true);
    _predictPerformanceButton.setEnabled(true);

  }

  /**
   * @author George A. David
   */
  public void setPercentProgress(final double percentProgress)
  {
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        _scriptProgressBar.setValue((int)percentProgress);
      }
    });
  }

  /**
   * @author George A. David
   */
  void _exitMenuItem_actionPerformed(ActionEvent e)
  {
    closeManualReviewOfJoints();
  }

  /**
   * @author Erica Wheatcroft
   */
  void _nextImageMenuItem_actionPerformed(ActionEvent e)
  {
    goToNextJoint();
  }

  /**
   * @author Erica Wheatcroft
   */
  void _previousImageMenuItem_actionPerformed(ActionEvent e)
  {
    goToPreviousJoint();
  }

  /**
   * @author Erica Wheatcroft
   */
  void _showFullImageMenuItem_actionPerformed(ActionEvent e)
  {
    showFullImage();
  }

  /**
   * @author Erica Wheatcroft
   */
  void _showZoomedInImageMenuItem_actionPerformed(ActionEvent e)
  {
    showZoomedInImage();
  }

  /**
   * @author Erica Wheatcroft
   */
  void _toggleGraphicsMenuItem_actionPerformed(ActionEvent e)
  {
    toggleGraphicsForImage();
  }
}
