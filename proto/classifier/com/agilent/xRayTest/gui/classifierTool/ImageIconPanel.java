package proto.classifier.com.agilent.xRayTest.gui.classifierTool;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.agilent.util.*;
import com.agilent.xRayTest.datastore.panelDesc.*;
import proto.classifier.com.agilent.xRayTest.datastore.classifierTool.*;

/**
 * This class draws the image of a classified joint.
 * The image is 1024 x 1024 in pixels and is cropped to 150 x 150
 * @author George A. David
 */
class ImageIconPanel extends JPanel
{
  private static java.util.List _selectedPanels = new ArrayList();
  private static ImageIconPanel _shiftAnchorPanel = null;
  private static Map _panels = new HashMap();
  public static Color _GOOD_JOINT_COLOR = Color.GREEN.darker().darker();
  public static Color _BAD_JOINT_COLOR = Color.RED;
  private static Color _UNKOWN_JOINT_COLOR = Color.BLACK;
  private static final String _LOADING_IMAGE = "Loading image...";
  private static final String _IMAGE_DOES_NOT_EXIST = "Image does not exist.";
  public static boolean _drawJointGraphics = true;
  private java.util.List _images = new ArrayList();
  private Rectangle2D _selectionBox = new Rectangle2D.Double(0, 0, _IMAGE_WIDTH, _IMAGE_HEIGHT);
  private Shape _jointBox = new Rectangle2D.Double();
  private JLabel _label = null;
  private static int _IMAGE_WIDTH = 150;
  private static int _IMAGE_HEIGHT = 150;
  private int _imageXCoordinate = 0;
  private int _imageYCoordinate = 0;
  private boolean _isSelected = false;
  private boolean _areImagesLoaded = false;
  private Dimension _dimension = new Dimension(_IMAGE_WIDTH + 1, _IMAGE_HEIGHT + 1);
  private Image _image = null;
  private int _currentImageNumber = 0;
  private ClassifiedJoint _classifiedJoint = null;
  private JTable _table = null;
  private ImageScrollPanel _parentPanel = null;

  /**
   * @author George A. David
   */
  ImageIconPanel(ClassifiedJoint classifiedJoint, JTable table, ImageScrollPanel parentPanel)
  {
    Assert.expect(classifiedJoint != null);
    Assert.expect(table != null);
    Assert.expect(parentPanel != null);

    _classifiedJoint = classifiedJoint;
    _table = table;
    _parentPanel = parentPanel;

    //calculate the center of the joint in question
    IntCoordinate coordinate = _classifiedJoint.getCoordinate();
    double jointXCenter = coordinate.getX();
    double jointYCenter = coordinate.getY();
    Point2D point = new Point2D.Double(coordinate.getX() - (_classifiedJoint.getWidth() / 2.0),
                                       coordinate.getY() - (_classifiedJoint.getLength() / 2.0));

    //if the center is too close to the edge of the image,
    //then we cannot center the joint. So we will adjust the
    //image to fit the 150 x 150 dimension
    if( (jointXCenter + 75) > 1024)
    {
      double offset = (jointXCenter + 75) - 1024;
      jointXCenter -= offset;
      if(jointXCenter < 0)
      {
        jointXCenter = 512;
      }
    }
    if( (jointXCenter - 75) < 0)
    {
      double offset = (75 - jointXCenter);
      jointXCenter += offset;
      if(jointXCenter > 1024)
      {
        jointXCenter = 512;
      }
    }
    if( (jointYCenter + 75) > 1024)
    {
      double offset = (jointYCenter + 75) - 1024;
      jointYCenter -= offset;
      if(jointYCenter < 0)
      {
        jointYCenter = 512;
      }
    }
    if( (jointYCenter - 75) < 0)
    {
      double offset = (75 - jointYCenter);
      jointYCenter += offset;
      if(jointYCenter > 1024)
      {
        jointYCenter = 512;
      }
    }

    _imageXCoordinate = (int)Math.rint(jointXCenter - 75);
    _imageYCoordinate = (int)Math.rint(jointYCenter - 75);
    ShapeEnum shapeEnum = _classifiedJoint.getPadShape();
    if(shapeEnum.equals(ShapeEnum.RECTANGLE))
    {
      _jointBox = new Rectangle2D.Double(point.getX() - _imageXCoordinate, point.getY() - _imageYCoordinate, _classifiedJoint.getWidth(), _classifiedJoint.getLength());
    }
    else if(shapeEnum.equals(ShapeEnum.CIRCLE))
    {
      _jointBox = new Ellipse2D.Double(point.getX() - _imageXCoordinate, point.getY() - _imageYCoordinate, _classifiedJoint.getWidth(), _classifiedJoint.getLength());
    }
    else
    {
      Assert.expect(false);
    }

    addMouseListener(new MouseListener()
    {
      public void mouseEntered(MouseEvent e){}
      public void mouseExited(MouseEvent e){}
      public void mouseReleased(MouseEvent e){}
      public void mouseClicked(MouseEvent e)
      {
        if(e.getClickCount() > 1)
        {
          if(_areImagesLoaded)
          {
            showFullImage(ClassifierToolFrame.getInstance());
          }
        }
      }
      public void mousePressed(MouseEvent e)
      {
        boolean clearPreviousSelections = false;

        ImageIconPanel iconPanel = (ImageIconPanel)e.getSource();
        FilterSortTableModel tableModel = (FilterSortTableModel)_table.getModel();

        if(e.isControlDown() == false)
        {
          if(e.isShiftDown() == false)
          {
            clearSelectedPanels();
            _shiftAnchorPanel = iconPanel;
            clearPreviousSelections = true;
            iconPanel.setSelected(true);
            ImageIconPanel.addSelectedPanel(iconPanel);
            tableModel.selectRow(iconPanel.getClassifiedJoint(), clearPreviousSelections);
            iconPanel.requestFocus();
          }
          else
          {
//            if(_shiftAnchorPanel == null)
//              _shiftAnchorPanel = ImageIconPanel.getLastSelectedPanel();
            _parentPanel.selectImages(_shiftAnchorPanel, iconPanel);
            tableModel.selectRows(_shiftAnchorPanel.getClassifiedJoint(), iconPanel.getClassifiedJoint());
            iconPanel.requestFocus();
          }
        }
        else
        {
          _shiftAnchorPanel = iconPanel;
          if(iconPanel.isSelected())
          {
            iconPanel.setSelected(false);
            ImageIconPanel.removeSelectedPanel(iconPanel);
            tableModel.clearSelectedRow(iconPanel.getClassifiedJoint());
          }
          else
          {
            iconPanel.setSelected(true);
            ImageIconPanel.addSelectedPanel(iconPanel);
            tableModel.selectRow(iconPanel.getClassifiedJoint(), clearPreviousSelections);
          }
        }
      }
    });

    _label = new JLabel(_LOADING_IMAGE);
    add(_label);
    setFocusable(true);
    setMaximumSize(_dimension);
    setMinimumSize(_dimension);
    setPreferredSize(_dimension);
    setSize(_dimension);
    setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
    setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
  }

  /**
   * @author George A. David
   */
  public void paint(Graphics graphics)
  {
    super.paint(graphics);
    Assert.expect(graphics != null);

    Graphics2D graphics2D = (Graphics2D)graphics;
    if(_areImagesLoaded)
    {
      ImageIcon imageIcon = (ImageIcon)_images.get(_currentImageNumber);
      imageIcon.paintIcon(this, graphics2D, 0, 0);
      if(_drawJointGraphics)
      {
        Color previousColor = graphics2D.getColor();
        JointClassificationEnum jointClassification = _classifiedJoint.getTrueClassification();
        if(jointClassification.equals(JointClassificationEnum.BAD))
        {
          graphics2D.setColor(_BAD_JOINT_COLOR);
        }
        else if(jointClassification.equals(JointClassificationEnum.GOOD))
        {
          graphics2D.setColor(_GOOD_JOINT_COLOR);
        }

        graphics2D.draw(_jointBox);
        graphics2D.setColor(previousColor);
      }
    }

    if(_isSelected)
    {
      Color color = graphics2D.getColor();
      graphics2D.setColor(Color.BLUE);
      graphics2D.draw(_selectionBox);
      graphics2D.setColor(color);
    }
  }

  /**
   * @author George A. David
   */
  public void setAsAnchorPanel()
  {
    _shiftAnchorPanel = this;
  }

  /**
   * @author George A. David
   */
  public void select()
  {
    if(_isSelected == false)
    {
      setSelected(true);
      _selectedPanels.add(this);
    }
  }

  /**
   * @author George A. David
   */
  private void setSelected(boolean isSelected)
  {
    _isSelected = isSelected;
    repaint();
  }

  /**
   * @author George A. David
   */
  public boolean isSelected()
  {
    return _isSelected;
  }

  /**
   * @author George A. David
   */
  public ClassifiedJoint getClassifiedJoint()
  {
    Assert.expect(_classifiedJoint != null);

    return _classifiedJoint;
  }

  /**
   * @author George A. David
   */
  public int getImageCount()
  {
    return _images.size();
  }

  /**
   * @author George A. David
   */
  public int getCurrentImageNumber()
  {
    Assert.expect(_currentImageNumber >= 0 && _currentImageNumber < _images.size());

    return _currentImageNumber;
  }

  /**
   * @author George A. David
   */
  public void nextImage()
  {
    ++_currentImageNumber;
    if(_currentImageNumber >= _images.size())
    {
      _currentImageNumber = 0;
    }
    repaint();
  }

  /**
   * @author George A. David
   */
  ImagePanel getFullImage()
  {
    Assert.expect(_areImagesLoaded);

    return new ImagePanel(_classifiedJoint, _currentImageNumber);
  }

  /**
   * @author George A. David
   */
  void showFullImage(JFrame frame)
  {
    Assert.expect(_areImagesLoaded);

    ImagePanel fullImage = getFullImage();
    fullImage.showFullImage(frame);
  }

  /**
   * @author George A. David
   */
  void showZoomedImage(JFrame frame)
  {
    Assert.expect(_areImagesLoaded);

    ImagePanel fullImage = getFullImage();
    fullImage.showZoomedImage(frame);
  }


  /**
   * @author George A. David
   */
  public void loadImages()
  {
    ImageFilter filter = new CropImageFilter(_imageXCoordinate,
                                             _imageYCoordinate,
                                             _IMAGE_WIDTH,
                                             _IMAGE_HEIGHT);

    java.util.List imageFileNames = _classifiedJoint.getViewFileNames();
    Iterator it = imageFileNames.iterator();
    while(it.hasNext())
    {
      String imageFileName = (String)it.next();
      ImageIcon icon = new ImageIcon(imageFileName);
      Image image = icon.getImage();
      ImageProducer imageProducer = new FilteredImageSource(image.getSource(), filter);
      icon = new ImageIcon(createImage(imageProducer));

      _images.add(icon);
    }
  }

  /**
   * @author George A. David
   */
  public void setAreImagesLoaded(boolean areImagesLoaded)
  {
    _areImagesLoaded = areImagesLoaded;
    if(_areImagesLoaded)
    {
      if(_images.size() > 0)
      {
        _currentImageNumber = 0;
      }
      else
      {
        _areImagesLoaded = false;
        _label.setText(_IMAGE_DOES_NOT_EXIST);
      }
      repaint();
    }
  }

  /**
   * @author George A. David
   */
  public boolean areImagesLoaded()
  {
    return _areImagesLoaded;
  }

  /**
   * @author George A. David
   */
  public void clearImages()
  {
    _currentImageNumber = -1;
    _areImagesLoaded = false;
    _label.setText(_LOADING_IMAGE);
    _images.clear();
    repaint();
  }

  /**
   * @author George A. David
   */
  private static void addSelectedPanel(ImageIconPanel selectedPanel)
  {
    Assert.expect(selectedPanel != null);

    _selectedPanels.add(selectedPanel);
  }

  /**
   * @author George A. David
   */
  private static void removeSelectedPanel(ImageIconPanel selectedPanel)
  {
    Assert.expect(selectedPanel != null);

    _selectedPanels.remove(selectedPanel);
  }

  /**
   * @author George A. David
   */
  public static int getSelectedPanelCount()
  {
    return _selectedPanels.size();
  }

  /**
   * Returns the first selected panel in the list
   * @author George A. David
   */
  public static ImageIconPanel getSelectedPanel()
  {
    Assert.expect(_selectedPanels.size() > 0);

    return (ImageIconPanel)_selectedPanels.get(0);
  }

  /**
   * @author George A. David
   */
  public static ImageIconPanel getLastSelectedPanel()
  {
    Assert.expect(_selectedPanels.size() > 0);

    return (ImageIconPanel)_selectedPanels.get(_selectedPanels.size() - 1);
  }

  /**
   * @author George A. David
   */
  public static void clearSelectedPanels()
  {
    Iterator it = _selectedPanels.iterator();
    while(it.hasNext())
    {
      ImageIconPanel iconPanel = (ImageIconPanel)it.next();
      iconPanel.setSelected(false);
    }
    _selectedPanels.clear();
//    _shiftAnchorPanel = null;
  }

  /**
   * @author George A. David
   */
  public static java.util.List getSelectedPanels()
  {
    return _selectedPanels;
  }

  /**
   * @author George A. David
   */
  public static void toggleGrahics()
  {
    _drawJointGraphics = !_drawJointGraphics;
  }

  /**
   * @author George A. David
   */
  public static void reset()
  {
    _selectedPanels.clear();
    _shiftAnchorPanel = null;
    _panels.clear();
  }
}