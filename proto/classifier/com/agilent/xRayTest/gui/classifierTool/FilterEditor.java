package proto.classifier.com.agilent.xRayTest.gui.classifierTool;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.filechooser.*;
import javax.swing.table.*;
import java.util.*;

import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.images.*;
import com.agilent.util.*;
import proto.classifier.com.agilent.xRayTest.datastore.classifierTool.*;

/**
 * @author George A. David
 */
public class FilterEditor extends JDialog
{
  private JPanel _filterEditorMainPanel = new JPanel();
  private JPanel _chooseFilterPanel = new JPanel();
  private JPanel _viewCurrentFilterPanel = new JPanel();
  private JPanel _filterActionButtonPanel = new JPanel();
  private JPanel _filterPriorityPanel = new JPanel();
  private JPanel _labelPanel = new JPanel();
  private JPanel _innerLabelPanel = new JPanel();
  private JLabel _fieldLabel = new JLabel();
  private JLabel _showOnlyLabel = new JLabel();
  private JLabel _sortByLabel = new JLabel();
  private GridLayout _innerLabelPanelGridLayout = new GridLayout();
  private FlowLayout _labelPanelFlowLayout = new FlowLayout();
  private JPanel _chooseFilterLabelPanel = new JPanel();
  private FlowLayout _avialableColumnsLabelPanelFlowLayout = new FlowLayout();
  private JLabel _availableColumnsLabel = new JLabel();
  private JPanel _availableColumnsLabelPanel = new JPanel();
  private JPanel _innerFilterActionButtonPanel = new JPanel();
  private JButton _saveFilterButton = new JButton();
  private JButton _loadFilterButton = new JButton();
  private JButton _defaultFilterButon = new JButton();
  private JButton _cancelButton = new JButton();
  private JButton _updateButton = new JButton();
  private GridLayout _innerFilterActonButtonPanelGridLayout = new GridLayout();
  private FlowLayout _filterActionButtonPanelFlowLayout = new FlowLayout();
  private BorderLayout _viewCurrentFilterPanelBorderLayout = new BorderLayout();
  private BorderLayout _filterEditorMainPanelBorderLayout = new BorderLayout();
  private Border _emptySpaceBorder = null;
  private JPanel _chooseFilterActionPanel = new JPanel();
  private GridLayout _chooseFilterActionPanelGridLayout = new GridLayout();
  private JPanel _emptyLabelPanel = new JPanel();
  private JLabel _fielldsDisplayedLabel = new JLabel();
  private FlowLayout _fieldsDisplayedLabelPanelFlowLayout = new FlowLayout();
  private JPanel _filedsDisplayedLabelPanel = new JPanel();
  private JLabel _emptyLabel = new JLabel();
  private GridLayout _chooseFilterLabelPanelGridLayout = new GridLayout();
  private BorderLayout _chooseFilterPanelBorderLayout = new BorderLayout();
  private JList _fieldsDisplayedListBox = new JList();
  private FlowLayout _emptyLabelPanelFlowLayout = new FlowLayout();
  private JScrollPane _availableColumnsScrollPane = new JScrollPane();
  private JList _availableColumnsListBox = new JList();
  private JScrollPane _fieldsDisplayedScrollPane = new JScrollPane();
  private GridLayout _chooseFilterButtonInnerPanelGridLayout = new GridLayout();
  private JButton _removeAllButton = new JButton();
  private JPanel _chooseFilterButtonInnerPanel = new JPanel();
  private JButton _addAllButton = new JButton();
  private FlowLayout _chooseFilterButtonPanelFlowLayout = new FlowLayout();
  private JButton _addButton = new JButton();
  private JButton _removeButton = new JButton();
  private JPanel _chooseFilterButtonPanel = new JPanel();
  private JScrollPane _filterEditorTableScrollPane = new JScrollPane();
  private JTable _filterEditorTable = new JTable();
  private Set _availableFiltersList = null;
  private LinkedList _fieldsDisplayedList = new LinkedList();
  public static final int CANCEL = 0;
  public static final int UPDATE = 1;
  private int _response = -1;
  private Filter _filter = null;
  private JPanel _emptyLeftPriorityPanel = new JPanel();
  private JPanel _emptyMiddlePriorityPanel = new JPanel();
  private JPanel _priorityActionPanel = new JPanel();
  private JButton _movePriorityDownButton = new JButton();
  private JLabel _priorityLabel = new JLabel();
  private JButton _movePriorityUpButton = new JButton();
  private GridLayout _filterPriorityPanelGridLayout = new GridLayout();
  private FlowLayout _priorityActionPanelFlowLayout = new FlowLayout();

  /**
   * @author George A. David
   */
  public FilterEditor(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    Assert.expect(frame != null);
    Assert.expect(title != null);

    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author George A. David
   */
  private void jbInit() throws Exception
  {
    _emptySpaceBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
    _filterEditorMainPanel.setLayout(_filterEditorMainPanelBorderLayout);
    _chooseFilterPanel.setLayout(_chooseFilterPanelBorderLayout);
    _viewCurrentFilterPanel.setLayout(_viewCurrentFilterPanelBorderLayout);
    _filterActionButtonPanel.setLayout(_filterActionButtonPanelFlowLayout);
    _filterPriorityPanel.setLayout(_filterPriorityPanelGridLayout);
    _labelPanel.setLayout(_labelPanelFlowLayout);
    _innerLabelPanel.setLayout(_innerLabelPanelGridLayout);
    _fieldLabel.setText("Field:");
    _showOnlyLabel.setText("Show Only:");
    _sortByLabel.setText("Sort By:");
    _innerLabelPanelGridLayout.setColumns(1);
    _innerLabelPanelGridLayout.setRows(3);
    _innerLabelPanelGridLayout.setVgap(10);
    _labelPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _chooseFilterLabelPanel.setLayout(_chooseFilterLabelPanelGridLayout);
    _avialableColumnsLabelPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _availableColumnsLabel.setText("Available Columns:");
    _availableColumnsLabelPanel.setLayout(_avialableColumnsLabelPanelFlowLayout);
    _innerFilterActionButtonPanel.setLayout(_innerFilterActonButtonPanelGridLayout);
    _saveFilterButton.setText("Save Filter");
    _saveFilterButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _saveFilterButton_actionPerformed(e);
      }
    });
    _loadFilterButton.setText("Load Filter");
    _loadFilterButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _loadFilterButton_actionPerformed(e);
      }
    });
    _defaultFilterButon.setText("Default Filter");
    _defaultFilterButon.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _defaultFilterButon_actionPerformed(e);
      }
    });
    _cancelButton.setText("Cancel");
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _cancelButton_actionPerformed(e);
      }
    });
    _updateButton.setText("OK");
    _updateButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _updateButton_actionPerformed(e);
      }
    });
    _innerFilterActonButtonPanelGridLayout.setColumns(5);
    _innerFilterActonButtonPanelGridLayout.setHgap(25);
    _innerFilterActonButtonPanelGridLayout.setVgap(15);
    _chooseFilterActionPanel.setLayout(_chooseFilterActionPanelGridLayout);
    _chooseFilterActionPanelGridLayout.setColumns(3);
    _fielldsDisplayedLabel.setText("Fields Displayed:");
    _fieldsDisplayedLabelPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _filedsDisplayedLabelPanel.setLayout(_fieldsDisplayedLabelPanelFlowLayout);
    _chooseFilterLabelPanelGridLayout.setColumns(3);
    _chooseFilterPanel.setBorder(_emptySpaceBorder);
    _emptyLabelPanel.setLayout(_emptyLabelPanelFlowLayout);
    _chooseFilterButtonInnerPanelGridLayout.setRows(4);
    _chooseFilterButtonInnerPanelGridLayout.setVgap(15);
    _removeAllButton.setText("<< Remove All");
    _removeAllButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _removeAllButton_actionPerformed(e);
      }
    });
    _chooseFilterButtonInnerPanel.setLayout(_chooseFilterButtonInnerPanelGridLayout);
    _addAllButton.setText("Add All >>");
    _addAllButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _addAllButton_actionPerformed(e);
      }
    });
    _addButton.setText("Add >>");
    _addButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _addButton_actionPerformed(e);
      }
    });
    _removeButton.setText("<< Remove");
    _removeButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _removeButton_actionPerformed(e);
      }
    });
    _chooseFilterButtonPanel.setLayout(_chooseFilterButtonPanelFlowLayout);
    _viewCurrentFilterPanel.setBorder(_emptySpaceBorder);
    _movePriorityDownButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _movePriorityDownButton_actionPerformed(e);
      }
    });
    _movePriorityDownButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_DOWN_ARROW));
    _priorityLabel.setText("Priority");
    _movePriorityUpButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _movePriorityUpButton_actionPerformed(e);
      }
    });
    _movePriorityUpButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_UP_ARROW));
    _priorityActionPanel.setLayout(_priorityActionPanelFlowLayout);
    _chooseFilterPanel.add(_chooseFilterLabelPanel,  BorderLayout.NORTH);
    _chooseFilterLabelPanel.add(_availableColumnsLabelPanel, null);
    _availableColumnsLabelPanel.add(_availableColumnsLabel, null);
    _chooseFilterLabelPanel.add(_emptyLabelPanel, null);
    _emptyLabelPanel.add(_emptyLabel, null);
    _chooseFilterLabelPanel.add(_filedsDisplayedLabelPanel, null);
    _filedsDisplayedLabelPanel.add(_fielldsDisplayedLabel, null);
    _chooseFilterPanel.add(_chooseFilterActionPanel, BorderLayout.CENTER);
    _chooseFilterActionPanel.add(_availableColumnsScrollPane, null);
    _availableColumnsScrollPane.getViewport().add(_availableColumnsListBox, null);
    _availableColumnsListBox.addMouseListener(new MouseListener()
    {
      public void mouseClicked(MouseEvent e)
      {
        if(e.getClickCount() > 1)
        {
          if(_availableColumnsListBox.getSelectedIndex() != -1)
          {
            Object[] selectedValues = _availableColumnsListBox.getSelectedValues();
            addTableColumns(selectedValues);
          }
        }
      }

      public void mouseReleased(MouseEvent e) {}
      public void mouseExited(MouseEvent e) {}
      public void mouseEntered(MouseEvent e) {}
      public void mousePressed(MouseEvent e) {}
    });
    _chooseFilterActionPanel.add(_chooseFilterButtonPanel, null);
    _chooseFilterButtonPanel.add(_chooseFilterButtonInnerPanel, null);
    _chooseFilterButtonInnerPanel.add(_addButton, null);
    _chooseFilterButtonInnerPanel.add(_addAllButton, null);
    _chooseFilterButtonInnerPanel.add(_removeButton, null);
    _chooseFilterButtonInnerPanel.add(_removeAllButton, null);
    _chooseFilterActionPanel.add(_fieldsDisplayedScrollPane, null);
    _fieldsDisplayedScrollPane.getViewport().add(_fieldsDisplayedListBox, null);
    this.getContentPane().add(_filterActionButtonPanel, BorderLayout.SOUTH);
    _filterActionButtonPanel.add(_innerFilterActionButtonPanel, null);
    _filterEditorMainPanel.add(_viewCurrentFilterPanel, BorderLayout.CENTER);
    _filterEditorMainPanel.add(_chooseFilterPanel, BorderLayout.NORTH);
    _filterPriorityPanel.add(_emptyLeftPriorityPanel, null);
    _filterPriorityPanel.add(_emptyMiddlePriorityPanel, null);
    _filterPriorityPanel.add(_priorityActionPanel, null);
    _priorityActionPanel.add(_movePriorityUpButton, null);
    _priorityActionPanel.add(_priorityLabel, null);
    _priorityActionPanel.add(_movePriorityDownButton, null);
    _viewCurrentFilterPanel.add(_filterEditorTableScrollPane, BorderLayout.CENTER);
    _viewCurrentFilterPanel.add(_labelPanel, BorderLayout.WEST);
    _innerFilterActionButtonPanel.add(_saveFilterButton, null);
    _innerFilterActionButtonPanel.add(_loadFilterButton, null);
    _innerFilterActionButtonPanel.add(_defaultFilterButon, null);
    _innerFilterActionButtonPanel.add(_cancelButton, null);
    _innerFilterActionButtonPanel.add(_updateButton, null);
    _viewCurrentFilterPanel.add(_filterPriorityPanel, BorderLayout.NORTH);
    this.getContentPane().add(_filterEditorMainPanel,  BorderLayout.CENTER);
    _labelPanel.add(_innerLabelPanel, null);
    _innerLabelPanel.add(_fieldLabel, null);
    _innerLabelPanel.add(_showOnlyLabel, null);
    _innerLabelPanel.add(_sortByLabel, null);

    _fieldsDisplayedListBox.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        int index = _fieldsDisplayedListBox.getMinSelectionIndex();
        int selectedIndex = _filterEditorTable.getSelectedColumn();
        if(index != -1 && index != selectedIndex)
        {
          _filterEditorTable.setColumnSelectionInterval(index, index);
          _filterEditorTable.setRowSelectionInterval(0,0);
          _filterEditorTable.changeSelection(0, index, true, true);
        }
      }
    });

    populateAvailableFiltersList();
    populateFieldsDisplayedList();
    createDefaultTable();

  }

  /**
   * @author George A. David
   */
  void _cancelButton_actionPerformed(ActionEvent e)
  {
    _response = CANCEL;
    hide();
  }

  /**
   * @author George A. David
   */
  void populateAvailableFiltersList()
  {
    _availableFiltersList = Filter.getAvailableFilters();
    _availableColumnsListBox.setListData(_availableFiltersList.toArray());
  }

  /**
   * @author George A. David
   */
  void populateFieldsDisplayedList()
  {
    _fieldsDisplayedList.addAll(Filter.getDefaultFilterFieldList());
    _fieldsDisplayedListBox.setListData(_fieldsDisplayedList.toArray());
    _availableFiltersList.removeAll(_fieldsDisplayedList);
    _availableColumnsListBox.setListData(_availableFiltersList.toArray());
  }

  /**
   * @author George A. David
   */
  void createDefaultTable()
  {

    Filter filter = Filter.getDefaultFilter();
    java.util.List filterDataList = filter.getFilterDataList();

    FilterEditorTableModel filterEditorTableModel = new FilterEditorTableModel();
    _filterEditorTable = new JTable(filterEditorTableModel);
    FilterTableCellEditor filterTableCellEditor = new FilterTableCellEditor(_filterEditorTable);

    Iterator it = filterDataList.iterator();
    while(it.hasNext())
    {
      FilterData filterData = (FilterData) it.next();
      String columnName = filterData.getFilterName();

      filterEditorTableModel.addColumnName(columnName);
      int columnIndex = _filterEditorTable.getColumnCount();
      filterEditorTableModel.setValueAt(filterData.getFilterString(), 0, columnIndex);
      filterEditorTableModel.setValueAt(filterData.getSortEnum().toString(), 1, columnIndex);
      TableColumn tableColumn = new TableColumn(columnIndex);
      tableColumn.setHeaderValue(columnName);
      tableColumn.setIdentifier(columnName);
      tableColumn.setCellEditor(filterTableCellEditor);
      _filterEditorTable.addColumn(tableColumn);
    }

    setUpFilterEditorTable();
  }

  /**
   * @author George A. David
   */
  void _addButton_actionPerformed(ActionEvent e)
  {
    if(_availableColumnsListBox.getSelectedIndex() != -1)
    {
      Object[] selectedValues = _availableColumnsListBox.getSelectedValues();
      addTableColumns(selectedValues);
    }
  }

  /**
   * @author George A. David
   */
  void _addAllButton_actionPerformed(ActionEvent e)
  {
    addTableColumns(_availableFiltersList.toArray());
  }

  /**
   * @author George A. David
   */
  void addTableColumns(Object[] columnNames)
  {
    Assert.expect(columnNames != null);

    FilterEditorTableModel tableModel = (FilterEditorTableModel)_filterEditorTable.getModel();

    FilterTableCellEditor filterTableCellEditor = null;
    if(_filterEditorTable.getColumnCount() > 0)
    {
      filterTableCellEditor = (FilterTableCellEditor)_filterEditorTable.getColumnModel().getColumn(0).getCellEditor();
    }
    else
    {
      filterTableCellEditor = new FilterTableCellEditor(_filterEditorTable);
    }
    int selectionIndex = _fieldsDisplayedList.size();
    int[] selectionIndicies = new int[columnNames.length];
    for(int index = 0; index < columnNames.length; ++index)
    {
      String columnName = (String)columnNames[index];
      _availableFiltersList.remove(columnName);
      _fieldsDisplayedList.add(columnName);
      selectionIndicies[index] = selectionIndex;
      ++selectionIndex;
      int columnIndex = _filterEditorTable.getColumnCount();
      if(tableModel.containsColumnName(columnName) == false)
      {
        tableModel.addColumnName(columnName);
      }
      else
      {
        columnIndex = tableModel.getColumnIndex(columnName);
      }
      TableColumn tableColumn = new TableColumn(columnIndex);
      tableColumn.setHeaderValue(columnName);
      tableColumn.setIdentifier(columnName);
      tableColumn.setCellEditor(filterTableCellEditor);
      _filterEditorTable.addColumn(tableColumn);
    }
    _fieldsDisplayedListBox.setListData(_fieldsDisplayedList.toArray());
    _fieldsDisplayedListBox.setSelectedValue(columnNames[0], true);
    _fieldsDisplayedListBox.setSelectedIndices(selectionIndicies);
    _availableColumnsListBox.setListData(_availableFiltersList.toArray());
    setUpFilterEditorTable();
  }

  /**
   * @author George A. David
   */
  void _removeButton_actionPerformed(ActionEvent e)
  {
    if(_fieldsDisplayedListBox.getSelectedIndex() != -1)
    {
      Object[] selectedValues = _fieldsDisplayedListBox.getSelectedValues();
      int index = _fieldsDisplayedListBox.getMaxSelectionIndex();
      removeTableColumns(selectedValues);
    }
  }

  /**
   * @author George A. David
   */
  void _removeAllButton_actionPerformed(ActionEvent e)
  {
    removeTableColumns(_fieldsDisplayedList.toArray());
    _filterEditorTable = new JTable(new FilterEditorTableModel());
    setUpFilterEditorTable();
  }

  /**
   * @author George A. David
   */
  void removeTableColumns(Object[] columnNames)
  {
    Assert.expect(columnNames != null);

    FilterEditorTableModel tableModel = (FilterEditorTableModel)_filterEditorTable.getModel();
    for(int index = 0; index < columnNames.length; ++index)
    {
      String columnName = (String)columnNames[index];
      _availableFiltersList.add(columnName);
      _fieldsDisplayedList.remove(columnName);
      TableColumn tableColumn = _filterEditorTable.getColumn(columnName);
      _filterEditorTable.removeColumn(tableColumn);
    }
    _fieldsDisplayedListBox.setListData(_fieldsDisplayedList.toArray());
    _availableColumnsListBox.setListData(_availableFiltersList.toArray());
  }

  /**
   * @author George A. David
   */
  private void setUpFilterEditorTable()
  {
    FontMetrics fontMetrics = _filterEditorTable.getFontMetrics(_filterEditorTable.getFont());
    _filterEditorTable.setRowHeight((fontMetrics.getMaxAscent() + fontMetrics.getMaxDescent() + fontMetrics.getLeading()) * 2);
    Iterator it = _fieldsDisplayedList.iterator();
    while(it.hasNext())
    {
      String columnName = (String)it.next();
      int stringWidth = (int)Math.rint(fontMetrics.stringWidth(columnName) * 1.5);
      TableColumn tableColumn = _filterEditorTable.getColumn(columnName);
      tableColumn.setPreferredWidth(stringWidth);
    }

    _filterEditorTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    _filterEditorTable.getTableHeader().setReorderingAllowed(false);

    _filterEditorTableScrollPane.getViewport().removeAll();
    _filterEditorTableScrollPane.getViewport().add(_filterEditorTable);
  }

  /**
   * @author George A. David
   */
  void _movePriorityUpButton_actionPerformed(ActionEvent e)
  {
    if(_fieldsDisplayedListBox.getSelectedIndex() != -1)
    {
      Object selectedValue = _fieldsDisplayedListBox.getSelectedValue();
      int index = _fieldsDisplayedList.indexOf(selectedValue);
      if(index > 0)
      {
        int targetIndex = index - 1;
        _fieldsDisplayedList.remove(selectedValue);
        _fieldsDisplayedList.add(targetIndex, selectedValue);
        _fieldsDisplayedListBox.setListData(_fieldsDisplayedList.toArray());
        _availableColumnsListBox.setListData(_availableFiltersList.toArray());
        _fieldsDisplayedListBox.setSelectedValue(selectedValue, true);
        _filterEditorTable.moveColumn(index, targetIndex);
      }
    }
  }

  /**
   * @author George A. David
   */
  void _movePriorityDownButton_actionPerformed(ActionEvent e)
  {
    if(_fieldsDisplayedListBox.getSelectedIndex() != -1)
    {
      Object selectedValue = _fieldsDisplayedListBox.getSelectedValue();
      int index = _fieldsDisplayedList.indexOf(selectedValue);
      if(index< _fieldsDisplayedList.size() - 1)
      {
        int targetIndex = index + 1;
        _fieldsDisplayedList.remove(selectedValue);
        _fieldsDisplayedList.add(targetIndex, selectedValue);
        _fieldsDisplayedListBox.setListData(_fieldsDisplayedList.toArray());
        _availableColumnsListBox.setListData(_availableFiltersList.toArray());
        _fieldsDisplayedListBox.setSelectedValue(selectedValue, true);
        _filterEditorTable.moveColumn(index, targetIndex);
      }
    }
  }

  /**
   * @author George A. David
   */
  void _defaultFilterButon_actionPerformed(ActionEvent e)
  {
    _availableFiltersList = Filter.getAvailableFilters();
    _fieldsDisplayedList.clear();
    _fieldsDisplayedList.addAll(Filter.getDefaultFilterFieldList());
    _availableFiltersList.removeAll(_fieldsDisplayedList);
    createDefaultTable();
    _availableColumnsListBox.setListData(_availableFiltersList.toArray());
    _fieldsDisplayedListBox.setListData(_fieldsDisplayedList.toArray());
  }

  /**
   * @author George A. David
   */
  void _saveFilterButton_actionPerformed(ActionEvent e)
  {
    JFileChooser filterChooser = getFilterChooser();
    if(filterChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION)
    {
      String fileName = filterChooser.getSelectedFile().getPath();
      if(fileName.endsWith(".filter") == false)
      {
        fileName += ".filter";
      }

      Filter filter = new Filter();
      filter.setFilterFilePath(fileName);
      int columnCount = _filterEditorTable.getColumnCount();
      for(int index = 0; index < columnCount; ++index)
      {
        String columnName = _filterEditorTable.getColumnName(index);
        String filterString = (String)_filterEditorTable.getValueAt(0, index);
        SortEnum sortEnum = (SortEnum)_filterEditorTable.getValueAt(1, index);

        FilterData filterData = new FilterData();
        filterData.setFilterName(columnName);
        filterData.setFilterString(filterString);
        filterData.setSortEnum(sortEnum);

        filter.addFilterData(filterData);
      }

      try
      {
        filter.save();
      }
      catch(DatastoreException dex)
      {
        String errorMessage = dex.getLocalizedMessage();
        JOptionPane.showMessageDialog(this, errorMessage, "Save Error", JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  /**
   * @author George A. David
   */
  void _loadFilterButton_actionPerformed(ActionEvent e)
  {
    JFileChooser filterChooser = getFilterChooser();
    if(filterChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
    {

      String fileName = filterChooser.getSelectedFile().getPath();
      Filter filter = null;
      try
      {
        filter = Filter.load(fileName);
      }
      catch(DatastoreException dex)
      {
        String errorMessage = dex.getLocalizedMessage();
        JOptionPane.showMessageDialog(this, errorMessage, "Load Error", JOptionPane.ERROR_MESSAGE);
        return;
      }

      FilterEditorTableModel filterEditorTableModel = new FilterEditorTableModel();
      _filterEditorTable = new JTable(filterEditorTableModel);
      FilterTableCellEditor filterTableCellEditor = null;
      filterTableCellEditor = new FilterTableCellEditor(_filterEditorTable);

      _fieldsDisplayedList.clear();
      java.util.List filterDataList = filter.getFilterDataList();
      Iterator it = filterDataList.iterator();
      while(it.hasNext())
      {
        FilterData filterData = (FilterData)it.next();
        String columnName = filterData.getFilterName();
        filterEditorTableModel.addColumnName(filterData.getFilterName(), filterData.getFilterString(), filterData.getSortEnum());
        _fieldsDisplayedList.add(filterData.getFilterName());
        int columnIndex = _filterEditorTable.getColumnCount();
        TableColumn tableColumn = new TableColumn(columnIndex);
        tableColumn.setHeaderValue(columnName);
        tableColumn.setIdentifier(columnName);
        tableColumn.setCellEditor(filterTableCellEditor);
        _filterEditorTable.addColumn(tableColumn);
      }

      _availableFiltersList = Filter.getAvailableFilters();
      _availableFiltersList.removeAll(_fieldsDisplayedList);
      _availableColumnsListBox.setListData(_availableFiltersList.toArray());
      _fieldsDisplayedListBox.setListData(_fieldsDisplayedList.toArray());
      setUpFilterEditorTable();
    }
  }

  /**
   * @author George A. David
   */
  void _updateButton_actionPerformed(ActionEvent e)
  {
    _response = UPDATE;
    hide();

    _filter = new Filter();
    int columnCount = _filterEditorTable.getColumnCount();
    for(int index = 0; index < columnCount; ++index)
    {
      String columnName = _filterEditorTable.getColumnName(index);
      String filterString = (String)_filterEditorTable.getValueAt(0, index);
      SortEnum sortEnum = (SortEnum)_filterEditorTable.getValueAt(1, index);

      FilterData filterData = new FilterData();
      filterData.setFilterName(columnName);
      filterData.setFilterString(filterString);
      filterData.setSortEnum(sortEnum);

      _filter.addFilterData(filterData);
    }
  }

  /**
   * @author George A. David
   */
  int getResponse()
  {
    return _response;
  }

  /**
   * @author George A. David
   */
  Filter getFilter()
  {
    Assert.expect(_filter != null);

    return _filter;
  }

  /**
   * @author George A. David
   */
  boolean hasFilter()
  {
    return _filter != null;
  }

  /**
   * @author George A. David
   */
  void setSelectedField(int index)
  {
    Assert.expect(index >= 0 && index < _fieldsDisplayedListBox.getModel().getSize());

    _fieldsDisplayedListBox.setSelectedIndex(index);
  }

  /**
   * @author George A. David
   */
  private JFileChooser getFilterChooser()
  {
    JFileChooser filterChooser = new JFileChooser();
    filterChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    filterChooser.setMultiSelectionEnabled(false);
    filterChooser.setFileFilter(new FilterEditorFileFilter());

    return filterChooser;
  }
}