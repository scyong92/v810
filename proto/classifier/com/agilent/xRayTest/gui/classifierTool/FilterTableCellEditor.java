package proto.classifier.com.agilent.xRayTest.gui.classifierTool;

import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import javax.swing.event.*;

import com.agilent.util.*;
import proto.classifier.com.agilent.xRayTest.datastore.classifierTool.*;

/**
 * @author George A. David
 */
public class FilterTableCellEditor implements TableCellEditor
{
  private TableCellEditor _tableCellEditor;
  private Map _columnNameToTableCellEditor = new TreeMap(new CaseInsensitiveStringComparator());
  private DefaultCellEditor _sortComboBoxCellEditor = null;
  private DefaultCellEditor _defaultCellEditor = null;
  private JTable _table = null;

  /**
   * @author George A. David
   */
  FilterTableCellEditor(JTable table)
  {
    Assert.expect(table != null);

    _table = table;

    JComboBox comboBox = new JComboBox();
    comboBox.addItem("None");
    comboBox.addItem("Ascending");
    comboBox.addItem("Descending");
    _sortComboBoxCellEditor = new DefaultCellEditor(comboBox);
    comboBox = new JComboBox();
    comboBox.addItem("Not Specified");
    comboBox.addItem("Good");
    comboBox.addItem("Bad");
    DefaultCellEditor comboBoxCellEditor = new DefaultCellEditor(comboBox);
    _columnNameToTableCellEditor.put(Filter._SYSTEM_CLASSIFICATION, comboBoxCellEditor);
    _columnNameToTableCellEditor.put(Filter._TRUE_CLASSIFICATION, comboBoxCellEditor);
    JCheckBox checkBox = new JCheckBox();
    DateButtonEditor dateButtonEditor = new DateButtonEditor(checkBox);
    _columnNameToTableCellEditor.put(Filter._INSPECTION_START_TIME, dateButtonEditor);
    comboBox = new JComboBox();
    comboBox.addItem("FPGullwing");
    comboBox.addItem("Gullwing");
    comboBox.addItem("BGA2");
    comboBox.addItem("BGA");
    comboBox.addItem("JLead");
    comboBoxCellEditor = new DefaultCellEditor(comboBox);
    _columnNameToTableCellEditor.put(Filter._ALGORITHM_FAMILY_NAME, comboBoxCellEditor);
    comboBox = new JComboBox();
    comboBox.addItem("Not Specified");
    comboBox.addItem("Rectangle");
    comboBox.addItem("Circle");
    comboBoxCellEditor = new DefaultCellEditor(comboBox);
    _columnNameToTableCellEditor.put(Filter._PAD_SHAPE, comboBoxCellEditor);
    comboBox = new JComboBox();
    comboBox.addItem("Not Specified");
    comboBox.addItem("Yes");
    comboBox.addItem("No");
    comboBoxCellEditor = new DefaultCellEditor(comboBox);
    _columnNameToTableCellEditor.put(Filter._REVIEW_NEEDED, comboBoxCellEditor);

    JTextField textField = new JTextField();
    textField.addFocusListener(new FocusListener()
    {
      public void focusGained(FocusEvent e){}

      public void focusLost(FocusEvent e)
      {
        if(e.isTemporary())
          return;
        if (_table.isEditing() && e.getOppositeComponent() != _table)
          _tableCellEditor.stopCellEditing();
      }
    });
    _defaultCellEditor = new DefaultCellEditor(textField);

  }

  /**
   * @author George A. David
   */
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
  {
    return _tableCellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
  }

  /**
   * @author George A. David
   */
  public Object getCellEditorValue()
  {
    return _tableCellEditor.getCellEditorValue();
  }

  /**
   * @author George A. David
   */
  public boolean isCellEditable(EventObject anEvent)
  {
    selectTableCellEditor((MouseEvent)anEvent);
    return true;
  }

  /**
   * @author George A. David
   */
  public boolean shouldSelectCell(EventObject anEvent)
  {
    if(_table.isEditing() == false)
    {
      selectTableCellEditor((MouseEvent)anEvent);
    }
    return _tableCellEditor.shouldSelectCell(anEvent);
  }

  /**
   * @author George A. David
   */
  public boolean stopCellEditing()
  {
    return _tableCellEditor.stopCellEditing();
  }

  /**
   * @author George A. David
   */
  public void cancelCellEditing()
  {
    _tableCellEditor.cancelCellEditing();
  }

  /**
   * @author George A. David
   */
  public void addCellEditorListener(CellEditorListener l)
  {
    _tableCellEditor.addCellEditorListener(l);
  }

  /**
   * @author George A. David
   */
  public void removeCellEditorListener(CellEditorListener l)
  {
    _tableCellEditor.removeCellEditorListener(l);
  }

  /**
   * @author George A. David
   */
  void selectTableCellEditor(MouseEvent e)
  {
    int row = -1;
    int column = -1;
    if(e == null)
    {
      row = _table.getSelectionModel().getAnchorSelectionIndex();
      column = _table.getSelectionModel().getLeadSelectionIndex();
      if(column == -1)
        column = 0;
    }
    else
    {
      row = _table.rowAtPoint(e.getPoint());
      column = _table.columnAtPoint(e.getPoint());
    }

    if(row == 1)
    {
      _tableCellEditor = _sortComboBoxCellEditor;
    }
    else
    {
      String columnName = _table.getColumnName(column);
      _tableCellEditor = (TableCellEditor)_columnNameToTableCellEditor.get(columnName);
      if(_tableCellEditor == null)
      {
        _tableCellEditor = _defaultCellEditor;
      }
    }
  }
}