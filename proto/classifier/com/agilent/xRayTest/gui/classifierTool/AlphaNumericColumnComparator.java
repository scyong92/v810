package proto.classifier.com.agilent.xRayTest.gui.classifierTool;

import java.util.*;

import com.agilent.guiUtil.*;
import com.agilent.util.*;
import proto.classifier.com.agilent.xRayTest.datastore.classifierTool.*;

/**
 * @author George A. David
 */
public class AlphaNumericColumnComparator implements Comparator
{
  private int _index;
  private boolean _ascending;
  private String _columnName = null;
  private AlphaNumericComparator _alphaNumericComparator = new AlphaNumericComparator();

  /**
   * @author George A. David
   */
  public AlphaNumericColumnComparator(int index, boolean ascending, String columnName)
  {
    Assert.expect(index >= 0);
    Assert.expect(columnName != null);

    _index = index;
    _ascending = ascending;
    _columnName = columnName;
  }

  /**
   * @author George A. David
   */
  public int compare(Object lhs, Object rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);

    if (lhs instanceof Vector &&
        rhs instanceof Vector)
    {
      Vector lhsVector = (Vector)lhs;
      Vector rhsVector = (Vector)rhs;
      ClassifiedJoint lhsJoint = (ClassifiedJoint)lhsVector.elementAt(_index);
      ClassifiedJoint rhsJoint = (ClassifiedJoint)rhsVector.elementAt(_index);
      Object lhsElement = null;
      Object rhsElement = null;



      if(_columnName.equalsIgnoreCase(Filter._REFERENCE_DESIGNATOR))
      {
        lhsElement = lhsJoint.getReferenceDesignator();
        rhsElement = rhsJoint.getReferenceDesignator();
      }
      else if(_columnName.equalsIgnoreCase(Filter._PANEL_NAME))
      {
        lhsElement = lhsJoint.getPanelName();
        rhsElement = rhsJoint.getPanelName();
      }
      else if(_columnName.equalsIgnoreCase(Filter._PANEL_SERIAL_NUMBER))
      {
        lhsElement = lhsJoint.getPanelSerialNumber();
        rhsElement = rhsJoint.getPanelSerialNumber();
      }
      else if(_columnName.equalsIgnoreCase(Filter._PIN_NAME))
      {
        lhsElement = lhsJoint.getPinName();
        rhsElement = rhsJoint.getPinName();
      }
      else if(_columnName.equalsIgnoreCase(Filter._SYSTEM_CLASSIFICATION))
      {
        lhsElement = lhsJoint.getSystemClassification().toString();
        rhsElement = rhsJoint.getSystemClassification().toString();
      }
      else if(_columnName.equalsIgnoreCase(Filter._TRUE_CLASSIFICATION))
      {
        lhsElement = lhsJoint.getTrueClassification().toString();
        rhsElement = rhsJoint.getTrueClassification().toString();
      }
      else if(_columnName.equalsIgnoreCase(Filter._SUBTYPE_NUMBER))
      {
        lhsElement = Integer.toString(lhsJoint.getSubtypeNumber());
        rhsElement = Integer.toString(rhsJoint.getSubtypeNumber());
      }
      else if(_columnName.equalsIgnoreCase(Filter._SORTED_JOINT_NUMBER))
      {
        lhsElement = Integer.toString(lhsJoint.getSortedJointNumber());
        rhsElement = Integer.toString(rhsJoint.getSortedJointNumber());
      }
      else if(_columnName.equalsIgnoreCase(Filter._RESULT_FILE_NAME))
      {
        lhsElement = lhsJoint.getResultFileName();
        rhsElement = rhsJoint.getResultFileName();
      }
      else if(_columnName.equalsIgnoreCase(Filter._BOARD_NUMBER))
      {
        lhsElement = Integer.toString(lhsJoint.getBoardNumber());
        rhsElement = Integer.toString(rhsJoint.getBoardNumber());
      }
      else if(_columnName.equalsIgnoreCase(Filter._BOARD_SERIAL_NUMBER))
      {
        lhsElement = lhsJoint.getBoardSerialNumber();
        rhsElement = rhsJoint.getBoardSerialNumber();
      }
      else if(_columnName.equalsIgnoreCase(Filter._INSPECTION_START_TIME))
      {
        lhsElement = lhsJoint.getInspectionStartDateAndTimeString();
        rhsElement = rhsJoint.getInspectionStartDateAndTimeString();
      }
      else if(_columnName.equals(Filter._ALGORITHM_FAMILY_NAME))
      {
        lhsElement = lhsJoint.getAlgorithmFamilyName();
        rhsElement = rhsJoint.getAlgorithmFamilyName();
      }
      else if(_columnName.equals(Filter._PAD_SHAPE))
      {
        lhsElement = lhsJoint.getPadShape().toString();
        rhsElement = rhsJoint.getPadShape().toString();
      }
      else if(_columnName.equals(Filter._VIEW_NUMBER))
      {
        lhsElement = Integer.toString(lhsJoint.getViewNumber());
        rhsElement = Integer.toString(rhsJoint.getViewNumber());
      }
      else if(_columnName.equalsIgnoreCase(Filter._REVIEW_NEEDED))
      {
        lhsElement = Boolean.toString(lhsJoint.isReviewNeeded());
        rhsElement = Boolean.toString(rhsJoint.isReviewNeeded());
      }
      else if(_columnName.equalsIgnoreCase(Filter._NUMBER_OF_TIMES_REVIEWED))
      {
        lhsElement = Integer.toString(lhsJoint.getNumberOfTimesReviewed());
        rhsElement = Integer.toString(rhsJoint.getNumberOfTimesReviewed());
      }
      else
      {
        //this should not happen
        Assert.expect(false);
      }

      if(lhsElement instanceof String && rhsElement instanceof String)
      {
        if(_ascending)
        {
          return _alphaNumericComparator.compare(lhsElement, rhsElement);
        }
        else
        {
          return _alphaNumericComparator.compare(rhsElement, lhsElement);
        }
      }
      else if (lhsElement instanceof Comparable &&
               rhsElement instanceof Comparable)
      {
        Comparable lhsComparable = (Comparable)lhsElement;
        Comparable rhsComparable = (Comparable)rhsElement;
        if (_ascending)
        {
          return lhsComparable.compareTo(rhsComparable);
        }
        else
        {
          return rhsComparable.compareTo(lhsComparable);
        }
      }
    }
    return 1;
  }
}