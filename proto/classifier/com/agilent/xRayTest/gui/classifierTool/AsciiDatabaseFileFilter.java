package proto.classifier.com.agilent.xRayTest.gui.classifierTool;

import javax.swing.filechooser.*;
import java.io.*;

import com.agilent.util.*;
/**
 * @author George A. David
 */
class AsciiDatabaseFileFilter extends javax.swing.filechooser.FileFilter
{
  /**
   * @author George A. David
   */
  public boolean accept(File file)
  {
    Assert.expect(file != null);
    boolean accept = false;

    if(file.isDirectory() == false)
    {
      String fileName = file.getName().toLowerCase();
      accept = fileName.endsWith(".adb");
    }
    else
    {
      accept = true;
    }

    return accept;
  }

  /**
   * @author George A. David
   */
  public String getDescription()
  {
    return "Ascii Database Files (*.adb)";
  }
}