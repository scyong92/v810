package com.agilent.xRayTest.gui.drawCAD;

import java.awt.*;
import java.awt.geom.*;
import java.lang.*;
import java.util.*;

import com.agilent.xRayTest.datastore.panelDesc.*;
import com.agilent.xRayTest.gui.*;
import com.agilent.util.*;

/**
 * This class controls how to draw a pin on the screen.
 *
 * @author Bill Darbie
 */
class CadPin extends CadShape
{
  private double _xPixel;
  private double _yPixel;
  private double _pixelWidth;
  private double _pixelLength;
  private double _lengthInNanoMeters;
  private double _widthInNanoMeters;
  private int _degreesRotation;
  private String _name = null;
  private ShapeEnum _pinShape = null;
  private DoubleCoordinate _coordinate = null;
  private static Color _normalColor = Color.blue;

  /**
   * @author George A. David
   */
  CadPin(PinAdapter pinAdapter)
  {
    Assert.assert(pinAdapter != null);

    _name = pinAdapter.getName();
    _coordinate = pinAdapter.getPanelCoordinate();
    int degreesOrientation = pinAdapter.getDegreesOrientation();
    // A pin rotated at 0 and 180 degrees has a width and length. If the pin is
    // rotated at 90 or 270 degrees, the width and length are swapped, so
    // essentially they are already rotated. We need to remove this rotation from
    // the total rotation.
    if(degreesOrientation % 90 == 0)
      _degreesRotation = pinAdapter.getDegreesRotationRelativeToCAD() - pinAdapter.getDegreesOrientation();
    else
      _degreesRotation = pinAdapter.getDegreesRotationRelativeToCAD();
    _lengthInNanoMeters = pinAdapter.getLengthInNanoMeters();
    _widthInNanoMeters = pinAdapter.getWidthInNanoMeters();
    _pinShape = pinAdapter.getShapeEnum();
    calculateSize();
  }

  /**
   * @author George A. David
   */
  public void calculateSize()
  {
//    _xPixel = DrawUtil.getPixelForNanos(_coordinate.getX(), _scale);
//    _yPixel = DrawUtil.getPixelForNanos(_coordinate.getY(), _scale);
//    _pixelWidth = DrawUtil.getPixelForNanos(_widthInNanoMeters, _scale);
//    _pixelLength = DrawUtil.getPixelForNanos(_lengthInNanoMeters, _scale);
//    if(_pinShape.equals(ShapeEnum.RECTANGLE))
//    {
//      Point2D topLeft = new Point2D.Double();
//      Point2D topRight = new Point2D.Double();
//      Point2D bottomRight = new Point2D.Double();
//      Point2D bottomLeft = new Point2D.Double();
//      Point2D coordinate = new Point2D.Double(_xPixel - _pixelWidth / 2.0, _yPixel - _pixelLength / 2.0);
//
//      DrawUtil.calculateCornersOfRectangle(coordinate,
//                                           _pixelWidth,
//                                           _pixelLength,
//                                           topLeft,
//                                           topRight,
//                                           bottomLeft,
//                                           bottomRight);
//      coordinate.setLocation(_xPixel, _yPixel);
//      DrawUtil.rotateRectangle(topLeft,
//                               topRight,
//                               bottomRight,
//                               bottomLeft,
//                               _degreesRotation,
//                               coordinate);
//      DrawUtil.transformRectangleToScreenCoordinate(topLeft,
//                                                    topRight,
//                                                    bottomRight,
//                                                    bottomLeft,
//                                                    _screenCoordinateOffset);
//      _shape = DrawUtil.createShape(topLeft, topRight, bottomLeft, bottomRight);
//    }
//    else
//    {
//      _yPixel = _screenCoordinateOffset.getY() - _yPixel;
//      double diameter = _pixelWidth;
//      double radius = diameter / 2;
//      _shape = new Ellipse2D.Double(_xPixel - radius, _yPixel - radius, diameter, diameter);
//    }
  }

  /**
   * @author George A. David
   */
  public void draw(Graphics2D graphics)
  {
    Assert.assert(graphics != null);

    if(isInDrawRegion())
    {
      graphics.draw(_shape);
    }
  }

}
