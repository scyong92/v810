package com.agilent.xRayTest.gui.drawCAD;

import java.awt.*;
import java.rmi.*;
import javax.swing.UIManager;

import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.gui.*;
import com.agilent.xRayTest.hardware.*;
import com.agilent.xRayTest.util.*;
import com.agilent.util.*;

/**
 * The main application class for the DrawCad piece of the photogrammetry gui.
 * It draws the board the user has chosen to set up surface map. It can draw
 * the board outline, the component outline, the pins, the pads, and the
 * surface map features.
 *
 * @author George A. David
 */
public class DrawCadApplication
{
  private boolean _packFrame = true;

  /**
   * @author George A. David
   */
  public DrawCadApplication() throws DatastoreException, NoFieldOfViewHardwareException
  {
    init();
  }

  /**
   * Initialize the application
   *
   * @author George A. David
   */
  void init() throws DatastoreException, NoFieldOfViewHardwareException
  {
    DrawCadFrame frame = new DrawCadFrame();

    //Validate frames that have preset sizes
    //Pack frames that have useful preferred size info, e.g. from their layout
    if (_packFrame)
    {
      frame.pack();
    }
    else
    {
      frame.validate();
    }
    //Center the window
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = frame.getSize();
    if (frameSize.height > screenSize.height)
    {
      frameSize.height = screenSize.height;
    }
    if (frameSize.width > screenSize.width)
    {
      frameSize.width = screenSize.width;
    }
    frame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
    frame.setVisible(true);
  }

  /**
   * @author George A. David
   */
  public static void main(String[] args) throws DatastoreException, NoFieldOfViewHardwareException
  {
    try
    {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch(Exception e)
    {
      Assert.logException(e);
    }
    new DrawCadApplication();
  }
}
