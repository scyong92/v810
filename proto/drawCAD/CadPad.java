package com.agilent.xRayTest.gui.drawCAD;

import java.awt.*;
import java.awt.geom.*;

import com.agilent.xRayTest.gui.*;
import com.agilent.xRayTest.datastore.panelDesc.*;
import com.agilent.util.*;

/**
 * This class controls how to draw a pad.
 *
 * @author George A. David
 */
class CadPad extends CadShape
{
  private double _xPixel;
  private double _yPixel;
  private double _pixelLength;
  private double _pixelWidth;
  private double _lengthInNanoMeters;
  private double _widthInNanoMeters;
  private int _degreesRotation;
  private DoubleCoordinate _coordinate = null;
  private ShapeEnum _padShape = null;
  private static Color _normalColor = Color.green;

  /**
   * @author George A. David
   */
  CadPad(PadAdapter padAdapter)
  {
    Assert.assert(padAdapter != null);

    _degreesRotation = padAdapter.getDegreesRotationRelativeToCAD();
    _coordinate = padAdapter.getPanelCoordinate();
    _lengthInNanoMeters = padAdapter.getLengthInNanoMeters();
    _widthInNanoMeters = padAdapter.getWidthInNanoMeters();
    _padShape = padAdapter.getShapeEnum();
    calculateSize();
  }

  /**
   * Initialize the pad
   * @author George A. David
   */
  public void calculateSize()
  {
//    _xPixel = DrawUtil.getPixelForNanos(_coordinate.getX(), _scale);
//    _yPixel = DrawUtil.getPixelForNanos(_coordinate.getY(), _scale);
//    _pixelLength = DrawUtil.getPixelForNanos(_lengthInNanoMeters, _scale);
//    _pixelWidth = DrawUtil.getPixelForNanos(_widthInNanoMeters, _scale);
//    if(_padShape.equals(ShapeEnum.RECTANGLE))
//    {
//      Point2D topLeft = new Point2D.Double();
//      Point2D topRight = new Point2D.Double();
//      Point2D bottomRight = new Point2D.Double();
//      Point2D bottomLeft = new Point2D.Double();
//      Point2D coordinate = new Point2D.Double(_xPixel - _pixelWidth / 2.0, _yPixel - _pixelLength / 2.0);
//
//      DrawUtil.calculateCornersOfRectangle(coordinate,
//                                           _pixelWidth,
//                                           _pixelLength,
//                                           topLeft,
//                                           topRight,
//                                           bottomLeft,
//                                           bottomRight);
//      coordinate.setLocation(_xPixel, _yPixel);
//      DrawUtil.rotateRectangle(topLeft,
//                               topRight,
//                               bottomRight,
//                               bottomLeft,
//                               _degreesRotation,
//                               coordinate);
//      DrawUtil.transformRectangleToScreenCoordinate(topLeft,
//                                                    topRight,
//                                                    bottomRight,
//                                                    bottomLeft,
//                                                    _screenCoordinateOffset);
//      _shape = DrawUtil.createShape(topLeft, topRight, bottomLeft, bottomRight);
//    }
//    else
//    {
//      _yPixel = _screenCoordinateOffset.getY() - _yPixel;
//      double diameter = _pixelWidth;
//      double radius = diameter / 2;
//      _shape = new Ellipse2D.Double(_xPixel - radius, _yPixel - radius, diameter, diameter);
//    }
  }

  /**
   * @author George A. David
   */
  public void draw(Graphics2D graphics)
  {
    Assert.assert(graphics != null);

    if(isInDrawRegion())
    {
      graphics.draw(_shape);
    }
  }
}