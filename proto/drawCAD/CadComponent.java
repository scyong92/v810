package com.agilent.xRayTest.gui.drawCAD;

import java.awt.*;
import java.awt.geom.*;
import java.util.*;

import com.agilent.xRayTest.datastore.panelDesc.*;
import com.agilent.xRayTest.gui.*;
import com.agilent.util.*;

/**
 * This class controls how to draw an outline of a component.
 *
 * @author Bill Darbie
 */
class CadComponent extends CadShape
{
  private double _xPixel;
  private double _yPixel;
  private double _pixelLength;
  private double _pixelWidth;
  private double _lengthInNanoMeters;
  private double _widthInNanoMeters;
  private int _degreesRotation;

  private DoubleCoordinate _coordinate = null;
  private static Color _normalColor = Color.magenta;

  /**
   * @author George A. David
   */
  CadComponent(ComponentAdapter componentAdapter)
  {
    Assert.assert(componentAdapter != null);

    _coordinate = componentAdapter.getPanelCoordinate();
    _lengthInNanoMeters = componentAdapter.getLengthInNanoMeters();
    _widthInNanoMeters = componentAdapter.getWidthInNanoMeters();
    _degreesRotation = componentAdapter.getDegreesRotationRelativeToCAD();
    calculateComponentDimensions(componentAdapter);
    calculateSize();
  }

  /**
   * Initialize the CadComponent
   */
  public void calculateSize()
  {
//    _xPixel = DrawUtil.getPixelForNanos(_coordinate.getX(), _scale);
//    _yPixel = DrawUtil.getPixelForNanos(_coordinate.getY(), _scale);
//    _pixelWidth = DrawUtil.getPixelForNanos(_widthInNanoMeters, _scale);
//    _pixelLength = DrawUtil.getPixelForNanos(_lengthInNanoMeters, _scale);
//
//    Point2D topLeft = new Point2D.Double();
//    Point2D topRight = new Point2D.Double();
//    Point2D bottomRight = new Point2D.Double();
//    Point2D bottomLeft = new Point2D.Double();
//    Point2D coordinate = new Point2D.Double(_xPixel - _pixelWidth / 2.0, _yPixel - _pixelLength / 2.0);
//
//    DrawUtil.calculateCornersOfRectangle(coordinate,
//                                         _pixelWidth,
//                                         _pixelLength,
//                                         topLeft,
//                                         topRight,
//                                         bottomLeft,
//                                         bottomRight);
//    coordinate.setLocation(_xPixel, _yPixel);
//    DrawUtil.rotateRectangle(topLeft,
//                             topRight,
//                             bottomRight,
//                             bottomLeft,
//                             _degreesRotation,
//                             coordinate);
//    DrawUtil.transformRectangleToScreenCoordinate(topLeft,
//                                                  topRight,
//                                                  bottomRight,
//                                                  bottomLeft,
//                                                  _screenCoordinateOffset);
//    _shape = DrawUtil.createShape(topLeft, topRight, bottomLeft, bottomRight);
  }

  /**
   * @author George A. David
   */
  void calculateComponentDimensions(ComponentAdapter componentAdapter)
  {
    java.util.List pinAdapters = componentAdapter.getPins();
    Iterator it = pinAdapters.iterator();
    int xMin = 0;
    int xMax = 0;
    int yMin = 0;
    int yMax = 0;
    PinAdapter pinAdapter = null;

    // iterate through all the pins and find the
    // min and max x and y coordinates
    while(it.hasNext())
    {
      pinAdapter = (PinAdapter)it.next();
      IntCoordinate coordinate = pinAdapter.getCoordinateInNanoMeters();
      int x = coordinate.getX();
      int y = coordinate.getY();
      xMin = Math.min(xMin, x);
      xMax = Math.max(xMax, x);
      yMin = Math.min(yMin, y);
      yMax = Math.max(yMax, y);
    }
    _widthInNanoMeters = xMax - xMin;
    _lengthInNanoMeters = yMax - yMin;

    if(pinAdapter != null)
    {
      // if either the _pixelWidth or _pixelLength is zero,
      // then set their values to the actual width
      // or length of the pin. This will only occur
      // for components with two pins or less.
       if(_widthInNanoMeters == 0)
        _widthInNanoMeters = pinAdapter.getWidthInNanoMeters();

      if(_lengthInNanoMeters == 0)
        _lengthInNanoMeters = pinAdapter.getLengthInNanoMeters();
    }
  }

  /**
   * @author George A. David
   */
  public void draw(Graphics2D graphics)
  {
    Assert.assert(graphics != null);

    if(isInDrawRegion())
    {
      graphics.draw(_shape);
    }
  }
}
