package com.agilent.xRayTest.gui.drawCAD;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;
import java.util.*;

import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.hardware.*;
import com.agilent.xRayTest.gui.algoTuner.*;
import com.agilent.util.*;

/**
 * This is the panel that actually draws the CAD.
 *
 * @author Bill Darbie
 */
class DrawCadPanel extends JPanel
{
  private static int _xSize = 640;
  private static int _ySize = 480;
  private static final double _scaleMultiplier = 2.0;

  private int _mouseX;
  private int _mouseY;
  private int _mouseWidth;
  private int _mouseLength;
  private int _boxX;
  private int _boxY;
  private int _boxWidth;
  private int _boxLength;

  private boolean _drawMouseRectangle = false;
  private boolean _dragDrawing = false;
  private boolean _drawPanel = true;
  private boolean _drawZoomBox = false;
  private double _scaleFactor = 1.0;

  private int _xOrigin = 0;
  private int _yOrigin = 0;
  private int _prevXOrigin = 0;
  private int _prevYOrigin = 0;

  private Rectangle _boxRegion = new Rectangle();
  private Rectangle _clipRegion = new Rectangle();
  private JButton _jButton = null;
  private CadShapeList _cadShapeList = null;

  /**
   * @author George A. David
   */
  public DrawCadPanel(int xSize, int ySize) throws DatastoreException, NoFieldOfViewHardwareException
  {
    Assert.assert(xSize > 0);
    Assert.assert(ySize > 0);

    _xSize = xSize;
    _ySize = ySize;

    _cadShapeList = new CadShapeList(_xSize, _ySize, _drawPanel);
    calculateOrigin();

    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      Assert.logException(e);
    }
  }

  /**
   * @author Bill Darbie / Andy Mechtenberg
   */
  private void jbInit() throws Exception
  {
    setPreferredSize(new Dimension(_xSize, _ySize));
    this.addComponentListener(new java.awt.event.ComponentAdapter()
    {
      public void componentResized(ComponentEvent e)
      {
        this_componentResized(e);
      }
    });
    this.addMouseMotionListener(new java.awt.event.MouseMotionAdapter()
    {
      public void mouseDragged(MouseEvent e)
      {
        this_mouseDragged(e);
      }
    });
    this.addMouseListener(new java.awt.event.MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        this_mouseReleased(e);
      }
      public void mousePressed(MouseEvent e)
      {
        this_mousePressed(e);
      }
    });
    setBackground(Color.black);
    this.setBorder(BorderFactory.createRaisedBevelBorder());
  }

  /**
   * @author George A. David
   */
  public void paint(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    if (_drawMouseRectangle)
    {
      // the user is creating a box by dragging and dropping
      // we will only redraw the necessary regions.
      graphics2D.translate(_xOrigin * _scaleFactor, _yOrigin * _scaleFactor);
      graphics2D.clearRect(_clipRegion.x - 1, _clipRegion.y - 1, _clipRegion.width + 1, _clipRegion.height + 1);
      CadShape.setClipRegion(_clipRegion);
      graphics2D.setColor(Color.black);
      graphics2D.draw(_clipRegion);

      // there is a bug in java where an image of one of the buttons is drawn on the top left corner of the JPanel
      // the only way around it is to always redraw that region, hence the jButtonHackRegion variable.
      // I could have re-drawn the entire board instead, but on large boards, it takes too long to redraw, and the
      // response to the mouse is slow.
      // George A. David
      java.awt.Rectangle jButtonHackRegion = new Rectangle((int)(-_xOrigin * _scaleFactor), (int)(-_yOrigin * _scaleFactor),_jButton.getWidth() , _jButton.getHeight());
      graphics2D.clearRect(jButtonHackRegion.x, jButtonHackRegion.y, jButtonHackRegion.width, jButtonHackRegion.height);
      CadShape.setJButtonHackRegion(jButtonHackRegion);

      _cadShapeList.draw(graphics2D);
      graphics2D.setColor(Color.white);
      graphics2D.draw(_boxRegion);
      _drawMouseRectangle = false;
    }
    else
    {
      // redraw all graphics
      Dimension dim = getSize();
      graphics2D.clearRect(0, 0, dim.width, dim.height);
      CadShape.setClipRegion(_clipRegion);
      graphics2D.translate(_xOrigin * _scaleFactor, _yOrigin * _scaleFactor);
      _cadShapeList.draw(graphics2D);
      _dragDrawing = false;
    }
  }

  /**
   * @author George A. David
   */
  void this_mousePressed(MouseEvent e)
  {
    if (_scaleFactor != 0)
    {
      _mouseX = e.getX();
      _mouseY = e.getY();
    }
    _boxX = 0;
    _boxY = 0;
    _boxLength = 0;
    _boxWidth = 0;
    _cadShapeList.clearSelectedShapes();
    repaint();
  }

  /**
   * @author George A. David
   */
  void this_mouseDragged(MouseEvent e)
  {
    int modifiers = e.getModifiers();
    if (_scaleFactor != 0)
    {
      _mouseWidth = e.getX()  - _mouseX;
      _mouseLength = e.getY() - _mouseY;
    }

    if (modifiers == e.BUTTON1_MASK)
    {
      // create a box
      _boxX = _mouseX - (int)(_xOrigin * _scaleFactor);
      _boxY = _mouseY - (int)(_yOrigin * _scaleFactor);
      _boxWidth = _mouseWidth;
      _boxLength = _mouseLength;

      if (_boxWidth < 0)
      {
        _boxX = _boxX + _boxWidth;
        _boxWidth = -_boxWidth;
      }
      if (_boxLength < 0)
      {
        _boxY = _boxY + _boxLength;
        _boxLength = -_boxLength;
      }

      // create the region that needs to be redrawn
      _clipRegion.setRect(_boxRegion.x, _boxRegion.y, _boxRegion.width, _boxRegion.height);
      _boxRegion.setRect(_boxX, _boxY, _boxWidth, _boxLength);

      _drawMouseRectangle = true;
    }
    if (modifiers == e.BUTTON3_MASK)
    {
      // drag the cad drawing
      _xOrigin = _prevXOrigin + (int)(_mouseWidth / _scaleFactor);
      _yOrigin = _prevYOrigin + (int)(_mouseLength / _scaleFactor);
      _dragDrawing = true;
    }
    repaint();
  }

  /**
   * @author George A. David
   */
  void this_mouseReleased(MouseEvent e)
  {
    // the user has finished creating a zoom box
    // we need to zoom in
    _drawMouseRectangle = false;
    int modifiers = e.getModifiers();
    if (modifiers == e.BUTTON1_MASK)
    {
      if(_drawZoomBox)
      {
        if(_boxWidth > 1 || _boxLength > 1)
        {
          if(_scaleFactor > 0)
          {
            double prevScaleFactor = _scaleFactor;
            zoomToBestFit(_boxWidth, _boxLength);
            _xOrigin = (int)(((-_boxX - (_boxWidth / 2)) / prevScaleFactor) + ((_xSize / 2) / _scaleFactor));
            _yOrigin = (int)(((-_boxY - (_boxLength / 2)) / prevScaleFactor) + ((_ySize / 2) / _scaleFactor));
          }
        }
      }
      else
      {
        if(_boxLength == 0 || _boxWidth == 0)
        {
          double xInPixels = _mouseX - (_xOrigin * _scaleFactor);
          double yInPixels = _mouseY - (_yOrigin * _scaleFactor);
          _cadShapeList.selectShapes(new Point2D.Double(xInPixels, yInPixels));
        }
        else
          _cadShapeList.selectShapesInRegion(_clipRegion);
      }
    }
    repaint();
    _prevXOrigin = _xOrigin;
    _prevYOrigin = _yOrigin;
    _clipRegion.setRect(0,0,0,0);
    _drawZoomBox = false;
  }

  /**
   * @author George A. David
   */
  void zoomIn()
  {
    _scaleFactor *= _scaleMultiplier;
    _xOrigin -= (_xSize / 2) / _scaleFactor;
    _yOrigin -= (_ySize / 2) / _scaleFactor;
    _prevXOrigin = _xOrigin;
    _prevYOrigin = _yOrigin;
    _cadShapeList.zoomIn();
    _clipRegion.setRect(0,0,0,0);
    repaint();
  }

  /**
   * @author George A. David
   */
  void zoomOut()
  {
    _xOrigin += (_xSize / 2) / _scaleFactor;
    _yOrigin += (_ySize / 2) / _scaleFactor;
    _prevXOrigin = _xOrigin;
    _prevYOrigin = _yOrigin;
    _scaleFactor /= _scaleMultiplier;
    _cadShapeList.zoomOut();
    _clipRegion.setRect(0,0,0,0);
    repaint();
  }

  /**
   * @author George A. David
   */
  void reset()
  {
    _scaleFactor = 1.0;
    _cadShapeList.reset();
    calculateOrigin();
    _boxX = 0;
    _boxY = 0;
    _boxLength = 0;
    _boxWidth = 0;
    _clipRegion.setRect(0,0,0,0);
    repaint();
  }

  /**
   * @author George A. David
   */
  void toggleComponentsVisible()
  {
    _cadShapeList.toggleComponentsVisible();
    repaint();
  }

  /**
   * @author George A. David
   */
  void togglePinsVisible()
  {
    _cadShapeList.togglePinsVisible();
    repaint();
  }

  /**
   * @author George A. David
   */
  void toggleReferenceDesignatorsVisible()
  {
    _cadShapeList.toggleReferenceDesignatorsVisible();
    repaint();
  }

  /**
   * @author George A. David
   */
  void toggleBoardSide()
  {
    _cadShapeList.clearSelectedShapes();
    _cadShapeList.toggleBoardSide();
    repaint();
  }

  /**
   * @author George A. David
   */
  void togglePadsVisible()
  {
    _cadShapeList.togglePadsVisible();
    repaint();
  }

  /**
   * @author George A. David
   */
  void toggleBoardVisible()
  {
    _cadShapeList.toggleBoardVisible();
    repaint();
  }

  /**
   * @author George A. David
   */
  void toggleSurfaceMapVisible()
  {
    _cadShapeList.toggleSurfaceMapVisible();
    repaint();
  }

  /**
   * @author George A. David
   */
  void setJButton(JButton jButton)
  {
    Assert.assert(jButton != null);

    _jButton = jButton;
  }

  /**
   * @author George A. David
   */
  void this_componentResized(ComponentEvent e)
  {
    Dimension dim = getSize();
    _xSize = dim.width;
    _ySize = dim.height;
    _cadShapeList.setDimension(dim);
  }

  /**
   * @author George A. David
   */
  private void calculateOrigin()
  {
    if(_drawPanel)
    {
      _xOrigin = 0;
      _yOrigin = 0;
    }
    else
    {
      DoubleCoordinate origin = _cadShapeList.getBoardOrigin();
      Rectangle2D boardBounds = _cadShapeList.getBoardBounds();
      _xOrigin = (int)Math.rint(-origin.getX() / _scaleFactor);
      _yOrigin = (int)Math.rint((-origin.getY() + boardBounds.getHeight()) / _scaleFactor);
      zoomToBestFit(boardBounds.getWidth(), boardBounds.getHeight());
    }
    _prevXOrigin = _xOrigin;
    _prevYOrigin = _yOrigin;
  }

  /**
   * Using the width and length of a zoom region, calculate the
   * largest the zoom region can be made so it still fits within
   * the confines of the JPanel.
   * @author George A. David
   */
  private void zoomToBestFit(double width, double length)
  {
    if(_scaleFactor > 0)
    {
      if (width / _scaleFactor > 1 || length / _scaleFactor > 1)
      {
        double prevScaleFactor = _scaleFactor;
        if (width != 0 && width > length)
        {
          _scaleFactor = _xSize / (width / prevScaleFactor);
          if( (length / prevScaleFactor) * _scaleFactor > _ySize)
            _scaleFactor = _ySize / (length / prevScaleFactor);
        }
        else if(length != 0 && length > width)
        {
          _scaleFactor = _ySize / (length / _scaleFactor);
          if((width / prevScaleFactor) * _scaleFactor > _xSize)
            _scaleFactor = _xSize / (width / prevScaleFactor);
        }
        _cadShapeList.zoom(_scaleFactor / prevScaleFactor);
      }
    }
  }

  /**
   * Covert the parameter to panel coordinates in nanometers
   * @author George A. David
   */
  private Point2D getPointInPanelCoordinate(Point2D pointInPixels)
  {
    Assert.assert(pointInPixels != null);

    double xInPixels = (pointInPixels.getX() / _scaleFactor) - _xOrigin;
    double yInPixels = (pointInPixels.getY() / _scaleFactor) - _yOrigin;
    java.awt.geom.Rectangle2D panelBoundsInNanoMeters = _cadShapeList.getPanelBoundsInNanoMeters();
    double xInNanos = xInPixels / CadShape.getScale();
    double yInNanos = panelBoundsInNanoMeters.getHeight() - (yInPixels / CadShape.getScale());

    return new Point2D.Double(xInNanos, yInNanos);

  }

  /**
   * @author George A. David
   */
  void prepareToDrawZoomBox()
  {
    _drawZoomBox = true;
  }

  /**
   * @author Bill Darbie
   * @author Andy Mechtenberg
   */
  void writeToFile()
  {
    // wpdfix - this method should really call through RMI to the business layer
    System.out.println("writeToFile stub");
    BufferedImage bufferedImage = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_BGR);
    Graphics graphics = bufferedImage.createGraphics();
    paint(graphics);
    String fileName = "c:/temp/image.jpg";
    //wpdfix - this try catch should not be here
    try
    {
      BufferedImageUtil.writeImageToJpegFile(bufferedImage, fileName);
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

}
