package com.agilent.xRayTest.gui.drawCAD;

import java.awt.*;
import java.awt.geom.*;

import com.agilent.xRayTest.gui.*;
import com.agilent.util.*;

/**
 * This class controls how to draw a panel outline.
 * @author George A. David
 */
class CadPanel extends CadShape
{
  private DoubleCoordinate _coordinate = null;
  private double _xPixel;
  private double _yPixel;
  private double _pixelWidth;
  private double _pixelLength;
  private int _lengthInNanoMeters;
  private int _widthInNanoMeters;
  private int _degreesRotation;
  private static Color _normalColor = Color.pink;

  /**
   * @author George A. David
   */
  CadPanel(PanelAdapter panelAdapter)
  {
    Assert.assert(panelAdapter != null);

    _xPixel = 0;
    _yPixel = 0;
    _lengthInNanoMeters = panelAdapter.getLengthInNanoMeters();
    _widthInNanoMeters = panelAdapter.getWidthInNanoMeters();
    _degreesRotation = panelAdapter.getDegreesRotationRelativeToCad();
    calculateSize();
  }

  /**
   * @author George A. David
   */
  public void calculateSize()
  {
//    _pixelWidth = DrawUtil.getPixelForNanos(_widthInNanoMeters, _scale);
//    _pixelLength = DrawUtil.getPixelForNanos(_lengthInNanoMeters, _scale);
//
//    Point2D topLeft = new Point2D.Double();
//    Point2D topRight = new Point2D.Double();
//    Point2D bottomRight = new Point2D.Double();
//    Point2D bottomLeft = new Point2D.Double();
//    Point2D coordinate = new Point2D.Double(0.0, 0.0);
//
//    DrawUtil.calculateCornersOfRectangle(coordinate,
//                                         _pixelWidth,
//                                         _pixelLength,
//                                         topLeft,
//                                         topRight,
//                                         bottomLeft,
//                                         bottomRight);
//    DrawUtil.rotateRectangle(topLeft,
//                             topRight,
//                             bottomRight,
//                             bottomLeft,
//                             _degreesRotation,
//                             coordinate);
//    Point2D lowerLeftCorner = DrawUtil.getLowerLeftCornerOfRectangle(topLeft, topRight, bottomRight, bottomLeft);
//    _firstQuadrantOffset.setLocation(Math.abs(lowerLeftCorner.getX()), Math.abs(lowerLeftCorner.getY()));
//    DrawUtil.transformRectangleToFirstQuadrant(topLeft,
//                                               topRight,
//                                               bottomRight,
//                                               bottomLeft,
//                                               _firstQuadrantOffset);
//    _shape = DrawUtil.createShape(topLeft, topRight, bottomLeft, bottomRight);
//    java.awt.geom.Rectangle2D bounds = _shape.getBounds2D();
//    _screenCoordinateOffset.setLocation(0.0, bounds.getHeight());
//    DrawUtil.transformRectangleToScreenCoordinate(topLeft,
//                                                  topRight,
//                                                  bottomRight,
//                                                  bottomLeft,
//                                                  _screenCoordinateOffset);
//    _shape = DrawUtil.createShape(topLeft, topRight, bottomLeft, bottomRight);
  }

  /**
   * @author George A. David
   */
  public void draw(Graphics2D graphics)
  {
    Assert.assert(graphics != null);

    graphics.draw(_shape);
  }

  /**
   * @author George A. David
   */
  Rectangle2D getBounds()
  {
    Assert.assert(_shape != null);

    return _shape.getBounds2D();
  }

  /**
   * @author George A. David
   */
  Color getNormalColor()
  {
    return _normalColor;
  }

  /**
   * @author George A. David
   */
  void setNormalColor(Color color)
  {
    Assert.assert(color != null);

    _normalColor = color;
  }
}