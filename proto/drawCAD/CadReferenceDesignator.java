package com.agilent.xRayTest.gui.drawCAD;

import java.awt.*;
import java.awt.geom.*;
import java.util.*;

import com.agilent.xRayTest.datastore.panelDesc.*;
import com.agilent.xRayTest.gui.*;
import com.agilent.util.*;

/**
 * This class draws the reference designators of each component.
 *
 * @author Bill Darbie
 */
class CadReferenceDesignator extends CadShape
{
  private double _xPixel;
  private double _yPixel;
  private String _name = null;
  private DoubleCoordinate _coordinate = null;
  private static Color _normalColor = Color.red;

  /**
   * @author George A. David
   */
  CadReferenceDesignator(String name, DoubleCoordinate coordinate)
  {
    Assert.assert(name != null);
    Assert.assert(coordinate != null);

    _name = name;
    _coordinate = coordinate;
    calculateSize();
  }

  /**
   * Initialize the reference designator
   * @author George A. David
   */
  public void calculateSize()
  {
//    _xPixel = DrawUtil.getPixelForNanos(_coordinate.getX(), _scale);
//    _yPixel = _screenCoordinateOffset.getY() - DrawUtil.getPixelForNanos(_coordinate.getY(), _scale);
  }

  /**
   * @author George A. David
   */
  public void draw(Graphics2D graphics)
  {
    Assert.assert(graphics != null);

    if(_clipRegion.contains(_xPixel, _yPixel) || _jButtonHackRegion.contains(_xPixel, _yPixel));
    {
      graphics.drawString(_name, (int)_xPixel, (int)_yPixel);
    }
  }
}
