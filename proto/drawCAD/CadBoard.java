package com.agilent.xRayTest.gui.drawCAD;

import java.awt.*;
import java.awt.geom.*;

import com.agilent.xRayTest.datastore.panelDesc.*;
import com.agilent.xRayTest.gui.*;
import com.agilent.xRayTest.gui.drawCAD.*;
import com.agilent.util.*;

/**
 * This class controls how to draw a board
 * @author George A. David
 */
class CadBoard extends CadShape
{
  private DoubleCoordinate _coordinate = null;
  private double _xPixel;
  private double _yPixel;
  private double _pixelWidth;
  private double _pixelLength;
  private int _lengthInNanoMeters;
  private int _widthInNanoMeters;
  private int _degreesRotationBoard;
  private int _degreesRotationPanel;
  private static Color _normalColor = Color.lightGray;

  /**
   * @author George A. David
   */
  CadBoard(BoardAdapter boardAdapter, SideBoardAdapter sideBoardAdapter)
  {
    Assert.assert(boardAdapter != null);
    Assert.assert(sideBoardAdapter != null);

    _coordinate = new DoubleCoordinate(sideBoardAdapter.getCoordinateInNanoMeters());
    _degreesRotationBoard = sideBoardAdapter.getDegreesRotationRelativeToPanel();
    _degreesRotationPanel = boardAdapter.getPanelDegreesRotationRelativeToCAD();
    _lengthInNanoMeters = sideBoardAdapter.getLengthInNanoMeters();
    _widthInNanoMeters = sideBoardAdapter.getWidthInNanoMeters();
    calculateSize();
  }

  /**
   * @author George A. David
   */
  public void calculateSize()
  {
//    _xPixel = DrawUtil.getPixelForNanos(_coordinate.getX(), _scale);
//    _yPixel = DrawUtil.getPixelForNanos(_coordinate.getY(), _scale);
//    _pixelWidth = DrawUtil.getPixelForNanos(_widthInNanoMeters, _scale);
//    _pixelLength = DrawUtil.getPixelForNanos(_lengthInNanoMeters, _scale);
//
//    Point2D topLeft = new Point2D.Double();
//    Point2D topRight = new Point2D.Double();
//    Point2D bottomRight = new Point2D.Double();
//    Point2D bottomLeft = new Point2D.Double();
//    DrawUtil.calculateCornersOfRectangle(new Point2D.Double(_xPixel, _yPixel),
//                                         _pixelWidth,
//                                         _pixelLength,
//                                         topLeft,
//                                         topRight,
//                                         bottomLeft,
//                                         bottomRight);
//    DrawUtil.rotateRectangle(topLeft,
//                             topRight,
//                             bottomRight,
//                             bottomLeft,
//                             _degreesRotationBoard);
//    DrawUtil.rotateRectangle(topLeft,
//                             topRight,
//                             bottomRight,
//                             bottomLeft,
//                             _degreesRotationPanel,
//                             new Point2D.Double(0,0));
//
//    DrawUtil.transformRectangleToFirstQuadrant(topLeft,
//                                               topRight,
//                                               bottomRight,
//                                               bottomLeft,
//                                               _firstQuadrantOffset);
//    DrawUtil.transformRectangleToScreenCoordinate(topLeft,
//                                                  topRight,
//                                                  bottomRight,
//                                                  bottomLeft,
//                                                  _screenCoordinateOffset);
//    _shape = DrawUtil.createShape(topLeft, topRight, bottomLeft, bottomRight);
  }

  /**
   * @author George A. David
   */
  public void draw(Graphics2D graphics)
  {
    Assert.assert(graphics != null);

    if(isInDrawRegion())
    {
      graphics.draw(_shape);
    }
  }

  /**
   * Return the bounds of the board oultine.
   * @author George A. David
   */
  java.awt.geom.Rectangle2D getBounds()
  {
    return _shape.getBounds2D();
  }

  DoubleCoordinate getBoardOrigin()
  {
    return new DoubleCoordinate(_xPixel, _screenCoordinateOffset.getY()  -  _yPixel);
  }
}