package com.agilent.xRayTest.gui.drawCAD;

import java.awt.*;
import java.awt.geom.*;

import com.agilent.xRayTest.datastore.panelDesc.*;
import com.agilent.util.*;

/**
 * This class knows how to draw a surface map feature.
 *
 * @author George A. David
 */
public class CadSurfaceMapFeature extends CadShape
{
  private DoubleCoordinate _coordinate = null;
  private double _xPixel;
  private double _yPixel;
  private double _widthInNanoMeters;
  private double _pixelWidth;
  private boolean _isXray = false;
  private boolean _isActive = true;
  private static Color _activeSMFColor = Color.yellow;
  private static Color _deActiveSMFColor = Color.gray;
  private static Color _xRaySMFColor = Color.orange;

  /**
   * @author George A. David
   */
  public CadSurfaceMapFeature(int x, int y, int diameter, int rotation)
  {
    DoubleCoordinate coordinate = new DoubleCoordinate(x, y);
    _coordinate = coordinate.getRotatedCoordinate(rotation);
    _widthInNanoMeters = diameter;
    calculateSize();
  }

  /**
   * @author George A. David
   */
  public void calculateSize()
  {
//    _xPixel = _pixelOffset.getX() + DrawUtil.getPixelForNanos(_coordinate.getX(), _scale);
//    _yPixel = _pixelOffset.getY() - DrawUtil.getPixelForNanos(_coordinate.getY(), _scale);
//    _pixelWidth = DrawUtil.getPixelForNanos(_widthInNanoMeters, _scale);
//    _shape = new Ellipse2D.Double(_xPixel, _yPixel, _pixelWidth, _pixelWidth);
  }

  /**
   * @author George A. David
   */
  public void draw(Graphics2D graphics)
  {
    Assert.assert(graphics != null);

    if(isInDrawRegion())
    {
      Color savedColor = graphics.getColor();
      if(_isXray)
        graphics.setColor(_xRaySMFColor);
      else
      {
        if(_isActive)
          graphics.setColor(_activeSMFColor);
        else
          graphics.setColor(_deActiveSMFColor);
      }
      graphics.fill(_shape);
      graphics.setColor(savedColor);
    }
  }
}