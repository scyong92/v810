package com.agilent.xRayTest.gui.drawCAD;

import java.awt.*;
import java.awt.geom.*;

import com.agilent.xRayTest.datastore.panelDesc.*;
import com.agilent.guiUtil.*;
import com.agilent.util.*;

/**
 * Every type of shape that needs to be drawn should extend this
 * class. All classes derived from this class are forced to provide
 * the following functions:
 * draw()
 * calculateSize()
 *
 * @author Bill Darbie
 */
abstract class CadShape extends com.agilent.guiUtil.Shape
{
  protected static double _scale = 1.0;
  private static double _zoomFactor = 2.0;
  protected java.awt.Shape _shape = null;
  protected boolean _isSelected = false;
  protected static final Color _selectedColor = Color.white;
  protected static java.awt.Rectangle _clipRegion = new java.awt.Rectangle();
  /**
   * This offset is used to take care of two problems:
   * 1) Because of rotation, the coordinates of the may no longer reside
   * in the first quadrant. We need to adjust the coordinates so that they
   * all reside in the first quadrant.
   *
   * 2) The Cartesian coordinate system increases in x as you go right,
   * and increases in y as you go up. In the screen coordinate system,
   * it also increases in x as you go left, but it increases in y as
   * you go down, instead of up. The coordinate offset will handle
   * this as well.
   *
   * First we get the minimum coordinate, which consists of values
   * between 0 and negative infinity, of the bounds of the board.
   * Therefore, the largest a minimum coordinate can be is (0, 0).
   * Once we have the minimum coordinate, we add the absolute value of the
   * minimum coordinate to the coordinates of the board and the components.
   * This accomplishes the task of transposing the board to the first quadrant.
   *
   * In order to handle the descrepency as far as increasing values of y between
   * the Cartesian coordinate system and the screen coordinate system, we will
   * subtract the y adjusted coordinate (first quadrant coordinate) of each component
   * from the height of the bounds of the board.
   *
   * So here are the formulas for x and y
   *
   * X = Xo + |Xmin|
   * Y = boundsHeight - (Yo + |Ymin|)
   *   = boundsHeight - |Ymin| - Yo
   *
   * The coordinate offset returned from this function is:
   *
   * (|Xmin|, boundsHeight - |Ymin|) = (Xoffset, Yoffset)
   *
   * The boundsHeigth is the maximum y point subtracted by the minimum y point:
   *
   * boundsHeight = Ymax - Ymin

   * (|Xmin|, (Ymax - Ymin) - |Ymin|) = (Xoffset, Yoffset)

   * To use the offset, first you need to add the Xoffset to the original X value,
   *  then you need to subtract the original Y value from the Yoffset.
   *
   * @author George A. David
   */
  protected static Point2D _firstQuadrantOffset = new Point2D.Double(0.0, 0.0);
  protected static Point2D _screenCoordinateOffset = new Point2D.Double(0.0, 0.0);

  // there is a bug in java where an image of one of the buttons is drawn on the top left corner of the JPanel
  // the only way around it is to always redraw that region, hence the jButtonHackRegion variable.
  // I could have re-drawn the entire board instead, but on large boards, it takes too long to redraw, and the
  // response to the mouse is slow.
  // George A. David
  protected static java.awt.Rectangle _jButtonHackRegion = new java.awt.Rectangle();

  /**
   * @author George A. David
   */
  public boolean isSelected()
  {
    return _isSelected;
  }

  /**
   * @author George A. David
   */
  void setIsSelected(boolean isSelected)
  {
    _isSelected = isSelected;
  }

  /**
   * @author George A. David
   */
  static void setScale(double scale)
  {
    Assert.assert(scale > 0);

    _firstQuadrantOffset.setLocation(_firstQuadrantOffset.getX() / _scale, _firstQuadrantOffset.getY() / _scale);
    _screenCoordinateOffset.setLocation(_screenCoordinateOffset.getX() / _scale, _screenCoordinateOffset.getY() / _scale);
    _scale = scale;
    _screenCoordinateOffset.setLocation(_screenCoordinateOffset.getX() * _scale, _screenCoordinateOffset.getY() * _scale);
    _firstQuadrantOffset.setLocation(_firstQuadrantOffset.getX() * _scale, _firstQuadrantOffset.getY() * _scale);
  }

  /**
   * @author George A. David
   */
  static double getScale()
  {
    return _scale;
  }

  /**
   * Any CadShapes that fall in the clip region need to be redrawn
   * @author George A. David
   */
  static void setClipRegion(java.awt.Rectangle clipRegion)
  {
    Assert.assert(clipRegion != null);

    _clipRegion = clipRegion;
  }

  /**
   * there is a bug in java where an image of one of the buttons is drawn on the top left corner of the JPanel
   * the only way around it is to always redraw that region, hence the jButtonHackRegion variable.
   * I could have re-drawn the entire board instead, but on large boards, it takes too long to redraw, and the
   * response to the mouse is slow.
   *
   * @author George A. David
   */
  static void setJButtonHackRegion(java.awt.Rectangle jButtonHackRegion)
  {
    Assert.assert(jButtonHackRegion != null);

    _jButtonHackRegion = jButtonHackRegion;
  }

  /**
   * Check if the CadShape is in the region
   * @author George A. David
   */
  protected boolean isInDrawRegion()
  {
    java.awt.geom.Rectangle2D bounds = _shape.getBounds2D();
    if(_clipRegion.isEmpty())
      return true;
    else
    return (_clipRegion.intersects(bounds) || _jButtonHackRegion.contains(bounds) || _jButtonHackRegion.intersects(bounds));

  }

  /**
   * @author George A. David
   */
  boolean isInRegion(java.awt.Rectangle region)
  {
    Assert.assert(region != null);

    if(_shape == null)
      return false;

    java.awt.geom.Rectangle2D bounds = _shape.getBounds2D();
    return region.contains(bounds);
  }

  /**
   * @author George A. David
   */
  boolean contains(Point2D point)
  {
    Assert.assert(point != null);

    if(_shape == null)
      return false;

   return _shape.getBounds2D().contains(point);
  }

  /**
   * @author George A. David
   */
  static void zoom(double zoomFactor)
  {
    Assert.assert(zoomFactor > 0);

    _scale *= zoomFactor;
    _firstQuadrantOffset.setLocation(_firstQuadrantOffset.getX() * zoomFactor, _firstQuadrantOffset.getY() * zoomFactor);
    _screenCoordinateOffset.setLocation(_screenCoordinateOffset.getX() * zoomFactor, _screenCoordinateOffset.getY() * zoomFactor);
  }

  /**
   * @author George A. David
   */
  static void zoomIn()
  {
    _scale *= _zoomFactor;
    _firstQuadrantOffset.setLocation(_firstQuadrantOffset.getX() * _zoomFactor, _firstQuadrantOffset.getY() * _zoomFactor);
    _screenCoordinateOffset.setLocation(_screenCoordinateOffset.getX() * _zoomFactor, _screenCoordinateOffset.getY() * _zoomFactor);
  }

  /**
   * @author George A. David
   */
  static void zoomOut()
  {
    _scale /= _zoomFactor;
    _firstQuadrantOffset.setLocation(_firstQuadrantOffset.getX() / _zoomFactor, _firstQuadrantOffset.getY() / _zoomFactor);
    _screenCoordinateOffset.setLocation(_screenCoordinateOffset.getX() / _zoomFactor, _screenCoordinateOffset.getY() / _zoomFactor);
  }
}
