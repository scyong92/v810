package com.agilent.xRayTest.gui.drawCAD;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.gui.algoTuner.*;
import com.agilent.xRayTest.hardware.*;
import com.agilent.util.*;

/**
 * This is the main frame for the DrawCad piece of the photogrammetry GUI.
 * It holds the panels that displays the cad drawing and the buttons.
 *
 * @author George A. David
 */
public class DrawCadFrame extends JFrame
{
  private static int _maxX = 640;
  private static int _maxY = 480;

  private JPanel _contentPane;
  private BorderLayout _contentPaneBorderLayout = new BorderLayout();
  private DrawCadPanel _drawCadPanel = null;
  private JPanel _eastPanel = new JPanel();
  private JButton _boardOutlineButton = new JButton();
  private JButton _zoomOutButton = new JButton();
  private JButton _pinOutlineButton = new JButton();
  private JButton _padOutlineButton = new JButton();
  private JButton _toggleBoardSide = new JButton();
  private JPanel _buttonPanel = new JPanel();
  private JButton _surfaceMapFeatureButton = new JButton();
  private GridLayout _buttonGridLayout = new GridLayout();
  private JButton _refDesButton = new JButton();
  private JButton _componentOutlineButton = new JButton();
  private JButton _zoomInButton = new JButton();
  private JButton _resetButton = new JButton();
  private JButton _zoomBoxButton = new JButton();

  /**
   * @author George A. David
   */
  public DrawCadFrame() throws DatastoreException, NoFieldOfViewHardwareException
  {
    _drawCadPanel = new DrawCadPanel(_maxX, _maxY);
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      Assert.logException(e);
    }
    _drawCadPanel.setJButton(_resetButton);
  }

  /**
   * @author Bill Darbie / Andy Mechtenberg
   */
  private void jbInit() throws Exception
  {
    _contentPane = (JPanel) this.getContentPane();
    //setIconImage(Toolkit.getDefaultToolkit().createImage(DrawCadFrame.class.getResource("[Your Icon]")));

    _contentPane.setLayout(_contentPaneBorderLayout);
    _boardOutlineButton.setActionCommand("board outlne");
    _boardOutlineButton.setText("board outline");
    _boardOutlineButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _boardOutlineButton_actionPerformed(e);
      }
    });
    _zoomOutButton.setText("zoom out");
    _zoomOutButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _zoomOutButton_actionPerformed(e);
      }
    });
    _pinOutlineButton.setText("pin outline");
    _pinOutlineButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _pinOutlineButton_actionPerformed(e);
      }
    });
    _padOutlineButton.setActionCommand("pad outline");
    _padOutlineButton.setText("pad outline");
    _padOutlineButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _padOutlineButton_actionPerformed(e);
      }
    });
    _toggleBoardSide.setText("Toggle Board Side");
    _toggleBoardSide.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _toggleBoardSide_actionPerformed(e);
      }
    });
    _buttonPanel.setLayout(_buttonGridLayout);
    _surfaceMapFeatureButton.setActionCommand("surface map feature");
    _surfaceMapFeatureButton.setText("surface map feature");
    _surfaceMapFeatureButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _surfaceMapFeatureButton_actionPerformed(e);
      }
    });
    _buttonGridLayout.setRows(11);
    _buttonGridLayout.setColumns(1);
    _refDesButton.setText("reference designators");
    _refDesButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _refDesButton_actionPerformed(e);
      }
    });
    _componentOutlineButton.setText("component outline");
    _componentOutlineButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _componentOutlineButton_actionPerformed(e);
      }
    });
    _zoomInButton.setText("zoom in");
    _zoomInButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _zoomInButton_actionPerformed(e);
      }
    });
    _resetButton.setText("reset");
    _resetButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _resetButton_actionPerformed(e);
      }
    });
    _zoomBoxButton.setActionCommand("zoom box");
    _zoomBoxButton.setText("zoom box");
    _zoomBoxButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _zoomBoxButton_actionPerformed(e);
      }
    });
    _contentPane.add(_drawCadPanel, BorderLayout.CENTER);
    _contentPane.add(_eastPanel, BorderLayout.EAST);
    _eastPanel.add(_buttonPanel, null);
    _buttonPanel.add(_refDesButton, null);
    _buttonPanel.add(_padOutlineButton, null);
    _buttonPanel.add(_toggleBoardSide, null);
    _buttonPanel.add(_pinOutlineButton, null);
    _buttonPanel.add(_componentOutlineButton, null);
    _buttonPanel.add(_boardOutlineButton, null);
    _buttonPanel.add(_surfaceMapFeatureButton, null);
    _buttonPanel.add(_resetButton, null);
    _buttonPanel.add(_zoomOutButton, null);
    _buttonPanel.add(_zoomInButton, null);
    _buttonPanel.add(_zoomBoxButton, null);
//    setSize(new Dimension(_maxX, _maxY));
    setTitle("CAD");
    setBackground(Color.black);
  }

  /**
   * @author Bill Darbie / Andy Mechtenberg
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      dispose();
      System.exit(0);
    }
  }

  /**
   * @author Bill Darbie / Andy Mechtenberg
   */
  void _zoomInButton_actionPerformed(ActionEvent e)
  {
    _drawCadPanel.zoomIn();
  }

  /**
   * @author Bill Darbie / Andy Mechtenberg
   */
  void _zoomOutButton_actionPerformed(ActionEvent e)
  {
    _drawCadPanel.zoomOut();
  }

  /**
   * @author Bill Darbie / Andy Mechtenberg
   */
  void _resetButton_actionPerformed(ActionEvent e)
  {
    _drawCadPanel.reset();
  }

  /**
   * @author Bill Darbie / Andy Mechtenberg
   */
  void _componentOutlineButton_actionPerformed(ActionEvent e)
  {
    _drawCadPanel.toggleComponentsVisible();
  }

  /**
   * @author Bill Darbie / Andy Mechtenberg
   */
  void _pinOutlineButton_actionPerformed(ActionEvent e)
  {
    _drawCadPanel.togglePinsVisible();
  }

  /**
   * @author Bill Darbie / Andy Mechtenberg
   */
  void _refDesButton_actionPerformed(ActionEvent e)
  {
    _drawCadPanel.toggleReferenceDesignatorsVisible();
  }

  /**
   * @author Bill Darbie / Andy Mechtenberg
   */
  void _toggleBoardSide_actionPerformed(ActionEvent e)
  {
    _drawCadPanel.toggleBoardSide();
  }

  /**
   * @author George A. David
   */
  void _padOutlineButton_actionPerformed(ActionEvent e)
  {
    _drawCadPanel.togglePadsVisible();
  }

  /**
   * @author George A. David
   */
  void _boardOutlineButton_actionPerformed(ActionEvent e)
  {
    _drawCadPanel.toggleBoardVisible();
  }

  /**
   * @author George A. David
   */
  void _surfaceMapFeatureButton_actionPerformed(ActionEvent e)
  {
    _drawCadPanel.toggleSurfaceMapVisible();
  }

  /**
   * @author George A. David
   */
  void _zoomBoxButton_actionPerformed(ActionEvent e)
  {
    _drawCadPanel.prepareToDrawZoomBox();
  }
}
