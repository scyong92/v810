package com.agilent.xRayTest.gui.drawCAD;

import java.awt.*;
import java.awt.geom.*;
import java.rmi.*;
import java.util.*;

import com.agilent.xRayTest.business.panelDesc.*;
import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.datastore.panelSettings.*;
import com.agilent.xRayTest.datastore.hwConfig.*;
import com.agilent.xRayTest.hardware.*;
import com.agilent.xRayTest.gui.*;
import com.agilent.guiUtil.*;
import com.agilent.util.*;

/**
 * This class contains a list of all the components that might need to
 * be drawn on the screen.  It allows different components to be set
 * visible or invisible.  It also controls what color each shape is
 * drawn in and the scale of the drawing.
 *
 * @author George A. David
 */
class CadShapeList
{
  private int _xSize;
  private int _ySize;

  private boolean _topSide = true;
  private boolean _topSideLoaded = false;
  private boolean _bottomSideLoaded = false;

  private boolean _showComponents = false;
  private boolean _showPins = false;
  private boolean _showRefDes = false;
  private boolean _showPads = false;
  private boolean _showBoard = false;
  private boolean _showPanel = true;
  private boolean _showSurfaceMap = false;
  private boolean _drawPanel = false;

  private int _xOrigin = 0;
  private int _yOrigin = 0;

  private LayerList _layerList = new LayerList();
  private static final Integer _PANEL_LAYER_TOP = new Integer(1);
  private static final Integer _PANEL_LAYER_BOTTOM = new Integer(2);
  private static final Integer _BOARD_LAYER_TOP = new Integer(3);
  private static final Integer _BOARD_LAYER_BOTTOM = new Integer(4);
  private static final Integer _COMPONENT_LAYER_TOP = new Integer(5);
  private static final Integer _COMPONENT_LAYER_BOTTOM = new Integer(6);
  private static final Integer _PIN_LAYER_TOP = new Integer(7);
  private static final Integer _PIN_LAYER_BOTTOM = new Integer(8);
  private static final Integer _PAD_LAYER_TOP = new Integer(9);
  private static final Integer _PAD_LAYER_BOTTOM = new Integer(10);
  private static final Integer _REF_DES_LAYER_TOP = new Integer(11);
  private static final Integer _REF_DES_LAYER_BOTTOM = new Integer(12);
  private static final Integer _SMF_ACTIVE_LAYER_TOP = new Integer(13);
  private static final Integer _SMF_ACTIVE_LAYER_BOTTOM = new Integer(14);
  private static final Integer _SMF_DEACTIVE_LAYER_TOP = new Integer(15);
  private static final Integer _SMF_DEACTIVE_LAYER_BOTTOM = new Integer(16);
  private static final Integer _X_RAY_POINT_LAYER_TOP = new Integer(17);
  private static final Integer _X_RAY_POINT_LAYER_BOTTOM = new Integer(18);

  private Color _panelColor = Color.pink;
  private Color _boardColor = Color.lightGray;
  private Color _componentColor = Color.magenta;
  private Color _pinColor = Color.blue;
  private Color _padColor = Color.green;
  private Color _refDesColor = Color.red;
  private Color _smfActiveColor = Color.yellow;
  private Color _smfDeactiveColor = Color.cyan;
  private Color _xrayColor = Color.orange;

  private PanelDescAdapter _panelDescAdapter = null;
  private PanelAdapter _panelAdapter = null;
  private java.awt.geom.Rectangle2D _panelBoundsInNanoMeters = null;
  private java.awt.geom.Rectangle2D _boardBounds = null;
  private DoubleCoordinate _cadOrigin = new DoubleCoordinate(0.0, 0.0);

  /**
   * @param xSize the width of the panel the shapes will be drawn on
   * @param ySize the height/length of the panel the shapes will be drawn on
   * @author George A. David
   */
  CadShapeList(int xSize, int ySize, boolean drawPanel) throws DatastoreException, NoFieldOfViewHardwareException
  {
    Assert.assert(xSize > 0);
    Assert.assert(ySize > 0);

    _xSize = xSize;
    _ySize = ySize;
    _drawPanel = drawPanel;

    loadPanel();
    loadShapes();
  }

  /**
   * This is a temporary function that needs to be removed once the photogrammetry gui
   * is using this.
   * @author George A. David
   */
  private void loadPanel() throws DatastoreException, NoFieldOfViewHardwareException
  {
    java.util.List warnings = new ArrayList();
    HardwareConfigInt hardwareConfig = DatastoreFactory.getHardwareConfigInt();
    hardwareConfig.load(DatastoreMode.READ_WRITE, warnings);

    RemotePanelDescInt remotePanelDescInt = null;
    RemoteExceptionHandlerInt exceptionHandler = new RemoteExceptionHandler();
    try
    {
      remotePanelDescInt = RemotePanelDesc.getInstance();
    }
    catch(RemoteException re)
    {
      exceptionHandler.handleRemoteException(re);
      System.exit(1);
    }
    _panelDescAdapter = new PanelDescAdapter(remotePanelDescInt,exceptionHandler);
    String panelName = "MOTO_SPEED";
    //String panelName = "SLEDGEHAMMER";
    //String panelName = "CISCO_SWITCH";
    //String panelName = "PUERTO_RICO";
    //String panelName = "NEPCON_WIDE";
    //String panelName = "GEORGE_TEST";
    //String panelName = "ROTATED_SLEDGE";
    //String panelName = "5064_8726_A3_B";
    //String panelName = "DRAWCADTEST";
    //String panelName = "HP5DX_512";
    _panelAdapter = _panelDescAdapter.loadPanelProgram(DatastoreMode.READ_WRITE,panelName,warnings);
  }

  /**
   * @author George A. David
   */
  void toggleBoardSide()
  {
    _topSide = !_topSide;
    if(_topSide)
    {
      setLayerVisible(_PANEL_LAYER_BOTTOM, false);
      setLayerVisible(_BOARD_LAYER_BOTTOM, false);
      setLayerVisible(_COMPONENT_LAYER_BOTTOM, false);
      setLayerVisible(_PIN_LAYER_BOTTOM, false);
      setLayerVisible(_PAD_LAYER_BOTTOM, false);
      setLayerVisible(_REF_DES_LAYER_BOTTOM, false);

      if(_topSideLoaded)
      {
        setLayerVisible(_PANEL_LAYER_TOP, true);
        setLayerVisible(_BOARD_LAYER_TOP, _showBoard);
        setLayerVisible(_COMPONENT_LAYER_TOP, _showComponents);
        setLayerVisible(_PIN_LAYER_TOP, _showPins);
        setLayerVisible(_PAD_LAYER_TOP, _showPads);
        setLayerVisible(_REF_DES_LAYER_TOP, _showRefDes);

        calculateShapes();
       }
      else
        loadShapes();
    }
    else
    {
      setLayerVisible(_PANEL_LAYER_TOP, false);
      setLayerVisible(_BOARD_LAYER_TOP, false);
      setLayerVisible(_COMPONENT_LAYER_TOP, false);
      setLayerVisible(_PIN_LAYER_TOP, false);
      setLayerVisible(_PAD_LAYER_TOP, false);
      setLayerVisible(_REF_DES_LAYER_TOP, false);

      if(_bottomSideLoaded)
      {
        setLayerVisible(_PANEL_LAYER_BOTTOM, true);
        setLayerVisible(_BOARD_LAYER_BOTTOM, _showBoard);
        setLayerVisible(_COMPONENT_LAYER_BOTTOM, _showComponents);
        setLayerVisible(_PIN_LAYER_BOTTOM, _showPins);
        setLayerVisible(_PAD_LAYER_BOTTOM, _showPads);
        setLayerVisible(_REF_DES_LAYER_BOTTOM, _showRefDes);

        calculateShapes();
      }
      else
        loadShapes();
    }
  }

  /**
   * @author George A. David
   */
  void toggleComponentsVisible()
  {
    _showComponents = !_showComponents;
    Layer layer = null;
    try
    {
      if(_topSide)
        layer = _layerList.getLayer(_COMPONENT_LAYER_TOP);
      else
        layer = _layerList.getLayer(_COMPONENT_LAYER_BOTTOM);
    }
    catch(EntryNotFoundException e)
    {
      // this should not happen
      e.printStackTrace();
      Assert.assert(false,"This is a stub, you need to add exception handling code here.");
    }
    if(layer != null)
    {
      layer.toggleVisible();
      if(_showComponents)
       calculateShapes(layer.getShapes());
    }
  }

  /**
   * @author George A. David
   */
  void togglePinsVisible()
  {
    _showPins = !_showPins;
    Layer layer = null;
    try
    {
      if(_topSide)
        layer = _layerList.getLayer(_PIN_LAYER_TOP);
      else
        layer = _layerList.getLayer(_PIN_LAYER_BOTTOM);
    }
    catch(EntryNotFoundException e)
    {
      // this should not happen
      e.printStackTrace();
      Assert.assert(false,"This is a stub, you need to add exception handling code here.");
    }
    if(layer != null)
    {
      layer.toggleVisible();
      if(_showPins)
        calculateShapes(layer.getShapes());
    }
  }


  /**
   * @author George A. David
   */
  void toggleReferenceDesignatorsVisible()
  {
    _showRefDes = !_showRefDes;
    Layer layer = null;
    try
    {
      if(_topSide)
        layer = _layerList.getLayer(_REF_DES_LAYER_TOP);
      else
        layer = _layerList.getLayer(_REF_DES_LAYER_BOTTOM);
    }
    catch(EntryNotFoundException e)
    {
      // this should not happen
      e.printStackTrace();
      Assert.assert(false,"This is a stub, you need to add exception handling code here.");
    }
    if(layer != null)
    {
      layer.toggleVisible();
      if(_showRefDes)
        calculateShapes(layer.getShapes());
    }
  }

  /**
   * @author George A. David
   */
  void togglePadsVisible()
  {
    _showPads = !_showPads;
    Layer layer = null;
    try
    {
      if(_topSide)
        layer = _layerList.getLayer(_PAD_LAYER_TOP);
      else
        layer = _layerList.getLayer(_PAD_LAYER_BOTTOM);
    }
    catch(EntryNotFoundException e)
    {
      // this should not happen
      e.printStackTrace();
      Assert.assert(false,"This is a stub, you need to add exception handling code here.");
    }
    if(layer != null)
    {
      layer.toggleVisible();
      if(_showPads)
        calculateShapes(layer.getShapes());
    }
  }

  /**
   * @author George A. David
   */
  void toggleBoardVisible()
  {
    _showBoard = !_showBoard;
    Layer layer = null;
    try
    {
      if(_topSide)
        layer = _layerList.getLayer(_BOARD_LAYER_TOP);
      else
        layer = _layerList.getLayer(_BOARD_LAYER_BOTTOM);
    }
    catch(EntryNotFoundException e)
    {
      // this should not happen
      e.printStackTrace();
      Assert.assert(false,"This is a stub, you need to add exception handling code here.");
    }
    if(layer != null)
    {
      layer.toggleVisible();
      if(_showBoard)
        calculateShapes(layer.getShapes());
    }
  }

  /**
   * @author George A. David
   */
  void toggleSurfaceMapVisible()
  {
    _showSurfaceMap = !_showSurfaceMap;
    if(_showSurfaceMap)
    {
    }
  }

  /**
   * @author George A. David
   */
  private void calculateScalingFactor()
  {
     double scaleX = _xSize / _panelBoundsInNanoMeters.getWidth();
     double scaleY = _ySize / _panelBoundsInNanoMeters.getHeight();
     if(scaleX < scaleY)
       CadShape.setScale(scaleX);
     else
       CadShape.setScale(scaleY);
  }

  /**
   * @author George A. David
   */
  private void paintShapes(java.util.List shapes, Graphics2D graphics)
  {
    Assert.assert(shapes != null);
    Assert.assert(graphics != null);

    Iterator it = shapes.iterator();
    while(it.hasNext())
    {
      CadShape cadShape = (CadShape)it.next();
      cadShape.draw(graphics);
    }
  }

  /**
   * Use this to re-initialize all shapes when the scale has changed.
   * @author George A. David
   */
  private void calculateShapes()
  {
    java.util.List layers = _layerList.getLayers();
    Iterator it = layers.iterator();
    while(it.hasNext())
    {
      Layer layer = (Layer)it.next();
      if(layer.isVisible())
      {
        java.util.List shapes = layer.getShapes();
        Iterator sit = shapes.iterator();
        while(sit.hasNext())
        {
          com.agilent.guiUtil.Shape shape = (com.agilent.guiUtil.Shape)sit.next();
          shape.calculateSize();
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  private void calculateShapes(java.util.List shapes)
  {
    Assert.assert(shapes != null);

    Iterator it = shapes.iterator();
    while(it.hasNext())
    {
      com.agilent.guiUtil.Shape shape = (com.agilent.guiUtil.Shape)it.next();
      shape.calculateSize();
    }
  }


  /**
   * @author George A. David
   */
  void draw(Graphics2D graphics)
  {
    Assert.assert(graphics != null);
    _layerList.draw(graphics);
  }

  /**
   * Create all the necessary shapes to display the pins, pads, components, board, and reference designators
   * @author George A. David
   */
  private void loadShapes()
  {
    System.out.println("wpd building shapes");

    if(_drawPanel)
      createPanel();
    else
      createBoard();

    if(_topSide)
      _topSideLoaded = true;
    else
      _bottomSideLoaded = true;

    System.out.println("wpd DONE building shapes");
  }


  /**
   * Reset the scaling factor and the clip region
   * @author George A. David
   */
  void reset()
  {
    calculateScalingFactor();
    CadShape.setClipRegion(new java.awt.Rectangle());
    calculateShapes();
  }

  /**
   * @author George A. David
   */
  void zoom(double zoomFactor)
  {
    CadShape.zoom(zoomFactor);
    calculateShapes();
  }

  /**
   * @author George A. David
   */
  void zoomIn()
  {
    CadShape.zoomIn();
    calculateShapes();
  }

  /**
   * @author George A. David
   */
  void zoomOut()
  {
    CadShape.zoomOut();
    calculateShapes();
  }

  /**
   * @author George A. David
   */
  void setScale(double scale)
  {
    Assert.assert(scale > 0.0);
    CadShape.setScale(scale);
    calculateShapes();
  }

  /**
   * @author George A. David
   */
  void setDimension(Dimension dim)
  {
    Assert.assert(dim != null);

    _xSize = dim.width;
    _ySize = dim.height;
  }

  /**
   * @author George A. David
   */
  java.awt.Rectangle getCadDrawingBounds()
  {
    return _panelBoundsInNanoMeters.getBounds();
  }

  /**
   * @author George A. David
   */
  private void createPanel()
  {
    CadPanel cadPanel = new CadPanel(_panelAdapter);
    Integer layerNumber;
    if(_topSide)
      layerNumber = _PANEL_LAYER_TOP;
    else
      layerNumber = _PANEL_LAYER_BOTTOM;
    Layer layer = new Layer(layerNumber, _panelColor);
    layer.setVisible(_showPanel);
    try
    {
      layer.addShape(cadPanel);
      _layerList.addLayer(layer);
    }
    catch(CannotCreateDuplicateEntryException e)
    {
      // this should not happen
      e.printStackTrace();
      Assert.assert(false, "Exception handling code needs to be added here!");
    }

    if(_panelBoundsInNanoMeters == null)
    {
      _panelBoundsInNanoMeters = cadPanel.getBounds();
      calculateScalingFactor();
      cadPanel.calculateSize();
    }

    java.util.List boardAdapters = _panelAdapter.getBoards();
    Iterator it = boardAdapters.iterator();
    while(it.hasNext())
    {
      BoardAdapter boardAdapter = (BoardAdapter)it.next();
      createBoard(boardAdapter, cadPanel);
    }
  }

  /**
   * @author George A. David
   */
  private void createBoard()
  {
    CadPanel cadPanel = new CadPanel(_panelAdapter);

    Integer layerNumber;
    if(_topSide)
      layerNumber = _PANEL_LAYER_TOP;
    else
      layerNumber = _PANEL_LAYER_BOTTOM;
    Layer layer = new Layer(layerNumber, _panelColor);
    layer.setVisible(_showPanel);
    try
    {
      layer.addShape(cadPanel);
      _layerList.addLayer(layer);
    }
    catch(CannotCreateDuplicateEntryException e)
    {
      // this should not happen
      e.printStackTrace();
      Assert.assert(false, "Exception handling code needs to be added here!");
    }

    if(_panelBoundsInNanoMeters == null)
    {
      _panelBoundsInNanoMeters = cadPanel.getBounds();
      calculateScalingFactor();
      cadPanel.calculateSize();
    }

    //BoardAdapter boardAdapter = _panelDescAdapter.getLegacyCurrentBoard();
    // once this is hooked up to the photogrammetry gui, we can use the above
    // line instead. of the next three lines
    java.util.List boardAdapters = _panelAdapter.getBoards();
    Iterator it = boardAdapters.iterator();
    BoardAdapter boardAdapter = null;

    Assert.assert(it.hasNext());

    boardAdapter = (BoardAdapter)it.next();
    createBoard(boardAdapter, cadPanel);
  }

  /**
   * @author George A. David
   */
  private void createBoard(BoardAdapter boardAdapter, CadPanel cadPanel)
  {
    Assert.assert(boardAdapter != null);
    Assert.assert(cadPanel != null);

    SideBoardAdapter sideBoardAdapter = null;
    if(_topSide)
    {
      if(boardAdapter.topSideBoardExists())
        sideBoardAdapter = boardAdapter.getTopSideBoard();
      else
        return;
    }
    else
    {
      if(boardAdapter.bottomSideBoardExists())
        sideBoardAdapter = boardAdapter.getBottomSideBoard();
      else
        return;
    }

    // create the board
    CadBoard cadBoard = new CadBoard(boardAdapter,sideBoardAdapter);

    Integer layerNumber;
    if(_topSide)
      layerNumber = _BOARD_LAYER_TOP;
    else
      layerNumber = _BOARD_LAYER_BOTTOM;

    Layer layer = null;
    try
    {
      layer = _layerList.getLayer(layerNumber);
    }
    catch(EntryNotFoundException e)
    {
      // this should not happen
      e.printStackTrace();
      Assert.assert(false, "Exception handling code needs to be added here!");
    }

    try
    {
      if(layer == null)
      {
        layer = new Layer(layerNumber, _boardColor);
        _layerList.addLayer(layer);
      }
      layer.setVisible(_showBoard);
      layer.addShape(cadBoard);
    }
    catch(CannotCreateDuplicateEntryException e)
    {
      // this should not happen
      e.printStackTrace();
      Assert.assert(false, "Exception handling code needs to be added here!");
    }

    if(_drawPanel == false && _boardBounds == null)
    {
      _boardBounds = cadBoard.getBounds();
      _cadOrigin = cadBoard.getBoardOrigin();
    }

    createComponents(sideBoardAdapter);
  }

  /**
   * @author George A. David
   */
  private void createComponents(SideBoardAdapter sideBoardAdapter)
  {
    Assert.assert(sideBoardAdapter != null);
    try
    {
      Integer layerNumber;
      if(_topSide)
        layerNumber = _COMPONENT_LAYER_TOP;
      else
        layerNumber = _COMPONENT_LAYER_BOTTOM;
      Layer layer = _layerList.getLayer(layerNumber);
      if(layer == null)
      {
        layer = new Layer(layerNumber, _componentColor);
        layer.setVisible(_showComponents);
        _layerList.addLayer(layer);
      }

      java.util.List componentAdapters = sideBoardAdapter.getComponents();
      Iterator cit = componentAdapters.iterator();
      while(cit.hasNext())
      {
        // create the component
        ComponentAdapter componentAdapter = (ComponentAdapter)cit.next();
        ComponentSettingsAdapter componentSettingsAdapter = componentAdapter.getComponentSettings();
        ComponentInspectEnum componentInspectEnum = componentSettingsAdapter.getComponentInspectEnum();
        if(componentInspectEnum.equals(ComponentInspectEnum.NO_INSPECTION))
          continue;
        CadComponent cadComponent = new CadComponent(componentAdapter);
        layer.addShape(cadComponent);

        createReferenceDesignator(componentAdapter);
        createPins(componentAdapter);
        createPads(componentAdapter);
      }
    }
    catch(CannotCreateDuplicateEntryException e)
    {
      e.printStackTrace();
      Assert.assert(false, "Add exception error handling code here.");
    }
    catch(EntryNotFoundException e)
    {
      e.printStackTrace();
      Assert.assert(false, "Add exception error handling code here.");
    }
  }

  /**
   * @author George A. David
   */
  private void createReferenceDesignator(ComponentAdapter componentAdapter)
  {
    Assert.assert(componentAdapter != null);

    try
    {
      // create the reference designator
      String refDes = componentAdapter.getReferenceDesignator();
      DoubleCoordinate componentCoordinate = new DoubleCoordinate(componentAdapter.getPanelCoordinate());
      CadReferenceDesignator cadRefDes = new CadReferenceDesignator(refDes,componentCoordinate);
      Integer layerNumber;
      if(_topSide)
        layerNumber = _REF_DES_LAYER_TOP;
      else
        layerNumber = _REF_DES_LAYER_BOTTOM;
      Layer layer = _layerList.getLayer(layerNumber);
      if(layer == null)
      {
        layer = new Layer(layerNumber, _refDesColor);
        layer.setVisible(_showRefDes);
        _layerList.addLayer(layer);
      }
      layer.addShape(cadRefDes);
    }
    catch(CannotCreateDuplicateEntryException e)
    {
      e.printStackTrace();
      Assert.assert(false, "Add exception error handling code here.");
    }
    catch(EntryNotFoundException e)
    {
      e.printStackTrace();
      Assert.assert(false, "Add exception error handling code here.");
    }
  }

  /**
   * @author George A. David
   */
  private void createPins(ComponentAdapter componentAdapter)
  {
    Assert.assert(componentAdapter != null);

    try
    {
      Integer layerNumber;
      if(_topSide)
        layerNumber = _PIN_LAYER_TOP;
      else
        layerNumber = _PIN_LAYER_BOTTOM;
      Layer layer = _layerList.getLayer(layerNumber);
      if(layer == null)
      {
        layer = new Layer(layerNumber, _pinColor);
        layer.setVisible(_showPins);
        _layerList.addLayer(layer);
      }

      java.util.List pinAdapters = componentAdapter.getPins();
      Iterator pIt = pinAdapters.iterator();
      while(pIt.hasNext())
      {
        // create a pin
        PinAdapter pinAdapter = (PinAdapter)pIt.next();
        CadPin cadPin = new CadPin(pinAdapter);
        layer.addShape(cadPin);
      }
    }
    catch(CannotCreateDuplicateEntryException e)
    {
      e.printStackTrace();
      Assert.assert(false, "Add exception error handling code here.");
    }
    catch(EntryNotFoundException e)
    {
      e.printStackTrace();
      Assert.assert(false, "Add exception error handling code here.");
    }
  }

  /**
   * @author George A. David
   */
  private void createPads(ComponentAdapter componentAdapter)
  {
    Assert.assert(componentAdapter != null);

    try
    {
      Integer layerNumber;
      if(_topSide)
        layerNumber = _PAD_LAYER_TOP;
      else
        layerNumber = _PAD_LAYER_BOTTOM;
      Layer layer = _layerList.getLayer(layerNumber);
      if(layer == null)
      {
        layer = new Layer(layerNumber, _padColor);
        layer.setVisible(_showPads);
        _layerList.addLayer(layer);
      }

      java.util.List padAdapters = componentAdapter.getTopSidePads();
      Iterator pIt = padAdapters.iterator();
      while(pIt.hasNext())
      {
        // create a pad
        PadAdapter padAdapter = (PadAdapter)pIt.next();
        String pinNumber = padAdapter.getName();
        CadPad cadPad = new CadPad(padAdapter);
        layer.addShape(cadPad);
      }
    }
    catch(CannotCreateDuplicateEntryException e)
    {
      e.printStackTrace();
      Assert.assert(false, "Add exception error handling code here.");
    }
    catch(EntryNotFoundException e)
    {
      e.printStackTrace();
      Assert.assert(false, "Add exception error handling code here.");
    }
  }

  /**
   * @author George A. David
   */
  DoubleCoordinate getBoardOrigin()
  {
    CadBoard cadBoard = null;
    try
    {
      Integer layerNumber;
      if(_topSide)
        layerNumber = _BOARD_LAYER_TOP;
      else
        layerNumber = _BOARD_LAYER_BOTTOM;
      Layer layer = _layerList.getLayer(layerNumber);
      Assert.assert(layer != null);
      java.util.List shapes = layer.getShapes();
      Iterator it = shapes.iterator();
      Assert.assert(it.hasNext());
      cadBoard = (CadBoard)it.next();
    }
    catch(EntryNotFoundException e)
    {
      e.printStackTrace();
      Assert.assert(false, "Add exception error handling code here.");
    }

    return cadBoard.getBoardOrigin();
  }

  /**
   * @author George A. David
   */
  java.awt.geom.Rectangle2D getBoardBounds()
  {
    CadBoard cadBoard = null;
    try
    {
      Integer layerNumber;
      if(_topSide)
        layerNumber = _BOARD_LAYER_TOP;
      else
        layerNumber = _BOARD_LAYER_BOTTOM;
      Layer layer = _layerList.getLayer(layerNumber);
      Assert.assert(layer != null);
      java.util.List shapes = layer.getShapes();
      Iterator it = shapes.iterator();
      Assert.assert(it.hasNext());
      cadBoard = (CadBoard)it.next();
    }
    catch(EntryNotFoundException e)
    {
      e.printStackTrace();
      Assert.assert(false, "Add exception error handling code here.");
    }

    return cadBoard.getBounds();
  }

  /**
   * @author George A. David
   */
  java.awt.geom.Rectangle2D getPanelBoundsInNanoMeters()
  {
    return _panelBoundsInNanoMeters;
  }

  /**
   * @author George A. David
   */
  void setDrawPanel(boolean drawPanel)
  {
    _drawPanel = drawPanel;
  }

  /**
   * @author George A. David
   */
  void selectShapesInRegion(java.awt.Rectangle region)
  {
    java.util.List layers = _layerList.getLayers();
    Iterator it = layers.iterator();
    while(it.hasNext())
    {
      Layer layer = (Layer)it.next();
      selectShapesInRegion(layer.getShapes(), region);
    }
  }

  /**
   * @author George A. David
   */
  void selectShapesInRegion(java.util.List shapes, java.awt.Rectangle region)
  {
    Assert.assert(shapes != null);
    Assert.assert(region != null);

    Iterator it = shapes.iterator();
    while(it.hasNext())
    {
      CadShape shape = (CadShape)it.next();
      if(shape.isInRegion(region))
        shape.setIsSelected(true);
    }
  }

  /**
   * @author George A. David
   */
  void selectShapes(Point2D point)
  {
    Assert.assert(point != null);

    java.util.List layers = _layerList.getLayers();
    Iterator it = layers.iterator();
    while(it.hasNext())
    {
      Layer layer = (Layer)it.next();
      selectShapes(layer.getShapes(), point);
    }
  }

  /**
   * @author George A. David
   */
  void selectShapes(java.util.List shapes, Point2D point)
  {
    Assert.assert(shapes != null);
    Assert.assert(point != null);

    Iterator it = shapes.iterator();
    while(it.hasNext())
    {
      CadShape shape = (CadShape)it.next();
      if(shape.contains(point))
        shape.setIsSelected(true);
    }
  }

  /**
   * @author George A. David
   */
  void clearSelectedShapes()
  {
    java.util.List layers = _layerList.getLayers();
    Iterator it = layers.iterator();
    while(it.hasNext())
    {
      Layer layer = (Layer)it.next();
      clearSelectedShapes(layer.getShapes());
    }
  }

  /**
   * @author George A. David
   */
  void clearSelectedShapes(java.util.List shapes)
  {
    Iterator it = shapes.iterator();
    while(it.hasNext())
    {
      CadShape shape = (CadShape)it.next();
      shape.setIsSelected(false);
    }
  }

  /**
   * @author George A. David
   */
  public void setLayerVisible(Integer layerNumber, boolean visible)
  {
    Assert.assert(layerNumber != null);

    try
    {
      Layer layer = _layerList.getLayer(layerNumber);
      if(layer != null)
        layer.setVisible(visible);
    }
    catch(EntryNotFoundException e)
    {
      e.printStackTrace();
      Assert.assert(false, "Add exception error handling code here.");
    }
  }
}
