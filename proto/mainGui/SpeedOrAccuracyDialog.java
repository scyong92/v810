import java.awt.*;
import java.util.*;
import javax.swing.*;
import java.awt.event.*;

import com.agilent.util.*;

public class SpeedOrAccuracyDialog extends JDialog
{
  private JPanel _mainPanel = new JPanel();
  private Hashtable _labelTable = new Hashtable();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private BorderLayout borderLayout1 = new BorderLayout();
  private GridLayout gridLayout1 = new GridLayout();
  private MainGuiFrame _mainFrame;
  private JPanel _sliderPanel = new JPanel();
  private JPanel jPanel1 = new JPanel();
  private JRadioButton _accuracyRadioButton = new JRadioButton();
  private JRadioButton _balancedRadioButton = new JRadioButton();
  private JRadioButton _speedRadioButton = new JRadioButton();
  private GridLayout gridLayout3 = new GridLayout();
  private FlowLayout flowLayout1 = new FlowLayout();
  private ButtonGroup _accuracyOrSpeedGroup = new ButtonGroup();
  private JLabel jLabel1 = new JLabel();

  public SpeedOrAccuracyDialog(MainGuiFrame mainFrame, String title, boolean modal)
  {
    super(mainFrame, title, modal);
    Assert.expect(mainFrame != null);
    _mainFrame = mainFrame;

    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public void setVisible(boolean visible)
  {
    if (visible)
    {
      //Center the window
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      Dimension frameSize = getSize();
      if (frameSize.height > screenSize.height)
        frameSize.height = screenSize.height;
      if (frameSize.width > screenSize.width)
        frameSize.width = screenSize.width;
      setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
    }

    super.setVisible(visible);
  }

  void jbInit() throws Exception
  {
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _labelTable.put(new Integer(0), new JLabel(" Optimize for Test Throughput"));
    _labelTable.put(new Integer(2), new JLabel(" Optimize for Test Quality"));


    _sliderPanel.setLayout(flowLayout1);
    _accuracyRadioButton.setSelected(true);
    _accuracyRadioButton.setText("Accuracy");
    _balancedRadioButton.setText("Equal Mix of Accuracy and Speed");
    _speedRadioButton.setText("Speed");
    jPanel1.setLayout(gridLayout3);
    gridLayout3.setRows(4);
    gridLayout3.setColumns(1);
    jLabel1.setText("Optimize for Accuracy, Test Speed, or balance them both?");
    getContentPane().add(_mainPanel);

    _okButton.setText("OK");
    _cancelButton.setText("Cancel");
    _mainPanel.setLayout(borderLayout1);
    _innerButtonPanel.setLayout(gridLayout1);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
    _buttonPanel.add(_innerButtonPanel, null);
    _innerButtonPanel.add(_okButton, null);
    _innerButtonPanel.add(_cancelButton, null);
    _mainPanel.add(_sliderPanel, BorderLayout.CENTER);
    _sliderPanel.add(jPanel1, null);
    jPanel1.add(jLabel1, null);
    jPanel1.add(_accuracyRadioButton, null);
    jPanel1.add(_balancedRadioButton, null);
    jPanel1.add(_speedRadioButton, null);
    _accuracyOrSpeedGroup.add(_accuracyRadioButton);
    _accuracyOrSpeedGroup.add(_balancedRadioButton);
    _accuracyOrSpeedGroup.add(_speedRadioButton);
  }

  private void okButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  void cancelButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }
}
