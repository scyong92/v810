import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class SetUpWizardDialog extends JDialog
{
  private JPanel panel1 = new JPanel();
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel _labelPanel = new JPanel();
  private JLabel _titleLabel = new JLabel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();
  private JButton _okButton = new JButton();
  private GridLayout gridLayout1 = new GridLayout();
  private JTextArea _temporayTextArea = new JTextArea();

  public SetUpWizardDialog(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);

    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }

    //Center the window
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = getSize();
    if (frameSize.height > screenSize.height)
      frameSize.height = screenSize.height;
    if (frameSize.width > screenSize.width)
      frameSize.width = screenSize.width;
    setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
  }

  public SetUpWizardDialog()
  {
    this(null, "", false);
  }

  void jbInit() throws Exception
  {
    panel1.setLayout(borderLayout1);
    _titleLabel.setText("Setup Wizard");
    _okButton.setText("OK");
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    _innerButtonPanel.setLayout(gridLayout1);
    _temporayTextArea.setEditable(false);
    _temporayTextArea.append("This wizard will come up only the first time the software is run and ask for things like:\n");
    _temporayTextArea.append("  company name\n");
    _temporayTextArea.append("  pop 3 server (if known) so email about bugs can be sent to Agilent\n");
    _temporayTextArea.append("  if they have more than one system and want to use a central program server\n");
    getContentPane().add(panel1);
    panel1.add(_labelPanel, BorderLayout.NORTH);
    _labelPanel.add(_titleLabel, null);
    panel1.add(_buttonPanel, BorderLayout.SOUTH);
    _buttonPanel.add(_innerButtonPanel, null);
    _innerButtonPanel.add(_okButton, null);
    panel1.add(_temporayTextArea, BorderLayout.CENTER);
  }

  void okButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }
}
