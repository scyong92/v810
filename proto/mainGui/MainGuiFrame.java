import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import javax.swing.*;
import javax.swing.border.*;

import com.agilent.util.*;
import com.agilent.guiUtil.*;
import javax.swing.border.*;

public class MainGuiFrame extends JFrame
{
  private JPanel _contentPane;
  private JMenuBar _menuBar = new JMenuBar();
  private JMenu _fileMenu = new JMenu();
  private JMenuItem _exitMenuItem = new JMenuItem();
  private JMenu _helpMenu = new JMenu();
  private JLabel statusBar = new JLabel();
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel _mainPanel = new JPanel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();
  private JButton _runInspectionButton = new JButton();
  private GridLayout gridLayout2 = new GridLayout();
  private FlowLayout flowLayout1 = new FlowLayout();

  private JPanel _currentPanel = _mainPanel;
  private ProgramDevelopmentPanel _programDevelopmentPanel;
  private RunInspectionPanel _runInspectionPanel;
  private ConfigurationPanel _configurationPanel;
  private JButton _exitButton = new JButton();
  private JLabel _mainMenuLabel = new JLabel();
  private JPanel _mainLabelPanel = new JPanel();
  private JButton _configButton = new JButton();
  private JPanel _currentBoardPanel = new JPanel();
  private JLabel _panelImageLabel = new JLabel();
  private TitledBorder _panelTitledBorder;
  private ImageIcon _panelIcon;
  private ImageIcon _noPanelIcon;
  private String _panelProgramName = "";
  private JMenuItem _gettingStartedHelpMenuItem = new JMenuItem();
  private JMenuItem _aboutHelpMenuItem = new JMenuItem();
  private boolean _pullerMessageDisplayed = false;
  private JMenu _optionsMenu = new JMenu();
  private JMenu _fontSizeMenu = new JMenu();
  private JMenuItem _smallerFontMenuItem = new JMenuItem();
  private JMenuItem _largerFontMenuItem = new JMenuItem();
  private JMenu _languageMenu = new JMenu();
  private JMenuItem _englishMenuItem = new JMenuItem();
  private JMenuItem _germanMenuItem = new JMenuItem();
  private JMenuItem _chineseMenuItem = new JMenuItem();
  private JMenu _accessMenu = new JMenu();
  private JMenuItem _limitedAccessMenuItem = new JMenuItem();
  private JMenuItem _fullAccessMenuItem = new JMenuItem();
  private JButton _developProgramButton = new JButton();
  private Font _currentFont;

  public MainGuiFrame()
  {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception
  {
    _contentPane = (JPanel) this.getContentPane();
    _panelTitledBorder = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),2), _panelProgramName);
    _contentPane.setLayout(borderLayout1);
    this.setSize(new Dimension(639, 519));
    this.setTitle("Agilent Xray Test System");
    _programDevelopmentPanel = new ProgramDevelopmentPanel(this);
    _runInspectionPanel = new RunInspectionPanel(this);
    _configurationPanel = new ConfigurationPanel(this);
    statusBar.setBorder(BorderFactory.createEtchedBorder());
    statusBar.setText(" ");
    _fileMenu.setText("File");
    _exitMenuItem.setText("Exit");
    _exitMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _exitMenuItem_actionPerformed(e);
      }
    });
    _helpMenu.setText("Help");
    _mainPanel.setLayout(borderLayout2);
    _buttonPanel.setLayout(flowLayout1);
    _runInspectionButton.setText("Run Test");
    _runInspectionButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        inspectJointsButton_actionPerformed(e);
      }
    });
    _innerButtonPanel.setLayout(gridLayout2);
    gridLayout2.setRows(7);
    gridLayout2.setColumns(1);
    _exitButton.setText("Exit");
    _exitButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        exitButton_actionPerformed(e);
      }
    });
    _mainMenuLabel.setText("Main Menu");
    _configButton.setToolTipText("");
    _configButton.setText("Configuration");
    _configButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        configButton_actionPerformed(e);
      }
    });
    _panelIcon = new ImageIcon(MainGuiFrame.class.getResource("panel1.jpg"));
    _noPanelIcon = new ImageIcon(MainGuiFrame.class.getResource("noPanel.jpg"));
    _panelImageLabel.setIcon(new ImageIcon(MainGuiFrame.class.getResource("splash5dx.gif")));
    _currentBoardPanel.setBorder(_panelTitledBorder);
    _panelTitledBorder.setTitleJustification(TitledBorder.CENTER);
    _gettingStartedHelpMenuItem.setText("Getting Started Tutorial");
    _aboutHelpMenuItem.setText("About");
    _optionsMenu.setText("Options");
    _fontSizeMenu.setText("Font Size");
    _smallerFontMenuItem.setText("Smaller");
    _smallerFontMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        smallerFontMenuItem_actionPerformed(e);
      }
    });
    _largerFontMenuItem.setText("Larger");
    _largerFontMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        largerFontMenuItem_actionPerformed(e);
      }
    });
    _languageMenu.setText("Language");
    _englishMenuItem.setText("English");
    _germanMenuItem.setText("German");
    _chineseMenuItem.setText("Chinese");
    _accessMenu.setText("Access");
    _limitedAccessMenuItem.setText("Limited");
    _limitedAccessMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        limitedAccessMenuItem_actionPerformed(e);
      }
    });
    _fullAccessMenuItem.setText("Full");
    _fullAccessMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        fullAccessMenuItem_actionPerformed(e);
      }
    });
    _developProgramButton.setText("Program Development");
    _developProgramButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        developProgramButton_actionPerformed(e);
      }
    });
    _fileMenu.add(_exitMenuItem);
    _helpMenu.add(_gettingStartedHelpMenuItem);
    _helpMenu.add(_aboutHelpMenuItem);
    _menuBar.add(_fileMenu);
    _menuBar.add(_optionsMenu);
    _menuBar.add(_helpMenu);
    this.setJMenuBar(_menuBar);
    _contentPane.add(statusBar, BorderLayout.SOUTH);
    _contentPane.add(_mainPanel, BorderLayout.CENTER);
    _mainPanel.add(_buttonPanel, BorderLayout.WEST);
    _buttonPanel.add(_innerButtonPanel, null);
    _innerButtonPanel.add(_runInspectionButton, null);
    _innerButtonPanel.add(_developProgramButton, null);
    _innerButtonPanel.add(_configButton, null);
    _innerButtonPanel.add(_exitButton, null);
    _mainPanel.add(_mainLabelPanel, BorderLayout.NORTH);
    _mainLabelPanel.add(_mainMenuLabel, null);
    _mainPanel.add(_currentBoardPanel, BorderLayout.CENTER);
    _currentBoardPanel.add(_panelImageLabel, null);
    _optionsMenu.add(_fontSizeMenu);
    _optionsMenu.add(_languageMenu);
    _optionsMenu.add(_accessMenu);
    _fontSizeMenu.add(_smallerFontMenuItem);
    _fontSizeMenu.add(_largerFontMenuItem);
    _languageMenu.add(_englishMenuItem);
    _languageMenu.add(_germanMenuItem);
    _languageMenu.add(_chineseMenuItem);
    _accessMenu.add(_limitedAccessMenuItem);
    _accessMenu.add(_fullAccessMenuItem);
  }

  public void _exitMenuItem_actionPerformed(ActionEvent e)
  {
    System.exit(0);
  }

  public void jMenuHelpAbout_actionPerformed(ActionEvent e)
  {
    MainGuiFrame_AboutBox dlg = new MainGuiFrame_AboutBox(this);
    Dimension dlgSize = dlg.getPreferredSize();
    Dimension frmSize = getSize();
    Point loc = getLocation();
    dlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
    dlg.setModal(true);
    dlg.show();
  }

  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      _exitMenuItem_actionPerformed(null);
    }
  }

  private void inspectJointsButton_actionPerformed(ActionEvent e)
  {
    switchMainPanel(_runInspectionPanel);
  }

  private void exitButton_actionPerformed(ActionEvent e)
  {
    System.exit(0);
  }

  void backToMainMenu()
  {
    switchMainPanel(_mainPanel);
  }

  private void switchMainPanel(JPanel panel)
  {
    Assert.expect(panel != null);

    _contentPane.remove(_currentPanel);
    _currentPanel = panel;
    if (_currentFont != null)
      SwingUtils.setFont(_currentPanel, _currentFont);
    _contentPane.add(_currentPanel, BorderLayout.CENTER);
    _contentPane.revalidate();
    _contentPane.repaint();
  }

  private void loadPanelProgramIfNeeded()
  {
    if (_panelProgramName.length() == 0)
    {
      loadPanelProgram();
    }
  }

  private void goneButton_actionPerformed(ActionEvent e)
  {
    String title = "Things that have been completely removed from the old 5dx main GUI";
    String list = "all calibration menu items\n" +
                  "all confirmation and diagnostics menu items\n" +
                  "reset IAS\n" +
                  "automatic startup\n";
    JOptionPane.showMessageDialog(this, list, title, JOptionPane.ERROR_MESSAGE);



  }

  private void loadPanelProgramButton_actionPerformed(ActionEvent e)
  {
    loadPanelProgram();

    if (_pullerMessageDisplayed == false)
    {
      _pullerMessageDisplayed = true;
      String message = "A newer version of the panel program is available on your panel program server.\n" +
                       "Would you like to copy it over to this machine?";
      JOptionPane.showConfirmDialog(this, message, "Panel Puller", JOptionPane.YES_NO_CANCEL_OPTION);
    }
  }

  void loadPanelProgram()
  {
    JDialog dialog = new PanelProgramSelectorDialog(this, "Select Panel Program", true);
    dialog.setVisible(true);
  }

  void setCurrentPanel(String panelName)
  {
    Assert.expect(panelName != null);

//    _panelImageLabel.setText("");
//    _panelImageLabel.setIcon(_panelIcon);
    _panelProgramName = panelName;
//    _panelTitledBorder.setTitle(panelName);
//    _contentPane.revalidate();
//    _contentPane.repaint();
  }

  private void developProgramButton_actionPerformed(ActionEvent e)
  {
    String[] options = new String[]{"Modify", "Create", "Cancel"};
    int choice = JOptionPane.showOptionDialog(this,
                                              "Do you want to modify an existing program or develop a new one?",
                                              "Modify or Create?",
                                              JOptionPane.YES_NO_CANCEL_OPTION,
                                              JOptionPane.QUESTION_MESSAGE,
                                              null,
                                              options,
                                              options[0]);
    if (choice == JOptionPane.YES_OPTION)
    {
      loadPanelProgram();
    }
    else if (choice == JOptionPane.NO_OPTION)
    {
      _panelProgramName = JOptionPane.showInputDialog(this,
                                                      "Enter Panel Name",
                                                      "Panel Name Entry",
                                                      JOptionPane.INFORMATION_MESSAGE);
    }
    else
      return;

    if (_panelProgramName == null)
      _panelProgramName = "";
    switchMainPanel(_programDevelopmentPanel);
  }


  private void modifyProgramButton_actionPerformed(ActionEvent e)
  {
    loadPanelProgramIfNeeded();
    switchMainPanel(_programDevelopmentPanel);
  }

  private void createNewProgramButton_actionPerformed(ActionEvent e)
  {
    _panelProgramName = JOptionPane.showInputDialog(this,
                                                    "Enter New Panel Program Name",
                                                    "New Program Name",
                                                    JOptionPane.PLAIN_MESSAGE);
    switchMainPanel(_programDevelopmentPanel);
  }


  private void configButton_actionPerformed(ActionEvent e)
  {
    switchMainPanel(_configurationPanel);
  }

  private void limitedAccessMenuItem_actionPerformed(ActionEvent e)
  {
    _developProgramButton.setEnabled(false);
    _configButton.setEnabled(false);
  }

  private void fullAccessMenuItem_actionPerformed(ActionEvent e)
  {
    JOptionPane.showInputDialog(this,
                                "Enter your password",
                                "Full Access Password",
                                JOptionPane.PLAIN_MESSAGE);

    _developProgramButton.setEnabled(true);
    _configButton.setEnabled(true);
  }

  void runSetUpWizard()
  {
    JDialog wizard = new SetUpWizardDialog(this, "Agilent Xray Tester Setup Wizard", true);
    wizard.setVisible(true);
  }

  private void smallerFontMenuItem_actionPerformed(ActionEvent e)
  {
    Font font = getFont();
    _currentFont = new Font(font.getName(), font.getStyle(), font.getSize() - 2);
    SwingUtils.setFont(this, _currentFont);
  }

  private void largerFontMenuItem_actionPerformed(ActionEvent e)
  {
    enlargeFont();
  }

  private void enlargeFont()
  {
    Font font = getFont();
    _currentFont = new Font(font.getName(), font.getStyle(), font.getSize() + 2);
    SwingUtils.setFont(this, _currentFont);
  }
}
