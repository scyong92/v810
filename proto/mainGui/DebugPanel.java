import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import com.agilent.util.*;
import com.agilent.guiUtil.*;

public class DebugPanel extends JPanel
{
  private BorderLayout borderLayout1 = new BorderLayout();
  private ProgramDevelopmentPanel _programDevelopmentPanel;
  private ImageFocusPanel _imageFocusPanel;
  private JPanel _mainPanel = new JPanel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private JButton _doneButton = new JButton();
  private JPanel _innerButtonPanel = new JPanel();
  private FlowLayout flowLayout1 = new FlowLayout();
  private JLabel _debugLabel = new JLabel();
  private JButton _imageFocusButton = new JButton();
  private JPanel _labelPanel = new JPanel();
  private GridLayout gridLayout2 = new GridLayout();
  private JPanel _buttonPanel = new JPanel();
  private JButton _algorithmSettingsButton = new JButton();
  private JPanel _currentPanel = _mainPanel;
  private JButton _learnFromPreviousCallsButton = new JButton();
  private AlgorithmSettingsPanel _algorithmSettingsPanel;
  private JTextArea _textArea = new JTextArea();
  private JButton _fieldOfViewSettingsButton = new JButton();
  private JButton _cameraSettingsButton = new JButton();

  public DebugPanel(ProgramDevelopmentPanel programDevelopmentPanel)
  {
    Assert.expect(programDevelopmentPanel != null);
    _programDevelopmentPanel = programDevelopmentPanel;

    _imageFocusPanel = new ImageFocusPanel(this);
    _algorithmSettingsPanel = new AlgorithmSettingsPanel(this);

    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception
  {
    _algorithmSettingsButton.setText("Algorithm Settings");
    _algorithmSettingsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        algorithmSettingsButton_actionPerformed(e);
      }
    });
    _buttonPanel.setLayout(flowLayout1);
    gridLayout2.setRows(15);
    gridLayout2.setColumns(1);
    _imageFocusButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        imageFocusButton_actionPerformed(e);
      }
    });
    _imageFocusButton.setText("Image Focus");
    _debugLabel.setText("5dx Debuger");
    _innerButtonPanel.setLayout(gridLayout2);
    _doneButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doneButton_actionPerformed(e);
      }
    });
    _doneButton.setText("Done");
    this.setLayout(borderLayout1);
    _mainPanel.setLayout(borderLayout2);
    _learnFromPreviousCallsButton.setText("Learn From Previous Calls");
    _learnFromPreviousCallsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        learnFromPreviousCallsButton_actionPerformed(e);
      }
    });
    _fieldOfViewSettingsButton.setText("Field Of View Settings");
    _fieldOfViewSettingsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        viewSettingsButton_actionPerformed(e);
      }
    });
    _cameraSettingsButton.setText("Camera Settings");
    _cameraSettingsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cameraSettingsButton_actionPerformed(e);
      }
    });
    this.add(_mainPanel,  BorderLayout.CENTER);
    _innerButtonPanel.add(_imageFocusButton, null);
    _innerButtonPanel.add(_fieldOfViewSettingsButton, null);
    _innerButtonPanel.add(_algorithmSettingsButton, null);
    _innerButtonPanel.add(_cameraSettingsButton, null);
    _innerButtonPanel.add(_learnFromPreviousCallsButton, null);
    _innerButtonPanel.add(_doneButton, null);
    _mainPanel.add(_textArea,  BorderLayout.CENTER);
    _mainPanel.add(_labelPanel, BorderLayout.NORTH);
    _labelPanel.add(_debugLabel, null);
    _mainPanel.add(_buttonPanel, BorderLayout.WEST);
    _buttonPanel.add(_innerButtonPanel, null);
  }

  void doneButton_actionPerformed(ActionEvent e)
  {
    _programDevelopmentPanel.backToMainScreen();
  }

  private void switchMainPanel(JPanel panel)
  {
    Assert.expect(panel != null);

    remove(_currentPanel);
    _currentPanel = panel;
    SwingUtils.setFont(_currentPanel, getFont());
    add(_currentPanel, BorderLayout.CENTER);
    revalidate();
    repaint();
  }

  void backToMainScreen()
  {
    switchMainPanel(_mainPanel);
  }

  void inspectionSettingsButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("set things like:\n");
    _textArea.append("  retest failing pins\n");
    _textArea.append("  aware test data enabled\n");
  }

  void imageFocusButton_actionPerformed(ActionEvent e)
  {
    switchMainPanel(_imageFocusPanel);
  }

  void algorithmSettingsButton_actionPerformed(ActionEvent e)
  {
    switchMainPanel(_algorithmSettingsPanel);
  }

  void learnFromPreviousCallsButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("classifier");
  }

  void viewSettingsButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("set things like:\n");
    _textArea.append("  FOV assignment\n");
  }

  void cameraSettingsButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("set things like:\n");
    _textArea.append("  number of camera integrations per joint\n");
    _textArea.append("  camera contrast (no index)\n");
  }
}
