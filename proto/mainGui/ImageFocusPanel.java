import java.awt.*;

import javax.swing.*;

import com.agilent.util.*;
import java.awt.event.*;

public class ImageFocusPanel extends JPanel
{
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();
  private JButton _stageSpeedButton = new JButton();
  private JButton _surfaceMapPointsButton = new JButton();
  private JButton _alignmentPointsButton = new JButton();
  private GridLayout gridLayout1 = new GridLayout();
  private JButton _panelThicknessButton = new JButton();
  private JButton _doneButton = new JButton();
  private JPanel _labelPanel = new JPanel();
  private JLabel _imageFocusLabel = new JLabel();
  private DebugPanel _debugPanel;
  private JButton _verifyViewFocusButton = new JButton();
  private JTextArea _textArea = new JTextArea();

  public ImageFocusPanel(DebugPanel debugPanel)
  {
    Assert.expect(debugPanel != null);
    _debugPanel = debugPanel;

    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    _stageSpeedButton.setText("Stage Speed");
    _stageSpeedButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        stageSpeedButton_actionPerformed(e);
      }
    });
    _surfaceMapPointsButton.setText("Surface Map Points");
    _surfaceMapPointsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        surfaceMapPointsButton_actionPerformed(e);
      }
    });
    _alignmentPointsButton.setText("Alignment Points");
    _alignmentPointsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        alignmentPointsButton_actionPerformed(e);
      }
    });
    _innerButtonPanel.setLayout(gridLayout1);
    gridLayout1.setColumns(1);
    gridLayout1.setRows(10);
    _panelThicknessButton.setText("Panel Thickness");
    _panelThicknessButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        panelThicknessButton_actionPerformed(e);
      }
    });
    _doneButton.setText("Done");
    _doneButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doneButton_actionPerformed(e);
      }
    });
    _imageFocusLabel.setText("Image Focus");
    _verifyViewFocusButton.setText("Verify Views and CAD");
    _verifyViewFocusButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        verifyViewFocusButton_actionPerformed(e);
      }
    });
    this.add(_buttonPanel,  BorderLayout.WEST);
    _buttonPanel.add(_innerButtonPanel, null);
    _innerButtonPanel.add(_panelThicknessButton, null);
    _innerButtonPanel.add(_alignmentPointsButton, null);
    _innerButtonPanel.add(_surfaceMapPointsButton, null);
    _innerButtonPanel.add(_stageSpeedButton, null);
    _innerButtonPanel.add(_verifyViewFocusButton, null);
    _innerButtonPanel.add(_doneButton, null);
    this.add(_labelPanel, BorderLayout.NORTH);
    _labelPanel.add(_imageFocusLabel, null);
    this.add(_textArea,  BorderLayout.CENTER);
  }

  void doneButton_actionPerformed(ActionEvent e)
  {
    _debugPanel.backToMainScreen();
  }

  void verifyViewFocusButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("verify views are correct here, allow setting view height\n");
    _textArea.append("draw CAD on top of each View and verify that CAD is correct\n");
    _textArea.append("allow user to choose between checking ALL views or\n");
    _textArea.append("on view for each unique land pattern\n");
    _textArea.append("provide the same modify CAD screen from CAD Entry here\n");
    _textArea.append("to allow CAD editing as needed\n");
    _textArea.append("to allow CAD editing as needed\n");
    _textArea.append("\n");
    _textArea.append("verify:\n");
    _textArea.append("  CAD\n");
    _textArea.append("  Algorithm Family assignment (JLead algorithm to JLead joint)\n");
  }

  void panelThicknessButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("Set panel thickness\n");
    _textArea.append("Allow user to specify measuring thickness at inspection time\n");
    _textArea.append("Allow user to specify thickness point(s) to measure\n");
  }

  void alignmentPointsButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("allow user to modify alignment points here\n");
  }

  void surfaceMapPointsButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("allow user to modify, add, delete surface map points");
    _textArea.append("for each surface map point allow user to view an x-ray image\n");
    _textArea.append("so they can verify the points are set up properly\n");
  }

  void stageSpeedButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("allow user to set stage speed, run program and verify that images are in focus.\n");
  }
}
