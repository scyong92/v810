import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import com.agilent.util.*;

public class CadEntryPanel extends JPanel
{
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();
  private JButton _modifyCadButton = new JButton();
  private JButton _selectCadFileButton = new JButton();
  private GridLayout gridLayout1 = new GridLayout();
  private JButton _doneButton = new JButton();
  private ProgramDevelopmentPanel _programDevelopmentPanel;
  private JPanel _labelPanel = new JPanel();
  private JLabel cadEntryLabel = new JLabel();
  private JTextArea _textArea = new JTextArea();
  private JButton _importPackagesButton = new JButton();
  private JButton _exportPackagesButton = new JButton();
  private JButton _createPackagesButton = new JButton();
  private JButton _createPanelButton = new JButton();

  public CadEntryPanel(ProgramDevelopmentPanel programDevelopmentPanel)
  {
    Assert.expect(programDevelopmentPanel != null);
    _programDevelopmentPanel = programDevelopmentPanel;
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    _modifyCadButton.setText("Modify CAD");
    _modifyCadButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        modifyCadButton_actionPerformed(e);
      }
    });
    _selectCadFileButton.setText("Select CAD file");
    _selectCadFileButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectCadFileButton_actionPerformed(e);
      }
    });
    _innerButtonPanel.setLayout(gridLayout1);
    gridLayout1.setColumns(1);
    gridLayout1.setRows(10);
    _doneButton.setText("Done");
    _doneButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doneButton_actionPerformed(e);
      }
    });
    cadEntryLabel.setText("CAD Entry");
    _importPackagesButton.setText("Import Packages from Library");
    _importPackagesButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        importPackagesButton_actionPerformed(e);
      }
    });
    _exportPackagesButton.setText("Export Packages to Library");
    _exportPackagesButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        exportPackagesButton_actionPerformed(e);
      }
    });
    _createPackagesButton.setText("Create Packages");
    _createPackagesButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        createPackagesButton_actionPerformed(e);
      }
    });
    _createPanelButton.setText("Create Panel");
    _createPanelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        createPanelButton_actionPerformed(e);
      }
    });
    this.add(_buttonPanel,  BorderLayout.WEST);
    _buttonPanel.add(_innerButtonPanel, null);
    _innerButtonPanel.add(_selectCadFileButton, null);
    _innerButtonPanel.add(_createPanelButton, null);
    _innerButtonPanel.add(_importPackagesButton, null);
    _innerButtonPanel.add(_createPackagesButton, null);
    _innerButtonPanel.add(_modifyCadButton, null);
    _innerButtonPanel.add(_exportPackagesButton, null);
    _innerButtonPanel.add(_doneButton, null);
    this.add(_labelPanel, BorderLayout.NORTH);
    _labelPanel.add(cadEntryLabel, null);
    this.add(_textArea,  BorderLayout.CENTER);
  }

  void verifyCadButton_actionPerformed(ActionEvent e)
  {
    JOptionPane.showMessageDialog(this,
                                  "Please place a known good panel in the machine so the CAD can be verified",
                                  "Waiting for Panel Load",
                                  JOptionPane.INFORMATION_MESSAGE);
    String message = "show an Xray Image and draw CAD on top of it.\n" +
                     "Have the user verify that CAD is accurate or graphically fix it.";
    JOptionPane.showMessageDialog(this, message, "CAD Verification", JOptionPane.INFORMATION_MESSAGE);
  }

  void doneButton_actionPerformed(ActionEvent e)
  {
    _programDevelopmentPanel.backToMainScreen();
  }

  void selectCadFileButton_actionPerformed(ActionEvent e)
  {
    JFileChooser fileChooser = new JFileChooser();
    fileChooser.showOpenDialog(this);
  }

  void modifyCadButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("Modify and create CAD Functionality:\n");
    _textArea.append("  no loads\n");
    _textArea.append("  ECO's\n");
    _textArea.append("  creating new land patterns (replaces inspection zones)\n");
    _textArea.append("  any type of modifications\n");
    _textArea.append("\n");
  }

  void importPackagesButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("Import Package information from a Package library here\n");
    _textArea.append("Note that threshold info would not be a part of this anymore\n");
  }

  void createPackagesButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("Give the user the ability to create packages here\n");
    _textArea.append("It would be best to eliminate the need for packages entirely\n");
    _textArea.append("by removing the need for them in the algorithms");
    _textArea.append("\n");
    _textArea.append("Lets stop using terms like pitch, pin x,y to mean something that\n");
    _textArea.append("they are not here\n");
  }

  void exportPackagesButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("export new package definitions to package library here\n");
  }

  void createPanelButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("same as Today's Define Panel in Test Link\n");
  }
}
