import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import com.agilent.util.*;

public class AlgorithmSettingsPanel extends JPanel
{
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();
  private JButton _tuneThresholdsButton = new JButton();
  private JButton _algorithmAssignmentButton = new JButton();
  private GridLayout gridLayout1 = new GridLayout();
  private JPanel _labelPanel = new JPanel();
  private JLabel _algSettingsLabel = new JLabel();
  private JTextArea _textArea = new JTextArea();
  private DebugPanel _debugPanel;
  private JButton _doneButton = new JButton();
  private JButton _enableDisableJointInspectionButton = new JButton();
  private JButton _importAlgFamilyGroupsButton = new JButton();
  private JButton _exportAlgFamilyGroupsButton = new JButton();

  public AlgorithmSettingsPanel(DebugPanel debugPanel)
  {
    Assert.expect(debugPanel != null);
    _debugPanel = debugPanel;

    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    _tuneThresholdsButton.setText("Tune Thresholds");
    _tuneThresholdsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tuneThresholdsButton_actionPerformed(e);
      }
    });
    _algorithmAssignmentButton.setText("Algorithm Assignment");
    _algorithmAssignmentButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        algorithmAssignmentButton_actionPerformed(e);
      }
    });
    _innerButtonPanel.setLayout(gridLayout1);
    gridLayout1.setColumns(1);
    gridLayout1.setRows(10);
    _algSettingsLabel.setText("Algorithm Settings");
    _doneButton.setText("Done");
    _doneButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doneButton_actionPerformed(e);
      }
    });
    _enableDisableJointInspectionButton.setText("Enable/Disable Joint Inspection");
    _enableDisableJointInspectionButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enableDisableJointInspectionButton_actionPerformed(e);
      }
    });
    _importAlgFamilyGroupsButton.setText("Import Algorithm Family Groups from Library");
    _importAlgFamilyGroupsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        importAlgSettingsButton_actionPerformed(e);
      }
    });
    _exportAlgFamilyGroupsButton.setText("Export Algorithm Family Groups to Library");
    _exportAlgFamilyGroupsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        exportAlgSettingsButton_actionPerformed(e);
      }
    });
    this.add(_buttonPanel,  BorderLayout.WEST);
    _buttonPanel.add(_innerButtonPanel, null);
    _innerButtonPanel.add(_enableDisableJointInspectionButton, null);
    _innerButtonPanel.add(_importAlgFamilyGroupsButton, null);
    _innerButtonPanel.add(_algorithmAssignmentButton, null);
    _innerButtonPanel.add(_tuneThresholdsButton, null);
    this.add(_labelPanel,  BorderLayout.NORTH);
    _labelPanel.add(_algSettingsLabel, null);
    this.add(_textArea, BorderLayout.CENTER);
    _innerButtonPanel.add(_exportAlgFamilyGroupsButton, null);
    _innerButtonPanel.add(_doneButton, null);
  }

  void algorithmAssignmentButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("Assign an algorithm family group (subtype) to each Joint here\n");
    _textArea.append("Also allow creation of new algorithm family groups (subtypes)\n");
    _textArea.append("have system confirm (with user) that algorithm family group assignment is correct\n");
    _textArea.append("by showing x-ray image of joint being assigned\n");
    _textArea.append("allow the ability to enable/disable algorithms\n");
    _textArea.append("do slice height settings here\n");
  }

  void doneButton_actionPerformed(ActionEvent e)
  {
    _debugPanel.backToMainScreen();
  }

  void tuneThresholdsButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("algorithm tuner goes here\n");
    _textArea.append("includes review defect functionality\n");
    _textArea.append("includes review measurements functionality\n");
    _textArea.append("\n");
    _textArea.append("user may need to go back to surface map setup or modify CAD\n");
    _textArea.append("based on what they find here\n");
    _textArea.append("problems due to shading would cause user to add an\n");
    _textArea.append("algorithm family group (subtype) or change the joint/componet to\n");
    _textArea.append("not be tested\n");
    _textArea.append("user may need to change camera settings\n");
    _textArea.append("user may need to change FOV settings (not often)\n");
  }

  void modifySliceHeightsButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("modify slice heights for each algorithm here\n");
  }

  void enableDisableJointInspectionButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("allow user to set a Joint to be tested or not\n");
  }

  void importAlgSettingsButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("allow user to import Algorithm Settings (subtypes)\n");
  }

  void exportAlgSettingsButton_actionPerformed(ActionEvent e)
  {
    _textArea.setText("");
    _textArea.append("allow user to export Algorithm Settings (subtypes)\n");
  }
}
