import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import com.agilent.util.*;

public class PanelProgramSelectorDialog extends JDialog
{
  private JPanel _mainPanel = new JPanel();
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel _labelPanel = new JPanel();
  private JPanel _listPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButonPanel = new JPanel();
  private JButton _cancelButton = new JButton();
  private JButton _okButton = new JButton();
  private GridLayout gridLayout1 = new GridLayout();
  private MainGuiFrame _mainFrame;
  private String _panelName = "";
  private BorderLayout borderLayout2 = new BorderLayout();
  String[] _panelNames = new String[]{"panel program 1",
                                      "panel program 2",
                                      "panel program 3",
                                      "panel program 4",
                                      "panel program 5",
                                      "panel program 6",
                                      "panel program 7",
                                      "panel program 8",
                                      "panel program 9",
                                      "panel program 10"};
  private JScrollPane _programListScrollPane = new JScrollPane();
  private JList _panelProgramList = new JList();
  private JPanel jPanel1 = new JPanel();
  private JLabel _panelImageLabel = new JLabel();
  private JButton jButton1 = new JButton();
  private BorderLayout borderLayout3 = new BorderLayout();
  private JPanel jPanel4 = new JPanel();
  private JLabel _titleLabel = new JLabel();

  public PanelProgramSelectorDialog(MainGuiFrame mainFrame, String title, boolean modal)
  {
    super(mainFrame, title, modal);
    Assert.expect(mainFrame != null);
    _mainFrame = mainFrame;

    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public void setVisible(boolean visible)
  {
    if (visible)
    {
      //Center the window
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      Dimension frameSize = getSize();
      if (frameSize.height > screenSize.height)
        frameSize.height = screenSize.height;
      if (frameSize.width > screenSize.width)
        frameSize.width = screenSize.width;
      setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
    }

    super.setVisible(visible);
  }

  private void jbInit() throws Exception
  {
    _panelName = _panelNames[0];

    _mainPanel.setLayout(borderLayout1);
    _cancelButton.setText("Cancel");
    _okButton.setText("OK");
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    _innerButonPanel.setLayout(gridLayout1);
    _listPanel.setLayout(borderLayout2);
    _panelProgramList.setSelectedIndex(0);
    _panelProgramList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _programListScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    _panelImageLabel.setIcon(new ImageIcon(PanelProgramSelectorDialog.class.getResource("panel1.jpg")));
    jButton1.setText("New Program");
    _labelPanel.setLayout(borderLayout3);
    _titleLabel.setText("Please Select Which Panel Program To Use");
    getContentPane().add(_mainPanel);
    _mainPanel.add(_labelPanel, BorderLayout.NORTH);
    _labelPanel.add(jPanel4, BorderLayout.NORTH);
    jPanel4.add(_titleLabel, null);
    _mainPanel.add(_listPanel, BorderLayout.CENTER);
    _listPanel.add(_programListScrollPane, BorderLayout.WEST);
    _listPanel.add(jPanel1, BorderLayout.CENTER);
    jPanel1.add(_panelImageLabel, null);
    _programListScrollPane.getViewport().add(_panelProgramList, null);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
    _buttonPanel.add(_innerButonPanel, null);
    _innerButonPanel.add(_okButton, null);
    _innerButonPanel.add(_cancelButton, null);
    _panelProgramList.setListData(_panelNames);
  }

  void okButton_actionPerformed(ActionEvent e)
  {
    if (_panelProgramList.isSelectionEmpty() == false)
      _panelName = (String)_panelProgramList.getSelectedValue();

    _mainFrame.setCurrentPanel(_panelName);
    dispose();
    _panelName = "";
  }
}
