import java.awt.*;
import javax.swing.*;

import com.agilent.util.*;
import javax.swing.border.*;
import java.awt.event.*;

public class RunInspectionPanel extends JPanel
{
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel _labelPanel = new JPanel();
  private JLabel _mainLabel = new JLabel();
  private MainGuiFrame _mainFrame;
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();
  private JButton _doneButton = new JButton();
  private TitledBorder _panelSerialNumberTitledBorder;
  private JPanel _westPanel = new JPanel();
  private JPanel _inspectionControlButtonsPanel = new JPanel();
  private JPanel _innerInspectionControlButtonsPanel = new JPanel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private GridLayout gridLayout1 = new GridLayout();
  private JPanel _controlPanel = new JPanel();
  private JButton _playButton = new JButton();
  private JButton _stopButton = new JButton();
  private JButton _pauseButton = new JButton();
  private TitledBorder titledBorder1;
  private JPanel _serialNumberListPanel = new JPanel();
  private JTextArea _serialNumberTextArea = new JTextArea();
  private JPanel _serialNumberPanel = new JPanel();
  private JTextField _serialNumberTextField = new JTextField();
  private GridLayout gridLayout2 = new GridLayout();
  private JLabel _enterSerialNumberLabel = new JLabel();
  private BorderLayout borderLayout6 = new BorderLayout();
  private TitledBorder titledBorder2;
  private boolean _panelLoadedInMachine = false;
  private JTabbedPane _imagesTabbedPane = new JTabbedPane();
  private JPanel _xRayImagePanel = new JPanel();
  private JPanel _panelImagePanel = new JPanel();
  private JPanel _testResultsPanel = new JPanel();
  private BorderLayout borderLayout5 = new BorderLayout();
  private JTextArea _testResultsTextArea = new JTextArea();
  private GridLayout gridLayout3 = new GridLayout();
  private JPanel _innerPanelImagePanel = new JPanel();
  private JPanel _panelNamePanel = new JPanel();
  private JLabel _panelImageLabel = new JLabel();
  private JLabel _westArrayLabel = new JLabel();
  private JLabel _panelNameLabel = new JLabel();
  private JLabel _eastArrowLabel = new JLabel();
  private BorderLayout borderLayout3 = new BorderLayout();
  private FlowLayout flowLayout1 = new FlowLayout();
  private BorderLayout borderLayout7 = new BorderLayout();
  private TitledBorder _contrastTitledBorder;
  private Border border1;
  private TitledBorder _brightnessTitledBorder;
  private JPanel _warningPanel = new JPanel();
  private JLabel _warningLabel = new JLabel();

  public RunInspectionPanel(MainGuiFrame mainFrame)
  {
    Assert.expect(mainFrame != null);
    _mainFrame = mainFrame;

    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    _panelSerialNumberTitledBorder = new TitledBorder(new EtchedBorder(EtchedBorder.RAISED,Color.white,new Color(142, 142, 142)),"Enter Serial Number");
    titledBorder1 = new TitledBorder(new EtchedBorder(EtchedBorder.RAISED,Color.white,new Color(142, 142, 142)),"Test Results");
    titledBorder2 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),2),"Panels that will be Inspected");
    _contrastTitledBorder = new TitledBorder(new EtchedBorder(EtchedBorder.RAISED,Color.white,new Color(142, 142, 142)),"Percent Contrast");
    border1 = BorderFactory.createEmptyBorder();
    _brightnessTitledBorder = new TitledBorder(new EtchedBorder(EtchedBorder.RAISED,Color.white,new Color(142, 142, 142)),"Percent Brightness");
    this.setLayout(borderLayout1);
    _mainLabel.setText("Panel Inspection");
    _doneButton.setText("Done");
    _doneButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doneButton_actionPerformed(e);
      }
    });
    _westPanel.setLayout(gridLayout3);
    _innerInspectionControlButtonsPanel.setLayout(gridLayout1);
    gridLayout1.setColumns(3);
    _controlPanel.setLayout(borderLayout2);
    _controlPanel.setBorder(_panelSerialNumberTitledBorder);
    _playButton.setIcon(new ImageIcon(RunInspectionPanel.class.getResource("play.gif")));
    _playButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        playButton_actionPerformed(e);
      }
    });
    _stopButton.setIcon(new ImageIcon(RunInspectionPanel.class.getResource("stop.gif")));
    _pauseButton.setIcon(new ImageIcon(RunInspectionPanel.class.getResource("pause.gif")));
    this.setMinimumSize(new Dimension(520, 500));
    _serialNumberListPanel.setLayout(gridLayout2);
    _serialNumberTextArea.setBorder(null);
    _serialNumberTextField.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        serialNumberTextField_actionPerformed(e);
      }
    });
    _serialNumberPanel.setLayout(borderLayout6);
    _enterSerialNumberLabel.setText("Serial Number");
    _panelImagePanel.setLayout(flowLayout1);
    _testResultsPanel.setLayout(borderLayout5);
    _testResultsPanel.setBorder(titledBorder1);
    _testResultsPanel.setMaximumSize(new Dimension(50, 50));
    _testResultsTextArea.setBorder(BorderFactory.createEtchedBorder());
    _testResultsTextArea.setEditable(false);
    gridLayout3.setRows(2);
    gridLayout3.setColumns(1);
    _panelImageLabel.setIcon(new ImageIcon(RunInspectionPanel.class.getResource("panel1.jpg")));
    _westArrayLabel.setIcon(new ImageIcon(RunInspectionPanel.class.getResource("upArrow.gif")));
    _panelNameLabel.setText("Panel Name");
    _eastArrowLabel.setIcon(new ImageIcon(RunInspectionPanel.class.getResource("upArrow.gif")));
    _innerPanelImagePanel.setLayout(borderLayout3);
    _xRayImagePanel.setLayout(borderLayout7);
    _warningLabel.setToolTipText("");
    _warningLabel.setText("Warning: Displaying this Image can slow down test");
    this.add(_labelPanel, BorderLayout.NORTH);
    _labelPanel.add(_mainLabel, null);
    this.add(_buttonPanel, BorderLayout.SOUTH);
    _buttonPanel.add(_innerButtonPanel, null);
    _innerButtonPanel.add(_doneButton, null);
    this.add(_westPanel, BorderLayout.WEST);
    _westPanel.add(_controlPanel, null);
    _controlPanel.add(_inspectionControlButtonsPanel, BorderLayout.SOUTH);
    _inspectionControlButtonsPanel.add(_innerInspectionControlButtonsPanel, null);
    _innerInspectionControlButtonsPanel.add(_playButton, null);
    _innerInspectionControlButtonsPanel.add(_pauseButton, null);
    _innerInspectionControlButtonsPanel.add(_stopButton, null);
    _controlPanel.add(_serialNumberListPanel, BorderLayout.CENTER);
    _serialNumberListPanel.add(_serialNumberTextArea, null);
    _controlPanel.add(_serialNumberPanel, BorderLayout.NORTH);
    _serialNumberPanel.add(_serialNumberTextField, BorderLayout.CENTER);
    _serialNumberPanel.add(_enterSerialNumberLabel, BorderLayout.WEST);
    _westPanel.add(_testResultsPanel, null);
    _testResultsPanel.add(_testResultsTextArea, BorderLayout.CENTER);
    this.add(_imagesTabbedPane, BorderLayout.CENTER);
    _imagesTabbedPane.add(_panelImagePanel, "Panel");
    _panelImagePanel.add(_innerPanelImagePanel, null);
    _innerPanelImagePanel.add(_panelNamePanel, BorderLayout.NORTH);
    _panelNamePanel.add(_panelNameLabel, null);
    _innerPanelImagePanel.add(_panelImageLabel, BorderLayout.CENTER);
    _innerPanelImagePanel.add(_westArrayLabel, BorderLayout.WEST);
    _innerPanelImagePanel.add(_eastArrowLabel, BorderLayout.EAST);
    _imagesTabbedPane.add(_xRayImagePanel, "X-ray Images");
    _xRayImagePanel.add(_warningPanel, BorderLayout.NORTH);
    _warningPanel.add(_warningLabel, null);
  }

  void doneButton_actionPerformed(ActionEvent e)
  {
    _mainFrame.backToMainMenu();
  }

  void serialNumberTextField_actionPerformed(ActionEvent e)
  {
    if (_serialNumberTextField.getText().equalsIgnoreCase("none"))
      _mainFrame.loadPanelProgram();

    runInspection();
  }

  void playButton_actionPerformed(ActionEvent e)
  {
    runInspection();
  }

  private void runInspection()
  {
    if (_panelLoadedInMachine == false)
    {
      _panelLoadedInMachine = true;
      Object[] options = {"Cancel"};
      JOptionPane.showOptionDialog(this,
                                   "Waiting for the panel to be loaded into the machine",
                                   "Wait for Panel Load",
                                   JOptionPane.CANCEL_OPTION,
                                   JOptionPane.INFORMATION_MESSAGE,
                                   null,
                                   options,
                                   options[0]);
    }
    _serialNumberTextArea.append(_serialNumberTextField.getText() + "\n");
    _serialNumberTextField.setText("");
    _testResultsTextArea.append("Test Results for panel Panel Name\n");
    _testResultsTextArea.append("U1-15 failed\n");
    _testResultsTextArea.append("U2-1 failed\n");
    _testResultsTextArea.append("C2-1 failed\n");
    _testResultsTextArea.append("NOTE: The failures will be marked in red on the panel image!\n");
    _testResultsTextArea.append("\n");
  }
  void _serialNumberTextField_actionPerformed(ActionEvent e)
  {

  }
}
