import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class FailureTypeDialog extends JDialog
{
  private JPanel _mainPanel = new JPanel();
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();
  private JButton _cancelButton = new JButton();
  private JButton _okButton = new JButton();
  private JPanel _labelPanel = new JPanel();
  private JLabel _mainLabel = new JLabel();
  private JPanel _centerPanel = new JPanel();
  private JPanel jPanel1 = new JPanel();
  private JCheckBox _excessiveSolderCheckBox = new JCheckBox();
  private JCheckBox jCheckBox3 = new JCheckBox();
  private JCheckBox jCheckBox2 = new JCheckBox();
  private JCheckBox jCheckBox1 = new JCheckBox();
  private GridLayout gridLayout1 = new GridLayout();

  public FailureTypeDialog(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public FailureTypeDialog()
  {
    this(null, "", false);
  }

  public void setVisible(boolean visible)
  {
    if (visible)
    {
      //Center the window
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      Dimension frameSize = getSize();
      if (frameSize.height > screenSize.height)
        frameSize.height = screenSize.height;
      if (frameSize.width > screenSize.width)
        frameSize.width = screenSize.width;
      setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
    }

    super.setVisible(visible);
  }

  void jbInit() throws Exception
  {
    _mainPanel.setLayout(borderLayout1);
    _cancelButton.setText("Cancel");
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _okButton.setText("OK");
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    _mainLabel.setText("Which failure types do you want to check for?");
    _excessiveSolderCheckBox.setText("Excessive Solder");
    jCheckBox3.setText("jCheckBox3");
    jCheckBox2.setText("jCheckBox2");
    jCheckBox1.setText("jCheckBox1");
    jPanel1.setLayout(gridLayout1);
    gridLayout1.setRows(5);
    gridLayout1.setColumns(1);
    getContentPane().add(_mainPanel);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
    _buttonPanel.add(_innerButtonPanel, null);
    _innerButtonPanel.add(_okButton, null);
    _innerButtonPanel.add(_cancelButton, null);
    _mainPanel.add(_labelPanel, BorderLayout.NORTH);
    _labelPanel.add(_mainLabel, null);
    _mainPanel.add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.add(jPanel1, null);
    jPanel1.add(_excessiveSolderCheckBox, null);
    jPanel1.add(jCheckBox3, null);
    jPanel1.add(jCheckBox2, null);
    jPanel1.add(jCheckBox1, null);
  }

  void okButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  void cancelButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }
}
