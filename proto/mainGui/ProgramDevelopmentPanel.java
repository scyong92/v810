import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.border.*;

import com.agilent.util.*;
import com.agilent.guiUtil.*;

public class ProgramDevelopmentPanel extends JPanel
{
  private BorderLayout borderLayout1 = new BorderLayout();
  private MainGuiFrame _mainFrame = null;
  private ImageIcon _panelIcon;
  private TitledBorder titledBorder1;
  private boolean _pullerMessageDisplayed = false;
  private JPanel _mainPanel = new JPanel();
  private BorderLayout borderLayout3 = new BorderLayout();
  private JButton _doneButton = new JButton();
  private JButton _cadEntryButton = new JButton();
  private JPanel _centerPanel = new JPanel();
  private JPanel jPanel1 = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();
  private JButton _debugButton = new JButton();
  private JLabel _panelImageLabel = new JLabel();
  private JButton _programGenerationButton = new JButton();
  private JLabel _totalJointsLabel = new JLabel();
  private JPanel _coverageReportPanel = new JPanel();
  private JLabel _jointsInspectedLabel = new JLabel();
  private JPanel _labelPanel = new JPanel();
  private GridLayout gridLayout2 = new GridLayout();
  private GridLayout gridLayout1 = new GridLayout();
  private JButton _saveProgramButton = new JButton();
  private JLabel _percentCoverageLabel = new JLabel();
  private JLabel jLabel1 = new JLabel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private JButton _serialNumberToPanelButton = new JButton();
  private JPanel _currentPanel = _mainPanel;
  private JPanel _cadEntryPanel;
  private JPanel _debugPanel;
  private JButton _programSetupButton = new JButton();
  private JTextArea _textArea = new JTextArea();

  public ProgramDevelopmentPanel(MainGuiFrame mainFrame)
  {
    Assert.expect(mainFrame != null);
    _mainFrame = mainFrame;

    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception
  {
    _cadEntryPanel = new CadEntryPanel(this);
    _debugPanel = new DebugPanel(this);

    titledBorder1 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),2),"Coverage Data");
    setLayout(borderLayout1);
    _panelIcon = new ImageIcon(MainGuiFrame.class.getResource("panel1.jpg"));


    _mainPanel.setLayout(borderLayout3);
    _doneButton.setText("Done");
    _doneButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        backToMainButton_actionPerformed(e);
      }
    });
    _cadEntryButton.setText("CAD Entry");
    _cadEntryButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cadEntryButton_actionPerformed(e);
      }
    });
    jPanel1.setLayout(borderLayout2);
    _innerButtonPanel.setLayout(gridLayout1);
    _debugButton.setToolTipText("");
    _debugButton.setText("Debug");
    _debugButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        debugButton_actionPerformed(e);
      }
    });
    _panelImageLabel.setIcon(_panelIcon);
    _programGenerationButton.setText("Generate Program");
    _programGenerationButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        programGenerationButton_actionPerformed(e);
      }
    });
    _totalJointsLabel.setText("0 total joints");
    _coverageReportPanel.setLayout(gridLayout2);
    _coverageReportPanel.setBorder(titledBorder1);
    _jointsInspectedLabel.setText("0 joints will be inspected");
    gridLayout2.setRows(4);
    gridLayout2.setColumns(1);
    gridLayout1.setRows(8);
    gridLayout1.setColumns(1);
    _saveProgramButton.setText("Save Program");
    _saveProgramButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveProgramButton_actionPerformed(e);
      }
    });
    _percentCoverageLabel.setText("0.0% joints covered by X-ray inspection");
    jLabel1.setText("Program Development");
    _serialNumberToPanelButton.setText("Serial Number To Panel Program Mapping");
    _programSetupButton.setText("Program Setup");
    _programSetupButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        programSetupButton_actionPerformed(e);
      }
    });
    this.add(_mainPanel, BorderLayout.NORTH);
    _coverageReportPanel.add(_totalJointsLabel, null);
    _coverageReportPanel.add(_jointsInspectedLabel, null);
    _coverageReportPanel.add(_percentCoverageLabel, null);
    jPanel1.add(_panelImageLabel, BorderLayout.CENTER);
    _mainPanel.add(_buttonPanel, BorderLayout.WEST);
    _buttonPanel.add(_innerButtonPanel, null);
    jPanel1.add(_coverageReportPanel, BorderLayout.NORTH);
    _innerButtonPanel.add(_cadEntryButton, null);
    _innerButtonPanel.add(_programSetupButton, null);
    _innerButtonPanel.add(_programGenerationButton, null);
    _innerButtonPanel.add(_debugButton, null);
    _innerButtonPanel.add(_serialNumberToPanelButton, null);
    _innerButtonPanel.add(_saveProgramButton, null);
    _innerButtonPanel.add(_doneButton, null);
    _mainPanel.add(_labelPanel,  BorderLayout.NORTH);
    _labelPanel.add(jLabel1, null);
    _mainPanel.add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.add(jPanel1, null);
  }

  void backToMainButton_actionPerformed(ActionEvent e)
  {
    _mainFrame.backToMainMenu();
  }

  private void programGenerationButton_actionPerformed(ActionEvent e)
  {
//    JDialog failureTypeDialog = new FailureTypeDialog(_mainFrame,
//                                                      "Failure Type",
//                                                      true);
//    failureTypeDialog.setVisible(true);

    JOptionPane.showMessageDialog(this,
                                  "Please place a known good panel in the machine so a program can be generated",
                                  "Waiting for Panel Load",
                                  JOptionPane.INFORMATION_MESSAGE);
    JOptionPane.showMessageDialog(this,
                                  "Generating the panel program.  Please wait...",
                                  "Panel Program Generation",
                                  JOptionPane.INFORMATION_MESSAGE);

    _totalJointsLabel.setText("14301 total joints");
    _jointsInspectedLabel.setText("11012 joints will be inspected");
    _percentCoverageLabel.setText("89.4% joints covered by X-ray inspection");
  }


  private void switchMainPanel(JPanel panel)
  {
    Assert.expect(panel != null);

    remove(_currentPanel);
    _currentPanel = panel;
    SwingUtils.setFont(_currentPanel, getFont());
    add(_currentPanel, BorderLayout.CENTER);
    revalidate();
    repaint();
  }

  private void cadEntryButton_actionPerformed(ActionEvent e)
  {
    switchMainPanel(_cadEntryPanel);
  }

  private void saveProgramButton_actionPerformed(ActionEvent e)
  {
    if (_pullerMessageDisplayed == false)
    {
      _pullerMessageDisplayed = true;
      String message = "An older version of the panel program is on your panel program server.\n" +
                       "Would you like to copy this local program to the panel programl server?";
      JOptionPane.showConfirmDialog(this, message, "Panel Puller", JOptionPane.YES_NO_CANCEL_OPTION);
    }
  }


  private void testJointsButton_actionPerformed(ActionEvent e)
  {
    String message = "Out of 14,391 joints, 11,000 will be inspected.\n" +
                     "For a total coverage of 76.4%";
    JOptionPane.showMessageDialog(this, message, "Test Coverage Report", JOptionPane.INFORMATION_MESSAGE);
  }

  private void debugButton_actionPerformed(ActionEvent e)
  {
    switchMainPanel(_debugPanel);
  }

  void backToMainScreen()
  {
    switchMainPanel(_mainPanel);
  }

  void programSetupButton_actionPerformed(ActionEvent e)
  {
    String message = "Things like target machine, retest failing pins, aware test data enabled\n" +
                     "surface map point density, would be entered here";
    JOptionPane.showMessageDialog(this, message, "Info", JOptionPane.INFORMATION_MESSAGE);

    JDialog speedOrAccuracyDialog = new SpeedOrAccuracyDialog(_mainFrame,
                                                              "Speed or Accuracy",
                                                              true);
    speedOrAccuracyDialog.setVisible(true);
  }
}
