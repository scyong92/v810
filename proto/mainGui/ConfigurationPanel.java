import java.awt.*;

import com.agilent.util.*;
import javax.swing.*;
import java.awt.event.*;

public class ConfigurationPanel extends JPanel
{
  private BorderLayout borderLayout1 = new BorderLayout();
  private MainGuiFrame _mainFrame;
  private JPanel _lablePanel = new JPanel();
  private JLabel _configLabel = new JLabel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();
  private JButton _doneButton = new JButton();
  private GridLayout gridLayout1 = new GridLayout();
  private JTabbedPane _configurationTabbedPane = new JTabbedPane();
  private JPanel _programServerPanel = new JPanel();
  private JPanel _testerConfigPanel = new JPanel();
  private JPanel jPanel1 = new JPanel();
  private GridLayout gridLayout3 = new GridLayout();
  private FlowLayout flowLayout1 = new FlowLayout();
  private JPanel _useServerPanel = new JPanel();
  private JCheckBox _useServerCheckBox = new JCheckBox();
  private JPanel _pathPanel = new JPanel();
  private JLabel _pathLabel = new JLabel();
  private JTextField _pathTextField = new JTextField();
  private BorderLayout borderLayout3 = new BorderLayout();
  private JPanel _radioButtonPanel = new JPanel();
  private JRadioButton _rightEntryRadioButton = new JRadioButton();
  private JRadioButton _leftEntryRadioButton = new JRadioButton();
  private GridLayout gridLayout2 = new GridLayout();
  private ButtonGroup _entryButtonGroup = new ButtonGroup();
  private ButtonGroup _exitButtonGroup = new ButtonGroup();
  private JLabel _entryLabel = new JLabel();
  private JLabel _exitLabel = new JLabel();
  private JRadioButton _rightExitRadioButton = new JRadioButton();
  private JRadioButton _leftExitRadioButton = new JRadioButton();
  private JPanel _barCodeReaderConfigPanel = new JPanel();
  private JPanel _limitedAccessPanel = new JPanel();
  private JCheckBox _serialNumberOnlyCheckBox = new JCheckBox();

  public ConfigurationPanel(MainGuiFrame mainFrame)
  {
    Assert.expect(mainFrame != null);
    _mainFrame = mainFrame;

    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    _configLabel.setText("Xray Tester Configuration");
    _doneButton.setText("Done");
    _doneButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doneButton_actionPerformed(e);
      }
    });
    _innerButtonPanel.setLayout(gridLayout1);
    _programServerPanel.setLayout(flowLayout1);
    jPanel1.setLayout(gridLayout3);
    gridLayout3.setRows(2);
    gridLayout3.setColumns(1);
    _useServerCheckBox.setText("Use a Central Server to Store And Retrieve Panel Programs");
    _useServerCheckBox.setActionCommand("Use Central Server to Store And Retrieve Panel Programs");
    _useServerCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        useServerCheckBox_actionPerformed(e);
      }
    });
    _pathPanel.setLayout(borderLayout3);
    _pathLabel.setText("Path: ");
    _pathTextField.setEnabled(false);
    _pathTextField.setText("z:\\programServer\\programs");
    _rightEntryRadioButton.setText("Right");
    _leftEntryRadioButton.setSelected(true);
    _leftEntryRadioButton.setText("Left");
    _radioButtonPanel.setLayout(gridLayout2);
    gridLayout2.setRows(7);
    gridLayout2.setColumns(1);
    _entryLabel.setText("Panels will be loaded from the");
    _exitLabel.setText("Panels will exit the tester to the");
    _rightExitRadioButton.setText("Right");
    _leftExitRadioButton.setSelected(true);
    _leftExitRadioButton.setText("Left");
    _serialNumberOnlyCheckBox.setText("Allow Panel Programs to be Selected by Board ID only");
    this.add(_lablePanel, BorderLayout.NORTH);
    _lablePanel.add(_configLabel, null);
    this.add(_buttonPanel, BorderLayout.SOUTH);
    _buttonPanel.add(_innerButtonPanel, null);
    _innerButtonPanel.add(_doneButton, null);
    this.add(_configurationTabbedPane, BorderLayout.CENTER);
    _configurationTabbedPane.add(_programServerPanel, "Program Server");
    _programServerPanel.add(jPanel1, null);
    jPanel1.add(_useServerPanel, null);
    _useServerPanel.add(_useServerCheckBox, null);
    jPanel1.add(_pathPanel, null);
    _pathPanel.add(_pathLabel, BorderLayout.WEST);
    _pathPanel.add(_pathTextField, BorderLayout.CENTER);
    _configurationTabbedPane.add(_testerConfigPanel, "Panel Handling");
    _testerConfigPanel.add(_radioButtonPanel, null);
    _radioButtonPanel.add(_entryLabel, null);
    _radioButtonPanel.add(_leftEntryRadioButton, null);
    _radioButtonPanel.add(_rightEntryRadioButton, null);
    _radioButtonPanel.add(_exitLabel, null);
    _radioButtonPanel.add(_leftExitRadioButton, null);
    _radioButtonPanel.add(_rightExitRadioButton, null);
    _configurationTabbedPane.add(_barCodeReaderConfigPanel, "Bar Code Reader");
    _configurationTabbedPane.add(_limitedAccessPanel, "Limited Access");
    _limitedAccessPanel.add(_serialNumberOnlyCheckBox, null);
    _entryButtonGroup.add(_rightEntryRadioButton);
    _entryButtonGroup.add(_leftEntryRadioButton);
    _exitButtonGroup.add(_leftExitRadioButton);
    _exitButtonGroup.add(_rightExitRadioButton);
  }

  void doneButton_actionPerformed(ActionEvent e)
  {
    _mainFrame.backToMainMenu();
  }

  void useServerCheckBox_actionPerformed(ActionEvent e)
  {
    _pathTextField.setEnabled(_useServerCheckBox.isSelected());
  }
}
