package com.agilent.xRayTest.hardware;

import com.agilent.xRayTest.datastore.hwConfig.*;
import com.agilent.util.*;

/**
 * This class provides an interface to get and set line scan camera acquisition properties
 *
 * This class should not be created directly.  Instead, the user of this class should
 * call "LineScanCameras.getImageAcquisitionProperties()".
 *
 * Note that some properties cannot be set because they are defined as part of the 5DX
 * hardware configuration such as "cameraType".  The client cannot redefine this property, and
 * as a result it is read-only.
 *
 * @author Greg Esparza
 */
public class LineScanCameraAcquisitionProperties
{
  private static final int _minimumImageLengthInPixels = 1;
  private static final int _minimumNumberOfImageTransferBuffers = 2;

  private static LineScanCameraAcquisitionProperties _instance = null;
  private HardwareConfigEnum _hardwareConfigEnum;
  private LineScanCameraTypeEnum _cameraType;
  private LineScanCameraPairInstalledEnum _cameraPairInstalled;
  private LineScanCameraResolutionEnum _imageResolution;
  private Integer _cameraResolution;
  private Integer _minBrightImageGrayLevel;
  private Integer _maxDarkImageGrayLevel;
  private int _numberOfImageTransferBuffers;
  private int _imageWidthInPixels;
  private int _imageLengthInPixels;
  private boolean _topFrontCameraHorizontalImageFlipEnable;
  private boolean _topRearCameraHorizontalImageFlipEnable;
  private boolean _bottomFrontCameraHorizontalImageFlipEnable;
  private boolean _bottomRearCameraHorizontalImageFlipEnable;


  private LineScanCameraAcquisitionProperties()
  {
    //_cameraType                                  = LineScanCameraTypeEnum.getEnum((String)HardwareConfigEnum.LINE_SCAN_CAMERA_TYPE.getValue());
    //_cameraPairInstalled                         = LineScanCameraPairInstalledEnum.getEnum((String)HardwareConfigEnum.LINE_SCAN_CAMERA_PAIR_INSTALLED.getValue());
    //_cameraResolution                            = (Integer)HardwareConfigEnum.LINE_SCAN_CAMERA_RESOLUTION.getValue();
    _imageResolution                             = LineScanCameraResolutionEnum.PIXELS_1x2048;
    _numberOfImageTransferBuffers                = _minimumNumberOfImageTransferBuffers;
    //_imageWidthInPixels                          = _cameraResolution.intValue();
    _imageLengthInPixels                         = _minimumImageLengthInPixels;
    //_minBrightImageGrayLevel                     = (Integer)HardwareConfigEnum.LINE_SCAN_CAMERA_MIN_BRIGHT_IMAGE_GRAY_LEVEL.getValue();
    //_maxDarkImageGrayLevel                       = (Integer)HardwareConfigEnum.LINE_SCAN_CAMERA_MAX_DARK_IMAGE_GRAY_LEVEL.getValue();
    _topFrontCameraHorizontalImageFlipEnable     = false;
    _topRearCameraHorizontalImageFlipEnable      = false;
    _bottomFrontCameraHorizontalImageFlipEnable  = false;
    _bottomRearCameraHorizontalImageFlipEnable   = false;
  }

  /**
   * Get the only and only instance of the top line scan camera pair
   *
   * Typically, the Agt5dx object will create all the hardware objects including
   * the TOP and BOTTOM line scan camera pair.  The client will then call
   * AGT5dx.getTopLineScanCameraPair() and AGT5dx.getBottomLineScanCameraPair().
   *
   * @return The one and only instance of top line scan camera pair class
   * @author Greg Esparza
   */
  public static synchronized LineScanCameraAcquisitionProperties getInstance()
  {
    if (_instance == null)
      _instance = new LineScanCameraAcquisitionProperties();

    return _instance;
  }

  /**
   * Get the camera type information
   *
   * @return A CameraType enumeration defining the camera type
   * @author Greg Esparza
   */
  public LineScanCameraTypeEnum getCameraType()
  {
    return _cameraType;
  }

  /**
   * Get which camera pair are installed
   *
   * @param cameraPairInstalled - A CameraPairInstalled enumeration defining which camera pair are installed
   * @author Greg Esparza
   */
  public LineScanCameraPairInstalledEnum getCameraPairInstalled()
  {
    return _cameraPairInstalled;
  }

  /**
   * Get the camera's resolution
   *
   * This function will return the number pixels defining the camera's CCD width
   *
   * @return An integer defining the CCD width in pixels
   * @author Greg Esparza
   */
  public int getCameraResolution()
  {
    return _cameraResolution.intValue();
  }

  /**
   * Get the minimum gray level value for a bright image
   *
   * @return An integer representing the minimum gray level for a bright image
   * @author Greg Esparza
   */
  public int getminBrightImageGrayLevel()
  {
    return _minBrightImageGrayLevel.intValue();
  }

  /**
   * Get the maximum gray level for a dark image
   *
   * @return An integer representing the maximum gray level for a dark image
   * @author Greg Esparza
   */
  public int getmaxDarkImageGrayLevel()
  {
    return _maxDarkImageGrayLevel.intValue();
  }

  /**
   * Set the image resolution (binning)
   *
   * @param imageResolution - An ImageResolution enumeration
   * @author Greg Esparza
   */
  public void setImageResolution(LineScanCameraResolutionEnum imageResolution)
  {
     _imageResolution = imageResolution;
  }

  /**
   * Get the image resolution (binning)
   *
   * @return An ImageResolution enumeration
   * @author Greg Esparza
   */
  public LineScanCameraResolutionEnum getImageResolution()
  {
    return _imageResolution;
  }

  /**
   * Set the number of image tranfer buffers
   *
   * The image transfer buffer will store (2) or (4) camera images and typically, we
   * will need (2) transfer buffers.  We could use more transfer buffers if physical
   * memory is not an issue.
   *
   * To ensure optimal pipeline execution, we will need to do the following:
   *
   * 1) Store the acquired (2) or (4) images in the first available transfer buffer (assume buffer #1)
   * 2) Buffer #1 can then be available for image analysis.
   * 3) While the images in buffer #1 are being analyzed, the next set of images are
   *    being acqiured and placed into transfer buffer #2.
   * 4) If buffer #1 is still in use, and we need to execute another acquisition, then
   *    the sequence will become blocked
   *
   * @param numberOfImageTransferBuffers - An integer defining the number of required transfer buffers
   * @author Greg Esparza
   */
  public void setNumberOfImageTransferBuffers(int numberOfImageTransferBuffers)
  {
    _numberOfImageTransferBuffers = numberOfImageTransferBuffers;
  }

  /**
   * Get the number of image tranfer buffers
   *
   * The image transfer buffer will store (2) or (4) camera images and typically, we
   * will need (2) transfer buffers.  We could use more transfer buffers if physical
   * memory is not an issue.
   *
   * To ensure optimal pipeline execution, we will need to do the following:
   *
   * 1) Store the acquired (2) or (4) images in the first available transfer buffer (assume buffer #1)
   * 2) Buffer #1 can then be available for image analysis.
   * 3) While the images in buffer #1 are being analyzed, the next set of images are
   *   being acqiured and placed into transfer buffer #2.
   * 4) If buffer #1 is still in use, and we need to execute another acquisition, then
   *    the sequence will become blocked
   *
   * @return An integer defining the number of required transfer buffers
   * @author Greg Esparza
   */
  public int getNumberOfImageTransferBuffers()
  {
    return _numberOfImageTransferBuffers;
  }

  /**
   * Set the image width in pixels
   *
   * This function defines the image frame width
   *
   * @param widthInPixels - An integer defining how wide the image will be in pixel dimensions
   * @author Greg Esparza
   */
  public void setImageWidthInPixels(int widthInPixels)
  {
    //Make sure that the image width never exceed the camera's physical pixel width resolution
    Assert.assert(_cameraResolution.intValue() >= widthInPixels);
    _imageWidthInPixels = widthInPixels;
  }

  /**
   * Get the image width in pixels
   *
   * This function returns the defined the image frame width
   *
   * @return An integer defining how wide the image will be in pixel dimensions
   * @author Greg Esparza
   */
  public int getImageWidthInPixels()
  {
    return _imageWidthInPixels;
  }

  /**
   * Set the image length in pixels
   *
   * This function defines the image frame length
   *
   * @param widthInPixels - A integer defining how wide the image will be in pixel dimensions
   * @author Greg Esparza
   */
  public void setImageLengthInPixels(int lengthInPixels)
  {
    //Make sure that the image length is at least (1) pixel long
    Assert.assert(lengthInPixels > 0);
    _imageLengthInPixels = lengthInPixels;
  }

  /**
   * Get the image length in pixels
   *
   * This function returns the defined the image frame length
   *
   * @return An integer defining how wide the image will be in pixel dimensions
   * @author Greg Esparza
   */
  public int getImageLengthInPixels()
  {
    return _imageLengthInPixels;
  }

  /**
   * Enable/Disable horizontal image flip for the TOP FRONT camera
   *
   * @param enable - A bool that defines whether to turn the horizontal image flip
   *                ON or OFF for the TOP FRONT camera
   * @author Greg Esparza
   */
  public void setTopFrontCameraHorizontalImageFlipEnable(boolean enable)
  {
    _topFrontCameraHorizontalImageFlipEnable = enable;
  }

  /**
   * Check if horizontal image flip for the TOP FRONT camera is enabled
   *
   * @return A bool indicating whether horizontal image flip is enabled for the
   *         TOP FRONT camera
   * @author Greg Esparza
   */
  public boolean getTopFrontCameraHorizontalImageFlipEnable()
  {
    return  _topFrontCameraHorizontalImageFlipEnable;
  }

  /**
   * Enable/Disable horizontal image flip for the TOP REAR camera
   *
   * @param enable - A bool that defines whether to turn the horizontal image flip
   *                 ON or OFF for the TOP rear camera
   * @author Greg Esparza
   */
  public void setTopRearCameraHorizontalImageFlipEnable(boolean enable)
  {
    _topRearCameraHorizontalImageFlipEnable = enable;
  }

  /**
   * Check if horizontal image flip for the TOP REAR camera is enabled
   *
   * @return A bool indicating whether horizontal image flip is enabled for the
   *         TOP REAR camera
   * @author Greg Esparza
   */
  public boolean getTopRearCameraHorizontalImageFlipEnable()
  {
    return _topRearCameraHorizontalImageFlipEnable;
  }

  /**
   * Enable/Disable horizontal image flip for the BOTTOM FRONT camera
   *
   * @param enable - A bool that defines whether to turn the horizontal image flip
   *                 ON or OFF for the BOTTOM FRONT camera
   * @author Greg Esparza
   */
  public void setBottomFrontCameraHorizontalImageFlipEnable(boolean enable)
  {
    _bottomFrontCameraHorizontalImageFlipEnable = enable;
  }

  /**
   * Check if horizontal image flip for the BOTTOM FRONT camera is enabled
   *
   * @return A bool indicating whether horizontal image flip is enabled for the
   *         BOTTOM FRONT camera
   * @author Greg Esparza
   */
  public boolean getBottomFrontCameraHorizontalImageFlipEnable()
  {
    return _bottomFrontCameraHorizontalImageFlipEnable;
  }

  /**
   * Enable/Disable horizontal image flip for the BOTTOM REAR camera
   *
   * @param enable - A bool that defines whether to turn the horizontal image flip
   *                 ON or OFF for the BOTTOM REAR camera
   * @author Greg Esparza
   */
  public void setBottomRearCameraHorizontalImageFlipEnable(boolean enable)
  {
    _bottomRearCameraHorizontalImageFlipEnable = enable;
  }

  /**
   * Check if horizontal image flip for the BOTTOM REAR camera is enabled
   *
   * @return A bool indicating whether horizontal image flip is enabled for the
   *         BOTTOM REAR camera
   * @author Greg Esparza
   */
  public boolean getBottomRearCameraHorizontalImageFlipEnable()
  {
    return _bottomRearCameraHorizontalImageFlipEnable;
  }
}