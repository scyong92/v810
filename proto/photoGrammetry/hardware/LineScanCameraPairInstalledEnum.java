package com.agilent.xRayTest.hardware;

import java.io.*;
import java.util.*;
import com.agilent.util.*;

/**
 * This class defines which pair of line scan cameras are installed.
 * The system can only have the following combinations:
 *
 * 1) top pair only
 * 2) bottom pair only (only valid for manufacturing bench testing)
 * 3) top and bottom pair
 *
 * @author Greg Esparza
 */
public class LineScanCameraPairInstalledEnum extends Enum
{
  private static Map _intToEnumMap = new HashMap();
  private static Map _stringToEnumMap = new HashMap();
  private static int _index = -1;
  public static final LineScanCameraPairInstalledEnum NONE_INSTALLED                = new LineScanCameraPairInstalledEnum(++_index, new String("none"));
  public static final LineScanCameraPairInstalledEnum TOP_PAIR_INSTALLED            = new LineScanCameraPairInstalledEnum(++_index, new String("topPair"));
  public static final LineScanCameraPairInstalledEnum TOP_AND_BOTTOM_PAIR_INSTALLED = new LineScanCameraPairInstalledEnum(++_index, new String("topAndBottomPair"));

  private LineScanCameraPairInstalledEnum(int cameraPairInstalled, String cameraPairInstalledString)
  {
    super(cameraPairInstalled);

    //Set up a map that can convert an integer value to an enumerated type
    _intToEnumMap.put(new Integer(cameraPairInstalled), this);
    //Set up a map that can convert a string value to an enumerated type
    _stringToEnumMap.put(cameraPairInstalledString, this);
  }

  /**
   * Given a string for the camera pair installed, return the corresponding enumerated type.
   *
   * @param cameraPairInstalled - A string that maps to the desired enumerated type
   * @return A LineScanCameraPairInstalledEnum that maps to the input string value
   * @author Greg Esparza
   */
  public static LineScanCameraPairInstalledEnum getEnum(String cameraPairInstalled)
  {
    LineScanCameraPairInstalledEnum enum = (LineScanCameraPairInstalledEnum)_stringToEnumMap.get(cameraPairInstalled);
    Assert.assert(enum != null);

    return enum;
  }

  /**
   * Given a legacy integer value, return the corresponding enumerated type.
   *
   * @param legacyEnumValue - An input that maps to the desired enumerated type
   * @return A LineScanCameraPairInstalledEnum that maps to the input integer value
   * @author Bill Darbie
   */
  public static LineScanCameraPairInstalledEnum getEnum(int legacyEnumValue)
  {
    LineScanCameraPairInstalledEnum enum = (LineScanCameraPairInstalledEnum)_intToEnumMap.get(new Integer(legacyEnumValue));
    Assert.assert(enum != null);

    return enum;
  }

  /**
   * Get the integer representation for the enumerated type
   *
   * @return the integer value that the legacy code uses for the camera.
   * @author Greg Esparza
   */
  public int getLegacyType()
  {
    return _id;
  }

}
