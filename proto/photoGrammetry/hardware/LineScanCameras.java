package com.agilent.xRayTest.hardware;

import com.agilent.xRayTest.hardware.nativeInt.*;
import com.agilent.util.*;

/**
 * This class provides control for line scan cameras
 *
 * The class will support both a TOP and BOTTOM pair of cameras.
 * This TOP and BOTTOM orientation references how the camera pair
 * are mounted in the 5DX system.
 *
 * The TOP pair will support top side surface mapping and the BOTTOM
 * pair will support bottom side surface mapping.
 *
 * 5DX Requirements:
 *
 * 1) The 5DX system will ALWAYS require a TOP pair to support top
 *    side surface mapping
 * 2) The 5DX system will control the BOTTOM pair as an option to
 *    support bottom side surface mapping.  Series 2L systems can be upgraded
 *    to support topside surface mapping but bottom side hardware cannot be
 *    installed.
 *
 * @author Greg Esparza
 */
public class LineScanCameras extends HardwareObject
{
  private static LineScanCameras _instance                           = null;
  private LineScanCameraAcquisitionProperties _acquisitionProperties = null;
  private NativeLineScanCameras _nativeLineScanCameras               = null;
  private LineScanController _lineScanController                     = null;

  /**
   * Constructor
   *
   * @author Greg Esparza
   */
  private LineScanCameras()
  {
    _lineScanController    = LineScanController.getInstance();
    _acquisitionProperties = LineScanCameraAcquisitionProperties.getInstance();
    _nativeLineScanCameras = NativeLineScanCameras.getInstance();
  }

  /**
   * Get the only and only instance of the line scan cameras unit
   *
   * @return The one and only instance of top line scan camera pair class
   * @author Greg Esparza
   */
  public static synchronized LineScanCameras getInstance()
  {
    if (_instance == null)
      _instance = new LineScanCameras();

    return _instance;
  }

  /**
   * Prepare the system for image acquisition
   *
   * This function will get all internal resources ready for image acquistion.
   * The function will allocate the memory blocks for all camera image data.  The client
   * should only call this method once, then call "beginImageAcquisition()" and
   * "endImageAcquisition()" as many times as needed for a given session.
   *
   * Typically,a session is defined by the image frame size (width x length in pixels).
   * However, if any of the property information has to change, then new resources must
   * be allocated and as a result, this function must be called again.
   *
   * If the property information passed in is the same as the current property settings,
   * then nothing happens (resources stay the same).  Otherwise, allocate all necessary
   * resources.
   *
   * Note that calling this function results in a slow execution due to resource allocaton
   * and should be used when its necessary to start a new acquisition session.
   *
   * @param acquisitionProperties - A LineScanCameraAcquisitionProperties object
   * @throws HardwareException
   * @author Greg Esparza
   */
  void prepareForImageAcquisition(LineScanCameraAcquisitionProperties properties) throws HardwareException
  {
    if (isSimulationModeOn() == false)
      _nativeLineScanCameras.prepareForImageAcquisition(properties);
  }

  /**
   * Clean up image acquisition resources
   *
   * This function will delete all memory allocated for the passed in images as well
   * as other resources associated with the line scan camera subsystem.
   *
   * @param topFrontImage    - A LineScanCameraImage object that was ALLOCATED or NULL for the previous acquisition
   *        topRearImage     - A LineScanCameraImage object that was ALLOCATED or NULL for the previous acquisition
   *        bottomFrontImage - A LineScanCameraImage object that was ALLOCATED or NULL for the previous acquisition
   *        bottomRearImage  - A LineScanCameraImage object that was ALLOCATED or NULL for the previous acquisition
   * @throws HardwareException
   * @author Greg Esparza
   */
  void cleanUpImageAcquisitionResources(LineScanCameraImage topFrontImage,
                                        LineScanCameraImage topRearImage,
                                        LineScanCameraImage bottomFrontImage,
                                        LineScanCameraImage bottomRearImage) throws HardwareException
  {
    if (isSimulationModeOn() == false)
    {
      _nativeLineScanCameras.cleanUpImageAcquisitionResources(topFrontImage,
                                                              topRearImage,
                                                              bottomFrontImage,
                                                              bottomRearImage);
    }
  }

  /**
   * Get the current image acquisition properties
   *
   * @return A LineScanCameraAcquisitionProperties object
   * @author Greg Esparza
   */
  LineScanCameraAcquisitionProperties getImageAcquisitionProperties()
  {
    if (isSimulationModeOn() == false)
    {
      //Pass in this object's properties to get an update from the native side.
      //Note that some of the property information cannot get modified by the
      //native side because the property information is defined as part of the
      //5DX configuration info such as "cameraType".
      _nativeLineScanCameras.getImageAcquisitionProperties(_acquisitionProperties);
    }
    else
    {
      if (UnitTest.unitTesting() == false)
        System.out.println("LineScanCameras.getImageAcquisitionProperties stub");
    }

    //wpdfix - do not return a private member outside of this class - breaks encapsulation
    return _acquisitionProperties;
  }

  /**
   * Notify the line scan camera pair to start aquiring the image
   *
   * This function will get the line scan camera control to start acquiring images.
   *
   * @throws HardwareException
   * @author Greg Esparza
   */
  void beginImageAcquisition() throws HardwareException
  {
    if (isSimulationModeOn() == false)
      _nativeLineScanCameras.beginImageAcquisition();
  }

  /**
   * End the image acquisition and get the images
   *
   * This function requires NULL or LineScanCameraImage objects. The object parameters that are instatiated
   * will receive the image data.
   *
   * @param topFrontImage    - A LineScanCameraImage object that will receive the image data
   *        topRearImage     - A LineScanCameraImage object that will receive the image data
   *        bottomFrontImage - A LineScanCameraImage object that will receive the image data
   *        bottomRearImage  - A LineScanCameraImage object that will receive the image data
   * @throws HardwareException
   * @author Greg Esparza
   */
  void endImageAcquisition(LineScanCameraImage topFrontImage,
                           LineScanCameraImage topRearImage,
                           LineScanCameraImage bottomFrontImage,
                           LineScanCameraImage bottomRearImage) throws HardwareException
  {
    if (isSimulationModeOn() == false)
      _nativeLineScanCameras.endImageAcquisition(topFrontImage, topRearImage, bottomFrontImage, bottomRearImage);
  }

  /**
   * Notify the line scan camera pair to abort all image acquisition
   *
   * This function will immediatlely stop acquiring image data.  Also, the function
   * must then call "reset()" to get all resources back to a known state.  Note that
   * all previous image data will be deleted.
   *
   * @throws HardwareException
   * @author Greg Esparza
   */
  void abortImageAcquisition() throws HardwareException
  {
    if (isSimulationModeOn() == false)
      _nativeLineScanCameras.abortImageAcquisition();
  }

  /**
   * Command the line scan camera to snap an image
   *
   * This function will be used only for manual and diagnostic operation and will
   * only acquire ONE line image.  The client can use this function by first
   * making a call into "beginImageAcquisition()" which requires a defined image size.
   * The client can then make multiple calls to this function which will generate
   * the image data size.  Finally, the client can then call "endImageAcquistion()" and
   * then "getImages()".
   *
   * This function will command the front and rear line scan cameras to snap an image.
   *
   * @throws HardwareException
   * @author Greg Esparza
   */
  void snap() throws HardwareException
  {
    if (isSimulationModeOn() == false)
    {
      //_lineScanController.snap();
    }
  }

  /**
   * Set the camera brightness (gain) level
   *
   * This function will only be used for manual or diagnostic purposes.
   *
   * @param brightnessLevel - An input defining the brightness (gain) level
   * @throws HardwareException
   * @author Greg Esparza
   */
  void setBrightnessLevel(LineScanCameraBrightnessLevelEnum brightnessLevel) throws HardwareException
  {
    _lineScanController.setCameraBrightnessLevel(LineScanCameraEnum.TOP_FRONT, brightnessLevel);
    _lineScanController.setCameraBrightnessLevel(LineScanCameraEnum.TOP_REAR, brightnessLevel);
    _lineScanController.setCameraBrightnessLevel(LineScanCameraEnum.BOTTOM_FRONT, brightnessLevel);
    _lineScanController.setCameraBrightnessLevel(LineScanCameraEnum.BOTTOM_REAR, brightnessLevel);
  }

  /**
   * Get the camera brightness (gain) level
   *
   * @return A LineScanCameraBrightnessLevelEnum enumerated type representing the brightness (gain) value.
   * @author Greg Esparza
   */
  LineScanCameraBrightnessLevelEnum getBrightnessLevel() throws HardwareException
  {
    return _lineScanController.getCameraBrightnessLevel(LineScanCameraEnum.TOP_FRONT);
  }

  /**
   * Set the camera pair resolution (binning)
   *
   * The line scan camera can only have two modes of resolution (binning)
   * 1) 1 pixel long by 2048 pixels wide (No binning (disabled))
   * 2) 1 pixel long by 1024 pixels wide (1x2 binning (enabled))
   *
   * @throws HardwareException
   * @author Greg Esparza
   */
  void setImageResolution(LineScanCameraResolutionEnum resolution) throws HardwareException
  {
    _lineScanController.setCameraImageResolution(LineScanCameraEnum.TOP_FRONT, resolution);
    _lineScanController.setCameraImageResolution(LineScanCameraEnum.TOP_REAR, resolution);
    _lineScanController.setCameraImageResolution(LineScanCameraEnum.BOTTOM_FRONT, resolution);
    _lineScanController.setCameraImageResolution(LineScanCameraEnum.BOTTOM_REAR, resolution);
  }

  /**
   * Get the camera pair resolution (binning)
   *
   * @return A LineScanCameraBinningModeEnum indicating whether binning is 1x2 or disabled
   * @author Greg Esparza
   */
  LineScanCameraResolutionEnum getImageResolution() throws HardwareException
  {
    return _lineScanController.getCameraImageResolution(LineScanCameraEnum.TOP_FRONT);
  }

  /**
   * Enable control to flip the image vertically
   *
   *
   *
   *  Y-axis (+)
   *
   *    ^                                                                                ^
   *    |   Rear Line-scan Cameras               Front Line-scan Cameras                 |
   *    |                                                                                |
   *    |        Top or Bottom                          Top or Bottom
   *    |                                                                        The width of the
   *    |        ___________                                    ___________      image will be
   *    |        |         |___ Pixel 1          Pixel 2048  ___|         |      parallel to the
   *    |        |         |   |                            |   |         |      machine Y-axis.
   *    |        |         |   |                            |   |         |
   *    |        |         |---|Pixel 2048          Pixel 1 |---|         |      This axis represents
   *    |        -----------                                    -----------      horizontal orientation
   *    |                                                                        of the image.
   *    |
   *    |                                                                                |
   *    |        <------------ Image strip will parallel to the X-axis ------------->    V
   *    |
   *    |             This axis represents the vertical orientation of the image
   *    |
   *    |
   *    +-------------------> X-axis (+)
   *  (0,0,0)
   *
   *
   *
   *                                    TOP VIEW
   *
   *
   * @param topFrontCameraEnable    - An input to enable vertical image flip for the TOP FRONT camera's image
   *        topRearCameraEnable     - An input to enable vertical image flip for the TOP REAR camera's image
   *        bottomFrontCameraEnable - An input to enable vertical image flip for the BOTTOM FRONT camera's image
   *        bottomRearCameraEnable  - An input to enable vertical image flip for the BOTTOM REAR camera's image
   * @throws HardwareException
   * @author Greg Esparza
   */
  void setVerticalImageFlipEnable(boolean topFrontCameraEnable,
                                  boolean topRearCameraEnable,
                                  boolean bottomFrontCameraEnable,
                                  boolean bottomRearCameraEnable) throws HardwareException
  {
    if (isSimulationModeOn() == false)
    {
      _nativeLineScanCameras.setVerticalImageFlipEnable(topFrontCameraEnable,
                                                        topRearCameraEnable,
                                                        bottomFrontCameraEnable,
                                                        bottomRearCameraEnable);
    }
  }

  /**
   * Set the flat field reference images
   *
   * This function will set DARK and BRIGHT images into the native line scan camera/frame grabber controller
   * Once the images are set, the client can the enable or disable flat fielding.
   *
   * @param darkImage - A LineScanCameraImage object
   *        brightImage - A LineScanCameraImage object
   * @throws HardwareException
   * @author Greg Esparza
   */
  public void setFlatFieldReferenceImages(LineScanCameraImage darkImage, LineScanCameraImage brightImage) throws HardwareException
  {
    if (isSimulationModeOn() == false)
      _nativeLineScanCameras.setFlatFieldReferenceImages(darkImage, brightImage);
  }

  /**
   * Enable flat-fielding
   *
   * @throws HardwareException
   * @author Greg Esparza
   */
  void enableFlatField() throws HardwareException
  {
    if (isSimulationModeOn() == false)
      _nativeLineScanCameras.enableFlatField();
  }

  /**
   * Disable flat-fielding
   *
   * @throws HardwareException
   * @author Greg Esparza
   */
  void disableFlatField() throws HardwareException
  {
    if (isSimulationModeOn() == false)
      _nativeLineScanCameras.disableFlatField();
  }

  /**
   * Check if flat-fielding is enabled
   *
   * @return A bool indicating whether flat-fielding is enabled or disabled
   * @author Greg Esparza
   */
  boolean isFlatFieldEnabled()
  {
    boolean enabled = false;
    if (isSimulationModeOn() == false)
      return enabled = _nativeLineScanCameras.isFlatFieldEnabled();
    else
    {
      if (UnitTest.unitTesting() == false)
        System.out.println("LineScanCameras.isFlatFieldEnabled stub"); // need to support sim mode properly here
    }

    return enabled;
  }
}
