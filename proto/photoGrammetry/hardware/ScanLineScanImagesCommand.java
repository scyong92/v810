package com.agilent.xRayTest.hardware;

import com.agilent.xRayTest.datastore.panelSettings.*;
import com.agilent.util.*;

/**
* This command will tell either a single pair of line scan cameras or both
* pairs of line scan cameras (top and bottom) to scan in images.  If
* the hardware has all 4 cameras then this command will always tell all
* 4 to scan.  If only 2 cameras exist then those two cameras will do the
* scan.
*
* This command takes care of:
* - telling the panel positioning subsystem to move
* - telling the line scan cameras to scan
* - getting the images to the correct ImageBuffer for processing
*
* @author Bill Darbie
*/
public class ScanLineScanImagesCommand extends Command
{
  private static LineScanSubsystem _lineScanSubsystem;
  private static LineScanImageObservable _lineScanImageObservable;

  private int _xStartPositionInNanos = -1;
  private int _xEndPositionInNanos = -1;
  private StageSpeed _stageSpeed = null;

  /**
   * @author Bill Darbie
   */
  static
  {
    _lineScanSubsystem = LineScanSubsystem.getInstance();
    _lineScanImageObservable = LineScanImageObservable.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public ScanLineScanImagesCommand(StageSpeed stageSpeed, int xStartPositionInNanos, int xEndPositionInNanos)
  {
    Assert.assert(stageSpeed != null);
    Assert.assert(xStartPositionInNanos != -1);
    /** @todo add more code here */
  }

  /**
   * Snap an image.  The image is available by Observing the XrayImageObservable object.
   * @author Bill Darbie
   */
  public void execute() throws HardwareException
  {
    LineScanCameraImage topFrontImage = new LineScanCameraImage();
    LineScanCameraImage topRearImage = new LineScanCameraImage();
    LineScanCameraImage bottomFrontImage = new LineScanCameraImage();
    LineScanCameraImage bottomRearImage = new LineScanCameraImage();

    /** @todo wpd */
//    _lineScanSubsystem.scan(_stageSpeed,
//                            _xStartPositionInNanos,
//                            _xEndPositionInNanos,
//                            topFrontImage,
//                            topRearImage,
//                            bottomFrontImage,
//                            bottomRearImage);

    _lineScanImageObservable.addImages(topFrontImage, topRearImage, bottomFrontImage, bottomRearImage);
  }

  /**
   * @author Kristin Casterton
   */
  public void isValid() throws HardwareException
  {
    // do nothing now
    // this command never has information that would be invalid
    // so don't throw an exception
  }
}
