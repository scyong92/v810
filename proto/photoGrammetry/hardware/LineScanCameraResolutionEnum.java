package com.agilent.xRayTest.hardware;

import java.io.*;
import java.util.*;

import com.agilent.util.*;

/**
 * This class contains all the possible Resolution settings for the line scan camera.
 * @author Tony Turner
 */
public class LineScanCameraResolutionEnum extends Enum
{
  private static Map _intToEnumMap = new HashMap();
  private static int _index = -1;
  public static final LineScanCameraResolutionEnum PIXELS_1x1024 = new LineScanCameraResolutionEnum(++_index);
  public static final LineScanCameraResolutionEnum PIXELS_1x2048 = new LineScanCameraResolutionEnum(++_index);

  /**
   * @author Tony Turner
   */
  private LineScanCameraResolutionEnum(int resolution)
  {
    super(resolution);

    //Set up a map that can convert an integer value to an enumerated type
    _intToEnumMap.put(new Integer(resolution), this);
  }

  /**
   * Given a legacy integer value, return the corresponding enumerated type.
   *
   * This function will be used from NativeLineScanCameraPair.getResolution().
   * The function will first get the integer resolution (binning) value from the native
   * interface.  The integer will then be converted to the appropriate enumerated type
   * passed up hardware and business layers.
   *
   * @param legacyEnumValue - An input that maps to the desired enumerated type
   * @return A LineScanCameraResolutionEnum that maps to the input integer value
   * @author Greg Esparza
   */
  public static LineScanCameraResolutionEnum getEnum(int legacyEnumValue)
  {
    LineScanCameraResolutionEnum enum = (LineScanCameraResolutionEnum)_intToEnumMap.get(new Integer(legacyEnumValue));
    Assert.assert(enum != null);

    return enum;
  }

  /**
   * @return the integer value that the legacy code uses for the camera resolution.
   * @author Greg Esparza
   */
  public int getLegacyType()
  {
    return _id;
  }
}
