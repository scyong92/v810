package com.agilent.xRayTest.hardware;

/**
 * This class provides an interface to control the line scan camera light subsystem
 *
 * @author Greg Esparza
 */
class LineScanLights extends HardwareObject
{
  private LineScanController _lineScanController;
  private static LineScanLights _instance;

  private LineScanLights()
  {
    _lineScanController = LineScanController.getInstance();
  }

 /**
  * Get the one and only instance of the LineScanLights object
  *
  * @return A LineScanLight object
  * @author Greg Esparza
  */
  static synchronized LineScanLights getInstance()
  {
    if (_instance == null)
      _instance = new LineScanLights();

    return _instance;
  }

  /**
   * Set the light intensity percentage
   *
   * This function will set the light subsystem intensity to the desired percentage.
   * A valid range is 1 to 100 percent.
   *
   * @param lightIntensityInPercent - An integer representing the light intensity percentage
   * @throws HardwareException
   * @author Greg Esparza
   */
  void setLightIntensity(int lightIntensityInPercent) throws HardwareException
  {
    _lineScanController.setLightIntensity(lightIntensityInPercent);
  }

  /**
   * Get the light intensity percentage
   *
   * This function will return the light intensity percentage that the line scan
   * light subsystem is currently set to.
   *
   * @return An integer representing the light intensity percentage
   * @throws HardwareException
   * @author Greg Esparza
   */
  int getLightIntensity() throws HardwareException
  {
    return _lineScanController.getLightIntensity();
  }
}
