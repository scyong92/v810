package com.agilent.xRayTest.hardware;

import com.agilent.xRayTest.hardware.nativeInt.*;
import java.io.*;
import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.datastore.hwConfig.*;
import java.util.*;

import com.agilent.util.*;

/**
* This is a unit test for the LineScanCameras class.  This test class will also
* exercise the NativeLineScanCameras class.
*
* @author Greg Esparza
*/
public class Test_LineScanCameras extends UnitTest
{
  private DatastoreFactory _datastoreFactory = new DatastoreFactory();
  private HardwareConfigInt _hwConfigInt = _datastoreFactory.getHardwareConfigInt();
  private DatastoreHandle _datastoreHandle;
  private LineScanCameras _lineScanCameras = null;
  private static final int _PIXELS_2048_WIDE = 2048;
  private static final int _PIXELS_18000_LONG = 18000;


  /**
   * Static declarations
   *
   * @author Greg Esparza
   */
  static
  {
    //Load the native5dxHardware.dll so calls can be made into the JNI layer and native code
    System.loadLibrary("native5dxHardware");
  }

  /**
   * Main
   *
   * @author Greg Esparza
   */
  public static void main(String[] args)
  {
    Test_LineScanCameras testLineScanCameras = new Test_LineScanCameras();
    UnitTest.execute (testLineScanCameras);
  }


  /**
   * Run a set of tests on the LineScanCameras object
   *
   * @param is - A BufferedReader object that will will read from a
   *             "hardware/autotest/Test_LineScanCameras_<number>.input" file if it exists
   *        os - A PrintWriter object that will write the results to a
   *             "hardware/autotest/Test_LineScanCameras_<number>.expect" file if it exists
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    //First, load all the values from the hardware-related configuration files
    //for the HardwareConfigEnum class
    List warnings = new ArrayList();
    try
    {
      _hwConfigInt.load(DatastoreMode.READ_ONLY, warnings);
      Iterator it = warnings.iterator();
      while(it.hasNext())
      {
        LocalizedString ls = (LocalizedString)it.next();
        os.println("WARNING: " + ls);
      }
    }
    catch(DatastoreException ioe)
    {
      ioe.printStackTrace();
    }

    boolean topFrontCameraEnable    = true;
    boolean topRearCameraEnable     = false;
    boolean bottomFrontCameraEnable = true;
    boolean bottomRearCameraEnable  = false;

    LineScanCameraImage topFrontImage    = new LineScanCameraImage();
    LineScanCameraImage topRearImage     = new LineScanCameraImage();
    LineScanCameraImage bottomFrontImage = new LineScanCameraImage();
    LineScanCameraImage bottomRearImage  = new LineScanCameraImage();

    LineScanCameraImage darkImage        = new LineScanCameraImage();
    LineScanCameraImage brightImage      = new LineScanCameraImage();
    byte[] tempDarkImagePixelArray       = new byte[1*2048*4];
    byte[] tempBrightImagePixelArray     = new byte[1*2048*4];

    darkImage.set(tempDarkImagePixelArray, 2048*4, 1);
    tempDarkImagePixelArray = null;
    brightImage.set(tempBrightImagePixelArray, 2048*4, 1);
    tempBrightImagePixelArray = null;

    // test constructor & getInstance
     _lineScanCameras = LineScanCameras.getInstance();

    //call the interface functions
    try
    {
      //Set the flat field reference images
      _lineScanCameras.setFlatFieldReferenceImages(darkImage, brightImage);
      darkImage.deleteImage();
      darkImage = null;
      brightImage.deleteImage();
      brightImage = null;

      //Get the current accquistion properties
      LineScanCameraAcquisitionProperties properties = _lineScanCameras.getImageAcquisitionProperties();

      //Make desired changes to the properties
      properties.setImageResolution(LineScanCameraResolutionEnum.PIXELS_1x2048);
      properties.setNumberOfImageTransferBuffers(2);
      properties.setImageWidthInPixels(_PIXELS_2048_WIDE);
      properties.setImageLengthInPixels(10000);
      properties.setTopFrontCameraHorizontalImageFlipEnable(topFrontCameraEnable);
      properties.setTopRearCameraHorizontalImageFlipEnable(topRearCameraEnable);
      properties.setBottomFrontCameraHorizontalImageFlipEnable(bottomFrontCameraEnable);
      properties.setBottomRearCameraHorizontalImageFlipEnable(bottomRearCameraEnable);

      //Now, update the LineScanCameras object with the modified properties and prepare for acquisition
      _lineScanCameras.prepareForImageAcquisition(properties);

      //Get the acquisition properties again
      LineScanCameraAcquisitionProperties newProperties = _lineScanCameras.getImageAcquisitionProperties();

      //Verify that the modifications were set in the LineScanCameras object
      Expect.expect(properties.getCameraType().equals(newProperties.getCameraType()));
      Expect.expect(properties.getCameraPairInstalled().equals(newProperties.getCameraPairInstalled()));
      Expect.expect(properties.getImageResolution().equals(newProperties.getImageResolution()));
      Expect.expect(newProperties.getNumberOfImageTransferBuffers() == properties.getNumberOfImageTransferBuffers());
      Expect.expect(newProperties.getImageWidthInPixels() == properties.getImageWidthInPixels());
      Expect.expect(newProperties.getImageLengthInPixels() == properties.getImageLengthInPixels());
      Expect.expect(newProperties.getTopFrontCameraHorizontalImageFlipEnable() == properties.getTopFrontCameraHorizontalImageFlipEnable());
      Expect.expect(newProperties.getTopRearCameraHorizontalImageFlipEnable() == properties.getTopRearCameraHorizontalImageFlipEnable());
      Expect.expect(newProperties.getBottomFrontCameraHorizontalImageFlipEnable() == properties.getBottomFrontCameraHorizontalImageFlipEnable());
      Expect.expect(newProperties.getBottomRearCameraHorizontalImageFlipEnable() == properties.getBottomRearCameraHorizontalImageFlipEnable());

      //Begin acquisition
      _lineScanCameras.beginImageAcquisition();

      //End acquisition
      _lineScanCameras.endImageAcquisition(topFrontImage, topRearImage, bottomFrontImage, bottomRearImage);

      //Abort acquisition
      _lineScanCameras.abortImageAcquisition();

      //long startTime = 0;
      //long endTime   = 0;
      //startTime = System.currentTimeMillis();

      //Get a copy of the TOP camera pair image data generated from the native side
      topFrontImage.getNativeImage();
      topRearImage.getNativeImage();

      //endTime = System.currentTimeMillis();
      //System.out.println("TOP front and rear image copy time = " + (endTime - startTime) +
      //                   " for an image size of " + topFrontImage.getWidthInPixels() + " x " + topFrontImage.getLengthInPixels());

      //startTime = System.currentTimeMillis();

      //Get a copy of the BOTTOM camera pair image data generated from the native side
      bottomFrontImage.getNativeImage();
      bottomRearImage.getNativeImage();

      //endTime = System.currentTimeMillis();
      //System.out.println("BOTTOM front and rear image copy time = " + (endTime - startTime) +
      //                   " for an image size of " + topFrontImage.getWidthInPixels() + " x " + topFrontImage.getLengthInPixels());

      //Clean up the acquisition resources
      _lineScanCameras.cleanUpImageAcquisitionResources(topFrontImage,
                                                        topRearImage,
                                                        bottomFrontImage,
                                                        bottomRearImage);

      //Set the vertical image flip
      _lineScanCameras.setVerticalImageFlipEnable(topFrontCameraEnable,
                                                  topRearCameraEnable,
                                                  bottomFrontCameraEnable,
                                                  bottomRearCameraEnable);

      //Enable flat field
      _lineScanCameras.enableFlatField();

      //Verify that flat field is enabled
      Expect.expect(_lineScanCameras.isFlatFieldEnabled() == true);

      //Disable flat field
      _lineScanCameras.disableFlatField();

      //Verify that flat field is disabled
      Expect.expect(_lineScanCameras.isFlatFieldEnabled() == false);
    }
    catch(HardwareException he)
    {
       he.printStackTrace();
    }
  }
}



