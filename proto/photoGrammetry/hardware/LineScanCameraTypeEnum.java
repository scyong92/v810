package com.agilent.xRayTest.hardware;

import java.io.*;
import java.util.*;

import com.agilent.util.*;

/**
 * This class contains all the possible line scan camera types being used
 * for Photogrammetry Surface Mapping.
 *
 * @author Greg Esparza
 */
public class LineScanCameraTypeEnum extends Enum
{
  private static Map _intToEnumMap = new HashMap();
  private static Map _stringToEnumMap = new HashMap();
  private static int _index = -1;
  public static final LineScanCameraTypeEnum DALSA_SP_14_02K30 = new LineScanCameraTypeEnum(++_index, new String("DALSA_SP_14_02K30"));


  private LineScanCameraTypeEnum(int cameraType, String cameraTypeName)
  {
    super(cameraType);

    //Set up a map that can convert an integer value to an enumerated type
    _intToEnumMap.put(new Integer(cameraType), this);
    _stringToEnumMap.put(cameraTypeName, this);
  }

  /**
   * Given a legacy integer value, return the corresponding enumerated type.
   *
   * This function will be used from NativeLineScanCameraPair.getCameraType().
   * The function will first get the integer camera type value from the native
   * interface.  The integer will then be converted to the appropriate enumerated type
   * passed up hardware and business layers.
   *
   * @param legacyEnumValue - An input that maps to the desired enumerated type
   * @return A LineScanCameraTypeEnum that maps to the input integer value
   * @author Greg Esparza
   */
  public static LineScanCameraTypeEnum getEnum(int legacyEnumValue)
  {
    LineScanCameraTypeEnum enum = (LineScanCameraTypeEnum)_intToEnumMap.get(new Integer(legacyEnumValue));
    Assert.assert(enum != null);

    return enum;
  }

  /**
   * Given a string name for the camera type, return the corresponding enumerated type.
   *
   * @param cameraTypeName - A string that maps to the desired enumerated type
   * @return A LineScanCameraTypeEnum that maps to the input string value
   * @author Greg Esparza
   */
  public static LineScanCameraTypeEnum getEnum(String cameraTypeName)
  {
    LineScanCameraTypeEnum enum = (LineScanCameraTypeEnum)_stringToEnumMap.get(cameraTypeName);
    Assert.assert(enum != null);

    return enum;
  }

  /**
   * Get the integer representation that maps to the legacy code's enumeration
   *
   * @return An integer value that the legacy code uses for the camera type.
   * @author Greg Esparza
   */
  public int getLegacyType()
  {
    return _id;
  }
}
