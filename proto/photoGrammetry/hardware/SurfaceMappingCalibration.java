package com.agilent.xRayTest.hardware;

import com.agilent.xRayTest.hardware.HardwareException;
import com.agilent.util.*;
// import java.awt.image.*;

public class SurfaceMappingCalibration
{
    public SurfaceMappingCalibration()
    {
    try
    {
        // message #2.1.1 to _lineScanLights:com.agilent.xRayTest.hardware.LineScanLights
        LineScanLights _lineScanLights = LineScanLights.getInstance();
        // message #2.1.2 to _lineScanCameras:com.agilent.xRayTest.hardware.LineScanCameras
        LineScanCameras _lineScanCameras = LineScanCameras.getInstance();
        // message #2.1.3 to _topFrontImage:com.agilent.xRayTest.hardware.LineScanCameraImage
        LineScanCameraImage _topFrontImage = new LineScanCameraImage();
        // message #2.1.4 to _topRearImage:com.agilent.xRayTest.hardware.LineScanCameraImage
        LineScanCameraImage _topRearImage = new LineScanCameraImage();
        // message #2.1.5 to _bottomFrontImage:com.agilent.xRayTest.hardware.LineScanCameraImage
        LineScanCameraImage _bottomFrontImage = new LineScanCameraImage();
        // message #2.1.6 to _bottomRearImage:com.agilent.xRayTest.hardware.LineScanCameraImage
        LineScanCameraImage _bottomRearImage = new LineScanCameraImage();
        // message #2.1.7 to _imageOperation:com.agilent.xRayTest.hardware.LineScanCameraImageOperation
        LineScanCameraImageOperation _imageOperation = new LineScanCameraImageOperation();
        // message #2.1.8 to _lineScanCameras:com.agilent.xRayTest.hardware.LineScanCameras
        LineScanCameraAcquisitionProperties _acquisitionProperties = _lineScanCameras.getImageAcquisitionProperties();
        // message #2.1.9 to _acquisitionProperties:com.agilent.xRayTest.hardware.LineScanCameraAcquisitionProperties
        int _savedImageLengthInPixels = _acquisitionProperties.getImageLengthInPixels();
        // message #2.1.10 to _acquisitionProperties:com.agilent.xRayTest.hardware.LineScanCameraAcquisitionProperties
        int _savedImageHeightInPixels = _acquisitionProperties.getImageWidthInPixels();
        // message #2.1.11 to _acquisitionProperties:com.agilent.xRayTest.hardware.LineScanCameraAcquisitionProperties
        _acquisitionProperties.setImageLengthInPixels(1);
        // message #2.1.12 to _acquisitionProperties:com.agilent.xRayTest.hardware.LineScanCameraAcquisitionProperties
        _acquisitionProperties.setImageWidthInPixels(2048);
        // message #2.1.13 to _acquisitionProperties:com.agilent.xRayTest.hardware.LineScanCameraAcquisitionProperties
        _acquisitionProperties.setImageResolution(LineScanCameraResolutionEnum.PIXELS_1x2048);
        // message #2.1.14 to _lineScanLights:com.agilent.xRayTest.hardware.LineScanLights
        _lineScanLights.setLightIntensity(100);
    }
    catch (HardwareException he)
    {
      System.out.println("Hardware Exception thrown");
    }
    }

    public int get_lightIntensity(){
            return _lightIntensity;
        }

    public void set_lightIntensity(int _lightIntensity){
            this._lightIntensity = _lightIntensity;
        }

    public void adjustPitch(LineScanCameraPairInstalledEnum installed) throws HardwareException
    {
        // message #2.1.1 to _lineScanCameras:com.agilent.xRayTest.hardware.LineScanCameras
        _lineScanCameras.prepareForImageAcquisition(_acquisitionProperties);
        while (!is_correctPitch()) {
            // message #2.1.2.1 to _lineScanCameras:com.agilent.xRayTest.hardware.LineScanCameras
            _lineScanCameras.beginImageAcquisition();
            // message #2.1.2.2 to _lineScanCameras:com.agilent.xRayTest.hardware.LineScanCameras
            _lineScanCameras.snap();
            // message #2.1.2.3 to _lineScanCameras:com.agilent.xRayTest.hardware.LineScanCameras
            _lineScanCameras.endImageAcquisition(_topFrontImage, _topRearImage, _bottomFrontImage, _bottomRearImage);
            // message #2.1.2.4 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
            this.operatorAdjustPitch(_topFrontImage, _topRearImage);
            if (installed.equals(LineScanCameraPairInstalledEnum.TOP_AND_BOTTOM_PAIR_INSTALLED))
            {
                // message #2.1.2.5.1 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
                this.operatorAdjustPitch(_bottomFrontImage, _bottomRearImage);
            }
        }
    }

    public void confirmCCDPixelDynamicIntegrity(){}

    public void confirmLightSourceAndCameraOperational(LineScanCameraPairInstalledEnum installed)
    {

        try
        {
          // message #1.2.1 to _lineScanCameras:com.agilent.xRayTest.hardware.LineScanCameras
          _lineScanCameras.prepareForImageAcquisition(_acquisitionProperties);
          // message #1.2.2 to _lineScanCameras:com.agilent.xRayTest.hardware.LineScanCameras
          _lineScanCameras.beginImageAcquisition();
          // message #1.2.3 to _lineScanCameras:com.agilent.xRayTest.hardware.LineScanCameras
          _lineScanCameras.snap();
          // message #1.2.4 to _lineScanCameras:com.agilent.xRayTest.hardware.LineScanCameras
          _lineScanCameras.endImageAcquisition(_topFrontImage, _topRearImage, _bottomFrontImage, _bottomRearImage);
          if (installed == LineScanCameraPairInstalledEnum.TOP_PAIR_INSTALLED) {
              // message #1.2.5.1 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
              this.confirmLightSourceAndCameraBasicOperation(_topFrontImage, _topRearImage);
          }
          else {
              // message #1.2.6.1 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
              this.confirmLightSourceAndCameraBasicOperation(_bottomFrontImage, _bottomRearImage);
          }
    }
        catch (HardwareException he)
        {
        }
    }

    public void pixelIntegrity(){}

    public void lightSourceOperational(){}

    public void confirmCameraLinearity() throws HardwareException {}

    public void confirmCamerasOperational(){}

    public void confirmCCDPixelStaticIntegrity(){}

    public void executeRuntimeCalibration(LineScanCameraPairInstalledEnum installed) throws HardwareException
    {
        // message #2.1.1 to boardCADData:BoardCADData
/*        boolean alreadyCalibrated = boardCADData.isCalibrated();
        if (alreadyCalibrated) {
            // message #2.1.2.1 to boardCADData:BoardCADData
            int nominalIntensity = boardCADData.getNominalIntensity();
            // message #2.1.2.2 to _lineScanController:com.agilent.xRayTest.hardware.LineScanController
            _lineScanController.setLightIntensity(nominalIntensity);
            // message #2.1.2.4 to _lineScanCamera:com.agilent.xRayTest.hardware.LineScanCameraPair
            _lineScanCamera.setFlatFieldReferenceImages(darkFlatFieldImage, brightFlatFieldImage);
        }
        else {
            // message #2.1.3.2 to _lineScanController:com.agilent.xRayTest.hardware.LineScanController
            _lineScanController.setLightIntensity(intensity * 2);
            do {
                // message #2.1.3.7.1 to _lineScanController:com.agilent.xRayTest.hardware.LineScanController
                _lineScanController.setLightIntensity(((--intensity) * 2));
            } while (QValues < goodBoardQValues);
        }
        // message #2.1.4 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
        this.PerformAllRequiredCalibrations();
*/
    }

  // Yaw related methods
    public void operatorChecksYaw(){
        // message #1.1 to this:Calibration
        boolean some_boolean = this.is_yawOk();
        if (is_yawOk()) {
            // message #1.2.1 to this:Calibration
            this.set_correctYaw(true);
        }
        else {
            /**@todo act - handle operator manually adjusting Yaw */
        }
    }

    private boolean _yawOk;

    public boolean is_yawOk(){
            return _yawOk;
        }

    public void set_yawOk(boolean _yawOk){
        this._yawOk = _yawOk;
    }

    private boolean _correctYaw;

    public boolean is_correctYaw(){
            return _correctYaw;
        }

    public void set_correctYaw(boolean _correctYaw){
        this._correctYaw = _correctYaw;
    }

  // Pitch related methods
    public void operatorAdjustPitch(LineScanCameraImage frontImage, LineScanCameraImage rearImage) throws HardwareException
    {
        // message #2.1.1.1.2.4.1 to _frontImage:com.agilent.xRayTest.hardware.LineScanCameraImage
        byte[] _nativeFrontImage = frontImage.getNativeImage();
        // message #2.1.1.1.2.4.2 to _rearImage:com.agilent.xRayTest.hardware.LineScanCameraImage
        byte[] _nativeRearImage = rearImage.getNativeImage();
        // message #2.1.1.1.2.4.3 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
/*        BufferedImage shapeROIImage = this.createRegionOfInterestShapeImage(); */
        // message #2.1.1.1.2.4.4 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
/*        BufferedImage compositeImage = this.buildCompositeImage(frontImage, rearImage, shapeROIImage); */
        // message #2.1.1.1.2.4.5 to frmCalibration:FrmCalibration

// need to figure out how to 'inform' the GUI of the need to update its graphics
//        frmCalibration.update(compositeImage);

        // message #2.1.1.1.2.4.6 to _frontImage:com.agilent.xRayTest.hardware.LineScanCameraImage
        frontImage.deleteImage();
        // message #2.1.1.1.2.4.7 to _rearImage:com.agilent.xRayTest.hardware.LineScanCameraImage
        rearImage.deleteImage();
    }

    private boolean _pitchOk;

    public boolean is_pitchOk(){
            return _pitchOk;
        }

    public void set_pitchOk(boolean _pitchOk){
        this._pitchOk = _pitchOk;
    }

    private boolean _correctPitch;

    public boolean is_correctPitch(){
            return _correctPitch;
        }

    public void set_correctPitch(boolean _correctPitch){
        this._correctPitch = _correctPitch;
    }

  // Magnification related methods
    public void operatorChecksMagnification()
    {
        // message #1.1 to this:Calibration
        boolean some_boolean = this.is_MagnificationOk();
        if (is_MagnificationOk()) {
            // message #1.2.1 to this:Calibration
            this.set_correctMagnification(true);
        }
        else {
            /**@todo act - handle operator manually adjusting Magnification */
        }
    }

    private boolean _MagnificationOk;

    public boolean is_MagnificationOk(){
        return _MagnificationOk;
    }

    public void set_MagnificationOk(boolean _magnificationOk){
        this._MagnificationOk = _magnificationOk;
    }

    private boolean _correctMagnification;

    public boolean is_correctMagnification(){
            return _correctMagnification;
        }

    public void set_correctMagnification(boolean _correctMagnification){
        this._correctMagnification = _correctMagnification;
    }

  // FieldOfViewIntersection related methods
    public void operatorChecksFieldOfViewIntersection()
    {
        // message #1.1 to this:Calibration
        boolean some_boolean = this.is_fieldOfViewIntersectionOk();
        if (is_fieldOfViewIntersectionOk()) {
            // message #1.2.1 to this:Calibration
            this.set_correctFieldOfViewIntersection(true);
        }
        else {
            /**@todo act - handle operator manually adjusting FieldOfViewIntersection */
        }
    }

    private boolean _fieldOfViewIntersectionOk;

    public boolean is_fieldOfViewIntersectionOk(){
            return _fieldOfViewIntersectionOk;
        }

    public void set_fieldOfViewIntersectionOk(boolean _fieldOfViewIntersectionOk){
        this._fieldOfViewIntersectionOk = _fieldOfViewIntersectionOk;
    }

    private boolean _correctFieldOfViewIntersection;

    public boolean is_correctFieldOfViewIntersection(){
            return _correctFieldOfViewIntersection;
        }

    public void set_correctFieldOfViewIntersection(boolean _correctFieldOfViewIntersection){
        this._correctFieldOfViewIntersection = _correctFieldOfViewIntersection;
    }

    public void performAllRequiredCalibrations(LineScanCameraImage frontImage, LineScanCameraImage rearImage) throws HardwareException {
        // message #2.1.4.1 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
        this.planeOfFocusAdjustment();
        // message #2.1.4.2 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
        this.adjustFlatFieldingForCurrentBoardType();
        // message #2.1.4.3 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
        this.confirmCCDPixelIntegrity();
        // message #2.1.4.4 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
        this.confirmCameraPairPhysicalAlignment();
        // message #2.1.4.5 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
        this.confirmFocalDistance();
        // message #2.1.4.6 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
        this.confirmFocus();
        // message #2.1.4.7 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
        this.confirmFieldOfViewIntersection();
        // message #2.1.4.8 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
        this.confirmCameraLinearity();
        // message #2.1.4.9 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
        this.confirmLightSourceLinearity();
    }

    public void planeOfFocusAdjustment() throws HardwareException {}

    public void adjustFlatFieldingImages() throws HardwareException {}

    public void confirmCCDPixelIntegrity() throws HardwareException {}

    public void confirmCameraPairPhysicalAlignment() throws HardwareException {}

    public void confirmFocalDistance() throws HardwareException {}

    public void confirmFocus(){}

    public void confirmFieldOfViewIntersection() throws HardwareException {}

    public void confirmLightSourceLinearity() throws HardwareException {}

    public void confirmLightSourceAndCameraLinearity(LineScanCameraImage frontImage, LineScanCameraImage rearImage) throws HardwareException
    {
        // message #1.2.5.1.1 to frontImage:com.agilent.xRayTest.hardware.LineScanCameraImage
        byte[] nativeFrontImage = frontImage.getNativeImage();
        // message #1.2.5.1.2 to rearImage:com.agilent.xRayTest.hardware.LineScanCameraImage
        byte[] nativeRearImage = rearImage.getNativeImage();
        // message #1.2.5.1.3 to _imageOperation:com.agilent.xRayTest.hardware.LineScanCameraImageOperation
        int frontImageAverageGrayLevel = _imageOperation.averageGrayLevel(nativeFrontImage);
        // message #1.2.5.1.4 to _imageOperation:com.agilent.xRayTest.hardware.LineScanCameraImageOperation
        int rearImageAverageGrayLevel = _imageOperation.averageGrayLevel(nativeRearImage);
        // message #1.2.5.1.5 to _acquisitionProperties:com.agilent.xRayTest.hardware.LineScanCameraAcquisitionProperties
        int _maxDarkImageGrayLevelLimit = _acquisitionProperties.getmaxDarkImageGrayLevel();
        // message #1.2.5.1.6 to _acquisitionProperties:com.agilent.xRayTest.hardware.LineScanCameraAcquisitionProperties
        int _minBrightImageGrayLevelLimit = _acquisitionProperties.getminBrightImageGrayLevel();
        if ((frontImageAverageGrayLevel < _maxDarkImageGrayLevelLimit) &&
            (rearImageAverageGrayLevel < _maxDarkImageGrayLevelLimit))
        {
            throw new HardwareException(new LocalizedString("HW_CA_BAD_LIGHT_SOURCE", null));
        }
        if (frontImageAverageGrayLevel < _minBrightImageGrayLevelLimit)
        {
            throw new HardwareException(new LocalizedString("HW_CA_BAD_FRONT_CAMERA", null));
        }
        if (rearImageAverageGrayLevel < _minBrightImageGrayLevelLimit)
        {
            throw new HardwareException(new LocalizedString("HW_CA_BAD_REAR_CAMERA", null));
        }
        // message #1.2.5.1.10 to _imageOperation:com.agilent.xRayTest.hardware.LineScanCameraImageOperation
        _imageOperation.confirmGoodPixels(frontImage);
        // message #1.2.5.1.11 to _imageOperation:com.agilent.xRayTest.hardware.LineScanCameraImageOperation
        _imageOperation.confirmGoodPixels(rearImage);
    }

    public void alignCameraPitch(LineScanCameraImage frontImage, LineScanCameraImage rearImage) throws HardwareException {
        // message #2.1.1.1.1 to _lineScanCameras:com.agilent.xRayTest.hardware.LineScanCameras
        _lineScanCameras.prepareForImageAcquisition(_acquisitionProperties);
        while (!is_correctPitch()) {
            // message #2.1.1.1.2.1 to _lineScanCameras:com.agilent.xRayTest.hardware.LineScanCameras
            _lineScanCameras.beginImageAcquisition();
            // message #2.1.1.1.2.2 to _lineScanCameras:com.agilent.xRayTest.hardware.LineScanCameras
            _lineScanCameras.snap();
            // message #2.1.1.1.2.3 to _lineScanCameras:com.agilent.xRayTest.hardware.LineScanCameras
            _lineScanCameras.endImageAcquisition(_topFrontImage, _topRearImage, _bottomFrontImage, _bottomRearImage);
            // message #2.1.1.1.2.4 to this:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
            this.operatorAdjustPitch(_topFrontImage, _topRearImage);
        }
    }

/*    private BufferedImage buildCompositeImage(LineScanCameraImage frontImage, LineScanCameraImage rearImage, BufferedImage shapeImage)
    {
        return new BufferedImage(0,0,0);
    }
*/
    private boolean _benchTestTopAssembly;

    public boolean is_benchTestTopAssembly(){
            return _benchTestTopAssembly;
        }

    public void set_benchTestTopAssembly(boolean _benchTestTopAssembly){
        this._benchTestTopAssembly = _benchTestTopAssembly;
    }

    public void alignCameraYaw()
    {
    }

    public void adjustCameraMagnification(){}

    public void adjustCameraFocus(){}

    public void alignYaw(){}

    private boolean _correctFocus;

    public boolean is_correctFocus(){
            return _correctFocus;
        }

    public void set_correctFocus(boolean _correctFocus){
        this._correctFocus = _correctFocus;
    }

    public void operatorChecksFocus(LineScanCameraImage p0, LineScanCameraImage p1){}

    public void checkLightSourceAndCameraLinearity(LineScanCameraImage p0, LineScanCameraImage p1){}

    public int get_savedImageHeightInPixels(){
            return _savedImageHeightInPixels;
        }

    public void set_savedImageHeightInPixels(int _savedImageHeightInPixels){
            this._savedImageHeightInPixels = _savedImageHeightInPixels;
        }

    public int get_saveImageWidthInPixels(){
            return _saveImageWidthInPixels;
        }

    public void set_saveImageWidthInPixels(int _saveImageWidthInPixels){
            this._saveImageWidthInPixels = _saveImageWidthInPixels;
        }

/*    public BufferedImage createRegionOfInterestShapeImage()
    {
        return new BufferedImage(0,0,0);
    }
*/
    public void adjustYaw(LineScanCameraPairInstalledEnum installed){}

    public void operatorAdjustYaw(LineScanCameraImage frontImage, LineScanCameraImage rearImage){}

    public void adjustMagnification(LineScanCameraPairInstalledEnum installed){}

    public void operatorAdjustMagnification(LineScanCameraImage frontImage, LineScanCameraImage rearImage){}

    public void adjustFocus(LineScanCameraPairInstalledEnum installed){}

    public void operatorAdjustFocus(LineScanCameraImage frontImage, LineScanCameraImage rearImage){}

    public void adjustFieldOfViewIntersection(LineScanCameraPairInstalledEnum installed){}

    public void operatorAdjustFieldOfViewIntersection(LineScanCameraImage frontImage0, LineScanCameraImage rearImage){}

    public void confirmLightSourceAndCameraLinearity(LineScanCameraPairInstalledEnum installed){}

    public void confirmSurfaceMappingOperational(LineScanCameraPairInstalledEnum installed){}

    public void confirmLightSourceAndCameraBasicOperation(LineScanCameraImage frontImage, LineScanCameraImage rearImage){}

    public void confirmSurfaceMappingLinearity(LineScanCameraPairInstalledEnum installed){}

    public boolean is_newWeek(){
            return _newWeek;
        }

    public void set_newWeek(boolean _newWeek){
            this._newWeek = _newWeek;
        }

    public boolean is_newBoardType(){
        return _newBoardType;
    }

    public void set_newBoardType(boolean _newBoardType){
        this._newBoardType = _newBoardType;
    }

    public boolean is_newMonth(){
            return _newMonth;
        }

    public void set_newMonth(boolean _newMonth){
        this._newMonth = _newMonth;
    }

    public void adjustFlatFieldingForCurrentBoardType(){}

    public boolean is_newPanelLoaded(){
            return _newPanelLoaded;
        }

    public void set_newPanelLoaded(boolean _newPanelLoaded){
            this._newPanelLoaded = _newPanelLoaded;
        }

    private int _lightIntensity;
    private LineScanLights _lineScanLights;
    private LineScanCameras _lineScanCameras;
    private LineScanCameraImage _topFrontImage;
    private LineScanCameraImage _topRearImage;
    private LineScanCameraImage _bottomFrontImage;
    private LineScanCameraImage _bottomRearImage;
    private boolean _correctFocalDistance = false;
    private LineScanCameraAcquisitionProperties _acquisitionProperties;
    private byte[][] _nativeFrontImages;
    private byte[][] _nativeRearImages;
    private int[] _frontImageAverageGrayLevel;
    private int _rearImageAverageGrayLevel;
    private int _maxDarkImageGrayLevelLimit;
    private int _minBrightImageGrayLevelLimit;
    private LineScanCameraImageOperation _imageOperation;
    private int _savedImageHeightInPixels;
    private int _saveImageWidthInPixels;
    private boolean _newWeek;
    private boolean _newBoardType;
    private boolean _newMonth;
    private PanelPositioner _panelPositioner;
    private boolean _newPanelLoaded;
    }
