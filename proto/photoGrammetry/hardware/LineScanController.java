package com.agilent.xRayTest.hardware;

import java.io.*;
import java.lang.*;
import java.util.*;

import com.agilent.util.*;

/**
 * ******************************************************************************************************* *
 *                                                                                                         *
 * This class is not finished with its implemetation, yet.                                                 *
 * The following is the notes for Eugene Kim-Leighton.                                                     *
 * Things to be done:                                                                                      *
 *     1) Find the right Address for hardware to be mapped and the address space.                          *
 *     2) Find out what hardware components can be controlled.                                             *
 *     3) Replace the hardcoded address with the Enumeration (LineScanHardwareControlAddressEnum).         *
 *     4) Finish defining LineScanHardwareControlAddressEnum.                                              *
 *     5) Change the parameters, in readAddress() and in writeAddress(),                                   *
 *        according to LineScanHardwareControlAddressEnum.                                                 *
 *     6) Uncomment the // comments that are part of this class implementation.                            *
 *     7) Find the way to handle exceptions that are thrown in constructor and getInstance().              *
 *     8) FTPClient and TINIFTPClient is not yet checked in.                                               *
 *     9) Make sure each method returns the right value.                                                   *
 *     10) Make sure the other Enumerations are used in this class are properly defined for this class.    *
 *         The Enumerations are LineScanCameraEnum, LineScanCameraBrightnessLeverEnum,                     *
 *         and LineScanCameraResolutionEnum.                                                               *
 *     11) In general, put more descriptive comments for this class and for each method defined.           *
 *     12) Change TINIServer to TINILineScanServer in runTelnet()                                          *
 *     13) change the path for the TINILineScanServer in runFTP()                                          *
 * ******************************************************************************************************* *
 *
 * The hardware subsystem for the surface mapping is controlled and monitored by the embedded controller,
 * TINI(Tini InterNet Interface).  TINI maps the memory in FPGA, which is main hardware controller, to do
 * something with surface mapping.  The information that TINI uses to map the memory in FPGA is sent from
 * LineScanController.  LineScanController is a bridge between hardware layer and the physical surface map
 * hardware.  LineScanController uses the TINI, via TINIServer and TINIClient, to control any hardware for
 * line Scanning, which is used for the surface mapping.  All the addresses for the hardware are  already
 * defined and hard coded as a emumeration type, so a user of this class doesn't have to worry about them.
 * LineScanController maps the address and the data for setting up for the Line Scanning.
 *
 *
 *              |                               |
 *              |               PC              |
 *              |    +---------------------+    |
 *              |    |  LineScanController |    |
 *              |    +---||-----------/\---+    |
 *              |    +---\/-----------||---+    |
 *              |    |      TINIClient     |    |
 *              |    +---||-----------/\---+    |
 *              +--------||-----------||--------+
 *  =====================||===========||========================
 *  Network-LAN          ||           ||
 *  =====================||===========||========================
 *              +--------||-----------||--------+
 *              |        ||   TINI    ||        |
 *              |    +---\/-----------||---+    |
 *              |    | TINILineScanServer  |    |
 *              |    +---||-----------/\---+    |
 *              |========||===========||========|
 *              |    +---\/-----------||---+    |
 *              |    |        FPGA         |    |
 *              |    +---------------------+    |
 *              |       Physical Hardware       |
 *              |                               |
 *
 *
 * @author Eugene Kim-Leighton
 */
class LineScanController extends HardwareObject
{
  private static final String _IP_ADDRESS = "10.20.30.41";
  private static final int _PORT       = 5679;

  private static LineScanController _instance;
  private TINIClient  _TINIClient;

  /**
   * We want to make sure there is only one instance of LineScanController.
   *
   * @throws TINIHardwareConnectionFailedException
   * @throws TINIHardwareCommandInvalidException
   * @throws TINIHardwareCommandFailedException
   * @author Eugene Kim-Leighton
   */
  public static synchronized LineScanController getInstance()
  {
    if (_instance == null)
      _instance = new LineScanController();

    return _instance;
  }

  /**
   * This creates the TINIClient instance. Also, it downloads the TINILineScanServer.tini by running FTP
   * and runs the TINILineScanServer.tini by running Telnet.
   * ****NOTE: for now this will catch the exceptions and do nothing until the better solution to handle
   * exceptions is found.
   * @author Eugene Kim-Leighton
   */
  private LineScanController()
  {
     _TINIClient = new TINIClient(_IP_ADDRESS, _PORT);
  }

  /**
   * This downloads the TINIServer to TINI using TINIFTPClient
   * and runs the TINIServer on TINI using TINITelnetClient.
   * @throws TINIHardwareConnectionFailedException
   * @throws TINIHardwareCommandInvalidException
   * @throws TINIHardwareCommandFailedException
   * @author Eugene Kim-Leighton
   */
  private void downloadAndRunServer() throws TINIHardwareConnectionFailedException,
                                             TINIHardwareCommandFailedException,
                                             TINIHardwareCommandInvalidException
  {
    if (isSimulationModeOn() == false)
    {
      runFTP();
      runTelnet();
    }
  }

  /**
   * @throws TINIHardwareConnectionFailedException if there is an error in connecting or disconnecting between the TINIFTPClient and the FTP server running on TINI.
   * @throws TINIHardwareCommandInvalidException if the commands that the TINIFTPClient sent is invalid to TINI FTP server.
   * @throws TINIHardwareCommandFailedException if the command that the TINIFTPClient sent is not sucessfully completed.
   * @author Eugene Kim-Leighton
   */
  private void runFTP() throws TINIHardwareConnectionFailedException,
                               TINIHardwareCommandInvalidException,
                               TINIHardwareCommandFailedException
  {
    if (isSimulationModeOn() == false)
    {
      TINIFTPClient tiniFTPClient = new TINIFTPClient(_IP_ADDRESS);
      if(tiniFTPClient.login())
      {
        /**@todo ekl-find out the correct path for TINILineScanServer.tini*/
        tiniFTPClient.sendBinaryFile("c:/5dx/rxx/bin/TINILineScanServer.tini", "TINILineScanServer.tini");
        tiniFTPClient.logout();
      }
    }
  }

  /**
   * @throws TINIHardwareConnectionFailedException if there is an error in connecting or disconnecting between the TINITelnetClient and the Telnet server running on TINI.
   * @throws TINIHardwareCommandInvalidException if the commands that the TINITelnetClient sent is invalid to TINI Telnet server.
   * @throws TINIHardwareCommandFailedException if the command that the TINITelnetClient sent is not sucessfully completed.
   * @author Eugene Kim-Leignton
   */
  private void runTelnet() throws TINIHardwareConnectionFailedException,
                                  TINIHardwareCommandInvalidException,
                                  TINIHardwareCommandFailedException
  {
    if (isSimulationModeOn() == false)
    {
      TINITelnetClient tiniTelnetClient = new TINITelnetClient(_IP_ADDRESS);
      if(tiniTelnetClient.login())
      {
        tiniTelnetClient.runJavaProgram("TINILineScanServer.tini");
        tiniTelnetClient.logout();
      }
    }
  }

  /**
   * If whole system needs to be reset, then networt connection to the hardware subsytem should be reset as well.
   * @throws TINIHardwareConnectionFailedException
   * @author Eugene Kim-Leighton
   */
  void resetConnectionWithHardware() throws TINIHardwareConnectionFailedException
  {
    if (isSimulationModeOn() == false)
    {
      disconnectFromHardware();
      connectToHardware();
    }
  }

  /**
   * @throws TINIHardwareConnectionFailedException
   * @throws TINIHardwareCommandInvalidException
   * @throws TINIHardwareCommandFailedException
   * @author Eugene Kim-Leighton
   */
  void setHardwareReady() throws TINIHardwareConnectionFailedException,
                                 TINIHardwareCommandInvalidException,
                                 TINIHardwareCommandFailedException
  {
    if (isSimulationModeOn() == false)
    {
      downloadAndRunServer();
      connectToHardware();
    }
  }
  /**
   * This method will be called only if disconnectFromTINI is called and to reestablish the connection.
   * Normally, conncetion to the TINI is already established when whole system boots up.
   * @throws TINIHardwareConnectionFailedException
   * @author Eugene Kim-Leighton
   */
  private void connectToHardware() throws TINIHardwareConnectionFailedException
  {
    if (isSimulationModeOn() == false)
      _TINIClient.connect();
  }

  /**
   * This method is to disconnect from the TINIServer
   *
   * @author Eugene Kim-Leighton
   */
  private void disconnectFromHardware() throws TINIHardwareConnectionFailedException
  {
    if (isSimulationModeOn() == false)
      _TINIClient.disconnect();
  }

  /**
   * Anytime the x axis of the stage moves between the minStageEncoderCount and the maxStageEncoderCount the
   * LineScan cameras will be triggered to scan in images.
   *
   * @param minXaxisEncoderCount is the lowest boundary of x-axis for the line scan camera needs to trigger.
   * @param maxXaxisEncoderCount is the highest boundary of x-axix for the line scan camera needs to trigger.
   * @throws TINIHardwareCommandInvalidException
   * @throws TINIHardwareCommandFailedException
   * @author Eugene Kim-Leighton
   */
  void setCameraTriggerRange(int minXaxisEncoderCount, int maxXaxisEncoderCount)
       throws TINIHardwareCommandFailedException, TINIHardwareCommandInvalidException
  {
    Assert.assert(minXaxisEncoderCount >= 0);
    Assert.assert(maxXaxisEncoderCount >= 0);
    Assert.assert(maxXaxisEncoderCount > minXaxisEncoderCount);

    if (isSimulationModeOn() == false)
    {
      int FPGAAddress = 0x800000;
      _TINIClient.writeAddress(FPGAAddress, minXaxisEncoderCount);
      FPGAAddress = 0x800000;
      _TINIClient.writeAddress(FPGAAddress, maxXaxisEncoderCount);
    }
  }

  /**
   * This should always match what the PanelPositioner X axis encoder count says.
   *
   * @return current X-axis counter value.
   * @throws TINIHardwareCommandInvalidException
   * @throws TINIHardwareCommandFailedException
   * @author Eugene Kim-Leighton
   */
  int getXaxisEncoderCount() throws TINIHardwareCommandFailedException, TINIHardwareCommandInvalidException
  {
    int count = 0;
    if (isSimulationModeOn() == false)
    {
      int FPGAAddress = 0x800000;
      count = _TINIClient.readAddress(FPGAAddress);
    }
    else
      System.out.println("LineScanController.getAxisEncoderCount.stub");

    return count;
  }

  /**
   * This sets the light intensity with light for line scanning.
   *
   * @param lightIntensity is the value of intensity to set
   * @throws TINIHardwareCommandInvalidException
   * @throws TINIHardwareCommandFailedException
   * @author Eugene Kim-Leighton
   */
  void setLightIntensity(int lightIntensity) throws TINIHardwareCommandFailedException, TINIHardwareCommandInvalidException
  {
    Assert.assert(lightIntensity > 0);

    if (isSimulationModeOn() == false)
    {
      int FPGAAddress = 0x800000;
      _TINIClient.writeAddress(FPGAAddress, lightIntensity);
    }
  }

  /**
   * @todo ekl - to be determined by Kendall if this will be supported in hardware.
   *
   * @return intensity of a lights
   * @throws TINIHardwareCommandInvalidException
   * @throws TINIHardwareCommandFailedException
   * @author Eugene Kim-Leignton
   */
  int getLightIntensity() throws TINIHardwareCommandFailedException, TINIHardwareCommandInvalidException
  {
    int lightIntensity = 0;
    if (isSimulationModeOn() == false)
    {
      int FPGAAddress = 0x800000;
      lightIntensity = _TINIClient.readAddress(FPGAAddress);
    }
    else
      System.out.println("LineScanController.getLightIntensity.stub");

    return lightIntensity;
  }

  /**
   * This enables the line scan camera to trigger and snap the image as long as the stage in x is in the triggering range.
   *
   * @throws TINIHardwareCommandInvalidException
   * @throws TINIHardwareCommandFailedException
   * @author Eugene Kim-Leighton
   */
  void enableLineScanCameraTrigger() throws TINIHardwareCommandFailedException, TINIHardwareCommandInvalidException
  {
    if (isSimulationModeOn() == false)
    {
      int FPGAAddress = 0x800000;
      _TINIClient.writeAddress(FPGAAddress, 1);
    }
  }

  /**
   * This disables the line scan camera to trigger and snap the image even if the stage in x is in the triggering range.
   *
   * @throws TINIHardwareCommandInvalidException
   * @throws TINIHardwareCommandFailedException
   * @author Eugene Kim-Leighton
   */
  void disableLineScanCameraTrigger() throws TINIHardwareCommandFailedException, TINIHardwareCommandInvalidException
  {
    if (isSimulationModeOn() == false)
    {
      int FPGAAddress = 0x800000;
      _TINIClient.writeAddress(FPGAAddress, 0);
    }
  }

  /**
   * This sets the time to wait after the lights are turned on before the line scan cameras should start scanning in images.
   *
   * @param delayInMillis is a time in milli second required for a camera to wait to be on after the line scan light is on.
   * @throws TINIHardwareCommandInvalidException
   * @throws TINIHardwareCommandFailedException
   * @author Eugene Kim-Leighton
   */
  /** @todo ekl - to be determined by Kendall if this will be supported in hardware */
  void setDelayInMillisBetweenLightAndCamera(int delayInMillis) throws TINIHardwareCommandFailedException, TINIHardwareCommandInvalidException
  {
    Assert.assert(delayInMillis > 0);

    if (isSimulationModeOn() == false)
    {
      int FPGAAddress = 0x800000;
      _TINIClient.writeAddress(FPGAAddress, delayInMillis);
    }
  }

  /**
   * @return time in millisecond that camera must wait to be on after the line scan light is on.
   * @throws TINIHardwareCommandInvalidException
   * @throws TINIHardwareCommandFailedException
   * @author Eugene Kim-Leighton
   */
  int getDelayInMillisBetweenLightAndCamera() throws TINIHardwareCommandFailedException, TINIHardwareCommandInvalidException
  {
    int delay = 0;
    if (isSimulationModeOn() == false)
    {
      int FPGAAddress = 0x800000;
      delay = _TINIClient.readAddress(FPGAAddress);
    }
    else
      System.out.println("LineScanController.getDelayInMillisBetweenLightAndCamera.stub");

    return delay;
  }

  /**
   * Set the camera brightness (gain) level.  This sets the line scan camera's brightness level which is
   * essentially the gain.  Note that each camera's gain can be set individually.  However, in normal
   * operation we will set all cameras to the same gain.
   *
   * @throws TINIHardwareCommandInvalidException
   * @throws TINIHardwareCommandFailedException
   * @author Eugene Kim-Leighton
   */
  void setCameraBrightnessLevel(LineScanCameraEnum camera, LineScanCameraBrightnessLevelEnum brightnessLevel)
       throws TINIHardwareCommandFailedException, TINIHardwareCommandInvalidException
  {
    Assert.assert(camera != null);
    Assert.assert(brightnessLevel != null);

    if (isSimulationModeOn() == false)
    {
      int intGain = 0;
      int FPGAAddress = 0;
      if (LineScanCameraEnum.TOP_FRONT.equals(camera))
        FPGAAddress = 0x800000;
      else if (LineScanCameraEnum.TOP_REAR.equals(camera))
        FPGAAddress = 0x800000;
      else if (LineScanCameraEnum.BOTTOM_FRONT.equals(camera))
        FPGAAddress = 0x800000;
      else if (LineScanCameraEnum.BOTTOM_REAR.equals(camera))
        FPGAAddress = 0x800000;
      else
        Assert.assert(false);

      if (LineScanCameraBrightnessLevelEnum.LOW_BRIGHTNESS.equals(brightnessLevel))
        intGain = 1;
      else if (LineScanCameraBrightnessLevelEnum.MEDIUM_BRIGHTNESS.equals(brightnessLevel))
        intGain = 2;
      else if (LineScanCameraBrightnessLevelEnum.MEDIUM_HIGH_BRIGHTNESS.equals(brightnessLevel))
        intGain = 3;
      else if (LineScanCameraBrightnessLevelEnum.HIGH_BRIGHTNESS.equals(brightnessLevel))
        intGain = 0;
      else
        Assert.assert(false);

      _TINIClient.writeAddress(FPGAAddress, intGain);
    }
  }

  /**
   * Get the camera brightness (gain) level.  This gets the line scan camera brightness level which is
   * essentially the gain.  Note that each camera's gain can be set individually.  However, in normal
   * operation we will set all cameras to the same gain.
   *
   * @param camera is the enumeration value that indicates which individual camara to control.
   * @return the brightness value of a camera.
   * @throws TINIHardwareCommandInvalidException
   * @throws TINIHardwareCommandFailedException
   * @author Eugene Kim-Leighton
   */
  LineScanCameraBrightnessLevelEnum getCameraBrightnessLevel(LineScanCameraEnum camera)
                                    throws TINIHardwareCommandFailedException, TINIHardwareCommandInvalidException
  {
    Assert.assert(camera != null);

    int level = 0;
    if (isSimulationModeOn() == false)
    {
      int FPGAAddress = 0;
      if (LineScanCameraEnum.TOP_FRONT.equals(camera))
        FPGAAddress = 0x800000;
      else if (LineScanCameraEnum.TOP_REAR.equals(camera))
        FPGAAddress = 0x800000;
      else if (LineScanCameraEnum.BOTTOM_FRONT.equals(camera))
        FPGAAddress = 0x800000;
      else if (LineScanCameraEnum.BOTTOM_REAR.equals(camera))
        FPGAAddress = 0x800000;
      else
        Assert.assert(false);

      //The client will get a string representation of the returned data
      level = _TINIClient.readAddress(FPGAAddress);
    }
    else
      System.out.println("LineScanController.getCameraBrightnessLevel stub");

    //Given an integer, return the corresponding enumerated type
    return LineScanCameraBrightnessLevelEnum.getEnum(level);
  }

  /**
   * Set the camera image resolution (binning).  This function will set the line scan camera image resolution
   * which is essentially the binning.  Note that each camera's binning can be set individually.  However, in
   * normal operation we will set all cameras to the same binning.
   *
   * @param camera is an individual camera whose resolution value will be set to a new value.
   * @param cameraImageResolution is a new reolution value to set.
   * @throws TINIHardwareCommandInvalidException
   * @throws TINIHardwareCommandFailedException
   * @author Eugene Kim-Leighton
   */
  void setCameraImageResolution(LineScanCameraEnum camera, LineScanCameraResolutionEnum cameraImageResolution)
       throws TINIHardwareCommandFailedException, TINIHardwareCommandInvalidException
  {
    Assert.assert(camera != null);
    Assert.assert(cameraImageResolution != null);

    if (isSimulationModeOn() == false)
    {
      int intCameraImageResolution = -1;
      int FPGAAddress = 0;
      if (LineScanCameraEnum.TOP_FRONT.equals(camera))
        FPGAAddress = 0x800000;
      else if (LineScanCameraEnum.TOP_REAR.equals(camera))
        FPGAAddress = 0x800000;
      else if (LineScanCameraEnum.BOTTOM_FRONT.equals(camera))
        FPGAAddress = 0x800000;
      else if (LineScanCameraEnum.BOTTOM_REAR.equals(camera))
        FPGAAddress = 0x800000;
      else
        Assert.assert(false);

      if(LineScanCameraResolutionEnum.PIXELS_1x1024.equals(cameraImageResolution))
        intCameraImageResolution = 0;
      else if(LineScanCameraResolutionEnum.PIXELS_1x2048.equals(cameraImageResolution))
        intCameraImageResolution = 1;
      else
        Assert.assert(false);

      _TINIClient.writeAddress(FPGAAddress, intCameraImageResolution);
    }
  }

  /**
   * Get the camera image resolution (binning).  This function will get the line scan camera image resolution
   * which is essentially the binning.  Note that each camera's binning can be set individually.  However, in
   * normal operation we will set all cameras to the same binning.
   *
   * @param camera is the camera that we want get the resoultion value from.
   * @return resolution value
   * @throws TINIHardwareCommandInvalidException
   * @throws TINIHardwareCommandFailedException
   * @author Eugene Kim-Leighton
   */
  LineScanCameraResolutionEnum getCameraImageResolution(LineScanCameraEnum camera)
                               throws TINIHardwareCommandFailedException, TINIHardwareCommandInvalidException
  {
    Assert.assert(camera != null);

    int resolution = 0;
    if (isSimulationModeOn() == false)
    {
      int FPGAAddress = 0;
      if (LineScanCameraEnum.TOP_FRONT.equals(camera))
        FPGAAddress = 0x800000;
      else if (LineScanCameraEnum.TOP_REAR.equals(camera))
        FPGAAddress = 0x800000;
      else if (LineScanCameraEnum.BOTTOM_FRONT.equals(camera))
        FPGAAddress = 0x800000;
      else if (LineScanCameraEnum.BOTTOM_REAR.equals(camera))
        FPGAAddress = 0x800000;
      else
        Assert.assert(false);

      //The client will get a string representation of the returned data
      resolution = _TINIClient.readAddress(FPGAAddress);
    }
    else
      System.out.println("LineScanController.getCameraImageResolution stub");

    //Given an integer, return the corresponding enumerated type
    return LineScanCameraResolutionEnum.getEnum(resolution);
  }
}
