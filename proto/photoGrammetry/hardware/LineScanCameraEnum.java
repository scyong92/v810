package com.agilent.xRayTest.hardware;

import com.agilent.util.*;

public class LineScanCameraEnum extends Enum
{
  private static int _index = -1;
  public static final LineScanCameraEnum TOP_FRONT    = new LineScanCameraEnum(++_index);
  public static final LineScanCameraEnum TOP_REAR     = new LineScanCameraEnum(++_index);
  public static final LineScanCameraEnum BOTTOM_FRONT = new LineScanCameraEnum(++_index);
  public static final LineScanCameraEnum BOTTOM_REAR  = new LineScanCameraEnum(++_index);

  private LineScanCameraEnum(int camera)
  {
    super(camera);
  }
}
