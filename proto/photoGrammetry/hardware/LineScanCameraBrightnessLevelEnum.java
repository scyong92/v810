package com.agilent.xRayTest.hardware;

import java.io.*;
import java.util.*;

import com.agilent.util.*;

/**
 * This class contains all the brightness (gain) levels possible gain settings for the line scan camera.
 * @author Bill Darbie
 */
public class LineScanCameraBrightnessLevelEnum extends Enum
{
  private static Map _intToEnumMap = new HashMap();
  private static int _index = -1;
  public static final LineScanCameraBrightnessLevelEnum LOW_BRIGHTNESS = new LineScanCameraBrightnessLevelEnum(++_index);
  public static final LineScanCameraBrightnessLevelEnum MEDIUM_BRIGHTNESS = new LineScanCameraBrightnessLevelEnum(++_index);
  public static final LineScanCameraBrightnessLevelEnum MEDIUM_HIGH_BRIGHTNESS = new LineScanCameraBrightnessLevelEnum(++_index);
  public static final LineScanCameraBrightnessLevelEnum HIGH_BRIGHTNESS = new LineScanCameraBrightnessLevelEnum(++_index);



  /**
   * @author Bill Darbie
   */
  private LineScanCameraBrightnessLevelEnum(int brightnessLevel)
  {
    super(brightnessLevel);

    if(this == null)
      System.out.println("LineScanCameraBrightnessEnum - this = null");

    //Set up a map that can convert an integer value to an enumerated type
    _intToEnumMap.put(new Integer(brightnessLevel), this);
  }

  /**
   * Given a legacy integer value, return the corresponding enumerated type.
   *
   * This function will be used from NativeLineScanCameraPair.getBrightnessLevel().
   * The function will first get the integer brightness level (gain) value from the native
   * interface.  The integer will then be converted to the appropriate enumerated type
   * passed up hardware and business layers.
   *
   * @param legacyEnumValue - An input that maps to the desired enumerated type
   * @return A LineScanCameraBrightnessLevelEnum that maps to the input integer value
   * @author Bill Darbie
   */
  public static LineScanCameraBrightnessLevelEnum getEnum(int legacyEnumValue)
  {
    LineScanCameraBrightnessLevelEnum enum = (LineScanCameraBrightnessLevelEnum)_intToEnumMap.get(new Integer(legacyEnumValue));
    Assert.assert(enum != null);

    return enum;
  }

  /**
   * @return the integer value that the legacy code uses for the camera brightness level.
   * @author Greg Esparza
   */
  public int getLegacyBrightnessLevel()
  {
    return _id;
  }
}
