package com.agilent.xRayTest.hardware;

import java.util.*;
import com.agilent.xRayTest.datastore.panelSettings.*;
import com.agilent.util.*;

/**
 * This class provides high level interface for any client to acquire line scan camera images
 *
 * @author Bill Darbie
 */
class LineScanSubsystem
{
  private static LineScanSubsystem _instance     = null;
  private LineScanCameras _lineScanCameras       = null;
  private PanelPositioner _panelPositioner       = null;
  private LineScanLights _lineScanLights         = null;
  private LineScanController _lineScanController = null;
  private LineScanCameraAcquisitionProperties _acquisitionProperties = null;
  private LineScanImageObservable _lineScanImageObservable = null;

  /**
   * Constructor
   *
   * @author Greg Esparza
   */
  private LineScanSubsystem()
  {
    _panelPositioner       = PanelPositioner.getInstance();
    _lineScanLights        = LineScanLights.getInstance();
    _lineScanController    = LineScanController.getInstance();
    _lineScanCameras       = LineScanCameras.getInstance();
    _acquisitionProperties = _lineScanCameras.getImageAcquisitionProperties();
    _lineScanImageObservable = LineScanImageObservable.getInstance();
  }

  /**
   * Get the one and only instance of a LineScanSubsystem object
   *
   * @return A LineScanSubsystem object
   * @author Greg Esparza
   */
  public static synchronized LineScanSubsystem getInstance()
  {
    if (_instance == null)
      _instance = new LineScanSubsystem();

    return _instance;
  }

  /**
   * Set the X-axis coordinate range that will cause the line scan cameras to snap
   * and the light system to strobe.  The coordinate range is defined in nanometers
   * and evenutally get converted into encoder counts.  The encoder output signal
   * will cause the hardware to trigger any time the Stage moves within these two coordinates.
   *
   * @param minXScanRangeInNanometers - An input that defines the minimum X-axis coordinate that will cause the hardware to trigger
   *        maxXScanRangeInNanometers - An input that defines the maximum X-axis coordinate that will cause the hardware to trigger
   *
   * @author Greg Esparza
   */
  void setCameraTriggerRange(int minXScanRangeInNanometers, int maxXScanRangeInNanometers) throws HardwareException
  {
      _panelPositioner.setLineScanTriggerRange(minXScanRangeInNanometers, maxXScanRangeInNanometers);
  }

  /**
  * Set the light intensity percentage
  *
  * This function will allow the client to adjust the light intensity to a desired
  * value.  The value is described as a percentage of the maximum intensity.  The
  * valid range is 1% to 100% in 1% increments.
  *
  * Note:  According to Kendall, the hardware might be able to control (3) knobs:
  *  - light intensity
  *  - camera gain
  *  - shutter control
  *
  * to handle an overall brightness control.  As a result, the hardware layer
  * could expose a "setBrightnessLevel()" function that takes a percentage value
  * and the hardware will do rest.
  *
  * @param lightIntensity - An input defining the percentage intensity
  * @author Greg Esparza
  */
  void setLightIntensity(int lightIntensity) throws HardwareException
  {
      _lineScanLights.setLightIntensity(lightIntensity);
  }

  void setCameraBrightnessLevel(LineScanCameraBrightnessLevelEnum brightnessLevel) throws HardwareException
  {
    _lineScanController.setCameraBrightnessLevel(LineScanCameraEnum.TOP_FRONT, brightnessLevel);
    _lineScanController.setCameraBrightnessLevel(LineScanCameraEnum.TOP_REAR, brightnessLevel);
    _lineScanController.setCameraBrightnessLevel(LineScanCameraEnum.BOTTOM_FRONT, brightnessLevel);
    _lineScanController.setCameraBrightnessLevel(LineScanCameraEnum.BOTTOM_REAR, brightnessLevel);
  }

  /**
  * Execute a scan
  *
  * This function will execute a scan that will generate one image strip per camera.
  * Once the scan is complete, the images from all cameras will be returned to the
  * client.  At this point the client can work with the image data specific to their needs.
  *
  * Note that the bottom side cameras will not exist for system upgrades.  As a result,
  * it is necessary to check if the bottom side cameras are installed.
  *
  * @param stageSpeed               - An input defining the speed to move the Stage
  *        xEndPositionInNanometers - An input defining the X-axis coordinate the Stage must move to so a scan can be generated
  *        topFrontImage            - An input/output that will receive the top front camera's image data
  *        topRearImage             - An input/output that will receive the top rear camera's image data
  *        bottomFrontCamera        - An input/output that will receive the bottom front camera's image data
  *        bottomRearCamera         - An input/output that will receive the bottom rear camera's image data
  * @author Greg Esparza
  */
  void scan(StageSpeed stageSpeed,
            int xEndPositionInNanometers,
            LineScanCameraImage topFrontImage,
            LineScanCameraImage topRearImage,
            LineScanCameraImage bottomFrontImage,
            LineScanCameraImage bottomRearImage) throws HardwareException
  {
    //By default, the top pair must be instantiated
    Assert.assert(topFrontImage != null && topRearImage != null,
                  "LineScanSubsystem.scan() - One or both TOP image pair images are null");

    if(bottomFrontImage != null)
    {
      //If the bottom pair are not installed, then stop here
      Assert.assert(LineScanCameraPairInstalledEnum.TOP_AND_BOTTOM_PAIR_INSTALLED.equals(_acquisitionProperties.getCameraPairInstalled()),
                    "LineScanSubsystem.scan() - Only TOP camera pair are installed but BOTTOM images are not NULL");
      //If the bottom front image class has been instantiated, then the bottom rear must also be instantiated
      Assert.assert(bottomRearImage != null,
                    "LineScanSubsystem.scan() - The BOTTOM REAR image is NULL but the BOTTOM FRONT image is instatiated");
    }

     //Set up the desired Stage speed
     _panelPositioner.setMoveProfile(stageSpeed);

    //Enable the line scan control board
    _lineScanController.enableLineScanCameraTrigger();

    //Enable the line scan cameras for image acquisition
    _lineScanCameras.beginImageAcquisition();

    //The Stage is already at the appropriate start position in the following ways:
    //
    //1) Pipelined Architecture
    //   - MovePanelCommand did the move
    //2) Non-piplelined Architecture
    //   - A client of this object made a previous call to the PanelPositioner.move(x, y, z);

    //Now, move to the end X-axis position.  This will cause the hardware to trigger
    _panelPositioner.setXpositionInNanoMeters(xEndPositionInNanometers);
    _panelPositioner.move();

    //Notify that the top side line scan cameras' image acquisition are done
    _lineScanCameras.endImageAcquisition(topFrontImage, topRearImage, bottomFrontImage, bottomRearImage);

    //Send the images over to the image buffer via the observable object
    _lineScanImageObservable.addImages(topFrontImage, topRearImage, bottomFrontImage, bottomRearImage);
  }
}
