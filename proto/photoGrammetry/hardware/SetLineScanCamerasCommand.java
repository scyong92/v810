package com.agilent.xRayTest.hardware;

import com.agilent.util.*;

/**
* This class sets up the Line Scan camera hardware so that it is ready
* to scan images.  The range of X in which the hardware triggers the
* line scan cameras is stored here.
*
* @author Bill Darbie
*/
public class SetLineScanCamerasCommand extends Command
{
  private static LineScanSubsystem _lineScanSubsystem = null;

  private int _minXScanRangeInNanometers                     = -1;
  private int _maxXScanRangeInNanometers                     = -1;
  private int _lightIntensityPercentage                      = 0;
  private LineScanCameraBrightnessLevelEnum _brightnessLevel = null;

  /**
   * Static declarations
   *
   * @author Greg Esparza
   */
  static
  {
    _lineScanSubsystem = LineScanSubsystem.getInstance();
  }

  /**
   * Constructor
   *
   * @param minXScanRangeInNanometers - An integer defining the minimum Stage X-axis position to
   *                                    trigger the line scan subsystem's image acquisition hardware (cameras, lights)
   * @param maxXScanRangeInNanometers - An integer defining the maximum Stage X-axis position to
   *                                    trigger the line scan subsystem's image acquisition hardware (cameras, lights)
   * @param lightIntensityPercentage  - An integer defining the percentage light intensity required for the next acquisition
   * @param brightnessLevel           - A LineScanCameraBrightnessLevelEnum object defining the required (camera gain) for the next acquisition
   *
   * @author Greg Esparza
   */
  public SetLineScanCamerasCommand(int minXScanRangeInNanometers,
                                   int maxXScanRangeInNanometers,
                                   int lightIntensityPercentage,
                                   LineScanCameraBrightnessLevelEnum brightnessLevel)
  {
    Assert.assert(minXScanRangeInNanometers != -1);
    Assert.assert(maxXScanRangeInNanometers != -1);
    Assert.assert(lightIntensityPercentage > 0);
    Assert.assert(brightnessLevel != null);

    _minXScanRangeInNanometers = minXScanRangeInNanometers;
    _maxXScanRangeInNanometers = maxXScanRangeInNanometers;
    _lightIntensityPercentage  = lightIntensityPercentage;
    _brightnessLevel           = brightnessLevel;
  }

  public void execute()
  {
    try
    {

    _lineScanSubsystem.setCameraTriggerRange(_minXScanRangeInNanometers, _maxXScanRangeInNanometers);
    _lineScanSubsystem.setLightIntensity(_lightIntensityPercentage);
    }
    catch(HardwareException he)
    {
      /** @todo this code needs to be finished */
      Assert.assert(false);
    }
  }

  /**
   * @author Kristin Casterton
   */
  public void isValid() throws HardwareException
  {
    // do nothing now
    // this command never has information that would be invalid
    // so don't throw an exception
  }
}
