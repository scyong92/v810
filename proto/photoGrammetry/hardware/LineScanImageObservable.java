package com.agilent.xRayTest.hardware;

import java.util.*;

import com.agilent.util.*;

/**
 * This class provides an interface to notify all observers of this object
 * when line scan images are available for use.  This class is designed to follow
 * the Observer/Observable design pattern and allows an object in the HARDWARE layer
 * (i.e. SnapLineScanImageCommand) to notify the BUSINESS layer (i.e. LineScanImageBuffer)
 * when images are available.
 *
 * Typically, this object will only be used in the context of pipeline execution but is
 * certainly not limited to this application.
 *
 * @author Bill Darbie
 */
public class LineScanImageObservable extends ThreadSafeObservable
{
  private static LineScanImageObservable _instance = null;

  /**
   * Constructor
   *
   * @author Bill Darbie
   */
  private LineScanImageObservable()
  {
    // do nothing
  }

  /**
   * Get the one and only instance of the LineScanImageObservable object
   *
   * @return A LineScanImageObservable object
   * @author Bill Darbie
   */
  public static synchronized LineScanImageObservable getInstance()
  {
    if (_instance == null)
      _instance = new LineScanImageObservable();

    return _instance;
  }

  /**
   * Add images to the all observers of this object
   *
   * The LineScanImageBuffer object is one of the observers added to the LineScanImageObservable's list, and is
   * notified every time this "notifyObservers()" function is called.
   *
   * Note: Since the BOTTOM side hardware does not exist on upgrade systems, it is possible
   *       that the BOTTOM pair of images will be passed in as NULL.
   *
   * @param topFrontImage    - A LineScanCameraImage object.  This parameter cannot be NULL.
   *        topRearImage     - A LineScanCameraImage object.  This parameter cannot be NULL.
   *        bottomFrontImage - A LineScanCameraImage object. This parameter can be NULL
   *        bottomRearImage  - A LineScanCameraImage object. This parameter can be NULL
    * @author Bill Darbie
   */
  void addImages(LineScanCameraImage topFrontImage,
                 LineScanCameraImage topRearImage,
                 LineScanCameraImage bottomFrontImage,
                 LineScanCameraImage bottomRearImage)
  {
    //Check for TOP image mismatch combinations.  The image data should not be in this state when passed into this function.
    if(topFrontImage != null)
      Assert.assert(topRearImage != null,
                    "LineScanImageObservable.addImages() - The TOP FRONT image was not NULL but the TOP REAR image was NULL");
    if(topRearImage != null)
      Assert.assert(topFrontImage != null,
                    "LineScanImageObservable.addImages() - The TOP REAR image was not NULL but the TOP FRONT image was NULL");

    //Check for BOTTOM image mismatch combinations.  The image data should not be in this state when passed into this function.
    if(bottomFrontImage != null)
      Assert.assert(bottomRearImage != null,
                    "LineScanImageObservable.addImages() - The BOTTOM FRONT image was not NULL but the BOTTOM REAR image was NULL");
    if(bottomRearImage != null)
      Assert.assert(bottomFrontImage != null,
                    "LineScanImageObservable.addImages() - The BOTTOM REAR image was not NULL but the BOTTOM FRONT image was NULL");

    List images = new ArrayList();

    //Always add the TOP side images since the TOP side hardware will always installed
    images.add(topFrontImage);
    images.add(topRearImage);
    //Since the BOTTOM side hardware is does not exist on upgraded systems, it is
    //possible that the BOTTOM pair of images will be passed in as NULL.  As a result,
    //check if, at least, the BOTTOM FRONT image is NULL before attempting to add to
    //the list.
    if((bottomFrontImage != null) && (bottomRearImage != null))
    {
      images.add(bottomFrontImage);
      images.add(bottomRearImage);
    }

    setChanged();
    //Notify all observers that images have been added.
    //This function will call all the observer's "update()" method as a notification.
     notifyObservers(images);
  }
}
