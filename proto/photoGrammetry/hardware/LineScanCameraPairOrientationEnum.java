package com.agilent.xRayTest.hardware;

import com.agilent.util.*;

/**
 * This class describes how the a line scan camera pair is oriented on either
 * the TOP or BOTTOM of the system.  When a camera pair is instantiated we will
 * use this enumeration to define a TOP and BOTTOM instance.
 *
 * @author Greg Esparza
 */
public class LineScanCameraPairOrientationEnum extends Enum
{
  private static int _index = -1;
  public static final LineScanCameraPairOrientationEnum TOP_PAIR    = new LineScanCameraPairOrientationEnum(++_index);
  public static final LineScanCameraPairOrientationEnum BOTTOM_PAIR = new LineScanCameraPairOrientationEnum(++_index);

  private LineScanCameraPairOrientationEnum(int cameraPairOrientation)
  {
    super(cameraPairOrientation);
  }

  /**
   * Get the integer representation for the enumerated type
   *
   * @return the integer value that the legacy code uses for the camera.
   * @author Greg Esparza
   */
  public int getLegacyCameraPairOrientation()
  {
    return _id;
  }

}
