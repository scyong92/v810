#include <iostream>
#include "thirdParty/boulderImaging/src/Image.h"
#include "com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameraImage.h"

using namespace boulderImaging;
using namespace std;

/*
 * Class:     com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameraImage
 * Method:    nativeGetCopyOfImage
 * Signature: (I[B)V
 */
JNIEXPORT void JNICALL Java_com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameraImage_nativeGetCopyOfImage
  (JNIEnv *pEnv, 
   jobject obj,
   jint imageWidthInPixels,
   jint imageLengthInPixels,
   jint imagePointer,
   jbyteArray imageArray )
{
  char*  pImageCharArray     = NULL;
  jint numberOfArrayElements = 0;

  cout << endl;
  cout << "Entering JNI function \"getCopyOfImage()\" ..." << endl;

  
  char* pPixelArray = reinterpret_cast<char*>(imagePointer);
  cout << "JNI nativeGetCopyOfImage() - image address location = " << reinterpret_cast<int>(pPixelArray) << endl;
  

  //Get the number of byte array elements we need to copy from the C++ array to the javaByteArray
  numberOfArrayElements = (imageWidthInPixels * imageLengthInPixels); 
  cout << "JNI nativeGetCopyOfImage() - image number of elements to copy = " << numberOfArrayElements << endl;

  //Now, copy the C++ byte array (pImageCharArray) into the javaByteArray (frontImageArray)
  pEnv->SetByteArrayRegion(imageArray, 0, numberOfArrayElements, reinterpret_cast<signed char*>(pPixelArray)); 
  pPixelArray = NULL;
}

