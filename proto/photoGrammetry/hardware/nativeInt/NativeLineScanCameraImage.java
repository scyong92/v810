package com.agilent.xRayTest.hardware.nativeInt;

import com.agilent.xRayTest.hardware.*;
//import com.agilent.util.*;


public class NativeLineScanCameraImage
{
  public NativeLineScanCameraImage()
  {

  }


  /**
   * Get a copy of the image data
   *
   * @param imagePointer - An input that defines the address location to get the image data.
   *        pixelArray   - An input/output byte array that will return the image data.
   * @throws HardwareException
   * @author Greg Esparza
   */
  public void getCopyOfImage(int widthInPixels, int lengthInPixels, int imagePointer, byte[] pixelArray) throws HardwareException
  {
    nativeGetCopyOfImage(widthInPixels, lengthInPixels, imagePointer, pixelArray);
  }



  //Native functions
  private native void nativeGetCopyOfImage(int widthInPixels, int lengthInPixels, int imagePointer, byte[] pixelArray);

}

