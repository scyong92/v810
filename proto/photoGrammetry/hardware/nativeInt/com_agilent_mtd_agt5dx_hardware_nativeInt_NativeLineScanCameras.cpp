#include <iostream>
#include "thirdParty/boulderImaging/src/LineScanCameras.h"
#include "thirdParty/boulderImaging/src/LineScanCameraProperties.h"
#include "thirdParty/boulderImaging/src/LineScanCameraAcquisitionProperties.h"
#include "com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras.h"
#include "util.h"
#include "util/src/sptAssert.h"

using namespace boulderImaging;
using namespace std;


static LineScanCameras* pLineScanCameras = LineScanCameras::getInstance();


/*
 * Class:     com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras
 * Method:    nativePrepareForImageAcquisition
 * Signature: (IIIIIIZZZZ)V
 */
JNIEXPORT void JNICALL Java_com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras_nativePrepareForImageAcquisition
  (JNIEnv *, 
   jobject, 
   jint cameraType, 
   jint cameraPairInstalled, 
   jint imageResolution, 
   jint imageWidthInPixels, 
   jint imageLengthInPixels, 
   jint numberOfImageBuffers,
   jboolean topFrontCameraHorizontalImageFlipEnable, 
   jboolean topRearCameraHorizontalImageFlipEnable, 
   jboolean bottomFrontCameraHorizontalImageFlipEnable,
   jboolean bottomRearCameraHorizontalImageFlipEnable)
{

  LineScanCameraAcquisitionProperties* pProperties = LineScanCameraAcquisitionProperties::getInstance();
    
  pProperties->setCameraType(static_cast<LineScanCameraProperties::LineScanCameraType>(cameraType)); 
  pProperties->setCameraPairInstalled(static_cast<LineScanCameraProperties::CameraPairInstalled>(cameraPairInstalled)); 
  pProperties->setImageResolution(static_cast<LineScanCameraProperties::ImageResolution>(imageResolution)); 
  pProperties->setImageWidthInPixels(imageWidthInPixels); 
  pProperties->setImageLengthInPixels(imageLengthInPixels); 
  pProperties->setNumberOfImageTransferBuffers(numberOfImageBuffers);
  pProperties->setTopFrontCameraHorizontalImageFlipEnable(topFrontCameraHorizontalImageFlipEnable); 
  pProperties->setTopRearCameraHorizontalImageFlipEnable(topRearCameraHorizontalImageFlipEnable);
  pProperties->setBottomFrontCameraHorizontalImageFlipEnable(bottomFrontCameraHorizontalImageFlipEnable);
  pProperties->setBottomRearCameraHorizontalImageFlipEnable(bottomRearCameraHorizontalImageFlipEnable);

  pLineScanCameras->prepareForImageAcquisition(*pProperties);
  
  LineScanCameraAcquisitionProperties::removeInstance(pProperties);

}

/*
 * Class:     com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras
 * Method:    nativeCleanUpImageAcquisitionResources
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras_nativeCleanUpImageAcquisitionResources
  (JNIEnv *, jobject)
{
  cout << endl;
  cout << "Entering JNI function \"nativeCleanUpImageAcquisitionResources()\" ..." << endl;

  pLineScanCameras->cleanUpImageAcquisitionResources();

}

/*
 * Class:     com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras
 * Method:    nativeGetImageAcquisitionProperties
 * Signature: (Lcom/agilent/mtd/util/IntegerRef;Lcom/agilent/mtd/util/IntegerRef;Lcom/agilent/mtd/util/IntegerRef;Lcom/agilent/mtd/util/IntegerRef;Lcom/agilent/mtd/util/IntegerRef;Lcom/agilent/mtd/util/IntegerRef;Lcom/agilent/mtd/util/BooleanRef;Lcom/agilent/mtd/util/BooleanRef;Lcom/agilent/mtd/util/BooleanRef;Lcom/agilent/mtd/util/BooleanRef;)V
 */
JNIEXPORT void JNICALL Java_com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras_nativeGetImageAcquisitionProperties
  (JNIEnv *pEnv, 
   jobject obj, 
   jobject cameraType, 
   jobject cameraPairInstalled, 
   jobject imageResolution, 
   jobject numberOfImageTransferBuffers, 
   jobject imageWidthInPixels, 
   jobject imageLengthInPixels, 
   jobject topFrontCameraHorizontalImageFlipEnable, 
   jobject topRearCameraHorizontalImageFlipEnable, 
   jobject bottomFrontCameraHorizontalImageFlipEnable, 
   jobject bottomRearCameraHorizontalImageFlipEnable)
{
  cout << endl;
  cout << "Entering JNI function \"nativeGetImageAcquisitionProperties()\" ..." << endl;

  LineScanCameraAcquisitionProperties* pProperties = LineScanCameraAcquisitionProperties::getInstance();

  pLineScanCameras->getImageAcquisitionProperties(*pProperties);
  
  
  integerRefSetValue(pEnv, obj, cameraType,                                 static_cast<LineScanCameraProperties::LineScanCameraType>(pProperties->getCameraType()));
  integerRefSetValue(pEnv, obj, cameraPairInstalled,                        static_cast<LineScanCameraProperties::CameraPairInstalled>(pProperties->getCameraPairInstalled()));
  integerRefSetValue(pEnv, obj, imageResolution,                            static_cast<LineScanCameraProperties::ImageResolution>(pProperties->getImageResolution()));
  integerRefSetValue(pEnv, obj, numberOfImageTransferBuffers,               pProperties->getNumberOfImageTransferBuffers());
  integerRefSetValue(pEnv, obj, imageWidthInPixels,                         pProperties->getImageWidthInPixels());
  integerRefSetValue(pEnv, obj, imageLengthInPixels,                        pProperties->getImageLengthInPixels());
  booleanRefSetValue(pEnv, obj, topFrontCameraHorizontalImageFlipEnable,    pProperties->getTopFrontCameraHorizontalImageFlipEnable());
  booleanRefSetValue(pEnv, obj, topRearCameraHorizontalImageFlipEnable,     pProperties->getTopRearCameraHorizontalImageFlipEnable());
  booleanRefSetValue(pEnv, obj, bottomFrontCameraHorizontalImageFlipEnable, pProperties->getBottomFrontCameraHorizontalImageFlipEnable());
  booleanRefSetValue(pEnv, obj, bottomRearCameraHorizontalImageFlipEnable,  pProperties->getBottomRearCameraHorizontalImageFlipEnable());

  LineScanCameraAcquisitionProperties::removeInstance(pProperties);

}

/*
 * Class:     com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras
 * Method:    nativeBeginImageAcquisition
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras_nativeBeginImageAcquisition
  (JNIEnv *, jobject)
{
  cout << endl;
  cout << "Entering JNI function \"nativeBeginImageAcquisition()\" ..." << endl;

  pLineScanCameras->beginImageAcquisition();
}


/*
 * Class:     com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras
 * Method:    nativeEndImageAcquisition
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras_nativeEndImageAcquisition
  (JNIEnv *pEnv, 
   jobject obj, 
   jobject widthInPixels,
   jobject lengthInPixels, 
   jobject topFrontImagePointer, 
   jobject topRearImagePointer, 
   jobject bottomFrontImagePointer, 
   jobject bottomRearImagePointer)
{
  cout << endl;
  cout << "Entering JNI function \"nativeEndImageAcquisition()\" ..." << endl;


  Image* pTopFrontImage    = Image::getInstance();
  Image* pTopRearImage     = Image::getInstance();
  Image* pBottomFrontImage = Image::getInstance();
  Image* pBottomRearImage  = Image::getInstance();
  int imageAddressLocation = 0;

  pLineScanCameras->endImageAcquisition(*pTopFrontImage, *pTopRearImage, *pBottomFrontImage, *pBottomRearImage);

  //Convert the front boulderImaging::Image object pointer to an integer that represents the address location 
  imageAddressLocation = reinterpret_cast<int>(pTopFrontImage->get()); 
  cout << "JNI nativeEndImageAcquisition() - top front image address = " << imageAddressLocation << endl;
  //Now, put the integer address representation into the topFrontImagePointer object
  integerRefSetValue(pEnv, obj, topFrontImagePointer, imageAddressLocation);

  //Convert the rear boulderImaging::Image object pointer to an integer that represents the address location 
  imageAddressLocation = reinterpret_cast<int>(pTopRearImage->get()); 
  cout << "JNI nativeEndImageAcquisition() - rear image address = " << imageAddressLocation << endl;
  //Now, put the integer address representation into the topRearImagePointer object
  integerRefSetValue(pEnv, obj, topRearImagePointer, imageAddressLocation);

  //Convert the front boulderImaging::Image object pointer to an integer that represents the address location 
  imageAddressLocation = reinterpret_cast<int>(pBottomFrontImage->get()); 
  cout << "JNI nativeEndImageAcquisition() - top front image address = " << imageAddressLocation << endl;
  //Now, put the integer address representation into the bottomFrontImagePointer object
  integerRefSetValue(pEnv, obj, bottomFrontImagePointer, imageAddressLocation);

  //Convert the rear boulderImaging::Image object pointer to an integer that represents the address location 
  imageAddressLocation = reinterpret_cast<int>(pBottomRearImage->get()); 
  cout << "JNI nativeEndImageAcquisition() - rear image address = " << imageAddressLocation << endl;
  //Now, put the integer address representation into the bottomRearImagePointer object
  integerRefSetValue(pEnv, obj, bottomRearImagePointer, imageAddressLocation);


  Image* pTempImage = NULL;
  //The system either generates a TOP pair, BOTTOM pair, or both.
  //Since we need to get the image dimension, we can use the first valid Image object's information.
  //The image object will either contain a valid pointer to the image data in memory or be NULL.
  //In this case, we will use the FRONT image.
  if(pTopFrontImage->get() != NULL)
    pTempImage = pTopFrontImage;
  else
    pTempImage = pBottomFrontImage;

  integerRefSetValue(pEnv, obj, widthInPixels, pTempImage->getWidthInPixels());
  integerRefSetValue(pEnv, obj, lengthInPixels, pTempImage->getLengthInPixels());

  pTempImage = NULL;

  Image::removeInstance(pTopFrontImage);
  Image::removeInstance(pTopRearImage);
  Image::removeInstance(pBottomFrontImage);
  Image::removeInstance(pBottomRearImage);
}

/*
 * Class:     com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras
 * Method:    nativeAbortImageAcquisition
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras_nativeAbortImageAcquisition
  (JNIEnv *, jobject)
{
  cout << endl;
  cout << "Entering JNI function \"nativeAbortImageAcquisition()\" ..." << endl;

  //Memory allocated at "beginImageAcquisition()" must be deleted here

  pLineScanCameras->abortImageAcquisition();
}


/*
 * Class:     com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras
 * Method:    nativeSetVerticalImageFlipEnable
 * Signature: (ZZ)V
 */
JNIEXPORT void JNICALL Java_com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras_nativeSetVerticalImageFlipEnable
  (JNIEnv *, 
   jobject, 
   jboolean topFrontCameraEnable, 
   jboolean topRearCameraEnable,
   jboolean bottomFrontCameraEnable,
   jboolean bottomRearCameraEnable)
{
  cout << endl;
  cout << "Entering JNI function \"nativeVerticalImageFlipEnable()\" ..." << endl;

  pLineScanCameras->setVerticalImageFlipEnable(topFrontCameraEnable, topRearCameraEnable, bottomFrontCameraEnable, bottomRearCameraEnable);
}

/*
 * Class:     com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras
 * Method:    setFlatFieldReferenceImages
 * Signature: (IIII[B[B)V
 */
JNIEXPORT void JNICALL Java_com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras_nativeSetFlatFieldReferenceImages
  (JNIEnv *pEnv, 
   jobject obj, 
   jint imageWidthInPixels, 
   jint imageLengthInPixels, 
   jbyteArray darkImagePixelArray, 
   jbyteArray brightImagePixelArray)
{
  jint numberOfArrayElements = 0;

  cout << endl;
  cout << "Entering JNI function \"nativeSetFlatFieldReferenceImages()\" ..." << endl;


  cout << "JNI nativeSetFlatFieldReferenceImages() - getting Dark image instance" << endl;
  Image* pDarkImage = Image::getInstance();
  if(pDarkImage == NULL)
    cout << "pDarkImage = NULL" << endl;

  cout << "JNI nativeSetFlatFieldReferenceImages() - getting Bright image instance" << endl;
  Image* pBrightImage = Image::getInstance();
  if(pBrightImage == NULL)
    cout << "pBrightImage = NULL" << endl;

  cout << "JNI nativeSetFlatFieldReferenceImages() - setting Dark image width" << endl; 
  pDarkImage->setWidthInPixels(imageWidthInPixels);
  cout << "JNI nativeSetFlatFieldReferenceImages() - setting Dark image length" << endl;
  pDarkImage->setLengthInPixels(imageLengthInPixels);
  cout << "JNI nativeSetFlatFieldReferenceImages() - setting Bright image width" << endl;
  pBrightImage->setWidthInPixels(imageWidthInPixels);
  cout << "JNI nativeSetFlatFieldReferenceImages() - setting Bright image length" << endl;
  pBrightImage->setLengthInPixels(imageLengthInPixels);

  //Get the number of byte array elements we need to copy from the javaByteAarray to the C++ array
  numberOfArrayElements = imageWidthInPixels * imageLengthInPixels;
  cout << "JNI nativeSetFlatFieldReferenceImages() - image number of elements to copy = " << numberOfArrayElements << endl;
 
  //Allocate memory for the pixel array data to be transferred from a javaByteArray to a C++ byte array
  char* pTempDarkImagePixelArray = new char[numberOfArrayElements];
  char* pTempBrightImagePixelArray = new char[numberOfArrayElements];
  
  //Now, copy the javaByteArray (darkImagePixelArray) into the C++ byte array (pTempDarkImagePixelArray))
  pEnv->GetByteArrayRegion(darkImagePixelArray, 0, numberOfArrayElements, reinterpret_cast<signed char*>(pTempDarkImagePixelArray)); 
  //Now set the Image object pixel array pointer
  pDarkImage->set(pTempDarkImagePixelArray);
  pTempDarkImagePixelArray = NULL;

  //Now, copy the javaByteArray (brightImagePixelArray) into the C++ byte array (pTempBrightImagePixelArray))
  pEnv->GetByteArrayRegion(brightImagePixelArray, 0, numberOfArrayElements, reinterpret_cast<signed char*>(pTempBrightImagePixelArray)); 
  //Now set the Image object pixel array pointer
  pBrightImage->set(pTempBrightImagePixelArray);
  pTempBrightImagePixelArray = NULL;

  pLineScanCameras->setFlatFieldReferenceImages(*pDarkImage, *pBrightImage);

  Image::removeInstance(pDarkImage);
  Image::removeInstance(pBrightImage);
}

/*
 * Class:     com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras
 * Method:    nativeEnableFlatField
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras_nativeEnableFlatField
  (JNIEnv *, jobject)
{
  cout << endl;
  cout << "Entering JNI function \"nativeEnableFlatField()\" ..." << endl;

  pLineScanCameras->enableFlatField();
}

/*
 * Class:     com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras
 * Method:    nativeDisableFlatField
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras_nativeDisableFlatField
  (JNIEnv *, jobject)
{
  cout << endl;
  cout << "Entering JNI function \"nativeDisableFlatField()\" ..." << endl;

  pLineScanCameras->disableFlatField();
}

/*
 * Class:     com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras
 * Method:    nativeIsFlatFieldEnabled
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_com_axi_mtd_agt5dx_hardware_nativeInt_NativeLineScanCameras_nativeIsFlatFieldEnabled
  (JNIEnv *, jobject)
{
  cout << endl;
  cout << "Entering JNI function \"nativeIsFlatFieldEnabled()\" ..." << endl;

  return pLineScanCameras->isFlatFieldEnabled();
}

