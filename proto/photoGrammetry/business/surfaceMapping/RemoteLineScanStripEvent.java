package com.agilent.xRayTest.business.surfaceMapping;
/* Generated by Together */

import java.rmi.*;
import java.rmi.server.*;

import com.agilent.util.*;  // RemoteObserverInt, RemoteObservableInt

class RemoteLineScanStripEvent extends UnicastRemoteObject implements RemoteLineScanStripEventInt
{


  private LineScanStripEvent _lineScanStripEvent;

  RemoteLineScanStripEvent(LineScanStripEvent event) throws RemoteException
  {
    Assert.assert(event != null);
    _lineScanStripEvent = event;
  }

  public int getActiveStrip() throws RemoteException
  {
    Assert.assert(_lineScanStripEvent != null);
    return _lineScanStripEvent.getActiveStrip();
  }

  public int getTotalStrips() throws RemoteException
  {
    Assert.assert(_lineScanStripEvent != null);
    return _lineScanStripEvent.getTotalStrips();
  }

}
