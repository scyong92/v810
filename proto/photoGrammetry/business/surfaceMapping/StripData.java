package com.agilent.xRayTest.business.surfaceMapping;

import java.util.*;


// This calss will be the data for each strip. It will conatin the scan position
// that is associated with this strip. As well as the Hashmaps for the top and the bottom
// PhotoG features
public class StripData
{
  private static int _yScanPositionInNanos;
  private HashMap _topFeatures = new HashMap();
  private HashMap _bottomFeatures = new HashMap();

  public StripData()
  {
  }

  public int getYScanPostion()
  {
    return _yScanPositionInNanos;
  }

  public Map getTopPhotoGFeatures()
  {
    return _topFeatures;
  }

  public Map getBottomPhotoGFeatures()
  {
    return _bottomFeatures;
  }
}