package com.agilent.xRayTest.business.surfaceMapping;

import java.util.*;
import com.agilent.util.*;
import com.agilent.xRayTest.datastore.panelDesc.*;
import com.agilent.xRayTest.business.surfaceMapping.*;


public class PhotoGSurfaceMapSetup
{
  // All X's and Y's referred to in this class are in machine coordinates
  private Board _board;
  private Panel _loadedPanel; // need to figure out when we will give this member value.
  private SideBoard _topSideBoard = null;
  private SideBoard _bottomSideBoard = null;

  //ROI datamembers
  private DoubleRectangle _roiBoundingRectangle = null;
  private List _surfaceMapFeatures; // TO DO: this will need to be figured out.
  // TODO: we will also need to implement the ROILineScanCameraImage and RegionOfInterest classes.

  // xray map points related datamembers
  private List _topSideXrayMapPoints;
  private List _bottomSideXrayMapPoints;

  //line scan information datamembers
  private final int _panelWidthExtensionforLineScanInNanos = 1000; // todo: find out the value
  private final int _panelLengthExtensionforLineScanInNanos = 1000;  // todo: find out the value
  private final int _yStripOverlapInNanos = 100;  //todo: find out real overlap
  private static int _lineScanWidthInNanos;  // todo: get CCD width from linescan camera

  // These values are actual scan stage positions
  private int _xMinScanPositionInNanos;
  private int _xMaxScanPositionInNanos;

  // The following structure will hold all information needed for each strip. Along with the two
  // haash maps for top and bottom PhotoGFeatures. See StripData comments for detailed information.
  private StripData[] _photoGFeaturesByStrip;

  // board coordinates
  private DoubleCoordinate _bottomLeftMostCoordOfBoard;
  private DoubleCoordinate _bottomRightMostCoordOfBoard;
  private DoubleCoordinate _topLeftMostCoordOfBoard;
  private DoubleCoordinate _topRightMostCoordOfBoard;

  static
  {
     //_lineScanWidthInNanos = get from hardware layer
  }

  public PhotoGSurfaceMapSetup(Board board)
  {
    // since the constructor will be called before learning alot of the data needed will not
    // be available. So at this stage the constructor will setup all array lists and it will start to
    // calculate the strip locations from cad.
    Assert.assert(board != null);
    _board = board;

    _topSideXrayMapPoints = new ArrayList();
    _bottomSideXrayMapPoints = new ArrayList();

    if(board.topSideBoardExists())
      _topSideBoard = board.getTopSideBoard();
    if(board.bottomSideBoardExists())
      _bottomSideBoard = board.getBottomSideBoard();

    calculateStripLocationsFromCAD();

    // Tracy's explanation
    // Start with board origin that will give us minBoardX and minBoardY,
    //   add board width to minBoardX to get maxBoardX
    //   add board length to minBoardY to get maxBoardY
    // Convert 4 corners to machine coordinates (this accounts for alignment/rotation)
    //   from these find the smallest and the largest X's and Y's, these are
    //   minX, maxX, minY, maxY.
    // Add the extensions to expand the board based on error, use these to calculate
    //   Y strip locations (the extended values are the X values).

    // Tracy's Questions
    // here we will load the CAD information and calculate all the strip
    //   positions
    // Should this be observing the CAD and updating with the new information
    //   when loaded?
  }

  // Determines on a strip basis the ROIs for each via out of the image strip.
  // It calculates the ROI, extracts it from the strip image, and stores it
  // with the PhotoGSurfaceMapFeature class for that feature.  It is only used
  // during feature identification/learning.
  // todo Bill would like to rename this to something like gatherRegionsOfInterest()
  public void calculateRegionsOfInterest()
  {
    System.out.println("PhotoGSurfaceMapSetup.calculateRegionsOfInterest() stub");
  }

  // this function will calculate the ROI bouding rectangles
  private void createROIBoundingRectangles()
  {
    System.out.println("PhotoGSurfaceMapSetup.createROIBoundingRectangle() stub");
  }

  // Determines how many image strips are required for the board and gives
  // each strip an y offset fromt he PIP postion that the board is loaded
  // against.  This information is calculated from the panelDesc data and
  // stored in SideBoard PhotoGMapData when a board is being learned.
  public void calculateStripLocationsFromCAD()
  {
    System.out.println("PhotoGSurfaceMapSetup.calculateStripLocationsFromCAD() stub");
  }

  // Determines which photoGFeatures reside in each image strip, this
  // is calculated based on the alignment of the loaded panel.  It is
  // calculated for each panel loaded for measurement (surface mapping).
  public void generateSurfaceMapFeaturesInStrips()
  {
    System.out.println("PhotoGSurfaceMapSetup.determineSurfaceMapFeaturesInStrips() stub");
  }

  // Creates the hardware command list for executing a surface map
  // measurement.  It must be able to skip strips if there are no surface
  // map features present in the strip.
  public void createCommandListForMeasurement()
  {
    System.out.println("PhotoGSurfaceMapSetup.createCommandListForMeasurement() stub");
  }

  /**
   * This method calls into the datastore layer to get the alignmentGroup
   * data for the currently loaded panel.  Based on the panel program
   * information, it finds the closest PhotoGSurfaceMapFeatures and
   * passes them back in a List.
   * @return a List of the closest surface map features to...
   */
  private List findClosestSurfaceMapFeatures()  //closest to what?
  {
    System.out.println("PhotoGSurfaceMapSetup.findClosestSurfaceMapFeature() stub");
    List features = new ArrayList();
    return features;
  }

  // this method will iterate through _photoGFeaturesByStrip and return a list of yScanPositions.
  public List getYScanPositionInNanos()
  {
     System.out.println("PhotoGSurfaceMapSetup.getYScanPositionInNanos() stub");
     return null;
  }

  //this method will return the list of the stips where the feature resides.
  public List getStripWhereFeatureResides(IntCoordinate feature)
  {
    System.out.println("PhotoGSurfaceMapSetup.getStripWhereFeatureResides() stub");
    List stripList = new ArrayList();
    return stripList;
  }

  // this method will add the feature to the appropriate strips. If the method
  // succeeds it will return true and false if it does not succeed.
  public boolean addFeatureToStrip(PhotoGSurfaceMapFeature feature, Boolean topSideFlag)
  {
    System.out.println("PhotoGSurfaceMapSetup.addFeatureToStrip() stub");
    return true;
  }

  public int getXMinScanPositionInNanos()
  {
     return _xMinScanPositionInNanos;
  }

  public int getXMaxScanPositionInNanos()
  {
     return _xMaxScanPositionInNanos;
  }

  // returns the number of stips we have calculated. The result will be zero based
  public int getNumberOfStrips()
  {
    System.out.println("PhotoGSurfaceMapSetup.getNumberOfStrips() stub");
    return 0;
  }

  private Map getTopSideFeaturesForStrip(int stripNumber)
  {
    System.out.println("PhotoGSurfaceMapSetup.getTopSideFeaturesForStrip() stub");
    return null;
  }

  private Map getBottomSideFeaturesForStrip(int stripNumber)
  {
    System.out.println("PhotoGSurfaceMapSetup.getBottomSideFeaturesForStrip() stub");
    return null;
  }

  // this function will do the actual work in figuring out the y scan positions.
  private void calculateStripLocations(int yMin, int yMax)
  {
    System.out.println("PhotoGSurfaceMapSetup.calculateStripLocations() stub");
  }

  // this function will calculate the min XY and max XY values
  private void calculateMinAndMaxValues()
  {
    System.out.println("PhotoGSurfaceMapSetup.calculateMinAndMaxValues() stub");
  }

  // this function will setup the 4 bouding coordinates of the board
  private void createBoundingCoordinates()
  {
    System.out.println("PhotoGSurfaceMapSetup.createBoundingCoordinates() stub");

     _bottomLeftMostCoordOfBoard = new DoubleCoordinate();
     _bottomRightMostCoordOfBoard = new DoubleCoordinate();
     _topLeftMostCoordOfBoard = new DoubleCoordinate();
     _topRightMostCoordOfBoard = new DoubleCoordinate();

    // to do: need to implement this function in panel.. see low level design for
    //        calculateStripLocationsFromCAD
    //   _loadedPanel.createCornersOfBoard( _bottomLeftMostCoordOfBoard,
    //                                      _bottomRightMostCoordOfBoard,
    //                                      _topLeftMostCoordOfBoard,
    //                                      _topRightMostCoordOfBoard);

    // once the four rectangles have been created need to convert to machine coords using
    // alignment data.
  }

  // this function will place the surface map feature into its appropriate list.
  // for example, if the feature is an xray map point it will be placed into either
  // _topSideXrayMapPoints or _bottomSideXrayMapPoints. And if the feature is a photo G
  // feature, it will be placed into the apropriate strip.
  private void placeSurfaceMapFeaturesIntoCorrectList(List surfaceMapPoints, Boolean topSideFlag)
  {
    System.out.println("PhotoGSurfaceMapSetup.placeSurfaceMapFeaturesIntoCorrectList(List, Boolean) stub");
  }
}
