package com.agilent.xRayTest.business.surfaceMapping;
/* Generated by Together */
import java.rmi.*;
import com.agilent.util.*;  // RemoteObserverInt, RemoteObservableInt

public interface RemoteLineScanStripEventInt extends Remote
{
  int getActiveStrip() throws RemoteException;
  int getTotalStrips() throws RemoteException;
}
