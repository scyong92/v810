package com.agilent.xRayTest.business.surfaceMapping.surfaceModeler;

import com.agilent.xRayTest.business.surfaceMapping.surfaceModeler.*;

/**
* This class mirrors the vexcel::LineScanImage class in C++.
* @author Vincent Wong
*/
public class LineScanImage
{
  /**
  * Default constructor.
  *
  * @author Vincent Wong
  */
  public LineScanImage()
  {
  }

  /**
  * Register the address in memory of the linescan image data and its size.
  *
  * @param widthInPixels - width in pixels of the linescan image.
  * @param lengthInPixels - length in pixels of the linescan image.
  * @param pArray - address in memory of the linescan image data.
  * @author Vincent Wong
  */
  public void setArray( int widthInPixels, int lengthInPixels, long pArray )
  {
    _widthInPixels  = widthInPixels;
    _lengthInPixels = lengthInPixels;
    _pArray         = pArray;
  }

  /**
  * Get the width in pixels of the linescan image.
  *
  * @return width in pixels of the linescan image.
  * @author Vincent Wong
  */
  public int getWidthInPixels()
  {
    return _widthInPixels;
  }

  /**
  * Get the length in pixels of the linescan image.
  *
  * @return length in pixels of the linescan image.
  * @author Vincent Wong
  */
  public int getLengthInPixels()
  {
    return _lengthInPixels;
  }

  /**
  * Get the start point of the linescan image in the machine space.
  *
  * @return start point of the linescan image in the machine space.
  * @author Vincent Wong
  */
  public Point2d getStartPoint()
  {
    return _startPoint;
  }

  /**
  * Get the end point of the linescan image in the machine space.
  *
  * @return end point of the linescan image in the machine space.
  * @author Vincent Wong
  */
  public Point2d getEndPoint()
  {
    return _endPoint;
  }

  /**
  * Get the address in memory of the linescan image data.
  *
  * @return address in memory of the linescan image data.
  * @author Vincent Wong
  */
  public long getArray()
  {
    return _pArray;
  }

  /**
  * Set the start point of the linescan image in the machine space.
  *
  * @param startPoint - start point of the linescan image in the machine space.
  * @author Vincent Wong
  */
  public void setStartPoint( Point2d startPoint )
  {
    _startPoint = startPoint;
  }

  /**
  * Set the end point of the linescan image in the machine space.
  *
  * @param endPoint - end point of thel linescan image in the machine space.
  * @author Vincent Wong
  */
  public void setEndPoint( Point2d endPoint )
  {
    _endPoint = endPoint;
  }

  private long    _pArray;
  private int     _widthInPixels;
  private int     _lengthInPixels;
  private Point2d _startPoint;
  private Point2d _endPoint;
}
