package com.agilent.xRayTest.business.surfaceMapping.surfaceModeler;

/**
* This class mirrors the vexcel::Point3d class in C++.
* @author Vincent Wong
*/
public class Point3d
{
  /**
  * Construct a point.
  *
  * @param x, y, and z are the coordinates of the point to be created.
  * @author Vincent Wong
  */
  public Point3d( int x, int y, int z )
  {
    _x = x;
    _y = y;
    _z = z;
  }

  /**
  *
  * Construct a default (0, 0, 0) point.
  * @author Vincent Wong
  */
  public Point3d()
  {
    _x = 0;
    _y = 0;
    _z = 0;
  }

  /**
  * Set the point to (x, y).
  *
  * @param x, y, and z are the coordinates of the point to be set.
  * @author Vincent Wong
  */
  public void set( int x, int y, int z )
  {
    _x = x;
    _y = y;
    _z = z;
  }

  /**
  * Get the x-coordinate of the point.
  *
  * @return x-coordinate of the point.
  * @author Vincent Wong
  */
  public int getX()
  {
    return _x;
  }

  /**
  * Get the y-coordinate of the point.
  *
  * @return y-coordinate of the point.
  * @author Vincent Wong
  */
  public int getY()
  {
    return _y;
  }

  /**
  * Get the z-coordinate of the point.
  *
  * @return z-coordinate of the point.
  * @author Vincent Wong
  */
  public int getZ()
  {
    return _z;
  }

  private int _x;
  private int _y;
  private int _z;
}
