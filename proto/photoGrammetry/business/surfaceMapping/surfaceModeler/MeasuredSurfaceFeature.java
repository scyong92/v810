package com.agilent.xRayTest.business.surfaceMapping.surfaceModeler;

import com.agilent.xRayTest.business.surfaceMapping.surfaceModeler.*;

/**
* This class mirrors the vexcel::MeasuredSurfaceFeature class in C++.
* @author Vincent Wong
*/
public class MeasuredSurfaceFeature
{
  /**
  * Default constructor.
  *
  * @author Vincent Wong
  */
  public MeasuredSurfaceFeature()
  {
  }

  /**
  * Construct a MeasuredSurfaceFeature object.
  *
  * @param coordsInMachine - coordinates in the machine space of the feature.
  * @param frontCoordsInPixels - coordinates in pixels of the featue in the front linescan image.
  * @param rearCoordsInPixels - coordinates in pixels of the featue in the rear linescan image.
  * @author Vincent Wong
  */
  public MeasuredSurfaceFeature( Point3d coordsInMachine,
                                 Point2d frontCoordsInPixels, 
                                 Point2d rearCoordsInPixels )
  {
    _machineCoords = coordsInMachine;
    _frontCoords   = frontCoordsInPixels;
    _rearCoords    = rearCoordsInPixels;
  }

  /**
  * Set the doNotIgnore flag to true. This measured feature cannot be ignored by the
  * setupSurfaceModel method of the SurfaceModeler class.
  *
  * @author Vincent Wong
  */
  public void setDoNotIgnoreFlag( boolean doNotIgnoreFlag )
  {
    _doNotIgnoreFlag = doNotIgnoreFlag;
  }

  /**
  * Get the state of the doNotIgnore flag.
  *
  * @return the boolean state of the doNotIgnore flag.
  * @author Vincent Wong
  */
  public boolean getDoNotIgnoreFlag()
  {
    return _doNotIgnoreFlag;
  }

  /**
  * Get the coordinates in the machine space of the surface feature. 
  *
  * @return coordinates in the machine space of the feature.
  * @author Vincent Wong
  */
  public Point3d getCoordsInMachine()
  {
    return _machineCoords;
  }

  /**
  * Get the coordinates in pixels of the featue in the front linescan image.
  *
  * @return coordinates in pixels of the featue in the front linescan image.
  * @author Vincent Wong
  */
  public Point2d getFrontCoordsInPixels()
  {
    return _frontCoords;
  }

  /**
  * Get the coordinates in pixels of the featue in the rear linescan image.
  *
  * @return coordinates in pixels of the featue in the rear linescan image.
  * @author Vincent Wong
  */
  public Point2d getRearCoordsInPixels()
  {
    return _rearCoords;
  }

  /**
  * Get the quality factor of the measurement.
  *
  * @return quality factor of the measurement.
  * @author Vincent Wong
  */
  public QualityFactor getQualityFactor()
  {
    return _qualityFactor;
  }

  /**
  * Get the ID of the feature.
  *
  * @return ID of the feature.
  * @author Vincent Wong
  */
  public int getId()
  {
    return _id;
  }

  /**
  * Set the coordinates in the machine space of the surface feature. 
  *
  * @param coordinates in the machine space of the feature.
  * @author Vincent Wong
  */
  private void setCoordsInMachine( Point3d machineCoords )
  {
    _machineCoords = machineCoords;
  }

  /**
  * Set the coordinates in pixels of the featue in the front linescan image.
  *
  * @param coordinates in pixels of the featue in the front linescan image.
  * @author Vincent Wong
  */
  private void setFrontCoordsInPixels( Point2d frontCoords )
  {
    _frontCoords = frontCoords;
  }

  /**
  * Set the coordinates in pixels of the featue in the rear linescan image.
  *
  * @param coordinates in pixels of the featue in the rear linescan image.
  * @author Vincent Wong
  */
  private void setRearCoordsInPixels( Point2d rearCoords )
  {
    _rearCoords = rearCoords;
  }

  /**
  * Set the ID of the feature.
  *
  * @param ID of the feature.
  * @author Vincent Wong
  */
  private void setId( int id )
  {
    _id = id;
  }

  private int           _id;
  private Point3d       _machineCoords;
  private Point2d       _frontCoords;
  private Point2d       _rearCoords;
  private boolean       _doNotIgnoreFlag;
  private QualityFactor _qualityFactor;
}
