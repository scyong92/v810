package com.agilent.xRayTest.business.surfaceMapping.surfaceModeler;

/**
* This class mirrors the vexcel::Point2d class in C++.
* @author Vincent Wong
*/
public class Point2d
{
  /**
  * Construct a point.
  *
  * @param x and y are the coordinates of the point to be created.
  * @author Vincent Wong
  */
  public Point2d( int x, int y )
  {
    _x = x;
    _y = y;
  }

  /**
  * Construct a default (0, 0) point.
  *
  * @author Vincent Wong
  */
  public Point2d()
  {
    _x = 0;
    _y = 0;
  }

  /**
  * Set the point to (x, y).
  *
  * @param x and y are the coordinates of the point to be set.
  * @author Vincent Wong
  */
  public void set( int x, int y )
  {
    _x = x;
    _y = y;
  }

  /**
  * Get the x-coordinate of the point.
  *
  * @return x-coordinate of the point.
  * @author Vincent Wong
  */
  public int getX()
  {
    return _x;
  }

  /**
  * Get the y-coordinate of the point.
  *
  * @return y-coordinate of the point.
  * @author Vincent Wong
  */
  public int getY()
  {
    return _y;
  }

  private int _x;
  private int _y;
}
