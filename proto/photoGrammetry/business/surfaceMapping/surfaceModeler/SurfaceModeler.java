package com.agilent.xRayTest.business.surfaceMapping.surfaceModeler;

import java.util.*;
import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.business.surfaceMapping.surfaceModeler.*;
import com.agilent.xRayTest.business.surfaceMapping.surfaceModeler.nativeInt.*;

/**-----------------------------------------------------------------------------
* This class mirrors the vexcel::SurfaceModeler class in C++.
*/
public class SurfaceModeler
{
  private static SurfaceModeler _topSideInstance = null;
  private static SurfaceModeler _bottomSideInstance = null;

  /**---------------------------------------------------------------------------
  * Construct a SurfaceModeler object.
  *
  * @param topSide - TRUE for top side and FALSE for bottom side.
  * @param surfaceModelerDirectory - the directory where all the implementation specific files will be stored.
  * @author Vincent Wong
  */
  private SurfaceModeler(boolean topSide) throws SurfaceModelerException
  {
    //We need to pass in the directory where Vexcel will look for and store info specific to their operation.
    //I temporarily made the directoy point to %RELROOT%\config.  However, we will probably have to define
    //a new directory and add the definition to the com.agilent.xRayTest.datastore.Directory class.
    //For example, the directory could be %RELROOT%\config\photogrammetry\surfaceMapping and the new
    //function in the Directory class could be "getPhotoGSurfaceMapDir()".
    //
    //Greg Esparza - 9/21/2001
     nativeSurfaceModeler = new NativeSurfaceModeler( topSide, Directory.getConfigDir() );
  }

  /**
   * Get the one and only TOP side SurfaceModeler object
   *
   * @return A SurfaceModeler object
   * @author Greg Esparza
   */
  public static SurfaceModeler getTopSideInstance() throws SurfaceModelerException
  {
    if(_topSideInstance == null)
      _topSideInstance = new SurfaceModeler(true);

    return _topSideInstance;
  }

  /**
   * Get the one and only BOTTOM side SurfaceModeler object
   *
   * @return A SurfaceModeler object
   * @author Greg Esparza
   */
  public static SurfaceModeler getBottomSideInstance() throws SurfaceModelerException
  {
    if(_bottomSideInstance == null)
      _bottomSideInstance = new SurfaceModeler(true);

    return _bottomSideInstance;
  }

  /**---------------------------------------------------------------------------
  * Dispose the resources allocated for this object.
  *
  * @author Vincent Wong
  */
  public void dispose()
  {
    nativeSurfaceModeler.dispose();
  }

  /**---------------------------------------------------------------------------
  * @author Vincent Wong
  */
  protected void finalize()
  {
    dispose();
  }

  /**---------------------------------------------------------------------------
  * Calibrate the surface modeler system with the front and rear images
  * of the calibration rig and the machine coordinates of the control points on
  * the rig. The calibration information will be stored in the directory
  * specified in the constructor.
  *
  * @param frontImage - front linescan image.
  * @param rearImage - rear linescan image.
  * @param calibrationRigID - ID of the calibration rig.
  * @param pointsInNanoMeters - an ArrayList of Point3d of the points on the calibration rig.
  * @author Vincent Wong
  */
  public void calibrate( final LineScanImage frontImage,
                         final LineScanImage rearImage,
                         int calibrationRigID,
                         final ArrayList pointsInNanoMeters ) throws SurfaceModelerException
  {
    nativeSurfaceModeler.nativeCalibrate( frontImage, rearImage,
                             calibrationRigID, pointsInNanoMeters );
  }

  /**---------------------------------------------------------------------------
  * Verify the calibration the surface modeler system with the front and rear images
  * of the calibration rig and the machine coordinates of the control points on
  * the rig. This method should be called with different data after a call to the
  * calibrate method in order to verify the validity of the calibration.
  *
  * @param frontImage - front linescan image.
  * @param rearImage - rear linescan image.
  * @param calibrationRigID - ID of the calibration rig.
  * @param pointsInNanoMeters - an ArrayList of Point3d - the points on the calibration rig.
  * @author Vincent Wong
  */
  public void verifyCalibration( final LineScanImage frontImage,
                                 final LineScanImage rearImage,
                                 int calibrationRigID,
                                 final ArrayList pointsInNanoMeters ) throws SurfaceModelerException
  {
    nativeSurfaceModeler.nativeVerifyCalibration( frontImage, rearImage,
                             calibrationRigID, pointsInNanoMeters );
  }

  /**---------------------------------------------------------------------------
  * Identify the surface features of specified feature sizes from the front and rear linescan images.
  * The images can be binarized with the specified grey level threshold value to separate the features (vias)
  * from the PCB background. The method will clear the output ArrayLists on entry if they are not
  * already empty.
  *
  * @param featureDiameter - an ArrayList of Integer - the acceptable feature diameter (via) in nano-meters.
  * @param tolerancePercentage - an integer specifying the percentage tolerance of the feature diameter.
  * @param frontImage - front linescan image.
  * @param rearImage - rear linescan image.
  * @param greyLevelThreshold - a grey level between 0 and 255 specifying the binarization threshold value.
  * @param swathRegistrationImage - an ArrayList of LineScanImage returning the registration images.
  * @param identifiedFeatures - an ArrayList of ApproxSurfaceFeature returning the identified surface features.
  * @author Vincent Wong
  */
  public void identifyFeatures( final ArrayList featureDiameterInNanoMeters,
                                int tolerancePercentage,
                                final LineScanImage frontImage,
                                final LineScanImage rearImage,
                                int greyLevelThreshold,
                                ArrayList swathRegistrationImage,
                                ArrayList identifiedFeatures ) throws SurfaceModelerException
  {
    swathRegistrationImage.clear();
    identifiedFeatures.clear();

    nativeSurfaceModeler.nativeIdentifyFeatures(
                            featureDiameterInNanoMeters, tolerancePercentage,
                            frontImage, rearImage, greyLevelThreshold,
                            swathRegistrationImage, identifiedFeatures );
  }

  /**---------------------------------------------------------------------------
  * Measure the surface features from the front and rear linescan images.
  * The images can be binarized with the specified grey level threshold value to separate the features (vias)
  * from the PCB background. The method will clear the output ArrayList on entry if it is not
  * already empty.
  *
  * @param frontImage - front linescan image.
  * @param rearImage - rear linescan image.
  * @param greyLevelThreshold - a grey level between 0 and 255 specifying the binarization threshold value.
  * @param swathRegistrationImage - an ArrayList of LineScanImage specifyng the registration images.
  * @param identifiedFeatures - an ArrayList of ApproxSurfaceFeature specifying the approximately identified surface features.
  * @param featuresToMeasure - an ArrayList of MeasuredSurfaceFeature returning the identified surface features.
  * @author Vincent Wong
  */
  public void measureFeatures( final LineScanImage frontImage,
                               final LineScanImage rearImage,
                               int greyLevelThreshold,
                               final ArrayList swathRegistrationImage,
                               final ArrayList identifiedFeatures,
                               ArrayList featuresToMeasure ) throws SurfaceModelerException
  {
    featuresToMeasure.clear();

    nativeSurfaceModeler.nativeMeasureFeatures( frontImage, rearImage, greyLevelThreshold,
                 swathRegistrationImage, identifiedFeatures, featuresToMeasure );
  }

  /**---------------------------------------------------------------------------
  * Construct a surface model from the list of points. Any points that look to be bad will be ignored
  * unless the doNotIgnore flag is set to true. The method will clear the output ArrayList on entry if
  * it is not already empty.
  *
  * @param featurePoints - an ArrayList of MeasuredSurfaceFeature specifying the points for constructing the surface model.
  * @param ignoredPoints - an ArrayList of MeasuredSurfaceFeature returning the points not used in constructing the surface model.
  * @author Vincent Wong
  */
  public void setupSurfaceModel( final ArrayList featurePoints,
                                 ArrayList ignoredPoints ) throws SurfaceModelerException
  {
    ignoredPoints.clear();

    nativeSurfaceModeler.nativeSetupSurfaceModel( featurePoints, ignoredPoints );
  }

  /**---------------------------------------------------------------------------
  * Construct a surface model from the surface model constructed with the method to
  * setupSurfaceModel which takes only 2 parameters. In this method, the surface model is
  * constructed based on the thickness of the surface. Any points that look to be bad will
  * be ignored unless the doNotIgnore flag is set to true. The method will clear the output
  * ArrayList on entry if it is not already empty.
  *
  * @param surfaceModeler - the surface model constructed with the method call to setupSurfaceModel which takes only 2 parameters.
  * @param thicknessPoints - an ArrayList of Point3d specifying thickness of the surface.
  * @param ignoredPoints - an ArrayList of MeasuredSurfaceFeature returning the points not used in constructing the surface model.
  * @author Vincent Wong
  */
  public void setupSurfaceModel( final SurfaceModeler surfaceModel,
                                 final ArrayList thicknessPoints,
                                 ArrayList ignoredPoints ) throws SurfaceModelerException
  {
    ignoredPoints.clear();

    nativeSurfaceModeler.nativeSetupSurfaceModel( surfaceModel, thicknessPoints, ignoredPoints );
  }

  /**---------------------------------------------------------------------------
  * Get the height (z) in nano-meters of the give point (x, y) in machine coordinates.
  * based on the surface model that was set up with th setupSurfaceModel method call.
  *
  * @param feature - the position of interest.
  * @return the height (z) in nano-meters.
  * @author Vincent Wong
  */
  public int getHeight( final Point2d position ) throws SurfaceModelerException
  {
    return nativeSurfaceModeler.nativeGetHeight( position );
  }

  /**---------------------------------------------------------------------------
  * For testing purpose only.
  * @author Vincent Wong
  */
  public long getImage()
  {
    long imagePtr = 0;

    try
    {
      imagePtr = nativeSurfaceModeler.nativeGetImage();
    }
    catch ( SurfaceModelerException e )
    {
    }

    return imagePtr;
  }


  /**---------------------------------------------------------------------------
  * Get the native surface modeler object.
  *
  * @return the native surface modeler object.
  * @author Vincent Wong
  */
  private NativeSurfaceModeler getNativeSurfaceModeler()
  {
    return nativeSurfaceModeler;
  }

  //----------------------------------------
  // Member variables.
  //
  private NativeSurfaceModeler nativeSurfaceModeler;
}
