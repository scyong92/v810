package com.agilent.xRayTest.business.surfaceMapping.surfaceModeler;

import com.agilent.xRayTest.business.surfaceMapping.surfaceModeler.*;

/**
* This class mirrors the vexcel::QualityFactor class in C++.
* @author Vincent Wong
*/
public class QualityFactor
{
  public QualityFactor()
  {
  }

  /**
  * Construct an QualityFactor object.
  *
  * @param correlationCoefficient - 
  * @param misclosure - 
  * @param objectResidual - 
  * @author Vincent Wong
  */
  public QualityFactor( double correlationCoefficient,
                        double misclosure,
                        Point3d objectResidual )
  {
    _correlationCoefficient = correlationCoefficient;
    _misclosure             = misclosure;
    _objectResidual         = objectResidual;
  }

  /**
  * Get the correlation coefficient.
  *
  * @return the correlation coefficient.
  * @author Vincent Wong
  */
  public double getcorrelationCoefficient()
  {
    return _correlationCoefficient;
  }

  /**
  * Get the misclosure value.
  *
  * @return the misclosure value.
  * @author Vincent Wong
  */
  public double getMisclosure()
  {
    return _misclosure;
  }

  /**
  * Get the object residual.
  *
  * @return the object residual.
  * @author Vincent Wong
  */
  public Point3d getObjectResidual()
  {
    return _objectResidual;
  }

  /**
  * Get the front image residual.
  *
  * @return the front image residual.
  * @author Vincent Wong
  */
  public Point2d getFrontImageResidual()
  {
    return _frontImageResidual;
  }

  /**
  * Get the rear image residual.
  *
  * @return the rear image residual.
  * @author Vincent Wong
  */
  public Point2d getRearImageResidual()
  {
    return _rearImageResidual;
  }

  /**
  * Set the correlation coefficient.
  *
  * @param correlation coefficient.
  * @author Vincent Wong
  */
  public void setcorrelationCoefficient( double correlationCoefficient )
  {
    _correlationCoefficient = correlationCoefficient;
  }

  /**
  * Set the misclosure value.
  *
  * @param misclosure value.
  * @author Vincent Wong
  */
  public void setMisclosure( double misclosure )
  {
    _misclosure = misclosure;
  }

  /**
  * Set the object residual.
  *
  * @param objectResidual - the object residual.
  * @author Vincent Wong
  */
  public void setResidual( Point3d objectResidual )
  {
    _objectResidual = objectResidual;
  }

  private double  _correlationCoefficient;
  private double  _misclosure;
  private Point3d _objectResidual;
  private Point2d _frontImageResidual;
  private Point2d _rearImageResidual;
}
