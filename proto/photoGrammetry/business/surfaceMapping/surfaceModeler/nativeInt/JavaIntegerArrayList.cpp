#include <cassert>

#include "NativeSurfaceModeler.h"
#include "JavaArrayList.h"
#include "JavaIntegerArrayList.h"


////////////////////////////////////////////////////////////////////////////////
// Construct a JavaIntegerArrayList C++ object with a Java ArrayList of
// Integer object.
//
// @param env - pointer to JNIEnv.
// @param objArrayList - an ArrayList of Integer object.
// @author Vincent Wong
//
JavaIntegerArrayList::JavaIntegerArrayList( JNIEnv *env, jobject const objArrayList )
{
  _env          = env;
  _objArrayList = objArrayList;

  jclass clsObject = env->GetObjectClass( _objArrayList );
  assert( clsObject != NULL );

  jclass clsArrayList = _env->FindClass( CLASS_ArrayList.c_str() );
  assert( clsArrayList != NULL );
  assert( clsArrayList != clsObject );

  _clsInt       = NULL;
  _midIntValue  = NULL;

  _env->DeleteLocalRef( clsObject );
  _env->DeleteLocalRef( clsArrayList );
}


////////////////////////////////////////////////////////////////////////////////
// @author Vincent Wong
//
JavaIntegerArrayList::~JavaIntegerArrayList()
{
  // do nothing
}


////////////////////////////////////////////////////////////////////////////////
// Translate from a Java ArrayList of Integer object to a
// std::vector<int> C++ object.
// 
// The method will clear the output vector on entry if it is not already empty.
//
// @param intVector - a vector of int.
// @author Vincent Wong
//
void
JavaIntegerArrayList::getVector( std::vector<int>& intVector )
{
  intVector.clear();

  if ( _midIntValue == NULL )
  {
    _clsInt = _env->FindClass( "java/lang/Integer" );
    assert( _clsInt );

    _midIntValue  = _env->GetMethodID( _clsInt, "intValue", "()I" );
    assert( _midIntValue );
  }

  JavaArrayList array( _env, _objArrayList, "" );

  int size = array.size();

  int *intArray = new int[size];

  jobject element;
  jint    value;

  for ( int i = 0; i < size; i++ )
  {
    element     = array.get( i );

    value       = _env->CallIntMethod( element, _midIntValue );
    assert( _env->ExceptionCheck() != JNI_TRUE );

    intArray[i] = value;
  }

  intVector.assign( &intArray[0], &intArray[size] );
  delete[] intArray;
}
