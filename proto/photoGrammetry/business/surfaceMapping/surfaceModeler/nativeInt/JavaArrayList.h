#ifndef SURFACEMODELER_NATIVEINT_JAVAARRAYLIST_H
#define SURFACEMODELER_NATIVEINT_JAVAARRAYLIST_H

#include <jni.h>
#include <string>

////////////////////////////////////////////////////////////////////////////////
// Provide access to Java ArrayList.
//
// @author Vincent Wong
//
class JavaArrayList
{
  public:
    ////////////////////////////////////////////////////////////////////////////////
    // Construct a JavaArrayList C++ object with a Java ArrayList. The array list
    // should only the type of object as specified. If type is equal to an empty 
    // string, only get() method can be used. The type must have a constructor with
    // no parameters.
    //
    // @param env - pointer to JNIEnv.
    // @param type - the type of object the ArrayList holds.
    // @author Vincent Wong
    //
    JavaArrayList( JNIEnv *env, jobject objArrayList, std::string type );

    ////////////////////////////////////////////////////////////////////////////////
    // @author Vincent Wong
    //
    ~JavaArrayList();

    ////////////////////////////////////////////////////////////////////////////////
    // Get the number of objects the current array list is holding.
    //
    // @return the number of objects the current array list is holding.
    // @author Vincent Wong
    //
    int size() const;

    ////////////////////////////////////////////////////////////////////////////////
    // Get the class the object the array list holds.
    //
    // @return the class the object the array list holds.
    // @author Vincent Wong
    //
    jclass getObjectClass() const;

    ////////////////////////////////////////////////////////////////////////////////
    // Get the object of the array list at the specified index position.
    // If index is greater than or equal to the number of objects in the array list,
    // the method returns null.
    //
    // @return the object of the array list at the specified index position.
    // @author Vincent Wong
    //
    jobject get( int index );

    ////////////////////////////////////////////////////////////////////////////////
    // Create a new object at the end of the array list and return the newly
    // created object. If the method is unable to create a new object, it returns
    // null.
    //
    // @return the newly created object.
    // @author Vincent Wong
    //
    jobject add();

    ////////////////////////////////////////////////////////////////////////////////
    // Remove all of the objects from the array list.
    //
    // @author Vincent Wong
    //
    void clear();

  private:
    JavaArrayList( JavaArrayList const& rhs );
    JavaArrayList& operator=( JavaArrayList const& rhs );

    JNIEnv *_env;
    jobject _objArrayList;
    jclass _clsArrayList;
    jclass _clsObject;
    int _size;

    jmethodID _midObjectInit;
    jmethodID _midJavaArrayListAdd;
    jmethodID _midJavaArrayListGet;
    jmethodID _midJavaArrayListClear;
};

#endif SURFACEMODELER_NATIVEINT_JAVAARRAYLIST_H
