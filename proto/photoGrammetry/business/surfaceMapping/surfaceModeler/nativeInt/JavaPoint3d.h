#ifndef SURFACEMODELER_NATIVEINT_JAVAPOINT3D_H
#define SURFACEMODELER_NATIVEINT_JAVAPOINT3D_H

#include <jni.h>
#include "Point3d.h"

////////////////////////////////////////////////////////////////////////////////
// Provide translation between vexcel::Point3d and Java Point3d.
//
// @author Vincent Wong
//
class JavaPoint3d
{
  public:
    ////////////////////////////////////////////////////////////////////////////////
    // Construct a JavaPoint3d C++ object with a Point3d Java object.
    //
    // @param env - pointer to JNIEnv.
    // @param objPoint3d - a Point3d object.
    // @author Vincent Wong
    //
    JavaPoint3d( JNIEnv *env, jobject const objPoint3d );

    ////////////////////////////////////////////////////////////////////////////////
    // @author Vincent Wong
    //
    ~JavaPoint3d();

    ////////////////////////////////////////////////////////////////////////////////
    // Translate from a Point3d Java object to a vexcel::Point3d C++ object.
    //
    // @author Vincent Wong
    //
    void getPoint3d( vexcel::Point3d& point3d );

    ////////////////////////////////////////////////////////////////////////////////
    // Get the coordinates.
    //
    // @author Vincent Wong
    //
    void get( int& x, int& y, int& z );

  private:
    JavaPoint3d();
    JavaPoint3d( JavaPoint3d const& rhs );
    JavaPoint3d& operator=( JavaPoint3d const& rhs );

    JNIEnv    *_env;
    jobject   _objPoint3d;
    jmethodID _midGetX;
    jmethodID _midGetY;
    jmethodID _midGetZ;
    jmethodID _midSet;
    jclass    _clsPoint3d;
};

#endif // SURFACEMODELER_NATIVEINT_JAVAPOINT3D_H
