#include <cassert>

#include "NativeSurfaceModeler.h"
#include "JavaLineScanImage.h"
#include "JavaPoint2d.h"
#include "Point2d.h"

using namespace std;
using namespace vexcel;

////////////////////////////////////////////////////////////////////////////////
// Construct a JavaLineScanImage C++ object with a LineScanImage Java object.
//
// @param env - pointer to JNIEnv.
// @param objLineScanImage - a Java LineScanImage object.
// @author Vincent Wong
//
JavaLineScanImage::JavaLineScanImage( JNIEnv *env, jobject const objLineScanImage )
{
  _env = env;
  _objLineScanImage = objLineScanImage;

  jclass clsObject = env->GetObjectClass( _objLineScanImage );
  assert( clsObject != NULL );

  _clsLineScanImage = _env->FindClass( CLASS_LineScanImage.c_str() );
  assert( _clsLineScanImage != NULL );
  assert( _clsLineScanImage != clsObject );

  env->DeleteLocalRef( clsObject );

  _midGetWidthInPixels  = NULL;
  _midGetLengthInPixels = NULL;
  _midGetStartPoint     = NULL;
  _midGetEndPoint       = NULL;
  _midGetArray          = NULL;

  _midPoint2dInit       = NULL;
  _midSetStartPoint     = NULL;
  _midSetEndPoint       = NULL;
  _midSetArray          = NULL;
}


////////////////////////////////////////////////////////////////////////////////
// Construct a JavaLineScanImage C++ object with a vexcel::LineScanImage C++ object.
//
// @param env - pointer to JNIEnv.
// @param lineScanImage - a vexcel::LineScanImage C++ object.
// @author Vincent Wong
//
JavaLineScanImage::JavaLineScanImage( JNIEnv *env, LineScanImage const& lineScanImage )
{
  _env = env;
  _objLineScanImage = NULL;
  _clsLineScanImage = NULL;
  _lineScanImage = lineScanImage;

  _midGetWidthInPixels  = NULL;
  _midGetLengthInPixels = NULL;
  _midGetStartPoint     = NULL;
  _midGetEndPoint       = NULL;
  _midGetArray          = NULL;

  _midPoint2dInit       = NULL;
  _midSetStartPoint     = NULL;
  _midSetEndPoint       = NULL;
  _midSetArray          = NULL;
}


////////////////////////////////////////////////////////////////////////////////
// @author Vincent Wong
//
JavaLineScanImage::~JavaLineScanImage()
{
  // do nothing
}


////////////////////////////////////////////////////////////////////////////////
// Translate from a LineScanImage Java object to a vexcel::LineScanImage C++ object.
//
// @author Vincent Wong
//
void
JavaLineScanImage::getLineScanImage( LineScanImage& lineScanImage )
{
  int width, length;
  int startX, startY;
  int endX, endY;
  long imageBuffer;

  Point2d point;

  getSize( width, length );
  getPosition( startX, startY, endX, endY );
  getArray( imageBuffer );

  lineScanImage.setArray( width, length,
    reinterpret_cast<unsigned char *>(imageBuffer) );

  point.set( startX, startY );
  lineScanImage.setStartPoint( point );

  point.set( endX, endY );
  lineScanImage.setEndPoint( point );
}


////////////////////////////////////////////////////////////////////////////////
// Translate from a vexcel::LineScanImage C++ object to a LineScanImage Java object.
//
// @author Vincent Wong
//
void
JavaLineScanImage::getObject( jobject objLineScanImage )
{
  int width            = _lineScanImage.getWidthInPixels();
  int length           = _lineScanImage.getLengthInPixels();
  unsigned char *image = _lineScanImage.getArray();

  Point2d startPoint   = _lineScanImage.getStartPoint();
  Point2d endPoint     = _lineScanImage.getEndPoint();

  if ( _midSetArray == NULL )
  {
    _clsLineScanImage = _env->FindClass( CLASS_LineScanImage.c_str() );
    assert( _clsLineScanImage );

    _midSetArray      = _env->GetMethodID( _clsLineScanImage, "setArray", "(IIJ)V" );
    assert( _midSetArray );

    string signature = "(L" + CLASS_Point2d + ";)V";

    _midSetStartPoint = _env->GetMethodID( _clsLineScanImage, "setStartPoint",
                                           signature.c_str() );
    assert( _midSetStartPoint );
  
    _midSetEndPoint   = _env->GetMethodID( _clsLineScanImage, "setEndPoint",
                                           signature.c_str() );
    assert( _midSetEndPoint );
  
    _clsPoint2d = _env->FindClass( CLASS_Point2d.c_str() );
    assert( _clsPoint2d );
  
    _midPoint2dInit   = _env->GetMethodID( _clsPoint2d, "<init>", "(II)V" );
    assert( _midPoint2dInit );
  }

  _env->CallVoidMethod( objLineScanImage, _midSetArray,
                        width, length, reinterpret_cast<jlong>( image ) );

  if ( _env->ExceptionCheck() == JNI_TRUE )
    return;

  jobject objPoint2d;

  objPoint2d = _env->NewObject( _clsPoint2d, _midPoint2dInit,
                                startPoint.getX(), startPoint.getY() );

  if ( _env->ExceptionCheck() == JNI_TRUE )
    return;

  _env->CallVoidMethod( objLineScanImage, _midSetStartPoint, objPoint2d );

  if ( _env->ExceptionCheck() == JNI_TRUE )
    return;

  objPoint2d = _env->NewObject( _clsPoint2d, _midPoint2dInit,
                                endPoint.getX(), endPoint.getY() );

  if ( _env->ExceptionCheck() == JNI_TRUE )
    return;

  _env->CallVoidMethod( objLineScanImage, _midSetEndPoint, objPoint2d );

  if ( _env->ExceptionCheck() == JNI_TRUE )
    return;
}


////////////////////////////////////////////////////////////////////////////////
// Get the size of the image.
//
// @param width - width of the image.
// @param length - length of the image.
// @author Vincent Wong
//
void
JavaLineScanImage::getSize( int& width, int& length )
{
  assert( _objLineScanImage );
  assert( _clsLineScanImage );

  if ( _midGetWidthInPixels == NULL )
  {
    _midGetWidthInPixels  = _env->GetMethodID( _clsLineScanImage, "getWidthInPixels", "()I" );
    assert( _midGetWidthInPixels != NULL );

    _midGetLengthInPixels = _env->GetMethodID( _clsLineScanImage, "getLengthInPixels", "()I" );
    assert( _midGetLengthInPixels != NULL );
  }

  width  = _env->CallIntMethod( _objLineScanImage, _midGetWidthInPixels );
  assert( _env->ExceptionCheck() != JNI_TRUE );

  length = _env->CallIntMethod( _objLineScanImage, _midGetLengthInPixels );
  assert( _env->ExceptionCheck() != JNI_TRUE );
}


////////////////////////////////////////////////////////////////////////////////
// Get the start and end positions in machine space of the line-scan image.
//
// @param startX - x-coordinate of the start point.
// @param startY - y-coordinate of the start point.
// @param endX - x-coordinate of the end point.
// @param endY - y-coordinate of the end point.
// @author Vincent Wong
//
void
JavaLineScanImage::getPosition( int& startX, int& startY, int& endX, int& endY )
{
  assert( _objLineScanImage );
  assert( _clsLineScanImage );

  string signature;

  if ( _midGetStartPoint == NULL )
  {
    signature = "()L" + CLASS_Point2d + ";";

    _midGetStartPoint = _env->GetMethodID( _clsLineScanImage, "getStartPoint", signature.c_str() );
    assert( _midGetStartPoint != NULL );
  
    _midGetEndPoint   = _env->GetMethodID( _clsLineScanImage, "getEndPoint", signature.c_str() );
    assert( _midGetEndPoint != NULL );
  }

  jobject objStartPoint = _env->CallObjectMethod( _objLineScanImage, _midGetStartPoint );
  assert( _env->ExceptionCheck() != JNI_TRUE );

  jobject objEndPoint   = _env->CallObjectMethod( _objLineScanImage, _midGetEndPoint );
  assert( _env->ExceptionCheck() != JNI_TRUE );

  JavaPoint2d startPoint( _env, objStartPoint );
  JavaPoint2d endPoint( _env, objEndPoint );

  int x, y;

  startPoint.get( x, y );
  startX = x;
  startY = y;

  endPoint.get( x, y );
  endX = x;
  endY = y;
}


////////////////////////////////////////////////////////////////////////////////
// Get the address of the image data.
//
// @param array - the address of the image data.
// @author Vincent Wong
//
void
JavaLineScanImage::getArray( long& array )
{
  assert( _objLineScanImage );
  assert( _clsLineScanImage );

  if ( _midGetArray == NULL )
  {
    _midGetArray = _env->GetMethodID( _clsLineScanImage, "getArray", "()J" );
    assert( _midGetArray != NULL );
  }

  array = _env->CallLongMethod( _objLineScanImage, _midGetArray );
  assert( _env->ExceptionCheck() != JNI_TRUE );
}
