#include <cassert>

#include "NativeSurfaceModeler.h"
#include "JavaPoint3d.h"

////////////////////////////////////////////////////////////////////////////////
// Construct a JavaPoint3d C++ object with a Point3d Java object.
//
// @param env - pointer to JNIEnv.
// @param objPoint3d - a Point3d object.
// @author Vincent Wong
//
JavaPoint3d::JavaPoint3d( JNIEnv *env, jobject const objPoint3d )
{
  _env        = env;
  _objPoint3d = objPoint3d;

  jclass clsObject = env->GetObjectClass( _objPoint3d );
  assert( clsObject != NULL );

  _clsPoint3d = _env->FindClass( CLASS_Point3d.c_str() );
  assert( _clsPoint3d != NULL );
  assert( _clsPoint3d != clsObject );

  env->DeleteLocalRef( clsObject );

  _midGetX = NULL;
  _midGetY = NULL;
  _midGetZ = NULL;
  _midSet  = NULL;
}


////////////////////////////////////////////////////////////////////////////////
// @author Vincent Wong
//
JavaPoint3d::~JavaPoint3d()
{
  // do nothing
}


////////////////////////////////////////////////////////////////////////////////
// Set the vexcel::Point3d object based on the Java Point3d object.
//
// @author Vincent Wong
//
void
JavaPoint3d::getPoint3d( vexcel::Point3d& point3d )
{
  int x, y, z;

  get( x, y, z );
  point3d.set( x, y, z );
}


////////////////////////////////////////////////////////////////////////////////
// Get the coordinates.
//
// @author Vincent Wong
//
void
JavaPoint3d::get( int& x, int& y, int& z ) 
{
  if ( _midGetX == NULL )
  {
    _midGetX = _env->GetMethodID( _clsPoint3d, "getX", "()I" );
    assert( _midGetX != NULL );

    _midGetY = _env->GetMethodID( _clsPoint3d, "getY", "()I" );
    assert( _midGetY != NULL );

    _midGetZ = _env->GetMethodID( _clsPoint3d, "getZ", "()I" );
    assert( _midGetZ != NULL );
  }

  x = _env->CallIntMethod( _objPoint3d, _midGetX );
  assert( _env->ExceptionCheck() != JNI_TRUE );

  y = _env->CallIntMethod( _objPoint3d, _midGetY );
  assert( _env->ExceptionCheck() != JNI_TRUE );

  z = _env->CallIntMethod( _objPoint3d, _midGetZ );
  assert( _env->ExceptionCheck() != JNI_TRUE );
}
