#include <cassert>

#include "NativeSurfaceModeler.h"
#include "JavaLineScanImageArrayList.h"
#include "JavaArrayList.h"
#include "JavaLineScanImage.h"
#include "JavaPoint2d.h"
#include "Point2d.h"

using namespace std;
using namespace vexcel;

////////////////////////////////////////////////////////////////////////////////
// Construct a JavaLineScanImageArrayList C++ object with a Java ArrayList of
// LineScanImage object.
//
// @param env - pointer to JNIEnv.
// @param objArrayList - an ArrayList of Point3d object.
// @author Vincent Wong
//
JavaLineScanImageArrayList::JavaLineScanImageArrayList( JNIEnv *env,
                            jobject const objArrayList )
{
  _env = env;
  _objArrayList = objArrayList;

  jclass clsObject = env->GetObjectClass( _objArrayList );
  assert( clsObject != NULL );

  jclass clsArrayList = _env->FindClass( CLASS_ArrayList.c_str() );
  assert( clsArrayList != NULL );
  assert( clsArrayList != clsObject );

  _env->DeleteLocalRef( clsObject );
  _env->DeleteLocalRef( clsArrayList );

  _lineScanImageArray = NULL;

  _midSetArray      = NULL;
  _midSetStartPoint = NULL;
  _midSetEndPoint   = NULL;
  _midPoint2dInit   = NULL;
  _midPoint2dSet    = NULL;
}


////////////////////////////////////////////////////////////////////////////////
// Construct a JavaLineScanImageArrayList C++ object with a
// std::vector<LineScanImage *> C++ object.
//
// @param env - pointer to JNIEnv.
// @param objArrayList - an ArrayList of Point3d object.
// @author Vincent Wong
//
JavaLineScanImageArrayList::JavaLineScanImageArrayList( JNIEnv *env,
                            vector<LineScanImage *> const& imageVector )
{
  _env = env;
  _imageVector = imageVector;
  _objArrayList = NULL;

  _lineScanImageArray = NULL;

  _midSetArray      = NULL;
  _midSetStartPoint = NULL;
  _midSetEndPoint   = NULL;
  _midPoint2dInit   = NULL;
  _midPoint2dSet    = NULL;
}


////////////////////////////////////////////////////////////////////////////////
// @author Vincent Wong
//
JavaLineScanImageArrayList::~JavaLineScanImageArrayList()
{
  delete[] _lineScanImageArray;
}


////////////////////////////////////////////////////////////////////////////////
// Translate from a Java ArrayList of LineScanImage object to a
// std::vector<vexcel::LineScanImage *> C++ object.
// 
// The method will clear the output vector on entry if it is not already empty.
//
// @param lineScanImageVector - a vector of LineScanImage.
// @author Vincent Wong
//
void
JavaLineScanImageArrayList::getVector( vector<LineScanImage *>& lineScanImageVector )
{
  lineScanImageVector.clear();

  JavaArrayList array( _env, _objArrayList, CLASS_LineScanImage.c_str() );

  int size = array.size();

  _lineScanImageArray = new LineScanImage[size];

  for ( int i = 0; i < size; i++ )
  {
    jobject element = array.get( i );

    JavaLineScanImage jLineScanImage( _env, element );
    jLineScanImage.getLineScanImage( _lineScanImageArray[i] );
    lineScanImageVector.push_back( &_lineScanImageArray[i] );

    _env->DeleteLocalRef( element );
  }
}


////////////////////////////////////////////////////////////////////////////////
// Translate from a std::vector<vexcel::LineScanImage *> C++ object to a 
// Java ArrayList of LineScanImage object.
// 
// The method will clear the output array list on entry if it is not already empty.
//
// @param objArrayList - an array list of LineScanImage object.
// @author Vincent Wong
//
void
JavaLineScanImageArrayList::getArrayList( jobject objArrayList ) 
{
  string signature;

  JavaArrayList array( _env, objArrayList, CLASS_LineScanImage.c_str() );
  array.clear(); // clear the output array on entry

  jclass clsLineScanImage = array.getObjectClass();
  jclass clsPoint2d;

  if ( _midSetArray == NULL )
  {
    _midSetArray      = _env->GetMethodID( clsLineScanImage, "setArray", "(IIJ)V" );
    assert( _midSetArray );

    signature = "(L" + CLASS_Point2d + ";)V";

    _midSetStartPoint = _env->GetMethodID( clsLineScanImage, "setStartPoint",
                                           signature.c_str() );
    assert( _midSetStartPoint );

    _midSetEndPoint   = _env->GetMethodID( clsLineScanImage, "setEndPoint",
                                           signature.c_str() );
    assert( _midSetEndPoint );

    clsPoint2d = _env->FindClass( CLASS_Point2d.c_str() );
  
    _midPoint2dInit   = _env->GetMethodID( clsPoint2d, "<init>", "(II)V" );
    assert( _midPoint2dInit );

    _midPoint2dSet    = _env->GetMethodID( clsPoint2d, "set", "(II)V" );
    assert( _midPoint2dSet );
  }

  vector<LineScanImage *>::iterator i;

  int           width;
  int           length;
  unsigned char	*bpImage;
  Point2d       startPoint;
  Point2d       endPoint;

  jobject       objLineScanImage;
  jobject       objPoint2d;

  for ( i = _imageVector.begin(); i != _imageVector.end(); ++i )
  {
    width      = (*i)->getWidthInPixels();
    length     = (*i)->getLengthInPixels();
    bpImage    = (*i)->getArray();
    startPoint = (*i)->getStartPoint();
    endPoint   = (*i)->getEndPoint();

    objLineScanImage = array.add();

    _env->CallVoidMethod( objLineScanImage, _midSetArray, width, length, 
                          reinterpret_cast<jlong>( bpImage ) );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    objPoint2d = _env->NewObject( clsPoint2d, _midPoint2dInit,
                                  startPoint.getX(), startPoint.getY() );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    _env->CallVoidMethod( objLineScanImage, _midSetStartPoint, objPoint2d );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    objPoint2d = _env->NewObject( clsPoint2d, _midPoint2dInit,
                                  endPoint.getX(), endPoint.getY() );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    _env->CallVoidMethod( objLineScanImage, _midSetEndPoint, objPoint2d );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;
  }
}
