#ifndef SURFACEMODELER_NATIVEINT_JAVAMEASUREDSURFACEFEATUREARRAYLIST_H
#define SURFACEMODELER_NATIVEINT_JAVAMEASUREDSURFACEFEATUREARRAYLIST_H

#include <jni.h>
#include <vector>

#include "MeasuredSurfaceFeature.h"

////////////////////////////////////////////////////////////////////////////////
// Provide translation between std::vector<vexcel::MeasuredSurfaceFeature *> and
// Java ArrayList of MeasuredSurfaceFeature.
//
// @author Vincent Wong
//
class JavaMeasuredSurfaceFeatureArrayList
{
  public:
    ////////////////////////////////////////////////////////////////////////////////
    // Construct a JavaMeasuredSurfaceFeatureArrayList C++ object with a Java
    // ArrayList of MeasuredSurfaceFeature object.
    //
    // @param env - pointer to JNIEnv.
    // @param objArrayList - an ArrayList of MeasuredSurfaceFeature object.
    // @author Vincent Wong
    //
    JavaMeasuredSurfaceFeatureArrayList( JNIEnv *env, jobject const objArrayList );

    ////////////////////////////////////////////////////////////////////////////////
    // Construct a JavaMeasuredSurfaceFeatureArrayList C++ object with a
    // std::vector<MeasuredSurfaceFeature *> C++ object.
    //
    // @param env - pointer to JNIEnv.
    // @param measuredSurfaceFeatureVector - an vector of MeasuredSurfaceFeature object.
    // @author Vincent Wong
    //
    JavaMeasuredSurfaceFeatureArrayList( JNIEnv *env,
      std::vector<vexcel::MeasuredSurfaceFeature *> const& measuredSurfaceFeatureVector );

    ////////////////////////////////////////////////////////////////////////////////
    // @author Vincent Wong
    //
    ~JavaMeasuredSurfaceFeatureArrayList();

    ////////////////////////////////////////////////////////////////////////////////
    // Translate from a Java ArrayList of MeasuredSurfaceFeature object to a
    // std::vector<vexcel::MeasuredSurfaceFeature *> C++ object.
    // 
    // The method will clear the output vector on entry if it is not already empty.
    //
    // @param measuredSurfaceFeatureVector - a vector of MeasuredSurfaceFeature.
    // @author Vincent Wong
    //
    void getVector( std::vector<vexcel::MeasuredSurfaceFeature *>& measuredSurfaceFeatureVector );

    ////////////////////////////////////////////////////////////////////////////////
    // Translate from a std::vector<vexcel::MeasuredSurfaceFeature *> C++ object to a 
    // Java ArrayList of MeasuredSurfaceFeature object.
    // 
    // The method will clear the output array list on entry if it is not already empty.
    //
    // @param objArrayList - an array list of MeasuredSurfaceFeature object.
    // @author Vincent Wong
    //
    void getArrayList( jobject objArrayList );

  private:
    JavaMeasuredSurfaceFeatureArrayList();
    JavaMeasuredSurfaceFeatureArrayList( JavaMeasuredSurfaceFeatureArrayList const& rhs );
    JavaMeasuredSurfaceFeatureArrayList& operator=( JavaMeasuredSurfaceFeatureArrayList const& rhs );

    JNIEnv *_env;
    jobject _objArrayList;
    std::vector<vexcel::MeasuredSurfaceFeature *> _feature;
    vexcel::MeasuredSurfaceFeature **_measuredSurfaceFeatureArray;
    int _size;

    jmethodID _midSetCoordsInMachine;
    jmethodID _midSetFrontCoordsInPixels;
    jmethodID _midSetRearCoordsInPixels;
    jmethodID _midSetId;

    jmethodID _midGetCoordsInMachine;
    jmethodID _midGetFrontCoordsInPixels;
    jmethodID _midGetRearCoordsInPixels;
    jmethodID _midGetId;

    jmethodID _midPoint2dInit;
    jmethodID _midPoint3dInit;

    jclass    _clsPoint2d;
    jclass    _clsPoint3d;
};

#endif SURFACEMODELER_NATIVEINT_JAVAMEASUREDSURFACEFEATUREARRAYLIST_H
