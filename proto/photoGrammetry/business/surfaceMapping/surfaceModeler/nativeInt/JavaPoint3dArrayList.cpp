#include <cassert>

#include "NativeSurfaceModeler.h"
#include "JavaPoint3dArrayList.h"
#include "JavaPoint3d.h"
#include "JavaArrayList.h"

using namespace std;
using namespace vexcel;

////////////////////////////////////////////////////////////////////////////////
// Construct a JavaPoint3dArrayList C++ object with a Java ArrayList of
// Point3d object.
//
// @param env - pointer to JNIEnv.
// @param objArrayList - an ArrayList of Point3d object.
// @author Vincent Wong
//
JavaPoint3dArrayList::JavaPoint3dArrayList( JNIEnv *env, jobject const objArrayList )
{
  _env = env;
  _objArrayList = objArrayList;

  jclass clsObject = env->GetObjectClass( _objArrayList );
  assert( clsObject != NULL );

  jclass clsArrayList = _env->FindClass( CLASS_ArrayList.c_str() );
  assert( clsArrayList != NULL );
  assert( clsArrayList != clsObject );

  _midGetX = NULL;
  _midGetY = NULL;
  _midGetZ = NULL;

  _point3dArray = NULL;

  _env->DeleteLocalRef( clsObject );
  _env->DeleteLocalRef( clsArrayList );
}


////////////////////////////////////////////////////////////////////////////////
// @author Vincent Wong
//
JavaPoint3dArrayList::~JavaPoint3dArrayList()
{
  delete[] _point3dArray;
}


////////////////////////////////////////////////////////////////////////////////
// Translate from a Java ArrayList of Point3d object to a
// std::vector<vexcel::Point3d *> C++ object.
// 
// The method will clear the output vector on entry if it is not already empty.
//
// @param point3dVector - a vector of Point3d.
// @author Vincent Wong
//
void
JavaPoint3dArrayList::getVector( vector<Point3d *>& point3dVector )
{
  point3dVector.clear();

  JavaArrayList array( _env, _objArrayList, CLASS_Point3d.c_str() );

  jclass clsPoint3d = array.getObjectClass();

  if ( _midGetX == NULL )
  {
    _midGetX = _env->GetMethodID( clsPoint3d, "getX", "()I" );
    assert( _midGetX );

    _midGetY = _env->GetMethodID( clsPoint3d, "getY", "()I" );
    assert( _midGetY );

    _midGetZ = _env->GetMethodID( clsPoint3d, "getZ", "()I" );
    assert( _midGetZ );
  }

  int size = array.size();

  _point3dArray = new Point3d[size];

  for ( int i = 0; i < size; i++ )
  {
    jobject element = array.get( i );

    JavaPoint3d jPoint3d( _env, element );
    jPoint3d.getPoint3d( _point3dArray[i] );
    point3dVector.push_back( &_point3dArray[i] );

    _env->DeleteLocalRef( element );
  }
}
