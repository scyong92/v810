#ifndef SURFACEMODELER_NATIVEINT_JAVAAPPROXSURFACEFEATUREARRAYLIST_H
#define SURFACEMODELER_NATIVEINT_JAVAAPPROXSURFACEFEATUREARRAYLIST_H

#include <jni.h>
#include <vector>

#include "ApproxSurfaceFeature.h"

////////////////////////////////////////////////////////////////////////////////
// Provide translation between std::vector<vexcel::ApproxSurfaceFeature *> and
// Java ArrayList of ApproxSurfaceFeature.
//
// @author Vincent Wong
//
class JavaApproxSurfaceFeatureArrayList
{
  public:
    ////////////////////////////////////////////////////////////////////////////////
    // Construct a JavaApproxSurfaceFeatureArrayList C++ object with a Java
    // ArrayList of ApproxSurfaceFeature object.
    //
    // @param env - pointer to JNIEnv.
    // @param objArrayList - an ArrayList of ApproxSurfaceFeature object.
    // @author Vincent Wong
    //
    JavaApproxSurfaceFeatureArrayList( JNIEnv *env, jobject const objArrayList );

    ////////////////////////////////////////////////////////////////////////////////
    // Construct a JavaApproxSurfaceFeatureArrayList C++ object with a
    // std::vector<ApproxSurfaceFeature *> C++ object.
    //
    // @param env - pointer to JNIEnv.
    // @param approxSurfaceFeatureVector - an vector of ApproxSurfaceFeature object.
    // @author Vincent Wong
    //
    JavaApproxSurfaceFeatureArrayList( JNIEnv *env,
      std::vector<vexcel::ApproxSurfaceFeature *> const& approxSurfaceFeatureVector );

    ////////////////////////////////////////////////////////////////////////////////
    // @author Vincent Wong
    //
    ~JavaApproxSurfaceFeatureArrayList();

    ////////////////////////////////////////////////////////////////////////////////
    // Translate from a Java ArrayList of ApproxSurfaceFeature object to a
    // std::vector<vexcel::ApproxSurfaceFeature *> C++ object.
    // 
    // The method will clear the output vector on entry if it is not already empty.
    //
    // @param approxSurfaceFeatureVector - a vector of ApproxSurfaceFeature.
    // @author Vincent Wong
    //
    void getVector( std::vector<vexcel::ApproxSurfaceFeature *>& approxSurfaceFeatureVector );

    ////////////////////////////////////////////////////////////////////////////////
    // Translate from a std::vector<vexcel::ApproxSurfaceFeature *> C++ object to a 
    // Java ArrayList of ApproxSurfaceFeature object.
    // 
    // The method will clear the output array list on entry if it is not already empty.
    //
    // @param objArrayList - an array list of ApproxSurfaceFeature object.
    // @author Vincent Wong
    //
    void getArrayList( jobject objArrayList );

  private:
    JavaApproxSurfaceFeatureArrayList();
    JavaApproxSurfaceFeatureArrayList( JavaApproxSurfaceFeatureArrayList const& rhs );
    JavaApproxSurfaceFeatureArrayList& operator=( JavaApproxSurfaceFeatureArrayList const& rhs );

    JNIEnv *_env;
    jobject _objArrayList;
    std::vector<vexcel::ApproxSurfaceFeature *> _feature;
    vexcel::ApproxSurfaceFeature **_approxSurfaceFeatureArray;
    vexcel::LineScanImage *_templateImageArray;
    int _size;

    jmethodID _midSetCoordsInMachine;
    jmethodID _midSetFrontCoordsInPixels;
    jmethodID _midSetRearCoordsInPixels;
    jmethodID _midSetTemplateImage;
    jmethodID _midSetDiameterInNanoMeters;
    jmethodID _midSetId;

    jmethodID _midGetCoordsInMachine;
    jmethodID _midGetFrontCoordsInPixels;
    jmethodID _midGetRearCoordsInPixels;
    jmethodID _midGetTemplateImage;
    jmethodID _midGetDiameterInNanoMeters;
    jmethodID _midGetId;

    jmethodID _midLineScanImageInit;
    jmethodID _midPoint2dInit;
    jmethodID _midPoint3dInit;

    jclass    _clsLineScanImage;
    jclass    _clsPoint2d;
    jclass    _clsPoint3d;
};

#endif SURFACEMODELER_NATIVEINT_JAVAAPPROXSURFACEFEATUREARRAYLIST_H
