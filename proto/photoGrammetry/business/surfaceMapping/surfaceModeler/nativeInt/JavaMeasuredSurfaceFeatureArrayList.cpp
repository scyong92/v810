#include <cassert>

#include "NativeSurfaceModeler.h"
#include "JavaMeasuredSurfaceFeatureArrayList.h"
#include "JavaArrayList.h"
#include "JavaPoint2d.h"
#include "JavaPoint3d.h"
#include "MeasuredSurfaceFeature.h"
#include "Point2d.h"
#include "Point3d.h"

using namespace std;
using namespace vexcel;

////////////////////////////////////////////////////////////////////////////////
// Construct a JavaMeasuredSurfaceFeatureArrayList C++ object with a Java
// ArrayList of MeasuredSurfaceFeature object.
//
// @param env - pointer to JNIEnv.
// @param objArrayList - an ArrayList of MeasuredSurfaceFeature object.
// @author Vincent Wong
//
JavaMeasuredSurfaceFeatureArrayList::JavaMeasuredSurfaceFeatureArrayList(
                                     JNIEnv *env, jobject const objArrayList )
{
  _env = env;
  _objArrayList = objArrayList;

  jclass clsObject = env->GetObjectClass( _objArrayList );
  assert( clsObject != NULL );

  jclass clsArrayList = _env->FindClass( CLASS_ArrayList.c_str() );
  assert( clsArrayList != NULL );
  assert( clsArrayList != clsObject );

  _size = 0;
  _measuredSurfaceFeatureArray = NULL;

  _midSetCoordsInMachine     = NULL;
  _midSetFrontCoordsInPixels = NULL;
  _midSetRearCoordsInPixels  = NULL;
  _midSetId                  = NULL;

  _midGetCoordsInMachine     = NULL;
  _midGetFrontCoordsInPixels = NULL;
  _midGetRearCoordsInPixels  = NULL;
  _midGetId                  = NULL;

  _midPoint2dInit            = NULL;
  _midPoint3dInit            = NULL;

  _clsPoint2d                = NULL;
  _clsPoint3d                = NULL;

  _env->DeleteLocalRef( clsObject );
  _env->DeleteLocalRef( clsArrayList );
}


////////////////////////////////////////////////////////////////////////////////
// Construct a JavaMeasuredSurfaceFeatureArrayList C++ object with a
// std::vector<MeasuredSurfaceFeature *> C++ object.
//
// @param env - pointer to JNIEnv.
// @param measuredSurfaceFeatureVector - an vector of MeasuredSurfaceFeature object.
// @author Vincent Wong
//
JavaMeasuredSurfaceFeatureArrayList::JavaMeasuredSurfaceFeatureArrayList(
                                     JNIEnv *env, std::vector<MeasuredSurfaceFeature *> const& feature )
{
  _env = env;
  _feature = feature;
  _objArrayList = NULL;
  _size = 0;

  _midSetCoordsInMachine     = NULL;
  _midSetFrontCoordsInPixels = NULL;
  _midSetRearCoordsInPixels  = NULL;
  _midSetId                  = NULL;

  _midGetCoordsInMachine     = NULL;
  _midGetFrontCoordsInPixels = NULL;
  _midGetRearCoordsInPixels  = NULL;
  _midGetId                  = NULL;

  _midPoint2dInit            = NULL;
  _midPoint3dInit            = NULL;

  _clsPoint2d                = NULL;
  _clsPoint3d                = NULL;
}


////////////////////////////////////////////////////////////////////////////////
// @author Vincent Wong
//
JavaMeasuredSurfaceFeatureArrayList::~JavaMeasuredSurfaceFeatureArrayList()
{
  for ( int i = 0; i < _size; i++ )
    MeasuredSurfaceFeature::destroyInstance( _measuredSurfaceFeatureArray[i] );

  delete[] _measuredSurfaceFeatureArray;
}


////////////////////////////////////////////////////////////////////////////////
// Translate from a Java ArrayList of MeasuredSurfaceFeature object to a
// std::vector<vexcel::MeasuredSurfaceFeature *> C++ object.
// 
// The method will clear the output vector on entry if it is not already empty.
//
// @param measuredSurfaceFeatureVector - a vector of MeasuredSurfaceFeature.
// @author Vincent Wong
//
void
JavaMeasuredSurfaceFeatureArrayList::getVector(
                                     std::vector<MeasuredSurfaceFeature *>& measuredSurfaceFeature )
{
  string signature;

  measuredSurfaceFeature.clear();

  JavaArrayList array( _env, _objArrayList, CLASS_MeasuredSurfaceFeature.c_str() );

  jclass clsMeasuredSurfaceFeature = array.getObjectClass();

  if ( _midGetCoordsInMachine == NULL )
  {
    signature = "()L" + CLASS_Point3d + ";";

    _midGetCoordsInMachine     = _env->GetMethodID( clsMeasuredSurfaceFeature,
                                 "getCoordsInMachine", signature.c_str() );
    assert( _midGetCoordsInMachine );

    signature = "()L" + CLASS_Point2d + ";";

    _midGetFrontCoordsInPixels = _env->GetMethodID( clsMeasuredSurfaceFeature,
                                 "getFrontCoordsInPixels", signature.c_str() );
    assert( _midGetFrontCoordsInPixels );

    _midGetRearCoordsInPixels  = _env->GetMethodID( clsMeasuredSurfaceFeature,
                                 "getRearCoordsInPixels", signature.c_str() );
    assert( _midGetRearCoordsInPixels ); 

    _midGetId                  = _env->GetMethodID( clsMeasuredSurfaceFeature,
                                 "getId", "()I" );
    assert( _midGetId ); 
  }

  _size = array.size();

  _measuredSurfaceFeatureArray = new MeasuredSurfaceFeature *[_size];

  Point3d coordsInMachine;
  Point2d frontCoordsInPixels;
  Point2d rearCoordsInPixels;

  jobject objPoint3d;
  jobject objPoint2d;
  int x, y, z;

  for ( int i = 0; i < _size; i++ )
  {
    jobject element = array.get( i );

    objPoint3d = _env->CallObjectMethod( element, _midGetCoordsInMachine );
    JavaPoint3d jCoordsInMachine( _env, objPoint3d );
    jCoordsInMachine.get( x, y, z );
    coordsInMachine.set( x, y, z );

    objPoint2d = _env->CallObjectMethod( element, _midGetFrontCoordsInPixels );
    JavaPoint2d jFrontCoordsInPixels( _env, objPoint2d );
    jFrontCoordsInPixels.get( x, y );
    frontCoordsInPixels.set( x, y );

    objPoint2d = _env->CallObjectMethod( element, _midGetRearCoordsInPixels );
    JavaPoint2d jRearCoordsInPixels( _env, objPoint2d );
    jRearCoordsInPixels.get( x, y );
    rearCoordsInPixels.set( x, y );

    _measuredSurfaceFeatureArray[i] = MeasuredSurfaceFeature::createInstance(
              coordsInMachine, frontCoordsInPixels, rearCoordsInPixels );

    measuredSurfaceFeature.push_back( _measuredSurfaceFeatureArray[i] );

    _env->DeleteLocalRef( element );
  }
}


////////////////////////////////////////////////////////////////////////////////
// Translate from a std::vector<vexcel::MeasuredSurfaceFeature *> C++ object to a 
// Java ArrayList of MeasuredSurfaceFeature object.
// 
// The method will clear the output array list on entry if it is not already empty.
//
// @param objArrayList - an array list of MeasuredSurfaceFeature object.
// @author Vincent Wong
//
void
JavaMeasuredSurfaceFeatureArrayList::getArrayList( jobject objArrayList ) 
{
  string signature;

  JavaArrayList array( _env, objArrayList, CLASS_MeasuredSurfaceFeature.c_str() );
  array.clear(); // clear the output array on entry

  jclass clsMeasuredSurfaceFeature = array.getObjectClass();

  if ( _midSetCoordsInMachine == NULL )
  {
    signature = "(L" + CLASS_Point3d + ";)V";

    _midSetCoordsInMachine     = _env->GetMethodID( clsMeasuredSurfaceFeature,
                                 "setCoordsInMachine", signature.c_str() );
    assert( _midSetCoordsInMachine );

    signature = "(L" + CLASS_Point2d + ";)V";
  
    _midSetFrontCoordsInPixels = _env->GetMethodID( clsMeasuredSurfaceFeature,
                                 "setFrontCoordsInPixels", signature.c_str() );
    assert( _midSetFrontCoordsInPixels );

    _midSetRearCoordsInPixels  = _env->GetMethodID( clsMeasuredSurfaceFeature,
                                 "setRearCoordsInPixels", signature.c_str() );
    assert( _midSetRearCoordsInPixels ); 

    _midSetId                  = _env->GetMethodID( clsMeasuredSurfaceFeature,
                                 "setId", "(I)V" );
    assert( _midSetId ); 
  }

  if ( _clsPoint2d == NULL )
  {
    _clsPoint2d = _env->FindClass( CLASS_Point2d.c_str() );
    assert( _clsPoint2d );

    _midPoint2dInit   = _env->GetMethodID( _clsPoint2d, "<init>", "(II)V" );
    assert( _midPoint2dInit );

    _clsPoint3d = _env->FindClass( CLASS_Point3d.c_str() );
    assert( _clsPoint3d );

    _midPoint3dInit   = _env->GetMethodID( _clsPoint3d, "<init>", "(III)V" );
    assert( _midPoint3dInit );
  }

  vector<MeasuredSurfaceFeature *>::iterator i;

  Point3d       machineCoords;
  Point2d       frontCoords;
  Point2d       rearCoords;
  int           id;

  jobject       objMeasuredSurfaceFeature;
  jobject       objPoint3d;
  jobject       objPoint2d;

  for ( i = _feature.begin(); i != _feature.end(); ++i )
  {
    machineCoords = (*i)->getCoordsInMachine();
    frontCoords   = (*i)->getFrontCoordsInPixels();
    rearCoords    = (*i)->getRearCoordsInPixels();
    id            = (*i)->getId();

    objMeasuredSurfaceFeature = array.add();

    objPoint3d = _env->NewObject( _clsPoint3d, _midPoint3dInit,
      machineCoords.getX(), machineCoords.getY(), machineCoords.getZ() );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    _env->CallVoidMethod( objMeasuredSurfaceFeature, _midSetCoordsInMachine, objPoint3d );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    objPoint2d = _env->NewObject( _clsPoint2d, _midPoint2dInit,
                                  frontCoords.getX(), frontCoords.getY() );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    _env->CallVoidMethod( objMeasuredSurfaceFeature, _midSetFrontCoordsInPixels, objPoint2d );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    objPoint2d = _env->NewObject( _clsPoint2d, _midPoint2dInit,
                                  rearCoords.getX(), rearCoords.getY() );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    _env->CallVoidMethod( objMeasuredSurfaceFeature, _midSetRearCoordsInPixels, objPoint2d );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    _env->CallVoidMethod( objMeasuredSurfaceFeature, _midSetId, id );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;
  }
}
