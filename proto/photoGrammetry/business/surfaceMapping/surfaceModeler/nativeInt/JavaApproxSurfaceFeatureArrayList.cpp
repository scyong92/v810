#include <cassert>

#include "NativeSurfaceModeler.h"
#include "JavaApproxSurfaceFeatureArrayList.h"
#include "JavaArrayList.h"
#include "JavaLineScanImage.h"
#include "JavaPoint2d.h"
#include "JavaPoint3d.h"
#include "ApproxSurfaceFeature.h"
#include "LineScanImage.h"
#include "Point2d.h"
#include "Point3d.h"

using namespace std;
using namespace vexcel;

////////////////////////////////////////////////////////////////////////////////
// Construct a JavaApproxSurfaceFeatureArrayList C++ object with a Java
// ArrayList of ApproxSurfaceFeature object.
//
// @param env - pointer to JNIEnv.
// @param objArrayList - an ArrayList of ApproxSurfaceFeature object.
// @author Vincent Wong
//
JavaApproxSurfaceFeatureArrayList::JavaApproxSurfaceFeatureArrayList(
                                   JNIEnv *env, jobject const objArrayList )
{
  _env = env;
  _objArrayList = objArrayList;

  jclass clsObject = env->GetObjectClass( _objArrayList );
  assert( clsObject != NULL );

  jclass clsArrayList = _env->FindClass( CLASS_ArrayList.c_str() );
  assert( clsArrayList != NULL );
  assert( clsArrayList != clsObject );

  _approxSurfaceFeatureArray  = NULL;
  _size = 0;

  _midSetCoordsInMachine      = NULL;
  _midSetFrontCoordsInPixels  = NULL;
  _midSetRearCoordsInPixels   = NULL;
  _midSetTemplateImage        = NULL;
  _midSetDiameterInNanoMeters = NULL;
  _midSetId                   = NULL;

  _midGetCoordsInMachine      = NULL;
  _midGetFrontCoordsInPixels  = NULL;
  _midGetRearCoordsInPixels   = NULL;
  _midGetTemplateImage        = NULL;
  _midGetDiameterInNanoMeters = NULL;
  _midGetId                   = NULL;

  _midLineScanImageInit       = NULL;
  _midPoint2dInit             = NULL;
  _midPoint3dInit             = NULL;

  _clsLineScanImage           = NULL;
  _clsPoint2d                 = NULL;
  _clsPoint3d                 = NULL;

  _env->DeleteLocalRef( clsObject );
  _env->DeleteLocalRef( clsArrayList );
}


////////////////////////////////////////////////////////////////////////////////
// Construct a JavaApproxSurfaceFeatureArrayList C++ object with a
// std::vector<ApproxSurfaceFeature *> C++ object.
//
// @param env - pointer to JNIEnv.
// @param approxSurfaceFeatureVector - an vector of ApproxSurfaceFeature object.
// @author Vincent Wong
//
JavaApproxSurfaceFeatureArrayList::JavaApproxSurfaceFeatureArrayList(
                                   JNIEnv *env, vector<ApproxSurfaceFeature *> const& feature )
{
  _env = env;
  _feature = feature;
  _objArrayList = NULL;
  _size = 0;

  _midSetCoordsInMachine      = NULL;
  _midSetFrontCoordsInPixels  = NULL;
  _midSetRearCoordsInPixels   = NULL;
  _midSetTemplateImage        = NULL;
  _midSetDiameterInNanoMeters = NULL;
  _midSetId                   = NULL;

  _midGetCoordsInMachine      = NULL;
  _midGetFrontCoordsInPixels  = NULL;
  _midGetRearCoordsInPixels   = NULL;
  _midGetTemplateImage        = NULL;
  _midGetDiameterInNanoMeters = NULL;
  _midGetId                   = NULL;

  _midLineScanImageInit       = NULL;
  _midPoint2dInit             = NULL;
  _midPoint3dInit             = NULL;

  _clsLineScanImage           = NULL;
  _clsPoint2d                 = NULL;
  _clsPoint3d                 = NULL;
}


////////////////////////////////////////////////////////////////////////////////
// @author Vincent Wong
//
JavaApproxSurfaceFeatureArrayList::~JavaApproxSurfaceFeatureArrayList()
{
  for ( int i = 0; i < _size; i++ )
    ApproxSurfaceFeature::destroyInstance( _approxSurfaceFeatureArray[i] );

  delete[] _approxSurfaceFeatureArray;
  delete[] _templateImageArray;
}


////////////////////////////////////////////////////////////////////////////////
// Translate from a Java ArrayList of ApproxSurfaceFeature object to a
// std::vector<vexcel::ApproxSurfaceFeature *> C++ object.
// 
// The method will clear the output vector on entry if it is not already empty.
//
// @param approxSurfaceFeatureVector - a vector of ApproxSurfaceFeature.
// @author Vincent Wong
//
void
JavaApproxSurfaceFeatureArrayList::getVector(
                                   vector<ApproxSurfaceFeature *>& approxSurfaceFeatureVector )
{
  string signature;

  approxSurfaceFeatureVector.clear();

  JavaArrayList array( _env, _objArrayList, CLASS_ApproxSurfaceFeature.c_str() );

  jclass clsApproxSurfaceFeature = array.getObjectClass();

  if ( _midGetCoordsInMachine == NULL )
  {
    signature = "()L" + CLASS_Point3d + ";";

    _midGetCoordsInMachine     = _env->GetMethodID( clsApproxSurfaceFeature,
                                 "getCoordsInMachine", signature.c_str() );
    assert( _midGetCoordsInMachine );

    signature = "()L" + CLASS_Point2d + ";";
  
    _midGetFrontCoordsInPixels = _env->GetMethodID( clsApproxSurfaceFeature,
                                 "getFrontCoordsInPixels", signature.c_str() );
    assert( _midGetFrontCoordsInPixels );

    _midGetRearCoordsInPixels  = _env->GetMethodID( clsApproxSurfaceFeature,
                                 "getRearCoordsInPixels", signature.c_str() );
    assert( _midGetRearCoordsInPixels ); 

    signature = "()L" + CLASS_LineScanImage + ";";

    _midGetTemplateImage       = _env->GetMethodID( clsApproxSurfaceFeature,
                                 "getTemplateImage", signature.c_str() );
    assert( _midGetTemplateImage ); 

    _midGetDiameterInNanoMeters = _env->GetMethodID( clsApproxSurfaceFeature,
                                  "getDiameterInNanoMeters", "()I" );
    assert( _midGetDiameterInNanoMeters ); 

    _midGetId                  = _env->GetMethodID( clsApproxSurfaceFeature,
                                 "getId", "()I" );
    assert( _midGetId ); 
  }

  _size = array.size();

  _approxSurfaceFeatureArray = new ApproxSurfaceFeature *[_size];

  Point3d coordsInMachine;
  Point2d frontCoordsInPixels;
  Point2d rearCoordsInPixels;
  _templateImageArray = new LineScanImage[_size];

  jobject objPoint3d;
  jobject objPoint2d;
  jobject objLineScanImage;
  int x, y, z;
  int diameter;

  for ( int i = 0; i < _size; i++ )
  {
    jobject element = array.get( i );

    objPoint3d = _env->CallObjectMethod( element, _midGetCoordsInMachine );
    assert( objPoint3d ); 

    JavaPoint3d jCoordsInMachine( _env, objPoint3d );
    jCoordsInMachine.get( x, y, z );
    coordsInMachine.set( x, y, z );

    objPoint2d = _env->CallObjectMethod( element, _midGetFrontCoordsInPixels );
    assert( objPoint2d ); 

    JavaPoint2d jFrontCoordsInPixels( _env, objPoint2d );
    jFrontCoordsInPixels.get( x, y );
    frontCoordsInPixels.set( x, y );

    objPoint2d = _env->CallObjectMethod( element, _midGetRearCoordsInPixels );
    assert( objPoint2d ); 

    JavaPoint2d jRearCoordsInPixels( _env, objPoint2d );
    jRearCoordsInPixels.get( x, y );
    rearCoordsInPixels.set( x, y );

    objLineScanImage = _env->CallObjectMethod( element, _midGetTemplateImage );
    assert( objLineScanImage ); 

    diameter = _env->CallIntMethod( element, _midGetDiameterInNanoMeters );

    JavaLineScanImage jTemplateImage( _env, objLineScanImage );
    jTemplateImage.getLineScanImage( _templateImageArray[i] );

    _approxSurfaceFeatureArray[i] = ApproxSurfaceFeature::createInstance( coordsInMachine,
                frontCoordsInPixels, rearCoordsInPixels, _templateImageArray[i], diameter  );

    approxSurfaceFeatureVector.push_back( _approxSurfaceFeatureArray[i] );

    _env->DeleteLocalRef( element );
  }
}


////////////////////////////////////////////////////////////////////////////////
// Translate from a std::vector<vexcel::ApproxSurfaceFeature *> C++ object to a 
// Java ArrayList of ApproxSurfaceFeature object.
// 
// The method will clear the output array list on entry if it is not already empty.
//
// @param objArrayList - an array list of ApproxSurfaceFeature object.
// @author Vincent Wong
//
void
JavaApproxSurfaceFeatureArrayList::getArrayList( jobject objArrayList ) 
{
  string signature;

  JavaArrayList array( _env, objArrayList, CLASS_ApproxSurfaceFeature.c_str() );
  array.clear(); // clear the output array on entry
  
  jclass clsApproxSurfaceFeature = array.getObjectClass();

  if ( _midSetCoordsInMachine == NULL )
  {
    signature = "(L" + CLASS_Point3d + ";)V";

    _midSetCoordsInMachine      = _env->GetMethodID( clsApproxSurfaceFeature,
                                  "setCoordsInMachine", signature.c_str() );
    assert( _midSetCoordsInMachine );

    signature = "(L" + CLASS_Point2d + ";)V";

    _midSetFrontCoordsInPixels  = _env->GetMethodID( clsApproxSurfaceFeature,
                                  "setFrontCoordsInPixels", signature.c_str() );
    assert( _midSetFrontCoordsInPixels );
  
    _midSetRearCoordsInPixels   = _env->GetMethodID( clsApproxSurfaceFeature,
                                  "setRearCoordsInPixels", signature.c_str() );
    assert( _midSetRearCoordsInPixels ); 

    signature = "(L" + CLASS_LineScanImage + ";)V";

    _midSetTemplateImage        = _env->GetMethodID( clsApproxSurfaceFeature,
                                  "setTemplateImage", signature.c_str() );
    assert( _midSetTemplateImage ); 

    _midSetDiameterInNanoMeters = _env->GetMethodID( clsApproxSurfaceFeature,
                                  "setDiameterInNanoMeters", "(I)V" );
    assert( _midSetDiameterInNanoMeters ); 

    _midSetId                   = _env->GetMethodID( clsApproxSurfaceFeature,
                                  "setId", "(I)V" );
    assert( _midSetId ); 
  }

  if ( _clsLineScanImage == NULL )
  {
    _clsLineScanImage = _env->FindClass( CLASS_LineScanImage.c_str() );
    assert( _clsLineScanImage ); 

    _midLineScanImageInit   = _env->GetMethodID( _clsLineScanImage, "<init>", "()V" );
    assert( _midLineScanImageInit ); 

    _clsPoint2d = _env->FindClass( CLASS_Point2d.c_str() );
    assert( _clsPoint2d );

    _midPoint2dInit   = _env->GetMethodID( _clsPoint2d, "<init>", "(II)V" );
    assert( _midPoint2dInit );

    _clsPoint3d = _env->FindClass( CLASS_Point3d.c_str() );
    assert( _clsPoint3d );

    _midPoint3dInit   = _env->GetMethodID( _clsPoint3d, "<init>", "(III)V" );
    assert( _midPoint3dInit );
  }

  vector<ApproxSurfaceFeature *>::iterator i;

  Point3d       machineCoords;
  Point2d       frontCoords;
  Point2d       rearCoords;
  LineScanImage image;
  int           diameter;
  int           id;

  jobject       objApproxSurfaceFeature;
  jobject       objPoint3d;
  jobject       objPoint2d;

  for ( i = _feature.begin(); i != _feature.end(); ++i )
  {
    machineCoords = (*i)->getCoordsInMachine();
    frontCoords   = (*i)->getFrontCoordsInPixels();
    rearCoords    = (*i)->getRearCoordsInPixels();
    image         = (*i)->getTemplateImage();
    diameter      = (*i)->getDiameterInNanoMeters();
    id            = (*i)->getId();

    objApproxSurfaceFeature = array.add();

    objPoint3d = _env->NewObject( _clsPoint3d, _midPoint3dInit,
      machineCoords.getX(), machineCoords.getY(), machineCoords.getZ() );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    _env->CallVoidMethod( objApproxSurfaceFeature, _midSetCoordsInMachine, objPoint3d );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    objPoint2d = _env->NewObject( _clsPoint2d, _midPoint2dInit,
                                  frontCoords.getX(), frontCoords.getY() );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    _env->CallVoidMethod( objApproxSurfaceFeature, _midSetFrontCoordsInPixels, objPoint2d );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    objPoint2d = _env->NewObject( _clsPoint2d, _midPoint2dInit,
                                  rearCoords.getX(), rearCoords.getY() );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    _env->CallVoidMethod( objApproxSurfaceFeature, _midSetRearCoordsInPixels, objPoint2d );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    jobject objLineScanImage = _env->NewObject( _clsLineScanImage, _midLineScanImageInit );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    JavaLineScanImage jLineScanImage( _env, image );
    jLineScanImage.getObject( objLineScanImage );

    _env->CallVoidMethod( objApproxSurfaceFeature, _midSetTemplateImage, objLineScanImage );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    _env->CallVoidMethod( objApproxSurfaceFeature, _midSetDiameterInNanoMeters, diameter );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;

    _env->CallVoidMethod( objApproxSurfaceFeature, _midSetId, id );

    if ( _env->ExceptionCheck() == JNI_TRUE )
      return;
  }
}
