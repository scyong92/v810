//////////////////////////////////////////////////////////////////////////////
// FILE   : com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler.cpp
// PURPOSE: To provide the interface for calls between Java and C++
//          for the SurfaceModeler
// AUTHOR : Vincent Wong
//////////////////////////////////////////////////////////////////////////////
#include <jni.h>
#include <cassert>
#include <string>
#include <vector>

#include "com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler.h"
#include "NativeSurfaceModeler.h"
#include "JavaArrayList.h"
#include "JavaIntegerArrayList.h"
#include "JavaPoint3dArrayList.h"
#include "JavaLineScanImageArrayList.h"
#include "JavaLineScanImageArrayList.h"
#include "JavaApproxSurfaceFeatureArrayList.h"
#include "JavaMeasuredSurfaceFeatureArrayList.h"
#include "JavaPoint2d.h"
#include "JavaPoint3d.h"
#include "JavaLineScanImage.h"
#include "SurfaceModeler.h"
#include "SurfaceModelerException.h"
#include "SurfaceModelerFileNotFoundException.h"
#include "ApproxSurfaceFeature.h"
#include "MeasuredSurfaceFeature.h"
#include "Point2d.h"
#include "Point3d.h"
#include "LineScanImage.h"

using namespace std;
using namespace vexcel;

//----------------------------------------
// Local function declarations.
//
static jlong getAddress( JNIEnv *env, jobject obj );
static void throwException( JNIEnv *env, string const exception  );


/*------------------------------------------------------------------------------
 * Class:     com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler
 * Method:    nativeCreate
 * Signature: (ZLjava/lang/String;)J
 *
 * Intent:    Construct a native surface modeler object.
 *
 * @param topSide - TRUE for top side and FALSE for bottom side.
 * @param directory - the directory where all the implementation specific files will be stored.
 * @author Vincent Wong
 */
JNIEXPORT jlong JNICALL
Java_com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler_nativeCreate( JNIEnv *env, jobject obj, jboolean topSide, jstring directory )
{
  SurfaceModeler *surfaceModeler = NULL;

  char const *szDirectory = env->GetStringUTFChars( directory, NULL );

  if ( topSide )
    surfaceModeler = SurfaceModeler::createInstance( SurfaceModeler::topSide, szDirectory );
  else
    surfaceModeler = SurfaceModeler::createInstance( SurfaceModeler::bottomSide, szDirectory );

  env->ReleaseStringUTFChars( directory, szDirectory );

  assert( surfaceModeler != NULL );

  return reinterpret_cast<jlong>( surfaceModeler );
}


/*------------------------------------------------------------------------------
 * Class:     com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler
 * Method:    nativeDestroy
 * Signature: ()V
 *
 * Intent:    Destroy the native surface modeler object.
 *
 * @author Vincent Wong
 */
JNIEXPORT void JNICALL
Java_com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler_nativeDestroy( JNIEnv *env, jobject obj )
{
  SurfaceModeler *surfaceModeler = reinterpret_cast<SurfaceModeler *>( getAddress( env, obj ) );
  assert( surfaceModeler != NULL );

  SurfaceModeler::destroyInstance( surfaceModeler );
}


/*------------------------------------------------------------------------------
 * Class:     com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler
 * Method:    nativeCalibrate
 * Signature: (Lcom/agilent/mtd/agt5dx/business/surfaceMapping/surfaceModeler/LineScanImage;Lcom/agilent/mtd/agt5dx/business/surfaceMapping/surfaceModeler/LineScanImage;ILjava/util/ArrayList;)V
 *
 * Intent:    Calibrate the surface modeler system with the front and rear images
 *            of the calibration rig and the machine coordinates of the control points on
 *            the rig. The calibration information will be stored in the directory
 *            specified in the constructor.
 *
 * @param objFrontImage - front linescan image.
 * @param objRearImage - rear linescan image.
 * @param calibrationRigID - ID of the calibration rig.
 * @param objPointsInNanoMeters - an ArrayList of Point3d of the points on the calibration rig.
 * @author Vincent Wong
 */
JNIEXPORT void JNICALL Java_com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler_nativeCalibrate
  ( JNIEnv *env, jobject obj,
    jobject objFrontImage, jobject objRearImage,
    jint calibrationRigID, jobject objPointsInNanoMeters )
{
  //----------------------------------------
  // Prepare input front image.
  //
  LineScanImage frontImage;
  JavaLineScanImage jFrontImage( env, objFrontImage );
  jFrontImage.getLineScanImage( frontImage );

  //----------------------------------------
  // Prepare input rear image.
  //
  LineScanImage rearImage;
  JavaLineScanImage jRearImage( env, objRearImage );
  jRearImage.getLineScanImage( rearImage );

  //----------------------------------------
  // Prepare input 3D coordinate vector.
  //
  vector<Point3d *> calibrationPoints;
  JavaPoint3dArrayList point3dArrayList( env, objPointsInNanoMeters );
  point3dArrayList.getVector( calibrationPoints );

  //----------------------------------------
  // Make the native call.
  //
  SurfaceModeler *surfaceModeler = reinterpret_cast<SurfaceModeler *>( getAddress( env, obj ) );
  assert( surfaceModeler != NULL );

  try
  {
    surfaceModeler->calibrate(
      frontImage, rearImage, calibrationRigID, calibrationPoints );
  }
  catch ( SurfaceModelerFileNotFoundException const& handler )
  {
    throwException( env, CLASS_SurfaceModelerOutOfMemoryException );
  }
}


/*------------------------------------------------------------------------------
 * Class:     com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler
 * Method:    nativeVerifyCalibration
 * Signature: (Lcom/agilent/mtd/agt5dx/business/surfaceMapping/surfaceModeler/LineScanImage;Lcom/agilent/mtd/agt5dx/business/surfaceMapping/surfaceModeler/LineScanImage;ILjava/util/ArrayList;)V
 *
 * Intent:    Verify the calibration the surface modeler system with the front and rear images
 *            of the calibration rig and the machine coordinates of the control points on
 *            the rig. This method should be called with different data after a call to the
 *            calibrate method in order to verify the validity of the calibration.
 *
 * @param objFrontImage - front linescan image.
 * @param objRearImage - rear linescan image.
 * @param calibrationRigID - ID of the calibration rig.
 * @param objPointsInNanoMeters - an ArrayList of Point3d - the points on the calibration rig.
 * @author Vincent Wong
 */
JNIEXPORT void JNICALL
Java_com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler_nativeVerifyCalibration
  ( JNIEnv *env, jobject obj,
    jobject objFrontImage, jobject objRearImage,
    jint calibrationRigID, jobject objPointsInNanoMeters )
{
  //----------------------------------------
  // Prepare input front image.
  //
  LineScanImage frontImage;
  JavaLineScanImage jFrontImage( env, objFrontImage );
  jFrontImage.getLineScanImage( frontImage );

  //----------------------------------------
  // Prepare input rear image.
  //
  LineScanImage rearImage;
  JavaLineScanImage jRearImage( env, objRearImage );
  jRearImage.getLineScanImage( rearImage );

  //----------------------------------------
  // Prepare input 3D coordinate vector.
  //
  vector<Point3d *> calibrationPoints;
  JavaPoint3dArrayList point3dArrayList( env, objPointsInNanoMeters );
  point3dArrayList.getVector( calibrationPoints );

  //----------------------------------------
  // Make the native call.
  //
  SurfaceModeler *surfaceModeler = reinterpret_cast<SurfaceModeler *>( getAddress( env, obj ) );
  assert( surfaceModeler != NULL );

  try
  {
    surfaceModeler->verifyCalibration(
      frontImage, rearImage, calibrationRigID, calibrationPoints );
  }
  catch ( SurfaceModelerFileNotFoundException const& handler )
  {
    throwException( env, CLASS_SurfaceModelerOutOfMemoryException );
  }
}


/*------------------------------------------------------------------------------
 * Class:     com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler
 * Method:    nativeIdentifyFeatures
 * Signature: (Ljava/util/ArrayList;I;Lcom/agilent/mtd/agt5dx/business/surfaceMapping/surfaceModeler/LineScanImage;Lcom/agilent/mtd/agt5dx/business/surfaceMapping/surfaceModeler/LineScanImage;ILjava/util/ArrayList;)V
 *
 * Intent:    Identify the surface features of specified feature sizes from the front and
 *            rear linescan images. The images can be binarized with the specified grey level
 *            threshold value to separate the features (vias) from the PCB background.
 *            The method will clear the output ArrayLists on entry if they are not
 *            already empty.
 *
 * @param objFeatureDiameter - an ArrayList of Integer - the acceptable feature diameter (via) in nano-meters.
 * @param tolerancePercentage - an integer specifying the percentage tolerance of the feature diameter.
 * @param objFrontImage - front linescan image.
 * @param objRearImage - rear linescan image.
 * @param greyLevelThreshold - a grey level between 0 and 255 specifying the binarization threshold value.
 * @param objSwathRegistrationImage - an ArrayList of LineScanImage returning the registration images.
 * @param objIdentifiedFeatures - an ArrayList of ApproxSurfaceFeature returning the identified surface features.
 * @author Vincent Wong
 */
JNIEXPORT void JNICALL
Java_com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler_nativeIdentifyFeatures
  ( JNIEnv *env, jobject obj,
    jobject objFeatureDiameter,
    jint tolerancePercentage,
    jobject objFrontImage, jobject objRearImage,
    jint greyLevelThreshold,
    jobject objSwathRegistrationImage,
    jobject objIdentifiedFeatures )
{
  //----------------------------------------
  // Prepare input feature diameter vector. 
  //
  vector<int> featureDiameters;
  JavaIntegerArrayList integerArrayList( env, objFeatureDiameter );
  integerArrayList.getVector( featureDiameters );

  //----------------------------------------
  // Prepare input front image.
  //
  LineScanImage frontImage;
  JavaLineScanImage jFrontImage( env, objFrontImage );
  jFrontImage.getLineScanImage( frontImage );

  //----------------------------------------
  // Prepare input rear image.
  //
  LineScanImage rearImage;
  JavaLineScanImage jRearImage( env, objRearImage );
  jRearImage.getLineScanImage( rearImage );

  //----------------------------------------
  // Make the native call.
  //
  vector<LineScanImage *> swathRegistrationImage;
  vector<ApproxSurfaceFeature *> identifiedFeatures;

  SurfaceModeler *surfaceModeler = reinterpret_cast<SurfaceModeler *>( getAddress( env, obj ) );
  assert( surfaceModeler != NULL );

  try
  {
    surfaceModeler->identifyFeatures( featureDiameters,
                                      tolerancePercentage,
                                      frontImage,
                                      rearImage,
                                      greyLevelThreshold, 
                                      swathRegistrationImage,
                                      identifiedFeatures );
  }
  catch ( SurfaceModelerFileNotFoundException const& handler )
  {
    throwException( env, CLASS_SurfaceModelerOutOfMemoryException );
  }

  //----------------------------------------
  // Prepare output swath registration image array list.
  //
  JavaLineScanImageArrayList lineScanImageArrayList( env, swathRegistrationImage );
  lineScanImageArrayList.getArrayList( objSwathRegistrationImage );

  //----------------------------------------
  // Prepare output identified features array list.
  //
  JavaApproxSurfaceFeatureArrayList approxSurfaceFeatureArrayList( env, identifiedFeatures );
  approxSurfaceFeatureArrayList.getArrayList( objIdentifiedFeatures );
}


/*------------------------------------------------------------------------------
 * Class:     com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler
 * Method:    nativeMeasureFeatures
 * Signature: (Lcom/agilent/mtd/agt5dx/business/surfaceMapping/surfaceModeler/LineScanImage;Lcom/agilent/mtd/agt5dx/business/surfaceMapping/surfaceModeler/LineScanImage;ILjava/util/ArrayList;)V
 *
 * Intent:    Measure the surface features from the front and rear linescan images.
 *            The images can be binarized with the specified grey level threshold value
 *            to separate the features (vias) from the PCB background. The method will
 *            clear the output ArrayList on entry if it is not already empty.
 *
 * @param objFrontImage - front linescan image.
 * @param objRearImage - rear linescan image.
 * @param greyLevelThreshold - a grey level between 0 and 255 specifying the binarization threshold value.
 * @param objSwathRegistrationImage - an ArrayList of LineScanImage specifyng the registration images.
 * @param objIdentifiedFeatures - an ArrayList of ApproxSurfaceFeature specifying the approximately identified surface features.
 * @param objFeaturesToMeasure - an ArrayList of MeasuredSurfaceFeature returning the identified surface features.
 * @author Vincent Wong
 */
JNIEXPORT void JNICALL
Java_com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler_nativeMeasureFeatures
  ( JNIEnv *env, jobject obj,
    jobject objFrontImage, jobject objRearImage,
    jint greyLevelThreshold,
    jobject objSwathRegistrationImage,
    jobject objIdentifiedFeatures,
    jobject objFeaturesToMeasure )
{
  //----------------------------------------
  // Prepare input front image.
  //
  LineScanImage frontImage;
  JavaLineScanImage jFrontImage( env, objFrontImage );
  jFrontImage.getLineScanImage( frontImage );


  //----------------------------------------
  // Prepare input rear image.
  //
  LineScanImage rearImage;
  JavaLineScanImage jRearImage( env, objRearImage );
  jRearImage.getLineScanImage( rearImage );


  //----------------------------------------
  // Prepare input swath registration image vector.
  //
  vector<LineScanImage *> swathRegistrationImage;
  JavaLineScanImageArrayList lineScanImageArrayList( env, objSwathRegistrationImage );
  lineScanImageArrayList.getVector( swathRegistrationImage );

  //----------------------------------------
  // Prepare input identified features array list.
  //
  vector<ApproxSurfaceFeature *> identifiedFeatures;
  JavaApproxSurfaceFeatureArrayList approxSurfaceFeatureArrayList( env, objIdentifiedFeatures );
  approxSurfaceFeatureArrayList.getVector( identifiedFeatures );

  //----------------------------------------
  // Make the native call.
  //
  vector<MeasuredSurfaceFeature *> featuresToMeasure;

  SurfaceModeler *surfaceModeler = reinterpret_cast<SurfaceModeler *>( getAddress( env, obj ) );
  assert( surfaceModeler != NULL );

  try
  {
    surfaceModeler->measureFeatures( frontImage,
                                     rearImage,
                                     greyLevelThreshold, 
                                     swathRegistrationImage,
                                     identifiedFeatures,
                                     featuresToMeasure );
  }
  catch ( SurfaceModelerFileNotFoundException const& handler )
  {
    throwException( env, CLASS_SurfaceModelerOutOfMemoryException );
  }

  //----------------------------------------
  // Prepare output measured features array list.
  //
  JavaMeasuredSurfaceFeatureArrayList measuredSurfaceFeatureArrayList( env, featuresToMeasure );
  measuredSurfaceFeatureArrayList.getArrayList( objFeaturesToMeasure );
}


/*------------------------------------------------------------------------------
 * Class:     com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler
 * Method:    nativeSetupSurfaceModel
 * Signature: (Ljava/util/ArrayList;Ljava/util/ArrayList;)V
 *
 * Intent:    Construct a surface model from the list of points. Any points that look
 *            to be bad will be ignored unless the doNotIgnore flag is set to true.
 *            The method will clear the output ArrayList on entry if it is not already empty.
 *
 * @param objFeaturePoints - an ArrayList of MeasuredSurfaceFeature specifying the points for constructing the surface model.
 * @param objIgnoredPoints - an ArrayList of MeasuredSurfaceFeature returning the points not used in constructing the surface model.
 * @author Vincent Wong
 */
JNIEXPORT void JNICALL Java_com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler_nativeSetupSurfaceModel__Ljava_util_ArrayList_2Ljava_util_ArrayList_2
  ( JNIEnv *env,
    jobject obj,
    jobject objFeaturePoints,
    jobject objIgnoredPoints )
{
  //----------------------------------------
  // Prepare input measured features array list.
  //
  vector<MeasuredSurfaceFeature *> featurePoints;
  JavaMeasuredSurfaceFeatureArrayList featurePointsArrayList( env, objFeaturePoints );
  featurePointsArrayList.getVector( featurePoints );

  //----------------------------------------
  // Make the native call.
  //
  vector<MeasuredSurfaceFeature *> ignoredPoints;

  SurfaceModeler *surfaceModeler = reinterpret_cast<SurfaceModeler *>( getAddress( env, obj ) );
  assert( surfaceModeler != NULL );

  try
  {
    surfaceModeler->setupSurfaceModel( featurePoints,
                                       ignoredPoints );
  }
  catch ( SurfaceModelerException const& handler )
  {
    throwException( env, CLASS_SurfaceModelerOutOfMemoryException );
  }

  //----------------------------------------
  // Prepare output ignored features array list.
  //
  JavaMeasuredSurfaceFeatureArrayList ignoredPointsArrayList( env, ignoredPoints );
  ignoredPointsArrayList.getArrayList( objIgnoredPoints );
}


/*------------------------------------------------------------------------------
 * Class:     com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler
 * Method:    nativeSetupSurfaceModel
 * Signature: (Lcom/agilent/mtd/agt5dx/business/surfaceMapping/surfaceModeler/SurfaceModeler;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
 *
 * Intent:    Construct a surface model from the surface model constructed with the method to
 *            setupSurfaceModel which takes only 2 parameters. In this method, the surface model is
 *            constructed based on the thickness of the surface. Any points that look to be bad will
 *            be ignored unless the doNotIgnore flag is set to true. The method will clear the output
 *            ArrayList on entry if it is not already empty.
 *
 * @param objSurfaceModeler - the surface model constructed with the method call to setupSurfaceModel which takes only 2 parameters.
 * @param objThicknessPoints - an ArrayList of Point3d specifying thickness of the surface.
 * @param objIgnoredPoints - an ArrayList of MeasuredSurfaceFeature returning the points not used in constructing the surface model.
 * @author Vincent Wong
 */
JNIEXPORT void JNICALL Java_com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler_nativeSetupSurfaceModel__Lcom_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_SurfaceModeler_2Ljava_util_ArrayList_2Ljava_util_ArrayList_2
  ( JNIEnv *env,
    jobject obj,
    jobject objSurfaceModel,
    jobject objThicknessPoints,
    jobject objIgnoredPoints )
{
  //----------------------------------------
  // Prepare input thickness points arrary list.
  //
  vector<Point3d *> thicknessPoints;
  JavaPoint3dArrayList point3dArrayList( env, objThicknessPoints );
  point3dArrayList.getVector( thicknessPoints );

  //----------------------------------------
  // Get the native address of the input surface model.
  //
  jclass clsObject = env->GetObjectClass( objSurfaceModel );
  assert( clsObject != NULL );

  jclass clsSurfaceModeler = env->FindClass( CLASS_SurfaceModeler.c_str() );
  assert( clsSurfaceModeler != NULL );
  assert( clsSurfaceModeler != clsObject );

  string signature = "()L" + CLASS_NativeSurfaceModeler + ";";

  jmethodID midGetNativeSurfaceModeler = env->GetMethodID(
			clsObject, "getNativeSurfaceModeler", signature.c_str() );
  assert( midGetNativeSurfaceModeler != NULL );

  jobject objNativeSurfaceModeler = env->CallObjectMethod( objSurfaceModel, midGetNativeSurfaceModeler );
  assert( env->ExceptionCheck() != JNI_TRUE );

  SurfaceModeler *otherSideModel = reinterpret_cast<SurfaceModeler *>(
					getAddress( env, objNativeSurfaceModeler ) );
  assert( otherSideModel != NULL );

  //----------------------------------------
  // Make the native call.
  //
  vector<MeasuredSurfaceFeature *> ignoredPoints;

  SurfaceModeler *surfaceModeler = reinterpret_cast<SurfaceModeler *>( getAddress( env, obj ) );
  assert( surfaceModeler != NULL );

  try
  {
    surfaceModeler->setupSurfaceModel( *otherSideModel,
                                       thicknessPoints,
                                       ignoredPoints );
  }
  catch ( SurfaceModelerException const& handler )
  {
    throwException( env, CLASS_SurfaceModelerOutOfMemoryException );
  }

  //----------------------------------------
  // Prepare output ignored features array list.
  //
  JavaMeasuredSurfaceFeatureArrayList ignoredPointsArrayList( env, ignoredPoints );
  ignoredPointsArrayList.getArrayList( objIgnoredPoints );
}


/*------------------------------------------------------------------------------
 * Class:     com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler
 * Method:    nativeGetHeight
 * Signature: ()I
 *
 * Intent:    Get the height (z) in nano-meters of the give point (x, y) in machine coordinates.
 *            based on the surface model that was set up with th setupSurfaceModel method call.
 *
 * @param feature - the position of interest.
 * @return the height (z) in nano-meters.
 * @author Vincent Wong
 */
JNIEXPORT jint JNICALL
Java_com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler_nativeGetHeight( JNIEnv *env, jobject obj, jobject objPoint2d )
{
  Point2d feature;
  JavaPoint2d jFeature( env, objPoint2d );
  jFeature.getPoint2d( feature );

  int height;

  SurfaceModeler *surfaceModeler = reinterpret_cast<SurfaceModeler *>( getAddress( env, obj ) );
  assert( surfaceModeler != NULL );

  //----------------------------------------
  // Make the native call.
  //
  try
  {
    height = surfaceModeler->getHeight( feature );
  }
  catch ( SurfaceModelerException const& handler )
  {
    throwException( env, CLASS_SurfaceModelerOutOfMemoryException );
  }

  return height;
}


/*------------------------------------------------------------------------------
 * Class:     com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler
 * Method:    nativeGetImage
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL
Java_com_axi_mtd_agt5dx_business_surfaceMapping_surfaceModeler_nativeInt_NativeSurfaceModeler_nativeGetImage( JNIEnv *, jclass )
{
  unsigned char *bpImage = new unsigned char[8];

  bpImage[0] = 'i';
  bpImage[1] = 'm';
  bpImage[2] = 'a';
  bpImage[3] = 'g';
  bpImage[4] = 'e';

  return reinterpret_cast<jlong>( bpImage );
}


/*------------------------------------------------------------------------------
 * Function:  getAddress
 * Intent:    Get the native address of the surface modeler from the NativeSurfaceModeler object.
 *
 * @return the native address of the surface modeler.
 * @author Vincent Wong
 */
static jlong
getAddress( JNIEnv *env, jobject obj )
{
  jclass clsObject = env->GetObjectClass( obj );
  assert( clsObject != NULL );

  jmethodID midGetAddress = env->GetMethodID( clsObject, "getAddress", "()J" );
  assert( midGetAddress != NULL );

  jlong address = env->CallLongMethod( obj, midGetAddress );
  assert( env->ExceptionCheck() != JNI_TRUE );

  return address;
}


/*------------------------------------------------------------------------------
 * Function:  throwException
 * Intent:    Throw an exception by name.
 *
 * @author Vincent Wong
 */
static void
throwException( JNIEnv *env, string const exception  )
{
  jclass cls = env->FindClass( exception.c_str() );
  assert( cls != NULL );

  env->ThrowNew( cls, "Thrown from JNI code" );
  env->DeleteLocalRef( cls );
}
