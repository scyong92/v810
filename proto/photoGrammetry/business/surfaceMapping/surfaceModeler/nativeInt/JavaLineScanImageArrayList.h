#ifndef SURFACEMODELER_NATIVEINT_JAVALINESCANIMAGEARRAYLIST_H
#define SURFACEMODELER_NATIVEINT_JAVALINESCANIMAGEARRAYLIST_H

#include <jni.h>
#include <vector>

#include "LineScanImage.h"

////////////////////////////////////////////////////////////////////////////////
// Provide translation between std::vector<vexcel::LineScanImage *> and
// Java ArrayList of LineScanImage.
//
// @author Vincent Wong
//
class JavaLineScanImageArrayList
{
  public:
    ////////////////////////////////////////////////////////////////////////////////
    // Construct a JavaLineScanImageArrayList C++ object with a Java ArrayList of
    // LineScanImage object.
    //
    // @param env - pointer to JNIEnv.
    // @param objArrayList - an ArrayList of Point3d object.
    // @author Vincent Wong
    //
    JavaLineScanImageArrayList( JNIEnv *env, jobject const objArrayList );

    ////////////////////////////////////////////////////////////////////////////////
    // Construct a JavaLineScanImageArrayList C++ object with a
    // std::vector<LineScanImage *> C++ object.
    //
    // @param env - pointer to JNIEnv.
    // @param objArrayList - an ArrayList of Point3d object.
    // @author Vincent Wong
    //
    JavaLineScanImageArrayList( JNIEnv *env, std::vector<vexcel::LineScanImage *> const& imageVector );

    ////////////////////////////////////////////////////////////////////////////////
    // @author Vincent Wong
    //
    ~JavaLineScanImageArrayList();

    ////////////////////////////////////////////////////////////////////////////////
    // Translate from a Java ArrayList of LineScanImage object to a
    // std::vector<vexcel::LineScanImage *> C++ object.
    // 
    // The method will clear the output vector on entry if it is not already empty.
    //
    // @param lineScanImageVector - a vector of LineScanImage.
    // @author Vincent Wong
    //
    void getVector( std::vector<vexcel::LineScanImage *>& lineScanImageVector );

    ////////////////////////////////////////////////////////////////////////////////
    // Translate from a std::vector<vexcel::LineScanImage *> C++ object to a 
    // Java ArrayList of LineScanImage object.
    // 
    // The method will clear the output array list on entry if it is not already empty.
    //
    // @param objArrayList - an array list of LineScanImage object.
    // @author Vincent Wong
    //
    void getArrayList( jobject objArrayList );

  private:
    JavaLineScanImageArrayList( JavaLineScanImageArrayList const& rhs );
    JavaLineScanImageArrayList& operator=( JavaLineScanImageArrayList const& rhs );

    JNIEnv *_env;
    jobject _objArrayList;
    std::vector<vexcel::LineScanImage *> _imageVector;
    vexcel::LineScanImage *_lineScanImageArray;

    jmethodID _midGetWidthInPixels;
    jmethodID _midGetLengthInPixels;
    jmethodID _midGetStartPoint;
    jmethodID _midGetEndPoint;
    jmethodID _midGetArray;

    jmethodID _midSetArray;
    jmethodID _midSetStartPoint;
    jmethodID _midSetEndPoint;
    jmethodID _midPoint2dInit;
    jmethodID _midPoint2dSet;
};

#endif SURFACEMODELER_NATIVEINT_JAVALINESCANIMAGEARRAYLIST_H
