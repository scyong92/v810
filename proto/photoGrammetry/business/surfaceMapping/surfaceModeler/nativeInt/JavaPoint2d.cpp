#include <cassert>

#include "NativeSurfaceModeler.h"
#include "JavaPoint2d.h"

////////////////////////////////////////////////////////////////////////////////
// Construct a JavaPoint2d C++ object with a Point2d Java object.
//
// @param env - pointer to JNIEnv.
// @param objPoint2d - a Point2d object.
// @author Vincent Wong
//
JavaPoint2d::JavaPoint2d( JNIEnv *env, jobject const objPoint2d )
{
  _env        = env;
  _objPoint2d = objPoint2d;

  jclass clsObject = env->GetObjectClass( _objPoint2d );
  assert( clsObject != NULL );

  _clsPoint2d = _env->FindClass( CLASS_Point2d.c_str() );
  assert( _clsPoint2d != NULL );
  assert( _clsPoint2d != clsObject );

  env->DeleteLocalRef( clsObject );

  _midGetX = NULL;
  _midGetY = NULL;
  _midSet  = NULL;
}


////////////////////////////////////////////////////////////////////////////////
// @author Vincent Wong
//
JavaPoint2d::~JavaPoint2d()
{
  // do nothing
}


////////////////////////////////////////////////////////////////////////////////
// Set the vexcel::Point2d object based on the Java Point2d object.
//
// @author Vincent Wong
//
void
JavaPoint2d::getPoint2d( vexcel::Point2d& point2d )
{
  int x, y;

  get( x, y );
  point2d.set( x, y );
}


////////////////////////////////////////////////////////////////////////////////
// Get the coordinates.
//
// @author Vincent Wong
//
void
JavaPoint2d::get( int& x, int& y ) 
{
  if ( _midGetX == NULL )
  {
    _midGetX = _env->GetMethodID( _clsPoint2d, "getX", "()I" );
    assert( _midGetX != NULL );

    _midGetY = _env->GetMethodID( _clsPoint2d, "getY", "()I" );
    assert( _midGetY != NULL );
  }

  x = _env->CallIntMethod( _objPoint2d, _midGetX );
  assert( _env->ExceptionCheck() != JNI_TRUE );

  y = _env->CallIntMethod( _objPoint2d, _midGetY );
  assert( _env->ExceptionCheck() != JNI_TRUE );
}
