package com.agilent.xRayTest.business.surfaceMapping.surfaceModeler.nativeInt;

import java.util.*;
import com.agilent.xRayTest.business.surfaceMapping.surfaceModeler.*;

public class NativeSurfaceModeler
{
  {
    System.loadLibrary( "nativeSurfaceModeler" );
  }

  /**
  * Construct a native surface modeler object.
  *
  * @param topSide - TRUE for top side and FALSE for bottom side.
  * @param surfaceModelerDirectory - the directory where all the implementation specific files will be stored.
  * @author Vincent Wong
  */
  public NativeSurfaceModeler( boolean topSide,
                               final String surfaceModelerDirectory ) throws SurfaceModelerException
  {
    address = nativeCreate( topSide, surfaceModelerDirectory );
  }

  /**
  * Create a native surface modeler object.
  *
  * @param topSide - TRUE for top side and FALSE for bottom side.
  * @param surfaceModelerDirectory - the directory where all the implementation specific files will be stored.
  * @return the address of the native object.
  * @author Vincent Wong
  */
  private native long
  nativeCreate( boolean topSide,
                String surfaceModelerDirectory ) throws SurfaceModelerException;

  /**
  * Destroy the native surface modeler object.
  *
  * @author Vincent Wong
  */
  private native void
  nativeDestroy();

  /**
  * Dispose the resources allocated for this object.
  *
  * @author Vincent Wong
  */
  public void dispose()
  {
    nativeDestroy();
  }

  /**
  * @author Vincent Wong
  */
  protected void finalize()
  {
    dispose();
  }

  /**
  * Calibrate the surface modeler system with the front and rear images
  * of the calibration rig and the machine coordinates of the control points on
  * the rig. The calibration information will be stored in the directory
  * specified in the constructor.
  *
  * @param frontImage - front linescan image.
  * @param rearImage - rear linescan image.
  * @param calibrationRigID - ID of the calibration rig.
  * @param pointsInNanoMeters - an ArrayList of Point3d of the points on the calibration rig.
  * @author Vincent Wong
  */
  public native void
  nativeCalibrate( final LineScanImage frontImage,
                   final LineScanImage rearImage,
                   int calibrationRigID,
                   final ArrayList pointsInNanoMeters ) throws SurfaceModelerException;

  /**
  * Verify the calibration the surface modeler system with the front and rear images
  * of the calibration rig and the machine coordinates of the control points on
  * the rig. This method should be called with different data after a call to the
  * calibrate method in order to verify the validity of the calibration.
  *
  * @param frontImage - front linescan image.
  * @param rearImage - rear linescan image.
  * @param calibrationRigID - ID of the calibration rig.
  * @param pointsInNanoMeters - an ArrayList of Point3d - the points on the calibration rig.
  * @author Vincent Wong
  */
  public native void
  nativeVerifyCalibration( final LineScanImage frontImage,
                           final LineScanImage rearImage,
                           int calibrationRigID,
                           final ArrayList pointsInNanoMeters ) throws SurfaceModelerException;

  /**
  * Identify the surface features of specified feature sizes from the front and rear linescan images.
  * The images can be binarized with the specified grey level threshold value to separate the features (vias)
  * from the PCB background. The method will clear the output ArrayLists on entry if they are not
  * already empty.
  *
  * @param featureDiameter - an ArrayList of Integer - the acceptable feature diameter (via) in nano-meters.
  * @param tolerancePercentage - an integer specifying the percentage tolerance of the feature diameter.
  * @param frontImage - front linescan image.
  * @param rearImage - rear linescan image.
  * @param greyLevelThreshold - a grey level between 0 and 255 specifying the binarization threshold value.
  * @param swathRegistrationImage - an ArrayList of LineScanImage returning the registration images.
  * @param identifiedFeatures - an ArrayList of ApproxSurfaceFeature returning the identified surface features.
  * @author Vincent Wong
  */
  public native void
  nativeIdentifyFeatures( final ArrayList featureDiameterInNanoMeters,
                          int tolerancePercentage,
                          final LineScanImage frontImage,
                          final LineScanImage rearImage,
                          int greyLevelThreshold,
                          ArrayList swathRegistrationImage,
                          ArrayList identifiedFeatures ) throws SurfaceModelerException;

  /**
  * Measure the surface features from the front and rear linescan images.
  * The images can be binarized with the specified grey level threshold value to separate the features (vias)
  * from the PCB background. The method will clear the output ArrayList on entry if it is not
  * already empty.
  *
  * @param frontImage - front linescan image.
  * @param rearImage - rear linescan image.
  * @param greyLevelThreshold - a grey level between 0 and 255 specifying the binarization threshold value.
  * @param swathRegistrationImage - an ArrayList of LineScanImage specifyng the registration images.
  * @param identifiedFeatures - an ArrayList of ApproxSurfaceFeature specifying the approximately identified surface features.
  * @param featuresToMeasure - an ArrayList of MeasuredSurfaceFeature returning the identified surface features.
  * @author Vincent Wong
  */
  public native void
  nativeMeasureFeatures( final LineScanImage frontImage,
                         final LineScanImage rearImage,
                         int greyLevelThreshold,
                         final ArrayList swathRegistrationImage,
                         final ArrayList identifiedFeatures,
                         ArrayList featuresToMeasure ) throws SurfaceModelerException;

  /**
  * Construct a surface model from the list of points. Any points that look to be bad will be ignored
  * unless the doNotIgnore flag is set to true. The method will clear the output ArrayList on entry if
  * it is not already empty.
  *
  * @param featurePoints - an ArrayList of MeasuredSurfaceFeature specifying the points for constructing the surface model.
  * @param ignoredPoints - an ArrayList of MeasuredSurfaceFeature returning the points not used in constructing the surface model.
  * @author Vincent Wong
  */
  public native void
  nativeSetupSurfaceModel( final ArrayList featurePoints,
                           ArrayList ignoredPoints ) throws SurfaceModelerException;

  /**
  * Construct a surface model from the surface model constructed with the method to
  * setupSurfaceModel which takes only 2 parameters. In this method, the surface model is
  * constructed based on the thickness of the surface. Any points that look to be bad will
  * be ignored unless the doNotIgnore flag is set to true. The method will clear the output
  * ArrayList on entry if it is not already empty.
  *
  * @param surfaceModeler - the surface model constructed with the method call to setupSurfaceModel which takes only 2 parameters.
  * @param thicknessPoints - an ArrayList of Point3d specifying thickness of the surface.
  * @param ignoredPoints - an ArrayList of MeasuredSurfaceFeature returning the points not used in constructing the surface model.
  * @author Vincent Wong
  */
  public native void
  nativeSetupSurfaceModel( final SurfaceModeler surfaceModel,
                           final ArrayList thicknessPoints,
                           ArrayList ignoredPoints ) throws SurfaceModelerException;

  /**
  * Get the height (z) in nano-meters of the give point (x, y) in machine coordinates.
  * based on the surface model that was set up with th setupSurfaceModel method call.
  *
  * @param feature - the position of interest.
  * @return the height (z) in nano-meters.
  * @author Vincent Wong
  */
  public native int
  nativeGetHeight( final Point2d position ) throws SurfaceModelerException;

  /**
  * For testing purpose only. 
  * @author Vincent Wong
  */
  public static native long
  nativeGetImage() throws SurfaceModelerException;

  /**
  * Get the native address of the object.
  *
  * @return the native address of the object.
  * @author Vincent Wong
  */
  private long getAddress()
  {
    return address;
  }

  //----------------------------------------
  // Member variables.
  //
  private long address; // native address of the NativeSurfaceModeler object
}
