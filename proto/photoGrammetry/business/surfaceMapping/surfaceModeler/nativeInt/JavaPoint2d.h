#ifndef SURFACEMODELER_NATIVEINT_JAVAPOINT2D_H
#define SURFACEMODELER_NATIVEINT_JAVAPOINT2D_H

#include <jni.h>
#include "Point2d.h"

////////////////////////////////////////////////////////////////////////////////
// Provide translation between vexcel::Point2d and Java Point2d.
//
// @author Vincent Wong
//
class JavaPoint2d
{
  public:
    ////////////////////////////////////////////////////////////////////////////////
    // Construct a JavaPoint2d C++ object with a Point2d Java object.
    //
    // @param env - pointer to JNIEnv.
    // @param objPoint2d - a Point2d object.
    // @author Vincent Wong
    //
    JavaPoint2d( JNIEnv *env, jobject const objPoint2d );

    ////////////////////////////////////////////////////////////////////////////////
    // @author Vincent Wong
    //
    ~JavaPoint2d();

    ////////////////////////////////////////////////////////////////////////////////
    // Translate from a Point2d Java object to a vexcel::Point2d C++ object.
    //
    // @author Vincent Wong
    //
    void getPoint2d( vexcel::Point2d& point2d );

    ////////////////////////////////////////////////////////////////////////////////
    // Get the coordinates.
    //
    // @author Vincent Wong
    //
    void get( int& x, int& y );

  private:
    JavaPoint2d();
    JavaPoint2d( JavaPoint2d const& rhs );
    JavaPoint2d& operator=( JavaPoint2d const& rhs );

    JNIEnv    *_env;
    jobject   _objPoint2d;
    jmethodID _midGetX;
    jmethodID _midGetY;
    jmethodID _midSet;
    jclass    _clsPoint2d;
};

#endif // SURFACEMODELER_NATIVEINT_JAVAPOINT2D_H
