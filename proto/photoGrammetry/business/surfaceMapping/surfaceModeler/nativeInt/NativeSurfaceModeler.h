#ifndef SURFACEMODELER_NATIVEINT_NATIVESURFACEMODELER_H
#define SURFACEMODELER_NATIVEINT_NATIVESURFACEMODELER_H

#include <string>

const std::string CLASSPATH = "com/agilent/mtd/agt5dx/business/surfaceMapping/surfaceModeler/";

const std::string CLASS_Point2d                = CLASSPATH + "Point2d";
const std::string CLASS_Point3d                = CLASSPATH + "Point3d";
const std::string CLASS_LineScanImage          = CLASSPATH + "LineScanImage";
const std::string CLASS_ApproxSurfaceFeature   = CLASSPATH + "ApproxSurfaceFeature";
const std::string CLASS_MeasuredSurfaceFeature = CLASSPATH + "MeasuredSurfaceFeature";
const std::string CLASS_SurfaceModeler         = CLASSPATH + "SurfaceModeler";

const std::string CLASS_SurfaceModelerOutOfMemoryException        
                                               = CLASSPATH + "SurfaceModelerOutOfMemoryException";

const std::string CLASS_NativeSurfaceModeler   = CLASSPATH + "nativeInt/NativeSurfaceModeler";

const std::string CLASS_ArrayList              = "java/util/ArrayList";

#endif // SURFACEMODELER_NATIVEINT_NATIVESURFACEMODELER_H
