#ifndef SURFACEMODELER_NATIVEINT_JAVAINTEGERARRAYLIST_H
#define SURFACEMODELER_NATIVEINT_JAVAINTEGERARRAYLIST_H

#include <jni.h>
#include <vector>

////////////////////////////////////////////////////////////////////////////////
// Provide translation between std::vector<int> and Java ArrayList of Integer.
//
// @author Vincent Wong
//
class JavaIntegerArrayList
{
  public:
    ////////////////////////////////////////////////////////////////////////////////
    // Construct a JavaIntegerArrayList C++ object with a Java ArrayList of
    // Integer object.
    //
    // @param env - pointer to JNIEnv.
    // @param objArrayList - an ArrayList of Integer object.
    // @author Vincent Wong
    //
    JavaIntegerArrayList( JNIEnv *env, jobject const objArrayList );

    ////////////////////////////////////////////////////////////////////////////////
    // @author Vincent Wong
    //
    ~JavaIntegerArrayList();

    ////////////////////////////////////////////////////////////////////////////////
    // Translate from a Java ArrayList of Integer object to a
    // std::vector<int> C++ object.
    // 
    // The method will clear the output vector on entry if it is not already empty.
    //
    // @param intVector - a vector of int.
    // @author Vincent Wong
    //
    void getVector( std::vector<int>& intVector );

  private:
    JavaIntegerArrayList();
    JavaIntegerArrayList( JavaIntegerArrayList const& rhs );
    JavaIntegerArrayList& operator=( JavaIntegerArrayList const& rhs );

    JNIEnv    *_env;
    jobject   _objArrayList;

    jmethodID _midIntValue;
    jclass    _clsInt;
};

#endif SURFACEMODELER_NATIVEINT_JAVAINTEGERARRAYLIST_H
