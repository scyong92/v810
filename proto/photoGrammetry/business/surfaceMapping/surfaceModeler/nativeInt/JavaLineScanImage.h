#ifndef SURFACEMODELER_NATIVEINT_JAVALINESCANIMAGE_H
#define SURFACEMODELER_NATIVEINT_JAVALINESCANIMAGE_H

#include <jni.h>
#include "LineScanImage.h"

////////////////////////////////////////////////////////////////////////////////
// Provide translation between vexcel::LineScanImage and Java LineScanImage.
//
// @author Vincent Wong
//
class JavaLineScanImage
{
  public:
    ////////////////////////////////////////////////////////////////////////////////
    // Construct a JavaLineScanImage C++ object with a LineScanImage Java object.
    //
    // @param env - pointer to JNIEnv.
    // @param objLineScanImage - a Java LineScanImage object.
    // @author Vincent Wong
    //
    JavaLineScanImage( JNIEnv *env, jobject const objLineScanImage );

    ////////////////////////////////////////////////////////////////////////////////
    // Construct a JavaLineScanImage C++ object with a vexcel::LineScanImage C++ object.
    //
    // @param env - pointer to JNIEnv.
    // @param lineScanImage - a vexcel::LineScanImage C++ object.
    // @author Vincent Wong
    //
    JavaLineScanImage( JNIEnv *env, vexcel::LineScanImage const& lineScanImage );

    ////////////////////////////////////////////////////////////////////////////////
    // @author Vincent Wong
    //
    ~JavaLineScanImage();

    ////////////////////////////////////////////////////////////////////////////////
    // Translate from a LineScanImage Java object to a vexcel::LineScanImage C++ object.
    //
    // @author Vincent Wong
    //
    void getLineScanImage( vexcel::LineScanImage& lineScanImage );

    ////////////////////////////////////////////////////////////////////////////////
    // Translate from a vexcel::LineScanImage C++ object to a LineScanImage Java object.
    //
    // @author Vincent Wong
    //
    void getObject( jobject objLineScanImage );

  private:
    JavaLineScanImage();
    JavaLineScanImage( JavaLineScanImage const& rhs );
    JavaLineScanImage& operator=( JavaLineScanImage const& rhs );
    void getSize( int& width, int& length );
    void getPosition( int& startX, int& startY, int& endX, int& endY );
    void getArray( long& array );

    JNIEnv *_env;
    jobject _objLineScanImage;
    vexcel::LineScanImage _lineScanImage;

    jmethodID _midGetWidthInPixels;
    jmethodID _midGetLengthInPixels;
    jmethodID _midGetStartPoint;
    jmethodID _midGetEndPoint;
    jmethodID _midGetArray;

    jmethodID _midSetStartPoint;
    jmethodID _midSetEndPoint;
    jmethodID _midSetArray;

    jmethodID _midPoint2dInit;

    jclass _clsLineScanImage;
    jclass _clsPoint2d;
};

#endif // SURFACEMODELER_NATIVEINT_JAVALINESCANIMAGE_H
