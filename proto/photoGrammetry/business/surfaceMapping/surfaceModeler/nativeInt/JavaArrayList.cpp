#include <cassert>

#include "NativeSurfaceModeler.h"
#include "JavaArrayList.h"

////////////////////////////////////////////////////////////////////////////////
// Construct a JavaArrayList C++ object with a Java ArrayList. The array list
// should only the type of object as specified. If type is equal to an empty 
// string, only get() method can be used. The type must have a constructor with
// no parameters.
//
// @param env - pointer to JNIEnv.
// @param type - the type of object the ArrayList holds.
// @author Vincent Wong
//
JavaArrayList::JavaArrayList( JNIEnv *env, jobject objArrayList, std::string objectClass )
{
  _env = env;

  _clsArrayList = _env->FindClass( CLASS_ArrayList.c_str() );
  assert( _clsArrayList != NULL );

  if ( objArrayList != NULL )
  {
    _objArrayList = objArrayList;

    jclass clsObject = _env->GetObjectClass( _objArrayList );
    assert( clsObject != NULL );
    assert( _clsArrayList != clsObject );
    _env->DeleteLocalRef( clsObject );

    jmethodID midSize = _env->GetMethodID( _clsArrayList, "size", "()I" );
    assert( midSize );

    _size = _env->CallIntMethod( _objArrayList, midSize );
  }
  else // object not yet exist
  {
    jmethodID midJavaArrayListInit = _env->GetMethodID( _clsArrayList, "<init>", "()V" );
    assert( midJavaArrayListInit );

    _objArrayList = _env->NewObject( _clsArrayList, midJavaArrayListInit );	// constructor
    _size = 0;
  }

  if ( objectClass != "" )
  {
    _clsObject = _env->FindClass( objectClass.c_str() );
    assert( _clsObject != NULL );
    
    _midObjectInit = _env->GetMethodID( _clsObject, "<init>", "()V" );
    assert( _midObjectInit );
  }
  else
  {
    _clsObject = NULL;
    _midObjectInit = NULL;
  }

  _midJavaArrayListClear = NULL;
  _midJavaArrayListAdd = NULL;
  _midJavaArrayListGet = NULL;
}


////////////////////////////////////////////////////////////////////////////////
// @author Vincent Wong
//
JavaArrayList::~JavaArrayList()
{
  // do nothing
}


////////////////////////////////////////////////////////////////////////////////
// Get the number of objects the current array list is holding.
//
// @return the number of objects the current array list is holding.
// @author Vincent Wong
//
int
JavaArrayList::size() const
{
  return _size;
}


////////////////////////////////////////////////////////////////////////////////
// Get the class the object the array list holds.
//
// @return the class the object the array list holds.
// @author Vincent Wong
//
jclass
JavaArrayList::getObjectClass() const 
{
  return _clsObject;
}


////////////////////////////////////////////////////////////////////////////////
// Get the object of the array list at the specified index position.
// If index is greater than or equal to the number of objects in the array list,
// the method returns null.
//
// @return the object of the array list at the specified index position.
// @author Vincent Wong
//
jobject
JavaArrayList::get( int index )
{
  if ( index >= _size )
    return NULL;

  if ( _midJavaArrayListGet == NULL )
  { 
    _midJavaArrayListGet = _env->GetMethodID( _clsArrayList, "get", "(I)Ljava/lang/Object;" );
    assert( _midJavaArrayListGet );
  }

  jobject element = _env->CallObjectMethod( _objArrayList, _midJavaArrayListGet, index );

  if ( _env->ExceptionCheck() == JNI_TRUE )
    return NULL;

  return element;
}


////////////////////////////////////////////////////////////////////////////////
// Create a new object at the end of the array list and return the newly
// created object. If the method is unable to create a new object, it returns
// null.
//
// @return the newly created object.
// @author Vincent Wong
//
jobject
JavaArrayList::add()
{
  jobject element = _env->NewObject( _clsObject, _midObjectInit ); // constructor

  if ( _midJavaArrayListAdd == NULL )
  {
    _midJavaArrayListAdd = _env->GetMethodID( _clsArrayList, "add", "(Ljava/lang/Object;)Z" );
    assert( _midJavaArrayListAdd );
  }

  _env->CallBooleanMethod( _objArrayList, _midJavaArrayListAdd, element );

  if ( _env->ExceptionCheck() == JNI_TRUE )
    return NULL;

  _size++;

  return element;
}


////////////////////////////////////////////////////////////////////////////////
// Remove all of the objects from the array list.
//
// @author Vincent Wong
//
void
JavaArrayList::clear()
{
  if ( _midJavaArrayListClear == NULL )
  {
    _midJavaArrayListClear = _env->GetMethodID( _clsArrayList, "clear", "()V" );
    assert( _midJavaArrayListClear );
  }

  _env->CallVoidMethod( _objArrayList, _midJavaArrayListClear );

  if ( _env->ExceptionCheck() == JNI_TRUE )
    return;

  _size = 0;
}

