#ifndef SURFACEMODELER_NATIVEINT_JAVAPOINT3DARRAYLIST_H
#define SURFACEMODELER_NATIVEINT_JAVAPOINT3DARRAYLIST_H

#include <jni.h>
#include <vector>

#include "Point3d.h"

////////////////////////////////////////////////////////////////////////////////
// Provide translation between std::vector<vexcel::Point3d *> and
// Java ArrayList of Point3d.
//
// @author Vincent Wong
//
class JavaPoint3dArrayList
{
  public:
    ////////////////////////////////////////////////////////////////////////////////
    // Construct a JavaPoint3dArrayList C++ object with a Java ArrayList of
    // Point3d object.
    //
    // @param env - pointer to JNIEnv.
    // @param objArrayList - an ArrayList of Point3d object.
    // @author Vincent Wong
    //
    JavaPoint3dArrayList( JNIEnv *env, jobject const objArrayList );

    ////////////////////////////////////////////////////////////////////////////////
    // @author Vincent Wong
    //
    ~JavaPoint3dArrayList();

    ////////////////////////////////////////////////////////////////////////////////
    // Translate from a Java ArrayList of Point3d object to a
    // std::vector<vexcel::Point3d *> C++ object.
    // 
    // The method will clear the output vector on entry if it is not already empty.
    //
    // @param point3dVector - a vector of Point3d.
    // @author Vincent Wong
    //
    void getVector( std::vector<vexcel::Point3d *>& point3dVector );

  private:
    JavaPoint3dArrayList();
    JavaPoint3dArrayList( JavaPoint3dArrayList const& rhs );
    JavaPoint3dArrayList& operator=( JavaPoint3dArrayList const& rhs );

    JNIEnv *_env;
    jobject _objArrayList;
    vexcel::Point3d *_point3dArray;

    jmethodID _midGetX;
    jmethodID _midGetY;
    jmethodID _midGetZ;
};

#endif SURFACEMODELER_NATIVEINT_JAVAPOINT3DARRAYLIST_H
