package com.agilent.xRayTest.business.surfaceMapping.surfaceModeler;

import com.agilent.xRayTest.business.surfaceMapping.surfaceModeler.*;

/**
* This class mirrors the vexcel::ApproxSurfaceFeature class in C++.
* @author Vincent Wong
*/
public class ApproxSurfaceFeature
{
  public ApproxSurfaceFeature()
  {
  }

  /**
  * Construct an ApproxSurfaceFeature object.
  *
  * @param coordsInMachine - coordinates in the machine space of the feature.
  * @param frontCoordsInPixels - coordinates in pixels of the featue in the front linescan image.
  * @param rearCoordsInPixels - coordinates in pixels of the featue in the rear linescan image.
  * @param templateImage - template image of the feature.
  * @param diameterInNanoMeters - diameter of the feature (via) in nano-meters.
  * @author Vincent Wong
  */
  public ApproxSurfaceFeature( Point3d coordsInMachine,
                               Point2d frontCoordsInPixels, 
                               Point2d rearCoordsInPixels, 
                               LineScanImage templateImage,
                               int diameterInNanoMeters )
  {
    _machineCoords = coordsInMachine;
    _frontCoords   = frontCoordsInPixels;
    _rearCoords    = rearCoordsInPixels;
    _image         = templateImage;
    _diameter      = diameterInNanoMeters;
  }

  /**
  * Get the coordinates in the machine space of the surface feature. 
  *
  * @return coordinates in the machine space of the feature.
  * @author Vincent Wong
  */
  public Point3d getCoordsInMachine()
  {
    return _machineCoords;
  }

  /**
  * Get the coordinates in pixels of the featue in the front linescan image.
  *
  * @return coordinates in pixels of the featue in the front linescan image.
  * @author Vincent Wong
  */
  public Point2d getFrontCoordsInPixels()
  {
    return _frontCoords;
  }

  /**
  * Get the coordinates in pixels of the featue in the rear linescan image.
  *
  * @return coordinates in pixels of the featue in the rear linescan image.
  * @author Vincent Wong
  */
  public Point2d getRearCoordsInPixels()
  {
    return _rearCoords;
  }

  /**
  * Get the template image of the feature.
  *
  * @return template image of the feature.
  * @author Vincent Wong
  */
  public LineScanImage getTemplateImage()
  {
    return _image;
  }

  /**
  * Get the diameter of the feature (via) in nano-meters.
  *
  * @return diameter of the feature (via) in nano-meters.
  * @author Vincent Wong
  */
  public int getDiameterInNanoMeters()
  {
    return _diameter;
  }

  /**
  * Get the ID of the feature.
  *
  * @return ID of the feature.
  * @author Vincent Wong
  */
  public int getId()
  {
    return _id;
  }

  /**
  * Set the coordinates in the machine space of the surface feature. 
  *
  * @param coordinates in the machine space of the feature.
  * @author Vincent Wong
  */
  private void setCoordsInMachine( Point3d machineCoords )
  {
    _machineCoords = machineCoords;
  }

  /**
  * Set the coordinates in pixels of the featue in the front linescan image.
  *
  * @param coordinates in pixels of the featue in the front linescan image.
  * @author Vincent Wong
  */
  private void setFrontCoordsInPixels( Point2d frontCoords )
  {
    _frontCoords = frontCoords;
  }

  /**
  * Set the coordinates in pixels of the featue in the rear linescan image.
  *
  * @param coordinates in pixels of the featue in the rear linescan image.
  * @author Vincent Wong
  */
  private void setRearCoordsInPixels( Point2d rearCoords )
  {
    _rearCoords = rearCoords;
  }

  /**
  * Set the template image of the feature.
  *
  * @param template image of the feature.
  * @author Vincent Wong
  */
  private void setTemplateImage( LineScanImage image )
  {
    _image = image;
  }

  /**
  * Set the diameter of the feature (via) in nano-meters.
  *
  * @param diameter of the feature (via) in nano-meters.
  * @author Vincent Wong
  */
  private void setDiameterInNanoMeters( int diameter )
  {
    _diameter = diameter;
  }

  /**
  * Set the ID of the feature.
  *
  * @param ID of the feature.
  * @author Vincent Wong
  */
  public void setId( int id )
  {
    _id = id;
  }

  private Point3d       _machineCoords;
  private Point2d       _frontCoords;
  private Point2d       _rearCoords;
  private LineScanImage _image;
  private int           _diameter;
  private int           _id;
}
