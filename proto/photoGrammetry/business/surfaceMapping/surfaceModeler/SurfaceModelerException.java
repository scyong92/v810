package com.agilent.xRayTest.business.surfaceMapping.surfaceModeler;

import com.agilent.xRayTest.util.*;
import com.agilent.util.*;

/**
* If the surface modeler library returns any kind of problem this class
* or a subclass of this class will be thrown.
*
* @author Vincent Wong
*/
public class SurfaceModelerException extends XrayTesterException
{
  /**
  * Create a SurfaceModelerException.
  *
  * @author Vincent Wong
  */
  public SurfaceModelerException( LocalizedString localizedString )
  {
    super( localizedString );
  }
}
