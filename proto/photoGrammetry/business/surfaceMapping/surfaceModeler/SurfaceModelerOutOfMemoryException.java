package com.agilent.xRayTest.business.surfaceMapping.surfaceModeler;

import com.agilent.util.*;

/**
 * If there is not enough memory for the operation, this exception is thrown.
 *
 * @author Vincent Wong
 */
public class SurfaceModelerOutOfMemoryException extends SurfaceModelerException
{
  /**
   * Default constructor.
   *
   * @author Vincent Wong
   */
  public SurfaceModelerOutOfMemoryException( String message )
  {
    super( new LocalizedString("SM_OUT_OF_MEMORY_EXCEPTION_KEY", null) );
  }
}
