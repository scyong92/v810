package com.agilent.xRayTest.business.panelInspection;

import java.util.*;

import com.agilent.xRayTest.hardware.*;
import com.agilent.util.*;

/**
 * This class holds a buffer of surface map images.  It allows the ImageAnalysisThread to analyze
 * images while the machine is still gathering more images.  This class helps the
 * machine to do things in parallel.  It also makes sure that we do not use too much memory
 * holding a bunch of surface map images.  The machine can be moving, setting up the camera, and
 * snapping while the image analysis subsystem is analyzing images already collected.
 *
 * @author Bill Darbie
 */
class LineScanImageBuffer implements Observer
{
  private static LineScanImageBuffer _instance = null;
  private LineScanImageObservable _lineScanImageObservable;

  private int _maxImages = 2; // 2 sets of 2 or 4 images
  private List _unUsedImageList = new LinkedList();
  private List _inUseImageList = new LinkedList();
  private long _waitTime;
  private boolean _verbose = false;

  /**
   * @author Bill Darbie
   */
  static synchronized LineScanImageBuffer getInstance()
  {
    if (_instance == null)
      _instance = new LineScanImageBuffer();

    return _instance;
  }

  /**
   * Do not allow the constructor to be used.  Use getInstance instead.
   * @author Bill Darbie
   */
  private LineScanImageBuffer()
  {
    _lineScanImageObservable = LineScanImageObservable.getInstance();
    _lineScanImageObservable.addObserver(this);
  }

  /**
   * @author Bill Darbie
   */
  void setVerbose(boolean verbose)
  {
    _verbose = verbose;
  }

  /**
   * @author Bill Darbie
   */
  synchronized void setImages(List lineScanImages)
  {
    Assert.assert(lineScanImages != null);
    Assert.assert((lineScanImages.size() == 2) || (lineScanImages.size() == 4));

    boolean imagesAdded = false;
    while(imagesAdded == false)
    {
      int size = _unUsedImageList.size() + _inUseImageList.size();
      if (size < _maxImages)
      {
        _unUsedImageList.add(lineScanImages);
        imagesAdded = true;

        notifyAll();
      }
      else
      {
        try
        {
          if (_verbose)
            System.out.println("******* ImageBuffer.setImage waiting to set an image");

          while(size == _maxImages)
          {
            wait();
            size = _unUsedImageList.size() + _inUseImageList.size();
          }

          if (_verbose)
            System.out.println("******* ImageBuffer.setImage DONE waiting to set an image");
        }
        catch(InterruptedException io)
        {
          // do nothing
        }
      }
    }
  }

  /**
   * @return a List of SurfaceMapImages
   * @author Bill Darbie
   */
  synchronized List getImages()
  {
    List images = null;
    while (images == null)
    {
      if (_unUsedImageList.size() > 0)
      {
        images = (List)_unUsedImageList.remove(0);
        _inUseImageList.add(images);
      }
      else
      {
        try
        {
          if (_verbose)
            System.out.println("******* ImageBuffer.getImage waiting to get an image");
          long startTime = System.currentTimeMillis();
          while(_unUsedImageList.size() == 0)
            wait();
          long stopTime = System.currentTimeMillis();
          _waitTime += stopTime - startTime;
          if (_verbose)
            System.out.println("******* ImageBuffer.getImage DONE waiting to get an image");
        }
        catch (InterruptedException ex)
        {
          // do nothing
        }
      }
    }

    return images;
  }

  /**
   * @author Bill Darbie
   */
  synchronized void removeImages(List lineScanImages)
  {
    Assert.assert(lineScanImages != null);
    boolean imageRemoved = _inUseImageList.remove(lineScanImages);
    Assert.assert(imageRemoved);

    notifyAll();
  }

  /**
   * @author Bill Darbie
   */
  synchronized void clear()
  {
    _inUseImageList.clear();
    _unUsedImageList.clear();
  }

  /**
   * @author Bill Darbie
   */
  void clearWaitTime()
  {
    _waitTime = 0;
  }

  /**
   * @author Bill Darbie
   */
  long getWaitTime()
  {
    return _waitTime;
  }

  /**
   * This method gets called each time a new image is snapped by the SnapInspectionImageCommand
   * @author Bill Darbie
   */
  public synchronized void update(Observable observable, Object args)
  {
    Assert.assert(args != null);

    List images = (List)args;
    setImages(images);
  }
}
