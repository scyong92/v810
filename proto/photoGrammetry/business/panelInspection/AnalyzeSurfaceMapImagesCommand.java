package com.agilent.xRayTest.business.panelInspection;

import com.agilent.xRayTest.business.surfaceMapping.surfaceModeler.*;
import com.agilent.xRayTest.hardware.*;
import com.agilent.util.*;

import java.util.*;

/**
 * This class will grab an image from the LineScanImageBuffer when it becomes available
 * and will call code to analyze that image.
 *
 * @author Bill Darbie
 */
public class AnalyzeSurfaceMapImagesCommand extends Command
{
  private static LineScanImageBuffer _lineScanImageBuffer;
  private static SurfaceModeler _topSideSurfaceModeler;
  private static SurfaceModeler _bottomSideSurfaceModeler;

  /**
   * @author Bill Darbie
   */
  static
  {
    _lineScanImageBuffer = LineScanImageBuffer.getInstance();
    //_topSideSurfaceModeler = SurfaceModeler.getTopSideInstance();
    //_bottomSideSurfaceModeler = SurfaceModeler.getBottomSideInstance();
  }

  /**
   * @author Bill Darbie
   */
  public AnalyzeSurfaceMapImagesCommand()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  public void execute() throws HardwareException
  {
    if (UnitTest.unitTesting() == false)
      System.out.println("wpd AnalyzeSurfaceMapImagesCommand.execute stub");
    List images = (List)_lineScanImageBuffer.getImages();
    Iterator it = images.iterator();

    // call some surface map analysis code here
    if (UnitTest.unitTesting() == false)
    {
      /*
      while(it.hasNext())
      {
        if(it.hasNext() != null)
      }
*/
      //System.out.println("wpd - call surface map analysis code here");

    }

    // now that the analysis is done, remove the images from the buffer
    _lineScanImageBuffer.removeImages(images);
  }

  /**
   * @author Kristin Casterton
   */
   public void isValid() throws HardwareException
   {
    // do nothing now
    // this command never has information that would be invalid
    // so don't throw an exception
   }
}
