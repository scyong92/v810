//package com.agilent.mtd.agt5dx.imageReconstruction;
package proto.quasar.embeddedHardware.imageReconstruction;

/**
* This class provides the methods that access the image reconstruction software
* which runs on each image reconstruction processorand which is implemented in C++
* @author Horst Mueller
*/
public class ImageReconstructionEngine 
{
  private static ImageReconstructionEngine _instance = null;

  /**
  * @author Horst Mueller
  */
  private ImageReconstructionEngine()
  {
  }

  /*
  * This method ensures that only the one image reconstruction engines can be created
  * @author Horst Mueller
  */
  public static synchronized ImageReconstructionEngine getInstance()
  {
    if( _instance == null)
    {
      _instance = new ImageReconstructionEngine();
    }

    return _instances;
  }

  /*
  * This method starts the connection for the data transfer back to the main PC
  * @author Horst Mueller
  */
  public void establishConnection() throws RemoteException
  {
  }

  /**
  * This method sets hardware specific constants
  * @param xRayCameraWidhtInPixels is the width of the x-ray camera in pixels
  * @param numberOfXrayCameras is the number of x-ray cameras
  * @param maxNumberOfDataBlocksPerScanPass is the maximum number of projetion region
  *        images that can be received for a single scan pass
  * @param zDistanceFromCameraToXraySourceInNanoMeters is the distance of 
  *        the x-ray camera army from the x-ray source
  * @param zReferenceHeightsInNanoMeters is the height of the reference z-plane 
  *        defined by the system fiducial from the x-ray camera array at each magnification
  */
  public void setHardwareParameters(int cameraWidthInPixels,
                                    int numberOfCameras,
                                    int maxNumberOfDataBlocksPerScanPass,
                                    int zDistanceFromCameraToXraySourceInNanoMeters,
                                    List zReferenceHeightPerMagnificationInNanoMeters)
  {
    Assert.expect(xRayCameraWidthInPixels > 0);
    _xRayCameraWidthInPixels = xRayCameraWidthInPixels;

    Assert.expect(numberOfXrayCameras > 0);
    _numberOfXrayCameras = numberOfXrayCameras;

    Assert.expect(maxNumberOfDataBlocksPerScanPass > 0);
    _maxNumberOfDataBlocksPerScanPass = maxNumberOfDataBlocksPerScanPass;

    Assert.expect(zDistanceFromCameraToXraySourceInNanoMeters >= 0);
    _zDistanceFromCameraToXraySourceInNanoMeters = zDistanceFromCameraToXraySourceInNanoMeters;

    Assert.expect(zReferenceHeightsInNanoMeters >= 0);
    _zReferenceHeightsInNanoMeters = zReferenceHeightsInNanoMeters;
  }

  /**
  * @return the unique Id of the processor 
  * @author Horst Mueller
  */
  public int getId()
  {
  }

  /**
  * @return all the available IP addresses for this processor.  The current hardware will have
  * 2 addresses per processor.  This could change in the future.
  * @author Horst Mueller
  */
  public List getIpAddresses()
  {
  }

  /** 
  * @return the version of this image reconstruction hardware
  * @author Horst Mueller
  */
  public String getHardwareVersion()
  {
  }

  /** 
  * @return the version of software that is running on the image reconstruction hardware
  * @author Horst Mueller
  */
  public String getSoftwareVersion()
  {
  }

  /**
  * This method sets the physical location of each x-ray camera. Note that the method should be called once before 
  * each scan
  * @param xRayCameraPositionData is a List XRayCameraPositionData of that contains the location of each camera
  * @author Horst Mueller
  */
  public void setXrayCameraPositionData(List xRayCameraPositionData)
  {
    Assert.expect(xRayCameraPositionData != null);
  }

  /**
  * This method updates the fiducial locations in each projection.
  * This method should be called once before each scan. The system fiducial is scanned for each magnification at the
  * beginning of the scan. A designated node will locate the fiducial in each projection and pass the result back to the
  * reconstruction data handler as one of the observers which then distributes the data to all other nodes.
  * @param systemFiducialData contains the location of the system fiducial in each projection
  * @author Horst Mueller
  */
  public void setSystemFiducialData(SystemFiducialData systemFiducialData)
  {
    Assert.expect(systemFiducialData != null);
  }

  /**
  * This method updates the magnification calibration data.
  * This method should be called once before each scan. A designated node will locate the magnification fiducial in each
  * projection and pass the result back to the image acquisition engine which then distributes the data to all other nodes.
  * @param magnificationCalibrationData contains the result data of the magnification calibration
  * @author Horst Mueller
  */
  public void setMagnificationCalibrationData(MagnificationCalibrationData magnificationCalibrationData)
  {
    Assert.expect(magnificationCalibrationData != null);
  }


  /**
  * This method sets the x location for of each scan path. The x location is of a scan pass is defined as
  * the distance between the x-ray spot and the panel in place sensor. This method should be called 
  * once before each scan path.
  * @param scanPassXlocationInNanoMeters is a List of Integers that contain the x coordinates of each scan pass
  * @author Horst Mueller
  */
  public void setScanPassXlocationInNanoMeters(List scanPassXlocationInNanoMeters)
  {
    Assert.expect(scanPassXlocationInNanoMeters != null);
  }

  /**
  * This method initializes the reconstruction process. It should be called before each scan path.
  * The parameter list includes a searchRegion for the system fiducial. This method should be used to
  * initialize the image reconstruction processor which receives images containing the system fiducial.
  * @param magnificationEnum is the target magnification
  * @param listOfAlignmentRegions is a List of AlignmentRegions for the entire panel
  * @param searchRegionForSystemFiducial is the search region in which the system fiducial is located
  * @param panelLengthInNanoMeters is the panel length in the x direction
  * @param scanPassWidthInNanoMeters is the width of a scan pass
  * @return the system fiducial data 
  * @author Horst Mueller
  */
  public SystemFiducialData initializeReconstruction(MagnificationEnum magnificationEnum,
                                                     List alignmentRegions, 
                                                     Rectangle searchRegionForSystemFiducial,
                                                     int panelLengthInNanoMeters,
                                                     int panelThicknessInNanoMeters, 
                                                     int scanPassWidthInNanoMeters)
  {
    Assert.expect(magnificationEnum != null);
    Assert.expect(alignmentRegions != null);
    Assert.expect(searchRegioniForSystemFiducial != null);
    Assert.expect(panelLengthInNanoMeters > 0);
    Assert.expect(panelThicknessInNanoMeters > 0);
    Assert.expect(scanPassWidthInNanoMeters > 0);
  }

  /**
  * This method initializes the reconstruction process. It should be called before each scan path.
  * This version does not include a searchRegion for the system fiducial which is located by only
  * one of the image reconstruction processors.
  * @param magnificationEnum is the target magnification
  * @param listOfAlignmentRegions is a List of AlignmentRegions for the entire panel
  * @param panelLengthInNanoMeters is the panel length in the x direction
  * @param scanPassWidthInNanoMeters is the width of a scan pass
  * @author Horst Mueller
  */
  public void initializeReconstruction(MagnificationEnum magnificationEnum,
                                       List alignmentRegions, 
                                       int panelLengthInNanoMeters,
                                       int panelThicknessInNanoMeters, 
                                       int scanPassWidthInNanoMeters)
  {
    Assert.expect(magnificationEnum != null);
    Assert.expect(alignmentRegions != null);
    Assert.expect(searchRegioniForSystemFiducial != null);
    Assert.expect(panelLengthInNanoMeters > 0);
    Assert.expect(panelThicknessInNanoMeters > 0);
    Assert.expect(scanPassWidthInNanoMeters > 0);
  }

  /**
  * This method initializes the reconstruction process for a scan path at a different magnification
  * It should be called when the magnification is changed during an inspection.
  * @param magnification is the target magnification
  * @param searchRegionForSystemFiducial is the search region in which the system fiducial is located
  * @param scanPassWidthInNanoMeters is the width of a scan pass
  * @author Horst Mueller
  */
  public void initializeReconstruction(MagnificationEnum magnificationEnum, int scanPassWidthInNanoMeters)
  {
    Assert.expect(magnificationEnum != null);
    Assert.expect(scanPassWidthInNanoMeters > 0);
  }

  /**
  * This method locates the system fiducial.
  * @param searchRegionForSystemFiducial is the search region in which the system fiducial is located
  * @return the location of the system fiducial in each projection
  * @author Horst Mueller
  */
  public SystemFiducialData locateSystemFiducial(Rectangle searchRegionForSystemFiducial)
  {
  }

  /**
  * This method performs the magnification calibration. For each camera a search region is supplied.
  * The measured size of the calibration feature should be within the range
  * expectedLengthInPixels +/- lengthToleranceInPixels , expectedWidthInPixels  +/- widthToleranceInPixels.
  * @param magnification is the target magnification
  * @param searchRegions is a List Rectangles which describe the search regions in which the calibration 
  *        feature is located
  * @param expectedWidthInPixels is the expected height of the calibration feature
  * @param expectedLengthInPixels is the expected length of the calibration feature
  * @param widthToleranceInPixels is the maximum deviation from the expected length which is accepted
  * @param lengthToleranceInPixels is the maximum deviation from the expected length which is accepted
  * @author Horst Mueller
  */
  public void magnificationCalibration(MagnificationEnum magnification, 
                                         List searchRegions, 
                                         int expectedWidthInPixels, 
                                         int expectedLengthInPixels,
                                         int widthToleranceInPixels,
                                         int lengthToleranceInPixels)
  {
    Assert.expect(magnificationEnum != null);
    Assert.expect(searchRegions != null);
    Assert.expect(expectedWidthInPixels > 0);
    Assert.expect(expectedLengthInPixels > 0);
    Assert.expect(widthToleranceInPixels > 0);
    Assert.expect(lengthToleranceInPixels > 0);
  }

  /**
  * This method performs the x-ray camera X position calibration. For each camera an expected position is 
  * supplied. The actual position of each camera should be within the range 
  * expectedPosition +/- xToleranceInNanoMeters
  * @param expectedXRayCameraPositionData is a List of XRayCameraPositionData that contain the expected position
  * @param xToleranceInNanoMeters is the maximum deviation from the expected location which is accepted
  * @author Horst Mueller
  */
  public void xRayCameraXPositionCalibration(List expectedXRayCameraPositionData, int xToleranceInNanoMeters)
  {
    Assert.expect(xToleranceInNanoMeters > 0);
    Assert.expect(expectedXRayCameraPositionData != null);
  }

  /**
  * This method performs the x-ray camera Y position calibration. For each camera an expected position is 
  * supplied. The actual position of each camera should be within the range 
  * expectedPosition +/- yToleranceInNanoMeters
  * @param expectedXRayCameraPositionData is a List of XRayCameraPositionData that contain the expected position
  * @param yToleranceInNanoMeters is the maximum deviation from the expected location which is accepted
  * @author Horst Mueller
  */
  public void xRayCameraYPositionCalibration(List expectedXRayCameraPositionData, int yToleranceInNanoMeters)
  {
    Assert.expect(xToleranceInNanoMeters > 0);
    Assert.expect(expectedXRayCameraPositionData != null);
  }

  /** 
  * This method performs the x-ray camera trigger calibration. 
  * For each camera a pixel offset in the y direction is determined. 
  * @return a List of Integers that represent offsets in the y direction
  * @author Horst Mueller
  */
  public List cameraTriggerCalibration() 
  {
  }

  /**
  * This method frees the memory that contains the image from all the projections from the panels 0,0
  * coordinate to the panels x coordinate passed in.
  * @param panelXInNanoMeters is the x location on the panel up to which projection data can be deleted
  * @author Horst Mueller
  */
  public void freeMemory(int panelsXInNanoMeters) 
  {
  }

  /**
  * This method confirms that all projections for a given scan pass are received by the 
  * embedded image reconstruction engine. This method returns true if all the projections
  * have been received from all cameras. The calling software needs to get this information so it can determine
  * if the processors (and cameras) are ready for another scan of the panel.
  * @param scanPassNumber is the scan pass up to which the projection data should be received from all cameras
  * @return true if all the projections are received
  * @author Horst Mueller
  */
  public bool areAllProjectionsReceived(int scanPassNumber) 
  {
  }

  /**
  *
  * @return the number of bytes that are available
  * @author Horst Mueller
  */
  public int getNumBytesFreeForProjectionImages() 
  {
  }

  /**
  * This method is used during an inspection of a panel. A list of images is requested 
  * for a given reconstruction region. The caller can get the images from the ImageObservable.
  * @param reconstructionRegion contains detailed information on the region on the panel
  *        for which an image or multiple images is created at appropriate slice heights
  * @author Horst Mueller
  */
  public void createImages(ReconstructionRegion reconstructionRegion) 
  {
    Assert.expect(reconstructionRegion != null);
  }

  /**
  * This method creates images and returns the radial offsets that were used to reconstruct the images at the
  * appropriate slice heights. It is used for diagnostic purposes. The caller can get the images from the 
  * ImageObservable.
  * @param reconstructionRegion contains detailed information on the region on the panel for which
  *        an image or multiple images is created at appropriate slice heights
  * @return a List of RadialOffsets that was used by the shift and add algorithm to create 
  *         the images at appropriate slice heights
  * @author Horst Mueller
  */
  public void createImages(ReconstructionRegion reconstructionRegion) 
  {
    Assert.expect(reconstructionRegion != null);
  }

  /** 
  * This method creates images based on the supplied list of radial offsets. It is used for diagnostic purposes.
  * The caller can get the images from the ImageObservable.
  * @param reconstructionRegion contains detailed information on the region on the panel for which
  *                             an image or multiple images is created at appropriate slice heights
  * @param radialOffsets is a List of RadialOffsets to be used by the shift and add algorithm to create the images
  * @author Horst Mueller
  */
  public void createImages(ReconstructionRegion reconstructionRegion, List radialOffsets) 
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(radialOffsets != null);
  }

  /** 
  * This method creates an image is created for a given distance from the reference z-plane.  
  * The caller can get the images from the ImageObservable.
  * @param reconstructionRegion contains detailed information on the region on the panel for which an image is 
  * created at the requested slice height.
  * @param zHeightFromReferencePlaneInNanoMeters is the distance from the reference plane at which the image is constructed
  * @author Horst Mueller
  */
  public void createImage(ReconstructionRegion reconstructionRegion, int zHeightFromReferencePlaneInNanoMeters) 
  {
    Assert.expect(reconstructionRegion != null);
  }

  /**
  * This method updates alignment information that was obtained by a different image reconstruction engine.
  * @param alignmentResult contains the expected and measured coordinates of an alignment region
  * @author Horst Mueller
  */
  public void updateAlignmentResult(AlignmentResult alignmentResult) 
  {
    Assert.expect(alignmentResult != null);
  }

  /**
  * This method returns the projection data for a given scan pass and camera. It is for diagnostic purposes only .
  * @param scanPassNumber the number of the scan pass
  * @param cameraId is the id of the camera from which the data is returned
  * @return ProjectionRegionData contains the projection data captured by a single camera
  * @author Horst Mueller
  */
  public ProjectionRegionData getProjectionImages(int scanPassNumber, int cameraId) 
  {
    Assert.expect(scanPassNumber >= 0);
    Assert.expect(cameraId >= 0);
  }

  /**
  * This method allows the embedded image reconstruction software to be filled with data from the PC. 
  * instead of from the x-ray cameras. This is useful for testing that the image reconstruction hardware is 
  * working properly.
  * @param projectionRegionData is a List of ProjectionRegionData which normally is captured by the camera array
  * @author Horst Mueller
  */
  public void setRawImageData(List projectionRegionData) 
  {
    Assert.expect(projectionRegionData != null);
  }
}
