/**
 * @author Bill Darbie
 */
class TestExecution
{
  private TestExecutionEngine _testExecutionEngine;

  /**
   * @author Bill Darbie
   */
  void executeTest(Project project)
  {
    // Take the list of InspectionRegions and
    // create the List of ReconstructionRegions (unless the generator
    // already has them created) to pass down
    // to the hardware layers ImageReconstructionRegion

    // Have an Observer get the ReconstructedImages and
    // organize them into ComponentJointImages (using IDs), then call
    // the image analysis subsystem with those.
    //
    // The ImageAcquisitionEngine will take the ReconstructionRegions
    // and break them down into smaller ReconstructionRegions based
    // on hardware requirements.  This class should take that updated
    // list and make sure it is saved as part of the panel program
    // so the next test execution will not have to recalculate them.
  }
}
