/**
 * This class will generate a test for a given Panel Program. 
 *
 * @author Bill Darbie
 */
class InspectionRegionGenerator
{
  /**
   * Generate the test for the panel passed in.  The test is returned as a List of InspectionRegions
   * that can be passed into the ImageReconstructionEngine class.
   * 
   * For each InspectionRegion this class will create a prioritized or weighted list of focus
   * regions and an initial focus hint that can be used by the auto focus algorithm as a starting point.  
   * It will also choose a list of alignment regions across the board.
   *
   *
   * @author Bill Darbie
   */
  List generate(Project project)
  {

  }
}
