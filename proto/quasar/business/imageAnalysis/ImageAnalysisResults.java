import java.util.*;

/**
 * This class contains the results
 * @author Bill Darbie
 */
class ImageAnalysisResults
{
  private List _badJoints;
  private Map _jointToMeasurementsMap;
}