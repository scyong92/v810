import java.util.*;

/**
 * This is the main interface into the image analysis subsytem.
 * It expects a List of 1 or more images that contain joints that need to have
 * the same algorithm inspect them.  The joints will be geometrically aligned
 * typically.
 *
 * @author Bill Darbie
 */
class ImageAnalysis
{
  /**
   * Pass in a List of ComponentImageGroups.  A single ComponentImageGroup contains
   * images for all the joints for a single component that are the same joint type.
   * If more than one component has the same joint type and they are geometrically
   * lined up, then more than one ComponentImageGroup can be sent in at a time.
   *
   * @author Bill Darbie
   */
  ImageAnalysisResults analyzeImages(List componentImageGroups);
}
