import java.util.*;

/**
 * This class contains a List of Images that contain all the joints for a single
 * Component that are the same joint type.  If a single Component has more than
 * one joint type, then there will be more than one ComponentImageGroup for that
 * Component.
 *
 * @author Bill Darbie
 */
class ComponentImageGroup
{
  private Component _component;
  private List _images;
}
