package proto.quasar.hardware;

import com.agilent.mtd.util.*;

/**
 * @author Bill Darbie
 */
public class PanelDataMutable implements PanelData
{
  private String _name;
  private int _widthInNanometers;
  private int _lengthInNanometers;
  private String _serialNumber;


  PanelDataMutable()
  {
    _name = new String("");
    _lengthInNanometers = 0;
    _widthInNanometers = 0;
    _serialNumber = new String("");
  }

  void setName(String name)
  {
    _name = name;
  }

  void setLengthInNanometers(int lengthInNanometers)
  {
    _lengthInNanometers = lengthInNanometers;
  }

  void setWidthInNanometers(int widthInNanometers)
  {
    _widthInNanometers = widthInNanometers;
  }

  void setSerialNumber(String serialNumber)
  {
    _serialNumber = serialNumber;
  }

  public String getName()
  {
    return _name;
  }

  public int getLengthInNanometers()
  {
    return _lengthInNanometers;
  }

  public int getWidthInNanometers()
  {
    return _widthInNanometers;
  }

  public String getSerialNumber()
  {
    return _serialNumber;
  }

}
