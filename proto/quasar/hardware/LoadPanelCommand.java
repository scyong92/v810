//package com.agilent.xRayTest.hardware;
package proto.quasar.hardware;

import com.agilent.xRayTest.hardware.*;

/**
* This class contains information required to load a panel into the machine.
*
* @author Bill Darbie
*/
public class LoadPanelCommand extends Command
{
  private static PanelLoader _panelLoader;
  private int _widthOfPanelInNanoMeters;

  /**
   * @author Bill Darbie
   */
  static
  {
//    AGT5dx agt5dx = AGT5dx.getInstance();
//    _panelLoader = agt5dx.getPanelLoader();
  }

  /**
  * Construct a load panel command that will load a panel with the specified width.
  *
  * @param widthOfPanelInMeters the width of the panel to be loaded in meters.
  * @author Bill Darbie
  */
  LoadPanelCommand(int widthOfPanelInNanoMeters)
  {
    _widthOfPanelInNanoMeters = widthOfPanelInNanoMeters;
  }

  public double getWidthOfPanelInNanoMeters()
  {
    return _widthOfPanelInNanoMeters;
  }

  /**
  * Execute this command.
  *
  * @author Bill Darbie
  */
  public void execute() throws HardwareException
  {
//    _panelLoader.loadPanel(_widthOfPanelInNanoMeters);
  }
}
