package proto.quasar.hardware;

import java.io.*;

import com.agilent.mtd.util.*;

/**
 * @author Bill Darbie
 */
public class PointToPointMotionProfileEnum extends Enum implements Serializable
{
  private static int _index = 0;

  public static final PointToPointMotionProfileEnum  PROFILE1 = new PointToPointMotionProfileEnum(++_index);
  public static final PointToPointMotionProfileEnum  PROFILE2 = new PointToPointMotionProfileEnum(++_index);
  public static final PointToPointMotionProfileEnum  PROFILE3 = new PointToPointMotionProfileEnum(++_index);
  public static final PointToPointMotionProfileEnum  PROFILE4 = new PointToPointMotionProfileEnum(++_index);
  public static final PointToPointMotionProfileEnum  PROFILE5 = new PointToPointMotionProfileEnum(++_index);
  public static final PointToPointMotionProfileEnum  PROFILE6 = new PointToPointMotionProfileEnum(++_index);
  public static final PointToPointMotionProfileEnum  PROFILE7 = new PointToPointMotionProfileEnum(++_index);
  public static final PointToPointMotionProfileEnum  PROFILE8 = new PointToPointMotionProfileEnum(++_index);
  public static final PointToPointMotionProfileEnum  PROFILE9 = new PointToPointMotionProfileEnum(++_index);
  public static final PointToPointMotionProfileEnum  PROFILE10 = new PointToPointMotionProfileEnum(++_index);


  /**
   * @author Bill Darbie
   */
  private PointToPointMotionProfileEnum(int id)
  {
    super(id);
  }
}
