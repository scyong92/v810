//package com.agilent.xRayTest.hardware;
package proto.quasar.hardware;

import java.util.*;

import com.agilent.xRayTest.hardware.*;
import com.agilent.util.*;

/**
 * This class contains all the data needed to describe a region on the panel that needs
 * to be examined.  This data is used at the business layer.  The ReconstructionRegion class
 * will be filled in with a hardware only (no CAD) data version of this data.
 *
 * @author Bill Darbie
 */
// wpd - this class is a business layer class
class InspectionRegion
{
  private static int _staticId = -1;

  // this id is sent down to the cameras, which pass it along to the ImageReconstructionEngines,
  // they use it to figure out which reconstructed image maps to which InspectionRegion reference
  private int _id;

  // the joints that will be in this Reconstruction Region - this could be a subset
  // of all the joints for the component listed above
  // the Joints here should all have the same type of Joint (JointTypeEnum - probably needs to
  // be added to JointSettings)
  private List _joints;

  // wpd - should this be here at all or all in ReconstructionRegion?
  //       at the very least ReconstructionRegion will make this area larger based on
  //       hardware constraints
  // information used to define the region
  // specifies if the image should be constructed from the top or bottom of the panel
  private SideEnum _sideEnum;
  // specifies the bottom left corner of the area to be constructed
  // relative to the panels bottom left corner
  // the hardware layer is responsible for converting
  private int _xInNanoMeters;
  // specifies the bottom left corner of the area to be constructed
  // relative to the panels bottom left corner
  private int _yInNanoMeters;
  // specifies the width of the area to be contructed
  // relative to the panels bottom left corner
  private int _widthInNanoMeters;
  //specifies the length of the area to be constructed
  // relative to the panels bottom left corner
  private int _lengthInNanoMeters;
  private int _degreesRotation; // hopefully we will not need this
  // x,y offset of the top left corner of the image relative to the center of the component
  // relative to the panels bottom left corner
  private int _imageXInNanoMeters;
  private int _imageYInNanoMeters;

  private List _heightFromSharpestFocusInNanoMeters;
  private List _focusRegionForEachHeight;  // need this for pcap and maybe others

  // PE:  Because the InspectionRegions can be broken down into
  // multiple ReconstructionRegions by the ImageAcquisitionEngine,
  // we cannot predetermine FocusRegions.  Instead, we need
  // to create a list of potential FocusRegions, and it's likely
  // that we'll need to specify some priority order based on
  // how good each region would be for alignment.  Then the 
  // ImageAcquisitionEngine can use this information to select
  // FocusRegions for itself.
  private List _potentialFocusRegions;

  // PE:  We'll need to select these alignment regions based
  // on CAD.  Hopefully we won't need to know scan path, but if
  // we do, we'll probably need switch to a "potentialAlignmentRegion"
  // strategy 
  private List _alignmentRegions;

  /**
   * @author Bill Darbie
   */
  private InspectionRegion()
  {
    ++_staticId;
    _id = _staticId;
  }

  /**
   * Set the List of joints that this inspection region should have inspected.  Note that the region could
   * physically contain more joints, this should just be the joints that need to be inspected.
   * @param joints
   */
  void setJoints(List joints)
  {
    Assert.expect(joints != null);

    /** @todo wpd - add an assert here that all the joints have the same algorithm assigned to them */
    _joints = joints;
  }

  /**
   *
   * @return the List of Joints that this inspection region will have inspected.  Note that the region could
   * physically contain more joints, this should just be the joints that need to be inspected.
   * @author Bill Darbie
   */
  List getJoints()
  {
    Assert.expect(_joints != null);

    return _joints;
  }

  /*
   * @author Bill Darbie
   */
  void setSideEnum(SideEnum sideEnum)
  {
    Assert.expect(sideEnum != null);
    _sideEnum = sideEnum;
  }
}
