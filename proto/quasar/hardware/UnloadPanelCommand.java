//package com.agilent.xRayTest.hardware;
package proto.quasar.hardware;

import com.agilent.xRayTest.hardware.*;

/**
* This class contains all the information required to unload a panel from the machine.
*
* @author Bill Darbie
*/
public class UnloadPanelCommand extends Command
{
  private static PanelLoader _panelLoader;

  /**
   * @author Bill Darbie
   */
  static
  {
    AGT5dx agt5dx = AGT5dx.getInstance();
    _panelLoader = agt5dx.getPanelLoader();
  }

  /**
  * Construct an object that can unload the current panel in the machine.
  *
  * @author Bill Darbie
  */
  UnloadPanelCommand()
  {
    System.out.println("UnloadPanelCommand() stub");
  }

  /**
  * Unload the panel.
  *
  * @author Bill Darbie
  */
  public void execute() throws HardwareException
  {
    _panelLoader.unloadPanel();
  }
}
