package proto.quasar.hardware;

import com.agilent.mtd.util.*;

/**
 * @author Bill Darbie
 */
public class PanelHandlerStateMutable implements PanelHandlerState
{
  private PanelDataMutable _panelHandlerPanel;
  private PanelHandlerWorkingStateEnum _workingState;
  private int _stageRailWidthInNanometers;
  private boolean _beltDirectionIsLeftToRight;
  private boolean _beltOn;
  private boolean _leftPanelInPlaceSensorIsExtended;
  private boolean _rightPanelInPlaceSensorIsExtended;
  private boolean _leftPanelInPlaceSensorIsEngaged;
  private boolean _rightPanelInPlaceSensorIsEngaged;
  private boolean _clampsClosed;
  private boolean _panelLoaded;
  private PanelFlowEnum _panelFlow;
  private boolean _inlineCommunicationEnabled;


  /**
   * @author Greg Esparza
   */
  PanelHandlerStateMutable()
  {
    _panelHandlerPanel = null;
    _workingState = PanelHandlerWorkingStateEnum.IDLE;
    _stageRailWidthInNanometers = 0;
    _beltDirectionIsLeftToRight = true;
    _beltOn = false;
    _leftPanelInPlaceSensorIsExtended = false;
    _rightPanelInPlaceSensorIsExtended = false;
    _leftPanelInPlaceSensorIsEngaged = false;
    _rightPanelInPlaceSensorIsEngaged = false;
    _clampsClosed = false;
    _panelLoaded = false;
    _panelFlow = PanelFlowEnum.LEFT_TO_RIGHT_FLOW_THROUGH;
    _inlineCommunicationEnabled = false;
  }

  /**
   * Set the current rail width value
   *
   * @param stageRailWidthInNanometers is the current rail width in nanometers
   * @author Greg Esparza
   */
  void setStageRailWidthInNanometers(int stageRailWidthInNanometers)
  {
    _stageRailWidthInNanometers = stageRailWidthInNanometers;
  }

  /**
   * Set the current panel information
   *
   * If a panel is in the machine, set the panel information.
   * Otherwise, set the panel object to null.
   * 
   * @param panelHandlerPanel describes the current panel in the machine.  
   * @author Greg Esparza
   */
  void setPanelInformation(PanelDataMutable panel)
  {
    //todo - make copy
    //_panel = panel;
  }

  /**
   * Set the current working state
   *
   * @param panelHandlerWorkingState is an enumeration describing the current, working state.
   * @author Greg Esparza
   */
  void setWorkingState(PanelHandlerWorkingStateEnum panelHandlerWorkingState)
  {
    _workingState = panelHandlerWorkingState;
  }

  /**
   * Set the current belt direction
   *
   * @param beltDirectionIsLeftToRight is true if the current state is left-to-right or 
   *                                   false if the current state is right-to-left.
   * @author Greg Esparza
   */
  void setBeltDirectionState(boolean beltDirectionIsLeftToRight)
  {
    _beltDirectionIsLeftToRight = beltDirectionIsLeftToRight;
  }

  /**
   * Set the current belt direction
   *
   * @param beltOn is true if the current state of the belt is on or false if it is off.
   * @author Greg Esparza
   */
  void setBeltState(boolean beltOn)
  {
    _beltOn = beltOn;
  }

  /**
   * Set whether the left Panel-In-Place sensor is currently extended
   *
   * @param leftPanelInPlaceSensorIsExtended is true if the sensor is extended and false if it is not.
   * @author Greg Esparza
   */
  void setLeftPanelInPlaceSensorExtendedState(boolean leftPanelInPlaceSensorIsExtended)
  {
    _leftPanelInPlaceSensorIsExtended = leftPanelInPlaceSensorIsExtended;
  }

  /**
   * Set whether the right Panel-In-Place sensor is currently extended
   *
   * @param rightPanelInPlaceSensorIsExtended is true if the sensor is extended and false if it is not.
   * @author Greg Esparza
   */
  void setRightPanelInPlaceSensorExtendedState(boolean rightPanelInPlaceSensorIsExtended)
  {
    _rightPanelInPlaceSensorIsExtended = rightPanelInPlaceSensorIsExtended;
  }

  /**
   * Set whether the left Panel-In-Place sensor is currently engaged
   *
   * @param leftPanelInPlaceSensorIsEngaged is true if the sensor is engaged and false if it is not.
   * @author Greg Esparza
   */
  void setLeftPanelInPlaceSensorEngagedState(boolean leftPanelInPlaceSensorIsEngaged)
  {
    _leftPanelInPlaceSensorIsEngaged = leftPanelInPlaceSensorIsEngaged;
  }

  /**
   * Set whether the right Panel-In-Place sensor is currently engaged
   *
   * @param rightPanelInPlaceSensorIsEngaged is true if the sensor is engaged and false if it is not.
   * @author Greg Esparza
   */
  void setRightPanelInPlaceSensorEngagedState(boolean rightPanelInPlaceSensorIsEngaged)
  {
    _rightPanelInPlaceSensorIsEngaged = rightPanelInPlaceSensorIsEngaged;
  } 

  /**
   * Set the current state of the clamps
   *
   * @param clampsClosed is true if the clamps are closed and false if they are not.
   * @author Greg Esparza
   */
  void setClampsState(boolean clampsClosed)
  {
    _clampsClosed = clampsClosed;
  } 

  /**
   * Set the current panel flow state
   *
   * @param panelFlow is an enumeration describing the current panel flow such as left-to-right flow through, etc.
   * @author Greg Esparza
   */
  void setPanelFlowState(PanelFlowEnum panelFlow)
  {
    _panelFlow = panelFlow;
  }

  /**
   * Set the current inline communication state
   * 
   * @param inlineCommunicationEnabled is true if machine is enabled for inline communication and false if it is not.
   * @author Greg Esparza
   */
  void setInlineCommunicationState(boolean inlineCommunicationEnabled)
  {
    _inlineCommunicationEnabled = inlineCommunicationEnabled;
  }

  /**
   * Set whether or not a panel is currently loaded in the machine
   * 
   * @param panelLoaded is true if a panel is currently loaded in the machine and false if it is not.
   * @author Greg Esparza
   */
  void setPanelLoadedState(boolean panelLoaded)
  {
    _panelLoaded = panelLoaded;
  }  

  /**
   * Get the current stage rail width
   *
   * @return The current rail width in nanometers
   * @author Greg Esparza
   */
  public int getStageRailWidthInNanometers()
  {
    return _stageRailWidthInNanometers;
  }

  /**
   * Check if a panel is loaded in the machine
   *
   * @return true if a panel is loaded nand false if it is not.
   * @author Greg Esparza
   */
  public boolean isPanelLoaded()
  {
    return _panelLoaded;
  }

  /**
   * Get the current panel information
   *
   * The client should first call "isPanelLoaded()" before calling this method. 
   *
   * @return a PanelHandlerPanelInt if a panel is currently loaded in the machine.  Otherwise, the method will return null.
   * @author Greg Esparza
   */
  public PanelData getPanelInformation()
  {
    return _panelHandlerPanel;
  }

  /**
   * Get the current working state of the Panel Handler
   *
   * @return an enumeration describing the current working state such as idle, loading, etc.
   * @author Greg Esparza
   */
  public PanelHandlerWorkingStateEnum getWorkingState()
  {
    return _workingState;
  }

  /**
   * Get the current panel flow configuration
   *
   * @return an enumeration describing the current panel flow such as left-to-right flow through, etc.
   * @author Greg Esparza
   */
  public PanelFlowEnum getPanelFlow()
  {
    return _panelFlow;
  }

  /**
   * Check if inline communication is enabled
   *
   * @return true if inline communication is enabled and false if it is not.
   * @author Greg Esparza
   */
  public boolean isInlineCommunicationEnabled()
  {
    return _inlineCommunicationEnabled;
  }

  /**
   * Check the current belt direction
   *
   * @return true if the direction is left-to-right and false if it is not.
   * @author Greg Esparza
   */
  public boolean isBeltDirectionFromLeftToRight()
  {
    return _beltDirectionIsLeftToRight;
  }

  /**
   * Check if the stage rail conveyor belt is on
   *
   * @return true if the belt is on and false if it is not.
   * @author Greg Esparza
   */
  public boolean isBeltOn()
  {
    return _beltOn;
  }

  /**
   * Check if the left Panel-In-Place sensor is extended
   *
   * @return true if the sensor is extended and false if it is not.
   * @author Greg Esparza
   */
  public boolean isLeftPanelInPlaceSensorExtended()
  {
    return _leftPanelInPlaceSensorIsExtended;
  }

  /**
   * Check if the right Panel-In-Place sensor is extended
   *
   * @return true if the sensor is extended and false if it is not.
   * @author Greg Esparza
   */
  public boolean isRightPanelInPlaceSensorExtended()
  {
    return _rightPanelInPlaceSensorIsExtended;
  }

  /**
   * Check if the left Panel-In-Place sensor is engaged
   *
   * @return true if the sensor is engaged and false if it is not.
   * @author Greg Esparza
   */
  public boolean isLeftPanelInPlaceSensorEngaged()
  {
    return _leftPanelInPlaceSensorIsEngaged;
  }

  /**
   * Check if the right Panel-In-Place sensor is engaged
   *
   * @return true if the sensor is engaged and false if it is not.
   * @author Greg Esparza
   */
  public boolean isRightPanelInPlaceSensorEngaged()
  {
    return _rightPanelInPlaceSensorIsEngaged;
  }

  /**
   * Check if the clamps are closed
   *
   * @return true if the clamps are closed and false if they are not.
   * @author Greg Esparza
   */
  public boolean areClampsClosed()
  {
    return _clampsClosed;
  }
}
