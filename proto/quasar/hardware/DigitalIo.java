package proto.quasar.hardware;

import com.agilent.mtd.agt5dx.hardware.*;
import com.agilent.mtd.util.*;
import com.agilent.mtd.agt5dx.datastore.*;
import com.agilent.mtd.agt5dx.hardware.nativeInt.*;



public class DigitalIo extends HardwareObject
{
  private static DigitalIo _instance = null;
  private DigitalIoBinaryValueEnum _inputBitValue;
 

  private DigitalIo()
  {
    _inputBitValue = DigitalIoBinaryValueEnum.ZERO;
  }

  static synchronized DigitalIo getInstance()
  {
    if (_instance == null)
      _instance = new DigitalIo();

    return _instance;
  }

  /**
   * Reset all output settings to an OFF state
   *
   * @author Greg Esparza
   */
  void reset() throws HardwareException
  {
  }
  
  /**
   * Set an output bit to either a 1 or 0
   *
   * @param bitNumber will correspond to the 
   *
   */
  void setOutputBit(DigitalIoOutputBitEnum bitNumber, DigitalIoBinaryValueEnum bitValue) throws HardwareException
  {
    //make the nativeInt call into the thirdParty API here
  }

  DigitalIoBinaryValueEnum getInputBitState(DigitalIoInputBitEnum bitNumber) throws HardwareException
  {
    //make the nativeInt call into the thirdParty API here
    return _inputBitValue;
  }

  /////////////////////////////////////////////////////////////////////////////
  // Run all diagnostics
  /////////////////////////////////////////////////////////////////////////////

  void runAllIoDiagnostics() throws HardwareException
  {
    runAllIoCommunicationModuleDiagnostics();
    runAllIoRelayDiagnostics();
  }

  /////////////////////////////////////////////////////////////////////////////
  // Run all diagnostics
  /////////////////////////////////////////////////////////////////////////////

  void runAllIoCommunicationModuleDiagnostics() throws HardwareException
  {
  }

  private void checkIoCommunicationModule() throws HardwareException
  {
  }

  /////////////////////////////////////////////////////////////////////////////
  // Run all diagnostics
  /////////////////////////////////////////////////////////////////////////////

  void runAllIoRelayDiagnostics() throws HardwareException
  {
  }

  private void checkIoRelays() throws HardwareException
  {
  }
}


