package proto.quasar.hardware;

/**
 * This class will provide all xray source functionality including
 * things like turning x-rays on and off.  The new tube has
 * the power supply built in so there will no longer be a separate
 * XrayTube and XrayPowerSupply class.
 *
 * @author Bill Darbie
 */
class XraySource
{
  /**
  * @author Matt Wharton
  */
  void turnOn() throws HardwareException
  {
  }

  /**
  * @author Matt Wharton
  */
  void turnOff() throws HardwareException
  {
  }

}
