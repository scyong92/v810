//package com.agilent.xRayTest.hardware;
package proto.quasar.hardware;

/**
 * This class contains a single image projection.  It is used for diagnostics.
 * @author Bill Darbie
 */
public class ProjectionImage
{
  private int _scanPass;
  private int _cameraId;
  private XrayImage _xRayImage;
}