//package com.agilent.xRayTest.hardware;
package proto.quasar.hardware;

import com.agilent.util.*;

/**
 * This class is used by the XrayCamera.setScanPath() method
 * @author Bill Darbie
 */
class ProjectionRegion
{
  private int _xInPixels;
  private int _yInPixels;
  private int _widthInPixels;
  private int _lengthInPixels;
  private int _projectionRegionId;
  private InetAddress _projectionDataDestination;
  private int _cameraId;
  private int _scanPassNumber;

/** @todo wpd - change this to sets and gets */
  /**
   * Hold data required to tell the camera how to break up a scan into separate images
   * Each image will be sent to a specific processing node.
   * @param xInPixels is x coordinate of the top left origin of the projection region to be sent
   *        to a particular imageReconstructionEngine
   * @param yInPixels is y coordinate of the top left origin of the projection region to be sent
   *        to a particular imageReconstructionEngine
   * @param widthInPixels is number of pixels in the x coordinate space to use to for the
   *        projection region to be sent to a particular imageReconstructionEngine
   * @param lengthInPixels is number of pixels in the y coordinate space to use to for the
   *        projection region to be sent to a particular imageReconstructionEngine
   * @param projectionnRegionId is a unique ID for this projectionRegion
   * @param imageReconstructionEngineId is the ID of the image reconstuction processor
   *        that this ProjectionRegion should be sent to
   * @author Bill Darbie
   */
  ProjectionRegion(int xInPixels,
                   int yInPixels,
                   int widthInPixels,
                   int lengthInPixels,
                   int projectionRegionId,
                   int imageReconstructionEngineId)
  {
    Assert.expect(xInPixels >= 0);
    Assert.expect(yInPixels >= 0);
    Assert.expect(widthInPixels > 0);
    Assert.expect(lengthInPixels > 0);
    Assert.expect(projectionRegionId >= 0);
    Assert.expect(imageReconstructionEngineId >= 0);

    System.out.println("ProjectionRegion() stub");
  }

  /**
   * @return the x coordinate of the projection region
   * @author Bill Darbie
   */
  int getXinPixels()
  {
    return _xInPixels;
  }

  /**
   * @return the y coordinate of the projection region
   * @author Bill Darbie
   */
  int getYinPixels()
  {
    return _yInPixels;
  }

  /**
   * @return the width of the projection region
   * @author Bill Darbie
   */
  int getWidthInPixels()
  {
    return _widthInPixels;
  }

  /**
   * @return the length of the projection region
   * @author Bill Darbie
   */
  int getLengthInPixels()
  {
    return _lengthInPixels;
  }

  /**
   * @return this regions unique ID
   * @author Bill Darbie
   */
  int getProjectionRegionId()
  {
    return _projectionRegionId;
  }

  /**
   * @return the ImageReconstructionEngineId that this region will be sent to
   * @author Bill Darbie
   */
  int getImageReconstructionEngineId()
  {
    return _imageReconstructionEngineId;
  }

  /**
  * @author Matt Wharton
  */
  InetAddress getProjectionDataDestination()
  {
    return _projectionDataDestination;
  }

  /**
  * @author Matt Wharton
  */
  void setProjectionDataDestination( InetAddress projectionDataDestination )
  {
    _projectionDataDestination = projectionDataDestination;
  }
}

