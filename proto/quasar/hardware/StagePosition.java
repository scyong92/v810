package proto.quasar.hardware;

import com.agilent.mtd.util.*;

/**
 * @author Bill Darbie
 */
public interface StagePosition
{
  public int getXInNanometers();
 
  public int getYInNanometers();
 
  public StageZPositionEnum getZ();
}
