package proto.quasar.hardware;

import java.io.*;

import com.agilent.mtd.util.*;

/**
 * @author Bill Darbie
 */
public class MotionTypeEnum extends Enum implements Serializable
{
  private static int _index = 0;

  public static final MotionTypeEnum POINT_TO_POINT = new MotionTypeEnum(++_index);
  public static final MotionTypeEnum LINEAR         = new MotionTypeEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private MotionTypeEnum(int id)
  {
    super(id);
  }
}
