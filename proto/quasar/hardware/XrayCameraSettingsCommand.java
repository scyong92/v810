//package com.agilent.xRayTest.hardware;
package proto.quasar.hardware;

import com.agilent.xRayTest.hardware.*;

/**
 *
 * @author Bill Darbie
 */
class XrayCameraSettingsCommand extends Command
{
  /**
   * Execute the command.
   * @author Bill Darbie
   */
  public void execute() throws HardwareException
  {
    System.out.println("XrayCameraSettingsCommand.execute() stub");
  }
}
