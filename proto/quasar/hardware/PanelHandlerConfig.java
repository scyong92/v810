package proto.quasar.hardware;

import com.agilent.mtd.util.*;

/**
 * @author Bill Darbie
 */
public interface PanelHandlerConfig
{
  public int getMaxStageRailWidthInNanometers();

  public int getMinStageRailWidthInNanometers();

  public int getStageRailHomeWidth();

  public int getLeftXstageLoadPositionInNanometers();

  public int getLeftYstageLoadPositionInNanometers();

  public StageZPositionEnum getLeftZstageLoadPosition();

  public int getRighttXstageLoadPositionInNanometers();

  public int getRightYstageLoadPositionInNanometers();

  public StageZPositionEnum getRightZstageLoadPosition();
  
  public String getHardwareVersion(); 

  public String getSerialNumber(); 

  public PanelFlowEnum getPanelFlowConstruction();

  public PanelFlowEnum getPanelFlowOperation();

}
