package proto.quasar.hardware;

import com.agilent.mtd.util.*;

/**
 * @author Bill Darbie
 */
/** @todo wpd - implement Observer to see changes in these config settings */
class PanelPositionerConfig
{
  private int _xEncoderCountsPerNanometer;
  private int _yEncoderCountsPerNanometer;
  private int _xScanEncoderCountsPerNanometer;
  private int _yScanEncoderCountsPerNanometer;
  private int _xMinPositionLimitInNanometers;
  private int _xMaxPositionLimitInNanometers;
  private int _yMinPositionLimitInNanometers;
  private int _yMaxPositionLimitInNanometers;
  private String _hardwareVersion;
  private String _serialNumber;
  private int _nanometersPerEncoderCount;

  PanelPositionerConfig()
  {
    //Need to get this from HardwareConfigEnum
    _xEncoderCountsPerNanometer = 0;
    _yEncoderCountsPerNanometer = 0;
    _xScanEncoderCountsPerNanometer = 0;
    _yScanEncoderCountsPerNanometer = 0;
    _xMinPositionLimitInNanometers = 0;
    _xMaxPositionLimitInNanometers = 0;
    _yMinPositionLimitInNanometers = 0;
    _yMaxPositionLimitInNanometers = 0;
    _hardwareVersion = null;
    _serialNumber = null;
    _nanometersPerEncoderCount = 0;
  }


  /**
   * @return The defined slowest profile for a point-to-point move
   *
   * @author Bill Darbie
   */
  PointToPointMotionProfileEnum getSlowestPointToPointMotionProfile()
  {
    return PointToPointMotionProfileEnum.PROFILE1;
  }

  /**
   * @return The defined fastest profile for a point-to-point move.
   * @author Bill Darbie
   */
  PointToPointMotionProfileEnum getFastestPointToPointMotionProfile()
  {
    return PointToPointMotionProfileEnum.PROFILE10;
  }

  /**
   * @return The defined slowest profile for a linear move.
   * @author Bill Darbie
   */
  LinearMotionProfileEnum getSlowestLinearMotionProfile()
  {
    return LinearMotionProfileEnum.PROFILE1;
  }

  /**
   * @return The defined fastest profile for a linear move.
   * @author Bill Darbie
   */
  LinearMotionProfileEnum getFastestLinearMotionProfile()
  {
    return LinearMotionProfileEnum.PROFILE2;
  }

  /**
   * @return the minimum allowed x position in nanometers.
   * @author Bill Darbie
   */
  int getXminPositionLimitInNanometers()
  {
    System.out.println("PanelPositionerConfig.getXminPositionLimitInNanometers() stub");
    return 0;
  }
  
  /**
   * @return the minimum allowed x position in nanometers.
   * @author Bill Darbie
   */
  int getXmaxPositionLimitInNanometers()
  {
    System.out.println("PanelPositionerConfig.getXmaxPositionLimitInNanometers() stub");
    return 0;
  }
  
  /**
   * @return the minimum allowed y position in nanometers.
   * @author Bill Darbie
   */
  int getYminPositionLimitInNanometers()
  {
    System.out.println("PanelPositionerConfig.getYMinPositionLimitInNanometers() stub");
    return 0;
  }
 
  /**
   * @return the maximum allowed y position in nanometers.
   * @author Bill Darbie
   */
  int getYmaxPositionLimitInNanometers()
  {
    System.out.println("PanelPositionerConfig.getYmaxPositionLimitInNanometers() stub");
    return 0;
  }
  
  /**
   * @return the number of encoder counts per nanometer of stage movement in the x axis.
   * @author Bill Darbie
   */
  int getXencoderCountsPerNanometer()
  {
    System.out.println("PanelPositionerConfig.getXencoderCountsPerNanometer() stub");
    return 0;
  }

  /**
   * @return the number of encoder counts per nanometer of stage movement in the y axis.
   * @author Bill Darbie
   */
  int getYencoderCountsPerNanometer()
  {
    System.out.println("PanelPositionerConfig.getYencoderCountsPerNanometer() stub");
    return 0;
  }
  
  /**
   * @return the number of encoder counts per nanometer of stage movement during a scan in the x direction
   * @author Bill Darbie
   */
  int getXscanEncoderCountsPerNanometer()
  {
    System.out.println("PanelPositionerConfig.getXscanEncoderCountsPerNanometer() stub");
    return 0;
  }
  
  /**
   * @return the number of encoder counts per nanometer of stage movement during a scan in the y direction
   * @author Bill Darbie
   */
  int getYscanEncoderCountsPerNanometer()
  {
    System.out.println("PanelPositionerConfig.getYscanEncoderCountsPerNanometer() stub");
    return 0;
  }

  /**
   * This will return the hardware version according to the motion controller.
   * @return the version of the panel positioning hardware.
   * @author Bill Darbie
   */
  String getHardwareVersion() 
  {
    System.out.println("PanelPositionerConfig.getHardwareVersion() stub");
    return "";
  }

  /**
   * @return the serial number of this hardware from the motion controller.
   * @author Bill Darbie
   */
  String getSerialNumber() 
  {
    System.out.println("PanelPositionerConfig.getSerialNumber() stub");
    return "";
  }

  int getNanometersPerEncoderCount()
  {
    return _nanometersPerEncoderCount;
  }
}
