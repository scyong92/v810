package proto.quasar.hardware;

import java.io.*;

import com.agilent.mtd.util.*;

/**
 * @author Bill Darbie
 */
class StageZPositionEnum extends Enum implements Serializable
{
  private static int _index = 0;
  static final StageZPositionEnum  POSITION1 = new StageZPositionEnum(++_index);
  static final StageZPositionEnum  POSITION2 = new StageZPositionEnum(++_index);


  /**
   * @author Bill Darbie
   */
  private StageZPositionEnum(int id)
  {
    super(id);
  }
}
