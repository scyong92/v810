package proto.quasar.hardware;
//package com.agilent.xRayTest.hardware;

import java.io.*;
import java.util.*;

import com.agilent.util.*;


/**
 * @author Bill Darbie
 */
public class Test_ImageReconstructionEngine extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ImageReconstructionEngine());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    ImageReconstructionEngine imageEngine = ImageReconstructionEngine.getInstance();

    List inspectionRegions = new ArrayList();
    InspectionRegion inspectionRegion = new InspectionRegion();


    imageEngine.scanPanel(inspectionRegions);
  }

}
