//package com.agilent.xRayTest.hardware;
package proto.quasar.hardware;

import java.util.*;

import com.agilent.xRayTest.hardware.*;

/**
 * This class contains all the data needed to describe a region to be reconstructed.
 * InspectionRegion is the business layer class that holds similar data (but more CAD
 * oriented).  This class only understands hardware information (no CAD).
 *
 * @author Bill Darbie
 */
class ReconstructionRegion
{
  private int _inspectionRegionId;
  // the x,y, width, and length of the joint features that will be in this Reconstruction Region
  private List _features;
  private int _featureBufferSizeInNanoMeters; // how much area around the feature is required
  private FocusMethodEnum _focusMethodEnum;

  // information used to define the region
  // specifies if the image should be constructed from the top or bottom of the panel
  private SideEnum topOrBottom;
  // specifies the bottom left corner of the area to be constructed
  // relative to the panels bottom left corner
  // the hardware layer is responsible for converting
  private int _xInNanoMeters;
  // specifies the bottom left corner of the area to be constructed
  // relative to the panels bottom left corner
  private int _yInNanoMeters;
  // specifies the width of the area to be contructed
  // relative to the panels bottom left corner
  private int _widthInNanoMeters;
  //specifies the length of the area to be constructed
  // relative to the panels bottom left corner
  private int _lengthInNanoMeters;
  private int _degreesRotation; // hopefully we will not need this

  private List _heightFromSharpestFocusInNanoMeters;
  private List _focusREgionForEAchSlice; // need for pcap and possible others

  // for now assume one focus region, we could support more later
  private FocusRegion _focusRegion;

  // rectangular region where there is some solder to align on
  private List _alignmentRegions;

  // information about how to focus
  // the height to start searching for the height that provides the sharpest image.
  // Typically this will be the focus height that was found in a nearby region that
  // was already reconstructed.
  private boolean _useInitialFocusHeight;
  private int _initialFocusHeightInNanoMeters;

  // information about alignment
  private boolean _align;
  private int _initialAlignmentXoffsetInNanoMeters;
  private int _initialAlignmentYoffsetInNanoMeters;
  // if there are no alignment regions that can be used then
  // use the alignment offsets from a previous region
  private List _xyAlignmentOffsetsFromPreviousRegions;
}
