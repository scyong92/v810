package proto.quasar.hardware;

/**
 * This class handles making the PanelLoader and the PanelPositioner work together.
 * @author Bill Darbie
 */
public class PanelMotionSubsystem
{
  private PanelLoader _panelLoader;
  private PanelPositioner _panelPositioner;

  /**
   * Reset the panel motion subsystem
   * @author Bill Darbie
   */
  void reset()
  {
    System.out.println("PanelMotionSubsystem.reset() stub");
  }

  /**
   * @author Bill Darbie
   */
  PanelLoader getPanelLoader()
  {
    return _panelLoader;
  }

  /**
   * @author Bill Darbie
   */
  PanelPostioner getPanelPositioner()
  {
    return _panelPositioner;
  }

 * This class handles making the PanelHandler and the PanelPositioner work together.
  private PanelHandler _panelHandler;
  PanelHandler getPanelHandler()
    return _panelHandler;
}
