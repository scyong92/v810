package proto.quasar.hardware;

import java.io.*;

import com.agilent.mtd.util.*;

/**
 * @author Bill Darbie
 */
public class DigitalIoOutputBitEnum extends Enum implements Serializable
{
  private static int _index = 0;

  public static final DigitalIoOutputBitEnum  OUTPUT_BIT_1 = new DigitalIoOutputBitEnum(++_index);
  public static final DigitalIoOutputBitEnum  OUTPUT_BIT_2 = new DigitalIoOutputBitEnum(++_index);
  public static final DigitalIoOutputBitEnum  OUTPUT_BIT_3 = new DigitalIoOutputBitEnum(++_index);
  public static final DigitalIoOutputBitEnum  OUTPUT_BIT_4 = new DigitalIoOutputBitEnum(++_index);


  /**
   * @author Bill Darbie
   */
  private DigitalIoOutputBitEnum(int id)
  {
    super(id);
  }

  /**
   * Get the integer value of the enumeration
   *
   * @return the integer value that corresponds to the enumeration
   * @author Greg Esparza
   */
  int getValue()
  {
    return _id;
  }
}
