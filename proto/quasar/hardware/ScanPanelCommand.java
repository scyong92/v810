//package com.agilent.xRayTest.hardware;
package proto.quasar.hardware;

import com.agilent.xRayTest.hardware.*;

class ScanPanelCommand extends Command
{
  private int _xStartNanoMeters;
  private int _yStartNanoMeters;
  private int _xStopNanoMeters;
  private int _yStopNanoMeters;

  private boolean _setMoveProfile;
  private static PanelPositioner _panelPositioner;
  private int _motionProfile;

  /**
   * @author Bill Darbie
   */
  static
  {
//    AGT5dx agt5dx = AGT5dx.getInstance();
//    _panelPositioner = agt5dx.getPanelPositioner();
  }

  /**
  * @author Bill Darbie
  */
  public ScanPanelCommand(int xStartNanoMeters,
                          int yStartNanoMeters,
                          int xStopNanoMeters,
                          int yStopNanoMeters,
                          int motionProfile)
  {
    System.out.println("ScanPanelCommand() stub");
  }

  /**
  * @author Bill Darbie
  */
  public ScanPanelCommand(int xStartNanoMeters,
                          int yStartNanoMeters,
                          int xStopNanoMeters,
                          int yStopNanoMeters)
  {
    System.out.println("ScanPanelCommand() stub");
  }

//  /**
//   * @author Bill Darbie
//   */
//  public int getXNanoMeters()
//  {
//    return _xNanoMeters;
//  }
//
//  /**
//   * @author Bill Darbie
//   */
//  public int getYNanoMeters()
//  {
//    return _yNanoMeters;
//  }
//
//  /**
//   * @author Bill Darbie
//   */
//  public int getZNanoMeters()
//  {
//    return _zNanoMeters;
//  }
//
  /**
   * @author Bill Darbie
   */
  public int getMoveProfile()
  {
    System.out.println("ScanPanelCommand.getMoveProfile() stub");
    return 0;
  }

  /**
   * @author Bill Darbie
   */
  public double getDelayAfterMoveInSeconds()
  {
    System.out.println("ScanPanelCommand.getDealyAfterMoveInSeconds() stub");
    /** @todo wpd find out what a good default is for this */
    return 0.0;
  }

  /**
  * Make the machine do the move of the panel.
  *
  * @author Bill Darbie
  */
  public void execute() throws HardwareException
  {
//    if (_setMoveProfile)
//      _panelPositioner.setMoveProfile(_stageSpeed);
//    _panelPositioner.setXYZPositionInNanoMeters(_xNanoMeters, _yNanoMeters, _zNanoMeters);
//    _panelPositioner.move();
  }
}
