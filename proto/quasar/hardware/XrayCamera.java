package proto.quasar.hardware;

import java.util.*;

import com.agilent.xrayTest.util.*;

/**
 * The interface to a single X-ray line scan camera.  The Quasar hardware may have as many as 16
 * of these per system. The camera hardware actually can have several detector rows.
 * The hardware will handle taking the multiple rows and combining them into one.
 * The camera hardware also does all flat fielding correction.
 *
 * Each camera logically has two internal memory buffers.  Each buffer can hold one full scan path
 * for the largest panel allowable in the Quasar system.
 *
 * While one buffer is being filled with data  the other buffer can be sending out data.
 *
 * The camera will output a single line of pixels (1x1288 pixels) with each pixel
 * being an 8 bit value.  This is the current implementation, but it will be done so that
 * it will not be hard to have the software handle a different number of pixels and a
 * different number of bits per pixel.
 *
 * Each camera will send out the image it is told to along with the following information
 * The packet will have:
 *   - the camera ID
 *   - the destination ID (ImageReconstructionProcessor ID)
 *   - the data (see below for details of what goes here)
 *   - error check bits
 * The data field will contain:
 *   - the scan pass number
 *   - the x,y location of the top left corner of the project region relative to the upper left corner
 *     of the entire projection (not just the piece being sent to a single ImageReconstructionProcessor)
 *   - the width and length of the pixels in the projection region
 *   - a packet ID (if we do not use TCP/IP)
 *   - the total number of packets (if we do not use TCP/IP)
 *   - a region of the projection
 *
 * The camera can scan in data in either the positive or negative y direction.
 * After a scan the camera sends out regions that are specified with x, y, width, and length
 * in pixels.  The origin (0,0) is always at the same point no matter which
 * way the scan was.  The camera will always expect all regions to be referenced with
 * 0,0 being at the top left most point of the area scanned in.
 *
 * wpd - The calibration is done in the following way:
 *   - Ask Gerry about this
 *
 * Each camera will have a unique MAC address.
 *
 * @author Bill Darbie
 */
class XrayCamera extends HardwareObject 
{
  private final static int _DETECTOR_WIDTH_IN_PIXELS = 1288;
  private final static int _MAX_NUMBER_CAMERAS = 16; 

  private static Map _cameraIdToInstanceMap = new HashMap();
  private int _cameraId;

  private int _numLinesToSkipDuringPositiveYscan;
  private int _numLinesToSkipDuringNegativeYscan;
  private int _numberOfCaptureLines;

  // location relative to the xray spot
  private int _xLocationInNanoMeters;
  private int _yLocationInNanoMeters;

  private boolean _simAcquisitionEnabled = false;
  private int _simNumLinesToSkipDuringPositiveYscan;
  private int _simNumLinesToSkipDuringNegativeYscan;
  private int _simNumberOfCaptureLines;
  private int _simTimePerCaptureLineInMicros;

  /////////////////////////////////////////////////////////////////////////////
  // methods used during normal operation
  /////////////////////////////////////////////////////////////////////////////

  /**
   * @return an instance of the XrayCamera object for the locationId passed in.
   */
  public static synchronized XrayCamera getInstance(int cameraId)
  {
    XrayCamera instance = (XrayCamera)_cameraIdToInstanceMap.get(new Integer(cameraId));
    if (instance == null)
      instance = new XrayCamera(cameraId);

    return instance;
  }

  /**
   * Set the unique camera ID of this camera.
   * This id is used by the camera to identify
   * the data coming from it.  The ImageConstructionProcessor nodes must have some way to identify
   * which camera a particular set of data is coming from.  This id can be used for that purpose.
   * @author Bill Darbie
   */
  private XrayCamera(int cameraId)
  {
    Assert.expect(cameraId >= 0);
    Assert.expect(cameraId < _MAX_NUMBER_CAMERAS);

    _cameraId = cameraId;
  }

  /**
   * @return the unique Id for this camera
   * @author Bill Darbie
   */
  int getCameraId()
  {
    return _cameraId;
  }

  /**
   * @return  the hardware version of this x-ray sensor hardware.
   * @author Bill Darbie
   */
  String getHardwareVersion() throws HardwareException
  {
    String hardwareVersion = "";
    if (isSimulationModeOn())
      hardwareVersion = "1";
    else
    {
      System.out.println("XrayCamera.getHardwareVersion()stub");
      hardwareVersion = "XrayCamera.getHardwareVersion() stub";
    }

    return hardwareVersion;
  }

  /**
   * @return the serial number of this camera
   * @author Bill Darbie
   */
  String getSerialNumber() throws HardwareException
  {
    String serialNumber = "";
    if (isSimulationModeOn())
    {
      serialNumber = "1";
    }
    else
    {
      System.out.println("XrayCamera.getSerialNumber() stub");
      serialNumber = "XrayCamera.getSerialNumber() stub";
    }

    return serialNumber;
  }

  /**
   * @return the version of software that is running in the camera hardware.
   * @author Bill Darbie
   */
  String getSoftwareVersion() throws HardwareException
  {
    String softwareVersion = "";
    if (isSimulationModeOn())
    {
      softwareVersion = "1";
    }
    else
    {
      System.out.println("XrayCamera.getSoftwareVersion() stub");
      softwareVersion = "XrayCamera.getSoftwareVersion() stub";
    }

    return softwareVersion;
  }

  /**
   * Set the location of where this camera is physically located relative to the center
   * point of all the cameras
   * @param xInNanoMeters is the x location relative to the center of all cameras
   * @param yInNanoMeters is the y location relative to the center of all cameras
   */
  void setLocation(int xLocationInNanoMeters, int yLocationInNanoMeters)
  {
    _xLocationInNanoMeters = xLocationInNanoMeters;
    _yLocationInNanoMeters = yLocationInNanoMeters;
  }

  /**
   * @return the x location of this camera relative the the center of all the cameras.
   * @author Bill Darbie
   */
  int getXLocationInNanoMeters()
  {
    return _xLocationInNanoMeters;
  }

  /**
   * @return the y location of this camera relative the the center of all the cameras.
   * @author Bill Darbie
   */
  int getYLocationInNanoMeters()
  {
    return _yLocationInNanoMeters;
  }

  /**
   * Reset the camera to a known state.
   * @author Bill Darbie
   */
  void reset() throws HardwareException
  {
    _simAcquisitionEnabled = false;
    _simNumLinesToSkipDuringPositiveYscan = 0;
    _simNumLinesToSkipDuringNegativeYscan = 0;
    _simNumberOfCaptureLines = 0;

    _numLinesToSkipDuringPositiveYscan = 0;
    _numLinesToSkipDuringNegativeYscan = 0;
    _numberOfCaptureLines = 0;
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.reset() stub");
    }
  }

  /**
   * Enable the camera so that when the stage hardware trigger arrives the camera will
   * begin acquiring images.
   * @author Bill Darbie
   */
  void enableAcquisition() throws HardwareException
  {
    _simAcquisitionEnabled = true;
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.enable() stub");
    }
  }

  /**
   * Disable the camera so it will ignore the stage hardware trigger.
   * @author Bill Darbie
   */
  void disableAcquisition() throws HardwareException
  {
    _simAcquisitionEnabled = false;
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.disable() stub");
    }
  }

  /**
   * @return true if the camera is enabled.
   * @author Bill Darbie
   */
  boolean isAcquisitionEnabled()
  {
    boolean enabled = _simAcquisitionEnabled;
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.isEnabled() stub");
      // enabled = jni call goes here
    }

    return enabled;
  }

  /**
   * Pass in all information needed before a panel is scanned.  This call will be made
   * once before the entire panel is scanned.
   *
   * It sets things like how long the camera should delay after getting the trigger to begin
   * taking readings and how many readings the sensor should take.  Since each camera is in a
   * different physical location, the time delay after the trigger needs to be different
   * for each one.
   *
   *
   * @param numLinesToSkipDuringPositiveYscan is the number of lines for the camera hardware
   *        to skip before it begins taking exposures when the y axis is moving in the positive
   *        direction.
   * @param numLinesToSkipDuringNegativeYscan is the number of lines  for the camera hardware
   *        to skip before it begins taking exposures when the y axis is moving in the negative
   *        direction.
   * @param numberOfCaptureLines is the number of lines the camera will capture during a single scan path.
   *        The wider the panel under test, the more lines will be required.
   *        This should be the same for all scans on the same panel.
   * @param projectionRegions is a List of ProjectionRegion instances that  provide information
   *        about how many projections to grab out of the scan including the size
   *        of each projection and which ImageReconstructionProcessor it should  go to.
   * @param firsPassIsPositiveYaxisScan will be true to indicate that the camera's first scan 
   *         is in the positive y direction.
   * @author Bill Darbie
   */
  void setScan(boolean firstPassIsPositiveYaxisScan,
               int numLinesToSkipDuringPositiveYscan,
               int numLinesToSkipDuringnegativeYscan,
               List projectionRegions) 
               throws HardwareException
  {
    Assert.expect(numLinesToSkipDuringPositiveYscan >= 0);
    Assert.expect(numLinesToSkipDuringNegativeYscan >= 0);
    /** @todo wpd put in a check for the maximum allowed value here */
    System.out.println("XrayCamera.setScan() stub");

    _simNumLinesToSkipDuringPositiveYscan = numLinesToSkipDuringPositiveYscan;
    _simNumLinesToSkipDuringNegativeYscan = numLinesToSkipDuringNegativeYscan;
    _simNumberOfCaptureLines = numberOfCaptureLines;
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.setScan() stub");
    }
  }

  /**
   * @return the number of lines to skip when scanning in the positive y direction
   * @author Bill Darbie
   */
  int getNumLinesToSkipDuringPositiveYscan() throws HardwareException
  {
    int numLines = _numLinesToSkipDuringPositiveYscan
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.getPositiveDelayTimeAfterTriggerInMicros() stub");
    }

    return numLines;
  }

  /**
   * @return the number of lines to skip when scanning in the negative y direction
   * @author Bill Darbie
   */
  int getNumLinesToSkipDuringNegativeYscan() throws HardwareException
  {
    int numLines = _numLinesToSkipDuringNegativeYscan;
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.getNegativeDelayTimeAfterTriggerInMicros() stub");
    }

    return delayInMicros;
  }

  /**
   * @return the number of lines the camera is set to capture after it gets the hardware trigger
   *         to begin taking exposures.
   * @author Bill Darbie
   */
  int getNumberOfCaptureLines() throws HardwareException
  {
    int lines = _simNumberOfCaptureLines;
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.getNumberOfCaptureLines() stub");
    }

    return lines;
  }

  /**
   * Set the amount of time that the camera will expose itself to reading
   * in x-rays for a single line to be received.
   * @param timePerLineInMicroSecs is the time in microseconds that the camera should capture a single exposure
   * @author Bill Darbie
   */
  void setTimePerCaptureLineInMicros(int timePerCaptureLineInMicros) throws HardwareException
  {
    _simTimePerCaptureLineInMicros = timePerCaptureLineInMicros;
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.setTimePerLineInMicros() stub");
    }
  }

  /**
   * @return the number of microseconds that the camera will use to read in a single line of an image.
   * @author Bill Darbie
   */
  int getTimePerCaptureLineInMicros()
  {
    int timeInMicros = _simTimePerCaptureLineInMicros;
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.getTimePerLinInMicros() stub");
    }

    return timeInMicros;
  }

  /**
   * Download the software required by the camera hardware.
   * I don't know if this is really going to be needed or not.
   * @author Bill Darbie
   */
  void downloadFirmwareProgram() throws HardwareException
  {
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.downloadFirmwareProgram() stub");
    }
  }

  /**
   * Download the software required by the camera hardware.
   * I don't know if this is really going to be needed or not.
   * @author Bill Darbie
   */
  void downloadXilinxProgram() throws HardwareException
  {
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.downloadPXilinxProgram() stub");
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // methods below here are for calibration and diagnostics
  /////////////////////////////////////////////////////////////////////////////

  /**
   * Do a global gain and offset calibration with full xray exposure.
   * This will compensate for different responsiveness of the detectors and their amplifiers.
   * It also adjusts for different signal amplitudes because of different physical locations.
   * It is not very sensitive to temperature so it does not have to be called often.
   *
   * @author Bill Darbie
   */
  void calibrateChannelGainAndOffsetWithXrayExposure()
  {
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.calibrateGlobalGainAndOffsetWithXraysOn() stub");
    }
  }

  /**
   * Do a global gain and offset calibration with no xray signal.
   * This will compensate for the change in offset voltage from leakage current in the detector.
   * This is very temperature sensitive so it should be called often.
   *
   * @author Bill Darbie
   */
  void calibrateChannelGainAndOffsetWithNoXrayExposure()
  {
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.calibrateGlobalGainAndOffsetWithXraysOff() stub");
    }
  }

  /**
   * Calibrate each pixel with full xray exposure.  This is needed to compensate for individual
   * variations in pixel gain and offset.  This should not change often so it does not
   * need to be called often.
   *
   * @author Bill Darbie
   */
  void calibratePixelGainAndOffsetWithXrayExposure()
  {
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.calibratePixelGainAndOffsetWithXraysOn() stub");
    }
  }

  /**
   * Calibrate each pixel with no xray exposure.  This is needed to compensate for individual
   * variations in pixel gain and offset.  This should not change often so it does not
   * need to be called often.
   *
   * @author Bill Darbie
   */
  void calibratePixelGainAndOffsetWithNoXrayExposure()
  {
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.calibratePixelGainAndOffsetWithXraysOff() stub");
    }
  }

  /**
   * For diagnostics
   * @return a List of Integers that represent the gain for each channel in the camera.
   * @author Bill Darbie
   */
  List getChannelGain() throws HardwareException
  {
    System.out.println("XrayCamera.getGlobalGain() stub");
    return new ArrayList();
  }

  /**
   * For diagnostics
   * Directly set the channel gain numbers for diagnostics purposes.
   * @author Bill Darbie
   */
  void setChannelGain(List channelGain)
  {
    System.out.println("XrayCamera.setChannelGain() stub");
  }

  /**
   * For diagnostics
   * @return a List of Integers that represent the offset for each channel in the camera.
   * @author Bill Darbie
   */
  int getChannelOffset() throws HardwareException
  {
    System.out.println("XrayCamera.getGlobalOffset() stub");
    return 0;
  }

  /**
   * For diagnostics.
   * Directory set the channel offset for diagnostic purposes.
   * @param channelOffset is the offset to set the camera to.
   * @author Bill Darbie
   */
  void setChannelOffset(int channelOffset)
  {
    System.out.println("XrayCamera.setChannelOffset() stub");
  }

  /**
   * For diagnostics
   * @return the pixel gain settings of the camera.  Each value in the array represents
   *         a 12 bit pixel value
   * @author Bill Darbie
   */
  int[] getPixelGain() throws HardwareException
  {
    System.out.println("XrayCamera.getPixelGain() stub");
    return new int[0];
  }

  /**
   * For diagnostics.
   * Directory set the pixel gain for diagnostic purposes.
   * @param pixelGain is the gain setting for each pixel
   * @author Bill Darbie
   */
  void setPixelGain(int[] pixelGain)
  {
    System.out.println("XrayCamera.setPixelGain() stub");
  }

  /**
   * For diagnostics
   * @return the pixel offset settings of the camera.  Each value in the array represents
   *         a 12 bit pixel value
   * @author Bill Darbie
   */
  int[] getPixelOffset() throws HardwareException
  {
    System.out.println("XrayCamera.getPixelOffset() stub");
    return new int[0];
  }

  /**
   * For diagnostics.
   * Directory set the pixel offset for diagnostic purposes.
   * @param pixelOffset is the offset setting for each pixel
   * @author Bill Darbie
   */
  void setPixelOffset(int[] pixelOffset)
  {
    System.out.println("XrayCamera.setPixelOffset() stub");
  }

  /**
   * For diagnostics
   * Get the projection image for debugging purposes.  This image will NOT go to any
   * processing nodes.  It will be returned directly to the caller.  The camera will
   * return the last image it scanned in when this method is called.
   * @return the image from one pass of the panel over the sensor
   * @author Bill Darbie
   */
  XrayImage getProjectionImage() throws HardwareException
  {
    System.out.println("XrayCamera.getProjectionImage() stub");
    return null;
  }

  /**
   * For diagnostics
   * @return the width in pixels that this camera outputs.
   * @author Bill Darbie
   */
  int getPixelWidth() throws HardwareException
  {
    System.out.println("XrayCamera.getPixelWidth() stub");
    return 0;
  }

  /**
   * For diagnostics
   * @return the maximum number of lines that this camera can hold
   * @author Bill Darbie
   */
  int getMaxNumLinesInBuffer() throws HardwareException
  {
    System.out.println("XrayCamera.getMaxNumLinesInBuffer() stub");
    return 0;
  }

  /**
   * For diagnostics - get the IP address that this camera uses
   * on the ethernet
   */
  String getIpAddress() throws HardwareException
  {
    System.out.println("XrayCamera.getIpAddress() stub");
    return "";
  }

  /**
   * Have the camera run through its built in selftest.
   * @author Bill Darbie
   */
  void selfTest() throws HardwareException
  {
    if (isSimulationModeOn() == false)
    {
      System.out.println("XrayCamera.seftTest() stub");
    }
  }
}
