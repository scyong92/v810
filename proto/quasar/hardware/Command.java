//package com.agilent.xRayTest.hardware;
package proto.quasar.hardware;

import com.agilent.xRayTest.hardware.*;
import com.agilent.xRayTest.hardware.nativeInt.*;
/**
* The Command class encapsulates the information needed for the
* tester to execute a single command.
*
* @author Bill Darbie
*/
public abstract class Command
{
  /**
   * Execute the command.
   * @author Bill Darbie
   */
  public abstract void execute() throws HardwareException;
}










