package proto.quasar.hardware;

import com.agilent.mtd.util.*;

/**
 * @author Bill Darbie
 */
class PanelHandlerConfigMutable implements PanelHandlerConfig
{
  private int _maxStageRailWidthInNanometers;
  private int _minStageRailWidthInNanometers;
  private int _stageRailHomeWidth;
  private int _leftXstageLoadPositionInNanometers;
  private int _leftYstageLoadPositionInNanometers;
  private StageZPositionEnum _leftZstageLoadPosition;  
  private int _rightXstageLoadPositionInNanometers;
  private int _rightYstageLoadPositionInNanometers;
  private StageZPositionEnum _rightZstageLoadPosition;
  private String _hardwareVersion;
  private String _serialNumber;
  private PanelFlowEnum _panelFlowConstruction;
  private PanelFlowEnum _panelFlowOperation;

  PanelHandlerConfigMutable()
  {
    //Get the info from HardwareConfigEnum
    _maxStageRailWidthInNanometers = 0;
    _minStageRailWidthInNanometers = 0;
    _stageRailHomeWidth = 0;
    _leftXstageLoadPositionInNanometers = 0;
    _leftYstageLoadPositionInNanometers = 0;
    _leftZstageLoadPosition = null;  
    _rightXstageLoadPositionInNanometers = 0;
    _rightYstageLoadPositionInNanometers = 0;
    _rightZstageLoadPosition = null;
    _hardwareVersion = null;
    _serialNumber = null;
    _panelFlowConstruction = null;
    _panelFlowOperation = null;
  }

  void setPanelFlowOperation(PanelFlowEnum panelFlow)
  {
    _panelFlowOperation = panelFlow;
    //Now, update datastore with this setting
  }



  public int getMaxStageRailWidthInNanometers()
  {
    return _maxStageRailWidthInNanometers;
  }

  public int getMinStageRailWidthInNanometers()
  {
    return _minStageRailWidthInNanometers;
  }

  public int getStageRailHomeWidth()
  {
    return _stageRailHomeWidth;
  }

  public int getLeftXstageLoadPositionInNanometers()
  {
    return _leftXstageLoadPositionInNanometers;
  }

  public int getLeftYstageLoadPositionInNanometers()
  {
    return _leftYstageLoadPositionInNanometers;
  }

  public StageZPositionEnum getLeftZstageLoadPosition()
  {
    return _leftZstageLoadPosition;
  }

  public int getRighttXstageLoadPositionInNanometers()
  {
    return _rightXstageLoadPositionInNanometers;
  }

  public int getRightYstageLoadPositionInNanometers()
  {
    return _rightYstageLoadPositionInNanometers;
  }

  public StageZPositionEnum getRightZstageLoadPosition()
  {
    return _rightZstageLoadPosition;
  }
  
  /**
   * This will return the hardware version according to the motion controller.
   * @return the version of the panel positioning hardware.
   * @author Bill Darbie
   */
  public String getHardwareVersion() 
  {
    System.out.println("PanelHandlerConfig.getHardwareVersion() stub");
    return "";
  }

  /**
   * @return the serial number of this hardware from the motion controller.
   * @author Bill Darbie
   */
  public String getSerialNumber() 
  {
    System.out.println("PanelHandlerConfig.getSerialNumber() stub");
    return "";
  }

  public PanelFlowEnum getPanelFlowConstruction()
  {
    return _panelFlowConstruction;
  }

  public PanelFlowEnum getPanelFlowOperation()
  {
    return _panelFlowOperation;
  }

}
