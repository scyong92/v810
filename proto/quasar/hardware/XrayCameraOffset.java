//package com.agilent.xRayTest.hardware;
package proto.quasar.hardware;

/**
 * This class holds the data needed to know what x,y offset was used
 * for a particular reconstuction for a single projection from a single camera.
 * @author Bill Darbie
 */
class XrayCameraOffset
{
  private int _xRayCameraId;
  private int _scanPassNumber;

  // location is relative to the xray spot
  private int _xInPixels;
  // location is relative to the xray spot
  private int _yInPixels;
}

