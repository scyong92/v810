//package com.agilent.xRayTest.hardware;
package proto.quasar.hardware;

import com.agilent.xRayTest.hardware.*;

import com.agilent.util.*;

import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.datastore.hwConfig.*;
import com.agilent.xRayTest.hardware.nativeInt.*;

/**
* This class provides all functionality related to loading and unloading panels
* for the Xray machine.
*
* @author Bill Darbie
*/
public class PanelLoader extends HardwareObject
{
  private static PanelLoader _instance = null;
  private PanelLoadingServer _panelLoadingServer = null;
  private AnalogDigitalIOServer _ioServer = null;
  private HardwareObservable _hardwareObservable = null;
  private boolean _cancelLoad = false;
  private HardwareConfigInt _configInt;

  private static int _widthClearanceInNanoMeters = 0;

  // instance variables used for simulation
  private boolean _panelLoadedSimm = false;
  private int _panelWidthInNanoMetersSimm = 0;
  private boolean _clampsClosedSimm = false;

  private boolean _panelFlowThrough = true;
  private boolean _panelFlowLeftToRight = true;

  /**
  * This contructor is not public on purpose - use the AT5dx object to get
  * references to this class
  *
  * @author Bill Darbie
  */
  private PanelLoader()
  {
    _panelLoadingServer = PanelLoadingServer.getInstance();
    _ioServer = AnalogDigitalIOServer.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();

    _configInt = DatastoreFactory.getHardwareConfigInt();
    double widthClearanceInMils =  _configInt.getDoubleValue(HardwareConfigEnum.PANEL_LOADER_WIDTH_CLEARANCE);

    // convert width clearance from mils to nanoMeters
    _widthClearanceInNanoMeters = 2 * MathUtil.convertMilsToNanoMeters(widthClearanceInMils);
    int loaderConfig = _configInt.getIntValue(HardwareConfigEnum.LOADER_CONFIGURATION);
    if (loaderConfig == 0)
    {
      // left to right flow through
      _panelFlowThrough = true;
      _panelFlowLeftToRight = true;
    }
    else if (loaderConfig == 1)
    {
      // right to left flow through
      _panelFlowThrough = true;
      _panelFlowLeftToRight = false;
    }
    else if (loaderConfig == 2)
    {
      // left to left flow through
      _panelFlowThrough = false;
      _panelFlowLeftToRight = true;
    }
    else if (loaderConfig == 3)
    {
      // right to right flow through
      _panelFlowThrough = false;
      _panelFlowLeftToRight = false;
    }
    else
      Assert.expect(false);

  }

  /**
  * @return an instance of this class.
  * @author Bill Darbie
  */
  public static synchronized PanelLoader getInstance()
  {
    if (_instance == null)
      _instance = new PanelLoader();

    return _instance;
  }

  /**
   * @return  the hardware version
   * @author Bill Darbie
   */
  String getHardwareVersion() throws HardwareException
  {
    String hardwareVersion = "";
    if (isSimulationModeOn())
      hardwareVersion = "1";
    else
    {
      System.out.println("PanelLoader.getHardwareVersion()stub");
      hardwareVersion = "PanelLoader.getHardwareVersion() stub";
    }

    return hardwareVersion;
  }

  /**
   * @return the serial number
   * @author Bill Darbie
   */
  String getSerialNumber() throws HardwareException
  {
    String serialNumber = "";
    if (isSimulationModeOn())
    {
      serialNumber = "1";
    }
    else
    {
      System.out.println("PanelLoader.getSerialNumber() stub");
      serialNumber = "PanelLoader.getSerialNumber() stub";
    }

    return serialNumber;
  }

  /**
   * Reset the panel handling subsystem.
   * @author Bill Darbie
   */
  public void reset() throws HardwareException
  {
    if (isSimulationModeOn() == false)
      _panelLoadingServer.reset();

    _hardwareObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  void enable() throws HardwareException
  {
    System.out.println("PanelLoader.enable() stub");
  }

  /**
   * @author Bill Darbie
   */
  void disable() throws HardwareException
  {
    System.out.println("PanelLoader.disable() stub");
  }

  /**
   * @author Bill Darbie
   */
  boolean isEnabled() throws HardwareException
  {
    System.out.println("PanelLoader.isEnabled() stub");
  }


  /**
  * Load the panel into the machine.  This method returns immediately, even before the
  * panel load has finished.
  *
  * @param widthInNanoMeters is the width of the panel to be loaded.
  * @author Bill Darbie
  */
  public void loadPanelWithoutWaiting(int widthInNanoMeters) throws HardwareException
  {
    _panelWidthInNanoMetersSimm = widthInNanoMeters;
    _panelLoadedSimm = true;

    if (isSimulationModeOn() == false)
    {
      _panelLoadingServer.loadPanelAsynchronously(widthInNanoMeters + _widthClearanceInNanoMeters);
    }

    _hardwareObservable.stateChanged(this);
  }

  /**
  * Pull the panel into the machine.  This method returns only after the panel has been loaded.
  *
  * @param widthInNanoMeters is the width of the panel.
  * @author Bill Darbie
  */
  public void loadPanelAndWait(int widthInNanoMeters) throws HardwareException
  {
    _cancelLoad = false;

    if (isSimulationModeOn() == false)
    {
      loadPanelAsynchronously(widthInNanoMeters);

      // wait until the panel is loaded before returning
      int sleepTime = 100; // sleep for 100 ms
      int timeOut = 120000; // timeout after 120 seconds
      int count = 0;
      int maxCount = timeOut/sleepTime;

      while ((isPanelLoaded() == false) && (_cancelLoad == false) && (count < maxCount))
      {
        ++count;
        try
        {
          Thread.sleep(sleepTime); // sleep for 100msecs
        }
        catch(InterruptedException e)
        {
          // do nothing
        }
      }

      if ((isPanelLoaded() == false) && (_cancelLoad == false) && (count == maxCount))
        throw new HardwareTimeoutException("load panel");
    }
    // loadPanelAsynchronously does the stateChanged() call
  }

  /**
   * @author Bill Darbie
   */
  public void cancelLoadPanel() throws HardwareException
  {
    _cancelLoad = true;
  }

  /**
  * Unload the panel.  This method returns immediately, even before the unload is complete.
  *
  * @author Bill Darbie
  */
  public void unloadPanelWithoutWait() throws HardwareException
  {
    _panelLoadedSimm = false;
    if (isSimulationModeOn() == false)
    {
      _panelLoadingServer.unloadPanel();
    }
    _hardwareObservable.stateChanged(this);
  }

  /**
   * Unload the panel.  This method will return after the panel is really unloaded.
   * @author Bill Darbie
   */
  void unloadPanelAndWait() throws HardwareException
  {
    System.out.println("PanelLoader.unloadPanelAndWait() stub");
  }

  /**
   * Load one panel and at the same time unload the one that is currently in the machine (if
   * a panel is already in the machine).  This call will return immediately, even before the hardware
   * has finished doing the command.
   *
   * @author Bill Darbie
   */
  void loadAndUnloadPanelWithoutWait()
  {
    System.out.println("PanelLoader.loadAndUnloadPanelWithoutWait() stub");
  }

  /**
   * Load one panel and at the same time unload the one that is currently in the machine (if
   * a panel is already in the machine).  This method will return after the load/unload is complete.
   *
   * @author Bill Darbie
   */
  void loadAndUnloadPanelAndWait() throws HardwareException
  {
    System.out.println("PanelLoader.loadAndUnloadPanelAndWait() stub");
  }

  /**
   * Bring the rails that are used for loading the panel to their home position.
   *
   * @author Bill Darbie
   */
  public void home() throws HardwareException
  {
    if (isSimulationModeOn() == false)
      _panelLoadingServer.homeRails();

    _hardwareObservable.stateChanged(this);
  }

  /**
   *
   * @param flowLeftToRight
   * @author Bill Darbie
   */
  void setPanelFlowLeftToRight(boolean flowLeftToRight) throws HardwareException
  {
    System.out.println("PanelLoader.setPanelFlowLeftToRight() stub");
  }

  /**
   * @return true if the panel flows from left to right, false if it
   * is from right to left.
   * @author Greg Esparza
   */
  boolean doesPanelFlowLeftToRight() throws HardwareException
  {
    return _panelFlowLeftToRight;
  }

  void setFlowThrough(boolean flowThrough)
  {
    System.out.println("PanelLoader.setFlowThrough() stub");
  }

  /**
   * @return true if the panel flows through the system, false
   * if the panel is returned back at the same door as it came in
   * @author Greg Esparza
   */
  boolean doesPanelFlowThrough() throws HardwareException
  {
    return _panelFlowThrough;
  }

  /**
   * @return true if a panel is loaded in the machine.
   * @author Bill Darbie
   */
  public boolean isPanelLoaded() throws HardwareException
  {
    boolean loaded = false;
    if (isSimulationModeOn() == true)
    {
      loaded = _panelLoadedSimm;
    }
    else
    {
      loaded = _panelLoadingServer.isPanelLoaded();
    }

    return loaded;
  }

  /**
  * @return the width that the rails are set to in NanoMeters
  * @author Bill Darbie
  */
  public int getRailWidthInNanoMeters() throws HardwareException
  {
    int railWidth = 0;

    if (isSimulationModeOn() == true)
    {
      railWidth = _panelWidthInNanoMetersSimm;
    }
    else
    {
      railWidth = _panelLoadingServer.getRailWidthInNanoMeters();
    }

    return railWidth;
  }

  /**
  * @return true if the clamps that hold the panel are closed.
  * @author Bill Darbie
  */
  public boolean areClampsClosed() throws HardwareException
  {
    boolean clampsClosed = false;

    if (isSimulationModeOn() == true)
    {
      clampsClosed = _clampsClosedSimm;
    }
    else
    {
      clampsClosed = _ioServer.areClampsClosed();
    }

    return clampsClosed;
  }

  /**
  * Close the clamps that hold the panel.
  *
  * @author Bill Darbie
  */
  public void closeClamps() throws HardwareException
  {
    _clampsClosedSimm = true;
    if (isSimulationModeOn() == false)
    {
      _ioServer.setClamps(false);
    }

    _hardwareObservable.stateChanged(this);
  }

  /**
  * Open the clamps that hold the panel.
  *
  * @author Bill Darbie
  */
  public void openClamps() throws HardwareException
  {
    _clampsClosedSimm = false;
    if (isSimulationModeOn() == false)
    {
      _ioServer.setClamps(true);
    }

    _hardwareObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  void extendLeftPanelStop(boolean extendStop) throws HardwareException
  {
    System.out.println("PanelLoader.extendLeftPanelStop() stub");
  }

  /**
   * @author Bill Darbie
   */
  boolean isLeftPanelStopExtended() throws HardwareException
  {
    System.out.println("PanelLoader.isLeftPanelStopExtended() stub");
  }

  /**
   * @author Bill Darbie
   */
  void extendRightPanelStop(boolean extendStop) throws HardwareException
  {
    System.out.println("PanelLoader.extendRightPanelStop() stub");
  }

  /**
   * @author Bill Darbie
   */

  boolean isRightPanelStopExtended() throws HardwareException
  {
    System.out.println("PanelLoader.isRightPanelStopExtended() stub");
  }

  /**
   * @author Bill Darbie
   */
  boolean isPanelInPlaceLeft() throws HardwareException
  {
    System.out.println("PanelLoader.isPanelInPlaceLeft stub");
  }

  /**
   * @author Bill Darbie
   */
  boolean isPanelInPlaceRight() throws HardwareException
  {
    System.out.println("PanelLoader.isPanelInPlaceRight stub");
  }

  /**
   * @author Bill Darbie
   */
  boolean isLeftPanelTrailingEdgeDetected() throws HardwareException
  {
    System.out.println("PanelLoader.isLeftPanelTrailingEdgeDetected() stub");
  }

  /**
   * @author Bill Darbie
   */
  boolean isRightPanelTrailingEdgeDetected() throws HardwareException
  {
    System.out.println("PanelLoader.isRightPanelTrailingEdgeDetected() stub");
  }

  /**
   * Turn on the stage belt so it starts moving.
   * @author Bill Darbie
   */
  public void setStageBeltOn(boolean on) throws HardwareException
  {
    System.out.println("PanelPositioner.setStageBeltOn() stub");
  }

  /**
   * Check to see if the stage belt is moving.
   * @author Bill Darbie
   */
  public boolean isStageBeltOn() throws HardwareException
  {
    System.out.println("PanelPositioner.isStageBeltOn() stub");
    return false;
  }

  /**
   * Set the direction that the belt will move to either bring the panel into the machine or to bring it out.
   *
   * @param forward will set the belt direction to move the panel into the machine if true
   * @author Bill Darbie
   */
  public void setBeltDirection(boolean leftToRight)  throws HardwareException
  {
    System.out.println("PanelPositioner.setBeltDirection() stub");
  }

  /**
   * @return true if the belt direction is set to go from the left of the machine to the right.
   * @author Bill Darbie
   */
  public boolean isBeltDirectionFromLeftToRight() throws HardwareException
  {
    System.out.println("PanelPositioner.isBeltDirectionFromLeftToRight() stub");
    return false;
  }

  /**
   * Tell the system to stop any current movement.
   * @author Bill Darbie
   */
  void stop() throws HardwareException
  {
  }
}


