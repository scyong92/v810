//package com.agilent.xRayTest.hardware;
package proto.quasar.hardware;

import java.util.*;

/**
 * The image and information about what joints are contained in the image and things like
 * the magnification, location, etc of the image.
 * @author Bill Darbie
 */
class Image
{
  // one image per slice height ordered from top to bottom
  private XrayImage _xrayImage;
  private List _joints;
  private int _sliceHeightInNanoMeters;

  // x,y offset of the top left corner of the image relative to the center of the component
  private int _imageXInNanoMeters;
  private int _imageYInNanoMeters;
  private int _xMagnification;
  private int _yMagnification;

  // wpd we need an x,y offset for each camera
  private List xrayCameraOffsetsForSharpestFocus;
  private List xrayCameraOffsetsForSliceHeight;

  // the x correction needed to align the image and the CAD
  private int _xAlignmentCorrectionInNanoMeters;
  // the y correction needed to align the image and the CAD
  private int _yAlignmentCorrectionInNanoMeters;

  // this is useful for telling nearby ReconstructionRegions what height to use
  private int _sharpestFocusHeightInNanoMeters;
  private double _sharpness;
}