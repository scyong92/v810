//package com.agilent.xRayTest.hardware;
package proto.quasar.hardware;

import com.agilent.xRayTest.hardware.*;

import com.agilent.util.*;
import com.agilent.xRayTest.datastore.panelSettings.*;

/**
* This class contains all the information required to move a panel in the machine.
*
* @author Bill Darbie
*/
public class MovePanelCommand extends Command
{
  private int _xNanoMeters;
  private int _yNanoMeters;
  private int _zNanoMeters;
  private boolean _setMoveProfile;
  private static PanelPositioner _panelPositioner;
  private StageSpeed  _stageSpeed;

  /**
   * @author Bill Darbie
   */
  static
  {
//    AGT5dx agt5dx = AGT5dx.getInstance();
//    _panelPositioner = agt5dx.getPanelPositioner();
  }

  /**
  * Construct a command to move to the x, y, z, and moveProfile specified.
  *
  * @param xNanoMeters the x distance in NanoMeters to move the panel from the home position.
  * @param yNanoMeters the y distance in NanoMeters to move the panel from the home position.
  * @param zNanoMeters the z distance in NanoMeters to move the panel from the home position.
  * @param moveProfile the speed that the panel should be moved.  See PanelPositioner
  *        for valid values for this input.
  *
  * @author Bill Darbie
  */
  public MovePanelCommand(int xNanoMeters, int yNanoMeters, int zNanoMeters, StageSpeed stageSpeed)
  {
    Assert.expect(stageSpeed != null);
    _xNanoMeters = xNanoMeters;
    _yNanoMeters = yNanoMeters;
    _zNanoMeters = zNanoMeters;
    _stageSpeed  = stageSpeed;
    _setMoveProfile = true;
  }

  /**
  * Construct a command to move to the x, y, and z specified.  The moveProfile
  * will not be changed from its last setting.
  *
  * @param xNanoMeters the x distance in NanoMeters to move the panel from the home position.
  * @param yNanoMeters the y distance in NanoMeters to move the panel from the home position.
  * @param zNanoMeters the z distance in NanoMeters to move the panel from the home position.
  *
  * @author Bill Darbie
  */
  public MovePanelCommand(int xNanoMeters, int yNanoMeters, int zNanoMeters)
  {
    _xNanoMeters = xNanoMeters;
    _yNanoMeters = yNanoMeters;
    _zNanoMeters = zNanoMeters;
    _stageSpeed  = StageSpeed.getStageSpeed(_panelPositioner.getFastestMoveProfile());
    _setMoveProfile = false;
  }

  /**
   * @author Bill Darbie
   */
  public boolean moveInX()
  {
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public boolean moveInY()
  {
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public boolean moveInZ()
  {
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public int getXNanoMeters()
  {
    return _xNanoMeters;
  }

  /**
   * @author Bill Darbie
   */
  public int getYNanoMeters()
  {
    return _yNanoMeters;
  }

  /**
   * @author Bill Darbie
   */
  public int getZNanoMeters()
  {
    return _zNanoMeters;
  }

  /**
   * @author Bill Darbie
   */
  public int getMoveProfile()
  {
    return _stageSpeed.getMotionProfile();
  }

  /**
   * @author Bill Darbie
   */
  public double getDelayAfterMoveInSeconds()
  {
    System.out.println("MovePanelCommand.getDelayAfterMoveInSeconds() stub");
    /** @todo wpd find out what a good default is for this */
    return 0.0;
  }

  /**
  * Make the machine do the move of the panel.
  *
  * @author Bill Darbie
  */
  public void execute() throws HardwareException
  {
//    if (_setMoveProfile)
//      _panelPositioner.setMoveProfile(_stageSpeed);
//    _panelPositioner.setXYZPositionInNanoMeters(_xNanoMeters, _yNanoMeters, _zNanoMeters);
//    _panelPositioner.move();
  }
}
