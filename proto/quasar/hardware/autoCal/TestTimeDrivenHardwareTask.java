

class TestTimeDrivenHardwareTask extends TimeDrivenHardwareTask
{
  TestTimeDrivenHardwareTask()
  {
    super("TimeDrivenTestTask", 15000, false);
  }

  protected void executeTask() throws com.agilent.xRayTest.hardware.HardwareException
  {
    System.out.println("RUNNING TimeDrivenTestTask");
  }

  protected void cancelTask()
  {
  }
}