
import com.agilent.xRayTest.hardware.*;
import com.agilent.util.*;

/**
 * The HardwareTaskEngine will execute a HardwareTask when it needs to be run.  The HardwareTaskEngine
 * class determines when to run each task based on the information that it gets from the task
 * object.
 *
 * The base HardwareTask class should also log everything it is doing to a file so we can use
 * that to make sure it is working properly!
 *
 * @author Bill Darbie
 */
abstract class HardwareTask
{
  private static HardwareTaskEngine _hardwareTaskEngine;
  private static HardwareWorkerThread _hardwareWorkerThread;

  private String _name;
  // serialize or read/write properties file
  private long _lastTimeRunInMillis;
  private boolean _running = false;
  // get this from FileName
  private String _logFileName;

  /**
   * @author Bill Darbie
   */
  static
  {
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
    _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  HardwareTask(String name)
  {
    Assert.expect(name != null);
    _name = name;
  }

  /**
   * @author Bill Darbie
   */
  String getName()
  {
    return _name;
  }

  /**
   * @author Bill Darbie
   */
  long getElapsedTimeSinceLastRunInMillis()
  {
    long currTimeInMillis = System.currentTimeMillis();
    long elapsedTimeInMillis = currTimeInMillis - _lastTimeRunInMillis;

    return elapsedTimeInMillis;
  }

  /**
   * @author Bill Darbie
   */
  void setRunning(boolean running)
  {
    _running = running;
  }

  /**
   * @author Bill Darbie
   */
  boolean getRunning()
  {
    return _running;
  }

  /**
   * This method should contain the task code.
   * It should check that the task was sucessful.  If it was not then a diagnostic routine
   * should be run to determine the hardware failure.  A HardwareException should be
   * thrown to indicate any errors.
   * @author Bill Darbie
   */
  abstract protected void executeTask() throws HardwareException;

  /**
   * Each Task should provide a cancelTask() method.  This method should stop
   * the task from running immediately.  This call should not block.
   * @author Bill Darbie
   */
  abstract protected void cancelTask();

  /**
   * TimeDrivenHardwareTask and EventDrivenHardwareTask need to implement
   * this method.  It will determine if the task needs to run.  If it
   * does, then executeTask() will be called.
   * @author Bill Darbie
   */
  abstract protected boolean executeIfNeeded() throws HardwareException;

  /**
   * Put the task on the proper thread, then disableEvents so that the task
   * itself does not cause other tasks to run, the enableEvents again.
   * @author Bill Darbie
   */
  protected void execute() throws HardwareException
  {
    _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
    {
      public void run() throws HardwareException
      {
        _hardwareTaskEngine.disableEvents();
        try
        {
          if (executeIfNeeded())
            _lastTimeRunInMillis = System.currentTimeMillis();
        }
        finally
        {
          _hardwareTaskEngine.enableEvents();
        }
      }
    });
  }

  /**
   * Stop the currently running task as quickly as possible.
   * @author Bill Darbie
   */
  protected void cancel()
  {
    if (getRunning())
      cancelTask();
  }
}
