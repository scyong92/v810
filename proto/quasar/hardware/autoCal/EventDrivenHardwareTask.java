package proto.quasar.hardware.autoCal;

import com.agilent.util.*;

import com.agilent.xRayTest.hardware.*;

/**
 * This is the base class for all hardware tasks that need to be run after a specific
 * hardware event (like a panel being loaded into the machine) has occurred.
 * @author Bill Darbie
 */
abstract class EventDrivenHardwareTask extends HardwareTask
{
  private HardwareEventEnum _event;
  private int _frequency; // 1 = on each event, 2 = every 2nd time, etc.
  private int _numEvents = -1;

  /**
   * @param name the name of the task
   * @param event is the HardwareEventEnum that will trigger this task to run
   * @param frequency controls how often this task will run.  If this is set to
   *        1 then every time the event happens, this task will run.  If this
   *        is 2 then the task will run every 2nd time the event occurs, etc.
   * @author Bill Darbie
   */
  EventDrivenHardwareTask(String name, HardwareEventEnum event, int frequency)
  {
    super(name);
    Assert.expect(event != null);
    Assert.expect(frequency > 0);
    _event = event;
    _frequency = frequency;
  }

  /**
   * @author Bill Darbie
   */
  HardwareEventEnum getHardwareEventEnum()
  {
    return _event;
  }

  /**
   * @see HardwareTask#executeTask()
   * @author Bill Darbie
   */
  protected abstract void executeTask() throws HardwareException;

  /**
   * @see HardwareTask#cancelTask()
   * @author Bill Darbie
   */
  protected abstract void cancelTask();

  /**
   * Determine if the task should be run.  If it should then run it.
   * @author Bill Darbie
   */
  protected boolean executeIfNeeded() throws HardwareException
  {
    boolean run = false;
    ++_numEvents;
    // keep reseting to 0 so we will never hit the maximum integer no matter
    // how long the program runs
    if (_numEvents == _frequency)
      _numEvents = 0;
    if (_numEvents % _frequency == 0)
    {
      try
      {
        run = true;
        setRunning(true);
        //String time = TimeUtil.getTimeStamp();
        //String logString = "[" + time + "] start task: " + getName();
        //System.out.println("wpd: " + logString);
        executeTask();
        //logString = "[" + time + "] end task: " + getName();
        //System.out.println("wpd: " + logString);
      }
      finally
      {
        setRunning(false);
      }
    }
    else
    {
      run = false;
      //String time = TimeUtil.getTimeStamp();
      //String logString = "[" + time + "]" + "did not need to run task: " + getName();
      //System.out.println("wpd: " + logString);
    }

    return run;
  }
}
