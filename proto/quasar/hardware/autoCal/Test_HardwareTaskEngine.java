
import java.io.*;

import com.agilent.xRayTest.hardware.*;
import com.agilent.util.*;

/**
 * @author Bill Darbie
 */
public class Test_HardwareTaskEngine extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_HardwareTaskEngine());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    /** @todo wpdfix - this is not the real test class - something much
                       better needs to be done */
    HardwareTaskEngine engine = HardwareTaskEngine.getInstance();
    try
    {
      engine.hardwareEvent(HardwareEventEnum.TEST_EVENT);
    }
    catch(HardwareException he)
    {
      he.printStackTrace();
    }
    for(int i = 0; i < 10000; ++i)
    {
      try
      {
        Thread.sleep(2000);
      }
      catch(InterruptedException ie)
      {
      }
      try
      {
        engine.hardwareEvent(HardwareEventEnum.TEST_EVENT);
      }
      catch(HardwareException he)
      {
        he.printStackTrace();
      }
    }
  }
}