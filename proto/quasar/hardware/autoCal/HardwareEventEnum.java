package proto.quasar.hardware.autoCal;
import com.agilent.util.*;

/**
 * Each method in the Hardware Layer that cooresponds to one of the events
 * below needs to pass the correct HardwareEventEnum through the
 * HardwareObservable.stateChanged call.   The HardwareTaskEngine
 * is an Observer of the HardwareObservable.  If it identifies an Event
 * that causes a calibration, then it will do the proper thing
 */
class HardwareEventEnum extends Enum
{
  private static int _index = -1;

  static HardwareEventEnum TEST_EVENT = new HardwareEventEnum(++_index);

  HardwareEventEnum(int index)
  {
    super(index);
  }
}
