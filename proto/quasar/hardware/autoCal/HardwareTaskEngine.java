import java.util.*;

import com.agilent.util.*;

import com.agilent.xRayTest.hardware.*;

/**
 * This class controls when HardwareTasks get run.
 * There are three ways that a task will be run.
 * 1. A hardware event can trigger an EventDrivenHardwareTask.  This happens when
 *    HardwareObservable.stateChanged() calls hardwareEvent(HardwareEventEnum)
 * 2. A TimeDrivenHardwareTask executes because a certain amount of time has elapsed
 *    since the last time it was run.
 * 3. The system is determined to have been idle for a long period of time.  In this
 *    case some TimeDrivenHardwareTasks may be run earlier than they would have been
 *    to minimize the chance of them running when the system is in use.
 *
 * Note that for this to work all calls from the business layer to the hardware layer
 * must be put on the HardwareWorkerThread.invokeAndWait(RunnableWithExceptions).
 * This includes the PanelInspection.inspect() call, the CandDTest.execute()
 * and any calls used by HardwareStatusGUI
 *
 * We cannot do it in the hardware layer because the inspection code needs to run
 * on multiple threads for it to work properly.  So everything must be done at
 * the business layer.
 *
 * If this is not done multiple
 * threads will be calling into the hardware layer causing unpredictable behavior.
 *
 * Note that this is the same way Swing works with its threading.
 *
 * @author Bill Darbie
 */
// STILL NEEDS - serialize last time run of each task into a single file
//               possibly a properties file or maybe a Java serialized file
//             - what package - I am thinking hardware/autoCal maybe
class HardwareTaskEngine
{
  private static HardwareTaskEngine _instance;
  private static final int _hoursToBeConsideredIdle = 2;

  private List _timeBasedTasks = new ArrayList();
  // map from a HardwareEventEnum to a List of HardwareTasks that should be run
  private Map _eventToTaskMap = new HashMap();
  // all Hardware tasks must be run on this thread
  // serialize (maybe read/write properties files) this somehow!!
  private long _timeOfLastEventInMillis;
  // this thread determines when the system has been idle for a long period of tim
  private WorkerThread _idleSystemThread = new WorkerThread("detectIdleSystemThread");
  private boolean _eventsEnabled = true;
  private boolean _running = false;

  /**
   * @author Bill Darbie
   */
  synchronized static HardwareTaskEngine getInstance()
  {
    if (_instance == null)
      _instance = new HardwareTaskEngine();
    _instance.run();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private HardwareTaskEngine()
  {
    // only use getInstance()
  }

  private void run()
  {
    if (_running == false)
    {
      _running = true;
      buildTimeBasedTaskList();
      buildEventBasedTaskList();
      getPersistentInfo();
      startTimerThreads();
      startIdleSystemThread();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void buildTimeBasedTaskList()
  {
    _timeBasedTasks.add(new TestTimeDrivenHardwareTask());
  }

  /**
   * @author Bill Darbie
   */
  private void buildEventBasedTaskList()
  {
    List testEventTasks = new ArrayList();
    testEventTasks.add(new TestEventDrivenHardwareTask());
    _eventToTaskMap.put(HardwareEventEnum.TEST_EVENT, testEventTasks);
  }

  /**
   * @author Bill Darbie
   */
  private void getPersistentInfo()
  {
    /** @todo wpdfix - read in last time run and other persistent info here */
  }

  /**
   * Start a thread that will determine when the system has been idle.  If an idle condition
   * is detected, run some TimeDrivenHardwareTasks ealier than they would run otherwise
   * to minimize the chance of them running during an inspection and affecting test throughput.
   * @author Bill Darbie
   */
  private void startIdleSystemThread()
  {
    _idleSystemThread.invokeLater(new Runnable()
    {
      public void run()
      {
        long timeToBeConsideredIdleInMillis = _hoursToBeConsideredIdle * 60 * 60 * 1000;
//        long timeToBeConsideredIdleInMillis = 3000;
        long sleepTimeInMillis = timeToBeConsideredIdleInMillis;
        while(true)
        {
          try
          {
            System.out.println("wpd sleeping for: " + sleepTimeInMillis);
            Thread.sleep(sleepTimeInMillis);
          }
          catch(InterruptedException ie)
          {
            // do nothing
          }
          long timeSinceLastEventInMillis = getTimeSinceLastEventInMillis();
          if (timeSinceLastEventInMillis > timeToBeConsideredIdleInMillis)
          {
            System.out.println("wpd system is idle");
            // the system has been idle for a long time - run some
            // TimeDrivenHardwareTasks (do long ones first)
            // after they are run set the correct sleep time
            List tasks = getTasksToRunWhileIdle();
            try
            {
              Iterator it = tasks.iterator();
              while(it.hasNext())
              {
                // check that we are still idle before running each task in case
                // the user started doing some work on the machine
                timeSinceLastEventInMillis = getTimeSinceLastEventInMillis();
                System.out.println("wpd timeSinceLastEvent: " + timeSinceLastEventInMillis);
                if (timeSinceLastEventInMillis > timeToBeConsideredIdleInMillis)
                {
                  System.out.println("wpd executing task");
                  TimeDrivenHardwareTask task = (TimeDrivenHardwareTask)it.next();
                  task.execute();
                }
                else
                {
                  // an event has occurred which means that the system is no longer
                  // idle, so break out of the loop and do not try to run more tasks
                  break;
                }
              }
            }
            catch(Exception ex)
            {
              // notify Observers that there was an error during cal
              // HardwareObservable.notifyObserversOfException(ex)
              /** @todo wpdfix - add this call in */
            }
          }
          sleepTimeInMillis = timeToBeConsideredIdleInMillis - timeSinceLastEventInMillis;
          if (sleepTimeInMillis < 0)
            sleepTimeInMillis = 0;
        }
      }
    });
  }

  /**
   * @author Bill Darbie
   */
  private synchronized long getTimeSinceLastEventInMillis()
  {
    long currTimeInMillis = System.currentTimeMillis();
    return currTimeInMillis - _timeOfLastEventInMillis;
  }

  /**
   * @return a List of TimeDrivenHardwareTask objects in order.  The first task
   *         is in more need of being run than the next.
   * @author Bill Darbie
   */
  private List getTasksToRunWhileIdle()
  {
    Map longRunningTaskMap = new TreeMap();
    Map shortRunningTaskMap = new TreeMap();
    Iterator it = _timeBasedTasks.iterator();
    while(it.hasNext())
    {
      TimeDrivenHardwareTask task = (TimeDrivenHardwareTask)it.next();
      long timeBeforeNextRunInMillis = task.getTimeUntilNextRunInMillis();
      if (task.isLongTask())
        longRunningTaskMap.put(new Long(timeBeforeNextRunInMillis), task);
      else
        shortRunningTaskMap.put(new Long(timeBeforeNextRunInMillis), task);
    }

    // put long tasks first, with the ones that have not been run for
    // the longest time first
    List tasks = new ArrayList(longRunningTaskMap.values());
    // then put in short tasks, again with the ones that have not been run for
    // the longest first
    tasks.addAll(shortRunningTaskMap.values());

    Assert.expect(tasks != null);
    return tasks;
  }

  /**
   * For each TimeDrivenHardwareTask we start up a thread.  That thread will sleep
   * until it is time for the task to be run.  After the task is run the thread
   * will sleep until it is time to be run again.
   * @author Bill Darbie
   */
  private void startTimerThreads()
  {
    Iterator it = _timeBasedTasks.iterator();
    while (it.hasNext())
    {
      final TimeDrivenHardwareTask task = (TimeDrivenHardwareTask)it.next();
      WorkerThread thread = new WorkerThread("thread for task: " + task.getName());
      thread.invokeLater(new Runnable()
      {
        public void run()
        {
          while(true)
          {
            // check time and determine how long to sleep
            long freqInMillis = task.getFrequencyInMillis();
            long timeElapsedInMillis = task.getElapsedTimeSinceLastRunInMillis();
            long sleepTime = freqInMillis - timeElapsedInMillis;
            if (sleepTime < 0)
              sleepTime = 0;
            try
            {
              Thread.sleep(sleepTime);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }

            try
            {
              // execute the task
              task.execute();
            }
            catch(Exception ex)
            {
              // pass this to an Observer/Observable
              // HardwareObservable.notifyObserversOfException(ex)
              /** @todo wpdfix - add this call in */
            }
          }
        }
      });
    }
  }

  /**
   * Hardware events will be ignored after this is called.  HardwareTask.execute() calls
   * this before executing a task
   * @author Bill Darbie
   */
  void disableEvents()
  {
    _eventsEnabled = false;
  }

  /**
   * @author Bill Darbie
   */
  void enableEvents()
  {
    _eventsEnabled = true;
  }

  /**
   * This method gets called from HardwareObservable.stateChanged()
   * If the HardwareEventEnum passed in has a task or list of tasks
   * associated with it, those tasks will be run.
   * @author Bill Darbie
   */
  void hardwareEvent(HardwareEventEnum event) throws HardwareException
  {
    if (_eventsEnabled)
    {
      synchronized(this)
      {
        _timeOfLastEventInMillis = System.currentTimeMillis();
      }

      Assert.expect(event != null);
      List tasks = (List)_eventToTaskMap.get(event);
      Iterator it = tasks.iterator();
      while(it.hasNext())
      {
        EventDrivenHardwareTask task = (EventDrivenHardwareTask)it.next();
        task.execute();
      }
    }
  }

  /**
   * cancel any running tasks
   * @author Bill Darbie
   */
  void cancel()
  {
    Iterator it = _timeBasedTasks.iterator();
    while(it.hasNext())
    {
      TimeDrivenHardwareTask task = (TimeDrivenHardwareTask)it.next();
      task.cancel();
    }
    it = _eventToTaskMap.values().iterator();
    while(it.hasNext())
    {
      List tasks = (List)it.next();
      Iterator lit = tasks.iterator();
      while(lit.hasNext())
      {
        EventDrivenHardwareTask task = (EventDrivenHardwareTask)lit.next();
        task.cancel();
      }
    }
  }
}
