import com.agilent.xRayTest.hardware.*;

class TestEventDrivenHardwareTask extends EventDrivenHardwareTask
{
  TestEventDrivenHardwareTask()
  {
    super("TestEvent", HardwareEventEnum.TEST_EVENT, 1);
  }

  protected void executeTask() throws HardwareException
  {
    System.out.println("RUNNING TestEvent task!");
  }

  protected void cancelTask()
  {
  }
}