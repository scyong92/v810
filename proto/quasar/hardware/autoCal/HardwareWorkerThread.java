import com.agilent.util.*;
import com.agilent.xRayTest.hardware.*;

/**
 * This class uses the WorkerThread class and wraps its invokeAndWait(RunnableWithExceptions)
 * call with one that only throws HardwareException instead of throwing Exception.  It
 * also uses getInstance() to make sure that all the hardware code is using the same
 * thread.
 *
 * @author Bill Darbie
 */
class HardwareWorkerThread
{
  private static HardwareWorkerThread _instance;
  private WorkerThread _workerThread = new WorkerThread("HardwareWorkerThread");

  /**
   * @author Bill Darbie
   */
  static synchronized HardwareWorkerThread getInstance()
  {
    if (_instance == null)
      _instance = new HardwareWorkerThread();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private HardwareWorkerThread()
  {
    // do not allow the constructor to be used
  }

  /**
   * @author Bill Darbie
   */
  void invokeAndWait(RunnableWithExceptions runnable) throws HardwareException
  {
    try
    {
      _workerThread.invokeAndWait(runnable);
    }
    catch(HardwareException he)
    {
      throw he;
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
      Assert.expect(false);
    }
  }
}
