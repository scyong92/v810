import java.util.*;

import com.agilent.xRayTest.hardware.*;
import com.agilent.util.*;

/**
 * This is the base class for all hardware tasks that need to be run
 * at a certain frequency.
 * @author Bill Darbie
 */
abstract class TimeDrivenHardwareTask extends HardwareTask
{
  private long _frequencyInMillis;
  private boolean _longTask;

  /**
   * @param name is the name of the task
   * @param frequencyInMillis determines how often the task should be run
   * @param longTask flags this task as taking a long time or short time to run
   * @author Bill Darbie
   */
  TimeDrivenHardwareTask(String name,
                         int frequencyInMillis,
                         boolean longTask)
  {
    super(name);
    Assert.expect(frequencyInMillis > 0);
    _frequencyInMillis = frequencyInMillis;
    _longTask = longTask;
  }

  /**
   * @return how frequently in milliseconds this task should be run
   * @author Bill Darbie
   */
  long getFrequencyInMillis()
  {
    return _frequencyInMillis;
  }

  /**
   * @author Bill Darbie
   */
  long getTimeUntilNextRunInMillis()
  {
    long elapsedTimeInMillis = getElapsedTimeSinceLastRunInMillis();
    long timeUntilNextRunInMillis = _frequencyInMillis - elapsedTimeInMillis;
    if (timeUntilNextRunInMillis < 0)
      timeUntilNextRunInMillis = 0;

    return timeUntilNextRunInMillis;
  }

  /**
   * @author Bill Darbie
   */
  boolean isLongTask()
  {
    return _longTask;
  }

  /**
   * @see HardwareTask#executeTask()
   * @author Bill Darbie
   */
  protected abstract void executeTask() throws HardwareException;

  /**
   * @see HardwareTask#executeTask()
   * @author Bill Darbie
   */
  protected abstract void cancelTask();

  /**
   * Determine if the task should be run.  If it should then run it.
   * @author Bill Darbie
   */
  protected boolean executeIfNeeded() throws HardwareException
  {
    boolean run = false;
    // do not run the TimeDriven task if it has already run
    // in less than half the time it is required to run
    long timeElapsedSinceLastRun = getElapsedTimeSinceLastRunInMillis();
    if (timeElapsedSinceLastRun > (_frequencyInMillis / 2))
    {
      run = true;
      setRunning(true);
      try
      {
        String time = TimeUtil.getTimeStamp();
        //String logString = "[" + time + "] start task: " + getName();
        // log logString here
        //System.out.println("wpd: " + logString);
        executeTask();
        //logString = "[" + time + "] end task: " + getName();
        //log logString here
        //System.out.println("wpd: " + logString);
      }
      catch(HardwareException he)
      {
        run = false;
        throw he;
      }
      finally
      {
        setRunning(false);
      }
    }

    return run;
  }
}
