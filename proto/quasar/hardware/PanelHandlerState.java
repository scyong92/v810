package proto.quasar.hardware;

import com.agilent.mtd.util.*;

/**
 * @author Greg Esparza
 */
public interface PanelHandlerState
{
  /**
   * Get the current stage rail width
   *
   * @return The current rail width in nanometers
   * @author Greg Esparza
   */
  public int getStageRailWidthInNanometers();

  /**
   * Check if a panel is loaded in the machine
   *
   * @return true if a panel is loaded nand false if it is not.
   * @author Greg Esparza
   */
  public boolean isPanelLoaded();

  /**
   * Get the current panel information
   *
   * The client should first call "isPanelLoaded()" before calling this method. 
   *
   * @return a PanelHandlerPanelInt if a panel is currently loaded in the machine.  Otherwise, the method will return null.
   * @author Greg Esparza
   */
  public PanelData getPanelInformation();

  /**
   * Get the current working state of the Panel Handler
   *
   * @return an enumeration describing the current working state such as idle, loading, etc.
   * @author Greg Esparza
   */
  public PanelHandlerWorkingStateEnum getWorkingState();
  
  /**
   * Get the current panel flow configuration
   *
   * @return an enumeration describing the current panel flow such as left-to-right flow through, etc.
   * @author Greg Esparza
   */
  public PanelFlowEnum getPanelFlow();

  /**
   * Check if inline communication is enabled
   *
   * @return true if inline communication is enabled and false if it is not.
   * @author Greg Esparza
   */
  public boolean isInlineCommunicationEnabled();

  /**
   * Check the current belt direction
   *
   * @return true if the direction is left-to-right and false if it is not.
   * @author Greg Esparza
   */
  public boolean isBeltDirectionFromLeftToRight(); 

  /**
   * Check if the stage rail conveyor belt is on
   *
   * @return true if the belt is on and false if it is not.
   * @author Greg Esparza
   */
  public boolean isBeltOn();

  /**
   * Check if the left Panel-In-Place sensor is extended
   *
   * @return true if the sensor is extended and false if it is not.
   * @author Greg Esparza
   */
  public boolean isLeftPanelInPlaceSensorExtended();

  /**
   * Check if the right Panel-In-Place sensor is extended
   *
   * @return true if the sensor is extended and false if it is not.
   * @author Greg Esparza
   */
  public boolean isRightPanelInPlaceSensorExtended();

  /**
   * Check if the left Panel-In-Place sensor is engaged
   *
   * @return true if the sensor is engaged and false if it is not.
   * @author Greg Esparza
   */
  public boolean isLeftPanelInPlaceSensorEngaged();

  /**
   * Check if the right Panel-In-Place sensor is engaged
   *
   * @return true if the sensor is engaged and false if it is not.
   * @author Greg Esparza
   */
  public boolean isRightPanelInPlaceSensorEngaged();

  /**
   * Check if the clamps are closed
   *
   * @return true if the clamps are closed and false if they are not.
   * @author Greg Esparza
   */
  public boolean areClampsClosed();
}
