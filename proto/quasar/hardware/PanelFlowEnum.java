package proto.quasar.hardware;

import java.io.*;

import com.agilent.mtd.util.*;

/**
 * @author Bill Darbie
 */
public class PanelFlowEnum extends Enum implements Serializable
{
  private static int _index = 0;

  public static final PanelFlowEnum  LEFT_TO_RIGHT_FLOW_THROUGH = new PanelFlowEnum(++_index);
  public static final PanelFlowEnum  LEFT_TO_RIGHT_PASS_BACK = new PanelFlowEnum(++_index);
  public static final PanelFlowEnum  RIGHT_TO_LEFT_FLOW_THROUGH = new PanelFlowEnum(++_index);
  public static final PanelFlowEnum  RIGHT_TO_LEFT_PASS_BACK = new PanelFlowEnum(++_index);


  /**
   * @author Bill Darbie
   */
  private PanelFlowEnum(int id)
  {
    super(id);
  }
}
