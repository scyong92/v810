package proto.quasar.hardware;

import java.io.*;

import com.agilent.mtd.util.*;

/**
 * @author Bill Darbie
 */
public class DigitalIoBinaryValueEnum extends Enum implements Serializable
{
  private static int _index = -1;

  public static final DigitalIoBinaryValueEnum  ZERO = new DigitalIoBinaryValueEnum(++_index);
  public static final DigitalIoBinaryValueEnum  ONE = new DigitalIoBinaryValueEnum(++_index);


  /**
   * @author Bill Darbie
   */
  private DigitalIoBinaryValueEnum(int id)
  {
    super(id);
  }

  /**
   * Get the integer value of the enumeration
   *
   * @author Greg Esparza
   */
  int getValue()
  {
    return _id;
  }
}
