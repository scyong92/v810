//package com.agilent.xRayTest.hardware;
package proto.quasar.hardware;

// wpd - change name to CameraPositionData
/**
 * This class holds the data needed to know where an XrayCamera is located.
 * @author Bill Darbie
 */
class XrayCameraPosition
{
  private int _xRayCameraId;
  // this is the location relative to the center of all the cameras
  // with some calibration information we should be able to convert
  // these nanometers to pixels for the processing node
  // maybe we should store as pixels instead of nanos?

  // wpd - relative to the xray spot as projected on the imaging plane (?) ask Tracy
  private int _xInNanoMeters;
  // wpd - relative to the xray spot
  private int _yInNanoMeters; // don't need this if we trigger cameras individually
}

