package proto.quasar.hardware;

import java.io.*;

import com.agilent.mtd.util.*;

/**
 * @author Bill Darbie
 */
public class DigitalIoInputBitEnum extends Enum implements Serializable
{
  private static int _index = 0;

  public static final DigitalIoInputBitEnum  INPUT_BIT_1 = new DigitalIoInputBitEnum(++_index);
  public static final DigitalIoInputBitEnum  INPUT_BIT_2 = new DigitalIoInputBitEnum(++_index);
  public static final DigitalIoInputBitEnum  INPUT_BIT_3 = new DigitalIoInputBitEnum(++_index);
  public static final DigitalIoInputBitEnum  INPUT_BIT_4 = new DigitalIoInputBitEnum(++_index);


  /**
   * @author Bill Darbie
   */
  private DigitalIoInputBitEnum(int id)
  {
    super(id);
  }

  /**
   * Get the integer value of the enumeration
   *
   * @return the integer value that corresponds to the enumeration
   * @author Greg Esparza
   */
  int getValue()
  {
    return _id;
  }
}
