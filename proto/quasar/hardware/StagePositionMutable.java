package proto.quasar.hardware;

import com.agilent.mtd.util.*;

/**
 * This class is used to describe a stage X, Y, and Z position for the 
 * PanelPositioner. The PanelPositioner will either move to the position 
 * defined in this class or return it's current position .
 *
 * @author Greg Esparza
 */
class StagePositionMutable implements StagePosition
{
  private int _xPositionInNanometers;
  private int _yPositionInNanometers;
  private StageZPositionEnum _zPosition = null;
  private boolean _moveX;
  private boolean _moveY;
  private boolean _moveZ;

  StagePositionMutable()
  {
    _xPositionInNanometers = 0;
    _yPositionInNanometers = 0;
    _zPosition = StageZPositionEnum.POSITION1;
    _moveX = false;
    _moveY = false;
    _moveZ = false;
  }

  /**
   * @author Greg Esparza
   */
  void setXInNanometers(int xPositionInNanometers)
  {
    _xPositionInNanometers = xPositionInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  void setYInNanometers(int yPositionInNanometers)
  {
    _yPositionInNanometers = yPositionInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  void setZ(StageZPositionEnum zPosition)
  {
    _zPosition = zPosition;
  }

  /**
   * @author Greg Esparza
   */
  void moveXaxisOnly()
  {
    _moveX = true;
    _moveY = false;
    _moveZ = false;
  }

  /**
   * @author Greg Esparza
   */
  void moveYaxisOnly()
  {
    _moveX = false;
    _moveY = true;
    _moveZ = false;
  }

  /**
   * @author Greg Esparza
   */
  void moveZaxisOnly()
  {
    _moveX = false;
    _moveY = false;
    _moveZ = true;
  }

  /**
   * @author Greg Esparza
   */
  void moveAllAxes()
  {
    _moveX = true;
    _moveY = true;
    _moveZ = true;
  }

  /**
   * @author Greg Esparza
   */
  public int getXInNanometers()
  {
    return _xPositionInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public int getYInNanometers()
  {
    return _yPositionInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public StageZPositionEnum getZ()
  {
    return _zPosition;
  }

  /**
   * @author Greg Esparza
   */
  public void getAxesToMove(boolean moveX, boolean moveY, boolean moveZ)
  {
    moveX = _moveX;
    moveY = _moveY;
    moveZ = _moveZ;
  }

}
