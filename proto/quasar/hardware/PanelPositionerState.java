package proto.quasar.hardware;

import com.agilent.mtd.util.*;

/**
 * @author Bill Darbie
 */
public interface PanelPositionerState
{
  public StagePosition getCommandedPosition();

  public StagePosition getActualPosition();

  public PointToPointMotionProfileEnum getPointToPointMotionProfile();

  public LinearMotionProfileEnum getLinearMotionProfile();

  public boolean isInMotion();

  public MotionTypeEnum getMotionType();
}
