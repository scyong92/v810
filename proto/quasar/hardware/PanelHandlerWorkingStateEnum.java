package proto.quasar.hardware;

import java.io.*;

import com.agilent.mtd.util.*;

/**
 * @author Bill Darbie
 */
public class PanelHandlerWorkingStateEnum extends Enum implements Serializable
{
  private static int _index = -1;

  public static final PanelHandlerWorkingStateEnum IDLE = new PanelHandlerWorkingStateEnum(++_index);
  public static final PanelHandlerWorkingStateEnum WAITING_FOR_UPSTREAM_READY_TO_SEND_PANEL_SMEMA_SIGNAL = new PanelHandlerWorkingStateEnum(++_index);
  public static final PanelHandlerWorkingStateEnum WAITING_FOR_DOWNSTREAM_READY_TO_RECEIVE_PANEL_SMEMA_SIGNAL = new PanelHandlerWorkingStateEnum(++_index);
  public static final PanelHandlerWorkingStateEnum WAITING_FOR_DOWNSTREAM_RECEIVED_PANEL_SMEMA_SIGNAL = new PanelHandlerWorkingStateEnum(++_index);
  public static final PanelHandlerWorkingStateEnum LOADING = new PanelHandlerWorkingStateEnum(++_index);
  public static final PanelHandlerWorkingStateEnum UNLOADING = new PanelHandlerWorkingStateEnum(++_index);
  public static final PanelHandlerWorkingStateEnum REPOSITIONING_TO_LEFT_PIP = new PanelHandlerWorkingStateEnum(++_index);
  public static final PanelHandlerWorkingStateEnum REPOSITIONING_TO_RIGHT_PIP = new PanelHandlerWorkingStateEnum(++_index);
  public static final PanelHandlerWorkingStateEnum ERROR = new PanelHandlerWorkingStateEnum(++_index);


  /**
   * @author Bill Darbie
   */
  private PanelHandlerWorkingStateEnum(int id)
  {
    super(id);
  }
}
