package proto.quasar.hardware;

import java.io.*;

import com.agilent.mtd.util.*;

/**
 * @author Bill Darbie
 */
public class LinearMotionProfileEnum extends Enum implements Serializable
{
  private static int _index = 0;

  //Motion profile for 10um pixel size at the object plane.
  public static final LinearMotionProfileEnum  PROFILE1 = new LinearMotionProfileEnum(++_index);
  //Motion profile for 16um pixel size at the object plane.
  public static final LinearMotionProfileEnum  PROFILE2 = new LinearMotionProfileEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private LinearMotionProfileEnum(int id)
  {
    super(id);
  }
}
