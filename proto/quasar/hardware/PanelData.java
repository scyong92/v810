package proto.quasar.hardware;

import com.agilent.mtd.util.*;

/**
 * @author Greg Esparza
 */
public interface PanelData 
{
  public String getName();

  public int getLengthInNanometers();

  public int getWidthInNanometers();

  public String getSerialNumber();
}
