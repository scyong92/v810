package proto.quasar.hardware;

import com.agilent.util.*;

/**
 * @author Bill Darbie
 */
public class PanelPositionerState
{
  private PanelPositioner _panelPositioner;
  private int _commandedXinNanoMeters;
  private int _commandedYInNanoMeters;
  private int _actualXinNanoMeters;
  private int _actualYinNanoMeters;
  private int _motionProfile;
  private boolean _inMotion;
  private ErrorStateEnum _errorStateEnum; // wpd - maybe hold a HardwareException here instead?

  /**
   * @author Bill Darbie
   */
  void setXinNanoMeters(int xInNanoMeters)
  {
    _xInNanoMeters = xInNanoMeters;
  }

  /**
   * @author Bill Darbie
   */
  int getXinNanoMeters()
  {
    return _xInNanoMeters;
  }

  /**
   * @author Bill Darbie
   */
  void setYinNanoMeters(int yInNanoMeters)
  {
    _yInNanoMeters = yInNanoMeters;
  }

  /**
   * @author Bill Darbie
   */
  int getYinNanoMeters()
  {
    return _yInNanoMeters;
  }

  /**
   * @author Bill Darbie
   */
  void setMotionProfile(int motionProfile)
  {
    Assert.expect(motionProfile >= _panelPositioner.getSlowestMotionProfile());
    Assert.expect(motionProfile <= _panelPositioner.getFastestMotionProfile());
    _motionProfile = motionProfile;
  }
}