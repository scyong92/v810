//package com.agilent.xRayTest.hardware;
package proto.quasar.hardware;

import com.agilent.xRayTest.hardware.*;


import com.agilent.util.*;

/**
 * This is the high level class that controls the entire image reconstruction process at the
 * hardware level.  It handles calling the XrayCameras, the ImageReconstructionEngines,
 * the Xray, and the PanelPositioner classes in a way that allows images to be scanned in
 * and reconstructed.  The initial list of ReconstructionRegions to be processed is passed into this
 * class, but it is still this classes responsibility to determine which projections to send to which
 * ImageReconstructionEngines on the fly based on how many processors exist and which ones are
 * busy and which are free.
 *
 * In addition to coordinating all of the hardware to get images, this class also will
 * take the initial list of ReconstructionRegions and break them up into smaller regions.
 * It will break them up based on many factors.  Some of them are:
 * - a reconstruction region that crosses over the boundry between two image reconstruction
 *   processors will need to be broken into two regions, one for each processor
 * - a large reconstruction region may be broken into smaller ones so that each one is
 *   at the best possible focus (one focus region per reconstruction region)
 *
 * This class also controls top level functionality like getting the entire system calibrated.
 *
 * @author Bill Darbie
 * @author Matt Wharton
 */
public class ImageAcquisitionEngine
{
  private static ImageAcquisitionEngine _instance;

  /**
   *@associates <{XrayCamera}>
   * @supplierCardinality 1..*
   */
  private List _xRayCameras = new ArrayList();

  private PanelPositioner _panelPositioner;
  private XraySource _xRaySource;
  private WorkerThread _scanningThread = new WorkerThread("Scanning Thread");

  /**
   *@associates <{ImageReconstructionEngine}>
   * @supplierCardinality 1..*
   */
  private List _imageReconstructionEngines = new ArrayList();

  private ImageReconstructionEngine _offlineReconstructionEngine;

  /**
   *@associates <{PanelBlock}>
   * @supplierCardinality 1..*
   */
  private List _panelBlocks = new ArrayList();


  /**
   * @return an instance of this class.
   * @author Bill Darbie
   */
  public static synchronized ImageAcquisitionEngine getInstance()
  {
    if (_instance == null)
      _instance = new ImageAcquisitionEngine();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private ImageAcquisitionEngine()
  {
    // do nothing
  }

  /**
   * This method will return the number 1 for the first version of hardware that
   * we ship with.  If we ever change the hardware in such a way that it would require
   * previous programs to no longer work properly, then the number returned
   * by this method would be incremented.  The business code will use this to
   * determine if it has to regenerate existing programs or not.
   * @author Bill Darbie
   */
  int getHardwareCompatibilityVersion()
  {
  // wpd - need to handle forward and backward compatibilty
    return 1;
  }


  /**
   * Scan the panel and return images through an Observer/Observable design pattern.
   *
   * This method will do the following:
   * - set up cameras (enabled so hardware trigger will activate them)
   *
   * on a scanning thread:
   *  - tell each camera what projectionRegions to send to which processor for each scan
   *  -  set up stage (so it will send triggers if it doesn't always do this)
   *  - set up processors (clear out memory from so they are ready to go)
   *  - check that processors are ready.  Make sure they
   *    - have all the data they should from the previous scan
   *    - have enough memory to take in another scan pass of data
   *    - assign ImageRegions with ImageReconstructionEngines by
   *      determining which processors can be loaded with more ImageRegions and which
   *      ones can be loaded with less (dynamic load balancing)
   *   - scan a pass
   *   - near the end of the scan pass (figure time using software) ask
   *     all ImageReconstructionEngines if they
   *       - have all the data they should from the previous scan
   *       - have enough memory to take in another scan pass of data
   *       - assign ImageRegions with ImageReconstructionEngines by
   *         determining which processors can be loaded with more ImageRegions and which
   *         ones can be loaded with less (dynamic load balancing)
   *   - once all processors are ready scan the next path
   *
   * have one thread for each processor do the following:
   *   - call getImages() wait for that call to return
   *   - when getImages() returns take any alignment and z height data and put it
   *     into any getImages() calls that have not been made yet
   *   - Also put it into the calls that are blocking since they may not have started yet
   *   - then call getImages() again
   *
   *  once all threads are done
   *   - disable cameras
   *   - clear processors
   *   - return from the call
   *
   * DON'T FORGET
   *  - we need to break up ReconstuctionRegions into different ProjectionRegions for
   *    each camera if the ImageRegion crosses over more than one scan path
   *    for a particular camera.  Then these need to be stitched back together by
   *    the ImageReconstructionEngine.
   *
   *  This method will have to do these things:
   *    - setting the scan step
   *    - order of image reconstruction (so no deadlock happens)
   *    - make sure there is not too much image overlap between processing nodes
   *      to avoid deadlock or too much data being sent
   *      To do this the test generator should calculate how large (in y) the
   *      biggest ReconstructionRegion can be.  To do this calculation it
   *      will have to know the magnification, how many bits per pixel, etc
   *      and calculate the amount of memory per inch on the panel.   Then
   *      it needs to make sure no single ReconstuctionRegion is larger than
   *      that.
   *    - same thing about longest ReconstrucitonRegion allowed in x
   *    - possibly might have to make sure the numbeflyr of scan paths is always
   *      an even or odd number
   *    - setting alignment confidence to affect ReconstructionRegion size -
   *      can this be done up front or do we really need this to adjust on
   *      the fly
   *    - might have to handle ReconstructionRegions at angles.
   *    - for skinny panels do we still use all processors, at what
   *      point to we cut down if ever?
   *    - If there's some excessive shading in any of the reconstruction regions, we need to
   *      take that into account and possibly use smaller projection regions in the shaded
   *      parts.  This will emulate the extra integrations we presently do on today's 5DX to
   *      compensate for shading.
   *    - If there are any reconstruction regions which require higher magnification, we need
   *      to adjust the scan step accordingly (i.e. use a smaller scan step in these areas).
   *
   */
  public void acquireImages(List reconstructionRegions)
  {
    Assert.expect(reconstructionRegions != null);

    // remember to check to see if the regions cross boundries that they should not
    // assert if needed
    System.out.println("ImageReconstructionEngine.scanPanel() stub");
  }

  /**
  * Breaks the reconstruction regions up so that they fit within the contraints of the current hardware.
  *
  * @param reconstructionRegions - initial list of reconstruction regions (modified in place).
  * @author Matt Wharton
  */
  void fitReconstructionRegionsToHardwareLimits( List reconstructionRegions )
  {
    Assert.expect( reconstructionRegions != null );
  }

  /**
  * Generates the scan path and passes it to the PanelPositioner.
  *
  * @author Matt Wharton
  */
  private void generateScanPath()
  {
    // Code me!
  }


  /**
  * Tells each camera which reconstruction regions it is responsible for and where to send the projection image data.
  *
  * @author Matt Wharton
  */
  private void assignProjectionRegionsToCameras()
  {
    // Code me!
  }

  /**
   * Calibrate the entire subsystem.
   */
  void calibrate() throws HardwareException
  {
    System.out.println("ImageReconstructionEngine.calibrate() stub");
  }

  /**
   * Confirm that the hardware is OK.
   */
  boolean confirmHardwareIsOk()
  {
    System.out.println("ImageReconstructionEngine.confirmHardwareIsOk() stub");
    return false;
  }

  private void confirmSensorsAreWorking() throws HardwareException
  {
    System.out.println("ImageReconstructionEngine.confirmSensorsAreWorking() stub");
  }

  private void confirmImageConstructionProcessorsAreWorking() throws HardwareException
  {
    System.out.println("ImageReconstructionEngine.confirmImageConstructionProcessorsAreWorking() stub");
  }

  public void initialize() throws HardwareException
  {
    // What does initialize mean?
  }

  public void reset() throws HardwareException
  {
    // What does reset mean??
  }

  private void allocatePanelBlocks()
  {
  }
}
