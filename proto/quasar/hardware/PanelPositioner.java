//package com.agilent.xRayTest.hardware;
package proto.quasar.hardware;

import java.util.*;

import com.agilent.xRayTest.hardware.*;
import com.agilent.util.*;

/**
 * This class controls the movement of the stage.  Specifically, the 
 * stage can move to an X,Y coordinate with point-to-point or linear
 * control.  A point-to-point move does not guarantee coordinated motion
 * both the X and Y axes.  That means that each axis will not finish moving
 * at the same time.  A linear move gurantees this.  The main use for
 * linear motion is during the execution of a panel inspection's scan 
 * path.  
 *
 * @author Greg Esparza
 */
class PanelPositioner extends HardwareObject
{
  private static PanelPositioner _panelPositioner = null;
  private PanelPositionerConfig _panelPositionerConfig = null;
  private PointToPointMotionProfileEnum _pointToPointMotionProfile = null;
  private LinearMotionProfileEnum _linearMotionProfile = null;
  private MotionTypeEnum _activeMotionType = null;
  private boolean _inMotion;
  private StagePositionMutable _actualPosition = null;
  private StagePositionMutable _commandedPosition = null;

  /**
   * Do not allow a direct call to the constructor.  
   * The client must call getInstance()
   *
   * @author Greg Esparza
   */
  private PanelPositioner()
  {
    _panelPositionerConfig = new PanelPositionerConfig();
    _linearMotionProfile = LinearMotionProfileEnum.PROFILE1;
    _pointToPointMotionProfile = PointToPointMotionProfileEnum.PROFILE1;
    _activeMotionType = MotionTypeEnum.POINT_TO_POINT;
    _inMotion = false;
    _commandedPosition = null ;
    _actualPosition = null;
  }

  /**
   * Get an instance of the one and only PanelPositioner
   *
   * @author Greg Esparza
   */
  static synchronized PanelPositioner getInstance()
  {
    if (_panelPositioner == null)
      _panelPositioner = new PanelPositioner();
    
    return _panelPositioner;
  }

  /**
   * Connect to the motion controller
   *
   * @author Greg Esparza
   */
  private void connect() throws HardwareException
  {
    System.out.println("PanelPositioner.connect() stub");
  }

  /**
   * Disconnect from the motion controller
   *
   * @author Greg Esparza
   */
  private void disconnect() throws HardwareException
  {
    System.out.println("PanelPositioner.disconnect() stub");
  }

  /** @todo - make sure Horst, Vincent, George use init to
      do the full thing, reset to do less */
  /**
   * Initialize the Panel Positioner
   *
   * This will run a full initialization of motion controller and stage hardware.  
   * The stage will end up in the home position when the sequence completes.
   *
   * @author Greg Esparza
   */
  void initialize()
  {
    //Get all the appropriate configuration info and store it 
    //Run through the motion controller initialization
    //Home the stage

    System.out.println("PanelPositioner.initialize() stub");
  }

  /**
   * Reset to a known state.
   * @author Greg Esparza
   */
  void reset()
  {
    //Home the stage
    //The will put the system into a known state
    System.out.println("PanelPositioner.reset() stub");
  }

  /**
   * Enable the panel positioning hardware.
   * @author Greg Esparza
   */
  void enable() throws HardwareException
  {
    System.out.println("PanelPositioner.enable() stub");
  }

  /**
   * Disable the panel positioning hardware.
   * @author Greg Esparza
   */
  void disable() throws HardwareException
  {
    System.out.println("PanelPositioner.disable() stub");
  }

  /**
   * Determine if the panel positioning hardware is enabled.
   *
   * @author Greg Esparza
   */
  boolean isEnabled() throws HardwareException
  {
    System.out.println("PanelPositioner.isEnabled() stub");
    return false;
  }

  /**
   * Get the current state of the Panel Positioner 
   *
   * The state information will contain items such as
   * if the stage is moving, if it is executing a point-to-point
   * or linear move, the commanded position, and actual position,
   * etc.
   *
   * @return the current state of the PanelPositioner
   * @author Greg Esparza
   */
  PanelPositionerState getState()
  {
    PanelPositionerStateMutable panelPositionerState = new PanelPositionerStateMutable();

    panelPositionerState.setCommandedPosition(_commandedPosition);
    panelPositionerState.setActualPosition(_actualPosition);
    panelPositionerState.setPointToPointMotionProfile(_pointToPointMotionProfile);
    panelPositionerState.setLinearMotionProfile(_linearMotionProfile);
    panelPositionerState.setMotionType(_activeMotionType);
    panelPositionerState.setMotionState(_inMotion);
 
    System.out.println("PanelPositioner.getState()");
    return panelPositionerState;
  }

  /**
   * Get the Panel Positioner's configuration
   *
   * The configuration information will contain items such as
   * the X and Y axes limits, motion controller type, 
   * motion controller version, etc.
   * 
   * @return the configuration of the PanelPositioner
   * @author Greg Esparza
   */
  PanelPositionerConfig getConfiguration()
  {
    System.out.println("PanelPositioner.getConfiguration()");
    return _panelPositionerConfig;
  }

  /**
   * Make sure that the configuration data matches the hardware configuration
   *
   * @author Greg Esparza
   */
  void verifyConfiguration() throws HardwareException
  {
    System.out.println("PanelPositioner.verifyConfiguration()");
  }

  /**
   * Move the panel to the home position. 

   * This is used to put the stage into a known position.  The encoders 
   * are then set to 0 at this point.   
   *
   * @author Greg Esparza
   */
  private void home() throws HardwareException
  {
    System.out.println("PanelPositioner.home() stub");
  }


  /////////////////////////////////////////////////////////////////////////////
  // methods for point-to-point moves
  /////////////////////////////////////////////////////////////////////////////

  /**
   * Execute a point-to-point move.
   *
   * This type of move does not guarantee the path taken to arrive at the target position
   *
   * @param targetPosition is a StagePosition object that contains the X, Y, and Z positions
   * @author Greg Esparza
   */
  void pointToPointMove(StagePosition targetPosition) throws HardwareException
  {
    _activeMotionType = MotionTypeEnum.POINT_TO_POINT;
    _inMotion = true;
    System.out.println("PanelPositioner.pointToPointMove() stub");
  }

  /**
   * The motion profile defines how fast or slow the panel positioning hardware 
   * actually moves the panel for a point to point move.  
   *
   * @param moveProfile the speed that panels will be moved at.
   * @author Greg Esparza
   */
  void setPointToPointMotionProfile(PointToPointMotionProfileEnum motionProfile) throws HardwareException
  {
    System.out.println("PanelPositioner.setMoveProfile() stub");
  }


  /////////////////////////////////////////////////////////////////////////////
  // methods related to linear movements
  /////////////////////////////////////////////////////////////////////////////


  /**
   * Execute a linear move.
   *
   * A linear move guarantees coordinated motion between the X and Y axes.
   * Specifically, this coordination of axes ensures that the stage will travel
   * in straight line from its current position to the target position.
   *
   * @param targetPosition is a StagePosition object that contains the X, Y, and Z positions
   * @author Greg Esparza
   */
  //void linearMove(StagePosition targetPosition) throws HardwareException
  //{
  //  _activeMotionType = MotionTypeEnum.LINEAR;
  //  _inMotion = true;
  //  System.out.println("PanelPositioner.linearMove() stub");
  //}

  /**
   * Set the linear motion profile
   *
   * The motion profile controls how fast or slow the panel positioning hardware 
   * moves the panel for a linear move.
   *
   * @param motionProfile the speed that panels will be moved at.
   * @author Greg Esparza
   */
  void setLinearMotionProfile(LinearMotionProfileEnum motionProfile) throws HardwareException
  {
    System.out.println("PanelPositioner.setLinearMotionProfile() stub");
  }

  /**
   * Set up the position pulse information for a scan move
   *
   * This method will determine the stage position necessary to start the output of 
   * of scan pulses defined by "numberOfStartingPulses".  The pulses provide a 
   * signal to a camera that it should get ready to start snapping after it sees
   * the set of start pulses.  
   *
   * A scan pulse is provided by the client's definition of "distanceInNanometersPerPulse".  
   * For example, a client can  request a pulse be generated after x nanometers of relative
   * distance.  The relative distance means, for example,  once every 1000 nanometers.  
   *
   * To determine the "distanceInNanometersPerPulse, the client
   * must first determine the ratio of nanometers/encoder count.  They can get
   * this information from the PanelPositionerConfig object.  Once they get
   * this ratio, then they can calculate how much distance that want the 
   * PanelPositioner to move before generating one pulse.
   * 
   *
   * @param distanceInNanometersPerPulse defines the relative distance the 
   *                                     PanelPositoner must move before the servo
   *                                     control generates one output pulse.
   * @param numberOfStartingPulses is the number of pulses that must be 
   *                               generated before the start of a scan pass.
   * @author Greg Esparza
   */
  void setPositionPulse(int numberOfStartingPulses, int distanceInNanometersPerPulse)
  {
    System.out.println("PanelPositioner.setPositionPulse() stub");
  }

  /**
   * @author Greg Esparza
   */
  void enablePositionPulse()
  {
    System.out.println("PanelPositioner.enablePositionPulse() stub");
  }

  /**
   * @author Greg Esparza
   */
  void disablePositionPulse()
  {
    System.out.println("PanelPositioner.disablePositionPulse() stub");
  }

  /**
   * @author Greg Esparza
   */
  boolean isPositionPulseEnabled()
  {
    System.out.println("PanelPositioner.isPositionPulseEnabled() stub");
    return false;
  }

  /**
   * Set up the entire scan path
   *
   * @param scanPath is a List of ScanPass objects; each describing one scan pass and corresponding scan step.
   */
  void setScanPath(List scanPath)
  {
    System.out.println("PanelPositioner.setScanPath() stub");
  }


  /**
   * This method will make the stage do a single scan pass move at constant velocity.
   *
   * @author Greg Esparza
   */
  void runNextScanPassAndWait(int percentageOfScanPassToWait) throws HardwareException
  {
    System.out.println("PanelPositioner.runNextScanPassAndWait() stub");
  }

  /**
   * Tell the system to stop any current movement.
   * @author Greg Esparza
   */
  void stop() throws HardwareException
  {
    System.out.println("PanelPositioner.stop() diagnostic stub");
  }


  /////////////////////////////////////////////////////////////////////////////
  // Run all diagnostics
  /////////////////////////////////////////////////////////////////////////////


  /**
   *
   * @author Greg Esparza
   */
  void runAllDiagnostics() throws HardwareException
  {
    System.out.println("PanelPositioner.runAllDiagnostics() diagnostic stub");
  }

  /////////////////////////////////////////////////////////////////////////////
  // Motion controller diagnostics
  /////////////////////////////////////////////////////////////////////////////

  /**
   *
   * @author Greg Esparza
   */
  void runAllMotionControllerDiagnostics() throws HardwareException
  {
    System.out.println("PanelPositioner.runAllMotionControllerDiagnostics() diagnostic stub");
  }

  /**
   *
   * @author Greg Esparza
   */
  private void checkIfMotionControllerDeviceDriverIsRunning() throws HardwareException
  {
    System.out.println("PanelPositioner.checkIfMotionControllerDeviceDriverIsRunning() diagnostic stub");
  }

  /**
   *
   * @author Greg Esparza
   */
  private void checkMotionControllerUpstreamCommunication() throws HardwareException
  {
    System.out.println("PanelPositioner.checkMotionControllerUpstreamCommunication() diagnostic stub");
  }


  /**
   *
   * @author Greg Esparza
   */
  private void checkMotionControllerDownstreamCommunication() throws HardwareException
  {
    System.out.println("PanelPositioner.checkMotionControllerDownstreamCommunication() diagnostic stub");
  }

  /**
   *
   * @author Greg Esparza
   */
  private void checkMotionControllerConfiguration() throws HardwareException
  {
    System.out.println("PanelPositioner.checkMotionControllerConfiguration() diagnostic stub");
  }

  /////////////////////////////////////////////////////////////////////////////
  // X-Y amplifier diagnostics
  /////////////////////////////////////////////////////////////////////////////

  /**
   *
   * @author Greg Esparza
   */
  void runAllXYamplifierDiagnostics() throws HardwareException
  {
    System.out.println("PanelPositioner.runAllXYamplifierDiagnostics() diagnostic stub");
  }

  /**
   *
   * @author Greg Esparza
   */
  private void checkXampFaultState() throws HardwareException
  {
    System.out.println("PanelPositioner.checkXampFaultState() diagnostic stub");
  }

  /**
   *
   * @author Greg Esparza
   */
  private void checkYampFaultState() throws HardwareException
  {
    System.out.println("PanelPositioner.checkYampFaultState() diagnostic stub");
  }

  /**
   *
   * @author Greg Esparza
   */
  private void checkXampOutputDriveSignal() throws HardwareException
  {
    System.out.println("PanelPositioner.checkXampOutputDriveSignal() diagnostic stub");
  }

  /**
   *
   * @author Greg Esparza
   */
  private void checkYampOutputDriveSignal() throws HardwareException
  {
    System.out.println("PanelPositioner.checkYampOutputDriveSignal() diagnostic stub");
  }


  /////////////////////////////////////////////////////////////////////////////
  // X-Y motor diagnostics
  /////////////////////////////////////////////////////////////////////////////

  /**
   *
   * @author Greg Esparza
   */
  void runAllXYmotorDiagnostics() throws HardwareException
  {
    System.out.println("PanelPositioner.runAllXYmotorDiagnostics() diagnostic stub");
  }

  /**
   *
   * @author Greg Esparza
   */
  private void checkXmotorEncoder() throws HardwareException
  {
    System.out.println("PanelPositioner.checkXmotorEncoder() diagnostic stub");
  }

  /**
   *
   * @author Greg Esparza
   */
  private void checkYmotorEncoder() throws HardwareException
  {
    System.out.println("PanelPositioner.checkYmotorEncoder() diagnostic stub");
  }

  /////////////////////////////////////////////////////////////////////////////
  // Z-axis position control diagnostics
  /////////////////////////////////////////////////////////////////////////////

  /**
   *
   * @author Greg Esparza
   */
  void runAllZpositionDiagnostics() throws HardwareException
  {
    System.out.println("PanelPositioner.runAllZpositionDiagnostics() diagnostic stub");
  }

  /**
   *
   * @author Greg Esparza
   */
  private void checkZpositionControl() throws HardwareException
  {
    System.out.println("PanelPositioner.checkZpositionControl() diagnostic stub");
  }


  /////////////////////////////////////////////////////////////////////////////
  // Home diagnostics
  /////////////////////////////////////////////////////////////////////////////

  /**
   *
   * @author Greg Esparza
   */
  void runAllHomePositionDiagnostics() throws HardwareException
  {
    System.out.println("PanelPositioner.runAllHomeDiagnostics() diagnostic stub");
  }

  /**
   *
   * @author Greg Esparza
   */
  private void checkHomePosition() throws HardwareException
  {
    System.out.println("PanelPositioner.checkHomePosition() diagnostic stub");
  }


}
