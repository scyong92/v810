#ifndef EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_MAGNIFICATIONCALIBRATIONDATA_H
#define EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_MAGNIFICATIONCALIBRATIONDATA_H

/**
* This class contains the result data for the magnification calibration
* @author Horst Mueller
*/
class MagnificationCalibrationData
{
public:
  static MagnificationCalibrationData* getInstance(MagnificationEnum magnification);
  static void removeInstance(MagnificationCalibrationData*& magnificationCalibrationData);

  void setXmagnification(double xMagnification);
  void setYmagnification(double yMagnification);
  void setXpixelPitch(double xPixelPitch);
  void setYpixelPitch(double yPixelPitch);
  double getXmagnification() const;
  double getYmagnification() const;
  double getXpixelPitch() const;
  double getYpixelPitch() const;

  void setMagnificationCalibrationData(MagnificationCalibrationData const& magnificationCalibrationData);

private:
  MagnificationCalibrationData();
  ~MagnificationCalibrationData();    
  MagnificationCalibrationData(MagnificationCalibrationData const& magnificationCalibrationData);
  MagnificationCalibrationData& operator=(MagnificationCalibrationData const& rhs);

  static std::vector<MagnificationCalibrationData*> _pInstances;
  // x magnification
  double _xMagnification;
  // y magnification
  double _yMagnification;
  // x pixel pitch
  double _xPixelPitch;
  // y pixel pitch
  double _yPixelPitch;
  // x pixels to nano meter conversion factor ( = 1 / _xPixelPitch )
  double _xPixelsToNanoMeters;
  // y pixels to nano meter conversion factor ( = 1 / _yPixelPitch )
  double _yPixelsToNanoMeters;
};

#endif //EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_MAGNIFICATIONCALIBRATIONDATA_H
