#ifndef EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_SOCKETDATARECEIVER_H
#define EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_SOCKETDATARECEIVER_H

/**
* This class receives projection data via a socket connection. The class is 
* implemented as a function object which used as the argument when creating 
* a seperate thread.
* @author Horst Mueller
*/
class SocketDataReceiver
{
public:
  SocketDataReceiver();
  ~SocketDataReceiver();
  operator () ();
private:
  SocketDataReceiver(SocketDataReceiver const& SocketDataReceiver);
  SocketDataReceiver& operator=(SocketDataReceiver const& rhs);

};

#endif //EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_SOCKETDATARECEIVER_H
