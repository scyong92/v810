#ifndef EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_PROJECTIONREGIONDATA_H
#define EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_PROJECTIONREGIONDATA_H

/**
* This class contains the data of a projection region from a single camera
* It is assumed that the origin of the projection (Line 0 and Row 0) is at the
* top left corner of the image
* @author Horst Mueller
*/
class ProjectionRegionData
{
public:
  ProjectionRegionData(int scanPassNumber,
                       int cameraId,
                       int systemFiducialXcoordinateInPixelsInPixels,
                       int topLeftCornerXcoordinateInPixels,
                       int topLeftCornerYcoordinateInPixels,
                       int bottomRightCornerXcoordinateInPixels,
                       int bottomRightCornerYcoordinateInPixels,
                       Image* pImage);
  ~ProjectionRegionData();

  int getScanPassNumber() const;
  int getCameraId() const;
  int getSystemFiducialXcoordianteInPixels() const
  int getTopLeftCornerXcoordinateInPixel() const;
  int getTopLeftCornerYcoordinateInPixels() const;
  int getTottomRightCornerXcoordinateInPixels() const;
  int getTottomRightCornerYcoordinateInPixels() const;
  Image const* getImage() const;

private:
  ProjectionRegionData(ProjectionRegionData const& projectionRegionData);
  ProjectionRegionData& operator=(ProjectionRegionData const& rhs);
  //The x coordinate of the top left corner
  int _topLeftCornerXcoordinateInPixels;
  //The y coordinate of the top left corner
  int _topLeftCornerYcoordinateInPixels;
  //The x coordinate of the bottom right corner
  int _bottomRightCornerXcoordinateInPixels;
  //The y coordinate of the bottom right corner
  int _bottomRightCornerYcoordinateInPixels;
  //Number of the scan pass
  int _scanPassNumber;
  //Camera ID
  int _cameraId;
  //If the camera width is assumed to be very wide this is the
  //x coordinateInPixels iat which the system fiducial would have been imaged
  int _systemFiducialXcoordianteInPixels;
  //A pointer to the image data
  Image* _pImageData;
};

#endif //EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_PROJECTIONREGIONDATA_H
