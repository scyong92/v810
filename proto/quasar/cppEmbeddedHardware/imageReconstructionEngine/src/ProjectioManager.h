#ifndef EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_PROJECTIOMANAGER_H
#define EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_PROJECTIOMANAGER_H

/**
* This class manages the projection data that is sent from each individual camera
* @author Horst Mueller
*/
class Observer;

class ProjectionManager: Subject
{

public:
  static ProjectionManager* getInstance();
  static void removeInstance();

  void setNumberOfScanPasses(int numberOfScanPasses);
  void setProjectionRegionData(ProjectionRegionData* pProjectionRegionData);
  void deleteProjectionRegionData(int xStart, int xEnd);
  bool isContainedInProjection(Rectangle const& pixelRegion, int cameraId, int& scanPassNumber) const;
  bool hasInterSectionWithProjection(Rectangle const& pixelRegion, int cameraId,  int& scanPassNumber) const;
  Rectangle getInterSectionWithProjection(Rectangle const& pixelRegion,  int cameraId,  int scanPassNumber) const;
  int getSytemFiducialXcoordianteInPixels(int cameraId, int scanPassNumber) const; 
  Image getProjectionRegionData(Rectangle const& pixelRegion, int cameraId, int scanPassNumber) const;
  bool areAllProjectionsReceivedForScanPass(int scanPassNumber) const;

private:
  ProjectionManager(ImageReconstructionEngine* pImageReconstructionManager);
  ~ProjectionManager();    
  ProjectionManager(ProjectionManager const& projectionManager);
  ProjectionManage & operato =(ProjectionManager const& rhs);
  void calculateSystemFiducialXcoordianteInPixels(int scanPassNumber, int cameraId);
  bool areAllProjectionsReceivedForScanPass(int scanPassNumber);

  static ProjectionManager* _pInstance;
  /**
  * @associates <{Observer}>
  */
  std::list<Observer*> _observers;
  /**
   * @shapeType AggregationLink
   * @associates <b>ProjectionRegionData</b>
   * @clientCardinality 1
   * @supplierCardinality 1..*
   */
  //For each camera create an array which contains all scan passes. For each scan pass create an array
  //that can hold maxNumberOfDataBlocksPerScanPass of pointers to a ProjectionData object
  ProjectionRegionData**** _projectionData;
  int _numberOfCameras;
  int _numberOfScanPasses;
  int _maxNumberOfDataBlocksPerScanPass;

};
#endif //EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_PROJECTIOMANAGER_H