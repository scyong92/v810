#ifndef EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_ALIGNMENTDATA_H
#define EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_ALIGNMENTDATA_H

/**
* This class holds the data that is created for a single alignment region. The data 
* consists of the coordinates where the feature is expected to be located and
* the actual coordinates that result from the alignment process
* @author Horst Mueller
*/
class AlignmentData
{
public:
    AlignmentData(int expectedXcoordinateInNanoMeters, 
                  int expectedYcoordinateInNanoMeter,
                  int xCoordinateInNanoMeters,
                  int yCoordinateInNanoMeters);
    AlignmentData(AlignmentData const& alignmentData);
    ~AlignmentData();
    AlignmentData& operator=(AlignmentData const& rhs);
    int getExpectedXcoordinateInNanoMeters() const;
    void setExpectedXcoordinateInNanoMeters(int expectedXcoordinateInNanoMeters);
    int getExpectedYcoordinateInNanoMeter() const;
    void setExpectedYcoordinateInNanoMeter(int expectedYcoordinateInNanoMeter);
    int getXcoordinateInNanoMeters() const;
    void setXcoordinateInNanoMeters(int xCoordinateInNanoMeters);
    int getYcoordinateInNanoMeters() const;
    void setYcoordinateInNanoMeters(int yCoordinateInNanoMeters);

private:
    int _expectedXcoordinateInNanoMeters;
    int _expectedYcoordinateInNanoMeter;
    int _xCoordinateInNanoMeters;
    int _yCoordinateInNanoMeters;
};

#endif //EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_ALIGNMENTDATA_H 
