#ifndef EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_IMAGERECONSTRUCTION_H
#define EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_IMAGERECONSTRUCTION_H

#include <list>

/**
 * This class reconstructs an image for a given region in object space and for a given list of 
 * radial offset or for a delta z from the reference z plane.
 *
 * @author Horst Mueller
 */
class ImageReconstructor
{
public:
  static ImageReconstructor* getInstance();
  static void removeInstance();

  void initialize(MagnificationEnum magnification);
  Image reconstructNoStitching(Rectangle const& objectSpaceRegion, double deltaZ);
  Image reconstructNoStitching(vector<RadialOffset> listOfRadialOffsets, Rectangle objectSpaceRegion, double deltaZ);
  Image reconstructWithStitching(Rectangle objectSpaceRegion, double deltaZ);
  Image reconstructWithStitching(vector<RadialOffset> listOfRadialOffsets, Rectangle objectSpaceRegion, double deltaZ);
  std::list<RadialOffset> getlistOfRadialOffsets() const;

private:
  ImageReconstructor();
  ~ImageReconstructor();  
  ImageReconstructor(ImageReconstructor const& imageReconstructor);
  ImageReconstructor & const operator=(ImageReconstructor const& rhs);
  static ImageReconstructor* _pInstance;
  int _numberOfCameras;
  std::list<Image*> _numeratorListOfImages;
  Image _denominatorImage;
  int _referenceCameraId; 
  //position of x-ray cameras
  std:: vector<XrayCameraPositionData> _xRayCameraPositions;
  //y offsets in pixels which indicate the pixel address of the panel origin
  //in y direction.
  std:: vector<int> _yOffsets;
  //x position of the panel during a scan pass
  //where the vector index corresppnds to the scan pass number
  std::vector<int> _scanPassToXlocationInNanoMeters;
  int _scanPassWidth;
  std::vector<int> _scanPassNumbers;
  //system geometrie:
  //_zSource = z-height of x-ray source above the detector
  //_zReference = z-height of plane that contains system fiducial
  int _zSource;
  int _zReference;
  //Radial offsets for a reconstructed image
  std::list<RadialOffset> _listOfRadialOffsets;
  //Convert coordiantes that describe a location in the object space into pixel coordiantes 
  //in the image plane
  void convertObjectPlaneCoordiantesToPixels(Rectangle const& objectSpaceRegion, Rectangle & pixelRegion, int cameraId, int scanPassNumber);
  void convertObjectPlaneCoordiantesToPixels(Point const& objectSpacePoint, Point & pixelPoint, int cameraId, int scanPassNumber);
  //For a given point on the panel and a camera find the list of scan passes during which the point was
  //captured by that camera
  void findScanPasses(Point const& objectSpacePoint, int cameraId);
  Point getRadialOffset(int cameraId, int scanPassNumber, float deltaZ);
  void upDateDenominatorImage(Rectangle const& intersection);
  //scan pass x location (stage position) during a given scan pass for a given 
  //camera coordinate system
  int getScanPassXLocationInNanoMeters(int cameraId, int scanPassNumber);
};

#endif //EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_IMAGERECONSTRUCTION_H