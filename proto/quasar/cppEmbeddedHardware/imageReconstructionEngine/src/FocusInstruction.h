#ifndef EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_FOCUSINSTRUCTION_H
#define EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_FOCUSINSTRUCTION_H

/**
* This class holds the focus instructions that are used to by the auto focus algorithm
* to identify the correct slice and slice height.
* @author Horst Mueller                            
*/
class FocusInstruction
{
public:
  FocusInstruction(FocusSearchMethodEnum focusSearchMethod,
                   int zHeightEstimateInNanoMeters,
                   int searchRangeInNanoMeters,
                   int zHeightAcceptableRangeInNanoMeters,
                   int sharpnessAcceptableRangeInNanoMeters,
                   SharpnessMetricEnum sharpnessMetric);

  FocusInstruction(FocusInstruction const& focusInstruction);
  ~FocusInstruction();
  FocusInstruction& operator=(FocusInstruction const& rhs);

  FocusSearchMethodEnum getFocusSearchMethod() const;
  void setFocusSearchMethod(int focusSearchMethod);
  int getzHeightEstimateInNanoMeters() const;
  void setzHeightEstimateInNanoMeters(int zHeightEstimateInNanoMeters);
  int getSearchRangeInNanoMeters() const;
  void setSearchRangeInNanoMeters(int searchRangeInNanoMeters);
  int getzHeightAcceptableRangeInNanoMeters() const;
  void setzHeightAcceptableRangeInNanoMeters(int zHeightAcceptableRangeInNanoMeters);
  int getSharpnessAcceptableRangeInNanoMeters() const;
  void setSharpnessAcceptableRangeInNanoMeters(int sharpnessAcceptableRangeInNanoMeters);
  SharpnessMetricEnum getSharpnessMetric() const;
  void setSharpnessMetric(SharpnessMetricEnum sharpnessMetric);

private:
  //The method that will be used to identify the "correct slice" 
  FocusSearchMethodEnum _focusSearchMethod;
  //Used as a starting point for the focus search 
  int _zHeightEstimateInNanoMeters;
  //Search this far above and below the z height guess
  int _searchRangeInNanoMeters;
  //Acceptable range  for the z height to be returned.
  //(Used to detect an error in auto focus)          
  int _zHeightAcceptableRangeInNanoMeters;
  //Acceptable range for the sharpness value.
  //(Used to detect an error in auto focus.) 
  int _sharpnessAcceptableRangeInNanoMeters;
  //We plan to vary the focus metric based on region size                
  //and joint type. Focus searches based on lower resolution             
  //images seem to be more robust to noise AND faster. This              
  //element describes exactly how the focus metric should be calculated. 
  SharpnessMetricEnum _sharpnessMetric;

};

#endif //EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_FOCUSINSTRUCTION_H
