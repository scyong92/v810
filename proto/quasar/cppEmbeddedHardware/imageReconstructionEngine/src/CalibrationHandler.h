#ifndef EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_CALIBRATIONHANDLER_H
#define EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_CALIBRATIONHANDLER_H

/**
* This class performs all calibration tasks that involve analyzing projection images
* @author Horst Mueller
*/
class CalibrationHandler
{
public:
  static CalibrationHandler* getInstance();
  static void removeInstance();

  void locateSystemFiducal(MagnificationEnum magnification, Rectangle const& searchRegion);
  void magnificationCalibration(MagnificationEnum magnification,  
                                Rectangle const& searchRegion, 
                                int expectedLength, 
                                int expectedHeight, 
                                int lengthTolerance, 
                                int heightTolerance);
  std::vector<XrayCameraPositionData> xRayCameraXPositionCalibration(std::vector<XrayCameraPositionData*> const& expectedXrayCameraPositionData, 
                                                                     int xToleranceInNanoMeters);
  std::vector<XrayCameraPositionData> xRayCameraYPositionCalibration(std::vector<XrayCameraPositionData*> const& expectedXrayCameraPositionData, 
                                                                     int yToleranceInNanoMeters);
  std::vector<int> cameraTriggerCalibration();
  void xRaySpotCalibration();

 private:    
  CalibrationHandler(ImageReconstructionEngine * pImageReconstructionManager);
  virtual ~CalibrationHandler();
  CalibrationHandler(CalibrationHandler const& calibrationHandler);
  CalibrationHandler& operator=(CalibrationHandler const& rhs);
  Rectangle findRectangle(Image const& image, Rectangle const& searchRegion);
  XrayCameraPositionData locateCamereaXPositionCalFeature(std::vector<XrayCameraPositionData*> const& expectedXrayCameraPositionData);
  XrayCameraPositionData locateCamereaYPositionCalFeature(std::vector<XrayCameraPositionData*> const& expectedXrayCameraPositionData);
  static CalibrationHandler* _pInstance;
};

#endif //EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_CALIBRATIONHANDLER_H
