#ifndef EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_ALIGNMENTHANDLER_H
#define EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_ALIGNMENTHANDLER_H
#include <functional>
#include <list>

/**
* This class aligns the list of alignement regions. The class is implemented as
* a function object which can be used as argument when creating a seperate thread.
* @author Horst Mueller
*/
class AlignmentHandler : unary_function<std::list<AlignementRegion*> const&, void>, Observer
{
public:
   AlignmentHandler();
   ~AlignmentHandler();
   result_type operator()(argument_type& alignementRegions);
   void upDate();

private:
   AlignmentHandler(AlignmentHandler const& alignmentHandler);
   AlignmentHandler& operator=(AlignmentHandler const& rhs);

   void alignRegion(AlignmentRegion alignmentRegion);
   void matchTemplate(Image& templateImage, Image& sourceImage, Rectangle searchRegion, int xShift, int yShift);
   std::list<AlignementRegion*> _alignementRegions;

};

#endif //EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_ALIGNMENTHANDLER_H