#ifndef EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_SYSTEMFIDUCIALDATA_H
#define EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_SYSTEMFIDUCIALDATA_H

/**
* This class holds the system fiducial data which is created by the embedded
* image reconstruction engine at the beginning of every scan path
* @author Horst Mueller
*/
class SystemFiducialData
{
public:
  static SystemFiducialData* getInstance(MagnificationEnum magnification);
  static void removeInstances();
  static void setSystemFiducialData(MagnificationEnum magnification, 
                                    SystemFiducialData & systemFiducialData);
  void setReferenceCameraId(int referenceCameraId);
  void setSystemFiducialXInPixels(int cameraId, 
                                  int systemFiducialXInPixels);
  void setStageXLocationInNanometers(MagnificationEnum magnification, 
                                     int stageXLocationInNanometers);
  int getReferenceCameraId() const;
  int getSystemFiducialXInPixels(int cameraId) const;
  int getStageXLocationInNanometers() const;

private:
  SystemFiducialData();
  ~SystemFiducialData();
  SystemFiducialData(SystemFiducialData const& systemFiducialData);
  SystemFiducialData& operator=(SystemFiducialData const& rhs);
  static std::vector<SystemFiducialData*> _pInstances;
  //Id of the reference camera
  int _referenceCameraId;
  //X coordinate of the system fiducial in each projection
  List _systemFiducialXInPixels;
  //X coordinate of the stage while the fiducial was imaged
  int _stageXLocationInNanometers;

};

#endif//EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_SYSTEMFIDUCIALDATA_H
