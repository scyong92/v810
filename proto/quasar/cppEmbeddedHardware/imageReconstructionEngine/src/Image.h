#ifndef UTIL_IMAGE_H
#define UTIL_IMAGE_H

/**
* This class contains an image of floating point gray values.
* @author Horst Mueller
*/
class Image
{
public:
    Image();    
    Image(int widthInPixels, int lengthInPixels);
    Image(int widthInPixels, int lengthInPixels, float grayValue);
    Image(int widthInPixels, int lengthInPixels, float* pImageBuffer);
    Image(Image const& image);
    virtual ~Image();
    Image& operator=(Image const& rhs);

    int getWidthInPixels() const;
    int getLengthInPixels() const;
    float* getImageBuffer();
    float const* getImageBuffer() const;
	float* getPixelPtr(int x, int y);
    float const* getPixelPtr(int x, int y) const;
    //Add an image to another pixel by pixel
    Image operator+(Image const& rhs);
    //Divide an image by another pixel by pixel
    Image operator/(Image const& rhs);

private:
    int _widthInPixels;
    int _lengthInPixels;
    float* _imageBuffer;
    bool _ownsMemory;
};

#endif//UTIL_IMAGE_H
