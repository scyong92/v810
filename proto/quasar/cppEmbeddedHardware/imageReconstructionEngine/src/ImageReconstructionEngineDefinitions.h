#ifndef IMAGERECONSTRUCTIONENGINEDEFINITIONS_H
#define IMAGERECONSTRUCTIONENGINEDEFINITIONS_H

enum SideEnum{TOP, BOTTOM};

enum FocusMethodEnum{SHARPEST, INFLECTIONPOINT, HALFMAXIMUM};

//Target values for the magnification in pixels per inch (PPI)
enum MagnificationEnum{1575, 2560};

//template <class T>
//struct Range {
//    lowestValue;
//    highestValue;};

typedef RadialOffset Point2D<int>;

#endif//IMAGERECONSTRUCTIONENGINEDEFINITIONS_H
