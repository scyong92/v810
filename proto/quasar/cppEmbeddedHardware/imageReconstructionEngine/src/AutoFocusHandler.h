#ifndef EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_AutoFocusHandler_H
#define EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_AutoFocusHandler_H

/**
* This class creates images at appropriate slice height by performing an auto focus
* search.
* @author Horst Mueller
*/

class AutoFocusHandler
{

public:
  static AutoFocusHandler* getInstance();
  static void removeInstance();
  void createFocusedImages(ReconstructionRegion const& reconstructionRegion);

private:
  AutoFocusHandler(AutoFocusHandler const& autoFocusHandler);
  AutoFocusHandler& operator=(AutoFocusHandler const& rhs);
  AutoFocusHandler();
  ~AutoFocusHandler();
  void makeFocusProfile(FocusRegion p0, double p1, double p2);
  float findSharpest();
  void findInflectionPoints(float& lowerInflectionPoint, float& upperInflectionPoint);
  void findHalfMaxPoints(float & lowerHalfMaxPoints, float & upperHalMaxPoint);
  static AutoFocusHandler* _pInstance;
};
#endif //EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_AutoFocusHandler_H