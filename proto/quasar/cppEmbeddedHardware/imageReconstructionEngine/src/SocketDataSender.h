#ifndef EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_SOCKETDATASENDER_H
#define EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_SOCKETDATASENDER_H

/**
* This class sends image data via a socket connection. The class is implemented 
* as a function object which is used as the argument when creating a seperate thread.
* @author Horst Mueller
*/
class SocketDataSender
{
public:
  SocketDataSender();
  ~SocketDataSender();
  void send(Image const& image);
  void send(FocusResult const& focusResult);
  void send(AlignementResult const& alignementResult);

private:
  SocketDataSender(SocketDataSender const& SocketDataSender);
  SocketDataSender& operator=(SocketDataSender const& rhs);
};
#endif //EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_SOCKETDATASENDER_H