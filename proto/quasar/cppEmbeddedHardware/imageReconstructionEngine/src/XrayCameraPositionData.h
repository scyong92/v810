#ifndef EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_XRAYCAMERAPOSITIONDATA_H
#define EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_XRAYCAMERAPOSITIONDATA_H

/**
* This class holds the location of an individual XrayCamera
* relative to the x-ray spot.
* @author Horst Mueller
*/
class XrayCameraPositionData
{
public:
  XrayCameraPositionData(int cameraId,
                         int xPositionInNanoMeters,
                         int yPositionInNanoMeters);
  XrayCameraPositionData(XrayCameraPositionData const& xRayCameraPositionData);
  ~XrayCameraPositionData();
  XrayCameraPositionData& operator=(XrayCameraPositionData const& rhs);
  int getXPositionInNanoMeters() const;
  void setXPositionInNanoMeters(int xPositionInNanoMeters);
  int getYPositionInNanoMeters() const;
  void setYPositionInNanoMeters(int yPositionInNanoMeters);
  int getCameraId() const;
  void setCameraId(int cameraId);

private:
  //The unique ID of the camera
  int _cameraId;
  // The x location relative to the x-ray spot.
  int _XPositionInNanoMeters;
  // The y location relative to the x-ray spot.
  int _YPositionInNanoMeters; // don't need this if we trigger cameras individually
};

#endif //EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_XRAYCAMERAPOSITIONDATA_H