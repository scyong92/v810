#ifndef EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_ALIGNMENTREGION_H
#define EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_ALIGNMENTREGION_H

/**
* This class describes regions that are used to align a panel
* @author Horst Mueller
*/
class AlignmentRegion{
public:
  AlignmentRegion(std::list<int> const& cameraIds,         
                std::list<string> const& templateImages, 
                AlignementData const& alignmentData,
                int zInNanoMeters,                  
                int scanPassNumber,                 
                Rectangle const& regionInNanoMeters);
  AlignmentRegion(AlignmentRegion const& alignmentRegion);
  ~AlignmentRegion();
  AlignmentRegion& operator=(AlignmentRegion const& rhs);

  void setCameraIds(std::list<int> const& cameraIds);
  std::list<int>& getCameraIds() const;
  void setAlignementData(AlignmentData const& alignmentData);
  AlignmentData& getAlignementData() const;
  void setScanPassNumber(int scanPassNumber);
  int getScanPassNumber() const;
  void setTemplateImages(std::list<string> const& templateImages);
  std::list<string>& getTemplateImages();

private:
    //The IDs of the cameras and corresponding projections on which to align
    std::list<int> _cameraIds;
    //A list of the template images that were captured during test development
    std::list<string> _templateImages;
    //Alignement data  containing the expected and the actual position
    AlignementData _alignmentData;
    //Z height of the alignement region extracted during test development
    int _zInNanoMeters;
    //Scan pass number during which the projection data was captured
    int _scanPassNumber;
    //The actual region on the panel in nano meters
    Rectangle _regionInNanoMeters;
};

#endif //EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_ALIGNMENTREGION_H
