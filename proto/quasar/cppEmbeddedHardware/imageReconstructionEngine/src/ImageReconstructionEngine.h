#ifndef EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_IMAGERECONSTRUCTIONENGINE_H
#define EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_IMAGERECONSTRUCTIONENGINE_H

#include <list>

/**
* This class provides acces to all the image reconstruction functionality 
* on the embedded image reconstruction processors
* @author Horst Mueller
*/
class ImageReconstructionEngine
{
public:
   static ImageReconstructionEngine* getInstance();
   static removeInstance(ImageReconstructionEngine*& pImageReconstructionEngine);

   void setHardwareParameters(int cameraWidthInPixels,
                              int numberOfCameras,
                              int zDistanceFromCameraToXraySourceInNanoMeters,
                              std::vector<int> const& zReferenceHeightPerMagnificationInNanoMeters);
   int getId() const;
   std::list<int> getIpAdresses() const;
   std::string getHardwareVersion() const;
   std::string getSoftwareVersion() const;
   void setXrayCameraPositionData(std::vector<XrayCameraPositionData*>& xRayCameraPositionData);
   void setSystemFiducialData(MagnificationEnum const& magnificationEnum,
                              SystemFiducialData& systemFiducialData);
   void setMagnificationCalibrationData(MagnificationEnum const& magnificationEnum,
                                        MagnificationCalibrationData& magnificationCalibrationData);
   void setScanPassXlocationInNanoMeters(std::vector<int> const& scanPassXlocationInNanoMeters);
   SystemFiducialData initializeReconstruction(MagnificationEnum const& magnificationEnum,
                                               std::list<AlignmentRegion*> const& alignmentRegions,
                                               Rectangle const& searchRegionForSystemFiducial,
                                               int panelLengthInNanoMeters,
                                               int panelThicknessInNanoMeters, 
                                               int scanPassWidthInNanoMeters);
   void initializeReconstruction(MagnificationEnum const& magnificationEnum,
                                 std::list<AlignmentRegion*> const& alignmentRegions,
                                 int panelLengthInNanoMeters,
                                 int panelThicknessInNanoMeters, 
                                 int scanPassWidthInNanoMeters);
   void initializeReconstruction(MagnificationEnum magnificationEnum, int scanPassWidthInNanoMeters);
   SystemFiducialData locateSystemFiducial(Rectangle const& searchRegionForSystemFiducial);
   void magnificationCalibration(MagnificationEnum magnification,
                                 Rectangle const& searchRegions,
                                 int expectedWidthInPixels, 
                                 int expectedLengthInPixels,
                                 dint lengthToleranceInPixels, 
                                 int heightToleranceInPixels);
   std::vector<XrayCameraPositionData> xRayCameraXPositionCalibration(std::vector<XrayCameraPositionData*> const& expectedXrayCameraPositionData, 
                                                                    int xToleranceInNanoMeters);
   std::vector<XrayCameraPositionData> xRayCameraYPositionCalibration(std::vector<XrayCameraPositionData*> const& expectedXrayCameraPositionData, 
                                                                    int yToleranceInNanoMeters);
   std::vector<int> cameraTriggerCalibration();
   void freeMemory(int panelsXInNanoMeters);
   bool areAllProjectionsReceived(int scanPassNumber) const;
   int getNumBytesFreeForProjectionImages() const;
   void createImages(ReconstructionRegion const& reconstructionRegion);
   std::list<RadialOffset> createImages(ReconstructionRegion const& reconstructionRegion);
   void createImages(ReconstructionRegion const& reconstructionRegion, std::list<RadialOffset*> const& radialOffsets);
   void createImage(ReconstructionRegion const& reconstructionRegion, int zHeightFromReferencePlaneInNanoMeters);
   void updateAlignmentResult(AlignmentResult& alignmentResult);
   ProjectionRegionData getProjectionImages(int scanPassNumber, int cameraId);
   void setRawImageData(std::list<ProjectionRegionData*> const& projectionRegionData);

private:
  ImageReconstructionEngine();
  ~ImageReconstructionEngine();
  ImageReconstructionEngine(ImageReconstructionEngine const& imageReconstructionEngine);
  ImageReconstructionEngine& operator=(ImageReconstructionEngine const& rhs);

  static ImageReconstructionEngine* _pInstance;
  boost::thread _dataReceiverThread;
  boost::thread _dataSenderThread;
  boost::thread _alignmentHandlerThread;
};    

#endif //EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_IMAGERECONSTRUCTIONENGINE_H
