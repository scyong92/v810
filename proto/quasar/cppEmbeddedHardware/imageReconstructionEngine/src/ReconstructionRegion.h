#ifndef EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_RECONSTRUCTIONREGION_H
#define EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_RECONSTRUCTIONREGION_H

/**
 * This class contains all the data needed to describe a region to be reconstructed.
 *
 * @author Horst Mueller
 */
class ReconstructionRegion
{
public:
  ReconstructionRegion(int reconstructionRegionId,
                       std::list<FocusInstructions> focusInstructions,
                       SideEnum topOrBottom,
                       int xInNanoMeters,
                       int yInNanoMeters,
                       int widthInNanoMeters,
                       int lengthInNanoMeters,
                       std::list<Rectangle> focusRegionForEachSlice);
  ReconstructionRegion(ReconstructionRegion const& theReconstructionRegion);
  ~ReconstructionRegion();
  ReconstructionRegion& operator=(ReconstructionRegion const& rhs);

  int getReconstructionRegionId() const;
  inline void setReconstructionRegionId(reconstructionRegionId);
  SideEnum getSide() const;
  void setSide(SideEnum side);
  int getXInNanoMeters() const;
  void setXInNanoMeters(int xInNanoMeters);
  int getYInNanoMeters() const;
  void setYInNanoMeters(int yInNanoMeters);
  int getWidthInNanoMeters() const;
  void setWidthInNanoMeters(int widthInNanoMeters);
  int getLengthInNanoMeters() const;
  void setLengthInNanoMeters(int lengthInNanoMeters);
  list<Rectangle>& getfocusRegionForEachSlice() const;
  void setfocusRegionForEachSlice(std::list<Rectangle> const& focusRegionForEachSlice);

private:
  int _reconstructionRegionId;
  std::list<FocusInstructions> _focusInstructions;
  //Information used to define the region
  //specifies if the image should be constructed from the top or bottom of the panel
  SideEnum _topOrBottom;
  //Specifies the bottom left corner of the area to be constructed
  //relative to the panels bottom left corner
  int _xInNanoMeters;
  //Specifies the bottom left corner of the area to be constructed
  //relative to the panels bottom left corner
  int _yInNanoMeters;
  // Specifies the width of the area to be contructed
  //relative to the panels bottom left corner
  int _widthInNanoMeters;
  //Specifies the length of the area to be constructed
  //relative to the panels bottom left corner
  int _lengthInNanoMeters;
  //A focus region for each slice
  std::list<Rectangle> _focusRegionForEachSlice; 
};

#endif //EMBEDDEDHARDWARE_IMAGERECONSTRUCTION_RECONSTRUCTIONREGION_H
