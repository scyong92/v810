package proto.quasar.imageReconstruction;

import java.rmi.*;
import java.util.*;

class ImageObservable extends Observable
{
  private List _imageList;

  /*
  * @author Horst Mueller
  */
  public ImageObservable()
  {
    _imageList = new Collections.synchronizedList(new LinkedList());
    super(this);
  }

  /*
  * @author Horst Mueller
  */
  void setImage(Image p0)
  {
    _imageList.append();

    setChanged();

    notifyObservers();
  }

  /*
  * @author Horst Mueller
  */
  Image getImage()
  {
    return _imageList.removeFirst();
  }
}
