//package com.agilent.mtd.agt5dx.imageReconstruction;
package proto.quasar.imageReconstruction;

import java.util.Observer;

/**
* This class handles all the data which is created by a single embedded image 
* reconstruction engine but needs to be made available to all.
* As an observer it gets updated by a single embedded image reconstruction engine
* as a mediator it distributes the information to all.
* @author Horst Mueller
*/
class ReconstructionDataHandler implements Observer 
{
  private List participants = new ArrayList();

  /**
  * @author Horst Mueller
  */
  ReconstructionDataHandler()
  {
  }

  /**
  * This method is used to register an image reconstruction engine
  * @author Horst Mueller
  */
  void register(ImageReconstructionEngine imageReconstructionEngine)
  {
    participants.append(imageReconstructionEngine);
  }

}
