//package com.agilent.mtd.agt5dx.imageReconstruction;
package proto.quasar.imageReconstruction;

/**
* This class contains the data of a projection region from a single camera
* @author Horst Mueller
*/
class ProjectionRegionData
{
  private int _heightInPixels;
  //Number of scan pass
  private int _scanPassNumber;
  //Camera ID
  private int _cameraId;
  //An ArrayList of bytes that contain the actual image data
  private ArrayList _imageData;
  //If the camera width is assumed to be very wide this is the
  //x coordinate iat which the system fiducial would have been imaged
  private int _systemFiducialXCoordianteInPixels;
  //Number of pixels from the top of the entire projection that leads to the first
  //row of the data
  private int _lineNumberInPixels;
  //Number of pixels in the x direction
  private int _widthInPixels;
  //Number of pixels in the y direction

  /**
  * This class contains the data of a projection region from a single camera
  * @author Horst Mueller
  */
  ProjectionRegionData(int scanPassNumber,
                       int cameraId,
                       int systemFiducialXCoordianteInPixels,
                       int lineNumberInPixels,
                       int widthInPixels,
                       int lengthInPixels,
                       ArrayList imageData)
  {
    Asset.expect(lineNumberInPixels >= 0);
    _lineNumberInPixels = lineNumberInPixels;

    Asset.expect(widthInPixels >= 0);
    _widthInPixels= widthInPixels ;

    Asset.expect(heightInPixels >= 0);
    _heightInPixels= heightInPixels ;

    _systemFiducialXCoordianteInPixels = systemFiducialXCoordianteInPixels;

    Asset.expect(scanPassNumber >= 0);
    _scanPassNumber = scanPassNumber;

    Asset.expect(cameraId >= 0);
    _cameraId = cameraId;

    Asset.expect(imageData != null);
    _imageData = imageData;
  }

  /**
  * @author Horst Mueller
  */
  int getLineNumberInPixels()
  { 
    return _lineNumberInPixels; 
  }

  /**
  * @author Horst Mueller
  */
  int getWidthInPixels()
  { 
    return _widthInPixels; 
  }

  /**
  * @author Horst Mueller
  */
  int getHeightInPixels()
  { 
    return heightInPixels; 
  }

  /**
  * @author Horst Mueller
  */
  int getSystemFiducialXCoordianteInPixels()
  { 
    return _systemFiducialXCoordiantesInPixel; 
  }

  /**
  * @author Horst Mueller
  */
  int getCameraId()
  { 
    return _cameraId; 
  }

  /**
  * @author Horst Mueller
  */
  int getScanPassNumber()
  { 
    return _scanPassNumber; 
  }

  /**
  * @author Horst Mueller
  */
  ArrayList getImage()
  { 
    return  _imageData;
  }

}
