//package com.agilent.mtd.agt5dx.imageReconstruction;
package proto.quasar.imageReconstruction;

/**
* This class holds the focus instructions that are used to by the auto focus algorithm
* to identify the correct slice and slice height.
* @author Horst Mueller                            
*/
class FocusInstruction
{
  //The method that will be used to identify the "correct slice" 
  private FocusSearchMethodEnum _focusSearchMethod;
  //Used as a starting point for the focus search 
  private int _zHeightEstimateInNanoMeters;
  //Search this far above and below the z height guess
  private int _searchRangeInNanoMeters;
  //Acceptable range  for the z height to be returned.
  //(Used to detect an error in auto focus)          
  private int _zHeightAcceptableRangeInNanoMeters;
  //Acceptable range for the sharpness value.
  //(Used to detect an error in auto focus.) 
  private int _sharpnessAcceptableRangeInNanoMeters;
  //We plan to vary the focus metric based on region size                
  //and joint type. Focus searches based on lower resolution             
  //images seem to be more robust to noise AND faster. This              
  //element describes exactly how the focus metric should be calculated. 
  private SharpnessMetricEnum _sharpnessMetric;

  /**
  * @author Horst Mueller                            
  */
  FocusInstruction(FocusSearchMethodEnum focusSearchMethod, 
                   int zHeightEstimateInNanoMeters, 
                   int searchRangeInNanoMeters, 
                   int zHeightAcceptableRangeInNanoMeters,
                   int sharpnessAcceptableRangeInNanoMeters, 
                   int sharpnessMetric)
  {
    Assert.expect(focusSearchMethod != null);
    _focusSearchMethod = focusSearchMethod;

    _zHeightEstimateInNanoMeters = zHeightEstimateInNanoMeters;

    Assert.expect(searchRangeInNanoMeters > 0);
    _searchRangeInNanoMeters = searchRangeInNanoMeters;

    Assert.expect(zHeightAcceptableRangeInNanoMeters > 0);
    _zHeightAcceptableRangeInNanoMeters = zHeightAcceptableRangeInNanoMeters;

    Assert.expect(sharpnessAcceptableRangeInNanoMeters > 0);
    _sharpnessAcceptableRangeInNanoMeters = sharpnessAcceptableRangeInNanoMeters;

    Assert.expect(sharpnessMetric != null);
    _sharpnessMetric = sharpnessMetric;
  }

  /**
  * @author Horst Mueller                            
  */
  FocusSearchMethodEnum getFocusSearchMethod()
  {
    return _focusSearchMethod;
  }

  /**
  * @author Horst Mueller                            
  */
  void setFocusSearchMethod(int focusSearchMethod)
  {
    Assert.expect(focusSearchMethod != null);
    _focusSearchMethod = focusSearchMethod;
  }

  /**
  * @author Horst Mueller                            
  */
  int getzHeightEstimateInNanoMeters()
  {
    return _zHeightEstimateInNanoMeters;
  }

  /**
  * @author Horst Mueller                            
  */
  void setzHeightEstimateInNanoMeters(int zHeightEstimateInNanoMeters)
  {
    _zHeightEstimateInNanoMeters = zHeightEstimateInNanoMeters;
  }

  /**
  * @author Horst Mueller                            
  */
  int getSearchRangeInNanoMeters()
  {
    return _searchRangeInNanoMeters;
  }

  /**
  * @author Horst Mueller                            
  */
  void setSearchRangeInNanoMeters(int searchRangeInNanoMeters)
  {
    Assert.expect(searchRangeInNanoMeters > 0);
    _searchRangeInNanoMeters = searchRangeInNanoMeters;
  }

  /**
  * @author Horst Mueller                            
  */
  int getzHeightAcceptableRangeInNanoMeters()
  {
    return _zHeightAcceptableRangeInNanoMeters;
  }

  /**
  * @author Horst Mueller                            
  */
  void setzHeightAcceptableRangeInNanoMeters(int zHeightAcceptableRangeInNanoMeters)
  {
    Assert.expect(zHeightAcceptableRangeInNanoMeters > 0);
    _zHeightAcceptableRangeInNanoMeters = zHeightAcceptableRangeInNanoMeters;
  }

  /**
  * @author Horst Mueller                            
  */
  int getSharpnessAcceptableRangeInNanoMeters()
  {
    return _sharpnessAcceptableRangeInNanoMeters;
  }

  /**
  * @author Horst Mueller                            
  */
  void setSharpnessAcceptableRangeInNanoMeters(int sharpnessAcceptableRangeInNanoMeters)
  {
    Assert.expect(sharpnessAcceptableRangeInNanoMeters > 0);
    _sharpnessAcceptableRangeInNanoMeters = sharpnessAcceptableRangeInNanoMeters;
  }

  /**
  * @author Horst Mueller                            
  */
  int getSharpnessMetric()
  {
    return _sharpnessMetric;
  }

  /**
  * @author Horst Mueller                            
  */
  void setSharpnessMetric(SharpnessMetricEnum sharpnessMetric)
  {
    Assert.expect(sharpnessMetric != null);
    _sharpnessMetric = sharpnessMetric;
  }

}
