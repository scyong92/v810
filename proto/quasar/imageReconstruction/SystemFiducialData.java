//package com.agilent.mtd.agt5dx.imageReconstruction;
package proto.quasar.imageReconstruction;

/**
* This class holds the system fiducial data which is created by the embedded
* image reconstruction engine at the beginning of every scan path
* @author Horst Mueller
*/
class SystemFiducialData
{
  //Id of the reference camera
  private int _referenceCameraId;
  //X coordinate of the system fiducial in each projection
  private List _systemFiducialXInPixels;
  //X coordinate of the stage while the fiducial was imaged
  private int _stageXLocationInNanometers;

  SystemFiducialData(int referenceCameraId, 
                     List systemFiducialXInPixels, 
                     int stageXLocationInNanometers)
  {
    Assert.expect(referenceCameraId >= 0);
    _referenceCameraId = referenceCameraId;

    Assert.expect(systemFiducialXInPixels >= 0);
    _systemFiducialXInPixels = systemFiducialXInPixels;

    _stageXLocationInNanometers = stageXLocationInNanometers;
  }

  /**
  * @author Horst Mueller
  */
  int getReferenceCameraId()
  {
    return _referenceCameraId;
  }

  /**
  * @author Horst Mueller
  */
  void setReferenceCameraId(int referenceCameraId)
  {
    Assert.expect(referenceCameraId >= 0);
    _referenceCameraId = referenceCameraId;
  }

  /**
  * @author Horst Mueller
  */
  int getSystemFiducialXInPixels()
  {
    return _systemFiducialXInPixels;
  }

  /**
  * @author Horst Mueller
  */
  void setSystemFiducialXInPixels(int systemFiducialXInPixels)
  {
    Assert.expect(systemFiducialXInPixels >= 0);
    _systemFiducialXInPixels = systemFiducialXInPixels;
  }

  /**
  * @author Horst Mueller
  */
  int getStageXLocationInNanometers()
  {
    return _stageXLocationInNanometers;
  }

  /**
  * @author Horst Mueller
  */
  void setStageXLocationInNanometers(int stageXLocationInNanometers)
  {
    _stageXLocationInNanometers = stageXLocationInNanometers;
  }
}
