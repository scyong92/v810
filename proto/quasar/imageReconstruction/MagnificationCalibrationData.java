//package com.agilent.mtd.agt5dx.imageReconstruction;
package proto.quasar.imageReconstruction;

/**
* This class holds the magnification calibration data
* The system currently supports two target magnifications
* with 1575 and 2560 pixels per inch.
* @author Horst Mueller
*/
class MagnificationCalibrationData
{
  //The target magnification
  private MagnificationEnum _magnificationEnum;
  //Actual magnification in x direction as determined by calibration
  private float _xMagnification;
  //Actual magnification in y direction as determined by calibration
  private float _yMagnification;
  private float _xPixelPitchInNanometers;
  private float _yPixelPitchInNanometers;

  /**
  * @author Horst Mueller
  */
  MagnificationCalibrationData(MagnificationEnum magnificationEnum, 
                                      float xMagnification, 
                                      float yMagnification, 
                                      float xPixelPitchInNanometers,
                                      float yPixelPitchInNanometers)
  {
     Assert.expect(magnificationEnum != null)
     magnificationEnum = magnificationEnum;

     Assert.expect(xMagnification > 0);
     _xMagnification = xMagnification;
     Assert.expect(xMagnification > 0)
     _yMagnification = yMagnification;

     Assert.expect(xPixelPitchInNanometers > 0)
     _xPixelPitchInNanometers = xPixelPitchInNanometers;
     Assert.expect(yPixelPitchInNanometers > 0)
     _yPixelPitchInNanometers = yPixelPitchInNanometers;
  }

  /**
  * @author Horst Mueller
  */
  MagnificationEnum getMagnification()
  {
    return magnificationEnum;
  }

  /**
  * @author Horst Mueller
  */
  void setMagnification(MagnificationEnum magnificationEnum)
  {
    _magnificationEnum = magnificationEnum;
  }

  /**
  * @author Horst Mueller
  */
  float getXMagnification()
  {
    return _xMagnification;
  }

  /**
  * @author Horst Mueller
  */
  void setXMagnification(float xMagnification)
  {
    Assert.expect(xMagnification > 0)
    _xMagnification = xMagnification;
  }

  /**
  * @author Horst Mueller
  */
  float getYMagnification()
  {
    return _yMagnification;
  }

  /**
  * @author Horst Mueller
  */
  void setYMagnification(float yMagnification)
  {
    Assert.expect(yMagnification > 0)
    _yMagnification = yMagnification;
  }

  /**
  * @author Horst Mueller
  */
  float getXPixelPitchInNanometers()
  {
    return _xPixelPitchInNanometers;
  }

  /**
  * @author Horst Mueller
  */
  void setXPixelPitchInNanometers(float xPixelPitchInNanometers)
  {
    Assert.expect(xPixelPitchInNanometers > 0)
    _xPixelPitchInNanometers = xPixelPitchInNanometers;
  }

  /**
  * @author Horst Mueller
  */
  float getYPixelPitchInNanometers()
  {
    return _yPixelPitchInNanometers;
  }

  /**
  * @author Horst Mueller
  */
  void setYPixelPitchInNanometers(float yPixelPitchInNanometers)
  {
    Assert.expect(yPixelPitchInNanometers > 0)
    _yPixelPitchInNanometers = yPixelPitchInNanometers;
  }
}
