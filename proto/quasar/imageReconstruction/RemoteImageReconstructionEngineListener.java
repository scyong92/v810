package proto.quasar.imageReconstruction;

import java.rmi.*;
import java.util.*;

/*
* This class sole purpose is to receive a signal from an
* embedded image reconstruction engine indicating that it is
* up and running.
* Horst Mueller
*/
class RemoteImageReconstructionEngineListener extends Observable implements RemoteImageReconstructionEngineListenerInt
{
  private static RemoteImageReconstructionEngineListener _instance;

  public static RemoteImageReconstructionEngineListener getInstance()
  {
    if( _instance == null )
    {
      _instance = new RemoteImageReconstructionEngineListener();
    }
    return _instance;
  }

  void setEmbeddedImageReconstructionEngine(int iPAdress) throws RemoteException
  {
      setChanged();

      notifyObservers(iPAdress);
  }
}
