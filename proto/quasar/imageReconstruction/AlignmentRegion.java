//package com.agilent.mtd.agt5dx.imageReconstruction;
package proto.quasar.imageReconstruction;

/**
* This class describes the regions that are used to align a panel
* @author Horst Mueller
*/
class AlignmentRegion
{
  //List of integers that describe the cameras (projections) that are used to align on
  private List _cameraIds;
  //List of Strings that contain the names of the templates images that were captured 
  //during test development and that are used to align on.
  private List _templateImages;
  //The alignment data
  private AlignmentData _alignmentData;
  //Z height of the alignement region extracted during test development
  private int _zInNanoMeters;
  //Scan pass number during which the projection data is captured
  private int _scanPassNumber;
  //The actual region on the panel in nano meters
  private Rectangle _regionInNanoMeters;

  /**
  * @author Horst Mueller
  */
  AlignmentRegion(List cameraIds, 
                  List templateImages, 
                  int alignmentData, 
                  int zInNanoMeters, 
                  int scanPassNumber,
                  Rectangle regionInNanoMeters)
  {
    Assert.expect(cameraIds != null);
    _cameraIds = new ArrayList(cameraIds);

    Assert.expect(templateImages != null);
    _templateImages = new ArrayList(templateImages);

    Assert.expect(alignmentData != null);
    _alignmentData = alignmentData;

    _zInNanoMeters = zInNanoMeters;

    Assert.expect(scanPassNumber >= 0);
    _scanPassNumber = scanPassNumber;

    Assert.expect(regionInNanoMeters != Null);
    _regionInNanoMeters = regionInNanoMeters;
  }

  /**
  * @author Horst Mueller
  */
  AlignmentRegion() 
  { 
  }

  /**
  * @author Horst Mueller
  */
  List getCameraIds()
  {
    return _cameraIds;
  }

  /**
  * @author Horst Mueller
  */
  void setCameraIds(List cameraIds)
  {
    Assert.expect(cameraIds != null);
    _cameraIds = cameraIds;
  }

  /**
  * @author Horst Mueller
  */
  List getTemplateImages()
  {
    return _templateImages;
  }

  /**
  * @author Horst Mueller
  */
  void setTemplateImages(List templateImages)
  {
    Assert.expect(templateImages != null);
    _templateImages = templateImages;
  }

  /**
  * @author Horst Mueller
  */
  AlignementData getAlignmentData()
  {
    return _alignmentData;
  }

  /**
  * @author Horst Mueller
  */
  void setAlignmentData(AlignementData alignmentData)
  {
    Assert.expect(alignmentData!= null);
    _alignmentData = alignmentData;
  }

  /**
  * @author Horst Mueller
  */
  int getZInNanoMeters()
  {
    return _zInNanoMeters;
  }

  /**
  * @author Horst Mueller
  */
  void setZInNanoMeters(int zInNanoMeters)
  {
    _zInNanoMeters = zInNanoMeters;
  }

  /**
  * @author Horst Mueller
  */
  int getScanPassNumber()
  {
    return _scanPassNumber;
  }

  /**
  * @author Horst Mueller
  */
  void setScanPassNumber(int scanPassNumber)
  {
    Assert.expect(scanPassNumber >= 0);
    _scanPassNumber = scanPassNumber;
  }

  /**
  * @author Horst Mueller
  */
  Rectangle getRegionInNanoMeters()
  {
    return _regionInNanoMeters;
  }

  /**
  * @author Horst Mueller
  */
  void setRegionInNanoMeters(int regionInNanoMeters)
  {
    Assert.expect(regionInNanoMeters != null);
    _regionInNanoMeters = regionInNanoMeters;
  }
}