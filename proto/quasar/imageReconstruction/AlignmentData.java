//package com.agilent.mtd.agt5dx.imageReconstruction;
package proto.quasar.imageReconstruction;

/**
* This class holds the data that is created for a single alignment region. The data 
* consists of the coordinates where the feature is expected to be located and
* the actual coordinates that result from the alignment process
* @author Horst Mueller
*/
class AlignmentData
{
  private int _expectedXCoordinateInNanoMeters;
  private int _expectedYCoordinateInNanoMeter;
  private int _xCoordinateInNanoMeters;
  private int _yCoordinateInNanoMeters;

  /**
  * @author Horst Mueller
  */
  AlignmentData(int expectedXCoordinateInNanoMeters, 
                int expectedYCoordinateInNanoMeter,
                int xCoordinateInNanoMeters,
                int yCoordinateInNanoMeters)
  {
    _expectedXCoordinateInNanoMeters = expectedXCoordinateInNanoMeters;
    _expectedYCoordinateInNanoMeter = expectedYCoordinateInNanoMeter;
    _xCoordinateInNanoMeters = xCoordinateInNanoMeters;
    _yCoordinateInNanoMeters = yCoordinateInNanoMeters;
  }

  /**
  * @author Horst Mueller
  */
  int getExpectedXCoordinateInNanoMeters()
  {
    return _expectedXCoordinateInNanoMeters;
  }

  /**
  * @author Horst Mueller
  */
  void setExpectedXCoordinateInNanoMeters(int expectedXCoordinateInNanoMeters)
  {
    _expectedXCoordinateInNanoMeters = expectedXCoordinateInNanoMeters;
  }

  /**
  * @author Horst Mueller
  */
  int getExpectedYCoordinateInNanoMeter()
  {
    return _expectedYCoordinateInNanoMeter;
  }

  /**
  * @author Horst Mueller
  */
  void setExpectedYCoordinateInNanoMeter(int expectedYCoordinateInNanoMeter)
  {
    _expectedYCoordinateInNanoMeter = expectedYCoordinateInNanoMeter;
  }

  /**
  * @author Horst Mueller
  */
  int getXCoordinateInNanoMeters()
  {
    return _xCoordinateInNanoMeters;
  }

  /**
  * @author Horst Mueller
  */
  void setXCoordinateInNanoMeters(int xCoordinateInNanoMeters)
  {
    _xCoordinateInNanoMeters = xCoordinateInNanoMeters;
  }

  /**
  * @author Horst Mueller
  */
  int getYCoordinateInNanoMeters()
  {
    return _yCoordinateInNanoMeters;
  }

  /**
  * @author Horst Mueller
  */
  void setYCoordinateInNanoMeters(int yCoordinateInNanoMeters)
  {
    _yCoordinateInNanoMeters = yCoordinateInNanoMeters;
  }
}
