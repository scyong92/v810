//package com.agilent.mtd.agt5dx.imageReconstruction;
package proto.quasar.imageReconstruction;

/**
* This class contains the results from an auto-focus search on a reconstruction region
* including time and date of reconstruction, a unique ID that corresponds to the region that was reconstructed,
* the slice that was reconstructed, the focus search method, the metric that was used to measure sharpness,
* the z height that was the result of this focus search, and the sharpness value.
* @author Horst Mueller
*/
class FocusResult
{
  //Time and date of reconstruction                                    
  private String _timeAndDate;
  //A unique ID that corresponds to the region that was reconstructed. 
  private int _reconstructionRegionId;
  //The slice that was reconstructed.                                  
  private int _sliceNumber;
  //Sharpest, inflection point, percent max,                           
  private FocusSearchMethodEnum _focusSearchMethod;
  //The metric that was used to measure sharpness.                     
  private SharpnessMetricEnum _sharpnessMetric;
  //The z HeightInNanoMeters that was the result of this focus search              
  private int _zHeightInNanoMeters;
  //The sharpness of the slice at the ZHeightInNanoMeters that is listed above.   
  private double _sharpness;

  /**
  *@author Horst Mueller
  */
  FocusResult(String timeAndDate, 
              int reconstructionRegionId, 
              int sliceNumber, 
              FocusSearchEnum focusSearchMethod,
              int sharpnessMetric, 
              int zHeightInNanoMeters, 
              double sharpness)
  { 
    Assert.expect(timeAndDate != null);
    _timeAndDate = timeAndDate;

    Assert.expect(reconstructionRegionId >= 0);
    _reconstructionRegionId = reconstructionRegionId;

    Assert.expect(sliceNumber >= 0);
    _sliceNumber = sliceNumber;

    Assert.expect(focusSearchMethod != null);
    _focusSearchMethod = focusSearchMethod;

    Assert.expect(sharpnessMetric != null);
    _sharpnessMetric = sharpnessMetric;

    _zHeightInNanoMeters = zHeightInNanoMeters;
    _sharpness = sharpness;
  }

  /**
  *@author Horst Mueller
  */
  String getTimeAndDate()
  {
    return _timeAndDate;
  }

  /**
  *@author Horst Mueller
  */
  void setTimeAndDate(String timeAndDate)
  {
    Assert.expect(timeAndDate != null);
    _timeAndDate = timeAndDate;
  }

  /**
  *@author Horst Mueller
  */
  int getReconstructionRegionId()
  {
    return _reconstructionRegionId;
  }

  /**
  *@author Horst Mueller
  */
  void setReconstructionRegionId(int reconstructionRegionId)
  {
    Assert.expect(reconstructionRegionId >= 0);
    _reconstructionRegionId = reconstructionRegionId;
  }

  /**
  *@author Horst Mueller
  */
  int getSliceNumber()
  {
    return _sliceNumber;
  }

  /**
  *@author Horst Mueller
  */
  void setSliceNumber(int sliceNumber)
  {
    Assert.expect(sliceNumber >= 0);
    _sliceNumber = sliceNumber;
  }

  /**
  *@author Horst Mueller
  */
  FocusSearchMethodEnum getFocusSearchMethod()
  {
    return _focusSearchMethod;
  }

  /**
  *@author Horst Mueller
  */
  void setFocusSearchMethod(FocusSearchMethodEnum focusSearchMethod)
  {
    Assert.expect(focusSearchMethod != null);
    _focusSearchMethod = focusSearchMethod;
  }

  /**
  *@author Horst Mueller
  */
  SharpnessMetricEnum getSharpnessMetric()
  {
    return _sharpnessMetric;
  }

  /**
  *@author Horst Mueller
  */
  void setSharpnessMetric(SharpnessMetricEnum sharpnessMetric)
  {
    Assert.expect(sharpnessMetric != null);
    _sharpnessMetric = sharpnessMetric;
  }

  /**
  *@author Horst Mueller
  */
  int getZHeightInNanoMeters()
  {
    return _zHeightInNanoMeters;
  }

  /**
  *@author Horst Mueller
  */
  void setZHeightInNanoMeters(int zHeightInNanoMeters)
  {
    _zHeightInNanoMeters = zHeightInNanoMeters;
  }

  /**
  *@author Horst Mueller
  */
  double getSharpness()
  {
    return _sharpness;
  }

  /**
  *@author Horst Mueller
  */
  void setSharpness(double sharpness)
  {
    _sharpness = sharpness;
  }
}
