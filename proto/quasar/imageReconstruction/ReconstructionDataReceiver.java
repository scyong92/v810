//package com.agilent.mtd.agt5dx.imageReconstruction;
package proto.quasar.imageReconstruction;

import java.util.*;


/**
* This class establishes a socket connection to the image reconstruction hardware.
* The socket client receives images, focus results and alignement data on a seperate 
* thread. 
*
*/
public class ReconstructionDataReceiver extends Thread
{
  private static final String _THREADNAME0 ="ImageDataReciever";
  private String _threadName ="";
  int _port;
  private Thread _socketClientThread = null;
  private ServerSocket _serverSocket = null;
  private Socket _clientSocket = null;
  ImageObservable _imageObservable = 0;

  /** @todo dont throw Exception */
  /**
  * @param imageReconstructionEngine is a reference to the image reconstruction engine
  * object which uses the socket connection
  * @param is the number of the port
  * @author Horst Mueller
  */
  public ReconstructionDataReceiver(ImageReconstructionEngine imageReconstructionEngine,
                                    int port)
  {
    _imageReconstructionEngine = imageReconstructionEngine;
    _port = port;
    _threadName + _THREADNAME0 + _imageReconstructionEngine.getId());
    super(_threadName);
  }

  /**
  * @author Horst Mueller
  */
  public void start() throw HardwareException
  {
      if (_socketClientThread == null) 
      {
        try
        {
          _socketClientThread = new Thread(this, _threadName);
          _socketClientThread.start();
        }
        catch(IOException e)
        {
          throw new ImageReconstructionServerException();
        }
      }
  }

  /**
  * @author Horst Mueller
  */
  public void stop() 
  { 
    _socketClientThread = null;
  }

  /**
  * This method uses the raw bytes received via the socket and poulates the image class
  * @param imageData is an array of bytes which contains the image
  * @author Horst Mueller
  */
  private Image populateImageClass(byte[] imageData)
  { 
  }

  /**
  * This method passes the image to the imageObservable object that is associated
  * with the image reconstruction engine
  * @author Horst Mueller
  */
  private void makeImageAvailable(Image image)
  { 
  }

  /**
  * This method overrides Thread's run method
  * @author Horst Mueller
  */
  public void run()
  {
    //Open a socket
    try 
    {
        _serverSocket = new ServerSocket(_port);
    } catch (IOException e) 
    {
    }

    try 
    {
        _clientSocket = _serverSocket.accept();
    } catch (IOException e) 
    {
    }

    while () 
    {
      //recive the image data
    }
        _clientSocket.close();
        _serverSocket.close();
  }
}
