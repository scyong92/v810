package proto.quasar.imageReconstruction;

import java.rmi.*;

public interface RemoteImageReconstructionEngineListenerInt extends Remote
{
  void setEmbeddedImageReconstructionEngine(int iPAdress);
}
