//package com.agilent.mtd.agt5dx.hardware;
package proto.quasar.imageReconstruction;

import com.agilent.mtd.util.*;

/**
* This class contains the metrics that are used by the auto-focus search algorithm to
* calculate sharpness values
* @author Horst Mueller
*/
class SharpnessMetricEnum extends Enum
{
  private static int _index = -1;

  /**
  * @author Horst Mueller
  */
  private SharpnessMetricEnum(int id)
  {
    super(id);
  }
}
