//package com.agilent.mtd.agt5dx.hardware;
package proto.quasar.imageReconstruction;

import com.agilent.mtd.util.*;

/**
* This class contains the target magnifications that are supported by the current
* hardware
* 1575 pixels per inch (ppi)
* 2560 pixels per inch 
* @author Horst Mueller
*/
class MagnificationEnum extends Enum
{
  public static final MagnificationEnum PIXELS_PER_INCH_1575 = new MagnificationEnum(1575);
  public static final MagnificationEnum PIXELS_PER_INCH_2560 = new MagnificationEnum(2560);

  /**
  * @author Horst Mueller
  */
  private MagnificationEnum(int id)
  {
    super(id);
  }

  int getNumPixelsPerInch()
  {
    return _id;
  }
}
