package proto.quasar.imageReconstruction;

import java.util.*;
import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;

class ReconstructionDataObservable extends Observable
{
  private SystemFiducialData _systemFiducialData;
  private MagnificationCalibrationData _MagnificationCalibrationData;
  private List _xrayCameraPositionData = new ArrayList();

  /*
  * @author Horst Mueller
  */
  void setSystemFiducialData(SystemFiducialData systemFiducialData)
  {
    Assert.expect(systemFiducialData != null);
    _systemFiducialData = systemFiducialData;

    setChanged();

    notifyObservers();
  }

  /*
  * @author Horst Mueller
  */
  void setMagnificationCalibrationData(MagnificationCalibrationData magnificationCalibrationData)
  {
    Assert.expect(magnificationCalibrationData != null);
    _magnificationCalibrationData = magnificationCalibrationData;

    setChanged();

    notifyObservers();
  }

  /*
  * @author Horst Mueller
  */
  void setXrayCameraPositionData(List xrayCameraPositionData)
  {
    Assert.expect(xrayCameraPositionData != null);
    _xrayCameraPositionData = xrayCameraPositionData;

    setChanged();

    notifyObservers();
  }

  /*
  * @author Horst Mueller
  */
  SystemFiducialData getSystemFiducialData()
  {
    return _systemFiducialData;
  }

  /*
  * @author Horst Mueller
  */
  MagnificationCalibrationData getMagnificationCalibrationData()
  {
    return _magnificationCalibrationData;
  }


  /*
  * @author Horst Mueller
  */
  XrayCameraPositionData getXrayCameraPositionData()
  {
    return _xrayCameraPositionData;
  }

  public void operation1()
  {
  }
}
