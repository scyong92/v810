package proto.saxParserTest;

import org.xml.sax.*;

/**
 * @author Erica Wheatcroft
 */

public class GenCam20SemanticHandlerFactory
{
  private static GenCam20BoardsSectionSemanticHandler _boardHandler = null;
  private static GenCam20ComponentPlacementsSectionSemanticHandler _componentPlacementsHandler = null;
  private static GenCam20DevicesSectionSemanticHandler _devicesHandler = null;
  private static GenCam20HeaderSectionSemanticHandler _headerHandler = null;
  private static GenCam20MainSemanticHandler _mainHandler = null;
  private static GenCam20MountingLocationsSectionSemanticHandler _mountingLocationsHandler = null;
  private static GenCam20PackagesSectionSemanticHandler _packagesHandler = null;
  private static GenCam20PadsSectionSemanticHandler _padsHandler = null;
  private static GenCam20PadStacksSectionSemanticHandler _padStacksHandler = null;
  private static GenCam20PanelsSectionSemanticHandler _panelsHandler = null;
  private static GenCam20PatternsSectionSemanticHandler _patternsHandler = null;
  private static GenCam20PrimitiveShapesSectionSemanticHandler _primitiveShapesHandler = null;
  private static GenCam20ProductsSectionSemanticHandler _productsHandler = null;
  private static GenCam20TargetsSectionSemanticHandler _targetsHandler = null;

  static
  {
    _boardHandler = new GenCam20BoardsSectionSemanticHandler();
    _componentPlacementsHandler = new GenCam20ComponentPlacementsSectionSemanticHandler();
    _devicesHandler = new GenCam20DevicesSectionSemanticHandler();
    _headerHandler = new GenCam20HeaderSectionSemanticHandler();
    _mainHandler = new GenCam20MainSemanticHandler();
    _mountingLocationsHandler = new GenCam20MountingLocationsSectionSemanticHandler();
    _packagesHandler = new GenCam20PackagesSectionSemanticHandler();
    _padsHandler = new GenCam20PadsSectionSemanticHandler();
    _padStacksHandler = new GenCam20PadStacksSectionSemanticHandler();
    _panelsHandler = new GenCam20PanelsSectionSemanticHandler();
    _patternsHandler = new GenCam20PatternsSectionSemanticHandler();
    _primitiveShapesHandler = new GenCam20PrimitiveShapesSectionSemanticHandler();
    _productsHandler = new GenCam20ProductsSectionSemanticHandler();
    _targetsHandler = new GenCam20TargetsSectionSemanticHandler();
  }

  private GenCam20SemanticHandlerFactory()
  {
  }

  static void attachParser(XMLReader parser)
  {
    _boardHandler.attachParser(parser);
    _componentPlacementsHandler.attachParser(parser);
    _devicesHandler.attachParser(parser);
    _headerHandler.attachParser(parser);
    _mainHandler.attachParser(parser);
    _mountingLocationsHandler.attachParser(parser);
    _packagesHandler.attachParser(parser);
    _padsHandler.attachParser(parser);
    _padStacksHandler.attachParser(parser);
    _panelsHandler.attachParser(parser);
    _patternsHandler.attachParser(parser);
    _primitiveShapesHandler.attachParser(parser);
    _productsHandler.attachParser(parser);
    _targetsHandler.attachParser(parser);
  }

  static GenCam20BoardsSectionSemanticHandler getBoardsSectionHandlerInstance()
  {
    return _boardHandler;
  }

  static GenCam20ComponentPlacementsSectionSemanticHandler getComponentPlacementsSectionHandlerInstance()
  {
    return _componentPlacementsHandler;
  }

  static GenCam20DevicesSectionSemanticHandler getDevicesSectionHandlerInstance()
  {
    return _devicesHandler;
  }
  static GenCam20HeaderSectionSemanticHandler getHeaderSectionHandlerInstance()
  {
    return _headerHandler;
  }
  static GenCam20MainSemanticHandler getMainHandlerInstance()
  {
    return _mainHandler;
  }
  static GenCam20MountingLocationsSectionSemanticHandler getMountingLocationsHandlerInstance()
  {
    return _mountingLocationsHandler;
  }
  static GenCam20PackagesSectionSemanticHandler getPackagesSectionHandlerInstance()
  {
    return _packagesHandler;
  }
  static GenCam20PadsSectionSemanticHandler getPadsSectionHandlerInstance()
  {
    return _padsHandler;
  }
  static GenCam20PadStacksSectionSemanticHandler getPadStacksSectionHandlerInstance()
  {
    return _padStacksHandler;
  }
  static GenCam20PanelsSectionSemanticHandler getPanelsSectionHandlerInstance()
  {
    return _panelsHandler;
  }
  static GenCam20PatternsSectionSemanticHandler getPatternsSectionHandlerInstance()
  {
    return _patternsHandler;
  }
  static GenCam20PrimitiveShapesSectionSemanticHandler getPrimitiveShapesSectionHandlerInstance()
  {
    return _primitiveShapesHandler;
  }
  static GenCam20ProductsSectionSemanticHandler getProductsSectionHandlerInstance()
  {
    return _productsHandler;
  }
  static GenCam20TargetsSectionSemanticHandler getTargetsSectionHandlerInstance()
  {
    return _targetsHandler;
  }




}
