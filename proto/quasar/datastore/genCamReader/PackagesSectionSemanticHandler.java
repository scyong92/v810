package proto.saxParserTest;

import java.util.*;
import org.xml.sax.*;

/**
 * @author Erica Wheatcroft
 */
public class GenCam20PackagesSectionSemanticHandler extends GenCam20MainSemanticHandler
{
  public Map map = new HashMap();

  public GenCam20PackagesSectionSemanticHandler()
  {
  }

  public void endElement(String namespaceURI, String localName, String qName) throws SAXException
  {
    if(qName.equalsIgnoreCase("Packages"))
      _reader.setContentHandler(GenCam20SemanticHandlerFactory.getMainHandlerInstance());
  }

  public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
  {

    if(qName.equalsIgnoreCase("Package"))
      _count ++;
    else if(qName.equalsIgnoreCase("Pin"))
    {
      _count ++;
      map.put(new Integer(_count), "Pin");
    }
    else if(qName.equalsIgnoreCase("Position"))
      _count ++;
    else if(qName.equalsIgnoreCase("Dimensions"))
      _count ++;
    else if(qName.equalsIgnoreCase("ShapeType"))
      _count ++;
    else if(qName.equalsIgnoreCase("PackageGroupingInformation"))
      _count ++;

  }

  public Map getMap()
  {
    return map;
  }
}
