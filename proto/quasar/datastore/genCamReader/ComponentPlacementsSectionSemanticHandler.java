package proto.saxParserTest;

import org.xml.sax.*;

/**
 * @author Erica Wheatcroft
 */
public class GenCam20ComponentPlacementsSectionSemanticHandler extends GenCam20MainSemanticHandler
{

  public GenCam20ComponentPlacementsSectionSemanticHandler()
  {
  }

  public void endElement(String namespaceURI, String localName, String qName) throws SAXException
  {
    if(qName.equalsIgnoreCase("ComponentPlacements"))
      _reader.setContentHandler(GenCam20SemanticHandlerFactory.getMainHandlerInstance());
  }

  public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
  {
    if(qName.equalsIgnoreCase("ComponentPlacement"))
      _count ++;
    else if(qName.equalsIgnoreCase("DeviceRef"))
      _count ++;
    else if(qName.equalsIgnoreCase("DeviceType"))
      _count ++;
  }
}
