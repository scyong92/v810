package proto.saxParserTest;

import org.xml.sax.*;

/**
 * @author Erica Wheatcroft
 */
public class GenCam20PanelsSectionSemanticHandler extends GenCam20MainSemanticHandler
{

  public GenCam20PanelsSectionSemanticHandler()
  {
  }

  public void endElement(String namespaceURI, String localName, String qName) throws SAXException
  {
    if(qName.equalsIgnoreCase("Panels"))
      _reader.setContentHandler(GenCam20SemanticHandlerFactory.getMainHandlerInstance());
  }

  public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
  {
    if(qName.equalsIgnoreCase("Panel"))
      _count ++;
    else if(qName.equalsIgnoreCase("TargetRef"))
      _count++;
    else if(qName.equalsIgnoreCase("Placement"))
      _count++;
    else if(qName.equalsIgnoreCase("Place"))
      _count++;
    else if(qName.equalsIgnoreCase("Width"))
      _count++;
    else if(qName.equalsIgnoreCase("BlockSkip"))
      _count++;
  }

}
