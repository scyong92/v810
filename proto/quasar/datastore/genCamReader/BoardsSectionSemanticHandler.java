package proto.saxParserTest;

import org.xml.sax.*;

/**
 * @author Erica Wheatcroft
 */

public class GenCam20BoardsSectionSemanticHandler extends GenCam20MainSemanticHandler
{
  public GenCam20BoardsSectionSemanticHandler()
  {
  }

  public void endElement(String namespaceURI, String localName, String qName) throws SAXException
  {
    if(qName.equalsIgnoreCase( "Boards"))
      _reader.setContentHandler(GenCam20SemanticHandlerFactory.getMainHandlerInstance());
  }

  public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
  {
    if(qName.equalsIgnoreCase( "Board"))
      _count ++;
    else if(qName.equalsIgnoreCase("TargetRef"))
      _count ++;
    else if(qName.equalsIgnoreCase("Place"))
      _count ++;
  }
}
