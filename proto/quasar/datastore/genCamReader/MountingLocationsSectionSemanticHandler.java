package proto.saxParserTest;

import org.xml.sax.*;

/**
 * @author Erica Wheatcroft
 */
public class GenCam20MountingLocationsSectionSemanticHandler extends GenCam20MainSemanticHandler
{

  public GenCam20MountingLocationsSectionSemanticHandler()
  {
  }

  public void endElement(String namespaceURI, String localName, String qName) throws SAXException
  {
    if(qName.equalsIgnoreCase("MountingLocations"))
      _reader.setContentHandler(GenCam20SemanticHandlerFactory.getMainHandlerInstance());
  }

  public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
  {
    if(qName.equalsIgnoreCase("MountingLocation"))
      _count ++;
    else if(qName.equalsIgnoreCase("Place"))
      _count ++;
    else if(qName.equalsIgnoreCase("PatternRef"))
      _count ++;
    else if(qName.equalsIgnoreCase("MachineFeederHead"))
      _count ++;
    else if(qName.equalsIgnoreCase("Parent"))
      _count ++;
  }
}
