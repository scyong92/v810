package proto.saxParserTest;

import org.xml.sax.*;

/**
 * @author Erica Wheatcroft
 */
public class GenCam20PadStacksSectionSemanticHandler extends GenCam20MainSemanticHandler
{

  public GenCam20PadStacksSectionSemanticHandler()
  {
  }

  public void endElement(String namespaceURI, String localName, String qName) throws SAXException
  {
    if(qName.equalsIgnoreCase("PadStacks"))
      _reader.setContentHandler(GenCam20SemanticHandlerFactory.getMainHandlerInstance());
  }

  public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
  {
    if(qName.equalsIgnoreCase("PadStack"))
      _count ++;
    else if(qName.equalsIgnoreCase("PadRef"))
      _count ++;
  }
}
