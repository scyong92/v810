package proto.saxParserTest;

import java.io.*;
import javax.xml.*;
import javax.xml.parsers.*;
import org.xml.sax.*;

public class TimeSaxParser
{

  public TimeSaxParser()
  {
    final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";
    final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";

    SAXParserFactory parserFactory = SAXParserFactory.newInstance();
    GenCam20ErrorHandler errorHandler = new GenCam20ErrorHandler();
    com.agilent.util.TimerUtil timer = new com.agilent.util.TimerUtil();
    try
    {
      parserFactory.setValidating(true);
      SAXParser parser = parserFactory.newSAXParser();
      parser.setProperty( JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA );
      parser.setProperty( JAXP_SCHEMA_SOURCE, new File("e:/GenCAM20Files/gc20_hacked.xsd") );
      XMLReader reader = parser.getXMLReader();
      reader.setContentHandler(GenCam20SemanticHandlerFactory.getMainHandlerInstance());
      errorHandler.setShowErrors( false );
      errorHandler.setPauseAfterMessages( true );
      reader.setErrorHandler(errorHandler);
      GenCam20SemanticHandlerFactory.attachParser(reader);
      timer.start();
      reader.parse("e:\\GenCam20Files\\gencam20Test.gcx");
      timer.stop();
      String time = com.agilent.util.StringUtil.convertMilliSecondsToElapsedTime(timer.getElapsedTimeInMills(), true);
      System.out.print("The Total Time to Parse is " + time);
      GenCam20PackagesSectionSemanticHandler handler = GenCam20SemanticHandlerFactory.getPackagesSectionHandlerInstance();
      System.out.print("\nThe Map size is " + handler.getMap().size());
    }
    catch (ParserConfigurationException pce)
    {
      pce.printStackTrace();
    }
    catch (SAXException se)
    {
      se.printStackTrace();
    }
    catch(IOException ioe)
    {
      ioe.printStackTrace();
    }
  }

  public static void main(String[] args)
  {
    TimeSaxParser timeSaxParser1 = new TimeSaxParser();
  }
