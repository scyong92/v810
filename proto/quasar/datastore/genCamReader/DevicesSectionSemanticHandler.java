package proto.saxParserTest;

import org.xml.sax.*;

/**
 * @author Erica Wheatcroft
 */
public class GenCam20DevicesSectionSemanticHandler extends GenCam20MainSemanticHandler
{

  public GenCam20DevicesSectionSemanticHandler()
  {
  }

  public void endElement(String namespaceURI, String localName, String qName) throws SAXException
  {
    if(qName.equalsIgnoreCase("Devices"))
      _reader.setContentHandler(GenCam20SemanticHandlerFactory.getMainHandlerInstance());
  }

  public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
  {
    if(qName.equalsIgnoreCase("Device"))
      _count ++;
  }
}
