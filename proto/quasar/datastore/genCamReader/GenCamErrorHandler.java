import java.io.*;
import org.xml.sax.*;

/**
 * @author Erica Wheatcroft
 */

public class GenCamErrorHandler implements ErrorHandler
{
  private boolean _pauseAfterMessages = false;
  private boolean _showErrors = false;

  public void warning(SAXParseException exception) throws SAXException
  {
    if ( _showErrors == true )
    {
      System.out.println("Warning --> " + exception.getMessage());
      if(_pauseAfterMessages == true)
      {
        try
        {
          System.in.read();
        }
        catch(IOException ioe)
        {
          // Do nothing
        }
      }
    }
  }

  public void error(SAXParseException exception) throws SAXException
  {
    if ( _showErrors == true )
    {
      System.out.println("ERROR --> " + exception.getMessage());
      if(_pauseAfterMessages == true)
      {
        try
        {
          System.in.read();
        }
        catch(IOException ioe)
        {
          // Do nothing
        }
      }
    }
  }

  public void fatalError(SAXParseException exception) throws SAXException
  {
    if ( _showErrors == true )
    {
      System.out.println("FATAL ERROR --> " + exception.getMessage());
      if(_pauseAfterMessages == true)
      {
        try
        {
          System.in.read();
        }
        catch(IOException ioe)
        {
          // Do nothing
        }
      }
    }
  }

  /**
   * @author Matt Wharton
   */
  public void setPauseAfterMessages( boolean pauseAfterMessages )
  {
    _pauseAfterMessages = pauseAfterMessages;
  }

  /**
   * @author Matt Wharton
   */
  public void setShowErrors( boolean showErrors )
  {
    _showErrors = showErrors;
  }
}
