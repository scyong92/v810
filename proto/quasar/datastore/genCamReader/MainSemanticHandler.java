package proto.saxParserTest;

import org.xml.sax.*;

/**
 * @author Erica Wheatcroft
 */

public class GenCam20MainSemanticHandler implements ContentHandler
{
  protected XMLReader _reader;
  protected int _count;
  public void attachParser(XMLReader parser)
  {
    _reader = parser;
  }

  // these methods are all inherited from the interface ContentHandler
  public GenCam20MainSemanticHandler()
  {
  }
  public void setDocumentLocator(Locator locator)
  {
  }
  public void startDocument() throws SAXException
  {
  }
  public void endDocument() throws SAXException
  {
  }
  public void startPrefixMapping(String prefix, String uri) throws SAXException
  {
  }
  public void endPrefixMapping(String prefix) throws SAXException
  {
  }
  public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
  {
    if ( qName.equalsIgnoreCase("Boards" ))
    {
      _reader.setContentHandler( GenCam20SemanticHandlerFactory.getBoardsSectionHandlerInstance() );
    }
    else if ( qName.equalsIgnoreCase("Devices") )
    {
      _reader.setContentHandler( GenCam20SemanticHandlerFactory.getDevicesSectionHandlerInstance() );
    }
    else if ( qName.equalsIgnoreCase("ComponentPlacements"))
    {
      _reader.setContentHandler( GenCam20SemanticHandlerFactory.getComponentPlacementsSectionHandlerInstance() );
    }
    else if ( qName.equalsIgnoreCase("Header" ))
    {
      _reader.setContentHandler(GenCam20SemanticHandlerFactory.getHeaderSectionHandlerInstance());
    }
    else if ( qName.equalsIgnoreCase("MountingLocations" ))
    {
      _reader.setContentHandler( GenCam20SemanticHandlerFactory.getMountingLocationsHandlerInstance() );
    }
    else if ( qName.equalsIgnoreCase("Packages" ))
    {
      _reader.setContentHandler( GenCam20SemanticHandlerFactory.getPackagesSectionHandlerInstance() );
    }
    else if ( qName.equalsIgnoreCase("Pads" ))
    {
      _reader.setContentHandler( GenCam20SemanticHandlerFactory.getPadsSectionHandlerInstance() );
    }
    else if ( qName.equalsIgnoreCase("PadStacks" ))
    {
      _reader.setContentHandler( GenCam20SemanticHandlerFactory.getPadStacksSectionHandlerInstance() );
    }
    else if ( qName.equalsIgnoreCase( "Panels" ))
    {
      _reader.setContentHandler( GenCam20SemanticHandlerFactory.getPanelsSectionHandlerInstance() );
    }
    else if ( qName.equalsIgnoreCase("Patterns" ))
    {
      _reader.setContentHandler( GenCam20SemanticHandlerFactory.getPatternsSectionHandlerInstance() );
    }
    else if ( qName.equalsIgnoreCase("PrimitiveShapes" ))
    {
      _reader.setContentHandler( GenCam20SemanticHandlerFactory.getPrimitiveShapesSectionHandlerInstance() );
    }
    else if ( qName.equalsIgnoreCase("Products" ))
    {
      _reader.setContentHandler( GenCam20SemanticHandlerFactory.getProductsSectionHandlerInstance() );
    }
    else if ( qName.equalsIgnoreCase("Targets" ))
    {
      _reader.setContentHandler( GenCam20SemanticHandlerFactory.getTargetsSectionHandlerInstance() );
    }
  }
  public void endElement(String namespaceURI, String localName, String qName) throws SAXException
  {
  }
  public void characters(char[] ch, int start, int length) throws SAXException
  {
  }
  public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException
  {
  }
  public void processingInstruction(String target, String data) throws SAXException
  {
  }
  public void skippedEntity(String name) throws SAXException
  {
  }
}
