package proto.saxParserTest;

import org.xml.sax.*;

/**
 * @author Erica Wheatcroft
 */
public class GenCam20PrimitiveShapesSectionSemanticHandler extends GenCam20MainSemanticHandler
{

  public GenCam20PrimitiveShapesSectionSemanticHandler()
  {
  }

  public void endElement(String namespaceURI, String localName, String qName) throws SAXException
  {
    if(qName.equalsIgnoreCase("PrimitiveShapes"))
      _reader.setContentHandler(GenCam20SemanticHandlerFactory.getMainHandlerInstance());
  }

  public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
  {
    if(qName.equalsIgnoreCase("CircleDef"))
      _count ++;
    else if(qName.equalsIgnoreCase("RectCornerDef"))
      _count++;
    else if(qName.equalsIgnoreCase("RectCenterDef"))
      _count++;
  }
}
