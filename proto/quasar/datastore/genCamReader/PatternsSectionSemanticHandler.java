package proto.saxParserTest;

import org.xml.sax.*;

/**
 * @author Erica Wheatcroft
 */
public class GenCam20PatternsSectionSemanticHandler extends GenCam20MainSemanticHandler
{

  public GenCam20PatternsSectionSemanticHandler()
  {
  }

  public void endElement(String namespaceURI, String localName, String qName) throws SAXException
  {
    if(qName.equalsIgnoreCase("Patterns"))
      _reader.setContentHandler(GenCam20SemanticHandlerFactory.getMainHandlerInstance());
  }

  public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
  {
    if(qName.equalsIgnoreCase("Pattern"))
      _count ++;
    else if(qName.equalsIgnoreCase("Place"))
      _count ++;
    else if(qName.equalsIgnoreCase("PadLocation"))
      _count ++;
    else if(qName.equalsIgnoreCase("PatternGroupingInformation"))
      _count ++;
  }
}
