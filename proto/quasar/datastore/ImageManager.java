//package com.agilent.xRayTest.datastore;
package proto.quasar.datastore;

import java.io.*;

import com.agilent.util.*;

/**
 * This class handles the saving, loading and caching of image data.
 *
 * @author Andy Mechtenberg
 */
public class ImageManager
{
  private ImageManager _instance;
  private int _cacheSizeInBytes;

  public synchronized ImageManager getInstance()
  {
    if (_instance == null)
      _instance = new ImageManager();

    return _instance;
  }

  /**
   * @author Andy Mechtenberg
   */
  private ImageManager()
  {
    // do nothing
  }

  /**
   * @author Andy Mechtenberg
   */
  public void saveSingleProjection(String projectName, ProjectionImage projectionImage) throws DatastoreException
  {
    /*
        Figure out what directory / filename to save this projection at:
           - project name
           - projectionImage._scanPass
           - projectionImage._cameraId
           - projectionImage.magnificationLevel --> mag1575 or mag2560
           - DIRECTORY:  %IMAGEROOT%/projectName/projections/mag1575 or mag2560/proj<scanPass:3><cameraID:3>.jpg (i.e. proj001012.jpg)
        Save it there in TIF format, throwing a DatastoreStore exception if necessary
           - save projectionImage._xRayImage
     */
  }

  /**
   * @param projectName
   * @param cameraId
   * @param scanPass
   * @param magnificationLevel
   * @author Andy Mechtenberg
   */
  public ProjectionImage loadProjectionImage(String projectName, int cameraId, int scanPass, MagnificationEnum magnificationLevel)
  {
    /*
        Figure out what directory / filename to load this projection from:
           - project name
           - scanPass
           - cameraId
           - magnificationLevel --> mag1575 or mag2560
           - DIRECTORY:  %IMAGEROOT%/projectName/projections/mag1575 or mag2560/proj<scanPass:3><cameraID:3>.jpg (i.e. proj001012.jpg)
        Save it there in TIF format, throwing a DatastoreStore exception if necessary
           - save projectionImage._xRayImage
     */
  }

  /**
   * @author Andy Mechtenberg
   */
  public void saveReconstructedImage(String projectName, ReconstructedImage reconstructedImage) throws DatastoreException
  {
    /*
        Figure out what directory / filename to save this reconstuction at
           - project name
           - ReconstructedImage.reconstructionRegion._topOrBottom
           - ReconstructedImage.reconstructionRegion._xInNanoMeters
           - ReconstructedImage.reconstructionRegion._yInNanoMeters
           - ReconstructedImage.reconstructionRegion._widthInNanoMeters
           - ReconstructedImage.reconstructionRegion._lengthInNanoMeters
           - ReconstructedImage.reconstructionRegion._magnificationLevel (target magnification level, not actual magnification level)
           - ReconstructedImage._sliceNumber

           - DIRECTORY:  %IMAGEROOT%/projectName/images/top/<maglevel>/<something>.tif
        Save it there in TIF format, throwing a DatastoreStore exception if necessary
           - save reconstructedImage._xRayImage
     */
  }

  /**
   * @param projectName The project for which this recontructed image is for
   * @param originInNanoMeters The lower left corner, in panel coordinates, for where this reconstructed image is located
   * @param lengthInNanoMeters The length from the origin in nanometers
   * @param heightInNanoMeters The height from the origin in nanometers
   * @author Andy Mechtenberg
   */
  //  public ReconstructedImage loadReconstructedImage(String projectName, Coordinate originInNanoMeters, int lengthInNanoMeters, int heightInNanoMeters)
  public ReconstructedImage loadReconstructedImage(String projectName, ReconstructionRegion reconstructionRegion, int sliceNumber)
  {

  }

  /**
   * @param projectName The project for which this recontructed image is for
   * @param originInNanoMeters The lower left corner, in panel coordinates, for where this reconstructed image is located
   * @param lengthInNanoMeters The length from the origin in nanometers
   * @param heightInNanoMeters The height from the origin in nanometers
   * @return List of ReconstructedImages, one for each slice.  Return an empty list if none on disk.
   * @author Andy Mechtenberg
   */
  //  public ReconstructedImage loadReconstructedImage(String projectName, Coordinate originInNanoMeters, int lengthInNanoMeters, int heightInNanoMeters)
  public List loadReconstructedImages(String projectName, ReconstructionRegion reconstructionRegion)
  {

  }

  /**
   * @param projectName The project for which this recontructed image is for
   * @param originInNanoMeters The lower left corner, in panel coordinates, for where this reconstruted image is located
   * @param lengthInNanoMeters The length from the origin in nanometers
   * @param heightInNanoMeters The height from the origin in nanomaters
   * @author Andy Mechtenberg
   */
  //  public int loadReconstructedImageIntoCache(String projectName, Coordinate originInNanoMeters, int lengthInNanoMeters, int heightInNanoMeters)
  public int loadReconstructedImageIntoCache(String projectName, ReconstructionRegion reconstructionRegion, int sliceNumber)
  {
    return sizeInBytes;  // return size in bytes of the image when it's in memory (for cache management)
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getTotalCacheSizeInBytes()
  {
    Assert.expect(_cacheSizeInBytes > 0);

    return _cacheSizeInBytes;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTotalCacheSize(int size)
  {
    Assert.expect(size > 0);

    // Should we check to see how much in currently in the cache?
    // Do we assert if the new size is too small for what's already there?

    _cacheSizeInBytes = size;
  }


  /**
   * @author Andy Mechtenberg
   */
  public void clearAllLoadedImages()
  {
    // clear out all cached images
  }

  /**
   * @author Andy Mechtenberg
   */
  public void clearAllLoadedImages(String projectName)
  {
    // clear out all cached images for the specified project
  }

  /**
   * @author Andy Mechtenberg
   */
  public void clearReconstructedImage(String projectName, ReconstructedImage reconstructedImage)
  {
    // remove the specified reconstructed image from the cache
  }

  /**
   * @author Andy Mechtenberg
   */
  public void deleteProjectionImage(String projectName, ProjectionImage projectionImage) throws DatastoreException
  {
  }

  /**
   * @author Andy Mechtenberg
   */
  public void deleteAllProjectionImages(String projectName) throws DatastoreException
  {
    // deletes all saved projection images for the specified project
  }

  /**
   * @author Andy Mechtenberg
   */
  public void deleteReconstuctedImage(String projectName, ReconstructedImage reconstructedImage) throws DatastoreException
  {
  }

  /**
   * @author Andy Mechtenberg
   */
  public void deleteAllReconstructionImages(String projectName) throws DatastoreException
  {
    // deletes all saved reconstructed images for the specified project
  }
}
