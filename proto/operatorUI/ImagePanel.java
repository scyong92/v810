package proto.operatorUI;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

public class ImagePanel extends JPanel
{
  private ImageIcon _noboardIcon = new ImageIcon(Operator5DX.class.getResource("noboard.gif"));
  private ImageIcon _upArrowIcon = new ImageIcon(Operator5DX.class.getResource("uparrow.gif"));
  private ImageIcon _blankArrowIcon = new ImageIcon(Operator5DX.class.getResource("blankarrow.gif"));
  private ArrowAnimation _animated = new ArrowAnimation();

  private BorderLayout borderLayout1 = new BorderLayout();
  private JLabel _imageLbl = new JLabel();
  private JLabel _arrow1Lbl = new JLabel();
  private JLabel _arrow2Lbl = new JLabel();
  private Border border1;
  private TitledBorder titledBorder1;

  public ImagePanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
    Thread arrowAnimate = new Thread( _animated );
    arrowAnimate.start();
  }

  void jbInit() throws Exception
  {
    border1 = BorderFactory.createLineBorder(SystemColor.controlText,1);
    titledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134)),"Panel Image");
    _imageLbl.setFont(new java.awt.Font("SansSerif", 1, 14));
    _imageLbl.setHorizontalTextPosition(SwingConstants.CENTER);
    _imageLbl.setIcon(_noboardIcon);
    _imageLbl.setVerticalTextPosition(SwingConstants.BOTTOM);
    this.setLayout(borderLayout1);
    _arrow1Lbl.setIcon(_upArrowIcon);
    _arrow2Lbl.setIcon(_upArrowIcon);
    this.add(_imageLbl, BorderLayout.CENTER);
    this.add(_arrow1Lbl, BorderLayout.WEST);
    this.add(_arrow2Lbl, BorderLayout.EAST);
  }

  public void loadPanel( String program, ImageIcon picture )
  {
    if (picture == null)
      _imageLbl.setIcon(_noboardIcon);
    else
      _imageLbl.setIcon(picture);
//    if (program == null)
//      _imageLbl.setText("No Panel Program");
//    else
//      _imageLbl.setText(program);

    _animated.running = true;  // start the animation
  }

  public void panelLoaded()
  {
    _animated.running = false;  // stop the animation
  }

  class ArrowAnimation implements Runnable
  {
    public boolean running = false;
    private boolean seen = true;
    public void run()
    {
      while (true)
      {
        if (running)
        {
          if (seen)
          {
            _arrow1Lbl.setIcon(_blankArrowIcon);
            _arrow2Lbl.setIcon(_blankArrowIcon);
          }
          else
          {
            _arrow1Lbl.setIcon(_upArrowIcon);
            _arrow2Lbl.setIcon(_upArrowIcon);
          }
          seen = !seen;
        }
        try { Thread.sleep(500); } catch (Exception e) { }
      }
    }
  }


}