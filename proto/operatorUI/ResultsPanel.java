package proto.operatorUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

/**
 * Title:        New 5DX GUI
 * Description:  This project contains the files created for the
 * new 5DX user interface.
 * Copyright:    Copyright (c) Steve Anonson
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ResultsPanel extends JPanel
{
  BorderLayout borderLayout1 = new BorderLayout();
  Border border1;
  TitledBorder titledBorder1;
  Border border2;
  JPanel _centerPanel = new JPanel();
  JPanel _southPanel = new JPanel();
  JButton _resetBtn = new JButton();
  BorderLayout borderLayout2 = new BorderLayout();
  JPanel _listPanel = new JPanel();
  BorderLayout borderLayout4 = new BorderLayout();
  JPanel _dataPanel = new JPanel();
  JPanel _resultPanel = new JPanel();
  BorderLayout borderLayout5 = new BorderLayout();
  JPanel _chartPanel = new JPanel();
  PanelList _panelList = new PanelList();
  JScrollPane jScrollPane1 = new JScrollPane();
  JLabel _countLbl = new JLabel();
  BorderLayout borderLayout3 = new BorderLayout();
  JPanel _lblPanel = new JPanel();
  JPanel _tfPanel = new JPanel();
  JLabel _passLbl = new JLabel();
  JLabel _acceptLbl = new JLabel();
  JLabel _rejectLbl = new JLabel();
  GridLayout gridLayout1 = new GridLayout(0,1);

  JChart2D _chart = null;
  int[] _data = { 75, 20, 5 };
  Color[] _colors = { Color.green, Color.yellow, Color.red };

  private int _maxPanelCount = 15;
  private int _panelCount = 0;

  public ResultsPanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    border1 = BorderFactory.createLineBorder(SystemColor.controlText,1);
    titledBorder1 = new TitledBorder(border1," Results ");
    border2 = BorderFactory.createCompoundBorder(titledBorder1,BorderFactory.createEmptyBorder(0,2,0,2));
    this.setBorder(border2);
    this.setLayout(borderLayout1);
    _resetBtn.setText("Reset");
    _resetBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _resetBtn_actionPerformed(e);
      }
    });
    _centerPanel.setLayout(borderLayout2);
    _listPanel.setLayout(borderLayout4);
    _resultPanel.setLayout(borderLayout5);
    _panelList.setVisibleRowCount(5);
    _countLbl.setFont(new java.awt.Font("Dialog", 1, 12));
    _countLbl.setForeground( Color.black );
    _countLbl.setText( new String("Last " + _panelCount + " panels"));
    _dataPanel.setLayout(borderLayout3);
    _passLbl.setBackground(Color.green);
    _passLbl.setForeground( Color.black );
    _passLbl.setOpaque(true);
    _passLbl.setText("Passed");
    _acceptLbl.setBackground(Color.yellow);
    _acceptLbl.setForeground( Color.black );
    _acceptLbl.setOpaque(true);
    _acceptLbl.setText("Failed - Accepted");
    _rejectLbl.setBackground(Color.red);
    _rejectLbl.setForeground( Color.black );
    _rejectLbl.setOpaque(true);
    _rejectLbl.setText("Rejected");
    _lblPanel.setLayout(gridLayout1);
//    gridLayout1.setColumns(1);
//    gridLayout1.setRows(0);
    _tfPanel.setLayout(gridLayout2);
//    gridLayout2.setColumns(1);
    _passTF.setText("75");
    _passTF.setColumns(3);
    _acceptTF.setText("20");
    _acceptTF.setColumns(3);
    _rejectTF.setText("5");
    _rejectTF.setColumns(3);
    borderLayout3.setHgap(2);
    _chartPanel.setLayout(borderLayout6);
    borderLayout5.setHgap(5);
    this.add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.add(_listPanel, BorderLayout.CENTER);
    _listPanel.add(jScrollPane1, BorderLayout.NORTH);
    jScrollPane1.getViewport().add(_panelList, null);
    _chart = new JPieChart2D(_data.length, _data, _colors, "");
//    _chart = new JBarChart2D(_data.length, null, _data, _colors, "");
//    _chart.setColumnWidth(50);
    _chart.setGap(0);
    _chartPanel.add(_chart, BorderLayout.CENTER);
    _centerPanel.add(_resultPanel, BorderLayout.SOUTH);
    _resultPanel.add(_chartPanel, BorderLayout.CENTER);
    _resultPanel.add(_dataPanel, BorderLayout.EAST);
    _dataPanel.add(_countLbl, BorderLayout.NORTH);
    _dataPanel.add(_lblPanel, BorderLayout.WEST);
    _lblPanel.add(_passLbl, null);
    _lblPanel.add(_acceptLbl, null);
    _lblPanel.add(_rejectLbl, null);
    _dataPanel.add(_tfPanel, BorderLayout.EAST);
    _tfPanel.add(_passTF, null);
    _tfPanel.add(_acceptTF, null);
    _tfPanel.add(_rejectTF, null);
    this.add(_southPanel, BorderLayout.SOUTH);
    _southPanel.add(_resetBtn, null);
  }

  public void panelDone( PanelEntry p )
  {
    if (_panelList.count() >= _maxPanelCount)
      _panelList.dequeue();  // take off the oldest one
    _panelList.enqueue(p);
    _panelCount = _panelList.count();
    updateResults();
  }

  private void updateResults()
  {
    Object[] arr = _panelList.toArray();
    if (arr == null)
      return;
    int failed = 0;
    int passed = 0;
    int accepted = 0;
    for (int i = 0; i < arr.length; i++)
    {
      PanelEntry p = (PanelEntry)arr[i];
      if (p.getTestResult() == PanelEntry.FAILED)
        failed++;
      else if (p.getTestResult() == PanelEntry.PASSED)
        passed++;
      else if (p.getTestResult() == PanelEntry.FAILED_ACCEPTED)
        accepted++;
    }
    _passTF.setText( new String("" + passed));
    _acceptTF.setText( new String("" + accepted));
    _rejectTF.setText( new String("" + failed));

    if (_panelCount < _maxPanelCount)
      _countLbl.setText( new String("Last " + _panelCount + " panels"));
    else
      _countLbl.setText( new String("Last " + _maxPanelCount + " panels"));

    _data[0] = passed;
    _data[1] = accepted;
    _data[2] = failed;
    _chart.updateValues(_data.length, null, _data, _colors);
  }

  public static void main(String[] args)
  {
    JFrame f = new JFrame("Results Panel test");
    f.addWindowListener( new WindowAdapter()
    { public void windowClosing(WindowEvent e) { System.exit(0); } });
    ResultsPanel p = new ResultsPanel();
    p._startFromMain = true;
    f.getContentPane().add(p);
    f.pack();
    f.show();
  }
  private boolean _startFromMain = false;
  JTextField _passTF = new JTextField();
  GridLayout gridLayout2 = new GridLayout(0,1);
  JTextField _acceptTF = new JTextField();
  JTextField _rejectTF = new JTextField();
  BorderLayout borderLayout6 = new BorderLayout();

  void _resetBtn_actionPerformed(ActionEvent e)
  {

  }
}
