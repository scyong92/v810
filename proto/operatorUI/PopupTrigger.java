package proto.operatorUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class PopupTrigger extends MouseAdapter
{
  private JPopupMenu _popup;

  public PopupTrigger( JPopupMenu popup )
  {
    this._popup = popup;
  }

  private void showIfPopupTrigger( MouseEvent me )
  {
    if (me.isPopupTrigger())
    {
      this._popup.show( me.getComponent(), me.getX(), me.getY() );
    }
  }

  public void mousePressed( MouseEvent me )
  {
    showIfPopupTrigger(me);
  }

  public void mouseReleased( MouseEvent me )
  {
    showIfPopupTrigger(me);
  }
}