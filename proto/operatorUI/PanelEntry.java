package proto.operatorUI;

import javax.swing.*;

public class PanelEntry
{
  public static final String PASSED = "Passed";
  public static final String FAILED = "Failed";
  public static final String FAILED_ACCEPTED = "Failed/Accepted";
  public static final String ABORTED = "Aborted";

  private String _serialNumber = null;
  private String _panelProgram = null;
  private String _testResult = null;
  private ImageIcon _picture = null;

  public PanelEntry( String num, String prog, ImageIcon image )
  {
    _serialNumber = num;
    _panelProgram = prog;
    _picture = image;
  }

  public PanelEntry( String num )
  {
    this(num, null, null);
  }

  public String getSerialNumber() { return _serialNumber; }
  public String getPanelProgram() { return _panelProgram; }
  public String getTestResult()   { return _testResult; }
  public ImageIcon getPicture()   { return _picture; }

  public void setSerialNumber( String num ) { _serialNumber = num; }
  public void setPanelProgram( String prog) { _panelProgram = prog; }
  public void setTestResult(String res)     { _testResult = res; }
  public void setPicture(ImageIcon image)     { _picture = image; }

  public boolean equals( PanelEntry rhs )
  {
    return (_serialNumber.equals(rhs._serialNumber));
  }

  public String toString()
  {
    String str = "";
    if (_serialNumber != null)
      str += _serialNumber;
    if (_panelProgram != null)
      str += "    " + _panelProgram;
    if (_testResult != null)
      str += "    " + _testResult;
    return str;
  }
}
