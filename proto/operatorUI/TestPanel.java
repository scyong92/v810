package proto.operatorUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
//import com.borland.jbcl.layout.*;

/**
 * Title:        New 5DX GUI
 * Description:  This project contains the files created for the
 * new 5DX user interface.
 * Copyright:    Copyright (c) Steve Anonson
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class TestPanel extends JPanel
{
  private boolean _stopped = false;
  private boolean _paused = false;
  private StringBuffer _testMsg = new StringBuffer(1024);
  private ImageIcon _noboardIcon = new ImageIcon(Operator5DX.class.getResource("nopanel.jpg"));


  public TestPanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public boolean stopped()  { return _stopped; }
  public boolean paused()   { return _paused; }

  void jbInit() throws Exception
  {
    border2 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134)),"  Inspection Messages "),BorderFactory.createEmptyBorder(0,2,0,2));
    border3 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Panel Test "),BorderFactory.createEmptyBorder(0,2,0,2));
    this.setLayout(borderLayout1);
    this.setBorder(border2);
    ActionListener pauseAction = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _pauseBtn_actionPerformed(e);
      }
    };
    ActionListener stopAction = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _stopBtn_actionPerformed(e);
      }
    };
    _centerPanel.setLayout(borderLayout3);
    _messages.setEditable(false);
    _messages.setRequestFocusEnabled(false);
    _centerPanel.setRequestFocusEnabled(false);
    _currentPanel.setBackground(SystemColor.info);
    _currentPanel.setRequestFocusEnabled(false);
    _currentPanel.setEditable(false);
    _currentPanel.setMargin(new Insets(0, 5, 0, 5));
    _northPanel.setLayout(borderLayout2);
    _northPanel.setRequestFocusEnabled(false);
    _statusLbl.setForeground(Color.black);
    _statusLbl.setText("Inspecting");
    borderLayout2.setHgap(5);
    borderLayout3.setVgap(3);
    _eastPanel.setLayout(borderLayout4);
    jLabel1.setHorizontalTextPosition(SwingConstants.CENTER);
    jLabel1.setIcon(_noboardIcon);
    jLabel1.setText("No Image");
    jLabel1.setVerticalTextPosition(SwingConstants.BOTTOM);
    this.add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.add(jScrollPane1, BorderLayout.CENTER);
    _centerPanel.add(_northPanel, BorderLayout.NORTH);
    _northPanel.add(_statusLbl, BorderLayout.WEST);
    _northPanel.add(_currentPanel, BorderLayout.CENTER);
//    this.add(_eastPanel, BorderLayout.EAST);
    _eastPanel.add(_imagePanel, BorderLayout.CENTER);
    _imagePanel.add(jLabel1, null);
    jScrollPane1.getViewport().add(_messages, null);
  }

  public boolean loadPanel( PanelEntry p )
  {
    if (p == null)
      return false;
    String program = p.getPanelProgram();
    _statusLbl.setText("Loading");
    _currentPanel.setText(p.toString());
    // Load the panel program if not already loaded.

    // Put up the load panel screen.
    this.setMessage("Waiting for panel...\n    " + p.toString());  // initialize the test message
// DEBUG
    try { Thread.sleep(7000); }
    catch (InterruptedException e) { }
// DEBUG
    // Remove load panel screen when panel is correctly loaded.
    return true;
  }

// DEBUG
  private int _panelCount = 0;
// DEBUG

  public String testPanel( PanelEntry p )
  {
    if (p == null)
      return PanelEntry.FAILED;
    _panelCount++;
    String program = p.getPanelProgram();
    _statusLbl.setText("Inspecting");
    _currentPanel.setText(p.toString());
    // Run the test for the panel.
// DEBUG
    String[] msgs = {
      "Checking alignment point 1\n",
      "Checking alignment point 2\n",
      "Checking alignment point 3\n",
      "\nChecking surface map point 72 of 72\n",
      "\nTesting view 154 of 155\n",
      "Testing view 155 of 155\n",
      "\nPins tested:  1726    Pins failed:  543\n",
      "\nAlignment:  00:13    Map:  00:24    Test: 1:26\n",
      "\nTesting Complete\n" };
    int[] delays = { 500, 200, 200, 200, 500, 1000, 1000, 200, 100 };

    this.setMessage("");  // initialize the test message
    for (int i = 0; i < msgs.length; i++)
    {
      try { Thread.sleep(delays[i]); }
      catch (InterruptedException e) { }
      if (_stopped)
      {
        this.appendMessage("\n\n Inspection Aborted\n");
        break;
      }
      this.appendMessage(msgs[i]);
    }
// DEBUG
    // Remove load panel screen when panel is correctly loaded.
// DEBUG
    int t1 = (int)(Math.random() * 10);
    int t3 = (int)(Math.random() * 6);
    if (_stopped)
      return PanelEntry.ABORTED;
    else if (t1 == t3)
      return PanelEntry.FAILED;
//    else if (t1 < t3)
//      return PanelEntry.FAILED_ACCEPTED;
    return PanelEntry.PASSED;
// DEBUG
  }

  public boolean unloadPanel( PanelEntry p )
  {
    if (p == null)
      return false;
    _statusLbl.setText("Unloading");
    _currentPanel.setText(p.toString());
    // Put up the unload panel screen.
// DEBUG
    try { Thread.sleep(3000); }
    catch (InterruptedException e) { }
// DEBUG
    // Remove unload panel screen when panel is correctly unloaded.
    return true;
  }

  private void setMessage( String msg )
  {
    _testMsg.delete(0, _testMsg.length());
    if (msg != null)
      _testMsg.append(msg);
    _messages.setText(_testMsg.toString());
  }

  private void appendMessage( String msg )
  {
    if (msg != null)
    {
      _testMsg.append(msg);
      _messages.setText(_testMsg.toString());
      _messages.setCaretPosition( _messages.getText().length());
    }
  }

  void _pauseBtn_actionPerformed(ActionEvent e)
  {
    this.setPaused(!_paused);
  }

  void _stopBtn_actionPerformed(ActionEvent e)
  {
    this.setStopped(!_stopped);
  }

  void setStopped( boolean flag )
  {
    if (_stopped == flag)
      return;
    _stopped = flag;
    if (_stopped)
      this.setPaused(false);
  }

  void setPaused( boolean flag )
  {
    if (_paused == flag)
      return;
    _paused = flag;
  }


  public static void main(String[] args)
  {
    JFrame f = new JFrame("Test Panel test");
    f.addWindowListener( new WindowAdapter()
    { public void windowClosing(WindowEvent e) { System.exit(0); } });
    TestPanel p = new TestPanel();
    p._startFromMain = true;
    f.getContentPane().add(p);
    f.pack();
//    f.setSize(500,200);
    f.show();
  }

  private BorderLayout borderLayout1 = new BorderLayout();
  private Border border2;
  private Border border3;
  private JPanel _centerPanel = new JPanel();
  private JScrollPane jScrollPane1 = new JScrollPane();
  private JTextPane _messages = new JReadOnlyTextPane();
  private boolean _startFromMain = false;
  private BorderLayout borderLayout3 = new BorderLayout();
  private JTextField _currentPanel = new JReadOnlyTextField();
  private JPanel _northPanel = new JPanel();
  private JLabel _statusLbl = new JLabel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private JPanel _eastPanel = new JPanel();
  private BorderLayout borderLayout4 = new BorderLayout();
  private JPanel _imagePanel = new JPanel();
  private JLabel jLabel1 = new JLabel();
}
