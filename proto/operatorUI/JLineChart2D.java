package proto.operatorUI;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;


public class JLineChart2D extends JChart2D
{
  public JLineChart2D(int nData, int[] xData, int[] yData, String text)
  {
    super(nData, xData, yData, null, text);
  }

  protected ChartPanel createChartPanel()
  {
    return new LineChartPanel();
  }


  class LineChartPanel extends ChartPanel
  {
    LineChartPanel()
    {
    }

    public void paintComponent(Graphics g)
    {
      int x0 = 0;
      int y0 = 0;

      g.setColor(Color.black);
      x0 = xChartToScreen(0);
      g.drawLine(x0, _y, x0, _y + _h);
      y0 = yChartToScreen(0);
      g.drawLine(_x, y0, _x + _w, y0);

      Graphics2D g2 = (Graphics2D) g;
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_RENDERING,
        RenderingHints.VALUE_RENDER_QUALITY);

      if (_stroke != null)
        g2.setStroke(_stroke);

      GeneralPath path = new GeneralPath();
      g2.setColor(_lineColor);
      path.moveTo(xChartToScreen(_xData[0]),
      yChartToScreen(_yData[0]));
      for (int k = 1; k < _nData; k++)
        path.lineTo(xChartToScreen(_xData[k]), yChartToScreen(_yData[k]));
      g2.draw(path);
    }
  } // LineChartPanel
}
