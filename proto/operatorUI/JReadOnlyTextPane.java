package proto.operatorUI;

import javax.swing.JTextPane;
import javax.swing.text.StyledDocument;


public class JReadOnlyTextPane extends JTextPane
{
  public JReadOnlyTextPane()
  {
    super();
  }

  public JReadOnlyTextPane(StyledDocument doc)
  {
    super(doc);
  }

  /**
   * Override this method means that the text field cannont recieve focus.
   */
  public boolean isFocusTraversable()
  {
    return false;
  }
}
