package proto.operatorUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

/**
 * Title:        New 5DX GUI
 * Description:  This project contains the files created for the
 * new 5DX user interface.
 * Copyright:    Copyright (c) Steve Anonson
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class Operator5DX extends JFrame
{
  private static final ImageIcon _RED_ICON = new ImageIcon(Operator5DX.class.getResource("redlight.gif"));
  private static final ImageIcon _YELLOW_ICON = new ImageIcon(Operator5DX.class.getResource("yellowlight.gif"));
  private static final ImageIcon _GREEN_ICON = new ImageIcon(Operator5DX.class.getResource("greenlight.gif"));
//  private static final int _RED_STATUS    = -1;
//  private static final int _YELLOW_STATUS =  0;
//  private static final int _GREEN_STATUS  =  1;
  private final StatusEnum READY_5DX = new StatusEnum("5DX Ready", _YELLOW_ICON);
  private final StatusEnum AUTOMATIC_STARTUP = new StatusEnum("Automatic Startup", _YELLOW_ICON);
  private final StatusEnum TESTING_STOPPED = new StatusEnum("TESTING STOPPED", _YELLOW_ICON);
  private final StatusEnum TESTING_PAUSED = new StatusEnum("TESTING PAUSED", _YELLOW_ICON);
  private final StatusEnum WAITING_FOR_PANEL = new StatusEnum("Waiting for panel", _YELLOW_ICON);
  private final StatusEnum LOADING = new StatusEnum("Loading panel", _YELLOW_ICON);
  private final StatusEnum TESTING = new StatusEnum("Testing panel", _GREEN_ICON);
  private final StatusEnum UNLOADING = new StatusEnum("Unloading panel", _YELLOW_ICON);
  private final StatusEnum TEST_COMPLETE = new StatusEnum("Testing complete", _YELLOW_ICON);
  private final StatusEnum LOGOUT = new StatusEnum("Logout", _YELLOW_ICON);

  private Color _specialBackground = SystemColor.activeCaption;
  private boolean _done = false;

  public Operator5DX()
  {
    super("5DX Operator");
    addWindowListener( new WindowAdapter()
    { public void windowClosing(WindowEvent e) { exitOperator5DX(0); } });

    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    setScreenPosition();
    this.setSize( this.getWidth() + 300, this.getHeight() + 50);
//    this.setSize( 600, 600);
  }

  public boolean done()       { return _done; }

  private void exitOperator5DX( int exitVal )
  {
    _done = true;
    if (_startFromMain)
      System.exit(exitVal);
  }

  private void setScreenPosition()
  {
   // Put the dialog in the upper center of the screen.
    pack();
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension thisSize = getSize();
    if (thisSize.height > screenSize.height)
      thisSize.height = screenSize.height;
    if (thisSize.width > screenSize.width)
      thisSize.width = screenSize.width;
    int x = screenSize.width / 2 - thisSize.width / 2;
    int y = screenSize.height / 8;
    setLocation( x, y );
  }

  public void start()
  {
    doAutomaticStartup();
//    doTestPanel();
    _operatorPanel.startTesting();
    doLogout();
  }

  private boolean doAutomaticStartup()
  {
    setStatus(AUTOMATIC_STARTUP);
    return true;
  }

  private boolean doTestPanel()
  {
    setStatus(TESTING);
    PanelList pList = _operatorPanel.serialNumPanel().getPanelList();
    do {
      while (!done() && (pList.empty() || _operatorPanel.testPanel().stopped() || _operatorPanel.testPanel().paused()) )
      {
        if (_operatorPanel.testPanel().stopped())
          setStatus(TESTING_STOPPED);
        else if (_operatorPanel.testPanel().paused())
          setStatus(TESTING_PAUSED);
        else if (pList.empty())
          setStatus(WAITING_FOR_PANEL);

        try { Thread.sleep(2000); }
        catch (InterruptedException e) { }
      }
      PanelEntry p = pList.dequeue();
      if (p != null)
      {
        _operatorPanel.picturePanel().loadPanel(p.getPanelProgram(), p.getPicture());
        setStatus(LOADING);
        _operatorPanel.testPanel().loadPanel(p);
        _operatorPanel.picturePanel().panelLoaded();
        setStatus(TESTING);
        String result = _operatorPanel.testPanel().testPanel(p);
        p.setTestResult(result);
//        _resultsPanel.panelDone(p);
        setStatus(UNLOADING);
        _operatorPanel.testPanel().unloadPanel(p);
        setStatus(TEST_COMPLETE);
      }
    } while (!done());

    return true;
  }

  private boolean doLogout()
  {
    setStatus(LOGOUT);
    return true;
  }

  private void setStatus( StatusEnum status )
  {
/*    ImageIcon lightIcon = null;
    if (level < 0)
      lightIcon = _RED_ICON;
    else if (level > 0)
      lightIcon = _GREEN_ICON;
    else
      lightIcon = _YELLOW_ICON;
    _readyIconLbl.setIcon(lightIcon);
    String status = "<html>";
    if (msg != null)
      status = status + msg;
    _readyIconLbl.setText(status);
*/
    _readyIconLbl.setIcon(status.getIcon());
    String stat = "<html>" + status.getMessage();
    _readyIconLbl.setText(stat);
    if (status == TESTING_STOPPED)
      _readyIconLbl.setBackground(Color.red);
    else if (status == TESTING_PAUSED)
      _readyIconLbl.setBackground(Color.yellow);
    else
      _readyIconLbl.setBackground(Color.lightGray);
  }

  private void jbInit() throws Exception
  {
    _rootPanelBorder = BorderFactory.createEmptyBorder(3,3,3,3);
    _rootPanel.setLayout(_rootPanelLayout);
    _rootPanelLayout.setVgap(3);
    _rootPanel.setBackground(_specialBackground);
    _rootPanel.setBorder(_rootPanelBorder);

    /**** Menu Bar *********************************************************/
    // File menu
    _fileMenu.setMnemonic('F');
    _fileMenu.setText("File");
    _openProgramMI.setMnemonic('O');
    _openProgramMI.setText("Open Panel Program...");
    _logonMI.setMnemonic('L');
    _logonMI.setText("Logon");
    _exitMI.setMnemonic('X');
    _exitMI.setText("Exit 5DX");
    _exitMI.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        exitMI_actionPerformed(e);
      }
    });
    _helpContentMI.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        helpContentMI_actionPerformed(e);
      }
    });
    _about5dxMI.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        about5dxMI_actionPerformed(e);
      }
    });
    _fileMenu.add(_openProgramMI);
    _fileMenu.add(_logonMI);
    _fileMenu.addSeparator();
    _fileMenu.add(_exitMI);
    // System menu
    _systemMenu.setMnemonic('S');
    _systemMenu.setText("System");
    _autoStartMI.setMnemonic('U');
    _autoStartMI.setText("Startup");
    _shutdownShortMI.setMnemonic('S');
    _shutdownShortMI.setText("Short Term");
    _shutdownLongMI.setMnemonic('L');
    _shutdownLongMI.setText("Long Term");
    _shutdownCM.setMnemonic('D');
    _shutdownCM.setText("Shutdown");
    _shutdownCM.add(_shutdownShortMI);
    _shutdownCM.add(_shutdownLongMI);
    _homeRailsMI.setMnemonic('H');
    _homeRailsMI.setText("Home Rails");
    _systemMenu.add(_autoStartMI);
    _systemMenu.add(_shutdownCM);
    _systemMenu.addSeparator();
    _systemMenu.add(_homeRailsMI);
    // Panel menu
    _panelMenu.setMnemonic('P');
    _panelMenu.setText("Panel");
    _panelLoadMI.setMnemonic('L');
    _panelLoadMI.setText("Load");
    _panelUnloadMI.setMnemonic('U');
    _panelUnloadMI.setText("Unload");
    _panelMenu.add(_panelLoadMI);
    _panelMenu.add(_panelUnloadMI);
    // Test menu
    _testMenu.setMnemonic('T');
    _testMenu.setText("Test");
    _autoTestMI.setMnemonic('A');
    _autoTestMI.setText("Run Automatic Test");
    _pauseTestMI.setMnemonic('P');
    _pauseTestMI.setText("Pause Test");
    _pauseTestMI.setAccelerator(javax.swing.KeyStroke.getKeyStroke(118, 0, true));
    _pauseTestMI.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        pauseBtn_actionPerformed(e);
      }
    });
    _stopTestMI.setMnemonic('S');
    _stopTestMI.setText("Stop Test");
    _stopTestMI.setAccelerator(javax.swing.KeyStroke.getKeyStroke(119, 0, true));
    _stopTestMI.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        stopTestMI_actionPerformed(e);
      }
    });
    _testMenu.add(_autoTestMI);
    _testMenu.addSeparator();
    _testMenu.add(_pauseTestMI);
    _testMenu.add(_stopTestMI);
    // Help menu
    _helpMenu.setText("Help");
    _helpContentMI.setMnemonic('C');
    _helpContentMI.setText("Contents");
    _about5dxMI.setMnemonic('A');
    _about5dxMI.setText("About 5DX");
    _helpMenu.add(_helpContentMI);
    _helpMenu.addSeparator();
    _helpMenu.add(_about5dxMI);

    _menuBar.add(_fileMenu);
    _menuBar.add(_systemMenu);
//    _menuBar.add(_panelMenu);
    _menuBar.add(_testMenu);
    _menuBar.add(_helpMenu);
    this.setJMenuBar(_menuBar);
    /**** END Menu Bar *****************************************************/

    /**** Toolbar  *********************************************************/
    Insets toolBarBtnInsets = new Insets(1,2,1,2);
    _loadBtn.setMargin(toolBarBtnInsets);
    //ImageIcon loadIcon = new ImageIcon(proto.menuUI.Menu5DX.class.getResource("loadcad.gif"));
    //ImageIcon loadIcon = new ImageIcon("../menuUI/loadcad.gif");
    ImageIcon loadIcon = new ImageIcon("proto/menuUI/loadcad.gif");
    _loadBtn.setIcon(loadIcon);  // load panel program
    _loadBtn.setToolTipText("Open Panel Program");
    _loadBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        loadBtn_actionPerformed(e);
      }
    });
    _startupBtn.setMargin(toolBarBtnInsets);
//    ImageIcon startIcon = new ImageIcon(Menu5DX.class.getResource("startup.gif"));
    ImageIcon startIcon = new ImageIcon("proto/menuUI/startup.gif");
    _startupBtn.setIcon(startIcon);  // automatic startup
    _startupBtn.setToolTipText("Automatic Startup");
    _startupBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        startupBtn_actionPerformed(e);
      }
    });
    _pauseBtn.setMargin(toolBarBtnInsets);
    ImageIcon pauseIcon = new ImageIcon(Operator5DX.class.getResource("pause.gif"));
    ImageIcon pausedIcon = new ImageIcon(Operator5DX.class.getResource("paused.gif"));
    _pauseBtn.setIcon(pauseIcon);  // pause testing
    _pauseBtn.setSelectedIcon(pausedIcon);
    _pauseBtn.setToolTipText("Pause Testing");
    _pauseBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        pauseBtn_actionPerformed(e);
      }
    });
    _stopBtn.setMargin(toolBarBtnInsets);
    ImageIcon stopIcon = new ImageIcon(Operator5DX.class.getResource("stop.gif"));
    ImageIcon stoppedIcon = new ImageIcon(Operator5DX.class.getResource("stopped.gif"));
    _stopBtn.setIcon(stopIcon);  // stop testing
    _stopBtn.setSelectedIcon(stoppedIcon);
    _stopBtn.setToolTipText("Stop Testing");
    _stopBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        stopBtn_actionPerformed(e);
      }
    });
    _toolBar.add(_loadBtn, null);
    _toolBar.add(_startupBtn, null);
    _toolBar.addSeparator();

//    _toolBar.addSeparator();
//    _toolBar.add(_pauseBtn, null);
//    _toolBar.add(_stopBtn, null);
    _rootPanel.add(_toolBar, BorderLayout.NORTH);
    /**** END Toolbar ******************************************************/

    /**** Center Panel *****************************************************/
    _centerPanel.setLayout(borderLayout1);
    borderLayout1.setVgap(3);
//    _operatorPanel.setLayout(borderLayout2);
//    _operatorPanel.add( _testPanel, BorderLayout.CENTER );
//    _operatorPanel.add(_westPanel, BorderLayout.WEST);
//    _westPanel.setLayout(borderLayout4);
//    _westPanel.add(_serialNumPanel, BorderLayout.CENTER);
//    _westPanel.add(_picturePanel, BorderLayout.SOUTH);
    _centerPanel.add(_operatorPanel, BorderLayout.CENTER);
    _rootPanel.add(_centerPanel, BorderLayout.CENTER);
    /**** END Center Panel *************************************************/

    /**** Status Panel *****************************************************/
    _readyIconLbl.setBackground(Color.lightGray);
    _readyIconLbl.setForeground(Color.black);
    _readyIconLbl.setOpaque(true);
    setStatus(READY_5DX);
//    _readyIconLbl.setIcon(_YELLOW_ICON);
//    _readyIconLbl.setText("5DX Ready");
    _statusPanel.setLayout(borderLayout3);
    _statusPanel.setBackground(_specialBackground);
    _statusPanel.add(_readyIconLbl, BorderLayout.CENTER);
    _rootPanel.add(_statusPanel, BorderLayout.SOUTH);
    /**** END Status Panel *************************************************/

    this.getContentPane().add(_rootPanel, BorderLayout.CENTER);
  }

  void exitMI_actionPerformed(ActionEvent e)
  {
    exitOperator5DX(0);
  }

  void helpContentMI_actionPerformed(ActionEvent e)
  {
// DEBUG
    System.out.println("Get Help!");
// DEBUG
  }

  void about5dxMI_actionPerformed(ActionEvent e)
  {
// DEBUG
    System.out.println("5DX Operator interface Prototype 4");
// DEBUG
  }


  void loadBtn_actionPerformed(ActionEvent e)
  {
// DEBUG
    System.out.println("Load Panel Program");
// DEBUG
  }

  void startupBtn_actionPerformed(ActionEvent e)
  {
    doAutomaticStartup();
  }

  void pauseBtn_actionPerformed(ActionEvent e)
  {
    pauseTestMI_actionPerformed(e);
  }

  void stopBtn_actionPerformed(ActionEvent e)
  {
    stopTestMI_actionPerformed(e);
  }

  void pauseTestMI_actionPerformed(ActionEvent e)
  {
// DEBUG
    System.out.println("Pause Button: " + _pauseTestMI.isSelected());
// DEBUG
    _operatorPanel.testPanel().setPaused(_pauseTestMI.isSelected());
  }

  void stopTestMI_actionPerformed(ActionEvent e)
  {
// DEBUG
    System.out.println("Stop Button: " + _stopTestMI.isSelected());
// DEBUG
    boolean paused = _operatorPanel.testPanel().paused();
    _operatorPanel.testPanel().setStopped(_stopTestMI.isSelected());
    if (paused)
      _pauseTestMI.setSelected(false);
    _pauseTestMI.setEnabled(!_stopTestMI.isSelected());
  }

  class StatusEnum
  {
    private String _message = "";
    private ImageIcon _icon = null;

    StatusEnum( String msg, ImageIcon icon )
    {
      _message = msg;
      _icon = icon;
    }

    public String getMessage()  { return _message; }
    public ImageIcon getIcon()  { return _icon; }
  }

  public static void main(String[] args)
  {
    try
    {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch(Exception e)
    {
    }
    Operator5DX operator5DX = new Operator5DX();
    operator5DX._startFromMain = true;
    operator5DX.show();
    operator5DX.start();
  }
  private boolean _startFromMain = false;

  private JPanel _rootPanel = new JPanel();
  private BorderLayout _rootPanelLayout = new BorderLayout();
  private Border _rootPanelBorder;
  private JLabel _readyIconLbl = new JLabel();
  private JPanel _statusPanel = new JPanel();
  private BorderLayout borderLayout3 = new BorderLayout();
  private JMenuBar _menuBar = new JMenuBar();
  private JMenu _fileMenu = new JMenu();
  private JMenuItem _openProgramMI = new JMenuItem();
  private JMenuItem _exitMI = new JMenuItem();
  private JMenu _systemMenu = new JMenu();
  private JMenuItem _autoStartMI = new JMenuItem();
  private JMenu _shutdownCM = new JMenu();
  private JMenuItem _shutdownShortMI = new JMenuItem();
  private JMenuItem _shutdownLongMI = new JMenuItem();
  private JMenuItem _homeRailsMI = new JMenuItem();
  private JMenu _panelMenu = new JMenu();
  private JMenuItem _panelLoadMI = new JMenuItem();
  private JMenuItem _panelUnloadMI = new JMenuItem();
  private JMenu _testMenu = new JMenu();
  private JMenuItem _autoTestMI = new JMenuItem();
  private JMenu _helpMenu = new JMenu();
  private JMenuItem _helpContentMI = new JMenuItem();
  private JMenuItem _about5dxMI = new JMenuItem();
  private JMenuItem _logonMI = new JMenuItem();
  private JToolBar _toolBar = new JToolBar();
  private JPanel _centerPanel = new JPanel();
  private JButton _loadBtn = new JButton();
  private JButton _startupBtn = new JButton();
  private BorderLayout borderLayout1 = new BorderLayout();
//  private JPanel _textPanel = new JPanel();
  private AutoTestPanel _operatorPanel = new AutoTestPanel();
//  private BorderLayout borderLayout2 = new BorderLayout();
//  private TestPanel _testPanel = new TestPanel();
//  private JButton _stopBtn = new JButton();
//  private JButton _pauseBtn = new JButton();
  private JToggleButton _stopBtn = new JToggleButton();
  private JToggleButton _pauseBtn = new JToggleButton();
//  private JPanel _westPanel = new JPanel();
  private BorderLayout borderLayout4 = new BorderLayout();
//  private SNPanel _serialNumPanel = new SNPanel();
//  private ImagePanel _picturePanel = new ImagePanel();
  private JRadioButtonMenuItem _stopTestMI = new JRadioButtonMenuItem();
  private JRadioButtonMenuItem _pauseTestMI = new JRadioButtonMenuItem();
}
