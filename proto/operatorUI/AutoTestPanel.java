package proto.operatorUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class AutoTestPanel extends JPanel
{
  private BorderLayout borderLayout1 = new BorderLayout();
  private TestPanel _testPanel = new TestPanel();
  private SNPanel _serialNumPanel = new SNPanel();
  private JPanel _westPanel = new JPanel();
  private BorderLayout borderLayout4 = new BorderLayout();
  private ImagePanel _picturePanel = new ImagePanel();

  public AutoTestPanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    _westPanel.setLayout(borderLayout4);
    this.add(_testPanel, BorderLayout.CENTER);
    this.add(_westPanel, BorderLayout.WEST);
    _westPanel.add(_serialNumPanel, BorderLayout.NORTH);
    _westPanel.add(_picturePanel, BorderLayout.SOUTH);

    // Kludge link between serial number control buttons and test window.
    this.serialNumPanel()._stopBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        AutoTestPanel.this.testPanel().setStopped(true);
      }
    });
    this.serialNumPanel()._startBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        AutoTestPanel.this.testPanel().setStopped(false);
      }
    });
  }

  public TestPanel testPanel()      { return _testPanel; }
  public SNPanel serialNumPanel()   { return _serialNumPanel; }
  public ImagePanel picturePanel()  { return _picturePanel; }

  public void startTesting()
  {
    Thread testingThread = new Thread()
    {
      public void run()
      {
        AutoTestPanel.this.doTestPanel();
      }
    };
    testingThread.start();
  }

  private void doTestPanel()
  {
    PanelList pList = this.serialNumPanel().getPanelList();
    do {
      while (pList.empty() || this.serialNumPanel().stopped() || this.serialNumPanel().paused())
      {
//        if (this.testPanel().stopped())
//          setStatus(TESTING_STOPPED);
//        else if (this.testPanel().paused())
//          setStatus(TESTING_PAUSED);
//        else if (pList.empty())
//          setStatus(WAITING_FOR_PANEL);

        try { Thread.sleep(2000); }
        catch (InterruptedException e) { }
      }
      PanelEntry p = pList.dequeue();
      if (p != null)
      {
        this.picturePanel().loadPanel(p.getPanelProgram(), p.getPicture());
        this.testPanel().loadPanel(p);
        this.picturePanel().panelLoaded();
        String result = this.testPanel().testPanel(p);
        p.setTestResult(result);
        this.testPanel().unloadPanel(p);
      }
    } while (true);
  }

}
