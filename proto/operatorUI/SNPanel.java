package proto.operatorUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
//import com.borland.jbcl.layout.*;

/**
 * Title:        New 5DX GUI
 * Description:  This project contains the files created for the
 * new 5DX user interface.
 * Copyright:    Copyright (c) Steve Anonson
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class SNPanel extends JPanel
{
  private ImageIcon _boardIcon = new ImageIcon(Operator5DX.class.getResource("board1.jpg"));
//  private ImageIcon _noboardIcon = new ImageIcon(Operator5DX.class.getResource("nopanel.jpg"));
  private ImageIcon _noboardIcon = new ImageIcon(Operator5DX.class.getResource("noboard.gif"));
  private ImageIcon _startIcon = new ImageIcon(Operator5DX.class.getResource("start.gif"));
  private ImageIcon _stopIcon = new ImageIcon(Operator5DX.class.getResource("stop.gif"));
  private ImageIcon _pauseIcon = new ImageIcon(Operator5DX.class.getResource("pause.gif"));
  private final String _TESTING_STOPPED = "TESTING STOPPED";
  private final String _TESTING_PAUSED = "TESTING PAUSED";
  private Color WHEAT = new Color(243, 218, 169);
  private boolean _stopped = false;
  private boolean _paused = false;

  public SNPanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception
  {
    border3 = BorderFactory.createEmptyBorder(0,2,0,2);
    borderLayout2.setHgap(5);
    _snTF.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _snTF_actionPerformed(e);
      }
    });
    _snTF.setColumns(10);
    _snTF.setMargin(new Insets(0, 3, 0, 3));
    _snTF.setFocusAccelerator('s');
    _northPanel.setLayout(borderLayout2);
    border1 = BorderFactory.createEmptyBorder();
    border2 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Serial Numbers "),BorderFactory.createEmptyBorder(0,2,0,2));
    this.setLayout(borderLayout1);
    this.setBorder(border2);
    ActionListener removeAction = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _removeBtn_actionPerformed(e);
      }
    };
    ActionListener removeAllAction = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _removeAllBtn_actionPerformed(e);
      }
    };
    _centerPanel.setLayout(borderLayout3);
    _panelList.setBackground(SystemColor.info);
    _panelList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _panelList.setVisibleRowCount(4);
    _panelList.addFocusListener(new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        _panelList_focusGained(e);
      }
    });
    _panelList.registerKeyboardAction(removeAction, KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0, true), WHEN_IN_FOCUSED_WINDOW);

    _removeMI.setText("Remove");
    _removeMI.addActionListener(removeAction);
    _removeAllMI.setText("Remove All");
    _removeAllMI.addActionListener(removeAllAction);
    _controlPanel.setLayout(borderLayout5);
//    _startBtn.setText("Start");
    _startBtn.setToolTipText("Start");
    _startBtn.setIcon(_startIcon);
    _startBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _startBtn_actionPerformed(e);
      }
    });
//    _pauseBtn.setText("Pause");
    _pauseBtn.setToolTipText("Pause");
    _pauseBtn.setIcon(_pauseIcon);
    _pauseBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _pauseBtn_actionPerformed(e);
      }
    });
//    _stopBtn.setText("Stop");
    _stopBtn.setToolTipText("Stop");
    _stopBtn.setIcon(_stopIcon);
    _stopBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _stopBtn_actionPerformed(e);
      }
    });
    _buttonPanel.setLayout(gridLayout1);
    gridLayout1.setColumns(3);
    ButtonGroup btnGroup = new ButtonGroup();
    jScrollPane1.setBorder(border3);
    borderLayout3.setVgap(2);
    _statusLbl.setFont(new java.awt.Font("Dialog", 1, 12));
    _statusLbl.setText("  ");
    this.add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.add(jScrollPane1, BorderLayout.CENTER);
    _centerPanel.add(_northPanel, BorderLayout.NORTH);
    _northPanel.add(_snTF, BorderLayout.CENTER);
    this.add(_controlPanel, BorderLayout.SOUTH);
    _controlPanel.add(_buttonPanel, BorderLayout.SOUTH);
    _buttonPanel.add(_startBtn, null);
    _buttonPanel.add(_pauseBtn, null);
    _buttonPanel.add(_stopBtn, null);
    _controlPanel.add(_statusPanel, BorderLayout.CENTER);
    _statusPanel.add(_statusLbl, null);
    jScrollPane1.getViewport().add(_panelList, null);
    _listPopupMenu.add(_removeMI);
    _listPopupMenu.add(_removeAllMI);
    _panelList.addMouseListener( new PopupTrigger(_listPopupMenu) );
  }

  public boolean stopped()  { return _stopped; }
  public boolean paused()   { return _paused; }

  void setStopped( boolean flag )
  {
    if (_stopped == flag)
      return;
    _stopped = flag;
    if (_stopped)
    {
      this.setPaused(false);
      this._statusLbl.setText(_TESTING_STOPPED);
    }
  }

  void setPaused( boolean flag )
  {
    if (_paused == flag)
      return;
    _paused = flag;
    if (_paused && !_stopped)
      this._statusLbl.setText(_TESTING_PAUSED);
  }

  public PanelList getPanelList()
  {
    return _panelList;
  }

  void _snTF_actionPerformed(ActionEvent e)
  {
    String sn = _snTF.getText();
    if (sn.equals("5"))
      _panelList.enqueue( new PanelEntry( sn, "Dilbert", _noboardIcon) );
    else
      _panelList.enqueue( new PanelEntry( sn, "NEPCON", _boardIcon) );
    _snTF.setText("");
  }

  void _removeBtn_actionPerformed(ActionEvent e)
  {
    _panelList.removeSelected();
  }

  void _removeAllBtn_actionPerformed(ActionEvent e)
  {
    _panelList.removeAll();
  }

  void _panelList_focusGained(FocusEvent e)
  {
    if (_panelList.getSelectedIndex() == -1 && _panelList.count() > 0)
      _panelList.setSelectedIndex(_panelList.count() - 1);
  }

  void _autoTestTBtn_actionPerformed(ActionEvent e)
  {
//    if (_autoTestTBtn.isSelected())
//      jScrollPane1.setVisible(true);
//      _panelList.setVisible(true);
//    _panelList.setVisibleRowCount( _panelList.getVisibleRowCount() + 1 );
//    this.invalidate();
//    this.doLayout();
//    this.validate();
  }

  void _panelTestTBtn_actionPerformed(ActionEvent e)
  {
//    if (_panelTestTBtn.isSelected())
//      jScrollPane1.setVisible(false);
//      _panelList.setVisible(false);
//    this.invalidate();
//    this.validate();
  }

  void _startBtn_actionPerformed(ActionEvent e)
  {
    this.setPaused(false);
    this.setStopped(false);
    this._statusLbl.setText("  ");  // clear the status label
  }

  void _pauseBtn_actionPerformed(ActionEvent e)
  {
    this.setPaused(true);
  }

  void _stopBtn_actionPerformed(ActionEvent e)
  {
    this.setStopped(true);
  }

  public static void main(String[] args)
  {
/*    for (java.util.Enumeration e = System.getProperties().propertyNames() ; e.hasMoreElements() ;)
    {
      System.out.println(e.nextElement());
    }
*/
    JFrame f = new JFrame("Serial Number Panel Test");
    f.addWindowListener( new WindowAdapter()
    { public void windowClosing(WindowEvent e) { System.exit(0); } });
    SNPanel p = new SNPanel();
    p._startFromMain = true;
    f.getContentPane().add(p);
    f.pack();
    f.show();
  }
  private boolean _startFromMain = false;

  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private Border border1;
  private Border border2;
  private BorderLayout borderLayout3 = new BorderLayout();
  private JScrollPane jScrollPane1 = new JScrollPane();
  private PanelList _panelList = new PanelList();
  private JPanel _northPanel = new JPanel();
  private JTextField _snTF = new JTextField();
  private BorderLayout borderLayout2 = new BorderLayout();
  private JPopupMenu _listPopupMenu = new JPopupMenu();
  private JMenuItem _removeMI = new JMenuItem();
  private JMenuItem _removeAllMI = new JMenuItem();
  private JPanel _controlPanel = new JPanel();
  private BorderLayout borderLayout5 = new BorderLayout();
  private JPanel _buttonPanel = new JPanel();
  public JButton _startBtn = new JButton();
  public JButton _pauseBtn = new JButton();
  public JButton _stopBtn = new JButton();
  private GridLayout gridLayout1 = new GridLayout();
  private Border border3;
  private JPanel _statusPanel = new JPanel();
  private JLabel _statusLbl = new JLabel();


}
