package proto.operatorUI;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;


public class JPieChart2D extends JChart2D
{
  public JPieChart2D(int nData, int[] sliceData, String text)
  {
    this(nData, sliceData, null, text);
  }

  public JPieChart2D(int nData, int[] sliceData, Color[] colors, String text)
  {
    super(nData, null, sliceData, colors, text);
  }

  protected ChartPanel createChartPanel()
  {
    return new PieChartPanel();
  }


  class PieChartPanel extends ChartPanel
  {
    PieChartPanel()
    {
    }

    public void paintComponent(Graphics g)
    {
      int x0 = 0;
      int y0 = 0;
      int _pieGap = _gap;

      Graphics2D g2 = (Graphics2D) g;
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_RENDERING,
        RenderingHints.VALUE_RENDER_QUALITY);

      if (_stroke != null)
        g2.setStroke(_stroke);

      GeneralPath path = new GeneralPath();
      Shape[] pieces = new Shape[_pieData.length];
      double start = 0.0;
      double finish = 0.0;
      int ww = _w - 2 * _pieGap;
      int hh = _h - 2 * _pieGap;
      if (_drawShadow)
      {
        ww -= _pieGap;
        hh -= _pieGap;
      }

      for (int k = 0; k < _pieData.length; k++)
      {
        finish = start + _pieData[k];
        double f1 = Math.min(90 - start, 90 - finish);
        double f2 = Math.max(90 - start, 90 - finish);
        Shape shp = new Arc2D.Double(_x, _y, ww, hh, f1, f2-f1, Arc2D.PIE);
        double f = (f1 + f2)/2*Math.PI/180;
        AffineTransform s1 = AffineTransform.getTranslateInstance(_pieGap*Math.cos(f), -_pieGap*Math.sin(f));
        s1.translate(_pieGap, _pieGap);
        Shape piece = s1.createTransformedShape(shp);
        pieces[k] = piece;
        path.append(piece, false);
        start = finish;
      }

      if (_drawShadow)
      {
        AffineTransform s0 = AffineTransform. getTranslateInstance(_pieGap, _pieGap);
        g2.setColor(Color.gray);
        Shape shadow = s0.createTransformedShape(path);
        g2.fill(shadow);
      }

      if (_effectIndex==EFFECT_GRADIENT && _gradient != null)
      {
        g2.setPaint(_gradient);
        g2.fill(path);
      }
      else if (_effectIndex==EFFECT_IMAGE && _foregroundImage != null)
        fillByImage(g2, path,0);
      else
      {
//        g2.setColor(_columnColor);
//        g2.fill(path);
        for (int k = 0; k < pieces.length; k++)
        {
          g2.setColor(_fillColor[k]);
          g2.fill(pieces[k]);
        }
      }

      g2.setColor(_lineColor);
      g2.draw(path);
    }
  }
}
