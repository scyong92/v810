package proto.operatorUI;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;


public class JChart2D extends JPanel
{
  public static final int EFFECT_PLAIN = 0;
  public static final int EFFECT_GRADIENT = 1;
  public static final int EFFECT_IMAGE = 2;

  protected JLabel _title;
  protected ChartPanel _chart;

  protected int _nData;     // number of data points
  protected int[] _xData;   // x axis points (usually from user)
  protected int[] _yData;   // y axis points from user
  protected int _xMin;      // minimum x value in x axis points
  protected int _xMax;      // maximum x value in x axis points
  protected int _yMin;      // minimum y value in y axis points
  protected int _yMax;      // maximum y value in y axis points
  protected double[] _pieData;  // pie chart data normalized to 360 degrees

  protected Stroke _stroke; // line style
  protected Color  _lineColor = Color.black;  // line color
  protected int    _columnWidth = -1; // column width (bar chart only)
  protected int    _gap = 10; // space between segments
  // the following apply to filled charts
  protected Color[]  _fillColor = null; // column color
  protected int    _effectIndex = EFFECT_PLAIN; // drawing effect
  protected GradientPaint _gradient;  // paint gradient
  protected Image  _foregroundImage;  // image to fill with
  protected boolean _drawShadow = false;  // draw shadow flag


  protected JChart2D(int nData, int[] xData, int[] yData, Color[] colors, String text)
  {
    super(new BorderLayout());
    setBackground(Color.white);
    _title = new JLabel(text, JLabel.CENTER);
    add(_title, BorderLayout.NORTH);

    setValues( nData, xData, yData, colors );

    _chart = createChartPanel();
    add(_chart, BorderLayout.CENTER);
  }

  protected ChartPanel createChartPanel()
  {
    return new ChartPanel();
  }

  public void setValues(int nData, int[] xData, int[] yData, Color[] colors)
  {
    if (xData == null)
    {
      xData = new int[nData];
      for (int k = 0; k < nData; k++)
        xData[k] = k;
    }
    if (yData == null)
      throw new IllegalArgumentException("yData can't be null");
    if (nData > yData.length)
      throw new IllegalArgumentException("Insufficient yData length");
    if (nData > xData.length)
      throw new IllegalArgumentException("Insufficient xData length");
    _nData = nData;
    _xData = xData;
    _yData = yData;

    _xMin = _xMax = 0;	// To include 0 into the interval
    _yMin = _yMax = 0;
    for (int k = 0; k < _nData; k++)
    {
      _xMin = Math.min(_xMin, _xData[k]);
      _xMax = Math.max(_xMax, _xData[k]);
      _yMin = Math.min(_yMin, _yData[k]);
      _yMax = Math.max(_yMax, _yData[k]);
    }
    if (_xMin == _xMax)
      _xMax++;
    if (_yMin == _yMax)
      _yMax++;


    double sum = 0;
    for (int k = 0; k < _nData; k++)
    {
      _yData[k] = Math.max(_yData[k], 0);
      sum += _yData[k];
    }
    _pieData = new double[_nData];
    double nomalizer = ((sum != 0) ? 360.0/sum : 0.0);
    for (int k = 0; k < _nData; k++)
      _pieData[k] = _yData[k] * nomalizer;

    _fillColor = new Color[_nData];
    for (int i = 0; i < _fillColor.length; i++)
    {
      if (colors != null && i < colors.length && colors[i] != null)
        _fillColor[i] = colors[i];
      else
        _fillColor[i] = Color.blue;
    }
  }

  public void updateValues(int nData, int[] xData, int[] yData, Color[] colors)
  {
    setValues(nData, xData, yData, colors);
    repaint();
  }

  public void setEffectIndex(int effectIndex)
  {
    _effectIndex = effectIndex;
    repaint();
  }

  public int getEffectIndex()
  {
    return _effectIndex;
  }

  public void setStroke(Stroke stroke)
  {
    _stroke = stroke;
    _chart.repaint();
  }

  public void setForegroundImage(Image img)
  {
    _foregroundImage = img;
    repaint();
  }

  public Image getForegroundImage()
  {
    return _foregroundImage;
  }

  public Stroke getStroke()
  {
    return _stroke;
  }

  public void setGradient(GradientPaint gradient)
  {
    _gradient = gradient;
    repaint();
  }

  public GradientPaint getGradient()
  {
    return _gradient;
  }

  public void setColumnWidth(int columnWidth)
  {
    _columnWidth = columnWidth;
    _chart.calcDimensions();
    _chart.repaint();
  }

  public int getColumnWidth()
  {
    return _columnWidth;
  }

  public void setGap(int gap)
  {
    _gap = gap;
    _chart.calcDimensions();
    _chart.repaint();
  }

  public int getGap()
  {
    return _gap;
  }

  public void setFillColor(int index, Color c)
  {
    if (index > 0 && index < _fillColor.length)
      _fillColor[index] = c;
    _chart.repaint();
  }

  public void setFillColor(Color c)
  {
    for (int i = 0; i < _fillColor.length; i++)
      _fillColor[i] = c;
    _chart.repaint();
  }

  public Color[] getFillColor()
  {
    return _fillColor;
  }

  public void setLineColor(Color c)
  {
    _lineColor = c;
    _chart.repaint();
  }

  public Color getLineColor()
  {
    return _lineColor;
  }

  public void setDrawShadow(boolean drawShadow)
  {
    _drawShadow = drawShadow;
    _chart.repaint();
  }

  public boolean getDrawShadow()
  {
    return _drawShadow;
  }


  protected class ChartPanel extends JComponent
  {
    int _xMargin = 5;
    int _yMargin = 5;
    int _x;
    int _y;
    int _w;
    int _h;

    ChartPanel()
    {
      super.enableEvents(ComponentEvent.COMPONENT_RESIZED);
    }

    protected void processComponentEvent(ComponentEvent e)
    {
      calcDimensions();
    }

    public void calcDimensions()
    {
      Dimension d = super.getSize();
      _x = _xMargin;
      _y = _yMargin;
      _w = d.width - 2 * _xMargin;
      _h = d.height - 2 * _yMargin;
      if (_nData > 0)
      {
        _columnWidth = _w - ((_nData - 1) * _gap);
        _columnWidth /= _nData;
      }
    }

    public int xChartToScreen(int x)
    {
      return _x + (x-_xMin)*_w/(_xMax-_xMin);
    }

    public int yChartToScreen(int y)
    {
      return _y + (_yMax-y)*_h/(_yMax-_yMin);
    }

    public void paintComponent(Graphics g)
    {
      // This method must be overridden.
    }

    protected void fillByImage(Graphics2D g2, Shape shape, int xOffset)
    {
      if (_foregroundImage == null)
        return;
      int wImg = _foregroundImage.getWidth(this);
      int hImg = _foregroundImage.getHeight(this);
      if (wImg <= 0 || hImg <= 0)
        return;
      g2.setClip(shape);
      Rectangle bounds = shape.getBounds();
      for (int xx = bounds.x + xOffset; xx < bounds.x + bounds.width; xx += wImg)
        for (int yy = bounds.y; yy < bounds.y+bounds.height; yy += hImg)
          g2.drawImage(_foregroundImage, xx, yy, this);
    }
  }

  public static void main(String argv[])
  {
    JFrame f = new JFrame("2D Charts");
    f.setSize(720, 280);
    f.getContentPane().setLayout(new GridLayout(1, 3, 10, 0));
    f.getContentPane().setBackground(Color.white);
    ImageIcon icon = new ImageIcon("D:/users/sanon/java/swing/Chapter23/1/hubble.gif");

    int nData = 8;
    int[] xData = new int[nData];
    int[] yData = new int[nData];
    for (int k=0; k<nData; k++)
    {
      xData[k] = k;
      yData[k] = (int)(Math.random()*100);
      if (k > 0)
        yData[k] = (yData[k-1] + yData[k])/2;
    }

//    JChart2D chart = new JChart2D(nData, xData, yData, null, "Line Chart");
    JChart2D chart = new JLineChart2D(nData, xData, yData, "Line Chart");
    chart.setStroke(new BasicStroke(5f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER));
    chart.setLineColor(new Color(0, 128, 128));
    f.getContentPane().add(chart);

    Color[] colors = new Color[nData];
    for (int i = 0; i < colors.length; i++)
    {
      if ((i % 3) == 0)
        colors[i] = Color.blue;
      else if ((i % 3) == 1)
        colors[i] = Color.red;
      if ((i % 3) == 2)
        colors[i] = Color.green;
    }
    chart = new JBarChart2D(nData, xData, yData, colors, "Column Chart");
//    GradientPaint gp = new GradientPaint(0, 100, Color.white, 0, 300, Color.blue, true);
//    chart.setGradient(gp);
//    chart.setEffectIndex(JChart2D.EFFECT_GRADIENT);
//    chart.setForegroundImage(icon.getImage());
//    chart.setEffectIndex(JChart2D.EFFECT_IMAGE);
//    chart.setGap(0);
    chart.setDrawShadow(true);
    f.getContentPane().add(chart);

    colors[0] = Color.yellow;
    colors[1] = Color.pink;
    colors[2] = Color.gray;
    chart = new JPieChart2D(nData, yData, colors, "Pie Chart");
    chart.setForegroundImage(icon.getImage());
    chart.setEffectIndex(JChart2D.EFFECT_IMAGE);
//    chart.setDrawShadow(true);
//    chart.setGap(0);
    f.getContentPane().add(chart);

    WindowListener wndCloser = new WindowAdapter()
    {
      public void windowClosing(WindowEvent e)
      {
        System.exit(0);
      }
    };
    f.addWindowListener(wndCloser);
    f.setVisible(true);
  }

}
