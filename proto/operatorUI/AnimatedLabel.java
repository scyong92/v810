package proto.operatorUI;

import java.awt.*;
import javax.swing.*;

public class AnimatedLabel extends JLabel implements Runnable
{
  protected Icon[] _icons = null;
  protected int _index = 0;
  protected boolean _isRunning = false;

  public AnimatedLabel(String gifName, int numGifs)
  {
    _icons = new Icon[ numGifs ];
    for (int i = 0; i < numGifs; i++)
//      _icons[i] = new ImageIcon( gifName + i + ".gif" );
      _icons[i] = new ImageIcon(proto.operatorUI.Operator5DX.class.getResource(gifName + i + ".gif"));
    setIcon(_icons[0]);

    Thread tr = new Thread(this);
    tr.setPriority(Thread.MAX_PRIORITY);
    tr.start();
  }

  public void setRunning( boolean isRunning )
  {
    _isRunning = isRunning;
  }

  public boolean getRunning()
  {
    return _isRunning;
  }

  public void run()
  {
    while (true)
    {
      if (_isRunning)
      {
        _index++;
        if (_index >= _icons.length)
          _index = 0;
        setIcon(_icons[_index]);
        Graphics g = getGraphics();
        _icons[_index].paintIcon(this, g, 0, 0);
      }
      else
      {
        if (_index > 0)
        {
          _index = 0;
          setIcon(_icons[0]);
        }
      }
      try { Thread.sleep(400); } catch (Exception e) { }
    }
  }
}
