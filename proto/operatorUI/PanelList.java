package proto.operatorUI;

import javax.swing.*;
import java.awt.event.*;

/**
 * Title:        New 5DX GUI
 * Description:  This project contains the files created for the
 * new 5DX user interface.
 * Copyright:    Copyright (c) Steve Anonson
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class PanelList extends JList
{
  DefaultListModel _data = new DefaultListModel();

  public PanelList()
  {
    this.setModel(_data);
  }

  public boolean empty()
  {
    return ((_data != null) ? (_data.size() == 0) : true);
  }

  public int count()
  {
    return ((_data != null) ? _data.size() : 0);
  }

  public PanelEntry enqueue( PanelEntry item )
  {
    if (_data != null)
    {
//      _data.add(0, item);
      _data.addElement(item);
      ensureIndexIsVisible( _data.size() - 1 );
    }
    return item;
  }

  public synchronized PanelEntry dequeue()
  {
    PanelEntry p = null;
    if (_data != null && _data.size() > 0)
    {
//      p = (PanelEntry)_data.remove(_data.size() - 1);
      p = (PanelEntry)_data.remove(0);
    }
    return p;
  }

  public synchronized void removeAll()
  {
    _data.clear();
  }

  public synchronized void removeSelected()
  {
    int selected = this.getSelectedIndex();
    if (selected >= 0)
      _data.removeElementAt(selected);
  }

  public Object[] toArray()
  {
    if (_data != null)
      return _data.toArray();
    return null;
  }


}
