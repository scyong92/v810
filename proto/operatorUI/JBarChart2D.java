package proto.operatorUI;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;

public class JBarChart2D extends JChart2D
{
  public JBarChart2D(int nData, int[] xData, int[] yData, String text)
  {
    super(nData, xData, yData, null, text);
  }

  public JBarChart2D(int nData, int[] xData, int[] yData, Color[] colors, String text)
  {
    super(nData, xData, yData, colors, text);
  }

  protected ChartPanel createChartPanel()
  {
    return new BarChartPanel();
  }


  class BarChartPanel extends ChartPanel
  {
    BarChartPanel()
    {
    }

    public void paintComponent(Graphics g)
    {
      int x0 = 0;
      int y0 = 0;

      g.setColor(Color.black);
      x0 = xChartToScreen(0);
      g.drawLine(x0, _y, x0, _y+_h);
      y0 = yChartToScreen(0);
      g.drawLine(_x, y0, _x+_w, y0);

      Graphics2D g2 = (Graphics2D) g;
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_RENDERING,
        RenderingHints.VALUE_RENDER_QUALITY);

      if (_stroke != null)
        g2.setStroke(_stroke);

      GeneralPath path = new GeneralPath();
      Shape[] columns = new Shape[_nData];
      for (int k = 0; k < _nData; k++)
      {
        _xMax++;
        int x = xChartToScreen(_xData[k]);
        int w = _columnWidth;
        int y1 = yChartToScreen(_yData[k]);
        int y = Math.min(y0, y1);
        int h = Math.abs(y1 - y0);
        Shape rc = new Rectangle2D.Double(x, y, w, h);
        columns[k] = rc;
        path.append(rc, false);
        _xMax --;
      }

      if (_drawShadow)
      {
        AffineTransform s0 = new AffineTransform(1.0, 0.0, 0.0, -1.0, x0, y0);
        s0.concatenate(AffineTransform.getScaleInstance(1.0, 0.5));
        s0.concatenate(AffineTransform.getShearInstance(0.5, 0.0));
        s0.concatenate(new AffineTransform(1.0, 0.0, 0.0, -1.0, -x0, y0));
        g2.setColor(Color.gray);
        Shape shadow = s0.createTransformedShape(path);
        g2.fill(shadow);
      }

      if (_effectIndex==EFFECT_GRADIENT && _gradient != null)
      {
        g2.setPaint(_gradient);
        g2.fill(path);
      }
      else if (_effectIndex==EFFECT_IMAGE && _foregroundImage != null)
        fillByImage(g2, path,0);
      else
      {
//        g2.setColor(_columnColor);
//        g2.fill(path);
        for (int k = 0; k < columns.length; k++)
        {
          g2.setColor(_fillColor[k]);
          g2.fill(columns[k]);
        }
      }
      g2.setColor(_lineColor);
      g2.draw(path);
    }
  } // BarChartPanel
}
