package proto.operatorUI;

import javax.swing.JTextField;
import javax.swing.text.Document;


public class JReadOnlyTextField extends JTextField
{
  public JReadOnlyTextField()
  {
    this(null, null, 0);
  }

  public JReadOnlyTextField(String text)
  {
    this(null, text, 0);
  }

  public JReadOnlyTextField(int columns)
  {
    this(null, null, columns);
  }

  public JReadOnlyTextField(String text, int columns)
  {
    this(null, text, columns);
  }

  public JReadOnlyTextField(Document doc, String text, int columns)
  {
    super(doc, text, columns);
    this.setEditable(false);
  }

  /**
   * Override this method means that the text field cannont recieve focus.
   */
  public boolean isFocusTraversable()
  {
    return false;
  }
}
