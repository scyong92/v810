package com.agilent.xRayTest.gui.programBackupTool;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.filechooser.FileFilter;

import com.agilent.xRayTest.business.*;
import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.gui.mainMenu.*;
import com.agilent.xRayTest.gui.*;
import com.agilent.xRayTest.util.*;
import com.agilent.guiUtil.*;
import com.agilent.util.*;


/**
 * Backup NDF, RTF, and the TestLink files of a panel program.
 * @author Vincent Wong
 */
public class ProgramBackupToolGUI extends JDialog
{
  static String _appName = StringLocalizer.keyToString("PBT_GUI_NAME_KEY");
  private static final String _assertLogFile = "ProgramBackupToolGUI";
  private static final String _zipFileFilterDescription = StringLocalizer.keyToString("PBT_ZIP_FILE_DESCRIPTION_KEY");

  // flag indicating that we are ready to convert the database
  private volatile boolean _backupFileValidated = false;
  private volatile boolean _doneConverting = false;

  // gui components
  private JPanel _mainPanel = new JPanel();
  private GridLayout _mainPanelLayout = new GridLayout();
  private JPanel _programNamePanel = new JPanel();
  private FlowLayout _programNamePanelLayout = new FlowLayout();
  private JLabel _programNameLabel = new JLabel();
  private JLabel _panelNameLabel = new JLabel();
  private JPanel _selectionPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JButton _backupButton = new JButton();
  private JButton _exitButton = new JButton();
  private FlowLayout _buttonPanelLayout = new FlowLayout();
  private JPanel _backupFilePanel = new JPanel();
  private GridLayout _selectionPanelLayout = new GridLayout();
  private JLabel _backupFileLabel = new JLabel();
  private JTextField _backupFileTextField = new JTextField();
  private JButton _backupFileBrowseButton = new JButton();
  private FlowLayout _backupFilePanelLayout = new FlowLayout();
  private JFileChooser _fileChooser = null;
  private MainMenuFrame _owner = null;
  private String _panelName = null;

  /**
   * @author Vincent Wong
   */
  static
  {
    try
    {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch(Exception e)
    {
      Assert.logException(e);
    }
  }

  /**
   * @author Vincent Wong
   */
  public ProgramBackupToolGUI( Frame owner, String panelName, boolean modal )
  {
    AssertUtil.setUpAssert(_assertLogFile, null);

    _owner = (MainMenuFrame)owner;
    _panelName = panelName;

    try
    {
      jbInit();
    }
    catch(RuntimeException e)
    {
      Assert.logException(e);
    }

    java.awt.Image image = com.agilent.xRayTest.images.Image5DX.getImage(
                           com.agilent.xRayTest.images.Image5DX.FRAME_5DX );

    this.setTitle(_appName);
    this.pack();

    // Center the window
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = getSize();

    if ( frameSize.height > screenSize.height )
    {
      frameSize.height = screenSize.height;
    }

    if ( frameSize.width > screenSize.width )
    {
      frameSize.width = screenSize.width;
    }

    this.setLocation( (screenSize.width - frameSize.width)/2,
                      (screenSize.height - frameSize.height)/2 );

    this.toFront();
    this.setVisible( true );
  }

  private void jbInit()
  {
    _mainPanel.setLayout(_mainPanelLayout);
    _mainPanelLayout.setRows(3);
    _mainPanelLayout.setColumns(1);
    _mainPanelLayout.setHgap(5);
    _programNamePanel.setLayout(_programNamePanelLayout);
    _selectionPanel.setLayout(_selectionPanelLayout);
    _backupButton.setNextFocusableComponent(_exitButton);
    _backupButton.setActionCommand("backupButton");
    _backupButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _backupButton.setText(StringLocalizer.keyToString("GUI_BACKUP_BUTTON_KEY"));
    _backupButton.setMnemonic((StringLocalizer.keyToString("GUI_BACKUP_BUTTON_MNEMONIC_KEY")).charAt(0));
    _backupButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        backupButton_actionPerformed(e);
      }
    });
    _exitButton.setNextFocusableComponent(_backupFileTextField);
    _exitButton.setMaximumSize(new Dimension(79, 27));
    _exitButton.setMinimumSize(new Dimension(79, 27));
    _exitButton.setPreferredSize(new Dimension(79, 27));
    _exitButton.setActionCommand("exitButton");
    _exitButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _exitButton.setText(StringLocalizer.keyToString("GUI_EXIT_BUTTON_KEY"));
    _exitButton.setMnemonic((StringLocalizer.keyToString("GUI_EXIT_BUTTON_MNEMONIC_KEY")).charAt(0));
    _exitButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        exitButton_actionPerformed(e);
      }
    });
    _buttonPanel.setLayout(_buttonPanelLayout);
    _backupFileLabel.setMaximumSize(new Dimension(80, 17));
    _backupFileLabel.setMinimumSize(new Dimension(80, 17));
    _backupFileLabel.setNextFocusableComponent(_backupFileTextField);
    _backupFileLabel.setPreferredSize(new Dimension(80, 17));
    _backupFileLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _backupFileLabel.setHorizontalTextPosition(SwingConstants.LEFT);
    _backupFileLabel.setLabelFor(_backupFileTextField);
    _backupFileLabel.setText(StringLocalizer.keyToString("PBTGUI_BACKUP_FILE_LABEL_KEY"));
    _backupFileTextField.setNextFocusableComponent(_backupFileBrowseButton);
    _backupFileTextField.setPreferredSize(new Dimension(240, 21));
    _backupFileTextField.setColumns(20);
    _backupFileTextField.setHorizontalAlignment(SwingConstants.LEFT);
    _backupFileTextField.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyPressed(KeyEvent e)
      {
        backupFileTextField_keyPressed(e);
      }
    });
    _backupFileBrowseButton.setMaximumSize(new Dimension(85, 21));
    _backupFileBrowseButton.setMinimumSize(new Dimension(85, 21));
    _backupFileBrowseButton.setNextFocusableComponent(_backupButton);
    _backupFileBrowseButton.setPreferredSize(new Dimension(85, 21));
    _backupFileBrowseButton.setActionCommand("backupFileBrowseButton");
    _backupFileBrowseButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _backupFileBrowseButton.setText(StringLocalizer.keyToString("GUI_BROWSE_BUTTON_KEY"));
    _backupFileBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        backupFileBrowseButton_actionPerformed(e);
      }
    });
    _backupFilePanel.setLayout(_backupFilePanelLayout);

    _programNameLabel.setMaximumSize(new Dimension(80, 17));
    _programNameLabel.setMinimumSize(new Dimension(80, 17));
    _programNameLabel.setNextFocusableComponent(_panelNameLabel);
    _programNameLabel.setPreferredSize(new Dimension(80, 17));
    _programNameLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _programNameLabel.setHorizontalTextPosition(SwingConstants.LEFT);
    _programNameLabel.setLabelFor(_panelNameLabel);
    _programNameLabel.setText(StringLocalizer.keyToString("PBTGUI_BACKUP_PROGRAM_LABEL_KEY"));

    _panelNameLabel.setText(_panelName);

    this.setResizable(false);
    _programNamePanel.setMinimumSize(new Dimension(95, 16));
    _programNamePanel.setPreferredSize(new Dimension(405, 16));
    _programNamePanelLayout.setAlignment(FlowLayout.LEFT);
    _programNamePanelLayout.setVgap(10);
    _backupFilePanelLayout.setVgap(0);
    this.getContentPane().add(_mainPanel, BorderLayout.CENTER);
    _mainPanel.add(_programNamePanel, null);
    _programNamePanel.add(_programNameLabel, null);
    _programNamePanel.add(_panelNameLabel, null);
    _mainPanel.add(_selectionPanel, null);
    _selectionPanel.add(_backupFilePanel, null);
    _backupFilePanel.add(_backupFileLabel, null);
    _backupFilePanel.add(_backupFileTextField, null);
    _backupFilePanel.add(_backupFileBrowseButton, null);
    _mainPanel.add(_buttonPanel, null);
    _buttonPanel.add(_backupButton, null);
    _buttonPanel.add(_exitButton, null);
    this.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyPressed(KeyEvent e)
      {
        escapeKeyPressed(e);
      }
    });
  }

  /**
   * @author Vincent Wong
   */
  void backupFileBrowseButton_actionPerformed(ActionEvent e)
  {
    String fileNameFullPath = selectFile();
    boolean fileValidated = validateTextField(fileNameFullPath);

    if ( fileValidated == false )
    {
      _backupFileValidated = false;
    }
    else
    {
      _backupFileValidated = true;
      _backupFileTextField.setText(fileNameFullPath);

      _fileChooser.setCurrentDirectory(new File(fileNameFullPath.substring(0,fileNameFullPath.lastIndexOf(File.separator))));
    }
  }

  /**
   * The backup procedure goes here.
   * @author Vincent Wong
   */
  void backupButton_actionPerformed(ActionEvent e)
  {
    JOptionPane.showMessageDialog( this,
      "This function has not been implemented yet.",
      _appName, JOptionPane.INFORMATION_MESSAGE );
  }

  /**
   * When the exit button is pressed the Program Backup Tool will exit.
   * @author Vincent Wong
   */
  void exitButton_actionPerformed(ActionEvent e)
  {
    this.setVisible(false);
    _owner.deactivateJavaGUI(); // return control to the native code
    dispose();
  }

  /**
   * This method displays a file chooser and return the file
   * selected by the user.
   * @author Vincent Wong
   */
  private String selectFile()
  {
    if ( _fileChooser == null )
    {
       _fileChooser = new JFileChooser();

       // initialize the JFIle Dialog.
       FileFilter fileFilter = _fileChooser.getAcceptAllFileFilter();
       _fileChooser.removeChoosableFileFilter(fileFilter);

       // create and instance of Zip File Filter
       FileFilterUtil zipFileFilter = new FileFilterUtil(
         FileName.getZipFileExtension(), _zipFileFilterDescription);

       _fileChooser.setFileFilter(zipFileFilter);

      // set the default directory to the system temp directory.
      _fileChooser.setCurrentDirectory( new File(Directory.getTempDir()) );
    }

    int result = _fileChooser.showDialog(this,
                 StringLocalizer.keyToString("GUI_SELECT_KEY"));
    File selectedFile = _fileChooser.getSelectedFile();
    String file = "";

    if ( selectedFile == null )
      return file;
    else
    {
      file = selectedFile.toString();

       if ( file.toLowerCase().endsWith(FileName.getZipFileExtension()) == false)
      {
        int extensionIndex = file.lastIndexOf(".");

        //make sure we don't get an index out of bounds exception
        if ( extensionIndex != -1 )
          file = file.substring(0,file.lastIndexOf("."));

        file = file + FileName.getZipFileExtension();
      }

      return file;
    }
  }

  /**
   * This method will be called to validate text field Values
   * @author Vincent Wong
   */
  private boolean validateTextField(String textFieldValue)
  {
    File file = new File(textFieldValue);

    if ( textFieldValue.equalsIgnoreCase("") )
      return false;

    if ( file.exists() &&
        textFieldValue.endsWith(FileName.getZipFileExtension()) )
    {
      int selection = showOverwriteFileDialog(textFieldValue);

      if ( selection == JOptionPane.OK_OPTION )
        return true;
      else
        return false;
    }
    else if ( textFieldValue.endsWith(FileName.getZipFileExtension()) )
      return true;
    else
      return false;
  }

  /**
   * @author Vincent Wong
   */
  void backupFileTextField_keyPressed(KeyEvent e)
  {
    if ( e.getKeyCode() == KeyEvent.VK_ENTER )
    {
      if ( validateTextField(_backupFileTextField.getText()) )
        _backupFileValidated = true;
      else
        _backupFileValidated = false;
    }
  }

  /**
   * @author Vincent Wong
   */
  private int showOverwriteFileDialog(String fileNameFullPath)
  {
    Assert.assert(fileNameFullPath != null);

    String fileName = fileNameFullPath.substring(fileNameFullPath.lastIndexOf(File.separator) + 1 , fileNameFullPath.length());

    return JOptionPane.showConfirmDialog(this,
           StringLocalizer.keyToString("PBT_FILE_EXISTS_OVERWRITE_KEY"),
           StringLocalizer.keyToString("PBT_OVERWRITE_BACKUP_FILE_KEY"),
           JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
  }

  /**
   * Exit this if the escape key is pressed
   * @author Vincent Wong
   */
  void escapeKeyPressed(KeyEvent e)
  {
    if ( e.getKeyCode() == KeyEvent.VK_ESCAPE )
    {
      this.setVisible(false);
      _owner.deactivateJavaGUI(); // return control to the native code
      dispose();
    }
  }

  /**
   * @author Vincent Wong
   */
  protected void processWindowEvent(WindowEvent e)
  {
    if ( e.getID() == WindowEvent.WINDOW_CLOSING )
    {
      this.setVisible(false);
      _owner.deactivateJavaGUI(); // return control to the native code
      dispose();
    }
  }
}
