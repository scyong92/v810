package proto.mainMenu;

import proto.mainMenu.menuActions.OperatorMenuEnable;

/**
 * Title:       MainMenuData
 * Description: This class contains the data used by the main menu application.
 * It is the data "model" part of the model-view-controller organization of the
 * MainMenu application.  When data is updated, this class calls the "view"
 * instance to update the display with the new information.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class MainMenuData
{
  private MainMenuFrame _view = null;
  // Panel program and current panel data
  private String _panelProgram = null;
  private String _panelProgramVersion = null;
  private String _boardName = null;
  private int _boardNumber = 0;
  // Current user info
  private User _user = new User(null, new OperatorMenuEnable());
  // Current status message.
  private String _statusMessage = null;

  /**
   * Constructor.
   */
  public MainMenuData()
  {
  }

  /**
   * Set the view part of the model-view-controller that is updated when there
   * is a change in data.
   */
  public void setView( MainMenuFrame view )
  {
    _view = view;
  }

  /**
   * Return the panel program name.
   */
  public String getPanelProgram()
  {
    return _panelProgram;
  }

  /**
   * Return the panel program version.
   */
  public String getPanelProgramVersion()
  {
    return _panelProgramVersion;
  }

  /**
   * Return the board name.
   */
  public String getBoardName()
  {
    return _boardName;
  }

  /**
   * Return the board number.
   */
  public int getBoardNumber()
  {
    return _boardNumber;
  }

  /**
   * Return the current user data.
   */
  public User getUser()
  {
    return _user;
  }

  /**
   * Return the current user data.
   */
  public String getStatus()
  {
    return _statusMessage;
  }

  /**
   * Set the panel data.
   *
   * @param   name - panel programm name.
   * @param   version - panel program version.
   * @param   board - current board name.
   * @param   boardNum - current board number.
   */
  public void setPanel(String name, String version, String board, int boardNum)
  {
    _panelProgram = name;
    _panelProgramVersion = version;
    _boardName = board;
    _boardNumber = boardNum;
    if (_view != null)
      _view.updatePanelView();
  }

  /**
   * Set the board data.
   *
   * @param   board - current board name.
   * @param   boardNum - current board number.
   */
  public void setBoard(String board, int boardNum)
  {
    _boardName = board;
    _boardNumber = boardNum;
    if (_view != null)
      _view.updatePanelView();
  }

  /**
   * Set the current user.
   *
   * @param   user - new User information.
   */
  public void setUser(User user)
  {
    //Assert(user != null);
    _user = user;
    if (_view != null)
      _view.updateUserView();
  }

  /**
   * Set the current status message.
   *
   * @param   message - String containing the status message.
   */
  public void setStatus(String message)
  {
    _statusMessage = message;
    if (_view != null)
      _view.updateStatusView();
  }

  /**
   * Send a notify message to the view component.
   *
   * @param   message - String containing type of notify.
   * @param   arg - optional Object sent by the calling method.
   */
  public void notify(String message, Object arg)
  {
    if (_view != null)
      _view.updateNotifyView(message, arg);
  }
}
