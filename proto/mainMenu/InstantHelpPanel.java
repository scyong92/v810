package proto.mainMenu;

import java.awt.*;
import java.net.URL;
import javax.swing.*;

import com.agilent.util.*;

/**
 * Title:       InstantHelpPanel
 * Description: This class contains the components for the instant help panel of
 * the main menu.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class InstantHelpPanel extends JPanel
{
  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JScrollPane _helpScrollPane = new JScrollPane();
  private JEditorPane _helpEditorPane = new JEditorPane();
  private BorderLayout _centerPanelLayout = new BorderLayout();

  /**
   * Constructor.
   */
  public InstantHelpPanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * Component initialization
   */
  void jbInit() throws Exception
  {
    this.setLayout(_borderLayout1);
    _helpEditorPane.setText("Instant help");
    _helpEditorPane.setForeground(Color.blue);
    _helpEditorPane.setEditable(false);
    _centerPanel.setLayout(_centerPanelLayout);
    this.add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.add(_helpScrollPane, BorderLayout.CENTER);
    _helpScrollPane.getViewport().add(_helpEditorPane, null);
  }

  /**
   * Overriding this method ensures that the panel will always be visible.
   */
  public Dimension getMinimumSize()
  {
    final int minimumWidth = 300;
    final int minimumHeight = 100;
    Dimension min = super.getMinimumSize();  // get recommended minimum size

    if (min.width < minimumWidth)
      min.width = minimumWidth;  // set new minimum width
    if (min.height < minimumHeight)
      min.height = minimumHeight;  // set new minimum height
    return min;
  }

  /**
   * Fill in the instant help with a String message.
   *
   * @param   msg - String containing the message.  Null will clear the instant help.
   */
  public boolean setMessage(String msg)
  {
    String message = ((msg == null) ? "" : msg);
    this._helpEditorPane.setText(message);
    return true;
  }

  /**
   * Fill in the instant help with a page.
   *
   * @param   msg - URL of the page to insert into the instant help.
   * @return true if the page is loaded without error.
   */
  public boolean setMessage(URL msg)
  {
    if (msg == null)
      return false;
    try
    {
      this._helpEditorPane.setPage(msg);
    }
    catch (java.io.IOException e)
    {
      return false;
    }
    return true;
  }

}
