package proto.mainMenu;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.agilent.xRayTest.images.Image5DX;
import com.agilent.guiUtil.PopupTrigger;
import com.agilent.util.*;

/**
 * Title:       SerialNumberPanel
 * Description: This class implements the part of the auto test GUI that inputs
 * and queues serial numbers.  It also contains the test loop controls that start,
 * pause and stop the testing loop.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class SerialNumberPanel extends JPanel
{
  // Icons for the control buttons.
  private static final ImageIcon _startIcon = Image5DX.getImageIcon(Image5DX.MM_START);
  private static final ImageIcon _stopIcon = Image5DX.getImageIcon(Image5DX.MM_STOP);
  private static final ImageIcon _pauseIcon = Image5DX.getImageIcon(Image5DX.MM_PAUSE);

  /**
   * Constructor
   */
  public SerialNumberPanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * Create the components.
   */
  void jbInit() throws Exception
  {
    _serialNumberTF.setToolTipText("Enter a serial number");
    _serialNumberTF.setMargin(new Insets(0, 5, 0, 0));
    _serialNumberTF.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _serialNumberTF_actionPerformed(e);
      }
    });
    _panelList.setBackground(SystemColor.info);
    _panelList.setToolTipText("Serial number queue");
    _panelList.setVisibleRowCount(5);
    _deleteMI.setText("Delete");
    ActionListener deleteAction = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _deleteMI_actionPerformed(e);
      }
    };
    _deleteMI.addActionListener(deleteAction);
    _panelList.registerKeyboardAction(deleteAction, KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0, true), WHEN_IN_FOCUSED_WINDOW);
    _deleteAllMI.setText("Delete All");
    _deleteAllMI.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _deleteAllMI_actionPerformed(e);
      }
    });
    _startButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _startButton_actionPerformed(e);
      }
    });
    _pauseBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _pauseBtn_actionPerformed(e);
      }
    });
    _stopBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _stopBtn_actionPerformed(e);
      }
    });
    _panelListPopupMenu.add(_deleteMI);
    _panelListPopupMenu.add(_deleteAllMI);
    _panelList.addMouseListener( new PopupTrigger(_panelListPopupMenu) );
    _listScrollPane.getViewport().add(_panelList, null);
    _centerPanel.setLayout(_centerPanelLayout);
    _centerPanel.add(_serialNumberTF, BorderLayout.NORTH);
    _centerPanel.add(_listScrollPane, BorderLayout.CENTER);

    _startButton.setToolTipText("Start");
    _pauseBtn.setToolTipText("Pause");
    _stopBtn.setToolTipText("Stop");
    _startButton.setIcon(_startIcon);
    _stopBtn.setIcon(_stopIcon);
    _pauseBtn.setIcon(_pauseIcon);
    _controlPanelLayout.setColumns(3);
    _controlPanelLayout.setHgap(2);
    _controlPanelLayout.setVgap(2);
    _controlPanel.setLayout(_controlPanelLayout);
    _controlPanel.add(_startButton, null);
    _controlPanel.add(_pauseBtn, null);
    _controlPanel.add(_stopBtn, null);

    _statusLbl.setText("   ");
    _statusPanel.add(_statusLbl, null);

    _southPanel.setLayout(_southPanelLayout);
    _southPanel.add(_statusPanel, BorderLayout.NORTH);
    _southPanel.add(_controlPanel, BorderLayout.SOUTH);

    this.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Serial Numbers "),BorderFactory.createEmptyBorder(5,5,5,5)));
    this.setLayout(_borderLayout1);
    this.add(_centerPanel, BorderLayout.CENTER);
    this.add(_southPanel, BorderLayout.SOUTH);
  }

  /**
   * Return the PanelEntryList to allow adding and removing of entries.
   */
  public PanelEntryList getPanelList()
  {
    return _panelList;
  }

  /**
   * Set the testing status.
   *
   * @param   status - Status string to display.
   */
  public void setStatus( String status )
  {
    final String stat = ((status == null) ? "  " : status);
    // Update the text area on the Swing thread.
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        _statusLbl.setText(stat);
      }
    });
  }

  /**
   * Callback method to remove highlighted entry from the panel list.
   */
  void _deleteMI_actionPerformed(ActionEvent e)
  {
    getPanelList().removeSelected();
  }

  /**
   * Callback method to remove all entries from the panel list.
   */
  void _deleteAllMI_actionPerformed(ActionEvent e)
  {
    getPanelList().removeAll();
  }

  /**
   * Callback method to remove a serial number from the text field, make a new
   * PanelEntry instance, pass it to the controller and clear the text field.
   */
  void _serialNumberTF_actionPerformed(ActionEvent e)
  {
    String sn = _serialNumberTF.getText();
    PanelEntry p = new PanelEntry( sn );
    // Send the new entry to the controller.
//    if (_controller != null)
//    {
//      _controller.newPanel(p);
//    }
    _serialNumberTF.setText("");

  }

  /**
   * Callback method for the start button.
   */
  void _startButton_actionPerformed(ActionEvent e)
  {
//    if (_controller != null)
//      _controller.startTest();
  }

  /**
   * Callback method for the pause button.
   */
  void _pauseBtn_actionPerformed(ActionEvent e)
  {
//    if (_controller != null)
//      _controller.pauseTest();
  }

  /**
   * Callback method for the stop button.
   */
  void _stopBtn_actionPerformed(ActionEvent e)
  {
//    if (_controller != null)
//      _controller.stopTest();
  }

  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _southPanel = new JPanel();
  private BorderLayout _centerPanelLayout = new BorderLayout();
  private BorderLayout _southPanelLayout = new BorderLayout();
  private JTextField _serialNumberTF = new JTextField();
  private JScrollPane _listScrollPane = new JScrollPane();
  private PanelEntryList _panelList = new PanelEntryList();
  private JPanel _controlPanel = new JPanel();
  private GridLayout _controlPanelLayout = new GridLayout();
  private JButton _startButton = new JButton();
  private JButton _stopBtn = new JButton();
  private JButton _pauseBtn = new JButton();
  private JPanel _statusPanel = new JPanel();
  private JLabel _statusLbl = new JLabel();
  private JPopupMenu _panelListPopupMenu = new JPopupMenu();
  private JMenuItem _deleteMI = new JMenuItem();
  private JMenuItem _deleteAllMI = new JMenuItem();
}
