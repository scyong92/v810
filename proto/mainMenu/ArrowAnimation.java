package proto.mainMenu;

import javax.swing.*;
import com.agilent.xRayTest.images.Image5DX;

/**
 * Title:       ArrowAnimation
 * Description: This class handles the animation for a pair of up or down arrows
 * on two JLabel instances.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class ArrowAnimation implements Runnable
{
  private static final ImageIcon _downArrowIcon = Image5DX.getImageIcon(Image5DX.MM_DOWN_ARROW);
  private static final ImageIcon _upArrowIcon = Image5DX.getImageIcon(Image5DX.MM_UP_ARROW);
  private static final ImageIcon _blankArrowIcon = Image5DX.getImageIcon(Image5DX.MM_BLANK_ARROW);

  public static final String UP = "up";
  public static final String DOWN = "down";

  private boolean _running = false;
  private boolean _visible = false;
  private String _direction = UP;
  private Thread _arrowThread = null;
  private ImageIcon _currentIcon = null;
  private JLabel _leftArrow;
  private JLabel _rightArrow;

  /**
   * Constructor.
   *
   * @param   leftArrow - a JLabel to display the blinking arrow.
   * @param   rightArrow - a JLabel to display the blinking arrow.
   */
  public ArrowAnimation(JLabel leftArrow, JLabel rightArrow)
  {
    _leftArrow = leftArrow;
    _rightArrow = rightArrow;
    _leftArrow.setIcon(_blankArrowIcon);
    _rightArrow.setIcon(_blankArrowIcon);

    _arrowThread = new Thread( this );
    _arrowThread.start();
  }

  /**
   * Change the JLabels to the new icon.
   *
   * @param   icon - new icon for the labels.
   */
  private void setIcon( ImageIcon icon )
  {
    _leftArrow.setIcon(icon);
    _rightArrow.setIcon(icon);
  }

  /**
   * Runnable method
   * This method causes the arrow icons to blink off and on.  A 500 millisecond
   * delay is used to display and hide the arrows.
   */
  public void run()
  {
    while (true)
    {
      if (_running)
      {
        // Select the next icon to be displayed.
        _currentIcon = _blankArrowIcon;
        if (!_visible)
          _currentIcon = ((isDirectionDown()) ? _downArrowIcon : _upArrowIcon);
        _visible = !_visible;
        // Show the icon in the labels
        SwingUtilities.invokeLater( new Runnable()
        {
          public void run()
          {
            ArrowAnimation.this.setIcon(_currentIcon);
          }
        });
      }
      try
      {
        // sleep for 0.5 seconds
        Thread.sleep(500);
      }
      catch(InterruptedException e)
      {
        // do nothing
      }
    }
  }

  // Access methods
  private boolean isDirectionDown()
  {
    return (_direction == DOWN);
  }

  private boolean isDirectionUp()
  {
    return (_direction == UP);
  }

  /**
   * Set the desired arrow direction.
   *
   * @param   direction - "up" or "down"
   */
  public void setArrowDirection( String direction )
  {
    if (direction.equalsIgnoreCase(DOWN))
      _direction = DOWN;
    else
      _direction = UP;
  }

  /**
   * Starts the animation.
   */
  public void start()
  {
    _running = true;
  }

  /**
   * Stop the animation.
   */
  public void stop()
  {
    _running = false;
  }
}
