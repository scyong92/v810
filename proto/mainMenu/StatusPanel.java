package proto.mainMenu;

import java.awt.*;
import javax.swing.*;

import com.agilent.guiUtil.*;
import com.agilent.util.*;

/**
 * Title:        StatusPanel
 * Description:  This panel contains the front panel status information for the
 * main menu.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class StatusPanel extends JPanel
{
  /**
   * Constructor.
   */
  public StatusPanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * Component initialization
   */
  void jbInit() throws Exception
  {
    // Current logged in user
    _userNameLbl.setText("<user>");
    _userLbl.setText("User:");
    flowLayout1.setAlignment(FlowLayout.LEFT);
    _userPanel.setLayout(flowLayout1);
    _userPanel.add(_userLbl, null);
    _userPanel.add(_userNameLbl, null);

    // Status message
    _statusCurrentLbl.setText("<status>");
    _statusLbl.setText("Status:");
    flowLayout3.setAlignment(FlowLayout.LEFT);
    _statusPanel.setLayout(flowLayout3);
    _statusPanel.add(_statusLbl, null);
    _statusPanel.add(_statusCurrentLbl, null);

    _centerPanel.setLayout(_verticalFlowLayout1);
    _centerPanel.add(_userPanel, null);
    _centerPanel.add(_statusPanel, null);

    this.setLayout(_borderLayout1);
    this.add(_centerPanel, BorderLayout.CENTER);
  }

  /**
   * Set the user name.
   */
  public void setUserName( String name )
  {
    final String newName = ((name == null) ? "" : name);
    SwingUtilities.invokeLater( new Runnable()
    {
      public void run()
      {
        StatusPanel.this._userNameLbl.setText(newName);

      }
    });
  }

  /**
   * Set the system status.
   */
  public void setStatus( String status )
  {
    final String stat = ((status == null) ? "" : status);
    SwingUtilities.invokeLater( new Runnable()
    {
      public void run()
      {
        StatusPanel.this._statusCurrentLbl.setText(stat);
      }
    });
  }

  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _statusPanel = new JPanel();
  private JPanel _userPanel = new JPanel();
  private JLabel _userNameLbl = new JLabel();
  private JLabel _userLbl = new JLabel();
  private JLabel _statusCurrentLbl = new JLabel();
  private JLabel _statusLbl = new JLabel();
  private VerticalFlowLayout _verticalFlowLayout1 = new VerticalFlowLayout(VerticalFlowLayout.FILL);
  private FlowLayout flowLayout1 = new FlowLayout();
  private FlowLayout flowLayout3 = new FlowLayout();
}
