package proto.mainMenu;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.agilent.guiUtil.*;
import com.agilent.util.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class OpenPanelDialog extends JDialog
{
  private int _status = JOptionPane.CANCEL_OPTION;
  private Object [] _listData = null;

  // variables used by the character look ahead.
  private static final int _CHAR_DELTA = 1000;
  private long _time = 0;
  private String _key = "";

  /**
   * Constructor.
   *
   * @param   frame - parent for the dialog.
   * @param   title - dialog title string.
   * @param   modal - true for a modal dialog.
   * @param   listData - array of Objects that are put in the selection list.
   */
  public OpenPanelDialog(Frame frame, String title, boolean modal, Object [] listData)
  {
    super(frame, title, modal);
    _listData = listData;
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }
    // Set the Window 'X' button to exit the application.
    addWindowListener( new java.awt.event.WindowAdapter()
    { public void windowClosing(java.awt.event.WindowEvent e) { _cancelBtn_actionPerformed(null); } });
    this.setSize(400,300);
  }

  /**
   * Constructor.
   *
   * @param   frame - parent for the dialog.
   * @param   listData - array of Objects that are put in the selection list.
   */
  public OpenPanelDialog( Frame frame, Object [] listData )
  {
    this(frame, "Open Panel Program", true, listData);
  }

  /**
   * Create the components.
   */
  void jbInit() throws Exception
  {
    _fileList.setListData(_listData);
    _fileList.setVisibleRowCount(8);
    _fileList.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyTyped(KeyEvent e)
      {
        _fileList_keyTyped(e);
      }
    });
    _fileList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _fileList.addMouseListener(new java.awt.event.MouseAdapter()
    {
      public void mouseClicked(MouseEvent e)
      {
        _fileList_mouseClicked(e);
      }
    });
    _fileListScroll.getViewport().add(_fileList, null);
    _fileListPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(93, 93, 93),new Color(134, 134, 134)));
    _fileListPanel.setLayout(fileListPanelLayout);
    _fileListPanel.add(_fileListScroll, BorderLayout.CENTER);

    _centerPanel.setBorder(BorderFactory.createEmptyBorder(10,5,5,5));
    _centerPanel.setLayout(_centerPanelLayout);
    _centerPanel.add(_fileListPanel, BorderLayout.CENTER);

    _okBtn.setFont(new java.awt.Font("Dialog", 1, 12));
    _okBtn.setMnemonic('O');
    _okBtn.setText("Open");
    _okBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _okBtn_actionPerformed(e);
      }
    });
    _cancelBtn.setMnemonic('C');
    _cancelBtn.setText("Cancel");
    _cancelBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _cancelBtn_actionPerformed(e);
      }
    });
    _helpBtn.setMnemonic('H');
    _helpBtn.setText("Help");
    _helpBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _helpBtn_actionPerformed(e);
      }
    });
    _gridLayout2.setHgap(5);
    _okCancelPanel.setLayout(_gridLayout2);
    _okCancelPanel.add(_okBtn, null);
    _okCancelPanel.add(_cancelBtn, null);
    _okCancelPanel.add(_helpBtn, null);
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,5,5,5));
    _flowLayout1.setAlignment(FlowLayout.RIGHT);
    _buttonPanel.setLayout(_flowLayout1);
    _buttonPanel.add(_okCancelPanel, null);

    _rootPanel.setLayout(_rootPanelLayout);
    _rootPanel.add(_centerPanel, BorderLayout.CENTER);
    _rootPanel.add(_buttonPanel, BorderLayout.SOUTH);
    getContentPane().add(_rootPanel);
    this.getRootPane().setDefaultButton(_okBtn);
  }

  /**
   * Exit method for the dialog.  Just hides the dialog.
   */
  private void exitDialog()
  {
    this.hide();
  }

  /**
   * Display the dialog and return the appropriate status depending upon which
   * button the user clicks.
   */
  public int showDialog()
  {
    this.show();
    return _status;
  }

  /**
   * Return the selected list item.
   */
  public String getSelectedFile()
  {
    int index = _fileList.getSelectedIndex();
    if (index >= 0)
      return _listData[index].toString();
    return null;
  }

  /**
   * Cancel button callback method.  Sets status and exits the dialog.
   */
  void _cancelBtn_actionPerformed(ActionEvent e)
  {
    _status = JOptionPane.CANCEL_OPTION;
    exitDialog();
  }

  /**
   * OK button callback method.  Checks that there is a list selectiong and if
   * so if sets status and exits the dialog.
   */
  void _okBtn_actionPerformed(ActionEvent e)
  {
    String file = getSelectedFile();
    if (file == null || file.equals(""))
      return;
    _status = JOptionPane.OK_OPTION;
    exitDialog();
  }

  /**
   * Bring up help for the dialog.
   */
  void _helpBtn_actionPerformed(ActionEvent e)
  {

  }

  /**
   * Registers the mouse clicks in the list.  First click selects and the second
   * cause the OK action to be preformed.
   */
  void _fileList_mouseClicked(MouseEvent e)
  {
    if (_fileList.getSelectedIndex() < 0)
      return;
    String panelName = _listData[ _fileList.getSelectedIndex() ].toString();
    this._selectedFileTF.setText(panelName);
    if (e.getClickCount() > 1)
      this._okBtn_actionPerformed(null);
  }

  /**
   * Key event method.  Advances the list to display the first entry that matches
   * the characters typed by the user.  Characters typed within 1 second of each
   * other are concatenated.  If more than a second elapes between key strokes, the
   * character string starts over.
   */
  void _fileList_keyTyped(KeyEvent e)
  {
    char ch = e.getKeyChar();
    if (!Character.isLetterOrDigit(ch) && ch != '_')
      return;

    // If there was a delay between keys start over.
    if (_time + _CHAR_DELTA < System.currentTimeMillis())
      _key = "";
    _time = System.currentTimeMillis();

    // Concatenate this keystroke onto the string.
    _key += Character.toLowerCase(ch);
    ListModel model = this._fileList.getModel();
    int count = _listData.length;
    // Look for the first entry to start with the key string.
    for (int k = 0; k < count; k++)
    {
      String str = _listData[k].toString().toLowerCase();
      if (str.startsWith(_key))
      {
        this._fileList.setSelectedIndex(k);
        this._fileList.ensureIndexIsVisible(k);
        break;
      }
    }
  }

  private JPanel _rootPanel = new JPanel();
  private BorderLayout _rootPanelLayout = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _okCancelPanel = new JPanel();
  private JButton _okBtn = new JButton();
  private JButton _cancelBtn = new JButton();
  private JButton _helpBtn = new JButton();
  private BorderLayout _centerPanelLayout = new BorderLayout();
  private JPanel _fileListPanel = new JPanel();
  private BorderLayout fileListPanelLayout = new BorderLayout();
  private JTextField _selectedFileTF = new JTextField();
  private JScrollPane _fileListScroll = new JScrollPane();
  private JList _fileList = new JList();
  private FlowLayout _flowLayout1 = new FlowLayout();
  private GridLayout _gridLayout2 = new GridLayout();
}
