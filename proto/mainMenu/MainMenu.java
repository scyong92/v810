package proto.mainMenu;

import java.awt.*;
import java.rmi.RemoteException;
import javax.swing.UIManager;

import com.agilent.xRayTest.util.*;
import com.agilent.xRayTest.gui.*;
import com.agilent.util.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class MainMenu implements ExitableInt
{
  private boolean packFrame = false;
  private MainMenuFrame _frame = null;
  private MainMenuController _controller = null;
  private MainMenuData _data = null;

  /**
   * Constructor.
   */
  public MainMenu()
  {
    _frame = new MainMenuFrame(this);

    // Validate frames that have preset sizes.
    // Pack frames that have useful preferred size info, e.g. from their layout.
    if (packFrame)
      _frame.pack();
    else
      _frame.validate();

    // Center the window
    com.agilent.guiUtil.SwingUtils.centerOnScreen(_frame);

    // Create a Controller instance.
    _controller = new MainMenuController(_frame, _frame);
    _frame.setController(_controller);

    // Create a Data instance and pass it to the controller and frame.
    _data = new MainMenuData();
    _data.setView(_frame);
    _controller.setModel(_data);
    _frame.setModel(_data);
  }

  /**
   * Exit method for the application.  Sets the done flag to true and does
   * a System.exit if the application was started from the class main method.
   */
  public void exitApp()
  {
    if (_startFromMain)
      System.exit(0);
  }

  /**
   * Start the application.
   */
  public void startApp()
  {
    // Create the ServerAdapter.
    ServerAdapter serverAdapter = null;
    try
    {
      serverAdapter = new ServerAdapter();
    }
    catch (RemoteException re)
    {
      MessageDialog.reportRemoteError(_frame);
      exitApp();  // get out if ServerAdapter fails
    }

    // Hook the application up to the ServerAdapter
    if (serverAdapter.connectToServer(this) == false)
    {
      // Can't connect to server so get out.
      serverAdapter = null;
      exitApp();
    }

    // Give the controller the server.
    _controller.setServer(serverAdapter);

    // Show the GUI
    if (_frame != null)
      _frame.setVisible(true);
    _frame.start();  // really start the application
  }

  /**
   * Return the start from main flag.
   */
  public boolean startFromMain()
  {
    return _startFromMain;
  }

  /**
   * Main method
   */
  public static void main(String[] args)
  {
    // Make sure the look and feel is set before any components are created.
    try
    {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch(Exception e)
    {
      Assert.logException(e);
    }

    com.agilent.xRayTest.util.AssertUtil.setUpAssert("5dxMainMenu", null);
    MainMenu mainMenu = new MainMenu();
    mainMenu._startFromMain = true;
    mainMenu.startApp();  // start the application
  }
  private boolean _startFromMain = false;
}
