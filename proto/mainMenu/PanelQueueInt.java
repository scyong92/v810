package proto.mainMenu;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public interface PanelQueueInt
{
  /**
   * Return the next panel in the queue.
   *
   * @return the next PanelEntry from the queue.
   */
  public PanelEntry removePanel();

  /**
   * Return true if a panel is available on the queue.
   *
   * @return true if a panel is available to be removed.
   */
  public boolean panelAvailable();
}
