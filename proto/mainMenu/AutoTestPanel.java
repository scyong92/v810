package proto.mainMenu;

import java.awt.*;
import javax.swing.*;

import com.agilent.xRayTest.images.*;
import com.agilent.util.*;

/**
 * Title:       AutoTestPanel
 * Description: This class is a container panel that holds the SerialNumberPanel,
 * ImagePanel and InspectionPanel.  Together they make up the user interface for
 * doing a panel inspection from entering the serial number, showing the panel
 * image and displaying the test messages.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 * @see SerialNumberPanel
 * @see ImagePanel
 * @see InspectionPanel
 */

public class AutoTestPanel extends JPanel
{
  /** @todo SLA Use LocalizedString for these. */
  private static final String _TESTING_STOPPED = "STOPPED";
  private static final String _TESTING_PAUSED = "PAUSED";
  private static final String _TESTING_STARTED = "   ";

  private ImageIcon _noboardIcon = Image5DX.getImageIcon(Image5DX.MM_NO_PANEL);
  private ImagePanel _imagePanel = null;
  private InspectionPanel _inspectionPanel = null;
  private SerialNumberPanel _serialNumberPanel = null;
  private MainMenuController _controller = null;

  public AutoTestPanel()
  {
    this(null);
  }

  public AutoTestPanel(MainMenuController controller)
  {
    _controller = controller;

    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }

    _imagePanel.setImage(_noboardIcon);  // initialize the panel image
  }

  void jbInit() throws Exception
  {
    // Create the sub-panels.
    _imagePanel = new ImagePanel();
    _inspectionPanel = new InspectionPanel();
    _serialNumberPanel = new SerialNumberPanel();

    _westPanel.setLayout(_westPanelLayout);
    _westPanel.add(_serialNumberPanel, BorderLayout.NORTH);
    _westPanel.add(_imagePanel, BorderLayout.SOUTH);

    _centerPanel.setLayout(_centerPanelLayout);
    _centerPanel.add(_inspectionPanel, BorderLayout.CENTER);

    this.setLayout(_borderLayout1);
    this.add(_centerPanel, BorderLayout.CENTER);
    this.add(_westPanel, BorderLayout.WEST);
  }

  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _westPanel = new JPanel();
  private BorderLayout _centerPanelLayout = new BorderLayout();
  private BorderLayout _westPanelLayout = new BorderLayout();
}
