package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import com.agilent.xRayTest.images.Image5DX;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: ReportDefectsAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ReportDefectsAction extends MenuAction
{
  private static final ImageIcon _reportIcon = Image5DX.getImageIcon(Image5DX.MM_DEFECT_REPORTER);

  /**
   * Constructor.
   */
  public ReportDefectsAction(MainMenuFrame parent)
  {
    super("Defect Reporter", _reportIcon, "Run Defect Reporter", 'R', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._programmingDefectReporterMI_actionPerformed(e);
  }
}
