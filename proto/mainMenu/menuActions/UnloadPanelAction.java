package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import com.agilent.xRayTest.images.Image5DX;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: UnloadPanelAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class UnloadPanelAction extends MenuAction
{
  private static final ImageIcon _icon = Image5DX.getImageIcon(Image5DX.MM_UNLOAD_PANEL);

  /**
   * Constructor.
   */
  public UnloadPanelAction(MainMenuFrame parent)
  {
    super("Unload", _icon, "Unload a panel from the 5DX", 'U', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._panelUnloadMI_actionPerformed(e);
  }
}
