package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: RepairImageAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class RepairImageAction extends MenuAction
{
  /**
   * Constructor.
   */
  public RepairImageAction(MainMenuFrame parent)
  {
    super("Collect Repair Images", null, "Save repair images", 'R', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._toolsRepairImageMI_actionPerformed(e);
  }
}
