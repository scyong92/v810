package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: ChangePasswordAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class ChangePasswordAction extends MenuAction
{
  /**
   * Constructor.
   */
  public ChangePasswordAction(MainMenuFrame parent)
  {
    super("Change Password...", null, "Change this user's password", 'P', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._fileChangePasswordMI_actionPerformed(e);
  }
}
