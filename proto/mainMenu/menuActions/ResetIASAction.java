package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: ResetIASAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ResetIASAction extends MenuAction
{
  /**
   * Constructor.
   */
  public ResetIASAction(MainMenuFrame parent)
  {
    super("Reset Image Analysis (IAS)", null, "Reset the image analysis subsystem", 'I', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._systemResetIASMI_actionPerformed(e);
  }
}
