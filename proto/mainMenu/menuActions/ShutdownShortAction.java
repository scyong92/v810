package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: ShutdownShortAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ShutdownShortAction extends MenuAction
{
  /**
   * Constructor.
   */
  public ShutdownShortAction(MainMenuFrame parent)
  {
    super("Short Term", null, "Run short term shutdown", 'S', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._systemShutdownShortMI_actionPerformed(e);
  }
}
