package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: OptionsAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class OptionsAction extends MenuAction
{
  /**
   * Constructor.
   */
  public OptionsAction(MainMenuFrame parent)
  {
    super("Options...", null, "Open configuration dialog", 'O', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._toolsOptionsMI_actionPerformed(e);
  }
}
