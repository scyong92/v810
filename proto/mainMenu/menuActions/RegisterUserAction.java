package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: RegisterUserAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class RegisterUserAction extends MenuAction
{
  /**
   * Constructor.
   */
  public RegisterUserAction(MainMenuFrame parent)
  {
    super("Register User...", null, "Create or modify a user logon", 'R', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._fileRegisterUserMI_actionPerformed(e);
  }
}
