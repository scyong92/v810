package proto.mainMenu.menuActions;

import java.util.*;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: MenuEnum
 * Description: This class defines all the menu items defined for the main menu
 * and associates an action with each one.  It is declared abstract so that any
 * instance that is created must define a getAction() method.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public abstract class MenuEnum
{
  private final int _value;
  private final String _enumName;

  private static int _index = 0;
  private static ArrayList _menuItems = new ArrayList();

  public static final MenuEnum OPEN_PANEL_PROGRAM = new MenuEnum(_index++, "OPEN_PANEL_PROGRAM")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new OpenPanelAction(parent);
    }
  };
  public static final MenuEnum REGISTER_USER = new MenuEnum(_index++, "REGISTER_USER")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new RegisterUserAction(parent);
    }
  };
  public static final MenuEnum CHANGE_PASSWORD = new MenuEnum(_index++, "CHANGE_PASSWORD")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new ChangePasswordAction(parent);
    }
  };
  public static final MenuEnum LOGON = new MenuEnum(_index++, "LOGON")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new LogonAction(parent);
    }
  };
  public static final MenuEnum LOGOUT = new MenuEnum(_index++, "LOGOUT")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new LogoutAction(parent);
    }
  };
  public static final MenuEnum EXIT = new MenuEnum(_index++, "EXIT")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new ExitAction(parent);
    }
  };
  public static final MenuEnum STARTUP_AUTOMATIC = new MenuEnum(_index++, "STARTUP_AUTOMATIC")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new StartupAutoAction(parent);
    }
  };
  public static final MenuEnum SHUTDOWN_SHORT_TERM = new MenuEnum(_index++, "SHUTDOWN_SHORT_TERM")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new ShutdownShortAction(parent);
    }
  };
  public static final MenuEnum SHUTDOWN_LONG_TERM = new MenuEnum(_index++, "SHUTDOWN_LONG_TERM")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new ShutdownLongAction(parent);
    }
  };
  public static final MenuEnum HOME_RAILS = new MenuEnum(_index++, "HOME_RAILS")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new HomeRailsAction(parent);
    }
  };
  public static final MenuEnum RESET_PANEL_HANDLING = new MenuEnum(_index++, "RESET_PANEL_HANDLING")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new ResetPanelHandlingAction(parent);
    }
  };
  public static final MenuEnum CHANGE_FOV = new MenuEnum(_index++, "CHANGE_FOV")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new ChangeFOVAction(parent);
    }
  };
  public static final MenuEnum STAGE_SPEED = new MenuEnum(_index++, "STAGE_SPEED")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new StageSpeedAction(parent);
    }
  };
  public static final MenuEnum RESET_5DX = new MenuEnum(_index++, "RESET_5DX")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new Reset5dxAction(parent);
    }
  };
  public static final MenuEnum RESET_IAS = new MenuEnum(_index++, "RESET_IAS")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new ResetIASAction(parent);
    }
  };
  public static final MenuEnum SELF_TEST = new MenuEnum(_index++, "SELF_TEST")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new SelfTestAction(parent);
    }
  };
  public static final MenuEnum STATUS_5DX = new MenuEnum(_index++, "STATUS_5DX")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new Status5dxAction(parent);
    }
  };
  public static final MenuEnum TUNER = new MenuEnum(_index++, "TUNER")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new TunerAction(parent);
    }
  };
  public static final MenuEnum CADLINK = new MenuEnum(_index++, "CADLINK")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new CadLinkAction(parent);
    }
  };
  public static final MenuEnum CHECKCAD = new MenuEnum(_index++, "CHECKCAD")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new CheckCadAction(parent);
    }
  };
  public static final MenuEnum REVIEW_DEFECTS = new MenuEnum(_index++, "REVIEW_DEFECTS")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new ReviewDefectsAction(parent);
    }
  };
  public static final MenuEnum REVIEW_MEASUREMENTS = new MenuEnum(_index++, "REVIEW_MEASUREMENTS")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new ReviewMeasurementsAction(parent);
    }
  };
  public static final MenuEnum REPORT_DEFECTS = new MenuEnum(_index++, "REPORT_DEFECTS")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new ReportDefectsAction(parent);
    }
  };
  public static final MenuEnum LOAD_PANEL = new MenuEnum(_index++, "LOAD_PANEL")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new LoadPanelAction(parent);
    }
  };
  public static final MenuEnum MANUAL_LOAD_PANEL = new MenuEnum(_index++, "MANUAL_LOAD_PANEL")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new LoadPanelManualAction(parent);
    }
  };
  public static final MenuEnum UNLOAD_PANEL = new MenuEnum(_index++, "UNLOAD_PANEL")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new UnloadPanelAction(parent);
    }
  };
  public static final MenuEnum ALIGN_AND_MAP = new MenuEnum(_index++, "ALIGN_AND_MAP")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new AlignAndMapAction(parent);
    }
  };
  public static final MenuEnum ALIGN_AUTOMATIC = new MenuEnum(_index++, "ALIGN_AUTOMATIC")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new AlignAutoAction(parent);
    }
  };
  public static final MenuEnum ALIGN_MANUAL = new MenuEnum(_index++, "ALIGN_MANUAL")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new AlignManualAction(parent);
    }
  };
  public static final MenuEnum DISPLAY_ALIGNMENT_MATRIX = new MenuEnum(_index++, "DISPLAY_ALIGNMENT_MATRIX")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new DisplayMatrixAction(parent);
    }
  };
  public static final MenuEnum MAP_AUTOMATIC = new MenuEnum(_index++, "MAP_AUTOMATIC")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new MapAutoAction(parent);
    }
  };
  public static final MenuEnum MAP_MANUAL = new MenuEnum(_index++, "MAP_MANUAL")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new MapManualAction(parent);
    }
  };
  public static final MenuEnum CHECK_SURFACE_MAP = new MenuEnum(_index++, "CHECK_SURFACE_MAP")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new CheckMapAction(parent);
    }
  };
  public static final MenuEnum VERIFY_LASER = new MenuEnum(_index++, "VERIFY_LASER")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new VerifyLaserAction(parent);
    }
  };
  public static final MenuEnum MAP_POINTS = new MenuEnum(_index++, "MAP_POINTS")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new MapPointsAction(parent);
    }
  };
  public static final MenuEnum VIEW_FOCUS = new MenuEnum(_index++, "VIEW_FOCUS")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new ViewFocusAction(parent);
    }
  };
  public static final MenuEnum SERVICE_MENU = new MenuEnum(_index++, "SERVICE_MENU")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new ServiceMenuAction(parent);
    }
  };
  public static final MenuEnum TOOLBOX = new MenuEnum(_index++, "TOOLBOX")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new ToolboxAction(parent);
    }
  };
  public static final MenuEnum OPTIONS = new MenuEnum(_index++, "OPTIONS")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new OptionsAction(parent);
    }
  };
  public static final MenuEnum HELP_CONTENTS = new MenuEnum(_index++, "HELP_CONTENTS")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new HelpContentsAction(parent);
    }
  };
  public static final MenuEnum HELP_ABOUT = new MenuEnum(_index++, "HELP_ABOUT")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new HelpAboutAction(parent);
    }
  };
  public static final MenuEnum WORK_IMAGE = new MenuEnum(_index++, "WORK_IMAGE")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new WorkImageAction(parent);
    }
  };
  public static final MenuEnum DIAGNOSTIC_IMAGE = new MenuEnum(_index++, "DIAGNOSTIC_IMAGE")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new DiagImageAction(parent);
    }
  };
  public static final MenuEnum REPAIR_IMAGE = new MenuEnum(_index++, "REPAIR_IMAGE")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new RepairImageAction(parent);
    }
  };
  public static final MenuEnum XOUT = new MenuEnum(_index++, "XOUT")
  {
    public MenuAction getAction( MainMenuFrame parent )
    {
      return new XoutAction(parent);
    }
  };


  /**
   * Constructor - private so that only the static instances can be created.
   */
  private MenuEnum( int value, String enumName )
  {
    // Assert(enumName != null && !enumName.equals(""));  // no empty name allowed
    _value = value;
    _enumName = enumName;
    _menuItems.add(_value, this);
  }

  /**
   * Return an instance of the Action associated with this enum.
   * Must override this method for any enum that has an Action.
   */
  abstract public MenuAction getAction( MainMenuFrame parent );

  /**
   * Return the int value of a MenuEnum.
   */
  public int getValue()
  {
    return _value;
  }

  /**
   * Return a List of all the defined MenuEnum objects.
   */
  public static List getMenuItems()
  {
    return _menuItems;
  }

  /**
   * Return the number of MenuEnum items defined.
   */
  public static int size()
  {
    return _menuItems.size();
  }

  /**
   * Look for a matching toString and return that MenuEnum.  Null if no match.
   */
  public static MenuEnum findEnum( String toString )
  {
    if (toString == null)
      return null;
    Iterator iter = _menuItems.iterator();
    while (iter.hasNext())
    {
      MenuEnum menuItem = (MenuEnum)iter.next();
      if (toString.equalsIgnoreCase(menuItem.toString()))
        return menuItem;
    }
    return null;
  }

  /**
   * Return the name of the enumeration.
   */
  public String toString()
  {
    return _enumName;
  }
}
