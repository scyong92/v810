package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import com.agilent.xRayTest.images.Image5DX;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: ReviewDefectsAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ReviewDefectsAction extends MenuAction
{
  private static final ImageIcon _revDefIcon = Image5DX.getImageIcon(Image5DX.MM_REVIEW_DEFECTS);

  /**
   * Constructor.
   */
  public ReviewDefectsAction(MainMenuFrame parent)
  {
    super("Review Defects", _revDefIcon, "Run Review Defects", 'D', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._programmingReveiwDefMI_actionPerformed(e);
  }
}
