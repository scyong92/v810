package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: WorkImageAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class WorkImageAction extends MenuAction
{
  /**
   * Constructor.
   */
  public WorkImageAction(MainMenuFrame parent)
  {
    super("Collect Workstation Images", null, "Save images for a test", 'W', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._toolsWorkImageMI_actionPerformed(e);
  }
}
