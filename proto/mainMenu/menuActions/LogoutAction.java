package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: LogoutAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class LogoutAction extends MenuAction
{
  /**
   * Constructor.
   */
  public LogoutAction(MainMenuFrame parent)
  {
    super("Logout", null, "Logout the current user", 'U', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._fileLogoutMI_actionPerformed(e);
  }
}
