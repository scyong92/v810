package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: LoadPanelManualAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author
 * @version 1.0
 */

public class LoadPanelManualAction extends MenuAction
{
  /**
   * Constructor.
   */
  public LoadPanelManualAction(MainMenuFrame parent)
  {
    super("Manual Load", null, "Manually load a panel into the 5DX", 'N', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._panelManualLoadPanelMI_actionPerformed(e);
  }
}
