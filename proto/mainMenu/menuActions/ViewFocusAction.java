package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: ViewFocusAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class ViewFocusAction extends MenuAction
{
  /**
   * Constructor.
   */
  public ViewFocusAction(MainMenuFrame parent)
  {
    super("Adjust View Focus", null, "Adjust view focus", 'F', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._programmingViewFocusMI_actionPerformed(e);
  }
}
