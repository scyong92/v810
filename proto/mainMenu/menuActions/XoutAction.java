package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: XoutAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class XoutAction extends MenuAction
{
  /**
   * Constructor.
   */
  public XoutAction(MainMenuFrame parent)
  {
    super("Setup X-out Detection", null, "Setup x-out detection", 'X', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._programmingXoutMI_actionPerformed(e);
  }
}
