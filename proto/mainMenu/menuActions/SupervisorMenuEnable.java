package proto.mainMenu.menuActions;

/**
 * Title: SupervisorMenuEnable
 * Description: This class defines the supervisor permission level.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 * @see MenuEnable
 */

class SupervisorMenuEnable extends MenuEnable
{
  /**
   * Constructor.
   */
  public SupervisorMenuEnable()
  {
    this.setAll();
  }
}
