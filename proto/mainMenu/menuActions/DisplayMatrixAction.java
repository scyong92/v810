package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: DisplayMatrixAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class DisplayMatrixAction extends MenuAction
{
  /**
   * Constructor.
   */
  public DisplayMatrixAction(MainMenuFrame parent)
  {
    super("Display Matrix", null, "Display the alignment matrix", 'D', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._panelAlignDisplayMatMI_actionPerformed(e);
  }
}
