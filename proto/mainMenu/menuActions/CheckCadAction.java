package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import com.agilent.xRayTest.images.Image5DX;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: CheckCadAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class CheckCadAction extends MenuAction
{
  private static final ImageIcon _checkCadIcon = Image5DX.getImageIcon(Image5DX.MM_CHECK_CAD);

  /**
   * Constructor.
   */
  public CheckCadAction(MainMenuFrame parent)
  {
    super("View Verifier", _checkCadIcon, "Run View Verifier", 'w', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._programmingCheckcadMI_actionPerformed(e);
  }
}
