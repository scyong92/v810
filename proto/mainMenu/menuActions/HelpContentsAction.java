package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import com.agilent.xRayTest.images.Image5DX;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: HelpContentsAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class HelpContentsAction extends MenuAction
{
  private static final ImageIcon _icon = Image5DX.getImageIcon(Image5DX.MM_HELP);

  /**
   * Constructor.
   */
  public HelpContentsAction(MainMenuFrame parent)
  {
    super("Contents", _icon, "Help information", 'C', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._helpContentsMI_actionPerformed(e);
  }
}
