package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: LogonAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class LogonAction extends MenuAction
{
  /**
   * Constructor.
   */
  public LogonAction(MainMenuFrame parent)
  {
    super("Logon", null, "Logon as a new user", 'L', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._fileLogonMI_actionPerformed(e);
  }
}
