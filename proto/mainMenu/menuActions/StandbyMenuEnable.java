package proto.mainMenu.menuActions;

/**
 * Title: StandbyMenuEnable
 * Description: This class defines the permission level available in standby mode.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author  Steve Anonson
 * @version 1.0
 */

public class StandbyMenuEnable extends MenuEnable
{

  /**
   * Constructor.
   */
  public StandbyMenuEnable()
  {
    this.set(MenuEnum.LOGON);
  }
}
