package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: ToolboxAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class ToolboxAction extends MenuAction
{
  /**
   * Constructor.
   */
  public ToolboxAction(MainMenuFrame parent)
  {
    super("Toolbox", null, "Confirmation and Diagnostics", 'T', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._toolsToolboxMI_actionPerformed(e);
  }
}
