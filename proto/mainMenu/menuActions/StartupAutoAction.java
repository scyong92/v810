package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import com.agilent.xRayTest.images.Image5DX;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: StartupAutoAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class StartupAutoAction extends MenuAction
{
  private static final ImageIcon _autoStartIcon = Image5DX.getImageIcon(Image5DX.MM_STARTUP);

  /**
   * Constructor.
   */
  public StartupAutoAction(MainMenuFrame parent)
  {
    super("Startup", _autoStartIcon, "Run automatic startup", 'A', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._systemStartupMI_actionPerformed(e);
  }
}
