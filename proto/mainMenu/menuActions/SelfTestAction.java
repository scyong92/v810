package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: SelfTestAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class SelfTestAction extends MenuAction
{
  /**
   * Constructor.
   */
  public SelfTestAction(MainMenuFrame parent)
  {
    super("Run Selftest", null, "Run selftest on the 5dx system", 'T', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._toolsSelfTestMI_actionPerformed(e);
  }
}
