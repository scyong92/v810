package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: ServiceMenuAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class ServiceMenuAction extends MenuAction
{
  /**
   * Constructor.
   */
  public ServiceMenuAction(MainMenuFrame parent)
  {
    super("Service Menu", null, "Display the service menu", 'S', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._toolsServiceMI_actionPerformed(e);
  }
}
