package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: AlignAutoAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class AlignAutoAction extends MenuAction
{
  /**
   * Constructor.
   */
  public AlignAutoAction(MainMenuFrame parent)
  {
    super("Automatic", null, "Automatically align panel", 'A', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._panelAlignAutoMI_actionPerformed(e);
  }
}
