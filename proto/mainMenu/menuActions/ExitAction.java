package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: ExitAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ExitAction extends MenuAction
{
  /**
   * Constructor.
   */
  public ExitAction(MainMenuFrame parent)
  {
    super("Exit", null, "Exit from 5DX", 'X', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    _parent._fileExitMI_actionPerformed(e);
  }
}
