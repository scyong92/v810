package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import com.agilent.xRayTest.images.Image5DX;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: CadLinkAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class CadLinkAction extends MenuAction
{
  private static final ImageIcon _cadlinkIcon = Image5DX.getImageIcon(Image5DX.MM_CADLINK);

  /**
   * Constructor.
   */
  public CadLinkAction(MainMenuFrame parent)
  {
    super("TestLink", _cadlinkIcon, "Run TestLink II", 'L', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._programmingCadlinkMI_actionPerformed(e);
  }
}
