package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: MapPointsAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class MapPointsAction extends MenuAction
{
  public MapPointsAction(MainMenuFrame parent)
  {
    super("Setup Map Points", null, "Setup surface map points", 'P', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._programmingMapPointsMI_actionPerformed(e);
  }
}
