package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: VerifyLaserAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class VerifyLaserAction extends MenuAction
{
  /**
   * Constructor.
   */
  public VerifyLaserAction(MainMenuFrame parent)
  {
    super("Verify Laser Repeatability", null, "Verify laser repeatability", 'V', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._panelMapVerifyLaserMI_actionPerformed(e);
  }
}
