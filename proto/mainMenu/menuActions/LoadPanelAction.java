package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import com.agilent.xRayTest.images.Image5DX;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: LoadPanelAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class LoadPanelAction extends MenuAction
{
  private static final ImageIcon _icon = Image5DX.getImageIcon(Image5DX.MM_LOAD_PANEL);

  /**
   * Constructor.
   */
  public LoadPanelAction(MainMenuFrame parent)
  {
    super("Load", _icon, "Load a panel into the 5DX", 'L', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._panelLoadMI_actionPerformed(e);
  }
}
