package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import com.agilent.xRayTest.images.Image5DX;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: ReviewMeasurementsAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ReviewMeasurementsAction extends MenuAction
{
  private static final ImageIcon _revMeasIcon = Image5DX.getImageIcon(Image5DX.MM_REVIEW_MEASUREMENTS);

  /**
   * Constructor.
   */
  public ReviewMeasurementsAction(MainMenuFrame parent)
  {
    super("Review Measurments", _revMeasIcon, "Run Review Measurments", 'M', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._programmingReviewMeasMI_actionPerformed(e);
  }
}
