package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: AlignManualAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class AlignManualAction extends MenuAction
{
  /**
   * Constructor.
   */
  public AlignManualAction(MainMenuFrame parent)
  {
    super("Manual", null, "Manually align panel", 'M', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._panelAlignManualMI_actionPerformed(e);
  }
}
