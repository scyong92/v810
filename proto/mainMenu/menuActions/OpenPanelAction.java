package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import com.agilent.xRayTest.images.Image5DX;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: OpenPanelAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class OpenPanelAction extends MenuAction
{
  private static final ImageIcon _icon = Image5DX.getImageIcon(Image5DX.MM_OPEN_PANEL);

  /**
   * Constructor.
   */
  public OpenPanelAction(MainMenuFrame parent)
  {
    super("Open Panel Program...", _icon, "Open a panel program", 'O', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._fileOpenPanelMI_actionPerformed(e);
  }
}
