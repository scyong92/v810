package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import com.agilent.xRayTest.images.Image5DX;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: AlignAndMapAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class AlignAndMapAction extends MenuAction
{
  private static final ImageIcon _icon = Image5DX.getImageIcon(Image5DX.MM_ALIGN_AND_MAP);

  /**
   * Constructor.
   */
  public AlignAndMapAction(MainMenuFrame parent)
  {
    super("Align and Map", _icon, "Align and map a panel", 'G', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._panelAlignAndMapMI_actionPerformed(e);
  }
}
