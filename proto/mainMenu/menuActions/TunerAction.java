package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import com.agilent.xRayTest.images.Image5DX;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: TunerAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class TunerAction extends MenuAction
{
  private static final ImageIcon _tunerIcon = Image5DX.getImageIcon(Image5DX.MM_ALGORITHM_TUNER);

  /**
   * Constructor.
   */
  public TunerAction(MainMenuFrame parent)
  {
    super("Algorithm Tuner", _tunerIcon, "Run Algorithm Tuner", 'A', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._programmingTunerMI_actionPerformed(e);
  }
}
