package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: ChangeFOVAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ChangeFOVAction extends MenuAction
{
  /**
   * Constructor.
   */
  public ChangeFOVAction(MainMenuFrame parent)
  {
    super("Change FOV", null, "Change the field of view", 'F', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    System.out.println(this.getTooltip());
// DEBUG
  }
}
