package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: ShutdownLongAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ShutdownLongAction extends MenuAction
{
  /**
   * Constructor.
   */
  public ShutdownLongAction(MainMenuFrame parent)
  {
    super("Long Term", null, "Run long term shutdown", 'L', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._systemShutdownLongMI_actionPerformed(e);
  }
}
