package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: MapManualAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class MapManualAction extends MenuAction
{
  public MapManualAction(MainMenuFrame parent)
  {
    super("Manual", null, "Manually surface map a panel", 'M', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._panelMapManualMI_actionPerformed(e);
  }
}
