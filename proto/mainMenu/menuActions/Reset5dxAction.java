package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: Reset5dxAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class Reset5dxAction extends MenuAction
{
  /**
   * Constructor.
   */
  public Reset5dxAction(MainMenuFrame parent)
  {
    super("Reset 5DX", null, "Reset the 5DX", 'R', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._systemReset5DXMI_actionPerformed(e);
  }
}
