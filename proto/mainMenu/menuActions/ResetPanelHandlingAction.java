package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: ResetPanelHandlingAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ResetPanelHandlingAction extends MenuAction
{
  /**
   * Constructor.
   */
  public ResetPanelHandlingAction(MainMenuFrame parent)
  {
    super("Reset", null, "Reset the panel handling subsystem", 'R', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._systemPanelHandlingResetMI_actionPerformed(e);
  }
}
