package proto.mainMenu.menuActions;

import java.util.*;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: MenuActions
 * Description: This class provides a map between a MenuAction for all the menu items
 * defined in MenuEnum.  It provides methods to fetch the action associated with a menu
 * item and a method to enable/disable the menu items.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 * @see MenuEnable
 * @see MenuEnum
 */

public class MenuActions
{
  private HashMap _actionList;

  public MenuActions( MainMenuFrame parent )
  {
    // Assert(parent != null);
    _actionList = new HashMap( MenuEnum.size() );  // large enough to hold all menu items
    java.util.Iterator iter = MenuEnum.getMenuItems().iterator();
    while (iter.hasNext())
    {
      MenuEnum menuItem = (MenuEnum)iter.next();
      MenuAction action = menuItem.getAction(parent);
      // Assert(action != null);
      _actionList.put( menuItem, action );
    }
  }

  /**
   * Return the appropriate MenuAction for the menu item.
   *
   * @param item - MenuEnum that determines which menu item to create an action for.
   * @return - Correct MenuAction for the menu item.  Null if menu item not found.
   */
  public MenuAction getAction( MenuEnum item )
  {
    return (MenuAction)_actionList.get( item );
  }

  /**
   * Enable the appropriate menu items.
   */
  public void setEnabled( MenuEnable menuEnable )
  {
    java.util.Iterator iter = MenuEnum.getMenuItems().iterator();
    while (iter.hasNext())
    {
      MenuEnum menuItem = (MenuEnum)iter.next();
      MenuAction action = this.getAction(menuItem);
      boolean enabled = menuEnable.get(menuItem);
      action.setEnabled( enabled );
    }
  }
}
