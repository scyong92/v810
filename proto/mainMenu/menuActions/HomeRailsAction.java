package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: HomeRailsAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class HomeRailsAction extends MenuAction
{
  /**
   * Constructor.
   */
  public HomeRailsAction(MainMenuFrame parent)
  {
    super("Home Rails", null, "Move panel handling rails to home position", 'H', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._systemPanelHandlingHomeRailsMI_actionPerformed(e);
  }
}
