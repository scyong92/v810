package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: MapAutoAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class MapAutoAction extends MenuAction
{
  public MapAutoAction(MainMenuFrame parent)
  {
    super("Automatic", null, "Automatically surface map a panel", 'A', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._panelMapAutoMI_actionPerformed(e);
  }
}
