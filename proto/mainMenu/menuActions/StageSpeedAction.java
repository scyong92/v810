package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: StageSpeedAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class StageSpeedAction extends MenuAction
{
  /**
   * Constructor.
   */
  public StageSpeedAction(MainMenuFrame parent)
  {
    super("Set Stage Speed", null, "Change the stage speed", 'P', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._panelStageSpeedMI_actionPerformed(e);
  }
}
