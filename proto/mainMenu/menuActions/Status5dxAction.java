package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: Status5dxAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class Status5dxAction extends MenuAction
{
  /**
   * Constructor.
   */
  public Status5dxAction(MainMenuFrame parent)
  {
    super("5DX Status...", null, "Show the 5DX status", 'S', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._systemStatus5DXMI_actionPerformed(e);
  }
}
