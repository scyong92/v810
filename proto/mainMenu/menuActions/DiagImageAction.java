package proto.mainMenu.menuActions;

import java.awt.event.ActionEvent;
import proto.mainMenu.MainMenuFrame;

/**
 * Title: DiagImageAction
 * Description: Defines the action taken when a menu and/or toolbar button is clicked.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class DiagImageAction extends MenuAction
{
  /**
   * Constructor.
   */
  public DiagImageAction(MainMenuFrame parent)
  {
    super("Collect Diagnostic Image", null, "Save diagnostic image", 'D', parent);
  }

  /**
   * Callback method for the action.  Calls the parent method.
   */
  public void actionPerformed(ActionEvent e)
  {
    this.getParent()._toolsDiagImageMI_actionPerformed(e);
  }
}
