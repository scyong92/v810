package proto.mainMenu;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.agilent.xRayTest.gui.*;
import proto.mainMenu.menuActions.*;
import com.agilent.xRayTest.images.*;
import com.agilent.util.*;

import com.agilent.util.LocalizedString;
import com.agilent.xRayTest.util.*;
/**
 * Title:       MainMenuFrame
 * Description: This class defines the main menu components.
 * It is the "view" part of the model-view-controller organization of the
 * MainMenu application.  The class is updated when the data changes in the model.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class MainMenuFrame extends JFrame implements PanelQueueInt
{
  private MainMenu _parent = null;
  private MenuActions _menuActions = null;
  private InstantHelpPanel _instantHelpPanel = new InstantHelpPanel();
  private StatusPanel _statusPanel = new StatusPanel();
  private AutoTestPanel _autoTestPanel = new AutoTestPanel();
  private MainMenuController _controller = null;
  private MainMenuData _model = null;

  private Object _returnValue = null;

  /**
   * Construct the frame
   */
  public MainMenuFrame(MainMenu parent)
  {
    _parent = parent;
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      Assert.logException(e);
    }

    // Set up glass pane listeners used to disable the interface.
    this.getGlassPane().addMouseListener(new MouseAdapter() { });
    this.getGlassPane().addKeyListener(new KeyAdapter() { });
  }

  /**
   * Component initialization
   */
  private void jbInit() throws Exception
  {
    _contentPane = (JPanel) this.getContentPane();
    _menuActions = new MenuActions(this);

    // File Menu
    _fileMenu.setMnemonic('F');
    _fileMenu.setText("File");
    _fileOpenPanelMI.setText("Open Panel Program...");
    setMenuAction( _fileOpenPanelMI, _menuActions.getAction(MenuEnum.OPEN_PANEL_PROGRAM) );
    _fileLogonMI.setText("Logon");
    setMenuAction( _fileLogonMI, _menuActions.getAction(MenuEnum.LOGON) );
    _fileLogoutMI.setText("Logout");
    setMenuAction( _fileLogoutMI, _menuActions.getAction(MenuEnum.LOGOUT) );
    _fileChangePasswordMI.setText("Change Password");
    setMenuAction( _fileChangePasswordMI, _menuActions.getAction(MenuEnum.CHANGE_PASSWORD) );
    _fileRegisterUserMI.setText("Register User");
    setMenuAction( _fileRegisterUserMI, _menuActions.getAction(MenuEnum.REGISTER_USER) );
    _fileExitMI.setText("Exit");
    setMenuAction( _fileExitMI, _menuActions.getAction(MenuEnum.EXIT) );  ////////////////
    _fileMenu.add(_fileOpenPanelMI);  ////////////////////////////////////////////////////
    _fileMenu.addSeparator();
    _fileMenu.add(_fileLogonMI);
    _fileMenu.add(_fileLogoutMI);
    _fileMenu.add(_fileChangePasswordMI);
    _fileMenu.add(_fileRegisterUserMI);
    _fileMenu.addSeparator();
    _fileMenu.add(_fileExitMI);

    // System Menu
    _systemMenu.setMnemonic('S');
    _systemMenu.setText("System");
    _systemStartupMI.setText("Startup");
    setMenuAction( _systemStartupMI, _menuActions.getAction(MenuEnum.STARTUP_AUTOMATIC) );
    _systemShutdownMenu.setMnemonic('D');
    _systemShutdownMenu.setText("Shutdown");
    _systemShutdownShortMI.setText("Short Term");
    setMenuAction( _systemShutdownShortMI, _menuActions.getAction(MenuEnum.SHUTDOWN_SHORT_TERM) );
    _systemShutdownLongMI.setText("Long Term");
    setMenuAction( _systemShutdownLongMI, _menuActions.getAction(MenuEnum.SHUTDOWN_LONG_TERM) );
    _systemShutdownMenu.add(_systemShutdownShortMI);
    _systemShutdownMenu.add(_systemShutdownLongMI);
    _systemPanelHandlingMenu.setMnemonic('H');
    _systemPanelHandlingMenu.setText("Panel Handling");
    _systemPanelHandlingHomeRailsMI.setText("Home Rails");
    setMenuAction( _systemPanelHandlingHomeRailsMI, _menuActions.getAction(MenuEnum.HOME_RAILS) );
    _systemPanelHandlingResetMI.setText("Reset");
    setMenuAction( _systemPanelHandlingResetMI, _menuActions.getAction(MenuEnum.RESET_PANEL_HANDLING) );
    _systemPanelHandlingMenu.add(_systemPanelHandlingHomeRailsMI);
    _systemPanelHandlingMenu.add(_systemPanelHandlingResetMI);
    _systemReset5DXMI.setText("Reset 5DX");
    setMenuAction( _systemReset5DXMI, _menuActions.getAction(MenuEnum.RESET_5DX) );
    _systemResetIASMI.setText("Reset Image Analysis (IAS)");
    setMenuAction( _systemResetIASMI, _menuActions.getAction(MenuEnum.RESET_IAS) );
    _systemStatus5DXMI.setText("5DX Status...");
    setMenuAction( _systemStatus5DXMI, _menuActions.getAction(MenuEnum.STATUS_5DX) );
    _systemMenu.add(_systemStartupMI);
    _systemMenu.add(_systemShutdownMenu);
    _systemMenu.addSeparator();
    _systemMenu.add(_systemPanelHandlingMenu);
    _systemMenu.add(_systemReset5DXMI);
    _systemMenu.add(_systemResetIASMI);
    _systemMenu.add(_systemStatus5DXMI);

    // Panel Menu
    _panelMenu.setMnemonic('P');
    _panelMenu.setText("Panel");
    _panelLoadMI.setText("Automaitc Load");
    setMenuAction( _panelLoadMI, _menuActions.getAction(MenuEnum.LOAD_PANEL) );
    _panelUnloadMI.setText("Unload");
    setMenuAction( _panelUnloadMI, _menuActions.getAction(MenuEnum.UNLOAD_PANEL) );
    _panelAlignAndMapMI.setText("Align and Map");
    setMenuAction( _panelAlignAndMapMI, _menuActions.getAction(MenuEnum.ALIGN_AND_MAP) );
    _panelAlignMenu.setMnemonic('A');
    _panelAlignMenu.setText("Align");
    _panelAlignAutoMI.setText("Automatic");
    setMenuAction( _panelAlignAutoMI, _menuActions.getAction(MenuEnum.ALIGN_AUTOMATIC) );
    _panelAlignManualMI.setText("Manual");
    setMenuAction( _panelAlignManualMI, _menuActions.getAction(MenuEnum.ALIGN_MANUAL) );
    _panelAlignDisplayMatMI.setText("Display Matrix");
    setMenuAction( _panelAlignDisplayMatMI, _menuActions.getAction(MenuEnum.DISPLAY_ALIGNMENT_MATRIX) );
    _panelAlignMenu.add(_panelAlignAutoMI);
    _panelAlignMenu.add(_panelAlignManualMI);
    _panelAlignMenu.add(_panelAlignDisplayMatMI);
    _panelMapMenu.setText("Map");
    _panelMapMenu.setMnemonic('M');
    _panelMapAutoMI.setText("Automaitc");
    setMenuAction( _panelMapAutoMI, _menuActions.getAction(MenuEnum.MAP_AUTOMATIC) );
    _panelMapManualMI.setText("Manual");
    setMenuAction( _panelMapManualMI, _menuActions.getAction(MenuEnum.MAP_MANUAL) );
    _panelMapCheckSurMapMI.setText("Check Surface Map");
    setMenuAction( _panelMapCheckSurMapMI, _menuActions.getAction(MenuEnum.CHECK_SURFACE_MAP) );
    _panelMapVerifyLaserMI.setText("Verify Laser Repeatability");
    setMenuAction( _panelMapVerifyLaserMI, _menuActions.getAction(MenuEnum.VERIFY_LASER) );
    _panelMapMenu.add(_panelMapAutoMI);
    _panelMapMenu.add(_panelMapManualMI);
    _panelMapMenu.add(_panelMapCheckSurMapMI);
    _panelMapMenu.add(_panelMapVerifyLaserMI);
    _panelStageSpeedMI.setText("Set Stage Speed");
    setMenuAction( _panelStageSpeedMI, _menuActions.getAction(MenuEnum.STAGE_SPEED) );
    _panelManualLoadMI.setText("Manual Load");
    setMenuAction( _panelManualLoadMI, _menuActions.getAction(MenuEnum.MANUAL_LOAD_PANEL) );
    _panelMenu.add(_panelLoadMI);
    _panelMenu.add(_panelUnloadMI);
    _panelMenu.add(_panelAlignAndMapMI);
    _panelMenu.add(_panelAlignMenu);
    _panelMenu.add(_panelMapMenu);
    _panelMenu.add(_panelStageSpeedMI);
    _panelMenu.add(_panelManualLoadMI);

    // Programming Menu
    _programmingMenu.setMnemonic('R');
    _programmingMenu.setText("Programming");
    _programmingCadlinkMI.setText("TestLink");
    setMenuAction( _programmingCadlinkMI, _menuActions.getAction(MenuEnum.CADLINK) );
    _programmingTunerMI.setText("Algorithm Tuner");
    setMenuAction( _programmingTunerMI, _menuActions.getAction(MenuEnum.TUNER) );
    _programmingCheckcadMI.setText("View Verifier");
    setMenuAction( _programmingCheckcadMI, _menuActions.getAction(MenuEnum.CHECKCAD) );
    _programmingMapPointsMI.setText("Setup Map Points");
    setMenuAction( _programmingMapPointsMI, _menuActions.getAction(MenuEnum.MAP_POINTS) );
    _programmingViewFocusMI.setText("Adjust View Focus");
    setMenuAction( _programmingViewFocusMI, _menuActions.getAction(MenuEnum.VIEW_FOCUS) );
    _programmingXoutMI.setText("Setup X-out Detection");
    setMenuAction( _programmingXoutMI, _menuActions.getAction(MenuEnum.XOUT) );
    _programmingReveiwDefMI.setText("Review Defects");
    setMenuAction( _programmingReveiwDefMI, _menuActions.getAction(MenuEnum.REVIEW_DEFECTS) );
    _programmingReviewMeasMI.setText("Review Measurements");
    setMenuAction( _programmingReviewMeasMI, _menuActions.getAction(MenuEnum.REVIEW_MEASUREMENTS) );
    _programmingDefectReporterMI.setText("Defect Reporter");
    setMenuAction( _programmingDefectReporterMI, _menuActions.getAction(MenuEnum.REPORT_DEFECTS) );
    _programmingMenu.add(_programmingCadlinkMI);
    _programmingMenu.add(_programmingTunerMI);
    _programmingMenu.add(_programmingCheckcadMI);
    _programmingMenu.addSeparator();
    _programmingMenu.add(_programmingMapPointsMI);
    _programmingMenu.add(_programmingViewFocusMI);
    _programmingMenu.add(_programmingXoutMI);
    _programmingMenu.addSeparator();
    _programmingMenu.add(_programmingReveiwDefMI);
    _programmingMenu.add(_programmingReviewMeasMI);
    _programmingMenu.add(_programmingDefectReporterMI);

    // Tools Menu
    _toolsMenu.setMnemonic('T');
    _toolsMenu.setText("Tools");
    _toolsWorkImageMI.setText("Collect Workstation Images...");
    setMenuAction( _toolsWorkImageMI, _menuActions.getAction(MenuEnum.WORK_IMAGE) );
    _toolsDiagImageMI.setText("Collect Diagnostic Image");
    setMenuAction( _toolsDiagImageMI, _menuActions.getAction(MenuEnum.DIAGNOSTIC_IMAGE) );
    _toolsRepairImageMI.setText("Collect Repair Image");
    setMenuAction( _toolsRepairImageMI, _menuActions.getAction(MenuEnum.REPAIR_IMAGE) );
    _toolsSelfTestMI.setText("Run Selftest");
    setMenuAction( _toolsSelfTestMI, _menuActions.getAction(MenuEnum.SELF_TEST) );
    _toolsServiceMI.setText("Service Menu");
    setMenuAction( _toolsServiceMI, _menuActions.getAction(MenuEnum.SERVICE_MENU) );
    _toolsToolboxMI.setText("Toolbox");
    setMenuAction( _toolsToolboxMI, _menuActions.getAction(MenuEnum.TOOLBOX) );
    _toolsOptionsMI.setText("Options...");
    setMenuAction( _toolsOptionsMI, _menuActions.getAction(MenuEnum.OPTIONS) );
    _toolsMenu.add(_toolsWorkImageMI);
    _toolsMenu.add(_toolsDiagImageMI);
    _toolsMenu.add(_toolsRepairImageMI);
    _toolsMenu.addSeparator();
    _toolsMenu.add(_toolsSelfTestMI);
    _toolsMenu.add(_toolsServiceMI);
    _toolsMenu.add(_toolsToolboxMI);
    _toolsMenu.addSeparator();
    _toolsMenu.add(_toolsOptionsMI);

    // Help Menu
    _helpMenu.setMnemonic('H');
    _helpMenu.setText("Help");
    _helpContentsMI.setText("Contents");
    setMenuAction( _helpContentsMI, _menuActions.getAction(MenuEnum.HELP_CONTENTS) );
    _helpAboutMI.setText("About");
    setMenuAction( _helpAboutMI, _menuActions.getAction(MenuEnum.HELP_ABOUT) );
    _helpMenu.add(_helpContentsMI);
    _helpMenu.add(_helpAboutMI);

    // Build the menu bar
    _menuBar.add(_fileMenu);
    _menuBar.add(_systemMenu);
    _menuBar.add(_panelMenu);
    _menuBar.add(_programmingMenu);
    _menuBar.add(_toolsMenu);
    _menuBar.add(_helpMenu);
    this.setJMenuBar(_menuBar);

    // Toolbar
    _toolBar.setFloatable(false);
    _toolBar.add(_menuActions.getAction(MenuEnum.OPEN_PANEL_PROGRAM));
    _toolBar.add(_menuActions.getAction(MenuEnum.STARTUP_AUTOMATIC));
    _toolBar.addSeparator();
    _toolBar.add(_menuActions.getAction(MenuEnum.LOAD_PANEL));
    _toolBar.add(_menuActions.getAction(MenuEnum.UNLOAD_PANEL));
    _toolBar.add(_menuActions.getAction(MenuEnum.ALIGN_AND_MAP));
    _toolBar.addSeparator();
    _toolBar.add(_menuActions.getAction(MenuEnum.HELP_CONTENTS));

    // Programmer Toolbar
    _programmerToolsPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Programming Tools "));
    _programmerToolBar.setBorder(BorderFactory.createEmptyBorder(0,15,0,15));
    _programmerToolBar.setFloatable(false);
    _programmerToolBar.setOrientation(JToolBar.VERTICAL);
    _programmerToolBar.addSeparator(new Dimension(0, 25));
    _programmerToolBar.add(_menuActions.getAction(MenuEnum.CADLINK));
    _programmerToolBar.addSeparator();
    _programmerToolBar.add(_menuActions.getAction(MenuEnum.CHECKCAD));
    _programmerToolBar.addSeparator();
    _programmerToolBar.add(_menuActions.getAction(MenuEnum.TUNER));
    _programmerToolBar.addSeparator();
    _programmerToolBar.add(_menuActions.getAction(MenuEnum.REVIEW_DEFECTS));
    _programmerToolBar.addSeparator();
    _programmerToolBar.add(_menuActions.getAction(MenuEnum.REVIEW_MEASUREMENTS));
    _programmerToolsPanel.add(_programmerToolBar, null);

    // Top action panel contents
    _topActionPanel.setLayout(_topActionPanelLayout);
    _topActionPanel.add(_autoTestPanel, BorderLayout.CENTER);

    // Bottom action panel contents
    _bottomActionSplitPane.setResizeWeight(1);  // make left component get extra space
    _bottomActionSplitPane.add(_statusPanel, JSplitPane.LEFT);
    _bottomActionSplitPane.add(_instantHelpPanel, JSplitPane.RIGHT);
    _bottomActionPanel.setLayout(_bottomActionPanelLayout);
    _bottomActionPanel.add(_bottomActionSplitPane, BorderLayout.CENTER);

    // Action panel contents
    _actionPanel.setLayout(_actionPanelLayout);
    _actionSplitPane.setResizeWeight(1);  // make top component get extra space
    _actionSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
    _actionSplitPane.add(_topActionPanel, JSplitPane.TOP);
    _actionSplitPane.add(_bottomActionPanel, JSplitPane.BOTTOM);
    _actionPanel.add(_actionSplitPane, BorderLayout.CENTER);

    // Center panel contents
    _centerPanel.setLayout(_centerPanelLayout);
    _centerPanel.add(_programmerToolsPanel, BorderLayout.WEST);
    _centerPanel.add(_actionPanel, BorderLayout.CENTER);

    _contentPane.setLayout(_borderLayout1);
    _contentPane.setBorder(BorderFactory.createEmptyBorder(5,0,0,0));
    _contentPane.add(_toolBar, BorderLayout.NORTH);
    _contentPane.add(_centerPanel, BorderLayout.CENTER);
    this.setSize(new Dimension(800, 700));
    this.setTitle("5DX");
  }

  /**
   * Application start up method.  Asks the user to log in before the application
   * can start.
   */
  public void start()
  {
    // Show the logon dialog first.
    //_currentUser = _noUser;
//    this._logonMI_actionPerformed(null);
    // Update the user name in the status pane
    //_statusPanel.setUserName(_currentUser.getUsername());
  }

  /**
   * This method sets the frame in an inactive state if the flag is true and
   * active if false.  A busy cursor is displayed when inactive.
   * All mouse and keyboard input is absorbed by the glass pane when it is set
   * to visible.
   */
  private void playDead( boolean flag )
  {
    if (flag)
    {
      SwingUtilities.invokeLater( new Runnable()
      {
        public void run()
        {
          // Set the glass pane visible to absorb all input and set the busy cursor.
          MainMenuFrame.this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
          MainMenuFrame.this.getGlassPane().setVisible(true);
          MainMenuFrame.this.getGlassPane().requestFocus();
        }
      });
    }
    else
    {
      SwingUtilities.invokeLater( new Runnable()
      {
        public void run()
        {
          // Set the glass pane invisible to pass all input and set the default cursor.
          MainMenuFrame.this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
          MainMenuFrame.this.getGlassPane().setVisible(false);
        }
      });
    }
  }

  /**
   * Set the application title to include the panel program information.
   *
   * @param   panelName - panel program name.
   * @param   boardName - current board if defined.
   * @param   version - panel program version.
   */
  private void setApplicationTitle( String panelName, String boardName, String version )
  {
    StringBuffer buf = new StringBuffer("5DX");
    if (panelName != null)
      buf.append(" - " + panelName);
    if (boardName != null)
      buf.append(" : " + boardName);
    if (version != null)
      buf.append(" : " + version);
    final String title = buf.toString();
    SwingUtilities.invokeLater( new Runnable()
    {
      public void run()
      {
        MainMenuFrame.this.setTitle(title);

      }
    });
  }

  /**
   * "View" method.
   * Method called by the data model to tell the view to update with new data.
   * Update the current panel program.
   */
  public void updatePanelView()
  {
    if (_model != null)
    {
      setApplicationTitle(_model.getPanelProgram(), _model.getBoardName(), _model.getPanelProgramVersion());
    }
  }

  /**
   * "View" method.
   * Method called by the data model to tell the view to update with new data.
   * Update the current user.
   */
  public void updateUserView()
  {
    if (_model != null)
    {
      _statusPanel.setUserName(_model.getUser().getName());
    }
  }

  /**
   * "View" method.
   * Method called by the data model to tell the view to update with new data.
   * Update the status message.
   */
  public void updateStatusView()
  {
    if (_model != null)
    {
      _statusPanel.setStatus(_model.getStatus());  // Update status
    }
  }

  /**
   * "View" method.
   * Method called by the data model to tell the view to update with new data.
   * Notify to enable the interface.
   *
   * @param   message - String containing type of notify.
   * @param   arg - optional Object sent by the calling method.
   */
  public void updateNotifyView(String message, Object arg)
  {
    playDead(false);
  }

  /**
   * "Worker Thread" method.
   * This method should be called from the worker thread to signal the completion
   * of some work.  The completion message indicated what job has fininshed.  The
   * optional argument can pass back additional information.
   *
   * @param   message - String indicating what job has fininshed.
   * @param   arg - Object containing an optional additional information.
   */
  public void jobComplete(String message, Object arg)
  {
  }

  // **************************** Callback methods ****************************

  /**
   * File | Open Panel Program... action performed
   * Callback for the open panel program menu item.
   * This method reads the available panel programs on the system and presents
   * them to the user in a list box.  After selecting one, the program is loaded
   * using the machine thread.
   */
  public void _fileOpenPanelMI_actionPerformed(ActionEvent e)
  {
    Object [] panels = _controller.getAvailablePanelPrograms();
    if (panels == null)
    {
      String msg = MessageKeys.getString(MessageKeys.NO_PANEL_PROGRAMS);
      String title = MessageKeys.getString(MessageKeys.NO_PANEL_PROGRAMS_TITLE);
      LocalizedString localMsg = new LocalizedString(msg, null);
      LocalizedString localTitle = new LocalizedString(title, null);
      reportError(localMsg, localTitle);
      return;
    }

    String currentPanelProgram = null;
    // Create the dialog.
    OpenPanelDialog dlg = new OpenPanelDialog(this, panels);
    com.agilent.guiUtil.SwingUtils.centerOnComponent(dlg, this);
    int returnVal = dlg.showDialog();
    if (returnVal == JOptionPane.OK_OPTION)
    {
      currentPanelProgram = dlg.getSelectedFile();
    }

    if (currentPanelProgram == null)
    {
      return;  // must be a cancel
    }

    playDead(true);  // disable the interface
    // Tell the controller about the new panel program to load.
    _controller.loadPanelProgram(currentPanelProgram);
//    String statusMsg = ((loaded) ? MessageKeys.LOAD_CAD_SUCCESSFUL : MessageKeys.LOAD_CAD_FAILED);
//    _statusPanel.setStatus(MessageKeys.getString(statusMsg));  // Update status
//    playDead(false);  // enable the interface
  }

  /**
   * File | Logon action performed
   * Callback for the logon menu item.
   */
  public void _fileLogonMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * File | Logout action performed
   * Callback for the logout menu item.
   */
  public void _fileLogoutMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * File | Change Password... action performed
   * Callback for the change password menu item.
   */
  public void _fileChangePasswordMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * File | Register User... action performed
   * Callback for the new user menu item.
   */
  public void _fileRegisterUserMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * File | Exit action performed
   * Call the application exit method.
   */
  public void _fileExitMI_actionPerformed(ActionEvent e)
  {
    _parent.exitApp();
  }

  /**
   * System | Startup action performed
   * DOS
   */
  public void _systemStartupMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * System | Shutdown | Short Term action performed
   */
  public void _systemShutdownShortMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * System | Shutdown | Long Term action performed
   */
  public void _systemShutdownLongMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * System | Panel Handling | Home Rails action performed
   */
  public void _systemPanelHandlingHomeRailsMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * System | Panel Handling | Reset action performed
   */
  public void _systemPanelHandlingResetMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * System | Reset 5DX action performed
   */
  public void _systemReset5DXMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * System | Reset IAS action performed
   */
  public void _systemResetIASMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * System | 5DX Status action performed
   */
  public void _systemStatus5DXMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Panel | Load action performed
   */
  public void _panelLoadMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Panel | Unload action performed
   */
  public void _panelUnloadMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Panel | Align and Map action performed
   */
  public void _panelAlignAndMapMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Panel | Align | Automatic action performed
   */
  public void _panelAlignAutoMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Panel | Align | Manual action performed
   */
  public void _panelAlignManualMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Panel | Align | Display Matrix action performed
   */
  public void _panelAlignDisplayMatMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Panel | Map | Automatic action performed
   */
  public void _panelMapAutoMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Panel | Map | Manual action performed
   */
  public void _panelMapManualMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Panel | Map | Check Surface Map action performed
   */
  public void _panelMapCheckSurMapMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Panel | Map | Verify Laser Repeatability action performed
   */
  public void _panelMapVerifyLaserMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Panel | Set Stage Speed action performed
   */
  public void _panelStageSpeedMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Panel | Manual Load action performed
   */
  public void _panelManualLoadPanelMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Programming | CadLink action performed
   */
  public void _programmingCadlinkMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Programming | Algorithm Tuner action performed
   */
  public void _programmingTunerMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Programming | Check CAD action performed
   */
  public void _programmingCheckcadMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Programming | Setup Map Points action performed
   */
  public void _programmingMapPointsMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Programming | View Focus action performed
   */
  public void _programmingViewFocusMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Programming | Setup X-out Detection action performed
   */
  public void _programmingXoutMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Programming | Review Defects action performed
   * DOS
   */
  public void _programmingReveiwDefMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Programming | Review Measurements action performed
   */
  public void _programmingReviewMeasMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Programming | Defect Reporter action performed
   */
  public void _programmingDefectReporterMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Tools | Collect Workstation Images action performed
   */
  public void _toolsWorkImageMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Tools | Collect Diagnostic Image action performed
   */
  public void _toolsDiagImageMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Tools | Collect Repair Images action performed
   */
  public void _toolsRepairImageMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Tools | Run Selftest action performed
   */
  public void _toolsSelfTestMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Tools | Service Menu action performed
   */
  public void _toolsServiceMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Tools | Toolbox action performed
   */
  public void _toolsToolboxMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Tools | Options... action performed
   */
  public void _toolsOptionsMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Help | Contents action performed
   * Start a browser with the initial help page.
   */
  public void _helpContentsMI_actionPerformed(ActionEvent e)
  {
  }

  /**
   * Help | About action performed
   * Show the application about box.
   */
  public void _helpAboutMI_actionPerformed(ActionEvent e)
  {
    MainMenu_AboutBox dlg = new MainMenu_AboutBox(this);
    com.agilent.guiUtil.SwingUtils.centerOnComponent(dlg, this);
    dlg.setModal(true);
    dlg.show();
  }

  /**
   * Overridden so we can exit when window is closed.
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      _fileExitMI_actionPerformed(null);
    }
  }

  /**
   * Assigns an MenuAction to a JMenuItem, removing the icon and setting
   * the mnemonic.
   */
  private void setMenuAction( JMenuItem mi, MenuAction action )
  {
    mi.setAction(action);
    mi.setIcon(null);
    mi.setMnemonic(action.getMnemonic());
    mi.setName( action.getName() );
  }

  /**
   * Set the controller class on this class.
   *
   * @param   controller - SystemController instance to use as the controller.
   */
  public void setController(MainMenuController controller)
  {
    _controller = controller;
  }

  /**
   * Set the data model instance.
   *
   * @param   model - MainMenuData instance to use as the data model.
   */
  public void setModel(MainMenuData model)
  {
    _model = model;
  }

  /**
   * ControllerObserverInt method.
   * Report an error from the controller to the user.
   *
   * @param   err - LocalizedString containing the message key and arguements.
   */
  public void reportError(LocalizedString error, LocalizedString title)
  {
    if (error == null)
      return;
    MessageDialog.showErrorDialog(this, error, title);
  }

  /**
   * ControllerObserverInt method.
   * Report a warning from the controller to the user.
   *
   * @param   warn - LocalizedString containing the message key and arguements.
   */
  public void reportWarning(LocalizedString warning, LocalizedString title)
  {
    if (warning == null)
      return;
    MessageDialog.showWarningDialog(this, warning, title);
  }

  /**
   * ControllerObserverInt method.
   * Report an exception from the controller to the user.
   *
   * @param   e - Exception to be reported.
   */
  public void reportException( Exception e )
  {
    if (e != null)
      MessageDialog.reportException(this, e);
  }

  /**
   * PanelQueueInt method.
   * Return the next panel in the queue.
   *
   * @return the next PanelEntry from the queue.
   */
  public PanelEntry removePanel()
  {
    /** @todo SLA tie this to the autotestpanel. */
    return null;
  }

  /**
   * PanelQueueInt method.
   * Return true if a panel is available on the queue.
   *
   * @return true if a panel is available to be removed.
   */
  public boolean panelAvailable()
  {
    /** @todo SLA tie this to the autotestpanel. */
    return false;
  }

  // GUI components
  private JPanel _contentPane;
  private BorderLayout _borderLayout1 = new BorderLayout(5, 5);
  private JMenuBar _menuBar = new JMenuBar();
  private JMenu _fileMenu = new JMenu();
  private JMenuItem _fileOpenPanelMI = new JMenuItem();
  private JMenuItem _fileLogonMI = new JMenuItem();
  private JMenuItem _fileLogoutMI = new JMenuItem();
  private JMenuItem _fileChangePasswordMI = new JMenuItem();
  private JMenuItem _fileRegisterUserMI = new JMenuItem();
  private JMenuItem _fileExitMI = new JMenuItem();
  private JMenu _helpMenu = new JMenu();
  private JMenuItem _helpContentsMI = new JMenuItem();
  private JMenuItem _helpAboutMI = new JMenuItem();
  private JMenu _systemMenu = new JMenu();
  private JMenu _systemShutdownMenu = new JMenu();
  private JMenu _systemPanelHandlingMenu = new JMenu();
  private JMenuItem _systemStartupMI = new JMenuItem();
  private JMenuItem _systemShutdownShortMI = new JMenuItem();
  private JMenuItem _systemShutdownLongMI = new JMenuItem();
  private JMenuItem _systemPanelHandlingHomeRailsMI = new JMenuItem();
  private JMenuItem _systemPanelHandlingResetMI = new JMenuItem();
  private JMenuItem _systemReset5DXMI = new JMenuItem();
  private JMenuItem _systemResetIASMI = new JMenuItem();
  private JMenuItem _systemStatus5DXMI = new JMenuItem();
  private JMenu _panelMenu = new JMenu();
  private JMenu _panelAlignMenu = new JMenu();
  private JMenu _panelMapMenu = new JMenu();
  private JMenuItem _panelLoadMI = new JMenuItem();
  private JMenuItem _panelUnloadMI = new JMenuItem();
  private JMenuItem _panelAlignAndMapMI = new JMenuItem();
  private JMenuItem _panelAlignAutoMI = new JMenuItem();
  private JMenuItem _panelAlignManualMI = new JMenuItem();
  private JMenuItem _panelAlignDisplayMatMI = new JMenuItem();
  private JMenuItem _panelMapAutoMI = new JMenuItem();
  private JMenuItem _panelMapManualMI = new JMenuItem();
  private JMenuItem _panelMapCheckSurMapMI = new JMenuItem();
  private JMenuItem _panelMapVerifyLaserMI = new JMenuItem();
  private JMenuItem _panelStageSpeedMI = new JMenuItem();
  private JMenuItem _panelManualLoadMI = new JMenuItem();
  private JMenu _programmingMenu = new JMenu();
  private JMenuItem _programmingCadlinkMI = new JMenuItem();
  private JMenuItem _programmingTunerMI = new JMenuItem();
  private JMenuItem _programmingCheckcadMI = new JMenuItem();
  private JMenuItem _programmingMapPointsMI = new JMenuItem();
  private JMenuItem _programmingViewFocusMI = new JMenuItem();
  private JMenuItem _programmingXoutMI = new JMenuItem();
  private JMenuItem _programmingReveiwDefMI = new JMenuItem();
  private JMenuItem _programmingReviewMeasMI = new JMenuItem();
  private JMenuItem _programmingDefectReporterMI = new JMenuItem();
  private JMenu _toolsMenu = new JMenu();
  private JMenuItem _toolsWorkImageMI = new JMenuItem();
  private JMenuItem _toolsDiagImageMI = new JMenuItem();
  private JMenuItem _toolsRepairImageMI = new JMenuItem();
  private JMenuItem _toolsSelfTestMI = new JMenuItem();
  private JMenuItem _toolsServiceMI = new JMenuItem();
  private JMenuItem _toolsToolboxMI = new JMenuItem();
  private JMenuItem _toolsOptionsMI = new JMenuItem();
  private JToolBar _toolBar = new JToolBar();
  private JPanel _programmerToolsPanel = new JPanel();
  private JToolBar _programmerToolBar = new JToolBar();
  private JPanel _centerPanel = new JPanel();
  private BorderLayout _centerPanelLayout = new BorderLayout(5, 5);
  private JPanel _actionPanel = new JPanel();
  private JSplitPane _actionSplitPane = new JSplitPane();
  private JPanel _topActionPanel = new JPanel();
  private JPanel _bottomActionPanel = new JPanel();
  private BorderLayout _topActionPanelLayout = new BorderLayout();
  private BorderLayout _bottomActionPanelLayout = new BorderLayout();
  private JSplitPane _bottomActionSplitPane = new JSplitPane();
  private BorderLayout _actionPanelLayout = new BorderLayout();
}
