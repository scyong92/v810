package proto.mainMenu;

import java.rmi.RemoteException;
import com.agilent.util.WorkerThread;
//import com.agilent.xRayTest.util.ServerAdapter;
import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.business.panelDesc.*;
import com.agilent.xRayTest.business.panelInspection.RemoteInspectionInt;
import com.agilent.xRayTest.business.server.RemoteDosShellInt;
import com.agilent.xRayTest.gui.*;  // Adapters
import com.agilent.util.LocalizedString;
/**
 * Title:       MainMenuController
 * Description: This class does the work of controlling the 5DX system.
 * It is the "controller" part of the model-view-controller organization of the
 * MainMenu application.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class MainMenuController implements RemoteExceptionHandlerInt
{
  private MainMenuFrame _observer = null;
  private PanelQueueInt _panelQueue = null;
  private MainMenuData _model = null;

  private ServerAdapter _server = null;
  private RemoteInspectionInt _machinePanelInspectInt = null;
  private RemotePanelDescInt _panelDescriptionInt = null;
  private RemoteDosShellInt _dosShell = null;

  private PanelDescAdapter _panelDescription = null;

//  private JobControl _jobs = new JobControl();
  private WorkerThread _workerThread = new WorkerThread("ControlThread");

  /**
   * Constructor.
   *
   * @param   observer - ControllerObserverInt instance to use.
   * @throws  IllegalArguementException if the observer is null.
   */
  public MainMenuController(MainMenuFrame observer, PanelQueueInt panelQueue)
  {
    if (observer == null)
      throw new IllegalArgumentException("Controller must have an observer.");
    _observer = observer;
    _panelQueue = panelQueue;
  }

  /**
   * Set the data model instance.
   *
   * @param   model - MainMenuData instance to use as the data model.
   */
  public void setModel(MainMenuData model)
  {
    _model = model;
  }

  /**
   * Set the server to communicate to the system.  If the server is not null,
   * get handles to the interfaces that we need.
   *
   * @param   server - ServerAdapter to use.
   */
  public void setServer( ServerAdapter server )
  {
    _server = server;
    if (_server == null)
      return;

    // Save references to the interfaces used.
    try
    {
      _machinePanelInspectInt = _server.getInspectionInt();
      _panelDescriptionInt = _server.getPanelDescInt();
    }
    catch (RemoteException re)
    {
      _observer.reportException(re);
    }

    _panelDescription = new PanelDescAdapter(_panelDescriptionInt, this);
  }

  /**
   * RemoteExceptionHandlerInt method.
   * Remote exception handler for the Adapter classes.
   */
  public void handleRemoteException(RemoteException re)
  {
    _observer.reportException(re);
  }

  /**
   * Ask the server for a list of the available panel programs and return them
   * as an Object[].
   *
   * @return an array of panel program names.
   */
  public Object[] getAvailablePanelPrograms()
  {
    Object [] panels = null;
    panels = _panelDescription.getPanelNames().toArray();
    return panels;
  }

  /**
   * Load cad for a panel.
   *
   * @param   panelName - String containing the name of the panel.
   */
  public void loadPanelProgram(String panelName)
  {
    if (panelName == null)
      return;

    final String pName = panelName;

    // Create a job to put on the worker thread.
    Runnable job = new Runnable()
    {
      public void run()
      {
        boolean loaded = false;
        java.util.List warnings = new java.util.ArrayList();
        try
        {
          _panelDescription.loadPanelProgram(DatastoreMode.READ_ONLY, pName, warnings);
          loaded = true;
        }
        catch (DatastoreException de)
        {
          _observer.reportException(de);
          loaded = false;
        }
        catch (com.agilent.xRayTest.hardware.HardwareException he)
        {
          _observer.reportException(he);
          loaded = false;
        }

        if (warnings.size() > 0)
        {
          java.util.Iterator iter = warnings.iterator();
          StringBuffer buf = new StringBuffer();
          while (iter.hasNext())
          {
            if (buf.length() != 0)
              buf.append("\n");
            buf.append(iter.next().toString());
            /** @todo SLA uncomment the following line when Localized.properties is updated with the keys. */
            //buf.append(MessageKeys.getString( (com.agilent.util.LocalizedString)iter.next() ));
          }
          String warning = buf.toString();
          String title = MessageKeys.getString(MessageKeys.LOAD_CAD_WARNING_TITLE);
          LocalizedString localWarning = new LocalizedString(warning, null);
          LocalizedString localTitle = new LocalizedString(title, null);
          _observer.reportWarning(localWarning, localTitle);
        }

        PanelAdapter panel = null;
        try
        {
          panel = _panelDescription.getCurrentlyLoadedPanelInt();
        }
        catch (com.agilent.xRayTest.business.PanelProgramNotLoadedException he)
        {
          _observer.reportException(he);
          loaded = false;
        }

        // Save the panel data.
        /** @todo SLA Get panel program version from panel description */
        String version = "1.0";
        String board = null;
        int boardNum = 0;
        java.util.Iterator iter = panel.getBoards().iterator();
        if (iter.hasNext())
        {
          BoardAdapter brd = (BoardAdapter)iter.next();
          board = brd.getName();
          boardNum = 1;
        }
        _model.setPanel(pName, version, board, boardNum);
        String statusMsg = ((loaded) ? MessageKeys.LOAD_CAD_SUCCESSFUL : MessageKeys.LOAD_CAD_FAILED);
        _model.setStatus(MessageKeys.getString(statusMsg));  // Update status
        // Notify any component waiting that we are done.
        _model.notify("loadCAD done", null);
      }
    };
    _workerThread.invokeLater(job);
  }
}
