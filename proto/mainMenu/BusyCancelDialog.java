package proto.mainMenu;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.agilent.xRayTest.images.*;
import com.agilent.guiUtil.*;
import com.agilent.util.*;


/**
 * Title:       BusyCancelDialog
 * Description: This dialog is displayed while a long task is executing.  It has
 * a cancel button to allow the user to abort the task.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class BusyCancelDialog extends JDialog
{
  private static final int _NUM_GIFS = 10;
  private static final String _gifName = "hourglass";
  private static Icon[] _icons = new Icon[_NUM_GIFS];
  static
  {
    for (int i = 0; i < _NUM_GIFS; i++)
    {
      _icons[i] = new ImageIcon(com.agilent.xRayTest.images.Image5DX.class.getResource(_gifName + (i + 1) + ".gif"));
    }
  }
  private String _message = "";

  /**
   * Constructor.
   *
   * @param   frame - parent component.
   * @param   message - String to be displayed in the dialog.
   * @param   title - Dialog title string.
   * @param   modal - true for a modal dialog.
   */
  public BusyCancelDialog(Frame frame, String message, String title, boolean modal)
  {
    super(frame, title, modal);
    _message = message;
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }

    this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    this._iconLbl.setRunning(true);
  }

  /**
   * Create the components.
   */
  void jbInit() throws Exception
  {
    _cancelBtn.setText("Cancel");
    _buttonPanel.add(_cancelBtn, null);

    _iconLbl.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent e)
      {
        _iconLbl.setRunning( !_iconLbl.isRunning() );
      }
    });
    _messageTA.setColumns(40);
    _messageTA.setWrapStyleWord(true);
    _messageTA.setLineWrap(true);
    _messageTA.setBackground(Color.lightGray);
    _messageTA.setEditable(false);
    _messageTA.setText(_message);

    _centerPanelLayout.setHgap(10);
    _centerPanel.setLayout(_centerPanelLayout);
    _centerPanel.add(_iconLbl, BorderLayout.WEST);
    _centerPanel.add(_messageTA, BorderLayout.CENTER);

    _borderLayout1.setVgap(5);
    _panel1.setLayout(_borderLayout1);
    _panel1.setBorder(BorderFactory.createEmptyBorder(10,10,0,10));
    _panel1.add(_centerPanel, BorderLayout.CENTER);
    _panel1.add(_buttonPanel, BorderLayout.SOUTH);
    getContentPane().add(_panel1);
  }

  /**
   * Add an ActionListener to the cancel button.  Adding a listener
   * is the only way the parent compnent will know that the button is pressed.
   * The Escape key is also tied to this ActionListener so that hitting Escape
   * is the same as pressing the cancel button.
   *
   * @param   listener - ActionListener to attach to the cancel button.
   */
  public void addCancelActionListener(ActionListener listener)
  {
    if (listener != null)
    {
      _cancelBtn.addActionListener(listener);
      _cancelBtn.registerKeyboardAction(listener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
  }

  /**
   * Remove an ActionListener from the cancel button.
   *
   * @param   listener - ActionListener to remove from the cancel button.
   */
  public void removeCancelActionListener(ActionListener listener)
  {
    if (listener != null)
    {
      _cancelBtn.removeActionListener(listener);
      _cancelBtn.unregisterKeyboardAction(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true));
    }
  }

// DEBUG
  public static void main(String[] args)
  {
    try
    {
      // Make sure the look and feel is set before any components are created.
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch(Exception e)
    {
      Assert.logException(e);
    }

    BusyCancelDialog dlg = new BusyCancelDialog(null, "This is the busy message.", "Busy Dialog", true);
    com.agilent.guiUtil.SwingUtils.setScreenPosition(dlg,0.5,0.25);
    dlg.addCancelActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        System.exit(0);
      }
    });
    dlg.show();
  }
// DEBUG
  private JPanel _panel1 = new JPanel();
  private BorderLayout _borderLayout1 = new BorderLayout();
  private AnimatedLabel _iconLbl = new AnimatedLabel(_icons);
  private JTextArea _messageTA = new JTextArea();
  private JPanel _centerPanel = new JPanel();
  private BorderLayout _centerPanelLayout = new BorderLayout();
  private JPanel _buttonPanel = new JPanel();
  private JButton _cancelBtn = new JButton();
}
