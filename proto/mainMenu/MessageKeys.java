package proto.mainMenu;

import com.agilent.util.LocalizedString;
import com.agilent.xRayTest.util.StringLocalizer;

/**
 * Title:        MessageKeys
 * Description:  This class contains the LocalizedString message keys used by the main menu.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

class MessageKeys
{
  public static final String NO_PANEL_PROGRAMS = "MM_NO_PANEL_PROGRAMS_KEY";
  public static final String NO_PANEL_PROGRAMS_TITLE = "MM_NO_PANEL_PROGRAMS_TITLE_KEY";
  public static final String LOAD_CAD_WARNING_TITLE = "MM_LOAD_CAD_WARNING_TITLE_KEY";
  public static final String LOAD_CAD_SUCCESSFUL = "Cad Loaded Successful";
  public static final String LOAD_CAD_FAILED = "MM_LOAD_CAD_FAILED";

  // constructor is private so that an instance can't be created.
  private MessageKeys()
  {
  }

  public static String getString( String key )
  {
    return getString( new LocalizedString(key, null) );
  }

  public static String getString( String key, Object [] args )
  {
    return getString( new LocalizedString(key, args) );
  }

  public static String getString( LocalizedString key )
  {
    String msg = StringLocalizer.keyToString(key);
    return msg;
  }
}
