package proto.mainMenu;

import proto.mainMenu.menuActions.MenuEnable;

/**
 * Title:       User
 * Description: This class holds the information about a logged in user of the
 * MainMenu application.
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class User
{
  private String _name = null;
  private MenuEnable _menuEnable = null;

  public User( String name, MenuEnable menuEnable )
  {
    // Assert(menuEnable)
    _name = ((name == null) ? "none" : name.toUpperCase());
    _menuEnable = menuEnable;
  }

  /**
   * Return the user name.
   */
  public String getName()
  {
    return _name;
  }

  /**
   * Return the menu enable.
   */
  public MenuEnable getMenuEnable()
  {
    return _menuEnable;
  }
}
