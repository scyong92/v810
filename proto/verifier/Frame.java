package proto.verifier;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.text.html.*;
import javax.swing.text.Element;
import javax.swing.text.StyleContext;
import javax.swing.event.*;
import java.net.*;
import java.io.*;



import com.agilent.xRayTest.business.server.*;
import com.agilent.xRayTest.business.panelDesc.*;
import com.agilent.xRayTest.datastore.*;


public class Frame extends JFrame
{
  private JPanel contentPane;
  private JPanel jPanel1 = new JPanel();
  private JPanel jPanel2 = new JPanel();
  private JPanel jPanel3 = new JPanel();
  private JPanel jPanel4 = new JPanel();
  private JLabel jLabel1 = new JLabel();
  private FlowLayout flowLayout1 = new FlowLayout();
  private GridLayout gridLayout1 = new GridLayout();
  private GridLayout gridLayout2 = new GridLayout();
  private FlowLayout flowLayout2 = new FlowLayout();
  private JButton _exitButton = new JButton();
  private JButton _loadButton = new JButton();
  private JScrollPane jScrollPane1 = new JScrollPane();
  private JEditorPane _editorPane = new JEditorPane();
  private JPanel jPanel5 = new JPanel();
  private JLabel _statusLabel = new JLabel();
  private BorderLayout borderLayout1 = new BorderLayout();
  private BorderLayout borderLayout2 = new BorderLayout();

  /**Construct the frame*/
  public Frame()
  {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  /**Component initialization*/
  private void jbInit() throws Exception
  {
    //setIconImage(Toolkit.getDefaultToolkit().createImage(Frame.class.getResource("[Your Icon]")));
    contentPane = (JPanel) this.getContentPane();
    contentPane.setLayout(borderLayout2);
    this.setSize(new Dimension(634, 560));
    this.setTitle("Frame Title");
    jPanel1.setLayout(flowLayout2);
    jPanel2.setLayout(gridLayout2);
    jPanel3.setLayout(gridLayout1);
    jPanel4.setLayout(flowLayout1);
    jLabel1.setFont(new java.awt.Font("Dialog", 1, 16));
    jLabel1.setText("Program Advisor");
    _exitButton.setText("Print");
    _loadButton.setText("Save");
    _loadButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        loadButton_actionPerformed(e);
      }
    });
    _editorPane.setEditable(false);

    String s = null;
    URL url;

    try
    {
      s = "file:" + System.getProperty("user.dir")
                  + System.getProperty("file.separator")
                  + "ASAPverificationreport.htm";
      url = new URL(s);
      _editorPane.setPage(url);
      //_editorPane.addHyperlinkListener(new Hyperactive());

    }
    catch (Exception e)
    {
      System.err.println("couldn't create url");
    }
    jPanel5.setLayout(borderLayout1);
    _statusLabel.setBorder(BorderFactory.createLoweredBevelBorder());
    _statusLabel.setText("status bar");
    contentPane.add(jPanel1, BorderLayout.NORTH);
    jPanel1.add(jLabel1, null);
    contentPane.add(jPanel2, BorderLayout.CENTER);
    jPanel2.add(jScrollPane1, null);
    jScrollPane1.getViewport().add(_editorPane, null);

    contentPane.add(jPanel3, BorderLayout.WEST);
    jPanel3.add(jPanel4, null);
    jPanel4.add(_loadButton, null);
    jPanel4.add(_exitButton, null);
    contentPane.add(jPanel5, BorderLayout.SOUTH);
    jPanel5.add(_statusLabel, BorderLayout.CENTER);
  }
  /**Overridden so we can exit when window is closed*/
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      System.exit(0);
    }
  }

  void loadButton_actionPerformed(ActionEvent e)
  {
    /*
    try
    {
      // real code would connect to the server - for proto just new in
      RemoteServer server = new RemoteServer();
      RemoteBusinessFactoryInt factoryInt = server.getRemoteBusinessFactoryInt();
      RemotePanelDescInt panelDescInt = factoryInt.getRemotePanelDescInt();
      java.util.List warnings = new ArrayList();
      _statusLabel.setText("loading cad...");
      RemotePanelInt panelInt = panelDescInt.loadPanelCAD(DatastoreMode.READ_ONLY, "NEPCON_WIDE", warnings);
      _statusLabel.setText("done loading cad");

      String name =  panelInt.getName();
      System.out.println("the name is " + name);

      Iterator it = warnings.iterator();
      while (it.hasNext());
      {
        String warning = (String)it.next();
        _statusLabel.setText(warning + "\n");
      }

      String panelName = panelInt.getName();
      _statusLabel.setText("panel name: " + panelName + "\n");
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    */

    HTMLDocument myDoc = new HTMLDocument();



    try
    {

      Element elem;
//      System.out.println("before insert after end");
//      Element root = myDoc.getDefaultRootElement();
//      System.out.println("after get default root");
      //StyleContext sc = new StyleContext();
      //Element bidiRoot = myDoc.getBidiRootElement();
      //Element elemAtPos = myDoc.getCharacterElement(0);
      //myDoc.insertString(0,"hi", null);
      HTMLEditorKit kit= new HTMLEditorKit();
     // kit.insertHTML();
      javax.swing.text.Document d = kit.createDefaultDocument();


      PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("foo.html")));
      System.out.println("after declaring new printwriter");

    Element element = myDoc.getDefaultRootElement();
    System.out.println("element is " + element.toString());
    myDoc.insertAfterEnd(element, "k");


    //myDoc.insertAfterStart(element, "hello there");
    //myDoc.insertAfterEnd(element, "hello there");
    //myDoc.insertString();
//    myDoc.
      HTMLWriter htmlwriter = new HTMLWriter(out, myDoc);

      htmlwriter.write();
    }
    catch (Exception ex)
    {
    }
/*
    HTMLDoc htmldoc = new HTMLDoc("c:\\temp\\test.html");
    htmldoc.WriteHTMLTag("<html>");
    htmldoc.WriteHTMLTitle("Program Advisor Report");
    htmldoc.WriteHTMLTag("<body>");
    htmldoc.WriteHTMLHeader("Summary");
    htmldoc.WriteHTMLLink("http:\\\\www.yahoo.com", "Yahoo");
    htmldoc.WriteHTMLAnchor("anchor1");


    htmldoc.WriteHTMLTag("</html>");
    htmldoc.close();

    */




/*
    File file = new File("test.html");
    try
    {
      FileWriter fw = new FileWriter("test.html");
      PrintWriter printOut = new PrintWriter(fw);
      printOut.println("<html>");
      printOut.println("<body>");
      printOut.println("<title>Program Advisor Report</title>");
      printOut.println("Program Advisor Report");
      printOut.println("<br>Summary:");
      printOut.println("</body>");
      printOut.println("</text>");
      fw.close();
    }
    catch (Exception exc)
    {
      // do something
    }

*/

  }
}