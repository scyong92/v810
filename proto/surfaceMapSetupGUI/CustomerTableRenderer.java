package proto.surfaceMapSetupGUI;

import java.awt.*;
import javax.swing.*;
import java.awt.Component;
import javax.swing.table.TableCellRenderer;

class CustomerTableRenderer extends JLabel implements TableCellRenderer
{

  public CustomerTableRenderer()
  {
  }

  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    setOpaque(true);
    setHorizontalAlignment(JLabel.CENTER);
    setBorder(null);
    if (isSelected)
    {
      Integer pointNumber = (Integer)table.getModel().getValueAt(row,0);
      if (pointNumber.intValue() == SurfaceMapSetupFrame.referencePointNumber)
      {
        // this is the case where the Reference point is selected
        setForeground(table.getSelectionForeground());
        setBackground(table.getSelectionBackground());
        setBorder(BorderFactory.createLineBorder(new Color(181, 228, 255), 3));
      }
      else
      {
        setForeground(table.getSelectionForeground());
        setBackground(table.getSelectionBackground());
      }
    }
    else
    {
      setForeground(Color.black);
      setBackground(Color.white);
      Integer pointNumber = (Integer)table.getModel().getValueAt(row,0);
      if (pointNumber.intValue() == SurfaceMapSetupFrame.referencePointNumber)
      {
        setBackground(new Color(181, 228, 255)); // pale blue for reference point
      }
//      else
//        if (row < 25)
//          setBackground(SystemColor.info);
    }
    if (value != null)
    {
      if (value instanceof Float)
      {
        String floatStr = value.toString();

        setText(floatStr.substring(0, floatStr.indexOf(".") + 2));
      }
      else
        setText(value.toString());
    }
    return this;
  }
}
