package proto.surfaceMapSetupGUI;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import com.borland.jbcl.layout.*;
import javax.swing.border.*;

import com.agilent.guiUtil.*;
import java.io.*;

public class SurfaceMapSetupFrame extends JFrame
{
  public static int referencePointNumber = 0; // zero means there is none yet.

  private static String mainInstantHelp = "file:" + System.getProperty("user.dir") + System.getProperty("file.separator") + "mainhelp.html";
  private static String cadGraphicsURL = "file:" + System.getProperty("user.dir") + System.getProperty("file.separator") + "cadgraphics.html";
  private static String adjustPointsInstantHelp = "file:" + System.getProperty("user.dir") + System.getProperty("file.separator") + "adjustpointshelp.html";
  private static String verifyFocusInstantHelp = "file:" + System.getProperty("user.dir") + System.getProperty("file.separator") + "verifyfocushelp.html";
  private static String addPointsInstantHelp = "file:" + System.getProperty("user.dir") + System.getProperty("file.separator") + "addpointshelp.html";
  private static String configureOffsetsInstantHelp = "file:" + System.getProperty("user.dir") + System.getProperty("file.separator") + "configureoffsetshelp.html";

  private SurfaceMapTableModel tableModel;
  private boolean listeningToCadClicks = false;
  private boolean listeningForReferenceClick = false;
  private int currentMaxPoints = 0;

  private JPanel contentPane;
  private JMenuBar jMenuBar1 = new JMenuBar();
  private JMenu jMenuFile = new JMenu();
  private JMenuItem jMenuFileExit = new JMenuItem();
  private JMenu jMenuHelp = new JMenu();
  private JMenuItem jMenuHelpAbout = new JMenuItem();
  private JToolBar jToolBar = new JToolBar();
  private ImageIcon image1;
  private ImageIcon image2;
  private ImageIcon image3;
  private ImageIcon zoomInimage;
  private ImageIcon zoomOutimage;
  private ImageIcon panimage;
  private JMenu jMenu1 = new JMenu();
  private JMenu jMenu2 = new JMenu();
  private JMenu jMenu3 = new JMenu();
  private JMenuItem jMenuItem1 = new JMenuItem();
  private JMenuItem jMenuItem2 = new JMenuItem();
  private JMenuItem jMenuItem3 = new JMenuItem();
  private JMenuItem jMenuItem4 = new JMenuItem();
  private JMenuItem jMenuItem5 = new JMenuItem();
  private JMenuItem jMenuItem6 = new JMenuItem();
  private JMenuItem jMenuItem7 = new JMenuItem();
  private JMenuItem jMenuItem8 = new JMenuItem();
  private JMenuItem jMenuItem9 = new JMenuItem();
  private JMenuItem jMenuItem10 = new JMenuItem();
  private JMenuItem jMenuItem11 = new JMenuItem();
  private JMenuItem jMenuItem12 = new JMenuItem();
  private JMenuItem jMenuItem13 = new JMenuItem();
  private JPopupMenu tablePopupMenu = new JPopupMenu();
  private JMenuItem setReferencePointPopupMenuItem = new JMenuItem();
  private JMenuItem setPadHeightOffsetPopupMenuItem = new JMenuItem();

  private JPanel jPanel1 = new JPanel();
  private JComboBox componentZoomComboBox = new JComboBox();
  private JButton jButton9 = new JButton();
  private JButton jButton8 = new JButton();
  private JButton jButton7 = new JButton();
  private JButton jButton6 = new JButton();
  private JButton panButton = new JButton();
  private JButton zoomOutButton = new JButton();
  private JButton zoomInButton = new JButton();
  private JButton jButton2 = new JButton();
  private XYLayout xYLayout1 = new XYLayout();
  private Border border1;
  private JLabel statusBar = new JLabel();
  private JPanel verifyFocusPanel = new JPanel();
  private XYLayout xYLayout10 = new XYLayout();
  private ButtonGroup buttonGroup1 = new ButtonGroup();
  private BorderLayout borderLayout2 = new BorderLayout();
  private XYLayout xYLayout11 = new XYLayout();
  private JButton adjustPointsPrevPointButton = new JButton();
  private JPanel surfaceMapAdjustPointsPanel = new JPanel();
  private JButton adjustPointsGotoPointsButton = new JButton();
  private JTextField adjustPointsGoToTextField = new JTextField();
  private JButton adjustPointsNextPointButton = new JButton();
  private JButton adjustPointsCancelButton = new JButton();
  private JLabel jLabel19 = new JLabel();
  private JButton adjustPointsDoneButton = new JButton();
  private JPanel jPanel11 = new JPanel();
  private JSplitPane jSplitPane1 = new JSplitPane();
  private JPanel jPanel3 = new JPanel();
  private JPanel jPanel2 = new JPanel();
  private JEditorPane cadGraphicsEditorPane = new JEditorPane();
  private JScrollPane jScrollPane2 = new JScrollPane();
  private JScrollPane jScrollPane1 = new JScrollPane();
  private JSortTable jTable1 = null;
  private BorderLayout borderLayout1 = new BorderLayout();
  private BorderLayout borderLayout4 = new BorderLayout();
  private JPanel jPanel13 = new JPanel();
  private Border border2;
  private Border border3;
  private BorderLayout borderLayout3 = new BorderLayout();
  private Border border4;
  private JPanel jPanel4 = new JPanel();
  private Border border5;
  private BorderLayout borderLayout6 = new BorderLayout();
  private Border border6;
  private JButton verifyFocusPrevPointButton = new JButton();
  private JTextField verifyFocusGotoTextField = new JTextField();
  private JButton verifyFocusGotoPointButton = new JButton();
  private JButton verifyFocusCancelButton = new JButton();
  private JButton verifyFocusNextPointButton = new JButton();
  private JButton verifyFocusDoneButton = new JButton();
  private JLabel jLabel14 = new JLabel();
  private JPanel jPanel6 = new JPanel();
  private JPanel jPanel9 = new JPanel();
  private JLabel jLabel4 = new JLabel();
  private FlowLayout flowLayout1 = new FlowLayout();
  private JMenu jMenu4 = new JMenu();
  private JMenuItem toggleLiveSnapMenuItem = new JMenuItem();
  private JMenuItem nextFOVMenuItem = new JMenuItem();
  private JMenuItem toggleLightsMenuItem = new JMenuItem();
  private JButton jButton1 = new JButton();
  private JButton jButton3 = new JButton();
  private JButton jButton12 = new JButton();
  private JButton jButton13 = new JButton();
  private JPanel jPanel7 = new JPanel();
  private BorderLayout borderLayout7 = new BorderLayout();
  private JButton deletePointsButton = new JButton();
  private GridLayout gridLayout1 = new GridLayout();
  private JButton jButton11 = new JButton();
  private JButton addPointsButton = new JButton();
  private JButton configureOffsetsButton = new JButton();
  private JButton adjustPointsButton = new JButton();
  private JButton verifyFocusButton = new JButton();
  private JPanel jPanel5 = new JPanel();
  private JPanel combinedPanel = new JPanel();
  private BorderLayout combinedPanelBorderLayout = new BorderLayout();
  private JRadioButton topRadioButton = new JRadioButton();
  private JRadioButton bottomRadioButton = new JRadioButton();
  private ButtonGroup buttonGroup2 = new ButtonGroup();
  private JPanel addPointsPanel = new JPanel();
  private XYLayout xYLayout2 = new XYLayout();
  private JButton addPointsDoneButton = new JButton();
  private JButton addPointsCancelButton = new JButton();
  private Border border7;
  private JScrollPane jScrollPane3 = new JScrollPane();
  private JEditorPane jEditorPane2 = new JEditorPane();
  private BorderLayout borderLayout5 = new BorderLayout();
  private JLabel jLabel1 = new JLabel();
  private JButton configureOffsetsCancelButton = new JButton();
  private JButton configureOffsetsDoneButton = new JButton();
  private JPanel configureOffsetsPanel = new JPanel();
  private JLabel jLabel2 = new JLabel();
  private XYLayout xYLayout3 = new XYLayout();

  //Construct the frame
  public SurfaceMapSetupFrame()
  {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  //Component initialization
  private void jbInit() throws Exception
  {
    image1 = new ImageIcon(SurfaceMapSetupFrame.class.getResource("openFile.gif"));
    image2 = new ImageIcon(SurfaceMapSetupFrame.class.getResource("closeFile.gif"));
    image3 = new ImageIcon(SurfaceMapSetupFrame.class.getResource("help.gif"));
    zoomInimage = new ImageIcon(SurfaceMapSetupFrame.class.getResource("palette_zoomin.gif"));
    zoomOutimage = new ImageIcon(SurfaceMapSetupFrame.class.getResource("palette_zoomout.gif"));
    panimage = new ImageIcon(SurfaceMapSetupFrame.class.getResource("palette_hand.gif"));
    //setIconImage(Toolkit.getDefaultToolkit().createImage(SurfaceMapSetupFrame.class.getResource("[Your Icon]")));
    contentPane = (JPanel) this.getContentPane();
    border1 = BorderFactory.createLineBorder(Color.black,3);
    border2 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134)),BorderFactory.createEmptyBorder(23,15,23,29));
    border3 = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,new Color(182, 182, 182),new Color(62, 62, 62),new Color(89, 89, 89)),BorderFactory.createEmptyBorder(0,0,15,15));
    border4 = BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.black,2),BorderFactory.createEmptyBorder(5,5,5,5));
    border5 = BorderFactory.createEmptyBorder(15,15,15,15);
    border6 = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(93, 93, 93),new Color(134, 134, 134)),BorderFactory.createEmptyBorder(0,15,0,15));
    border7 = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(93, 93, 93),new Color(134, 134, 134)),BorderFactory.createEmptyBorder(0,0,15,15));
    contentPane.setLayout(borderLayout2);
    this.setSize(new Dimension(1211, 1000));
    this.setTitle("Surface Map Setup");
    jMenuFile.setMnemonic('F');
    jMenuFile.setText("File");
    jMenuFileExit.setMnemonic('x');
    jMenuFileExit.setText("Exit");
    jMenuFileExit.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jMenuFileExit_actionPerformed(e);
      }
    });
    jMenuHelp.setMnemonic('H');
    jMenuHelp.setText("Help");
    jMenuHelpAbout.setText("About");
    jMenuHelpAbout.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jMenuHelpAbout_actionPerformed(e);
      }
    });
    jMenu1.setMnemonic('E');
    jMenu1.setText("Edit");
    jMenu2.setMnemonic('V');
    jMenu2.setText("View");
    jMenu3.setMnemonic('T');
    jMenu3.setText("Tools");
    jMenuItem1.setMnemonic('S');
    jMenuItem1.setText("Save");
    jMenuItem2.setText("Zoom In");
    jMenuItem3.setText("Zoom Out");
    jMenuItem4.setText("Top Side Only");
    jMenuItem5.setText("Bottom Side Only");
    jMenuItem6.setText("Both Sides");
    jMenuItem7.setText("Add Xray Map Point");
    jMenuItem8.setText("Delete Point(s)");
    jMenuItem9.setText("Add Points");
    jMenuItem10.setText("Adjust Points");
    jMenuItem11.setText("Calculate Offset");
    jMenuItem12.setText("Configure Offsets");
    jMenuItem13.setText("Verify Focus");
    jButton11.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButton11_actionPerformed(e);
      }
    });
    tablePopupMenu.add(setReferencePointPopupMenuItem);
    tablePopupMenu.add(setPadHeightOffsetPopupMenuItem);
    setReferencePointPopupMenuItem.setText("Set as Reference Point");
    setReferencePointPopupMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setReferencePointPopupMenuItem_actionPerformed(e);
      }
    });
    setPadHeightOffsetPopupMenuItem.setText("Set pad height offset");
    setPadHeightOffsetPopupMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setPadHeightOffsetPopupMenuItem_actionPerformed(e);
      }
    });

    componentZoomComboBox.setPreferredSize(new Dimension(70, 25));
    jButton9.setText("Go!");
    jButton8.setText("Both Sides");
    jButton7.setText("Bottom Side");
    jButton6.setText("Top Side");
    panButton.setIcon(panimage);
    zoomOutButton.setIcon(zoomOutimage);
    zoomInButton.setIcon(zoomInimage);
    jButton2.setIcon(image2);
    jButton2.setToolTipText("Close File");
    jPanel1.setLayout(xYLayout1);

    statusBar.setBorder(BorderFactory.createEtchedBorder());
    statusBar.setText(" ");
    verifyFocusPanel.setLayout(xYLayout10);
    verifyFocusPanel.setBackground(Color.gray);
    verifyFocusPanel.setBorder(border3);

    adjustPointsPrevPointButton.setText("Prev Point");
    adjustPointsPrevPointButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustPointsPrevPointButton_actionPerformed(e);
      }
    });
    surfaceMapAdjustPointsPanel.setLayout(xYLayout11);
    surfaceMapAdjustPointsPanel.setBorder(border3);
    surfaceMapAdjustPointsPanel.setBackground(Color.gray);

    adjustPointsGotoPointsButton.setText("Go to Point");
    adjustPointsGotoPointsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustPointsGotoButton_actionPerformed(e);
      }
    });
    adjustPointsGoToTextField.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustPointsGotoButton_actionPerformed(e);
      }
    });

    adjustPointsNextPointButton.setText("Next Point");
    adjustPointsNextPointButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustPointsNextPointButton_actionPerformed(e);
      }
    });

    adjustPointsCancelButton.setText("Cancel");
    adjustPointsCancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustPointsCancelButton_actionPerformed(e);
      }
    });

    jLabel19.setFont(new java.awt.Font("Dialog", 1, 16));
    jLabel19.setForeground(Color.white);
    jLabel19.setText("Adjust Points:");
    adjustPointsDoneButton.setText("Done");
    adjustPointsDoneButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustPointsDoneButton_actionPerformed(e);
      }
    });

    jPanel3.setLayout(borderLayout1);
    jPanel3.setBorder(BorderFactory.createLineBorder(Color.black));
    jPanel2.setLayout(borderLayout6);
    jPanel2.setBorder(BorderFactory.createLineBorder(Color.black));
    cadGraphicsEditorPane.setBorder(BorderFactory.createLineBorder(Color.black));
    cadGraphicsEditorPane.setContentType("text/html");
    cadGraphicsEditorPane.setPage(cadGraphicsURL);
    cadGraphicsEditorPane.addMouseListener(new java.awt.event.MouseAdapter()
    {
      public void mouseClicked(MouseEvent e)
      {
        cadGraphicsEditorPane_mouseClicked(e);
      }
    });
    jPanel11.setLayout(borderLayout4);
    jPanel13.setLayout(borderLayout3);
    borderLayout3.setHgap(5);
    jPanel4.setLayout(borderLayout5);
    jPanel4.setBorder(border5);
    jScrollPane1.setBorder(BorderFactory.createLoweredBevelBorder());
    verifyFocusPrevPointButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        verifyFocusPrevPointButton_actionPerformed(e);
      }
    });
    verifyFocusPrevPointButton.setText("Prev Point");
    verifyFocusPrevPointButton.setMnemonic('P');
    verifyFocusGotoPointButton.setMnemonic('G');
    verifyFocusGotoPointButton.setText("Go to Point");
    verifyFocusGotoPointButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        verifyFocusGotoPointButton_actionPerformed(e);
      }
    });
    verifyFocusCancelButton.setMnemonic('C');
    verifyFocusCancelButton.setText("Cancel");
    verifyFocusCancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        verifyFocusCancelButton_actionPerformed(e);
      }
    });
    verifyFocusNextPointButton.setMnemonic('N');
    verifyFocusNextPointButton.setText("Next Point");
    verifyFocusNextPointButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        verifyFocusNextPointButton_actionPerformed(e);
      }
    });
    verifyFocusDoneButton.setMnemonic('D');
    verifyFocusDoneButton.setText("Done");
    verifyFocusDoneButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        verifyFocusDoneButton_actionPerformed(e);
      }
    });
    jLabel14.setFont(new java.awt.Font("Dialog", 1, 16));
    jLabel14.setForeground(Color.white);
    jLabel14.setHorizontalAlignment(SwingConstants.CENTER);
    jLabel14.setText("Verify Focus");
    jPanel9.setBackground(new Color(181, 228, 255));
    jPanel9.setBorder(BorderFactory.createLineBorder(Color.black));
    jLabel4.setText("Reference Point");
    jPanel6.setLayout(flowLayout1);
    flowLayout1.setAlignment(FlowLayout.LEFT);
    jMenu4.setMnemonic('I');
    jMenu4.setText("Image Control");
    toggleLiveSnapMenuItem.setMnemonic('S');
    toggleLiveSnapMenuItem.setText("Toggle Live/Snap");
    toggleLiveSnapMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(83, java.awt.event.KeyEvent.ALT_MASK, false));
    toggleLiveSnapMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        toggleLiveSnapMenuItem_actionPerformed(e);
      }
    });
    nextFOVMenuItem.setMnemonic('N');
    nextFOVMenuItem.setText("Next FOV");
    nextFOVMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(79, java.awt.event.KeyEvent.ALT_MASK, false));
    nextFOVMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        nextFOVMenuItem_actionPerformed(e);
      }
    });
    toggleLightsMenuItem.setMnemonic('L');
    toggleLightsMenuItem.setText("Toggle Lights");
    toggleLightsMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(76, java.awt.event.KeyEvent.ALT_MASK, false));
    toggleLightsMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        toggleLightsMenuItem_actionPerformed(e);
      }
    });
    jButton1.setText("1 : 1");
    jButton3.setText("Zoom Box");
    jButton12.setText("From Top");
    jButton13.setText("From Bottom");
    jPanel7.setLayout(borderLayout7);
    deletePointsButton.setText("Delete Points");
    deletePointsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deletePointsButton_actionPerformed(e);
      }
    });
    gridLayout1.setColumns(2);
    gridLayout1.setHgap(20);
    gridLayout1.setRows(3);
    gridLayout1.setVgap(10);
    jButton11.setText("Calculate Single Offset...");
    addPointsButton.setText("Add Points. . .");
    addPointsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addPointsButton_actionPerformed(e);
      }
    });
    configureOffsetsButton.setText("Configure Offsets. . .");
    configureOffsetsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        configureOffsetsButton_actionPerformed(e);
      }
    });
    adjustPointsButton.setText("Adjust Points. . .");
    adjustPointsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustPointsButton_actionPerformed(e);
      }
    });
    verifyFocusButton.setText("Verify Focus. . .");
    verifyFocusButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        verifyFocusButton_actionPerformed(e);
      }
    });
    jPanel5.setLayout(gridLayout1);
    jPanel5.setBorder(border2);
    topRadioButton.setBackground(Color.gray);
    topRadioButton.setForeground(Color.white);
    topRadioButton.setSelected(true);
    topRadioButton.setText("Top");
    bottomRadioButton.setBackground(Color.gray);
    bottomRadioButton.setForeground(Color.white);
    bottomRadioButton.setText("Bottom");
    addPointsPanel.setLayout(xYLayout2);
    addPointsDoneButton.setText("Done");
    addPointsDoneButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addPointsDoneButton_actionPerformed(e);
      }
    });
    addPointsCancelButton.setText("Cancel");
    addPointsCancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addPointsCancelButton_actionPerformed(e);
      }
    });
    addPointsPanel.setBackground(Color.gray);
    addPointsPanel.setBorder(border7);
    jEditorPane2.setContentType("text/html");
    jEditorPane2.setBorder(border4);
    jEditorPane2.setEditable(false);
    jEditorPane2.setPage(mainInstantHelp);
    jLabel1.setFont(new java.awt.Font("Dialog", 1, 16));
    jLabel1.setForeground(Color.white);
    jLabel1.setText("Add Points:");
    configureOffsetsCancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        configureOffsetsCancelButton_actionPerformed(e);
      }
    });
    configureOffsetsCancelButton.setText("Cancel");
    configureOffsetsDoneButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        configureOffsetsDoneButton_actionPerformed(e);
      }
    });
    configureOffsetsDoneButton.setText("Done");
    configureOffsetsPanel.setBorder(border7);
    configureOffsetsPanel.setBackground(Color.gray);
    configureOffsetsPanel.setLayout(xYLayout3);
    jLabel2.setFont(new java.awt.Font("Dialog", 1, 16));
    jLabel2.setForeground(Color.white);
    jLabel2.setText("Configure Offsets");
    jPanel1.add(jButton2,    new XYConstraints(5, 4, -1, 27));
    jPanel1.add(zoomInButton,   new XYConstraints(61, 4, -1, 27));
    jPanel1.add(zoomOutButton,       new XYConstraints(113, 4, -1, 27));
    jPanel1.add(jButton3,   new XYConstraints(165, 4, -1, -1));
    jPanel1.add(jButton1,     new XYConstraints(261, 4, -1, -1));
    jPanel1.add(panButton,         new XYConstraints(325, 4, -1, 27));
    jPanel1.add(jButton6,     new XYConstraints(380, 4, -1, -1));
    jPanel1.add(jButton7,          new XYConstraints(468, 4, -1, -1));
    jPanel1.add(jButton8,       new XYConstraints(578, 4, -1, -1));
    jPanel1.add(jButton12,       new XYConstraints(681, 4, -1, -1));
    jPanel1.add(jButton13,         new XYConstraints(775, 4, -1, -1));
    jPanel1.add(componentZoomComboBox,       new XYConstraints(890, 4, 81, -1));
    jPanel1.add(jButton9,      new XYConstraints(976, 4, -1, -1));
    jMenuFile.add(jMenuItem1);
    jMenuFile.add(jMenuFileExit);
    jMenuHelp.add(jMenuHelpAbout);
    jMenuBar1.add(jMenuFile);
    jMenuBar1.add(jMenu1);
    jMenuBar1.add(jMenu2);
    jMenuBar1.add(jMenu4);
    jMenuBar1.add(jMenu3);
    jMenuBar1.add(jMenuHelp);
    this.setJMenuBar(jMenuBar1);
    contentPane.add(jToolBar,  BorderLayout.NORTH);
    jToolBar.add(jPanel1, null);
    contentPane.add(statusBar, BorderLayout.SOUTH);
    contentPane.add(jPanel11,  BorderLayout.CENTER);
    jPanel11.add(jSplitPane1, BorderLayout.CENTER);
    jPanel3.add(jScrollPane2, BorderLayout.CENTER);
    jPanel11.add(jPanel13,  BorderLayout.SOUTH);
//    jPanel13.add(surfaceMapAdjustPointsPanel, null);
    jPanel13.add(jPanel4,  BorderLayout.CENTER);
    jPanel4.add(jScrollPane3, BorderLayout.NORTH);
    jScrollPane3.getViewport().add(jEditorPane2, null);
    jPanel13.add(jPanel7,  BorderLayout.WEST);
    jPanel7.add(jPanel5,  BorderLayout.WEST);
    jPanel5.add(adjustPointsButton, null);
    jPanel5.add(configureOffsetsButton, null);
    jPanel5.add(addPointsButton, null);
    jPanel5.add(jButton11, null);
    jPanel5.add(deletePointsButton, null);
    jPanel5.add(verifyFocusButton, null);
    jSplitPane1.add(jPanel2, JSplitPane.LEFT);
    jScrollPane2.getViewport().add(cadGraphicsEditorPane, null);
    jSplitPane1.add(jPanel3, JSplitPane.RIGHT);
    jPanel2.add(jScrollPane1, BorderLayout.CENTER);
    jPanel2.add(jPanel6,  BorderLayout.SOUTH);
    jPanel6.add(jPanel9, null);
    jPanel6.add(jLabel4, null);

    surfaceMapAdjustPointsPanel.add(adjustPointsPrevPointButton,    new XYConstraints(5, 68, 101, -1));
    surfaceMapAdjustPointsPanel.add(adjustPointsGoToTextField,     new XYConstraints(112, 106, 72, -1));
    surfaceMapAdjustPointsPanel.add(adjustPointsGotoPointsButton,     new XYConstraints(5, 105, 101, -1));
    surfaceMapAdjustPointsPanel.add(adjustPointsNextPointButton,    new XYConstraints(5, 31, 101, -1));
    surfaceMapAdjustPointsPanel.add(jLabel19, new XYConstraints(5, 2, -1, -1));
    surfaceMapAdjustPointsPanel.add(adjustPointsCancelButton,    new XYConstraints(391, 105, 101, -1));
    surfaceMapAdjustPointsPanel.add(adjustPointsDoneButton,         new XYConstraints(391, 68, 101, -1));
    Random random = new Random();
    Vector rowData = new Vector();
    for (int i = 0; i < 60; i++)
    {
      Vector singleRow = new Vector();
      singleRow.add(new Integer(i+1));
      if (i == 29)
        singleRow.add("X-ray");
      else
        singleRow.add("Laser");
//      singleRow.add(new Integer(random.nextInt(10000)));
//      singleRow.add(new Integer(random.nextInt(10000)));
      if (i == 29)
        singleRow.add(new Float(0.0));
      else
        singleRow.add(new Float(random.nextFloat() + 3.0));
      rowData.add(singleRow);
    }
    Vector columnNames = new Vector();
    columnNames.add("#");
    columnNames.add("Type");
//    columnNames.add("X");
//    columnNames.add("Y");
    columnNames.add("Offset");
//    jTable1 = new JSortTable(rowData, columnNames);
    tableModel = new SurfaceMapTableModel();
    jTable1 = new JSortTable(tableModel);
    jScrollPane1.getViewport().add(jTable1, null);
    jTable1.addMouseListener(new java.awt.event.MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        jTable1_mouseReleased(e);
      }
      public void mousePressed(MouseEvent e)
      {
        jTable1_mousePressed(e);
      }
    });
    jMenu2.add(jMenuItem2);
    jMenu2.add(jMenuItem3);
    jMenu2.add(jMenuItem4);
    jMenu2.add(jMenuItem5);
    jMenu2.add(jMenuItem6);
    jMenu3.add(jMenuItem10);
    jMenu3.add(jMenuItem12);
    jMenu3.add(jMenuItem11);
    jMenu3.add(jMenuItem13);
    jMenu3.add(jMenuItem7);
    jMenu1.add(jMenuItem9);
    jMenu1.add(jMenuItem8);
    componentZoomComboBox.addItem("C1");
    componentZoomComboBox.addItem("C2");
    componentZoomComboBox.addItem("C3");
    componentZoomComboBox.addItem("C4");
    componentZoomComboBox.addItem("R1");
    componentZoomComboBox.addItem("R2");
    componentZoomComboBox.addItem("R3");
    componentZoomComboBox.addItem("U1");
    componentZoomComboBox.addItem("U2");
    componentZoomComboBox.addItem("U3");
    componentZoomComboBox.addItem("U4");
    componentZoomComboBox.addItem("U5");
    jSplitPane1.setDividerLocation(373);
    jTable1.setRowSelectionInterval(0, 0);
//    jTable1.getTableHeader().setReorderingAllowed(false);
    jTable1.setDefaultRenderer(Object.class, new CustomerTableRenderer());
    verifyFocusGotoTextField.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        verifyFocusGotoTextField_actionPerformed(e);
      }
    });
    verifyFocusPanel.add(jLabel14,    new XYConstraints(5, 2, -1, -1));
    verifyFocusPanel.add(verifyFocusNextPointButton,     new XYConstraints(5, 31, 101, -1));
    verifyFocusPanel.add(verifyFocusPrevPointButton,       new XYConstraints(5, 68, 101, -1));
    verifyFocusPanel.add(verifyFocusGotoPointButton,       new XYConstraints(5, 105, 101, -1));
    verifyFocusPanel.add(verifyFocusGotoTextField,    new XYConstraints(112, 106, 72, -1));
    verifyFocusPanel.add(verifyFocusDoneButton,     new XYConstraints(391, 68, 101, -1));
    verifyFocusPanel.add(verifyFocusCancelButton,    new XYConstraints(391, 105, 101, -1));
    verifyFocusPanel.add(topRadioButton,    new XYConstraints(140, 28, -1, -1));
    verifyFocusPanel.add(bottomRadioButton,    new XYConstraints(196, 28, -1, -1));
    jMenu4.add(toggleLiveSnapMenuItem);
    jMenu4.add(nextFOVMenuItem);
    jMenu4.add(toggleLightsMenuItem);
    buttonGroup2.add(topRadioButton);
    buttonGroup2.add(bottomRadioButton);
    addPointsPanel.add(addPointsDoneButton,   new XYConstraints(391, 68, 101, -1));
    addPointsPanel.add(addPointsCancelButton,    new XYConstraints(391, 105, 101, -1));
    addPointsPanel.add(jLabel1,    new XYConstraints(5, 2, -1, -1));
    combinedPanel.setLayout(combinedPanelBorderLayout);
//    configureOffsetsPanel.add(configureOffsetsDoneButton, new XYConstraints(391, 68, 101, -1));
    configureOffsetsPanel.add(configureOffsetsCancelButton, new XYConstraints(391, 105, 101, -1));
    configureOffsetsPanel.add(jLabel2, new XYConstraints(5, 2, -1, -1));

  }
  //File | Exit action performed
  public void jMenuFileExit_actionPerformed(ActionEvent e)
  {
    System.exit(0);
  }
  //Help | About action performed
  public void jMenuHelpAbout_actionPerformed(ActionEvent e)
  {
  }
  //Overridden so we can exit when window is closed
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      jMenuFileExit_actionPerformed(null);
    }
  }

  void adjustPointsButton_actionPerformed(ActionEvent e)
  {
    try
    {
      jEditorPane2.setPage(adjustPointsInstantHelp);
    }
    catch (IOException ex)
    {
    }

    jPanel7.remove(jPanel5);
    jPanel7.add(surfaceMapAdjustPointsPanel, BorderLayout.WEST);
//    jPanel13.add(surfaceMapAdjustPointsPanel, new XYConstraints(9, 724, 487, 144));
    validate();
//    pack();

    int currentRow = jTable1.getSelectedRow();
    int currentPoint = currentRow + 1;
    jLabel19.setText("Adjust Points, point #" + currentPoint);
    repaint();
    combinedPanel.validate();
    combinedPanel.repaint();
  }

  void addPointsButton_actionPerformed(ActionEvent e)
  {
    try
    {
      jEditorPane2.setPage(addPointsInstantHelp);
    }
    catch (IOException ex)
    {
    }
    jPanel7.remove(jPanel5);
    jPanel7.add(addPointsPanel, BorderLayout.WEST);
//    jPanel13.add(surfaceMapAdjustPointsPanel, new XYConstraints(9, 724, 487, 144));
    validate();
//    jPanel7.validate();
//    jPanel13.validate();
    repaint();

    combinedPanel.validate();
    combinedPanel.repaint();
    listeningToCadClicks = true;
    currentMaxPoints = tableModel.getRowCount();
  }

  void addPointsDoneButton_actionPerformed(ActionEvent e)
  {
    listeningToCadClicks = false;
    jTable1.setRowSelectionInterval(currentMaxPoints, currentMaxPoints);
    removeAddPointsPanel();
    try
    {
      jEditorPane2.setPage(adjustPointsInstantHelp);
    }
    catch (IOException ex)
    {
    }

    jPanel7.remove(jPanel5);
    jPanel7.add(surfaceMapAdjustPointsPanel, BorderLayout.WEST);
//    jPanel13.add(surfaceMapAdjustPointsPanel, new XYConstraints(9, 724, 487, 144));
    validate();
//    pack();

    int currentRow = jTable1.getSelectedRow();
    int currentPoint = currentRow + 1;
    jLabel19.setText("Adjust Points, point #" + currentPoint);
    repaint();
    combinedPanel.validate();
    combinedPanel.repaint();
  }

  void addPointsCancelButton_actionPerformed(ActionEvent e)
  {
    listeningToCadClicks = false;
    removeAddPointsPanel();
  }

  void adjustPointsNextPointButton_actionPerformed(ActionEvent e)
  {
    int currentRow = jTable1.getSelectedRow();
    int currentColumn = jTable1.getSelectedColumn();
    currentRow++;
    if (currentRow == jTable1.getRowCount())
      currentRow = 0;
    jTable1.setRowSelectionInterval(currentRow, currentRow);

    Rectangle rect = jTable1.getCellRect(currentRow, currentColumn, false);
    jTable1.scrollRectToVisible(rect);

    currentRow++;
    jLabel19.setText("Adjust Points, point #" + currentRow);
  }

  void adjustPointsPrevPointButton_actionPerformed(ActionEvent e)
  {
    int currentRow = jTable1.getSelectedRow();
    int currentColumn = jTable1.getSelectedColumn();
    if (currentRow == 0)
      currentRow = jTable1.getRowCount() - 1;
    else
      currentRow--;

    jTable1.setRowSelectionInterval(currentRow, currentRow);
    Rectangle rect = jTable1.getCellRect(currentRow, currentColumn, false);
    jTable1.scrollRectToVisible(rect);

    currentRow++;
    jLabel19.setText("Adjust Points, point #" + currentRow);
  }

  void adjustPointsGotoButton_actionPerformed(ActionEvent e)
  {
    String goToStr = adjustPointsGoToTextField.getText();
    int goToInt;
    try
    {
      goToInt = Integer.parseInt(goToStr);
    }
    catch (NumberFormatException nfex)
    {
      adjustPointsGoToTextField.setText(null);
      return;
    }

    goToInt--;
    int currentColumn = jTable1.getSelectedColumn();
    jTable1.setRowSelectionInterval(goToInt, goToInt);

    Rectangle rect = jTable1.getCellRect(goToInt, currentColumn, false);
    jTable1.scrollRectToVisible(rect);

    goToInt++;

    jLabel19.setText("Adjust Points, point #" + goToInt);
  }

  private void adjustPointsGoToTextField_actionPerformed(ActionEvent e)
  {
    String goToStr = adjustPointsGoToTextField.getText();
    int goToInt;
    try
    {
      goToInt = Integer.parseInt(goToStr);
    }
    catch (NumberFormatException nfex)
    {
      adjustPointsGoToTextField.setText(null);
      return;
    }
    goToInt--;
    int currentColumn = jTable1.getSelectedColumn();
    jTable1.setRowSelectionInterval(goToInt, goToInt);

    Rectangle rect = jTable1.getCellRect(goToInt-1, currentColumn, false);
    jTable1.scrollRectToVisible(rect);

    goToInt++;

    jLabel19.setText("Adjust Points, point #" + goToInt);
  }

  void verifyFocusGotoTextField_actionPerformed(ActionEvent e)
  {
    String goToStr = verifyFocusGotoTextField.getText();
    int goToInt;
    try
    {
      goToInt = Integer.parseInt(goToStr);
    }
    catch (NumberFormatException nfex)
    {
      verifyFocusGotoTextField.setText(null);
      return;
    }
    goToInt--;
    int currentColumn = jTable1.getSelectedColumn();
    jTable1.setRowSelectionInterval(goToInt, goToInt);

    Rectangle rect = jTable1.getCellRect(goToInt, currentColumn, false);
    jTable1.scrollRectToVisible(rect);
    int currentPoint = goToInt + 1;
    jLabel14.setText("Verify Focus, point #" + currentPoint);
  }

  void verifyFocusPrevPointButton_actionPerformed(ActionEvent e)
  {
    int currentRow = jTable1.getSelectedRow();
    int currentColumn = jTable1.getSelectedColumn();
    if (currentRow == 0)
      currentRow = jTable1.getRowCount() - 1;
    else
      currentRow--;

    jTable1.setRowSelectionInterval(currentRow, currentRow);
    Rectangle rect = jTable1.getCellRect(currentRow, currentColumn, false);
    jTable1.scrollRectToVisible(rect);

    int currentPoint = currentRow + 1;
    jLabel14.setText("Verify Focus, point #" + currentPoint);
  }

  void verifyFocusGotoPointButton_actionPerformed(ActionEvent e)
  {
    String goToStr = verifyFocusGotoTextField.getText();
    int goToInt;
    try
    {
      goToInt = Integer.parseInt(goToStr);
    }
    catch (NumberFormatException nfex)
    {
      verifyFocusGotoTextField.setText(null);
      return;
    }
    goToInt--;
    int currentColumn = jTable1.getSelectedColumn();
    jTable1.setRowSelectionInterval(goToInt, goToInt);

    Rectangle rect = jTable1.getCellRect(goToInt, currentColumn, false);
    jTable1.scrollRectToVisible(rect);

    int currentPoint = goToInt + 1;
    jLabel14.setText("Verify Focus, point #" + currentPoint);
  }
  void verifyFocusNextPointButton_actionPerformed(ActionEvent e)
  {
    int currentRow = jTable1.getSelectedRow();
    int currentColumn = jTable1.getSelectedColumn();
    currentRow++;
    if (currentRow == jTable1.getRowCount())
      currentRow = 0;
    jTable1.setRowSelectionInterval(currentRow, currentRow);

    Rectangle rect = jTable1.getCellRect(currentRow, currentColumn, false);
    jTable1.scrollRectToVisible(rect);

    int currentPoint = currentRow + 1;
    jLabel14.setText("Verify Focus, point #" + currentPoint);
  }

  void verifyFocusDoneButton_actionPerformed(ActionEvent e)
  {
    removeVerifyFocusPanel();
  }


  void removeAdjustPointsPanel()
  {
    try
    {
      jEditorPane2.setPage(mainInstantHelp);
    }
    catch (IOException ex)
    {
    }
    jPanel7.remove(surfaceMapAdjustPointsPanel);
    jPanel7.add(jPanel5, BorderLayout.WEST);//new XYConstraints(9, 724, 487, 144));
    validate();
    repaint();
    combinedPanel.validate();
    combinedPanel.repaint();
  }

  void removeVerifyFocusPanel()
  {
    try
    {
      jEditorPane2.setPage(mainInstantHelp);
    }
    catch (IOException ex)
    {
    }
    jPanel7.remove(verifyFocusPanel);
    jPanel7.add(jPanel5, BorderLayout.WEST);//new XYConstraints(9, 724, 487, 144));
    validate();
    repaint();
    combinedPanel.validate();
    combinedPanel.repaint();
  }

  void removeAddPointsPanel()
  {
    try
    {
      jEditorPane2.setPage(mainInstantHelp);
    }
    catch (IOException ex)
    {
    }
    jPanel7.remove(addPointsPanel);
    jPanel7.add(jPanel5, BorderLayout.WEST);//new XYConstraints(9, 724, 487, 144));
    validate();
    repaint();
    combinedPanel.validate();
    combinedPanel.repaint();
  }

  void removeConfigureOffsetsPanel()
  {
    try
    {
      jEditorPane2.setPage(mainInstantHelp);
    }
    catch (IOException ex)
    {
    }
    jPanel7.remove(configureOffsetsPanel);
    jPanel7.add(jPanel5, BorderLayout.WEST);//new XYConstraints(9, 724, 487, 144));
    validate();
    repaint();
    combinedPanel.validate();
    combinedPanel.repaint();
  }

  void adjustPointsDoneButton_actionPerformed(ActionEvent e)
  {
    removeAdjustPointsPanel();
  }

  void adjustPointsCancelButton_actionPerformed(ActionEvent e)
  {
    removeAdjustPointsPanel();
  }

  void verifyFocusButton_actionPerformed(ActionEvent e)
  {
    try
    {
      jEditorPane2.setPage(verifyFocusInstantHelp);
    }
    catch (IOException ex)
    {
    }
    jPanel7.remove(jPanel5);
    jPanel7.add(verifyFocusPanel, BorderLayout.WEST);
    validate();

    int currentRow = jTable1.getSelectedRow();
    int currentPoint = currentRow + 1;
    jLabel14.setText("Verify Focus, point #" + currentPoint);
    repaint();
    combinedPanel.validate();
    combinedPanel.repaint();
  }
  void verifyFocusCancelButton_actionPerformed(ActionEvent e)
  {
    removeVerifyFocusPanel();
  }

  void toggleLiveSnapMenuItem_actionPerformed(ActionEvent e)
  {
    JOptionPane.showMessageDialog(this, "Toggle Live Snap mode", "Image Control", JOptionPane.INFORMATION_MESSAGE);
  }

  void nextFOVMenuItem_actionPerformed(ActionEvent e)
  {
    JOptionPane.showMessageDialog(this, "Next FOV", "Image Control", JOptionPane.INFORMATION_MESSAGE);
  }

  void toggleLightsMenuItem_actionPerformed(ActionEvent e)
  {
    JOptionPane.showMessageDialog(this, "Toggle Laser Lights", "Image Control", JOptionPane.INFORMATION_MESSAGE);
  }

  public JPanel getSurfaceMapSetupPanel()
  {
    combinedPanel.add(jPanel2, BorderLayout.CENTER);
    combinedPanel.add(jPanel7, BorderLayout.SOUTH);

    return combinedPanel;
  }

  void deletePointsButton_actionPerformed(ActionEvent e)
  {
    int[] rows = jTable1.getSelectedRows();
    int[] pointsToDelete = rows;

    if (rows.length > 1)
    {
      int choice = JOptionPane.showConfirmDialog(this, "Do want to delete these points?", "Delete Points", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
      // I need to figure out all the points to delete first before deleting any of them, or the model won't match the table at the time
      // the user selected delete.
      if (choice == JOptionPane.YES_OPTION)
      {
        for (int i = 0; i < rows.length; i++)
        {
          Integer pointNum = (Integer)jTable1.getModel().getValueAt(rows[i],0);
          pointsToDelete[i] = pointNum.intValue();
        }
        // Now, all the point numbers to be deleted are in this array and we are safe to delete them one at a time.
        for (int i = 0; i < pointsToDelete.length; i++)
        {
          tableModel.deletePoint(pointsToDelete[i]);
        }
      }
    }

  }

  void cadGraphicsEditorPane_mouseClicked(MouseEvent e)
  {
    if (listeningToCadClicks)
    {
      // a new point has been added.  Update the table
      tableModel.addNewPoint();
      int currentRow = tableModel.getRowCount() -1 ;
      int currentColumn = 0;
      jTable1.setRowSelectionInterval(currentRow, currentRow);

      Rectangle rect = jTable1.getCellRect(currentRow, currentColumn, false);
      jTable1.scrollRectToVisible(rect);
    }

    if (listeningForReferenceClick)
    {
      referencePointNumber = currentMaxPoints + 1;
      tableModel.addNewPoint();
      listeningForReferenceClick = false;
      jTable1.setRowSelectionInterval(currentMaxPoints, currentMaxPoints);
      Rectangle rect = jTable1.getCellRect(currentMaxPoints, 0, false);
      jTable1.scrollRectToVisible(rect);

      removeConfigureOffsetsPanel();
      try
      {
        jEditorPane2.setPage(adjustPointsInstantHelp);
      }
      catch (IOException ex)
      {
      }

      jPanel7.remove(jPanel5);
      jPanel7.add(surfaceMapAdjustPointsPanel, BorderLayout.WEST);
//    jPanel13.add(surfaceMapAdjustPointsPanel, new XYConstraints(9, 724, 487, 144));
      validate();
//    pack();

      int currentRow = jTable1.getSelectedRow();
      int currentPoint = currentRow + 1;

      jLabel19.setText("Adjust Points, point #" + currentPoint);
      repaint();
      combinedPanel.validate();
      combinedPanel.repaint();
    }
  }

  void configureOffsetsCancelButton_actionPerformed(ActionEvent e)
  {
    listeningForReferenceClick = false;
    removeConfigureOffsetsPanel();
  }

  void configureOffsetsDoneButton_actionPerformed(ActionEvent e)
  {
    listeningForReferenceClick = false;
    jTable1.setRowSelectionInterval(currentMaxPoints, currentMaxPoints);
    removeConfigureOffsetsPanel();
    try
    {
      jEditorPane2.setPage(adjustPointsInstantHelp);
    }
    catch (IOException ex)
    {
    }

    jPanel7.remove(jPanel5);
    jPanel7.add(surfaceMapAdjustPointsPanel, BorderLayout.WEST);
//    jPanel13.add(surfaceMapAdjustPointsPanel, new XYConstraints(9, 724, 487, 144));
    validate();
//    pack();

    int currentRow = jTable1.getSelectedRow();
    int currentPoint = currentRow + 1;
    jLabel19.setText("Adjust Points, point #" + currentPoint);
    repaint();
    combinedPanel.validate();
    combinedPanel.repaint();
  }

  void configureOffsetsButton_actionPerformed(ActionEvent e)
  {
    try
    {
      jEditorPane2.setPage(configureOffsetsInstantHelp);
    }
    catch (IOException ex)
    {
    }
    jPanel7.remove(jPanel5);
    jPanel7.add(configureOffsetsPanel, BorderLayout.WEST);
    validate();
    repaint();

    combinedPanel.validate();
    combinedPanel.repaint();

    if (referencePointNumber == 0) // currently not set
    {
      listeningForReferenceClick = true;
      currentMaxPoints = tableModel.getRowCount();
    }
    else  // there currently is a reference point
    {
      Object[] options = {"Adjust", "Pick new point", "Cancel"};
      int choice = JOptionPane.showOptionDialog(this, "There already is a Reference Point configured.  Do you want to: ", "Configure Reference Point",
                                                JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
      if (choice == 0)  // Adjust
      {
        try
        {
          jEditorPane2.setPage(adjustPointsInstantHelp);
        }
        catch (IOException ex)
        {
        }
        jTable1.setRowSelectionInterval(referencePointNumber - 1, referencePointNumber - 1);
        jPanel7.remove(configureOffsetsPanel);
        jPanel7.add(surfaceMapAdjustPointsPanel, BorderLayout.WEST);
        validate();

        int currentRow = jTable1.getSelectedRow();
        int currentPoint = currentRow + 1;
        jLabel19.setText("Adjust Points, point #" + currentPoint);
        repaint();
        combinedPanel.validate();
        combinedPanel.repaint();
      }
      else if (choice == 1) // Start Over
      {
        referencePointNumber = 0;
        currentMaxPoints = tableModel.getRowCount();
        listeningForReferenceClick = true;
        return;
      }
      else // Cancel
      {
        removeConfigureOffsetsPanel();
      }

    }

  }

  // This is for popping up a right click menu (popup menu, context menu)
  // A popup appears when the mouse is release, not pressed.
  private void jTable1_mouseReleased(MouseEvent e)
  {
    if (e.isPopupTrigger())
    {
      tablePopupMenu.show(e.getComponent(), e.getX(), e.getY());

//      JOptionPane.showMessageDialog(this, "Popup menu on row " + row, "Image Control", JOptionPane.INFORMATION_MESSAGE);
//      _testTreePopupMenu.show(e.getComponent(), e.getX(), e.getY());
    }
  }

  // This is for popping up a right click menu (popup menu, context menu)
  // A popup appears when the mouse is release, not pressed.
  private void jTable1_mousePressed(MouseEvent e)
  {
    if (e.getButton() != e.BUTTON1)
    {
      int row = jTable1.rowAtPoint(e.getPoint());
      if (row == -1)  // not in a row
        return;
      jTable1.setRowSelectionInterval(row, row);
    }
  }

  void setPadHeightOffsetPopupMenuItem_actionPerformed(ActionEvent e)
  {
    int choice = JOptionPane.showConfirmDialog(this, "Set Pad Height offset for all points or selected point with single value or average value of selected points.", "Set Pad Height Offset", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
  }

  void setReferencePointPopupMenuItem_actionPerformed(ActionEvent e)
  {
    int row = jTable1.getSelectedRow();
    referencePointNumber = row + 1;
    tableModel.fireTableDataChanged();
    jTable1.setRowSelectionInterval(row, row);
  }

  void jButton11_actionPerformed(ActionEvent e)
  {
     int choice = JOptionPane.showConfirmDialog(this, "Please Focus the x-ray image", "Focus Image", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
     if (choice == JOptionPane.OK_OPTION)
     {
       choice = JOptionPane.showConfirmDialog(this, "Did you focus on the top?", "Focus Image", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
       if (choice == JOptionPane.YES_OPTION)
       {
         JOptionPane.showMessageDialog(this, "A new delta-Z was calculated!");
       }
     }

  }
}
