package proto.menuUI;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class CadLinkAction extends MenuAction
{
  private static final ImageIcon _cadlinkIcon = new ImageIcon(Menu5DX.class.getResource("cadlink.gif"));

  public CadLinkAction(Menu5DX parent)
  {
    super("CADLink", _cadlinkIcon, "Run CADLink II", 'L', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._cadlinkMI_actionPerformed(e);
  }
}