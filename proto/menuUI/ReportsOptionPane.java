package proto.menuUI;

import java.awt.*;
import javax.swing.border.Border;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import com.agilent.guiUtil.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class ReportsOptionPane extends JPanel
{
  public static final String NEVER = "Never";
  public static final String ALWAYS = "Always";
  public static final String DEFECTIVE = "When defects are detected";

  public ReportsOptionPane()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    _fileLbl.setText("Write to file:");
    _printLbl.setText("Send to printer:");
    _displayLbl.setText("Display in window:");

    _displayCB.addItem(NEVER);
    _displayCB.addItem(ALWAYS);
    _displayCB.addItem(DEFECTIVE);
    _printCB.addItem(NEVER);
    _printCB.addItem(ALWAYS);
    _printCB.addItem(DEFECTIVE);
    _fileCB.addItem(NEVER);
    _fileCB.addItem(ALWAYS);
    _fileCB.addItem(DEFECTIVE);

    _defectReportingPanel.setLayout(_pairLayout1);
    _defectReportingPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Automatic Defect Reporter "));
    _defectReportingPanel.add(_displayLbl, null);
    _defectReportingPanel.add(_displayCB, null);
    _defectReportingPanel.add(_printLbl, null);
    _defectReportingPanel.add(_printCB, null);
    _defectReportingPanel.add(_fileLbl, null);
    _defectReportingPanel.add(_fileCB, null);

    _centerPanel.setLayout(_verticalFlowLayout1);
    _centerPanel.add(_defectReportingPanel, null);

    this.setBorder(BorderFactory.createEmptyBorder(10,0,5,0));
    this.setLayout(_borderLayout1);
    this.add(_centerPanel, BorderLayout.CENTER);
  }

  public String getDefectReporterDisplay()
  {
    return (String)_displayCB.getSelectedItem();
  }
  public String getDefectReporterPrint()
  {
    return (String)_printCB.getSelectedItem();
  }
  public String getDefectReporterFile()
  {
    return (String)_fileCB.getSelectedItem();
  }

  public void setDefectReporterDisplay(String rhs)
  {
    if (rhs == DEFECTIVE)
      this._displayCB.setSelectedIndex(2);
    else if (rhs == ALWAYS)
      this._displayCB.setSelectedIndex(1);
    else
      this._displayCB.setSelectedIndex(0);
  }
  public void setDefectReporterPrint(String rhs)
  {
    if (rhs == DEFECTIVE)
      this._printCB.setSelectedIndex(2);
    else if (rhs == ALWAYS)
      this._printCB.setSelectedIndex(1);
    else
      this._printCB.setSelectedIndex(0);
  }
  public void setDefectReporterFile(String rhs)
  {
    if (rhs == DEFECTIVE)
      this._fileCB.setSelectedIndex(2);
    else if (rhs == ALWAYS)
      this._fileCB.setSelectedIndex(1);
    else
      this._fileCB.setSelectedIndex(0);
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("ReportsOptionPane [");
    buf.append(" Defect Reporter - ");
    buf.append(" Display:" + getDefectReporterDisplay());
    buf.append(", Print:" + getDefectReporterPrint());
    buf.append(", File:" + getDefectReporterFile());
    buf.append(" ]");
    return buf.toString();
  }

  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _defectReportingPanel = new JPanel();
  private JLabel _fileLbl = new JLabel();
  private JLabel _printLbl = new JLabel();
  private JLabel _displayLbl = new JLabel();
  private ButtonGroup _buttonGroup1 = new ButtonGroup();
  private ButtonGroup _buttonGroup2 = new ButtonGroup();
  private ButtonGroup _buttonGroup3 = new ButtonGroup();
  private VerticalFlowLayout _verticalFlowLayout1 = new VerticalFlowLayout();
  private PairLayout _pairLayout1 = new PairLayout();
  private JComboBox _fileCB = new JComboBox();
  private JComboBox _printCB = new JComboBox();
  private JComboBox _displayCB = new JComboBox();
}