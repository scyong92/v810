package proto.menuUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class BusyCancelDialog extends JDialog
{
  private static final ImageIcon _icon = new ImageIcon(proto.menuUI.Menu5DX.class.getResource("hourglass5.gif"));

  private final JButton [] _cancelOptions = { new JButton("Cancel") };
  private JOptionPane _pane = null;

  public BusyCancelDialog(Frame frame, String message, String title)
  {
    this(frame, message, title, false);
  }

  public BusyCancelDialog(Frame frame, String message, String title, boolean modal)
  {
    super(frame, title, modal);
    _pane = new JOptionPane(message, JOptionPane.INFORMATION_MESSAGE,
                            JOptionPane.DEFAULT_OPTION, _icon,
                            _cancelOptions, _cancelOptions[0]);

    Container             contentPane = this.getContentPane();
    contentPane.setLayout(new BorderLayout());
    contentPane.add(_pane, BorderLayout.CENTER);
    this.pack();
    this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    this.addWindowListener(new WindowAdapter()
    {
      boolean gotFocus = false;
      public void windowClosing(WindowEvent we)
      {
        _pane.setValue(null);
      }
      public void windowActivated(WindowEvent we)
      {
        // Once window gets focus, set initial focus
        if (!gotFocus)
        {
          _pane.selectInitialValue();
          gotFocus = true;
        }
      }
    });

    if (frame != null && frame.isVisible())
      com.agilent.guiUtil.SwingUtils.centerOnComponent(this, frame);
  }

  private JButton getCancelButton()
  {
    return _cancelOptions[0];
  }

  public void addCancelActionListener(ActionListener listener)
  {
    if (listener != null)
    {
      getCancelButton().addActionListener(listener);
      getCancelButton().registerKeyboardAction(listener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
  }

  public void removeCancelActionListener(ActionListener listener)
  {
    if (listener != null)
    {
      getCancelButton().removeActionListener(listener);
      getCancelButton().unregisterKeyboardAction(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true));
    }
  }

// DEBUG
  public static void main(String[] args)
  {
    try
    {
      // Make sure the look and feel is set before any components are created.
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (ClassNotFoundException e) { /* do nothing */ }
    catch (InstantiationException e) { /* do nothing */ }
    catch (IllegalAccessException e) { /* do nothing */ }
    catch (UnsupportedLookAndFeelException e) { /* do nothing */ }

    BusyCancelDialog dlg = new BusyCancelDialog(null, "This is the cancel message", "Cancel Dialog", true);
    com.agilent.guiUtil.SwingUtils.setScreenPosition(dlg,0.5,0.25);
    dlg.addCancelActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        System.exit(0);
      }
    });
    dlg.show();
  }
// DEBUG
}