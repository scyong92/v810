package proto.menuUI;

import com.agilent.util.*;
import proto.menuUI.autotest.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author
 * @version 1.0
 */

public interface ControllerObserverInt
{
  public void setController(SystemController controller);
  public void reportError( LocalizedString err );
  public void reportWarning( LocalizedString warn );
  public void reportException( Exception e );
  public void controllerUpdate( String message, Object arg );
  public void newPanel( PanelEntry panel );
  public void loadPanel(PanelEntry panel);
  public void panelLoaded();
  public void newInspection();
  public void unloadPanel(PanelEntry panel);
  public void panelUnloaded();
  public void reportStatus(LocalizedString status);
}