package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class HelpAboutAction extends MenuAction
{
  public HelpAboutAction(Menu5DX parent)
  {
    super("About", null, "Application information", 'A', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._helpAboutMI_actionPerformed(e);
  }
}