package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class StageSpeedAction extends MenuAction
{
  public StageSpeedAction(Menu5DX parent)
  {
    super("Set Stage Speed", null, "Change the stage speed", 'P', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._stageSpeedMI_actionPerformed(e);
  }
}