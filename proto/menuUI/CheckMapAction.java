package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class CheckMapAction extends MenuAction
{
  public CheckMapAction(Menu5DX parent)
  {
    super("Check Surface Map", null, "Check the surface map", 'C', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._checkSurMapMI_actionPerformed(e);
  }
}