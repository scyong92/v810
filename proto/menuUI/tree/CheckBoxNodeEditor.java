package proto.menuUI.tree;

import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.EventObject;
import javax.swing.event.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class CheckBoxNodeEditor extends CheckBoxNodeRenderer implements TreeCellEditor
{
  private EventListenerList _listenerList = new EventListenerList();
  private ChangeEvent _changeEvent = null;
  private JTree _tree;

  public CheckBoxNodeEditor( JTree tree )
  {
    _tree = tree;
  }

  public Component getTreeCellEditorComponent(JTree tree, Object value, boolean isSelected, boolean expanded, boolean leaf, int row)
  {
    Component editor = getTreeCellRendererComponent(tree, value, true, expanded, leaf, row, true);

    // editor always selected / focused
    ItemListener itemListener = new ItemListener()
    {
      public void itemStateChanged(ItemEvent itemEvent)
      {
        if (stopCellEditing())
          fireEditingStopped();
      }
    };
    if (editor instanceof JCheckBox)
      ((JCheckBox)editor).addItemListener(itemListener);

    return editor;
  }

  public Object getCellEditorValue()
  {
    CheckBoxNode checkBoxNode = new CheckBoxNode(_leafRenderer.getText(), _leafRenderer.isSelected());
    return checkBoxNode;
  }

  public boolean isCellEditable(EventObject event)
  {
    boolean returnValue = false;
    if (event instanceof MouseEvent)
    {
      MouseEvent mouseEvent = (MouseEvent)event;
      TreePath path = _tree.getPathForLocation(mouseEvent.getX(), mouseEvent.getY());
      if (path != null)
      {
        Object node = path.getLastPathComponent();
        if ((node != null) && (node instanceof DefaultMutableTreeNode))
        {
          DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode)node;
          Object userObject = treeNode.getUserObject();
          returnValue = ((treeNode.isLeaf()) && (userObject instanceof CheckBoxNode));
        }
      }
    }
    return returnValue;
  }

  public boolean shouldSelectCell(EventObject event)
  {
    return true;
  }

  public boolean stopCellEditing()
  {
    // validation if appropriate
    return true;
  }

  public void cancelCellEditing()
  {
    fireEditingCanceled();
  }

  public void addCellEditorListener(CellEditorListener listener)
  {
    _listenerList.add(CellEditorListener.class, listener);
  }

  public void removeCellEditorListener(CellEditorListener listener)
  {
    _listenerList.remove(CellEditorListener.class, listener);
  }

  protected void fireEditingCanceled()
  {
    Object listeners[] = _listenerList.getListenerList();
    for (int i = listeners.length - 2; i >= 0; i -= 2)
    {
      if (listeners[i] == CellEditorListener.class)
      {
        // Lazily create the change event
        if (_changeEvent == null)
        {
          _changeEvent = new ChangeEvent(this);
        }
        ((CellEditorListener)listeners[i]).editingCanceled(_changeEvent);
      }
    }
  }

  protected void fireEditingStopped()
  {
    Object listeners[] = _listenerList.getListenerList();
    for (int i = listeners.length - 2; i >= 0; i -= 2)
    {
      if (listeners[i] == CellEditorListener.class)
      {
        // Lazily create the change event
        if (_changeEvent == null)
        {
          _changeEvent = new ChangeEvent(this);
        }
        ((CellEditorListener)listeners[i]).editingStopped(_changeEvent);
      }
    }
  }
}