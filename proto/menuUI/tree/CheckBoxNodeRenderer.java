package proto.menuUI.tree;

import javax.swing.tree.*;
import java.awt.*;
import javax.swing.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class CheckBoxNodeRenderer implements TreeCellRenderer
{
  protected JCheckBox _leafRenderer = new JCheckBox();
  protected DefaultTreeCellRenderer _nonLeafRenderer = new DefaultTreeCellRenderer();
  private Color _selectionBorderColor, _selectionForeground, _selectionBackground,
                _textForeground, _textBackground;

  public CheckBoxNodeRenderer()
  {
    Font fontValue = UIManager.getFont("Tree.font");
    if (fontValue != null)
    {
      _leafRenderer.setFont(fontValue);
    }
    Boolean booleanValue = (Boolean)UIManager.get("Tree.drawsFocusBorderAroundIcon");
    if (booleanValue != null)
      _leafRenderer.setFocusPainted(booleanValue.booleanValue());
    else
      _leafRenderer.setFocusPainted(false);

    _selectionBorderColor = UIManager.getColor("Tree.selectionBorderColor");
    _selectionForeground = UIManager.getColor("Tree.selectionForeground");
    _selectionBackground = UIManager.getColor("Tree.selectionBackground");
    _textForeground = UIManager.getColor("Tree.textForeground");
    _textBackground = UIManager.getColor("Tree.textBackground");
  }

  public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus)
  {
    Component returnValue;
    if (leaf)
    {
      String stringValue = tree.convertValueToText(value, selected, expanded, leaf, row, false);
      _leafRenderer.setText(stringValue);
      _leafRenderer.setSelected(false);
      _leafRenderer.setEnabled(tree.isEnabled());

      if (selected)
      {
        _leafRenderer.setForeground(_selectionForeground);
        _leafRenderer.setBackground(_selectionBackground);
      }
      else
      {
        _leafRenderer.setForeground(_textForeground);
        _leafRenderer.setBackground(_textBackground);
      }

      if ((value != null) && (value instanceof DefaultMutableTreeNode))
      {
        Object userObject = ((DefaultMutableTreeNode)value).getUserObject();
        if (userObject instanceof CheckBoxNode)
        {
          CheckBoxNode node = (CheckBoxNode)userObject;
          _leafRenderer.setText(node.getText());
          _leafRenderer.setSelected(node.isSelected());
        }
      }
      returnValue = _leafRenderer;
    }
    else
    {
      returnValue = _nonLeafRenderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
    }
    return returnValue;
  }
}