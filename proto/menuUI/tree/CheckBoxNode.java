package proto.menuUI.tree;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class CheckBoxNode
{
  private String _text;
  private boolean _selected;

  public CheckBoxNode( String text, boolean selected )
  {
    _text = text;
    _selected = selected;
  }

  public boolean isSelected()
  {
    return _selected;
  }

  public void setSelected( boolean selected )
  {
    _selected = selected;
  }

  public String getText()
  {
    return _text;
  }

  public void setText( String text )
  {
    _text = text;
  }

  public String toString()
  {
    return getClass().getName() + "[" + _text + "/" + _selected + "]";
  }
}