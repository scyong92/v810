package proto.menuUI;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;
import java.awt.event.*;
import com.agilent.guiUtil.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class SerialNumOptionPane extends JPanel
{
  public static final int PROGRAM_SELECTION_PROMPT = 0;
  public static final int PROGRAM_SELECTION_PRESELECT = 1;
  public static final int PROGRAM_SELECTION_SERIAL_NUMBER_LOOKUP = 2;
  public static final int PROGRAM_SELECTION_SERIAL_NUMBER_FILE = 3;

  public static final int PANEL_BASED_SERIAL_NUMBERS = 0;
  public static final int BOARD_BASED_SERIAL_NUMBERS = 1;

  private SerialNumberSchemeDialog _snsDlg = null;  // dialog for individual serial number files
  private SerialNumberSkipDialog _skipDlg = null;  // dialog for individual serial number files
  private SerialNumberLookupDialog _lookupDlg = null;  // dialog for individual serial number files

  public SerialNumOptionPane()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    _enableBoardSkipCB.setText("Enable serial number board skipping");
    _enableBoardSkipCB.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        _enableBoardSkipCB_stateChanged(e);
      }
    });
    _boardSkipEditBtn.setEnabled(false);
    _boardSkipEditBtn.setText("Edit...");
    _boardSkipEditBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _boardSkipEditBtn_actionPerformed(e);
      }
    });
    _boardBasedRB.setText("Use board-based serial numbers");
    _panelBasedRB.setSelected(true);
    _panelBasedRB.setText("Use panel-based serial numbers");
    _serialNumSelectionRB.setText("Use serial number to select panel program");
    _serialNumSelectionRB.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        _serialNumSelectionRB_stateChanged(e);
      }
    });
    _serialNumEditBtn.setEnabled(false);
    _serialNumEditBtn.setText("Edit...");
    _serialNumEditBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _serialNumEditBtn_actionPerformed(e);
      }
    });
    _promptSelectionRB.setSelected(true);
    _promptSelectionRB.setText("Prompt for panel program");
    _preSelectionRB.setText("Pre-select panel program");
    _individualFileSelectionRB.setText("Use individual serial number files");
    _individualFileSelectionRB.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        _individualFileSelectionRB_stateChanged(e);
      }
    });
    _individualFileEditBtn.setEnabled(false);
    _individualFileEditBtn.setText("Edit...");
    _individualFileEditBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _individualFileEditBtn_actionPerformed(e);
      }
    });

    ButtonGroup _buttonGroup1 = new ButtonGroup();
    _buttonGroup1.add(_serialNumSelectionRB);
    _buttonGroup1.add(_individualFileSelectionRB);
    _buttonGroup1.add(_preSelectionRB);
    _buttonGroup1.add(_promptSelectionRB);
    ButtonGroup _buttonGroup2 = new ButtonGroup();
    _buttonGroup2.add(_panelBasedRB);
    _buttonGroup2.add(_boardBasedRB);

    _serialNumSelectionPanel.setLayout(_pairLayout2);
    _serialNumSelectionPanel.add(_serialNumSelectionRB, null);
    _serialNumSelectionPanel.add(_serialNumEditBtn, null);
    _serialNumSelectionPanel.add(_individualFileSelectionRB, null);
    _serialNumSelectionPanel.add(_individualFileEditBtn, null);

    _programSelectPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Panel Program Selection "));
    _programSelectPanel.setLayout(_verticalFlowLayout3);
    _programSelectPanel.add(_promptSelectionRB, null);
    _programSelectPanel.add(_preSelectionRB, null);
    _programSelectPanel.add(_serialNumSelectionPanel, null);

    _serialNumPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Serial Numbers "));
    _serialNumPanel.setLayout(_verticalFlowLayout2);
    _serialNumPanel.add(_panelBasedRB, null);
    _serialNumPanel.add(_boardBasedRB, null);

    _pairPanel.setBorder(BorderFactory.createEmptyBorder(0,5,0,0));
    _pairPanel.setLayout(_pairLayout1);
    _pairPanel.add(_enableBoardSkipCB, null);
    _pairPanel.add(_boardSkipEditBtn, null);

    _centerPanel.setLayout(_verticalFlowLayout1);
    _centerPanel.add(_programSelectPanel, null);
    _centerPanel.add(_serialNumPanel, null);
    _centerPanel.add(_pairPanel, null);
    this.setBorder(BorderFactory.createEmptyBorder(10,0,5,0));
    this.setLayout(_borderLayout1);
    this.add(_centerPanel, BorderLayout.CENTER);
  }

  public int getProgramSelection()
  {
    if (this._individualFileSelectionRB.isSelected())
      return PROGRAM_SELECTION_SERIAL_NUMBER_FILE;
    else if (this._serialNumSelectionRB.isSelected())
      return PROGRAM_SELECTION_SERIAL_NUMBER_LOOKUP;
    else if (this._preSelectionRB.isSelected())
      return PROGRAM_SELECTION_PRESELECT;
    else
      return PROGRAM_SELECTION_PROMPT;
  }

  public int getSerialNumberBased()
  {
    if (this._boardBasedRB.isSelected())
      return BOARD_BASED_SERIAL_NUMBERS;
    else
      return PANEL_BASED_SERIAL_NUMBERS;
  }

  public boolean getBoardSkipped()
  {
    return this._enableBoardSkipCB.isSelected();
  }

  public void setProgramSelection(int selection)
  {
    if (selection == PROGRAM_SELECTION_SERIAL_NUMBER_FILE)
      this._individualFileSelectionRB.setSelected(true);
    else if (selection == PROGRAM_SELECTION_SERIAL_NUMBER_LOOKUP)
      this._serialNumSelectionRB.setSelected(true);
    else if (selection == PROGRAM_SELECTION_PRESELECT)
      this._preSelectionRB.setSelected(true);
    else
      this._promptSelectionRB.setSelected(true);
  }

  public void setSerialNumberBased(int based)
  {
    if (based == BOARD_BASED_SERIAL_NUMBERS)
      this._boardBasedRB.setSelected(true);
    else
      this._panelBasedRB.setSelected(true);
  }

  public void setBoardSkipped(boolean rhs)
  {
    this._enableBoardSkipCB.setSelected(rhs);
  }

  // Callback methods
  void _serialNumSelectionRB_stateChanged(ChangeEvent e)
  {
    this._serialNumEditBtn.setEnabled( this._serialNumSelectionRB.isSelected() );
  }

  void _individualFileSelectionRB_stateChanged(ChangeEvent e)
  {
    this._individualFileEditBtn.setEnabled( this._individualFileSelectionRB.isSelected() );
  }

  void _enableBoardSkipCB_stateChanged(ChangeEvent e)
  {
    this._boardSkipEditBtn.setEnabled( this._enableBoardSkipCB.isSelected() );
  }

  void _boardSkipEditBtn_actionPerformed(ActionEvent e)
  {
    if (_skipDlg == null)
      _skipDlg = new SerialNumberSkipDialog(null, "Skip Boards", true);
    com.agilent.guiUtil.SwingUtils.centerOnComponent(_skipDlg,this);
    int returnVal = _skipDlg.showDialog();
// DEBUG
    if (returnVal == JOptionPane.OK_OPTION)
      System.out.println("OK");
    else
      System.out.println("Canceled");
// DEBUG
  }

  void _individualFileEditBtn_actionPerformed(ActionEvent e)
  {
    if (_snsDlg == null)
      _snsDlg = new SerialNumberSchemeDialog(null, "Individual Serial Number Files", true);
    com.agilent.guiUtil.SwingUtils.centerOnComponent(_snsDlg,this);
    int returnVal = _snsDlg.showDialog();
// DEBUG
    if (returnVal == JOptionPane.OK_OPTION)
      System.out.println("OK");
    else
      System.out.println("Canceled");
// DEBUG
  }

  void _serialNumEditBtn_actionPerformed(ActionEvent e)
  {
    if (_lookupDlg == null)
      _lookupDlg = new SerialNumberLookupDialog(null, "Lookup Panel Program", true);
    com.agilent.guiUtil.SwingUtils.centerOnComponent(_lookupDlg,this);
    int returnVal = _lookupDlg.showDialog();
// DEBUG
    if (returnVal == JOptionPane.OK_OPTION)
      System.out.println("OK");
    else
      System.out.println("Canceled");
// DEBUG
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("SerialNumOptionPane [");
    buf.append(" Program selection:");
    switch (getProgramSelection())
    {
      case PROGRAM_SELECTION_PROMPT:
        buf.append("Prompt");
        break;
      case PROGRAM_SELECTION_PRESELECT:
        buf.append("Pre-select");
        break;
      case PROGRAM_SELECTION_SERIAL_NUMBER_LOOKUP:
        buf.append("SN lookup");
        break;
      case PROGRAM_SELECTION_SERIAL_NUMBER_FILE:
        buf.append("Individual files");
        break;
    }
    buf.append(", SN:");
    switch (getSerialNumberBased())
    {
      case PANEL_BASED_SERIAL_NUMBERS:
        buf.append("Panel-based");
        break;
      case BOARD_BASED_SERIAL_NUMBERS:
        buf.append("Board-based");
        break;
    }
    buf.append(", Skipping:" + getBoardSkipped());
    buf.append(" ]");
    return buf.toString();
  }

  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _programSelectPanel = new JPanel();
  private JPanel _serialNumPanel = new JPanel();
  private JPanel _pairPanel = new JPanel();
  private JCheckBox _enableBoardSkipCB = new JCheckBox();
  private JButton _boardSkipEditBtn = new JButton();
  private JRadioButton _boardBasedRB = new JRadioButton();
  private JRadioButton _panelBasedRB = new JRadioButton();
  private JPanel _serialNumSelectionPanel = new JPanel();
  private JRadioButton _serialNumSelectionRB = new JRadioButton();
  private JButton _serialNumEditBtn = new JButton();
  private JRadioButton _promptSelectionRB = new JRadioButton();
  private JRadioButton _preSelectionRB = new JRadioButton();
  private VerticalFlowLayout _verticalFlowLayout1 = new VerticalFlowLayout( VerticalFlowLayout.FILL, VerticalFlowLayout.TOP);
  private VerticalFlowLayout _verticalFlowLayout2 = new VerticalFlowLayout();
  private VerticalFlowLayout _verticalFlowLayout3 = new VerticalFlowLayout();
  private PairLayout _pairLayout1 = new PairLayout(10, 5, false);
  private PairLayout _pairLayout2 = new PairLayout();
  private JRadioButton _individualFileSelectionRB = new JRadioButton();
  private JButton _individualFileEditBtn = new JButton();
}