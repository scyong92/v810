package proto.menuUI;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.*;
import com.agilent.guiUtil.VerticalFlowLayout;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class PanelHandlingOptionPane extends JPanel
{
  public static final String AUTOMATIC_FLOW_THROUGH = "Automatic Flow Through (SMEMA)";
  public static final String MANUAL_FLOW_THROUGH = "Manual Flow Through";
  public static final String AUTOMATIC_PASS_BACK = "Automatic Pass Back (SMEMA)";
  public static final String MANUAL_PASS_BACK = "Manual Pass Back";

  public PanelHandlingOptionPane()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    _middleRailCkB.setText("Middle rail present");
    _middleRailCkB.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        _middleRailCkB_stateChanged(e);
      }
    });
    _dualModeCkB.setEnabled(false);
    _dualModeCkB.setText("Dual panel mode");
    _modeLbl.setText("Panel handling mode:");
    _modeCB.addItem(AUTOMATIC_FLOW_THROUGH);
    _modeCB.addItem(MANUAL_FLOW_THROUGH);
    _modeCB.addItem(AUTOMATIC_PASS_BACK);
    _modeCB.addItem(MANUAL_PASS_BACK);

    _modePanel.add(_modeLbl, null);
    _modePanel.add(_modeCB, null);
    _dualModePanel.setBorder(BorderFactory.createEmptyBorder(0,15,0,0));
    _dualModePanel.add(_dualModeCkB, null);
    _middleRailPanel.setLayout(_verticalFlowLayout1);
    _middleRailPanel.add(_middleRailCkB, null);
    _middleRailPanel.add(_dualModePanel, null);

    _centerPanel.setLayout(_verticalFlowLayout1);
    _centerPanel.add(_modePanel, null);
    _centerPanel.add(_middleRailPanel, null);

    this.setBorder(BorderFactory.createEmptyBorder(10,0,5,0));
    this.setLayout(_borderLayout1);
    this.add(_centerPanel, BorderLayout.CENTER);
  }

  /**
   * Return the panel handling mode.
   */
  public String getPanelHandlingMode()
  {
    return (String)this._modeCB.getSelectedItem();
  }

  /**
   * Return whether the middle rail is present.
   */
  public boolean getMiddleRailPresent()
  {
    return this._middleRailCkB.isSelected();
  }

  /**
   * Return whether using dual panel mode.
   */
  public boolean getDualPanelMode()
  {
    return (this._middleRailCkB.isSelected() && this._dualModeCkB.isSelected());
  }

  /**
   * Return the panel handling mode.
   */
  public void setPanelHandlingMode(String mode)
  {
    if (mode == MANUAL_PASS_BACK)
      this._modeCB.setSelectedIndex(3);
    if (mode == AUTOMATIC_PASS_BACK)
      this._modeCB.setSelectedIndex(2);
    if (mode == MANUAL_FLOW_THROUGH)
      this._modeCB.setSelectedIndex(1);
    else  // AUTOMATIC_FLOW_THROUGH
      this._modeCB.setSelectedIndex(0);
  }

  /**
   * Return whether the middle rail is present.
   */
  public void setMiddelRailPresent(boolean b)
  {
    this._middleRailCkB.setSelected(b);
  }

  /**
   * Return whether using dual panel mode.
   */
  public void setDualPanelMode(boolean b)
  {
    this._middleRailCkB.setSelected(b);
    this._dualModeCkB.setSelected(b);
  }

  // Callback methods
  /**
   * State changed callback for the middle rail present check box.  Enables/disables
   * the dual panel mode check box.
   */
  void _middleRailCkB_stateChanged(ChangeEvent e)
  {
    this._dualModeCkB.setEnabled(this._middleRailCkB.isSelected());
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("PanelHandlingOptionPane [");
    buf.append(" Mode:" + getPanelHandlingMode());
    buf.append(", Middle rail:" + getMiddleRailPresent());
    buf.append(", Dual panel:" + getDualPanelMode());
    buf.append(" ]");
    return buf.toString();
  }

  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _modePanel = new JPanel();
  private JPanel _middleRailPanel = new JPanel();
  private JCheckBox _middleRailCkB = new JCheckBox();
  private JPanel _dualModePanel = new JPanel();
  private JCheckBox _dualModeCkB = new JCheckBox();
  private JLabel _modeLbl = new JLabel();
  private JComboBox _modeCB = new JComboBox();
  private VerticalFlowLayout _verticalFlowLayout1 = new VerticalFlowLayout();
  private VerticalFlowLayout _verticalFlowLayout2 = new VerticalFlowLayout();

}