package proto.menuUI;

import java.awt.event.ActionEvent;
import javax.swing.*;

/**
 * Title:        MenuAction
 * Description:  This class provides a concrete implementation for the 5DX main
 * menu and toolbar items.  The <code>actionPerformed</code> method MUST be
 * overridden if you want anything to happen when the menu item or toolbar
 * button is clicked.  An example of this follows:
 * <P><code>
 *
 *  MenuAction _fileOpenAction = new MenuAction("File Open", "fileOpenIconName.gif")
 *  {
 *    public void actionPerformed(ActionEvent e)
 *    {
 *      fileOpenMethod(e);
 *    }
 *  });
 *  _menuBar.add(_fileOpenAction);
 *  _toolBar.add(_fileOpenAction);
 *
 * </code><P>
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies
 * @author       Steve Anonson
 * @version 1.0
 */

public class MenuAction extends AbstractAction
{
  protected char _mnemonic;
  protected Menu5DX _parent = null;

  /**
   * Constructor
   * @param   name - String given to the menu item.
   * @param   parent - Menu5DX parent class.
   */
  public MenuAction( String name, Menu5DX parent )
  {
    this(name, null, null, parent);
  }

  /**
   * Constructor
   * @param   name - String given to the menu item.
   * @param   icon - Icon used for toolbar button.
   * @param   parent - Menu5DX parent class.
   */
  public MenuAction( String name, Icon icon, Menu5DX parent )
  {
    this(name, icon, null, parent);
  }

  /**
   * Constructor
   * @param   name - String given to the menu item.
   * @param   icon - Icon used for toolbar button.
   * @param   toolTip - String used for the tool tip for menu or toolbar.
   * @param   parent - Menu5DX parent class.
   */
  public MenuAction( String name, Icon icon, String tooltip, Menu5DX parent )
  {
    super(name, icon);
    this.setTooltip(tooltip);
    _parent = parent;
  }

  /**
   * Constructor
   * @param   name - String given to the menu item.
   * @param   icon - Icon used for toolbar button.
   * @param   toolTip - String used for the tool tip for menu or toolbar.
   * @param   mnemonic - character used for keyboard shortcut.
   * @param   parent - Menu5DX parent class.
   */
  public MenuAction( String name, Icon icon, String tooltip, char mnemonic, Menu5DX parent )
  {
    super(name, icon);
    this.setTooltip(tooltip);
    _mnemonic = mnemonic;
    _parent = parent;
  }

  /**
   * This method set the mnemonic for the menu button.
   * @param   mnemonic - char used for the menu mnemonic.
   */
  public char getMnemonic()
  {
    return _mnemonic;
  }

  /**
   * This method returns the name string.
   */
  public String getName()
  {
    return (String)this.getValue(Action.NAME);
  }

  /**
   * This method returns the tooltip string.
   */
  public String getTooltip()
  {
    return (String)this.getValue(Action.SHORT_DESCRIPTION);
  }

  /**
   * This method returns the Menu5DX parent.
   */
  public Menu5DX getParent()
  {
    return _parent;
  }

  /**
   * This method assigns a String for the tool tip.
   * @param   toolTip - String used for the tool tip for menu or toolbar.
   */
  public void setTooltip( String tooltip )
  {
    this.putValue(Action.SHORT_DESCRIPTION, tooltip);
  }

  /**
   * This method assigns a descriptive String to the action that can be used
   * for context sensitive help.
   * @param   description - String description for the action.
   */
  public void setDescription( String description )
  {
    this.putValue(Action.LONG_DESCRIPTION, description);
  }

  /**
   * This method assigns a icon to the action that can be used
   * for the toolbar.
   * @param   icon - icon for the toolbar.
   */
  public void setIcon( Icon icon )
  {
    this.putValue(Action.SMALL_ICON, icon);
  }

  /**
   * This method MUST be overridden to make the menu button work.
   */
  public void actionPerformed(ActionEvent parm1)
  {
    /* Override this method or nothing will happen. */
  }
}
