package proto.menuUI;

import java.awt.*;
import javax.swing.border.Border;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;
import com.agilent.guiUtil.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class SelfTestOptionPane extends JPanel
{
  public static final String PROMPT = "Prompt";
  public static final String IGNORE = "Ignore";
  public static final String STOP = "Stop";

  public SelfTestOptionPane()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    _printCkB.setEnabled(false);
    _printCkB.setText("Print results to default printer");
    _displayCkB.setEnabled(false);
    _displayCkB.setText("Display results in test window");
    _enableSelfTestCkB.setText("Enable automatic self-test (Laser, Gray level, Thickness, POF/Resolution)");
    _enableSelfTestCkB.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        _enableSelfTestCkB_stateChanged(e);
      }
    });
    _startupCkB.setEnabled(false);
    _startupCkB.setText("At startup");
    _boardsCkB.setEnabled(false);
    _boardsCkB.setText("Every");
    _boardsCkB.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        _boardsCkB_stateChanged(e);
      }
    });
    _boardsLbl.setEnabled(false);
    _boardsLbl.setText("boards");
    _panelsLbl.setEnabled(false);
    _panelsLbl.setText("panels");
    _panelsCkB.setEnabled(false);
    _panelsCkB.setText("Every");
    _panelsCkB.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        _panelsCkB_stateChanged(e);
      }
    });
    _minutesLbl.setEnabled(false);
    _minutesLbl.setText("minutes");
    _minutesCkB.setEnabled(false);
    _minutesCkB.setText("Every");
    _minutesCkB.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        _minutesCkB_stateChanged(e);
      }
    });
    _stopRB.setEnabled(false);
    _stopRB.setText("Stop");
    _ignoreRB.setEnabled(false);
    _ignoreRB.setText("Ignore");
    _promptRB.setEnabled(false);
    _promptRB.setSelected(true);
    _promptRB.setText("Prompt");
    _failureLbl.setEnabled(false);
    _failureLbl.setText("If test fails:");
    _grayCompensationCkB.setEnabled(false);
    _grayCompensationCkB.setText("Use gray level compensation");

    _boardsSpinBox.setEnabled(false);
    _minutesSpinBox.setEnabled(false);
    _panelsSpinBox.setEnabled(false);

    _startupPanel.add(_startupCkB, null);
    _boardsPanel.add(_boardsCkB, null);
    _boardsPanel.add(_boardsSpinBox, null);
    _boardsPanel.add(_boardsLbl, null);
    _panelsPanel.add(_panelsCkB, null);
    _panelsPanel.add(_panelsSpinBox, null);
    _panelsPanel.add(_panelsLbl, null);
    _minutesPanel.add(_minutesCkB, null);
    _minutesPanel.add(_minutesSpinBox, null);
    _minutesPanel.add(_minutesLbl, null);
    _frequencyPanel.setLayout(_verticalFlowLayout3);
    _frequencyPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Frequency "));
    _frequencyPanel.add(_startupPanel, null);
    _frequencyPanel.add(_boardsPanel, null);
    _frequencyPanel.add(_panelsPanel, null);
    _frequencyPanel.add(_minutesPanel, null);

    _displayPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Results "));
    _displayPanel.setLayout(_verticalFlowLayout2);
    _displayPanel.add(_displayCkB, null);
    _displayPanel.add(_printCkB, null);

    _grayCompPanel.setLayout( new FlowLayout(FlowLayout.LEFT) );
    _grayCompPanel.add(_grayCompensationCkB, null);

    _failurePanel.add(_failureLbl, null);
    _failurePanel.add(_promptRB, null);
    _failurePanel.add(_ignoreRB, null);
    _failurePanel.add(_stopRB, null);
    _buttonGroup1.add(_promptRB);
    _buttonGroup1.add(_ignoreRB);
    _buttonGroup1.add(_stopRB);

    _selfTestPanel.setBorder(BorderFactory.createEmptyBorder(0,15,0,0));
    _selfTestPanel.setLayout(_verticalFlowLayout1);
    _selfTestPanel.add(_frequencyPanel, null);
    _selfTestPanel.add(_displayPanel, null);
    _selfTestPanel.add(_grayCompPanel, null);
    _selfTestPanel.add(_failurePanel, null);

    _centerPanel.setBorder(BorderFactory.createEmptyBorder(0,5,0,5));
    _centerPanel.setLayout(_verticalFlowLayout4);
    _centerPanel.add(_enableSelfTestCkB, null);
    _centerPanel.add(_selfTestPanel, null);

    this.setBorder(BorderFactory.createEmptyBorder(10,0,5,0));
    this.setLayout(_borderLayout1);
    this.add(_centerPanel, BorderLayout.CENTER);
  }

  public boolean isSelfTestEnabled()  { return this._enableSelfTestCkB.isSelected(); }
  public boolean getGrayLevelCompensation()  { return this._grayCompensationCkB.isSelected(); }
  public String getTestFailureAction()
  {
    if (this._stopRB.isSelected())
      return STOP;
    else if (this._ignoreRB.isSelected())
      return IGNORE;
    else
      return PROMPT;
  }
  public boolean getRunAtStartup()    { return this._startupCkB.isSelected(); }
  /**
   * @return - number of boards between self tests, zero if not enabled.
   */
  public int getRunEveryNBoards()
  {
    return ((this._boardsCkB.isSelected()) ? this._boardsSpinBox.getValue() : 0);
  }
  /**
   * @return - number of panels between self tests, zero if not enabled.
   */
  public int getRunEveryNPanels()
  {
    return ((this._panelsCkB.isSelected()) ? this._panelsSpinBox.getValue() : 0);
  }
  /**
   * @return - number of minutes between self tests, zero if not enabled.
   */
  public int getRunEveryNMinutes()
  {
    return ((this._minutesCkB.isSelected()) ? this._minutesSpinBox.getValue() : 0);
  }
  public boolean getDisplayResults()     { return this._displayCkB.isSelected(); }
  public boolean getPrintResults()       { return this._printCkB.isSelected(); }

  public void setSelfTestEnabled(boolean b) { this._enableSelfTestCkB.setSelected(b); }
  public void setGrayLevelCompensation(boolean b) { this._grayCompensationCkB.setSelected(b); }
  public void setTestFailureAction(String rhs)
  {
    if (rhs == STOP)
      this._stopRB.setSelected(true);
    else if (rhs == IGNORE)
      this._ignoreRB.setSelected(true);
    else
      this._promptRB.setSelected(true);
  }
  public void setRunAtStartup(boolean b)    { this._startupCkB.setSelected(b); }
  /**
   * @return - number of boards between self tests, zero if not enabled.
   */
  public void setRunEveryNBoards(int num)
  {
    if (num > 0)
    {
      this._boardsCkB.setSelected(true);
      this._boardsSpinBox.setValue(num);
    }
    else
      this._boardsCkB.setSelected(false);
  }
  /**
   * @return - number of panels between self tests, zero if not enabled.
   */
  public void setRunEveryNPanels(int num)
  {
    if (num > 0)
    {
      this._panelsCkB.setSelected(true);
      this._panelsSpinBox.setValue(num);
    }
    else
      this._panelsCkB.setSelected(false);
  }
  /**
   * @return - number of minutes between self tests, zero if not enabled.
   */
  public void setRunEveryNMinutes(int num)
  {
    if (num > 0)
    {
      this._minutesCkB.setSelected(true);
      this._minutesSpinBox.setValue(num);
    }
    else
      this._minutesCkB.setSelected(false);
  }
  public void setDisplayResults(boolean b)     { this._displayCkB.setSelected(b); }
  public void setPrintResults(boolean b)       { this._printCkB.setSelected(b); }

  /**
   * State changed callback for the enable self test check box.  Enables/disables
   * all other components.
   */
  void _enableSelfTestCkB_stateChanged(ChangeEvent e)
  {
    boolean enabled = _enableSelfTestCkB.isSelected();
    _stopRB.setEnabled(enabled);
    _ignoreRB.setEnabled(enabled);
    _promptRB.setEnabled(enabled);
    _promptRB.setSelected(enabled);
    _failureLbl.setEnabled(enabled);
    _grayCompensationCkB.setEnabled(enabled);
    _printCkB.setEnabled(enabled);
    _displayCkB.setEnabled(enabled);
    _startupCkB.setEnabled(enabled);
    _boardsCkB.setEnabled(enabled);
    _boardsLbl.setEnabled(enabled);
    _panelsLbl.setEnabled(enabled);
    _panelsCkB.setEnabled(enabled);
    _minutesLbl.setEnabled(enabled);
    _minutesCkB.setEnabled(enabled);
    // Always disable the spin boxes but only enable the ones selected
    if (enabled == false || _panelsCkB.isSelected())
      _panelsSpinBox.setEnabled(enabled);
    if (enabled == false || _boardsCkB.isSelected())
      _boardsSpinBox.setEnabled(enabled);
    if (enabled == false || _minutesCkB.isSelected())
      _minutesSpinBox.setEnabled(enabled);
  }

  /**
   * State changed callback for the every N boards check box.  Enables/disables
   * the boards spin box.
   */
  void _boardsCkB_stateChanged(ChangeEvent e)
  {
    _boardsSpinBox.setEnabled(_boardsCkB.isSelected());
  }

  /**
   * State changed callback for the every N panels check box.  Enables/disables
   * the panels spin box.
   */
  void _panelsCkB_stateChanged(ChangeEvent e)
  {
    _panelsSpinBox.setEnabled(_panelsCkB.isSelected());
  }

  /**
   * State changed callback for the every N minutes check box.  Enables/disables
   * the minutes spin box.
   */
  void _minutesCkB_stateChanged(ChangeEvent e)
  {
    _minutesSpinBox.setEnabled(_minutesCkB.isSelected());
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    String or = " |";
    buf.append("SelfTestOptionPane [");
    buf.append(" Gray Compensation:" + getGrayLevelCompensation());
    buf.append(", Failures:" + getTestFailureAction());
    buf.append(", Frequency:");
    int count = 0;
    if (getRunAtStartup())
    {
      buf.append(" At startup");
      count++;
    }
    if (getRunEveryNBoards() > 0)
    {
      if (count > 0)
        buf.append(or);
      buf.append(" Every " + getRunEveryNBoards() + " boards");
    }
    if (getRunEveryNPanels() > 0)
    {
      if (count > 0)
        buf.append(or);
      buf.append(" Every " + getRunEveryNPanels() + " panels");
    }
    if (getRunEveryNMinutes() > 0)
    {
      if (count > 0)
        buf.append(or);
      buf.append(" Every " + getRunEveryNMinutes() + " minutes");
    }
    buf.append(", Display:" + getDisplayResults());
    buf.append(", Print:" + getPrintResults());
    buf.append(" ]");
    return buf.toString();
  }

  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _selfTestPanel = new JPanel();
  private JPanel _displayPanel = new JPanel();
  private JPanel _frequencyPanel = new JPanel();
  private JCheckBox _printCkB = new JCheckBox();
  private JCheckBox _displayCkB = new JCheckBox();
  private JCheckBox _enableSelfTestCkB = new JCheckBox();
  private JPanel _minutesPanel = new JPanel();
  private JPanel _panelsPanel = new JPanel();
  private JPanel _boardsPanel = new JPanel();
  private JPanel _startupPanel = new JPanel();
  private JCheckBox _startupCkB = new JCheckBox();
  private JCheckBox _boardsCkB = new JCheckBox();
  private JLabel _boardsLbl = new JLabel();
  private JLabel _panelsLbl = new JLabel();
  private JCheckBox _panelsCkB = new JCheckBox();
  private JLabel _minutesLbl = new JLabel();
  private JCheckBox _minutesCkB = new JCheckBox();
  private IntegerSpinBox _panelsSpinBox = new IntegerSpinBox(1, 1, 1, 1000);
  private IntegerSpinBox _boardsSpinBox = new IntegerSpinBox(1, 1, 1, 1000);
  private IntegerSpinBox _minutesSpinBox = new IntegerSpinBox(1, 1, 1, 1500);
  private VerticalFlowLayout _verticalFlowLayout1 = new VerticalFlowLayout(VerticalFlowLayout.FILL);
  private VerticalFlowLayout _verticalFlowLayout2 = new VerticalFlowLayout();
  private VerticalFlowLayout _verticalFlowLayout3 = new VerticalFlowLayout();
  private VerticalFlowLayout _verticalFlowLayout4 = new VerticalFlowLayout();
  private JPanel _failurePanel = new JPanel();
  private JRadioButton _stopRB = new JRadioButton();
  private JRadioButton _ignoreRB = new JRadioButton();
  private JRadioButton _promptRB = new JRadioButton();
  private JLabel _failureLbl = new JLabel();
  private JPanel _grayCompPanel = new JPanel();
  private JCheckBox _grayCompensationCkB = new JCheckBox();
  private ButtonGroup _buttonGroup1 = new ButtonGroup();
}
