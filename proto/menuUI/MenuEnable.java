package proto.menuUI;

import java.util.*;
import java.io.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 * @see MenuEnum
 */

class MenuEnable extends BitSet implements Serializable
{
  private ArrayList _enumNames;

  public MenuEnable()
  {
    super(MenuEnum.size());
    _enumNames = new ArrayList(MenuEnum.size());
    Iterator iter = MenuEnum.getMenuItems().iterator();
    while (iter.hasNext())
    {
      MenuEnum menuItem = (MenuEnum)iter.next();
      _enumNames.add(menuItem.getValue(), menuItem.toString());
    }
    this.set(MenuEnum.EXIT);
    this.set(MenuEnum.HELP_CONTENTS);
    this.set(MenuEnum.HELP_ABOUT);
  }

  /**
   * Return the menu enable value.
   */
  public boolean get( MenuEnum menu )
  {
    return super.get(menu.getValue());
  }

  /**
   * Set the menu enable to true.
   */
  public void set( MenuEnum menu )
  {
    super.set(menu.getValue());
  }

  /**
   * Set the menu enable to false.
   */
  public void clear( MenuEnum menu )
  {
    super.clear(menu.getValue());
  }

  /**
   * Set all enables to true.
   */
  protected void setAll()
  {
    for (int i = 0; i < MenuEnum.size(); i++)
      super.set(i);
  }

  /**
   * Set all enables to fales.
   */
  protected void clearAll()
  {
    for (int i = 0; i < MenuEnum.size(); i++)
      super.clear(i);
  }

  /**
   * Read in the serialized object and update it to match the current MenuEnum.
   */
  private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException
  {
    s.defaultReadObject();
    // customized deserialization code
    BitSet bitCopy = (BitSet)this.clone();
    ArrayList nameCopy = (ArrayList)_enumNames.clone();
    this.clearAll();
    _enumNames.clear();
    int i = 0;
    while (i < nameCopy.size() && i < bitCopy.size())
    {
      MenuEnum menuItem = MenuEnum.findEnum((String)nameCopy.get(i));
      if (menuItem != null && bitCopy.get(i) == true)
        this.set(menuItem);
      i++;
    }
  }
}
