package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class ChangePasswordAction extends MenuAction
{
  public ChangePasswordAction(Menu5DX parent)
  {
    super("Change Password", null, "Change this user's password", 'P', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._changePasswordMI_actionPerformed(e);
  }
}