package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author
 * @version 1.0
 */

public class LoadPanelManualAction extends MenuAction
{

  public LoadPanelManualAction(Menu5DX parent)
  {
    super("Manual Load", null, "Manually load a panel into the 5DX", 'N', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._manualLoadPanelMI_actionPerformed(e);
  }
}