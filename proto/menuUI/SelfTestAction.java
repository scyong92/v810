package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class SelfTestAction extends MenuAction
{
  public SelfTestAction(Menu5DX parent)
  {
    super("Run Selftest", null, "Run selftest on the 5dx system", 'T', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._runSelfTestMI_actionPerformed(e);
  }
}