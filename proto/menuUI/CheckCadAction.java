package proto.menuUI;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class CheckCadAction extends MenuAction
{
  private static final ImageIcon _checkCadIcon = new ImageIcon(Menu5DX.class.getResource("chkcad.gif"));

  public CheckCadAction(Menu5DX parent)
  {
    super("CheckCAD", _checkCadIcon, "Run CheckCAD", 'C', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._checkcadMI_actionPerformed(e);
  }
}