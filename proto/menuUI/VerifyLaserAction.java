package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class VerifyLaserAction extends MenuAction
{
  public VerifyLaserAction(Menu5DX parent)
  {
    super("Verify Laser Repeatability", null, "Verify laser repeatability", 'V', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._verifyLaserMI_actionPerformed(e);
  }
}