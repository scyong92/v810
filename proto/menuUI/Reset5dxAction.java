package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class Reset5dxAction extends MenuAction
{
  public Reset5dxAction(Menu5DX parent)
  {
    super("Reset 5DX", null, "Reset the 5DX", 'R', parent);
  }
  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._reset5DXMI_actionPerformed(e);
  }
}