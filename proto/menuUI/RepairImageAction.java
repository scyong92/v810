package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class RepairImageAction extends MenuAction
{
  public RepairImageAction(Menu5DX parent)
  {
    super("Collect Repair Images", null, "Save repair images", 'R', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._repairImageMI_actionPerformed(e);
  }
}