package proto.menuUI;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class TunerAction extends MenuAction
{
  private static final ImageIcon _tunerIcon = new ImageIcon(Menu5DX.class.getResource("algtuner.gif"));

  public TunerAction(Menu5DX parent)
  {
    super("Algorithm Tuner", _tunerIcon, "Run Algorithm Tuner", 'A', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._tunerMI_actionPerformed(e);
  }
}