package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class MapManualAction extends MenuAction
{
  public MapManualAction(Menu5DX parent)
  {
    super("Manual", null, "Manually surface map a panel", 'M', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._mapManualMI_actionPerformed(e);
  }
}