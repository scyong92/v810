package proto.menuUI;

import java.awt.*;
import java.awt.event.*;
import java.rmi.RemoteException;
import javax.swing.*;
import com.agilent.util.*;
import javax.swing.border.Border;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author  Steve Anonson
 * @version 1.0
 */

public class DOSShellObserverDialog extends JDialog implements RemoteObserverInt
{
  StringBuffer _buf = new StringBuffer();

  public DOSShellObserverDialog()
  {
    this(null, "DOSShellObserver", false);
  }

  public DOSShellObserverDialog(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception
  {
    _okBtn.setText("OK");
    _okBtn.setFont(new java.awt.Font("Dialog", 1, 12));
    _okBtn.setMnemonic('O');
    _okBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _okBtn_actionPerformed(e);
      }
    });
    _gridLayout2.setHgap(5);
    _okCancelPanel.setLayout(_gridLayout2 );
    _messageTA.setColumns(40);
    _messageTA.setRows(20);
    _okCancelPanel.add(_okBtn, null);
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,5,5,5));
    _flowLayout1.setAlignment(FlowLayout.RIGHT);
    _buttonPanel.setLayout(_flowLayout1);
    _buttonPanel.add(_okCancelPanel, null);

    _messageScroll.getViewport().add(_messageTA, null);

    _panel1.setBorder(BorderFactory.createEmptyBorder(10,10,5,10));
    _panel1.setLayout(_borderLayout1);
    _panel1.add(_messageScroll, BorderLayout.CENTER);
    _panel1.add(_buttonPanel, BorderLayout.SOUTH);
    getContentPane().add(_panel1);
  }

  public void update(RemoteObservableInt ro, Object arg) throws RemoteException
  {
    _buf.append(arg.toString());
    _messageTA.setText(_buf.toString());
    if (!this.isVisible())
      this.setVisible(true);
  }

  void _okBtn_actionPerformed(ActionEvent e)
  {
    this.hide();
    if (_startFromMain)
      System.exit(0);
  }

// DEBUG
  public static void main(String[] args)
  {
    try
    {
      // Make sure the look and feel is set before any components are created.
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (ClassNotFoundException e) { /* do nothing */ }
    catch (InstantiationException e) { /* do nothing */ }
    catch (IllegalAccessException e) { /* do nothing */ }
    catch (UnsupportedLookAndFeelException e) { /* do nothing */ }

    DOSShellObserverDialog dlg = new DOSShellObserverDialog();
    dlg._startFromMain = true;
    com.agilent.guiUtil.SwingUtils.setScreenPosition(dlg,0.5,0.25);
    dlg.setVisible(true);
  }
  private boolean _startFromMain = false;
// DEBUG

  private JPanel _panel1 = new JPanel();
  private BorderLayout _borderLayout1 = new BorderLayout();

  private FlowLayout _flowLayout1 = new FlowLayout();
  private JPanel _buttonPanel = new JPanel();
  private GridLayout _gridLayout2 = new GridLayout();
  private JPanel _okCancelPanel = new JPanel();
  private JButton _okBtn = new JButton();
  private JScrollPane _messageScroll = new JScrollPane();
  private JTextArea _messageTA = new JTextArea();

}