package proto.menuUI;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class UnloadPanelAction extends MenuAction
{
  private static final ImageIcon _icon = new ImageIcon(Menu5DX.class.getResource("unloadpanel.gif"));

  public UnloadPanelAction(Menu5DX parent)
  {
    super("Unload", _icon, "Unload a panel from the 5DX", 'U', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._unloadPanelMI_actionPerformed(e);
  }
}