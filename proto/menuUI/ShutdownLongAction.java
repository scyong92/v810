package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ShutdownLongAction extends MenuAction
{
  public ShutdownLongAction(Menu5DX parent)
  {
    super("Long Term", null, "Run long term shutdown", 'L', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._shortTermMI_actionPerformed(e);
  }
}