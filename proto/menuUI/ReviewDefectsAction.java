package proto.menuUI;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ReviewDefectsAction extends MenuAction
{
  private static final ImageIcon _revDefIcon = new ImageIcon(Menu5DX.class.getResource("revdef.gif"));

  public ReviewDefectsAction(Menu5DX parent)
  {
    super("Review Defects", _revDefIcon, "Run Review Defects", 'D', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._reveiwDefMI_actionPerformed(e);
  }
}