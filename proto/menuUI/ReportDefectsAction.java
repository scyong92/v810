package proto.menuUI;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ReportDefectsAction extends MenuAction
{
  private static final ImageIcon _reportIcon = new ImageIcon(Menu5DX.class.getResource("defreport.gif"));

  public ReportDefectsAction(Menu5DX parent)
  {
    super("Defect Reporter", _reportIcon, "Run Defect Reporter", 'R', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._defectReporterMI_actionPerformed(e);
  }
}