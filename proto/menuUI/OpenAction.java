package proto.menuUI;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

/**
 * Title:        OpenAction
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class OpenAction extends MenuAction
{
//  private static final ImageIcon _icon = new ImageIcon(Menu5DX.class.getResource("loadcad.gif"));
  private static final ImageIcon _icon = new ImageIcon(Menu5DX.class.getResource("open.gif"));

  public OpenAction(Menu5DX parent)
  {
    super("Open Panel Program...", _icon, "Open a panel program", 'O', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
//    System.out.println(this.getTooltip());
//
    this.getParent()._openPanelMI_actionPerformed(e);
  }
}