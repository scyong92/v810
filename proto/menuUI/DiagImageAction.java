package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class DiagImageAction extends MenuAction
{
  public DiagImageAction(Menu5DX parent)
  {
    super("Collect Diagnostic Image", null, "Save diagnostic image", 'D', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._homeRailsMI_actionPerformed(e);
  }
}