package proto.menuUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import com.agilent.xRayTest.util.*;
import com.agilent.xRayTest.images.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class Menu5DXAboutBox extends JDialog implements ActionListener
{
  private JPanel _panel1 = new JPanel();
  private JPanel _panel2 = new JPanel();
  private JPanel _insetsPanel1 = new JPanel();
  private JPanel _insetsPanel2 = new JPanel();
  private JPanel _insetsPanel3 = new JPanel();
  private JButton _okBtn = new JButton();
  private JLabel _imageLabel = new JLabel();
  private JLabel _productLbl = new JLabel();
  private JLabel _versionLbl = new JLabel();
  private JLabel _copyrightLbl = new JLabel();
  private JLabel _commentsLbl = new JLabel();
  private BorderLayout _borderLayout1 = new BorderLayout();
  private BorderLayout _borderLayout2 = new BorderLayout();
  private FlowLayout _flowLayout1 = new FlowLayout();
  private String product = "<html>5DX User Interface";
  private String version = "Version: " + Version.getVersion();
  private String copyright = "<html>Agilent Technologies, Inc.<p>All Rights Reserved<p>" + Version.getCopyRight();
  private String comments = "";
  private GridBagLayout gridBagLayout1 = new GridBagLayout();

  public Menu5DXAboutBox(Frame parent)
  {
    super(parent, true);
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    pack();
  }
  /**Component initialization*/
  private void jbInit() throws Exception
  {
    //ImageIcon _imageIcon = Image5DX.getImageIcon( Image5DX.AXI_5DX_SPLASH );
    ImageIcon _imageIcon = new ImageIcon(Image5DX.class.getResource("agilent_stack.gif"));
    //ImageIcon _imageIcon = new ImageIcon(Menu5DXAboutBox.class.getResource("Loader5DX_pale.gif"));
    _imageLabel.setIcon( _imageIcon );
    this.setTitle("About");
    setResizable(false);
    _panel1.setLayout(_borderLayout1);
    _panel2.setLayout(_borderLayout2);
    _insetsPanel1.setLayout(_flowLayout1);
    _insetsPanel2.setLayout(_flowLayout1);
    _insetsPanel2.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    _productLbl.setText(product);
    _versionLbl.setText(version);
    _copyrightLbl.setText(copyright);
    _commentsLbl.setText(comments);
    _insetsPanel3.setLayout(gridBagLayout1);
    _insetsPanel3.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    _okBtn.setText("OK");
    _okBtn.addActionListener(this);
    _insetsPanel2.add(_imageLabel, null);
    _panel2.add(_insetsPanel2, BorderLayout.WEST);
    this.getContentPane().add(_panel1, null);
    _insetsPanel3.add(_productLbl, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    _insetsPanel3.add(_versionLbl, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    _insetsPanel3.add(_copyrightLbl, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    _insetsPanel3.add(_commentsLbl, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    _panel2.add(_insetsPanel3, BorderLayout.CENTER);
    _insetsPanel1.add(_okBtn, null);
    _panel1.add(_insetsPanel1, BorderLayout.SOUTH);
    _panel1.add(_panel2, BorderLayout.NORTH);
  }
  /**Overridden so we can exit when window is closed*/
  protected void processWindowEvent(WindowEvent e)
  {
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      cancel();
    }
    super.processWindowEvent(e);
  }
  /**Close the dialog*/
  void cancel()
  {
    dispose();
  }
  /**Close the dialog on a button event*/
  public void actionPerformed(ActionEvent e)
  {
    if (e.getSource() == _okBtn)
    {
      cancel();
    }
  }
}