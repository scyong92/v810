package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class MapAutoAction extends MenuAction
{
  public MapAutoAction(Menu5DX parent)
  {
    super("Automatic", null, "Automatically surface map a panel", 'A', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._mapAutoMI_actionPerformed(e);
  }
}