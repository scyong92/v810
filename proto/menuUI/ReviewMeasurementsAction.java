package proto.menuUI;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ReviewMeasurementsAction extends MenuAction
{
  private static final ImageIcon _revMeasIcon = new ImageIcon(Menu5DX.class.getResource("revmeas.gif"));

  public ReviewMeasurementsAction(Menu5DX parent)
  {
    super("Review Measurments", _revMeasIcon, "Run Review Measurments", 'M', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._reviewMeasMI_actionPerformed(e);
  }
}