package proto.menuUI;

import java.io.*;
import java.util.StringTokenizer;
import com.agilent.xRayTest.util.HashUtil;
import proto.menuUI.logon.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class Valid5dxUser implements LogonValidationInt
{
  private String _passwordPath = null;
  private User5dx _user = null;

  public Valid5dxUser( String passwordPath )
  {
    _passwordPath = passwordPath;
  }

  public boolean validateUserLogon(String userName, String password) throws LogonException
  {
    boolean matchedUsername = false;

    if (userName.equalsIgnoreCase("s"))
      userName = "supervisor";  // make 's' equivalent to supervisor

    FileReader fileIn = null;
    try
    {
      fileIn = new FileReader(_passwordPath);
    }
    catch (FileNotFoundException e)
    {
      throw new PasswordFileException();
    }

    if (fileIn == null)
      return false;

    BufferedReader in = new BufferedReader( fileIn );
    String lineIn = null;
    do
    {
      try
      {
        lineIn = in.readLine();
      }
      catch (IOException e)
      {
        throw new PasswordFileException();
      }

      if (lineIn != null)
      {
        StringTokenizer st = new StringTokenizer(lineIn);
        String [] tokens = { null, null, null };
        int i = 0;
        while (st.hasMoreTokens() && i < tokens.length)
        {
          tokens[i++] = st.nextToken();
        }

        if (tokens[0] != null && tokens[0].equalsIgnoreCase(userName))
        {
          matchedUsername = true;
          if (tokens[1] != null && tokens[1].equalsIgnoreCase(HashUtil.hashName(password)))
          {
            MenuEnable access = null;
// DEBUG
            if (userName.equalsIgnoreCase("supervisor"))
              access = new SupervisorMenuEnable();
            else
              access = new OperatorMenuEnable();
// DEBUG
            _user = new User5dx( userName, access );
            return true;  // found a match
          }
        }
      }
    } while (lineIn != null);

    if (matchedUsername == false)
      throw new UsernameException();  // didn't find user name
    else
      throw new PasswordException();  // found user name but password didn't match
  }

  /**
   * Return the user name.
   */
   public User5dx getUser()
   {
      return _user;
   }

  /**
   * Set the user name.
   */
   public void setUser( User5dx user )
   {
      _user = user;
   }
}