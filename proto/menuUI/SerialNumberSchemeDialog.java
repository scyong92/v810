package proto.menuUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import com.agilent.guiUtil.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class SerialNumberSchemeDialog extends JDialog
{
  private int _status = JOptionPane.CANCEL_OPTION;

  public SerialNumberSchemeDialog()
  {
    this(null, "Individual Serial Number Files", true);
  }

  public SerialNumberSchemeDialog(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception
  {
    _okBtn.setText("OK");
    _okBtn.setFont(new java.awt.Font("Dialog", 1, 12));
    _okBtn.setMnemonic('O');
    _okBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _okBtn_actionPerformed(e);
      }
    });
    _cancelBtn.setText("Cancel");
    _cancelBtn.setMnemonic('C');
    _cancelBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _cancelBtn_actionPerformed(e);
      }
    });
    _gridLayout2.setHgap(5);
    _okCancelPanel.setLayout(_gridLayout2 );
    _charactersSpinBox.setEnabled(false);
    _okCancelPanel.add(_okBtn, null);
    _okCancelPanel.add(_cancelBtn, null);
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,5,5,5));
    _flowLayout4.setAlignment(FlowLayout.RIGHT);
    _buttonPanel.setLayout(_flowLayout4);
    _buttonPanel.add(_okCancelPanel, null);

    _allRB.setSelected(true);
    _allRB.setText("All");
    _charactesLbl2.setText("unique characters");
    _numCharRB.setText("First");
    _numCharRB.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        _numCharRB_stateChanged(e);
      }
    });
    _directoryLbl.setText("File directory:");
    _directoryTF.setText("\\SERIALNO");
    _directoryTF.setColumns(24);
    _deleteCkB.setSelected(true);
    _deleteCkB.setText("Delete serial number files");

    _numCharPanel.add(_numCharRB, null);
    _numCharPanel.add(_charactersSpinBox, null);
    _numCharPanel.add(_charactesLbl2, null);

    _buttonGroup1.add(_allRB);
    _buttonGroup1.add(_numCharRB);

    _charctersPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Number of characters to generate file name "));
    _flowLayout5.setAlignment(FlowLayout.LEFT);
    _charChoicePanel.setLayout(_flowLayout5);
    _charChoicePanel.add(_allRB, null);
    _charChoicePanel.add(_numCharPanel, null);

    _flowLayout1.setAlignment(FlowLayout.LEFT);
    _charctersPanel.setLayout(_flowLayout1);
    _charctersPanel.add(_charChoicePanel, null);

    _border1 = BorderFactory.createEmptyBorder(0,5,0,0);
    _directoryPanel.setBorder(_border1);
    _directoryPanel.setLayout(borderLayout1);
    _directoryPanel.add(_directoryLbl, BorderLayout.WEST);
    _directoryPanel.add(_directoryTF, BorderLayout.CENTER);

    _flowLayout3.setAlignment(FlowLayout.LEFT);
    _deletePanel.setLayout(_flowLayout3);
    _deletePanel.add(_deleteCkB, null);

    _centerPanel.setLayout(_verticalFlowLayout1);
    _centerPanel.add(_charctersPanel, null);
    _centerPanel.add(_directoryPanel, null);
    _centerPanel.add(_deletePanel, null);

    _panel1.setLayout(_borderLayout1);
    _panel1.add(_centerPanel, BorderLayout.CENTER);
    _panel1.add(_buttonPanel, BorderLayout.SOUTH);
    getContentPane().add(_panel1);
  }

  /**
   * Hide the dialog unless started from main method, then do System exit.
   */
  private void exitDialog()
  {
    this.hide();
  }

  public int showDialog()
  {
    this.show();
    return _status;
  }

  /**
   * Callback method for the OK button.
   */
  void _okBtn_actionPerformed(ActionEvent e)
  {
    _status = JOptionPane.OK_OPTION;
    exitDialog();
  }

  /**
   * Callback method for the Cancel button.
   */
  void _cancelBtn_actionPerformed(ActionEvent e)
  {
    exitDialog();
  }

  /**
   * State changed callback for the number of characters radio button.
   * Enable/disable the number spin box.
   */
  void _numCharRB_stateChanged(ChangeEvent e)
  {
    this._charactersSpinBox.setEnabled(this._numCharRB.isSelected());
  }

  private JPanel _panel1 = new JPanel();
  private BorderLayout _borderLayout1 = new BorderLayout();

  private JPanel _okCancelPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JButton _okBtn = new JButton();
  private JButton _cancelBtn = new JButton();
  private GridLayout _gridLayout2 = new GridLayout();
  private FlowLayout _flowLayout4 = new FlowLayout();

  private JPanel _centerPanel = new JPanel();
  private JPanel _deletePanel = new JPanel();
  private JPanel _directoryPanel = new JPanel();
  private JPanel _charctersPanel = new JPanel();
  private FlowLayout _flowLayout1 = new FlowLayout();
  private JLabel _directoryLbl = new JLabel();
  private JTextField _directoryTF = new JTextField();
  private JCheckBox _deleteCkB = new JCheckBox();
  private FlowLayout _flowLayout3 = new FlowLayout();
  private VerticalFlowLayout _verticalFlowLayout1 = new VerticalFlowLayout(VerticalFlowLayout.FILL);
  private JPanel _charChoicePanel = new JPanel();
  private JPanel _numCharPanel = new JPanel();
  private JRadioButton _allRB = new JRadioButton();
  private JLabel _charactesLbl2 = new JLabel();
  private IntegerSpinBox _charactersSpinBox = new IntegerSpinBox(40, 1, 1, 40);
  private JRadioButton _numCharRB = new JRadioButton();
  private FlowLayout _flowLayout5 = new FlowLayout();
  private BorderLayout borderLayout1 = new BorderLayout();
  private ButtonGroup _buttonGroup1 = new ButtonGroup();
  private Border _border1;
}