package proto.menuUI;
import com.agilent.util.*;
import java.util.Observer;
import java.util.Observable;

import proto.menuUI.autotest.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author
 * @version 1.0
 */

public interface InspectionObserverInt extends RemoteObserverInt
{
  //public void update(RemoteObservableInt o, Object arg);
  public void setRunning();
  public void setPaused();
  public void setStopped();
  public void newPanel( PanelEntry panel );
  public void newInspection();
  public void loadPanel(PanelEntry panel);
  public void panelLoaded();
  public void panelUnloaded();
  public void unloadPanel(PanelEntry panel);
  public void inspectionMessage(String message);
}