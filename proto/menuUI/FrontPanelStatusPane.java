package proto.menuUI;

import java.awt.*;
import javax.swing.*;
import com.agilent.util.*;
import com.agilent.guiUtil.VerticalFlowLayout;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class FrontPanelStatusPane extends JPanel// implements StatusUserInt
{

  public FrontPanelStatusPane()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    _userNameLbl.setText("<user>");
    _userLbl.setText("User:");
    flowLayout1.setAlignment(FlowLayout.LEFT);
    _userPanel.setLayout(flowLayout1);
    _userPanel.add(_userLbl, null);
    _userPanel.add(_userNameLbl, null);


    _statusCurrentLbl.setText("<status>");
    _statusLbl.setText("Status:");
    flowLayout3.setAlignment(FlowLayout.LEFT);
    _statusPanel.setLayout(flowLayout3);
    _statusPanel.add(_statusLbl, null);
    _statusPanel.add(_statusCurrentLbl, null);

    _centerPanel.setLayout(_verticalFlowLayout1);
    _centerPanel.add(_userPanel, null);
    _centerPanel.add(_statusPanel, null);

    this.setLayout(_borderLayout1);
    this.add(_centerPanel, BorderLayout.CENTER);
  }

  /**
   * Set the user name.
   */
  public void setUserName( String name )
  {
    if (name != null)
      this._userNameLbl.setText(name);
  }

  /**
   * Set the system status.
   */
  public void setStatus( LocalizedString status )
  {
    if (status == null)
      return;
    /** @todo Lookup the localized string for the message. */
    String stat = status.getMessageKey();
    this._statusCurrentLbl.setText(stat);
  }

// DEBUG
  public static void main(String[] args)
  {
    try
    {
      // Make sure the look and feel is set before any components are created.
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (ClassNotFoundException e) { /* do nothing */ }
    catch (InstantiationException e) { /* do nothing */ }
    catch (IllegalAccessException e) { /* do nothing */ }
    catch (UnsupportedLookAndFeelException e) { /* do nothing */ }

    JFrame frame = new JFrame("Front Panel Status");
    FrontPanelStatusPane fps = new FrontPanelStatusPane();
    frame.getContentPane().add(fps);
    frame.pack();
    // Set the Window 'X' button to exit the application.
    frame.addWindowListener( new java.awt.event.WindowAdapter()
    { public void windowClosing(java.awt.event.WindowEvent e) { System.exit(0); } });

    com.agilent.guiUtil.SwingUtils.setScreenPosition(frame, 0.5, 0.25);
    frame.show();
  }
  private boolean _startFromMain = false;
// DEBUG
  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _statusPanel = new JPanel();
  private JPanel _userPanel = new JPanel();
  private JLabel _userNameLbl = new JLabel();
  private JLabel _userLbl = new JLabel();
  private JLabel _statusCurrentLbl = new JLabel();
  private JLabel _statusLbl = new JLabel();
  private VerticalFlowLayout _verticalFlowLayout1 = new VerticalFlowLayout(VerticalFlowLayout.FILL);
  private FlowLayout flowLayout1 = new FlowLayout();
  private FlowLayout flowLayout3 = new FlowLayout();
}