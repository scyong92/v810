package proto.menuUI;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.*;
import javax.swing.border.TitledBorder;
import com.agilent.guiUtil.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class BarCodeOptionPane extends JPanel
{
  public static final String MS600_INTERFACE_MODEL = "MS 600";
  public static final String DL4300_INTERFACE_MODEL = "DL 4300";
  public static final int TRIGGER_SCAN_MODE = 1;
  public static final int CONTINUOUS_SCAN_MODE = 2;
  public static final int STAND_ALONE_CONFIGURATION = 3;
  public static final int MULTI_DROP_CONFIGURATION = 4;

  public BarCodeOptionPane()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    _enableBarCodeCkB.setText("Enable bar code reader for serial numbers.");
    _enableBarCodeCkB.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        _enableBarCodeCkB_stateChanged(e);
      }
    });
    _modelLbl.setEnabled(false);
    _modelLbl.setText("Interface model:");
    _continuousRB.setEnabled(false);
    _continuousRB.setText("Continuous");
    _triggerRB.setEnabled(false);
    _triggerRB.setSelected(true);
    _triggerRB.setText("Trigger");
    _standAloneRB.setEnabled(false);
    _standAloneRB.setSelected(true);
    _standAloneRB.setText("Stand-alone");
    _multidropRB.setEnabled(false);
    _multidropRB.setText("Multidrop with");
    _multidropRB.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        _multidropRB_stateChanged(e);
      }
    });
    _numHeadsTF.setEnabled(false);
    _headsLbl.setEnabled(false);
    _headsLbl.setText("heads");
    _modelCB.setEnabled(false);
    _modelCB.addItem(MS600_INTERFACE_MODEL);
    _modelCB.addItem(DL4300_INTERFACE_MODEL);
    _alignBtn.setEnabled(false);
    _alignBtn.setText("Align");

    _flowLayout1.setAlignment(FlowLayout.LEFT);
    _interfaceModelPanel.setLayout(_flowLayout1);
    _interfaceModelPanel.add(_modelLbl, null);
    _interfaceModelPanel.add(_modelCB, null);

    _buttonGroup1.add(_triggerRB);
    _buttonGroup1.add(_continuousRB);
    _scanModePanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Scan Mode "));
    _flowLayout2.setAlignment(FlowLayout.LEFT);
    _scanModePanel.setLayout(_flowLayout2);
    _scanModePanel.add(_triggerRB, null);
    _scanModePanel.add(_continuousRB, null);

    _buttonGroup2.add(_standAloneRB);
    _buttonGroup2.add(_multidropRB);
    _multidropPanel.add(_multidropRB, null);
    _multidropPanel.add(_numHeadsTF, null);
    _multidropPanel.add(_headsLbl, null);
    _configPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Configuration "));
    _flowLayout3.setAlignment(FlowLayout.LEFT);
    _configPanel.setLayout(_flowLayout3);
    _configPanel.add(_standAloneRB, null);
    _configPanel.add(_multidropPanel, null);

    _flowLayout4.setAlignment(FlowLayout.LEFT);
    _alignPanel.setLayout(_flowLayout4);
    _alignPanel.add(_alignBtn, null);

    _subPanel.setBorder(BorderFactory.createEmptyBorder(0,15,0,0));
    _subPanel.setLayout(_verticalFlowLayout2);
    _subPanel.add(_interfaceModelPanel, null);
    _subPanel.add(_scanModePanel, null);
    _subPanel.add(_configPanel, null);
    _subPanel.add(_alignPanel, null);

    _centerPanel.setBorder(BorderFactory.createEmptyBorder(0,5,0,5));
    _centerPanel.setLayout(_verticalFlowLayout1);
    _centerPanel.add(_enableBarCodeCkB, null);
    _centerPanel.add(_subPanel, null);

    this.setBorder(BorderFactory.createEmptyBorder(10,0,5,0));
    this.setLayout(_borderLayout1);
    this.add(_centerPanel, BorderLayout.CENTER);
  }

  public boolean getBarCodeEnabled()
  {
    return this._enableBarCodeCkB.isSelected();
  }
  public String getInterfaceModel()
  {
    return (String)this._modelCB.getSelectedItem();
  }
  public int getScanMode()
  {
    if (this._continuousRB.isSelected())
      return CONTINUOUS_SCAN_MODE;
    else
      return TRIGGER_SCAN_MODE;
  }
  public int getConfiguration()
  {
    if (this._multidropRB.isSelected())
      return MULTI_DROP_CONFIGURATION;
    else
      return STAND_ALONE_CONFIGURATION;
  }
  public int getNumberOfHeads()
  {
    if (this._multidropRB.isSelected())
      return this._numHeadsTF.getValue();
    else
      return 0;
  }
  public JButton getAlignButton()
  {
    return this._alignBtn;
  }

  public void setBarCodeEnabled(boolean b)
  {
    this._enableBarCodeCkB.setSelected(b);
  }
  public void setInterfaceModel(String model)
  {
    if (model == DL4300_INTERFACE_MODEL)
      this._modelCB.setSelectedIndex(1);
    else  // MS600_INTERFACE_MODEL
      this._modelCB.setSelectedIndex(0);
  }
  public void setScanMode(int mode)
  {
    if (mode == CONTINUOUS_SCAN_MODE)
      this._continuousRB.setSelected(true);
    else
      this._triggerRB.setSelected(true);
  }
  public void setConfiguration(int config)
  {
    if (config == MULTI_DROP_CONFIGURATION)
      this._multidropRB.setSelected(true);
    else
      this._standAloneRB.setSelected(true);
  }
  public void setNumberOfHeads(int heads)
  {
    this._numHeadsTF.setValue(heads);
  }

  // Callback methods
  void _enableBarCodeCkB_stateChanged(ChangeEvent e)
  {
    boolean enabled = this._enableBarCodeCkB.isSelected();
    _modelLbl.setEnabled(enabled);
    _continuousRB.setEnabled(enabled);
    _triggerRB.setEnabled(enabled);
    _standAloneRB.setEnabled(enabled);
    _multidropRB.setEnabled(enabled);
    if (_multidropRB.isSelected())
      _numHeadsTF.setEnabled(enabled);
    _headsLbl.setEnabled(enabled);
    _alignBtn.setEnabled(enabled);
    _modelCB.setEnabled(enabled);
  }

  void _multidropRB_stateChanged(ChangeEvent e)
  {
    _numHeadsTF.setEnabled(this._multidropRB.isSelected());
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("BarCodeOptionPane [");
    buf.append(" Enabled:" + _enableBarCodeCkB.isSelected());
    buf.append(", Model:" + getInterfaceModel());
    buf.append(", Mode:");
    switch (getScanMode())
    {
      case TRIGGER_SCAN_MODE:
        buf.append("Trigger");
        break;
      case CONTINUOUS_SCAN_MODE:
        buf.append("Continuous");
        break;
    }
    buf.append(", Configuration:");
    switch (getConfiguration())
    {
      case STAND_ALONE_CONFIGURATION:
        buf.append("Stand-alone");
        break;
      case MULTI_DROP_CONFIGURATION:
        buf.append("Multi-drop");
        break;
    }
    buf.append(", Heads:" + getNumberOfHeads());
    buf.append(" ]");
    return buf.toString();
  }

  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JCheckBox _enableBarCodeCkB = new JCheckBox();
  private JPanel _subPanel = new JPanel();
  private JPanel _configPanel = new JPanel();
  private JPanel _scanModePanel = new JPanel();
  private JPanel _interfaceModelPanel = new JPanel();
  private JComboBox _modelCB = new JComboBox();
  private JLabel _modelLbl = new JLabel();
  private JRadioButton _continuousRB = new JRadioButton();
  private JRadioButton _triggerRB = new JRadioButton();
  private JRadioButton _standAloneRB = new JRadioButton();
  private JPanel _multidropPanel = new JPanel();
  private JRadioButton _multidropRB = new JRadioButton();
  private IntegerSpinBox _numHeadsTF = new IntegerSpinBox(1, 1, 1, 999);
  private JLabel _headsLbl = new JLabel();
  private VerticalFlowLayout _verticalFlowLayout1 = new VerticalFlowLayout();
  private VerticalFlowLayout _verticalFlowLayout2 = new VerticalFlowLayout(VerticalFlowLayout.FILL);
  private JPanel _alignPanel = new JPanel();
  private JButton _alignBtn = new JButton();
  private ButtonGroup _buttonGroup1 = new ButtonGroup();
  private ButtonGroup _buttonGroup2 = new ButtonGroup();
  private FlowLayout _flowLayout1 = new FlowLayout();
  private FlowLayout _flowLayout2 = new FlowLayout();
  private FlowLayout _flowLayout3 = new FlowLayout();
  private FlowLayout _flowLayout4 = new FlowLayout();
}