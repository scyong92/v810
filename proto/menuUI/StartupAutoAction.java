package proto.menuUI;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class StartupAutoAction extends MenuAction
{
  private static final ImageIcon _autoStartIcon = new ImageIcon(Menu5DX.class.getResource("startup.gif"));

  public StartupAutoAction(Menu5DX parent)
  {
    super("Startup", _autoStartIcon, "Run automatic startup", 'A', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._startupMI_actionPerformed(e);
  }
}