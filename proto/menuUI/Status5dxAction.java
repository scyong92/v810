package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class Status5dxAction extends MenuAction
{
  public Status5dxAction(Menu5DX parent)
  {
    super("5DX Status...", null, "Show the 5DX status", 'S', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._status5DXMI_actionPerformed(e);
  }
}