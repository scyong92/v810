package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class DeleteUserAction extends MenuAction
{
  public DeleteUserAction(Menu5DX parent)
  {
    super("Delete User", null, "Delete a user logon", 'D', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    System.out.println(this.getTooltip());
// DEBUG
    //this.getParent()._deleteUserMI_actionPerformed(e);
  }
}