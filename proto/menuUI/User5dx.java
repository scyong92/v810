package proto.menuUI;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class User5dx
{
  private String _username = null;
  private MenuEnable _menuEnable = null;

  public User5dx( String username, MenuEnable menuEnable )
  {
    // Assert(menuEnable)
    _username = ((username == null) ? "none" : username.toUpperCase());
    _menuEnable = menuEnable;
  }

  /**
   * Return the user name.
   */
  public String getUsername()
  {
    return _username;
  }

  /**
   * Return the menu enable.
   */
  public MenuEnable getMenuEnable()
  {
    return _menuEnable;
  }
}