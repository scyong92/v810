package proto.menuUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.text.DecimalFormat;
import javax.swing.border.*;
import com.agilent.guiUtil.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class Status5dxDialog extends JDialog
{
  private static java.awt.Font _font = new java.awt.Font("SansSerif", 1, 12);

  public Status5dxDialog()
  {
    this(null, "5DX Status", false);
  }

  public Status5dxDialog(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    _okBtn.setText("OK");
    _okBtn.setFont(new java.awt.Font("Dialog", 1, 12));
    _okBtn.setMnemonic('O');
    _okBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _okBtn_actionPerformed(e);
      }
    });
    _gridLayout2.setHgap(5);
    _okCancelPanel.setLayout(_gridLayout2 );
    _centerPanel.setBorder(BorderFactory.createEmptyBorder(10,5,5,5));
    _okCancelPanel.add(_okBtn, null);
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,5,5,5));
    _flowLayout1.setAlignment(FlowLayout.RIGHT);
    _buttonPanel.setLayout(_flowLayout1);
    _buttonPanel.add(_okCancelPanel, null);

    // System panel
    _systemSeriesLbl.setText("5DX:");
    _systemSeriesTF.setFont(_font);
    _systemSeriesTF.setText("Series 3");
    _systemSeriesPanel.add(_systemSeriesLbl, null);
    _systemSeriesPanel.add(_systemSeriesTF, null);
    _systemSNTF.setFont(_font);
    _systemSNLbl.setText("Serial Number:");
    _systemSNTF.setText("122");
    _systemSNPanel.add(_systemSNLbl, null);
    _systemSNPanel.add(_systemSNTF, null);
    _systemTopPanel.add(_systemSeriesPanel, null);
    _systemTopPanel.add(_systemSNPanel, null);
    _systemPanel.setLayout(_flowLayout2);
    _systemPanel.add(_systemTopPanel, null);

    // X-ray Source panel
    _xraySourceStatusTF.setBackground(Color.green);
    _xraySourceStatusTF.setText("READY");
    _xraySourceStatusTF.setBorder(null);
    _xraySourceStatusTF.setEditable(false);
    _xraySourceStatusTF.setColumns(10);
    _xraySourceStatusTF.setHorizontalAlignment(SwingConstants.CENTER);
    _xraySourceStatusPanel.add(_xraySourceStatusTF, null);
    _xraySourceVoltageTF.setFont(_font);
    _xraySourceVoltageLbl.setText("Voltage:");
    _xraySourceVoltageTF.setBackground(Color.lightGray);
    _xraySourceVoltageTF.setBorder(null);
    _xraySourceVoltageTF.setEditable(false);
    _xraySourceVoltageTF.setValue(160.49);
    _xraySourceVoltageUnitLbl.setFont(_font);
    _xraySourceVoltageUnitLbl.setText("KV");
    _xraySourceVoltagePanel.add(_xraySourceVoltageLbl, null);
    _xraySourceVoltagePanel.add(_xraySourceVoltageTF, null);
    _xraySourceVoltagePanel.add(_xraySourceVoltageUnitLbl, null);
    _xraySourceCurrentLbl.setText("Current:");
    _xraySourceCurrentTF.setFont(_font);
    _xraySourceCurrentTF.setBackground(Color.lightGray);
    _xraySourceCurrentTF.setBorder(null);
    _xraySourceCurrentTF.setEditable(false);
    _xraySourceCurrentTF.setValue(100.49);
    _xraySourceCurrentUnitLbl.setFont(_font);
    _xraySourceCurrentUnitLbl.setText("uA");
    _xraySourceCurrentPanel.add(_xraySourceCurrentLbl, null);
    _xraySourceCurrentPanel.add(_xraySourceCurrentTF, null);
    _xraySourceCurrentPanel.add(_xraySourceCurrentUnitLbl, null);
    _xraySourceTopPanel.add(_xraySourceStatusPanel, null);
    _xraySourceTopPanel.add(_xraySourceVoltagePanel, null);
    _xraySourceTopPanel.add(_xraySourceCurrentPanel, null);
    _flowLayout4.setAlignment(FlowLayout.LEFT);
    _xraySourcePanel.setLayout(_flowLayout4);
    _xraySourcePanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," X-ray Source"));
    _xraySourcePanel.add(_xraySourceTopPanel, null);

    // X-ray Scan panel
    _xrayScanStatusTF.setBackground(Color.green);
    _xrayScanStatusTF.setBorder(null);
    _xrayScanStatusTF.setEditable(false);
    _xrayScanStatusTF.setText("READY");
    _xrayScanStatusTF.setColumns(10);
    _xrayScanStatusTF.setHorizontalAlignment(SwingConstants.CENTER);
    _xrayScanStatusPanel.add(_xrayScanStatusTF, null);
    _xrayScanCurrentBankLbl.setText("Current Bank:");
    _xrayScanCurrentBankTF.setFont(_font);
    _xrayScanCurrentBankTF.setBackground(Color.lightGray);
    _xrayScanCurrentBankTF.setBorder(null);
    _xrayScanCurrentBankTF.setEditable(false);
    _xrayScanCurrentBankTF.setText("Bank 1");
    _xrayScanCurrentPanel.add(_xrayScanCurrentBankLbl, null);
    _xrayScanCurrentPanel.add(_xrayScanCurrentBankTF, null);
    _flowLayout5.setAlignment(FlowLayout.LEFT);
    _xrayScanTopPanel.setLayout(_flowLayout5);
    _xrayScanTopPanel.add(_xrayScanStatusPanel, null);
    _xrayScanTopPanel.add(_xrayScanCurrentPanel, null);

    _xrayScanBank0Lbl.setText("Bank 0:");
    _xrayScanBank0TF.setFont(_font);
    _xrayScanBank0TF.setColumns(5);
    _xrayScanBank0TF.setBackground(Color.lightGray);
    _xrayScanBank0TF.setBorder(null);
    _xrayScanBank0TF.setEditable(false);
    _xrayScanBank0TF.setText("DUMP");
    _xrayScanBank0Panel.add(_xrayScanBank0Lbl, null);
    _xrayScanBank0Panel.add(_xrayScanBank0TF, null);
    _xrayScanBank1Lbl.setText("Bank 1:");
    _xrayScanBank1TF.setFont(_font);
    _xrayScanBank1TF.setColumns(5);
    _xrayScanBank1TF.setBackground(Color.lightGray);
    _xrayScanBank1TF.setBorder(null);
    _xrayScanBank1TF.setEditable(false);
    _xrayScanBank1TF.setText("400 mils");
    _xrayScanBank1Panel.add(_xrayScanBank1Lbl, null);
    _xrayScanBank1Panel.add(_xrayScanBank1TF, null);
    _xrayScanBank2Lbl.setText("Bank 2:");
    _xrayScanBank2TF.setFont(_font);
    _xrayScanBank2TF.setColumns(5);
    _xrayScanBank2TF.setBackground(Color.lightGray);
    _xrayScanBank2TF.setBorder(null);
    _xrayScanBank2TF.setEditable(false);
    _xrayScanBank2TF.setText("200 mils");
    _xrayScanBank2Panel.add(_xrayScanBank2Lbl, null);
    _xrayScanBank2Panel.add(_xrayScanBank2TF, null);
    _xrayScanBank3Lbl.setText("Bank 3:");
    _xrayScanBank3TF.setFont(_font);
    _xrayScanBank3TF.setColumns(5);
    _xrayScanBank3TF.setBackground(Color.lightGray);
    _xrayScanBank3TF.setBorder(null);
    _xrayScanBank3TF.setEditable(false);
    _xrayScanBank3TF.setText("800 mils");
    _xrayScanBank3Panel.add(_xrayScanBank3Lbl, null);
    _xrayScanBank3Panel.add(_xrayScanBank3TF, null);
    _border1 = BorderFactory.createEmptyBorder(0,30,0,0);
    _xrayScanBottomPanel.setBorder(_border1);
    _gridLayout1.setRows(2);
    _gridLayout1.setColumns(2);
    _xrayScanBottomPanel.setLayout(_gridLayout1);
    _xrayScanBottomPanel.add(_xrayScanBank0Panel, null);
    _xrayScanBottomPanel.add(_xrayScanBank2Panel, null);
    _xrayScanBottomPanel.add(_xrayScanBank1Panel, null);
    _xrayScanBottomPanel.add(_xrayScanBank3Panel, null);

    _xrayScanPanel.setLayout(_verticalFlowLayout2);
    _xrayScanPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," X-ray Scan "));
    _xrayScanPanel.add(_xrayScanTopPanel, null);
    _xrayScanPanel.add(_xrayScanBottomPanel, null);

    // Scintillator panel
    _scintillatorStatusTF.setBackground(Color.green);
    _scintillatorStatusTF.setBorder(null);
    _scintillatorStatusTF.setEditable(false);
    _scintillatorStatusTF.setText("READY");
    _scintillatorStatusTF.setColumns(10);
    _scintillatorStatusTF.setHorizontalAlignment(SwingConstants.CENTER);
    _scintillatorStatusPanel.add(_scintillatorStatusTF, null);
    _scintillatorSpeedLbl.setText("Speed:");
    _scintillatorSpeedTF.setFont(_font);
    _scintillatorSpeedTF.setBackground(Color.lightGray);
    _scintillatorSpeedTF.setBorder(null);
    _scintillatorSpeedTF.setEditable(false);
    _scintillatorSpeedTF.setValue(748.00);
    _scintillatorSpeedUnitLbl.setFont(_font);
    _scintillatorSpeedUnitLbl.setText("rpm");
    _scintillatorSpeedPanel.add(_scintillatorSpeedLbl, null);
    _scintillatorSpeedPanel.add(_scintillatorSpeedTF, null);
    _scintillatorSpeedPanel.add(_scintillatorSpeedUnitLbl, null);
    _scintillatorTopPanel.add(_scintillatorStatusPanel, null);
    _scintillatorTopPanel.add(_scintillatorSpeedPanel, null);
    _flowLayout6.setAlignment(FlowLayout.LEFT);
    _scintillatorPanel.setLayout(_flowLayout6);
    _scintillatorPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Scintillator "));
    _scintillatorPanel.add(_scintillatorTopPanel, null);

    // Panel Handling panel
    _panelHandlingStatusTF.setBackground(Color.green);
    _panelHandlingStatusTF.setBorder(null);
    _panelHandlingStatusTF.setEditable(false);
    _panelHandlingStatusTF.setText("READY");
    _panelHandlingStatusTF.setColumns(10);
    _panelHandlingStatusTF.setHorizontalAlignment(SwingConstants.CENTER);
    _panelHandlingStatusPanel.add(_panelHandlingStatusTF, null);
    _panelHandlingTopPanel.add(_panelHandlingStatusPanel, null);
    _flowLayout7.setAlignment(FlowLayout.LEFT);
    _panelHandlingPanel.setLayout(_flowLayout7);
    _panelHandlingPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Panel Handling "));
    _panelHandlingPanel.add(_panelHandlingTopPanel, null);

    // XYZ Stage panel
    _stageStatusTF.setBackground(Color.green);
    _stageStatusTF.setBorder(null);
    _stageStatusTF.setEditable(false);
    _stageStatusTF.setText("READY");
    _stageStatusTF.setColumns(10);
    _stageStatusTF.setHorizontalAlignment(SwingConstants.CENTER);
    _stageStatusPanel.add(_stageStatusTF, null);
    _stageTopPanel.add(_stageStatusPanel, null);
    _flowLayout8.setAlignment(FlowLayout.LEFT);
    _stagePanel.setLayout(_flowLayout8);
    _stagePanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," XYZ Stage "));
    _stagePanel.add(_stageTopPanel, null);

    // IAS panel
    _iasStatusTF.setBackground(Color.green);
    _iasStatusTF.setText("READY");
    _iasStatusTF.setBorder(null);
    _iasStatusTF.setEditable(false);
    _iasStatusTF.setColumns(10);
    _iasStatusTF.setHorizontalAlignment(SwingConstants.CENTER);
    _iasStatusPanel.add(_iasStatusTF, null);
    _iasTopPanel.add(_iasStatusPanel, null);
    _flowLayout9.setAlignment(FlowLayout.LEFT);
    _iasPanel.setLayout(_flowLayout9);
    _iasPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Image Analysis "));
    _iasPanel.add(_iasTopPanel, null);


    _centerPanel.setLayout(_verticalFlowLayout1);
    _centerPanel.add(_systemPanel, null);
    _centerPanel.add(_xraySourcePanel, null);
    _centerPanel.add(_xrayScanPanel, null);
    _centerPanel.add(_scintillatorPanel, null);
    _centerPanel.add(_panelHandlingPanel, null);
    _centerPanel.add(_stagePanel, null);
    _centerPanel.add(_iasPanel, null);

    _rootPanel.setLayout(_borderLayout1);
    _rootPanel.add(_buttonPanel, BorderLayout.SOUTH);
    _rootPanel.add(_centerPanel, BorderLayout.CENTER);
    getContentPane().add(_rootPanel);
  }

  /**
   * Set the 5dx series.
   */
  public void setSeries( String series )
  {
    _systemSeriesTF.setText(series);
  }

  /**
   * Set the 5dx serial number.
   */
  public void setSerialNumber( String serialNumber )
  {
    _systemSNTF.setText(serialNumber);
  }

  /**
   * Set a status text field the appropriate color and message depending upon the ready flag.
   */
  private void setStatus( boolean ready, JTextField tf )
  {
    String labelMsg = ((ready) ? "READY" : "NOT READY");
    Color bg = ((ready) ? Color.green : Color.yellow);
    tf.setBackground(bg);
    tf.setText(labelMsg);
  }

  /**
   * Set the x-ray source status.
   */
  public void setXraySourceStatus( boolean ready )
  {
    setStatus(ready, _xraySourceStatusTF);
  }

  /**
   * Set the x-ray source voltage.
   */
  public void setXraySourceVoltage( double value, boolean valid )
  {
    Color fg = ((valid) ? Color.black : Color.yellow);
    _xraySourceVoltageTF.setValue(value);
    _xraySourceVoltageTF.setForeground(fg);
  }

  /**
   * Set the x-ray source current.
   */
  public void setXraySourceCurrent( double value, boolean valid )
  {
    Color fg = ((valid) ? Color.black : Color.yellow);
    _xraySourceCurrentTF.setValue(value);
    _xraySourceCurrentTF.setForeground(fg);
  }

  /**
   * Set the x-ray scan status.
   */
  public void setXrayScanStatus( boolean ready )
  {
    setStatus(ready, _xrayScanStatusTF);
  }

  /**
   * Set the x-ray scan current bank.
   */
  public void setXrayScanCurrentBank( String bank, boolean valid )
  {
    Color fg = ((valid) ? Color.black : Color.yellow);
    _xrayScanCurrentBankTF.setText(bank);
    _xrayScanCurrentBankTF.setForeground(fg);
  }

  /**
   * Set the x-ray scan bank 0 FOV.
   */
  public void setXrayScanBank0FOV( String fov, boolean valid )
  {
    Color fg = ((valid) ? Color.black : Color.yellow);
    _xrayScanBank0TF.setText(fov);
    _xrayScanBank0TF.setForeground(fg);
  }

  /**
   * Set the x-ray scan bank 1 FOV.
   */
  public void setXrayScanBank1FOV( String fov, boolean valid )
  {
    Color fg = ((valid) ? Color.black : Color.yellow);
    _xrayScanBank1TF.setText(fov);
    _xrayScanBank1TF.setForeground(fg);
  }

  /**
   * Set the x-ray scan bank 2 FOV.
   */
  public void setXrayScanBank2FOV( String fov, boolean valid )
  {
    Color fg = ((valid) ? Color.black : Color.yellow);
    _xrayScanBank2TF.setText(fov);
    _xrayScanBank2TF.setForeground(fg);
  }

  /**
   * Set the x-ray scan bank 3 FOV.
   */
  public void setXrayScanBank3FOV( String fov, boolean valid )
  {
    Color fg = ((valid) ? Color.black : Color.yellow);
    _xrayScanBank3TF.setText(fov);
    _xrayScanBank3TF.setForeground(fg);
  }

  /**
   * Set the scintillator status.
   */
  public void setScintallatorStatus( boolean ready )
  {
    setStatus(ready, _scintillatorStatusTF);
  }

  /**
   * Set the scintillator speed.
   */
  public void setScintallatorSpeed( double rpm, boolean valid )
  {
    Color fg = ((valid) ? Color.black : Color.yellow);
    _scintillatorSpeedTF.setValue(rpm);
    _scintillatorSpeedTF.setForeground(fg);
  }

  /**
   * Set the panel handling status.
   */
  public void setPanelHandlingStatus( boolean ready )
  {
    setStatus(ready, _panelHandlingStatusTF);
  }

  /**
   * Set the xyz stage status.
   */
  public void setXYZStageStatus( boolean ready )
  {
    setStatus(ready, _stageStatusTF);
  }

  /**
   * Set the ias status.
   */
  public void setIASStatus( boolean ready )
  {
    setStatus(ready, _iasStatusTF);
  }

  /**
   * Callback method for the OK button.
   */
  void _okBtn_actionPerformed(ActionEvent e)
  {
    this.hide();
    if (_startFromMain)
      System.exit(0);
  }

// DEBUG
  public static void main(String[] args)
  {
    try
    {
      // Make sure the look and feel is set before any components are created.
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (ClassNotFoundException e) { /* do nothing */ }
    catch (InstantiationException e) { /* do nothing */ }
    catch (IllegalAccessException e) { /* do nothing */ }
    catch (UnsupportedLookAndFeelException e) { /* do nothing */ }

    Status5dxDialog dlg = new Status5dxDialog();
    dlg._startFromMain = true;
    com.agilent.guiUtil.SwingUtils.setScreenPosition(dlg,0.5,0.25);
//    dlg.setXraySourceVoltage(101.01, false);
//    dlg.setXraySourceStatus(false);
    dlg.setVisible(true);
  }
  private boolean _startFromMain = false;
// DEBUG

  private JPanel _rootPanel = new JPanel();
  private BorderLayout _borderLayout1 = new BorderLayout();

  private FlowLayout _flowLayout1 = new FlowLayout();
  private JPanel _buttonPanel = new JPanel();
  private GridLayout _gridLayout2 = new GridLayout();
  private JPanel _okCancelPanel = new JPanel();
  private JButton _okBtn = new JButton();

  private JPanel _centerPanel = new JPanel();
  private JPanel _systemPanel = new JPanel();
  private JPanel _systemSNPanel = new JPanel();
  private JPanel _systemSeriesPanel = new JPanel();
  private JTextField _systemStatusTF = new JTextField();
  private JLabel _systemSeriesTF = new JLabel();
  private JLabel _systemSeriesLbl = new JLabel();
  private JLabel _systemSNTF = new JLabel();
  private JLabel _systemSNLbl = new JLabel();
  private FlowLayout _flowLayout2 = new FlowLayout();
  private VerticalFlowLayout _verticalFlowLayout1 = new VerticalFlowLayout(VerticalFlowLayout.FILL);
  private VerticalFlowLayout _verticalFlowLayout2 = new VerticalFlowLayout(VerticalFlowLayout.FILL);
  private JPanel _xraySourcePanel = new JPanel();
  private JPanel _xrayScanPanel = new JPanel();
  private JPanel _xraySourceCurrentPanel = new JPanel();
  private JPanel _xraySourceVoltagePanel = new JPanel();
  private JPanel _xraySourceStatusPanel = new JPanel();
  private JTextField _xraySourceStatusTF = new JTextField();
  private JLabel _xraySourceVoltageUnitLbl = new JLabel();
  private NumericTextField _xraySourceVoltageTF = new NumericTextField();
  private JLabel _xraySourceVoltageLbl = new JLabel();
  private JLabel _xraySourceCurrentUnitLbl = new JLabel();
  private NumericTextField _xraySourceCurrentTF = new NumericTextField();
  private JLabel _xraySourceCurrentLbl = new JLabel();
  private FlowLayout _flowLayout4 = new FlowLayout();
  private JPanel _xrayScanBottomPanel = new JPanel();
  private JPanel _xrayScanTopPanel = new JPanel();
  private FlowLayout _flowLayout5 = new FlowLayout();
  private JPanel _xrayScanCurrentPanel = new JPanel();
  private JPanel _xrayScanStatusPanel = new JPanel();
  private JTextField _xrayScanStatusTF = new JTextField();
  private JTextField _xrayScanCurrentBankTF = new JTextField();
  private JLabel _xrayScanCurrentBankLbl = new JLabel();
  private JPanel _systemTopPanel = new JPanel();
  private JPanel _xraySourceTopPanel = new JPanel();
  private JPanel _xrayScanBank3Panel = new JPanel();
  private JPanel _xrayScanBank2Panel = new JPanel();
  private JPanel _xrayScanBank1Panel = new JPanel();
  private JPanel _xrayScanBank0Panel = new JPanel();
  private JLabel _xrayScanBank0Lbl = new JLabel();
  private JTextField _xrayScanBank0TF = new JTextField();
  private JTextField _xrayScanBank1TF = new JTextField();
  private JLabel _xrayScanBank1Lbl = new JLabel();
  private JTextField _xrayScanBank2TF = new JTextField();
  private JLabel _xrayScanBank2Lbl = new JLabel();
  private JTextField _xrayScanBank3TF = new JTextField();
  private JLabel _xrayScanBank3Lbl = new JLabel();
  private GridLayout _gridLayout1 = new GridLayout();
  private Border _border1;
  private JPanel _iasPanel = new JPanel();
  private JPanel _stagePanel = new JPanel();
  private JPanel _panelHandlingPanel = new JPanel();
  private JPanel _scintillatorPanel = new JPanel();
  private JPanel _scintillatorTopPanel = new JPanel();
  private JPanel _panelHandlingTopPanel = new JPanel();
  private JPanel _stageTopPanel = new JPanel();
  private JPanel _iasTopPanel = new JPanel();
  private JPanel _scintillatorSpeedPanel = new JPanel();
  private JPanel _scintillatorStatusPanel = new JPanel();
  private JTextField _scintillatorStatusTF = new JTextField();
  private NumericTextField _scintillatorSpeedTF = new NumericTextField();
  private JLabel _scintillatorSpeedUnitLbl = new JLabel();
  private JLabel _scintillatorSpeedLbl = new JLabel();
  private FlowLayout _flowLayout6 = new FlowLayout();
  private JPanel _panelHandlingStatusPanel = new JPanel();
  private JPanel _stageStatusPanel = new JPanel();
  private JPanel _iasStatusPanel = new JPanel();
  private JTextField _panelHandlingStatusTF = new JTextField();
  private FlowLayout _flowLayout7 = new FlowLayout();
  private JTextField _stageStatusTF = new JTextField();
  private FlowLayout _flowLayout8 = new FlowLayout();
  private JTextField _iasStatusTF = new JTextField();
  private FlowLayout _flowLayout9 = new FlowLayout();
}