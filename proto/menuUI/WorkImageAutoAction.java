package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class WorkImageAutoAction extends MenuAction
{
  public WorkImageAutoAction(Menu5DX parent)
  {
    super("Automatic Test", null, "Save images for automatic test", 'A', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._workImageAutoMI_actionPerformed(e);
  }
}