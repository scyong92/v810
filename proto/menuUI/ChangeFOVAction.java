package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ChangeFOVAction extends MenuAction
{
  public ChangeFOVAction(Menu5DX parent)
  {
    super("Change FOV", null, "Change the field of view", 'F', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    System.out.println(this.getTooltip());
// DEBUG
  }
}