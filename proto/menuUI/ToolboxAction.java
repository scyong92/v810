package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class ToolboxAction extends MenuAction
{
  public ToolboxAction(Menu5DX parent)
  {
    super("Toolbox", null, "Confirmation and Diagnostics", 'T', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._toolboxMI_actionPerformed(e);
  }
}