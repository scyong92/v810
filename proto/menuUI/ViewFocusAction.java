package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class ViewFocusAction extends MenuAction
{
  public ViewFocusAction(Menu5DX parent)
  {
    super("Adjust View Focus", null, "Adjust view focus", 'F', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._viewFocusMI_actionPerformed(e);
  }
}