package proto.menuUI;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;
import com.agilent.guiUtil.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class InspectionOptionPane extends JPanel
{
  public static final int MAP_ALIGNMENT_RECOVERY = 2;
  public static final int TERMINATE_ALIGNMENT_RECOVERY = 3;
  public static final int MANUAL_ALIGNMENT_RECOVERY = 4;

  public static final int NO_LOG_FILE = 5;
  public static final int APPEND_LOG_FILE = 6;
  public static final int REWRITE_LOG_FILE = 7;

  public InspectionOptionPane()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    _autoReviewCkB.setText("Run Review Defects if more than");
    _autoReviewCkB.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        _autoReviewCkB_stateChanged(e);
      }
    });
    _label1.setText("defects are found");
    _manualRecoveryRB.setSelected(true);
    _manualRecoveryRB.setText("Prompt for manual alignment");
    _terminateRecoveryRB.setText("Terminate inspection");
    _mapRecoveryRB.setText("Attempt recovery by improving image focus");
    _defectNumSpinBox.setEnabled(false);
    _appendRB.setText("Save all inspection messages");
    _rewriteRB.setText("Save current inspection messages");
    _noLogRB.setText("Do not save messages");
    _noLogRB.setSelected(true);
    _buttonGroup1.add(_mapRecoveryRB);
    _buttonGroup1.add(_terminateRecoveryRB);
    _buttonGroup1.add(_manualRecoveryRB);
    _buttonGroup2.add(_appendRB);
    _buttonGroup2.add(_rewriteRB);
    _buttonGroup2.add(_noLogRB);

    _flowLayout1.setAlignment(FlowLayout.LEFT);
    _autoReviewPanel.setLayout(_flowLayout1);
    _autoReviewPanel.add(_autoReviewCkB, null);
    _autoReviewPanel.add(_defectNumSpinBox, null);
    _autoReviewPanel.add(_label1, null);

    _alignRecoveryPanel.setLayout(_verticalFlowLayout1);
    _alignRecoveryPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Alignment Recovery "));
    _alignRecoveryPanel.add(_mapRecoveryRB, null);
    _alignRecoveryPanel.add(_manualRecoveryRB, null);
    _alignRecoveryPanel.add(_terminateRecoveryRB, null);

    _logPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Message Log File "));
    _logPanel.setLayout(_verticalFlowLayout3);
    _logPanel.add(_noLogRB, null);
    _logPanel.add(_rewriteRB, null);
    _logPanel.add(_appendRB, null);

    _centerPanel.setLayout(_verticalFlowLayout2);
    _centerPanel.add(_autoReviewPanel, BorderLayout.NORTH);
    _centerPanel.add(_alignRecoveryPanel, BorderLayout.CENTER);
    _centerPanel.add(_logPanel, null);

    this.setBorder(BorderFactory.createEmptyBorder(10,0,5,0));
    this.setLayout(_borderLayout1);
    this.add(_centerPanel, BorderLayout.CENTER);
  }

  public boolean getAutoReviewEnabled()
  {
    return this._autoReviewCkB.isSelected();
  }
  public int getAutoReviewDefectThreshold()
  {
    return ((this._autoReviewCkB.isSelected()) ? this._defectNumSpinBox.getValue() : 0);
  }
  public int getAlignmentRecovery()
  {
    if (this._terminateRecoveryRB.isSelected())
      return TERMINATE_ALIGNMENT_RECOVERY;
    else if (this._mapRecoveryRB.isSelected())
      return MAP_ALIGNMENT_RECOVERY;
    else
      return MANUAL_ALIGNMENT_RECOVERY;
  }
  public int getLogFile()
  {
    if (this._appendRB.isSelected())
      return APPEND_LOG_FILE;
    else if (this._rewriteRB.isSelected())
      return REWRITE_LOG_FILE;
    else
      return NO_LOG_FILE;
  }

  public void setAutoReviewEnabled(boolean b)
  {
    this._autoReviewCkB.setSelected(b);
  }
  public void setAutoReviewDefectThreshold(int num)
  {
    this._defectNumSpinBox.setValue(num);
  }
  public void setAlignmentRecovery(int recovery)
  {
    if (recovery == TERMINATE_ALIGNMENT_RECOVERY)
      this._terminateRecoveryRB.setSelected(true);
    else if (recovery == MAP_ALIGNMENT_RECOVERY)
      this._mapRecoveryRB.setSelected(true);
    else  // MANUAL_ALIGNMENT_RECOVERY
      this._manualRecoveryRB.setSelected(true);
  }
  public void setLogFile(int log)
  {
    if (log == APPEND_LOG_FILE)
      this._appendRB.setSelected(true);
    else if (log == REWRITE_LOG_FILE)
      this._rewriteRB.setSelected(true);
    else  // NO_LOG_FILE
      this._noLogRB.setSelected(true);
  }

  /**
   * State changed callback for auto review defects check box.  Enable/disable
   * the number of defects threshold spin box.
   */
  void _autoReviewCkB_stateChanged(ChangeEvent e)
  {
    this._defectNumSpinBox.setEnabled( this._autoReviewCkB.isSelected() );
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("InspectionOptionPane [");
    buf.append(" Auto-review:" + getAutoReviewEnabled());
    buf.append(", Auto-review threshold:" + getAutoReviewDefectThreshold());
    buf.append(", Alignment recovery:");
    switch (getAlignmentRecovery())
    {
      case MAP_ALIGNMENT_RECOVERY:
        buf.append("Map");
        break;
      case MANUAL_ALIGNMENT_RECOVERY:
        buf.append("Manual");
        break;
      case TERMINATE_ALIGNMENT_RECOVERY:
        buf.append("Terminate");
        break;
    }
    buf.append(", Log file:");
    switch (getLogFile())
    {
      case APPEND_LOG_FILE:
        buf.append("Append");
        break;
      case REWRITE_LOG_FILE:
        buf.append("Rewrite");
        break;
      case NO_LOG_FILE:
        buf.append("None");
        break;
    }
    buf.append(" ]");
    return buf.toString();
  }

  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _alignRecoveryPanel = new JPanel();
  private JPanel _autoReviewPanel = new JPanel();
  private JCheckBox _autoReviewCkB = new JCheckBox();
  private JLabel _label1 = new JLabel();
  private JRadioButton _manualRecoveryRB = new JRadioButton();
  private JRadioButton _terminateRecoveryRB = new JRadioButton();
  private JRadioButton _mapRecoveryRB = new JRadioButton();
  private IntegerSpinBox _defectNumSpinBox = new IntegerSpinBox(200, 1, 1, 99999);
  private VerticalFlowLayout _verticalFlowLayout1 = new VerticalFlowLayout();
  private VerticalFlowLayout _verticalFlowLayout2 = new VerticalFlowLayout(VerticalFlowLayout.FILL);
  private VerticalFlowLayout _verticalFlowLayout3 = new VerticalFlowLayout();
  private ButtonGroup _buttonGroup1 = new ButtonGroup();
  private ButtonGroup _buttonGroup2 = new ButtonGroup();
  private FlowLayout _flowLayout1 = new FlowLayout();
  private JPanel _logPanel = new JPanel();
  private JRadioButton _appendRB = new JRadioButton();
  private JRadioButton _rewriteRB = new JRadioButton();
  private JRadioButton _noLogRB = new JRadioButton();

}