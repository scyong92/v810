package proto.menuUI;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 * @see MenuEnable
 */

class OperatorMenuEnable extends MenuEnable
{
  public OperatorMenuEnable()
  {
    this.set(MenuEnum.STARTUP_AUTOMATIC);
    this.set(MenuEnum.OPEN_PANEL_PROGRAM);
    this.set(MenuEnum.SHUTDOWN_LONG_TERM);
    this.set(MenuEnum.SHUTDOWN_SHORT_TERM);
    this.set(MenuEnum.ALIGN_MANUAL);
    this.set(MenuEnum.HOME_RAILS);
    this.set(MenuEnum.RESET_PANEL_HANDLING);
    this.set(MenuEnum.LOGON);
    this.set(MenuEnum.LOGOUT);
  }
}