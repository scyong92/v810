package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class NewUserAction extends MenuAction
{
  public NewUserAction(Menu5DX parent)
  {
    super("Register User", null, "Create or modify a user logon", 'R', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._newUserMI_actionPerformed(e);
  }
}