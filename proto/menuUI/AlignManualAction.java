package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class AlignManualAction extends MenuAction
{
  public AlignManualAction(Menu5DX parent)
  {
    super("Manual", null, "Manually align panel", 'M', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._alignManualMI_actionPerformed(e);
  }
}