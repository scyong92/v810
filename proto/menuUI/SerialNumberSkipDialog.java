package proto.menuUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class SerialNumberSkipDialog extends JDialog
{
  private int _status = JOptionPane.CANCEL_OPTION;

  public SerialNumberSkipDialog()
  {
    this(null, "", false);
  }

  public SerialNumberSkipDialog(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception
  {
    _okBtn.setText("OK");
    _okBtn.setFont(new java.awt.Font("Dialog", 1, 12));
    _okBtn.setMnemonic('O');
    _okBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _okBtn_actionPerformed(e);
      }
    });
    _cancelBtn.setText("Cancel");
    _cancelBtn.setMnemonic('C');
    _cancelBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _cancelBtn_actionPerformed(e);
      }
    });
    _gridLayout2.setHgap(5);
    _okCancelPanel.setLayout(_gridLayout2 );
    _helpBtn.setMnemonic('H');
    _helpBtn.setText("Help");
    _helpBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _helpBtn_actionPerformed(e);
      }
    });
    _okCancelPanel.add(_okBtn, null);
    _okCancelPanel.add(_cancelBtn, null);
    _okCancelPanel.add(_helpBtn, null);
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,5,5,5));
    _flowLayout4.setAlignment(FlowLayout.RIGHT);
    _buttonPanel.setLayout(_flowLayout4);
    _buttonPanel.add(_okCancelPanel, null);

    _infoLbl.setFont(new java.awt.Font("Dialog", 0, 14));
    _infoLbl.setText("Add the serial numbers to skip, one per line.");
    _textArea.setRows(10);
    _scrollPane.getViewport().add(_textArea, null);

    _centerPanel.setBorder(BorderFactory.createEmptyBorder(10,10,0,10));
    _borderLayout2.setVgap(5);
    _centerPanel.setLayout(_borderLayout2);
    _centerPanel.add(_infoLbl, BorderLayout.NORTH);
    _centerPanel.add(_scrollPane, BorderLayout.CENTER);

    _panel1.setLayout(_borderLayout1);
    _panel1.add(_centerPanel, BorderLayout.CENTER);
    _panel1.add(_buttonPanel, BorderLayout.SOUTH);
    getContentPane().add(_panel1);
  }

  /**
   * Hide the dialog unless started from main method, then do System exit.
   */
  private void exitDialog()
  {
    this.hide();
  }

  public int showDialog()
  {
    this.show();
    return _status;
  }

  /**
   * Callback method for the OK button.
   */
  void _okBtn_actionPerformed(ActionEvent e)
  {
    _status = JOptionPane.OK_OPTION;
    exitDialog();
  }

  /**
   * Callback method for the Cancel button.
   */
  void _cancelBtn_actionPerformed(ActionEvent e)
  {
    exitDialog();
  }

  /**
   * Callback method for the Help button.
   */
  void _helpBtn_actionPerformed(ActionEvent e)
  {

  }

  private JPanel _panel1 = new JPanel();
  private BorderLayout _borderLayout1 = new BorderLayout();

  private JPanel _okCancelPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JButton _okBtn = new JButton();
  private JButton _cancelBtn = new JButton();
  private GridLayout _gridLayout2 = new GridLayout();
  private FlowLayout _flowLayout4 = new FlowLayout();
  private JPanel _centerPanel = new JPanel();
  private JScrollPane _scrollPane = new JScrollPane();
  private BorderLayout _borderLayout2 = new BorderLayout();
  private JTextArea _textArea = new JTextArea();
  private JLabel _infoLbl = new JLabel();
  private JButton _helpBtn = new JButton();

}