package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class MapPointsAction extends MenuAction
{
  public MapPointsAction(Menu5DX parent)
  {
    super("Setup Map Points", null, "Setup surface map points", 'P', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._mapPointsMI_actionPerformed(e);
  }
}