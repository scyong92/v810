package proto.menuUI;

import java.awt.*;
import javax.swing.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class InstantHelpPane extends JPanel
{

  public InstantHelpPane()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    this.setLayout(_borderLayout1);
    _helpEditorPane.setText("Instant help");
    _helpEditorPane.setForeground(Color.blue);
    _helpEditorPane.setEditable(false);
    _centerPanel.setLayout(_centerPanelLayout);
    this.add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.add(_helpScrollPane, BorderLayout.CENTER);
    _helpScrollPane.getViewport().add(_helpEditorPane, null);
  }

  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JScrollPane _helpScrollPane = new JScrollPane();
  private JEditorPane _helpEditorPane = new JEditorPane();
  private BorderLayout _centerPanelLayout = new BorderLayout();
}