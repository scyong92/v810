package proto.menuUI;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import javax.swing.event.*;
import com.agilent.guiUtil.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class StartupOptionPane extends JPanel
{

  public StartupOptionPane()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    _autoRestartEnableCB.setText("Enable automatic restart");
    _shortShutdownEnableCB.setText("Turn off x-ray voltage on short term shutdown");
    _longShutdownEnableCB.setText("Prepare for power down on long term shutdown");
    _logoutRotaryOffCB.setEnabled(false);
    _logoutRotaryOffCB.setText("Turn off rotary");
    _logoutXrayCurrentOffCB.setEnabled(false);
    _logoutXrayCurrentOffCB.setText("Turn off x-ray current");

    _jPanel2.setLayout(_verticalFlowLayout1);
    _jPanel2.setBorder(BorderFactory.createEmptyBorder(0,15,0,0));
    _logoutShutdownEnableCB.setText("Enable logout / standby shutdown");
    _logoutShutdownEnableCB.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        _logoutShutdownEnableCB_stateChanged(e);
      }
    });
    _jPanel2.add(_logoutXrayCurrentOffCB, null);
    _jPanel2.add(_logoutRotaryOffCB, null);

    _centerPanel.setLayout(_verticalFlowLayout2);
    _centerPanel.setBorder(BorderFactory.createEmptyBorder(0,5,0,5));
    _centerPanel.add(_autoRestartEnableCB, null);
    _centerPanel.add(_shortShutdownEnableCB, null);
    _centerPanel.add(_longShutdownEnableCB, null);
    _centerPanel.add(_logoutShutdownEnableCB, null);
    _centerPanel.add(_jPanel2, null);
    this.setBorder(BorderFactory.createEmptyBorder(10,0,5,0));
    this.setLayout(_borderLayout1);
    this.add(_centerPanel, BorderLayout.CENTER);
  }

  // Data access
  public boolean getAutomaticRestart()                { return _autoRestartEnableCB.isSelected(); }
  public boolean getLogoutShutdown()                  { return _logoutShutdownEnableCB.isSelected(); }
  public boolean getLogoutXrayCurrentOff()            { return _logoutXrayCurrentOffCB.isSelected(); }
  public boolean getLogoutRotaryOff()                 { return _logoutRotaryOffCB.isSelected(); }
  public boolean getShortTermShutdownXrayVoltageOff() { return _shortShutdownEnableCB.isSelected(); }
  public boolean getLongTermShutdownPowerDown()       { return _longShutdownEnableCB.isSelected(); }

  public void setAutomaticRestart(boolean rhs)                { _autoRestartEnableCB.setSelected(rhs); }
  public void setLogoutShutdown(boolean rhs)                  { _logoutShutdownEnableCB.setSelected(rhs); }
  public void setLogoutXrayCurrentOff(boolean rhs)            { _logoutXrayCurrentOffCB.setSelected(rhs); }
  public void setLogoutRotaryOff(boolean rhs)                 { _logoutRotaryOffCB.setSelected(rhs); }
  public void setShortTermShutdownXrayVoltageOff(boolean rhs) { _shortShutdownEnableCB.setSelected(rhs); }
  public void setLongTermShutdownPowerDown(boolean rhs)       { _longShutdownEnableCB.setSelected(rhs); }

  // Callback methods
  void _logoutShutdownEnableCB_stateChanged(ChangeEvent e)
  {
    _logoutXrayCurrentOffCB.setEnabled(_logoutShutdownEnableCB.isSelected());
    _logoutRotaryOffCB.setEnabled(_logoutShutdownEnableCB.isSelected());
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("StartupOptionPane [");
    buf.append(" Automatic restart:" + getAutomaticRestart());
    buf.append(", Logout shutdown:" + getLogoutShutdown());
    buf.append(", X-ray current off:" + getLogoutXrayCurrentOff());
    buf.append(", Rotary off:" + getLogoutRotaryOff());
    buf.append(", Short term shutdown x-ray voltage off:" + getShortTermShutdownXrayVoltageOff());
    buf.append(", Long term shutdown power down:" + getLongTermShutdownPowerDown());
    buf.append(" ]");
    return buf.toString();
  }

  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JCheckBox _autoRestartEnableCB = new JCheckBox();
  private JCheckBox _shortShutdownEnableCB = new JCheckBox();
  private JCheckBox _longShutdownEnableCB = new JCheckBox();
  private VerticalFlowLayout _verticalFlowLayout1 = new VerticalFlowLayout();
  private VerticalFlowLayout _verticalFlowLayout2 = new VerticalFlowLayout();
  private JCheckBox _logoutRotaryOffCB = new JCheckBox();
  private JCheckBox _logoutXrayCurrentOffCB = new JCheckBox();
  private JPanel _jPanel2 = new JPanel();
  private JCheckBox _logoutShutdownEnableCB = new JCheckBox();
}