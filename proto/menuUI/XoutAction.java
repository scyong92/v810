package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class XoutAction extends MenuAction
{
  public XoutAction(Menu5DX parent)
  {
    super("Setup X-out Detection", null, "Setup x-out detection", 'X', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._xoutMI_actionPerformed(e);
  }
}