package proto.menuUI;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class HelpContentsAction extends MenuAction
{
  private static final ImageIcon _icon = new ImageIcon(Menu5DX.class.getResource("help.gif"));

  public HelpContentsAction(Menu5DX parent)
  {
    super("Contents", _icon, "Help information", 'C', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._helpContentsMI_actionPerformed(e);
  }
}