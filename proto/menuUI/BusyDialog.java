package proto.menuUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;
import com.agilent.guiUtil.AnimatedLabel;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class BusyDialog extends JDialog
{
  private static final int _NUM_GIFS = 10;
  private static final String _gifName = "hourglass";
  private static Icon[] _icons = new Icon[_NUM_GIFS];
  static
  {
    for (int i = 0; i < _NUM_GIFS; i++)
    {
      //_icons[i] = new ImageIcon(proto.menuUI.Menu5DX.class.getResource(_gifName + (i + 1) + ".gif"));
      _icons[i] = new ImageIcon(com.agilent.xRayTest.images.Image5DX.class.getResource(_gifName + (i + 1) + ".gif"));
    }
  }
  private String _message = "";

  public BusyDialog(Frame frame, String message, String title, boolean modal)
  {
    super(frame, title, modal);
    _message = message;
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }

    this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    this._iconLbl.setRunning(true);
  }

  void jbInit() throws Exception
  {
    _iconLbl.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent e)
      {
        _iconLbl.setRunning( !_iconLbl.isRunning() );
      }
    });
    _messageTA.setColumns(40);
    _messageTA.setWrapStyleWord(true);
    _messageTA.setLineWrap(true);
    _messageTA.setBackground(Color.lightGray);
    _messageTA.setEditable(false);
    _messageTA.setText(_message);

    _borderLayout1.setHgap(10);
    _panel1.setLayout(_borderLayout1);
    _panel1.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    _panel1.add(_iconLbl, BorderLayout.WEST);
    _panel1.add(_messageTA, BorderLayout.CENTER);
    getContentPane().add(_panel1);
  }

  public void addEscapeActionListener(ActionListener listener)
  {
    if (listener != null)
    {
      _iconLbl.registerKeyboardAction(listener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
  }

  public void removeEscapeActionListener(ActionListener listener)
  {
    if (listener != null)
    {
      _iconLbl.unregisterKeyboardAction(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true));
    }
  }

// DEBUG
  public static void main(String[] args)
  {
    try
    {
      // Make sure the look and feel is set before any components are created.
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (ClassNotFoundException e) { /* do nothing */ }
    catch (InstantiationException e) { /* do nothing */ }
    catch (IllegalAccessException e) { /* do nothing */ }
    catch (UnsupportedLookAndFeelException e) { /* do nothing */ }

    BusyDialog dlg = new BusyDialog(null, "This is the busy message.", "Busy Dialog", true);
    com.agilent.guiUtil.SwingUtils.setScreenPosition(dlg,0.5,0.25);
    dlg.addEscapeActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        System.exit(0);
      }
    });
    dlg.show();
  }
// DEBUG
  private JPanel _panel1 = new JPanel();
  private BorderLayout _borderLayout1 = new BorderLayout();
  private AnimatedLabel _iconLbl = new AnimatedLabel(_icons);
  private JTextArea _messageTA = new JTextArea();
}