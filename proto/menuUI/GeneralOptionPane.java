package proto.menuUI;

import java.awt.*;
import javax.swing.border.Border;
import javax.swing.*;
import com.agilent.guiUtil.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class GeneralOptionPane extends JPanel
{

  public GeneralOptionPane()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    _mbLbl.setText("MB");
    _diskSpaceLbl.setText("Minimum free disk space:");

    _flowLayout1.setAlignment(FlowLayout.LEFT);
    _diskSpacePanel.setLayout(_flowLayout1);
    _diskSpacePanel.add(_diskSpaceLbl, null);
    _diskSpacePanel.add(_diskSpaceSpinBox, null);
    _diskSpacePanel.add(_mbLbl, null);

    _centerPanel.setLayout(_verticalFlowLayout1);
    _centerPanel.add(_diskSpacePanel, null);

    this.setBorder(BorderFactory.createEmptyBorder(10,0,5,0));
    this.setLayout(_borderLayout1);
    this.add(_centerPanel, BorderLayout.CENTER);
  }

  public int getFreeSpace()   { return this._diskSpaceSpinBox.getValue(); }

  public void setFreeSpace(int rhs)  { this._diskSpaceSpinBox.setValue(rhs); }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("GeneralOptionPane [");
    buf.append(" Free space:" + getFreeSpace());
    buf.append(" ]");
    return buf.toString();
  }

  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _diskSpacePanel = new JPanel();
  private JLabel _mbLbl = new JLabel();
  private IntegerSpinBox _diskSpaceSpinBox = new IntegerSpinBox(3, 1, 0, 100);
  private JLabel _diskSpaceLbl = new JLabel();
  private VerticalFlowLayout _verticalFlowLayout1 = new VerticalFlowLayout(VerticalFlowLayout.FILL);
  private FlowLayout _flowLayout1 = new FlowLayout();
}