package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class LogoutAction extends MenuAction
{
  public LogoutAction(Menu5DX parent)
  {
    super("Logout", null, "Logout the current user", 'U', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
//    System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._logoutMI_actionPerformed(e);
  }
}