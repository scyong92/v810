package proto.menuUI;

import proto.menuUI.autotest.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author
 * @version 1.0
 */

public interface PanelQueueInt
{
  public void setController(AutoTestInt autoTestPanel);
  public boolean panelAvailable();
  public PanelEntry removePanel();
  public void addPanel(PanelEntry panel);
}