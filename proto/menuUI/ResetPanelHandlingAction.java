package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ResetPanelHandlingAction extends MenuAction
{
  public ResetPanelHandlingAction(Menu5DX parent)
  {
    super("Reset", null, "Reset the panel handling subsystem", 'R', parent);
  }
  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._resetPanelHandlingMI_actionPerformed(e);
  }
}