package proto.menuUI.logon;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class DeleteUserDialog extends JDialog
{
  private int _status = JOptionPane.CANCEL_OPTION;
  private ArrayList _deletedUsers = new ArrayList();


  public DeleteUserDialog(Frame frame, String title, boolean modal, Object[] users)
  {
    super(frame, title, modal);
    if (users != null && users.length > 0)
      for (int i = 0; i < users.length; i++)
        if (users[i] != null)
          _users.addElement(users[i]);

    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }

    this.addWindowListener( new WindowAdapter()
    {
      public void windowClosing(WindowEvent e)
      {
        exitDialog();
      }
      public void windowOpened(WindowEvent e)
      {
        //_usernameTF.requestFocus();
      }
    });
  }

  void jbInit() throws Exception
  {
    // Define the ActionListeners for the buttons and textfields.
    ActionListener okActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _okBtn_actionPerformed(e);
      }
    };

    ActionListener cancelActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _cancelBtn_actionPerformed(e);
      }
    };

    String msg = "<html>Select a user from the list and click Delete.";
    border1 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134)),BorderFactory.createEmptyBorder(5,5,0,5));
    _messageLbl.setText(msg);
    _okCancelBtnPanel.setLayout(_flowLayout2);
    _flowLayout2.setAlignment(FlowLayout.RIGHT);
    _okBtn.setText("OK");
    _okBtn.addActionListener(okActionListener);
    _okBtn.setFont(new java.awt.Font("Dialog", 1, 12));
    _okBtn.setMnemonic('O');
    _cancelBtn.setText("Cancel");
    _cancelBtn.addActionListener(cancelActionListener);
    _cancelBtn.setMnemonic('C');
    _deleteBtn.setMnemonic('D');
    _deleteBtn.setText("Delete");
    _deleteBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _deleteBtn_actionPerformed(e);
      }
    });
    _deletePanel.setLayout(_flowLayout3);
    _flowLayout3.setAlignment(FlowLayout.LEFT);
    _messagePanel.add(_messageLbl, null);

    _list1.setModel(_users);
    _list1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _scrollPane1.getViewport().add(_list1, null);
    _listPanel.setLayout(_listPanelLayout);
    _listPanel.add(_scrollPane1, BorderLayout.CENTER);


    _centerPanel.setLayout(_centerPanelLayout);
    _centerPanel.setBorder(border1);
    _centerPanel.add(_listPanel, BorderLayout.CENTER);

    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,5,5,5));
    _buttonPanel.setLayout(_gridLayout1);

    _panel1.setLayout(_borderLayout1);
    _panel1.setBorder(BorderFactory.createEmptyBorder(10,5,0,5));
    _panel1.add(_messagePanel, BorderLayout.NORTH);
    _panel1.add(_centerPanel, BorderLayout.CENTER);
    _panel1.add(_buttonPanel, BorderLayout.SOUTH);
    _buttonPanel.add(_deletePanel, null);
    _deletePanel.add(_deleteBtn, null);
    _buttonPanel.add(_okCancelBtnPanel, null);
    _okCancelBtnPanel.add(_okBtn, null);
    _okCancelBtnPanel.add(_cancelBtn, null);
    getContentPane().add(_panel1);
  }

  /**
   * Return the list of selected users.
   */
  public java.util.List getSelectedList()
  {
    return _deletedUsers;
  }

  /**
   * Hide the dialog unless started from main method, then do System exit.
   */
  private void exitDialog()
  {
    this.hide();
    if (_startFromMain)
      System.exit(0);
  }

  public int showDialog()
  {
    this.show();
    return _status;
  }

  /**
   * Callback method for the Delete button.
   */
  void _deleteBtn_actionPerformed(ActionEvent e)
  {
    int selected = _list1.getSelectedIndex();
    if (selected < 0)
      return;  // nothing selected
    _deletedUsers.add(_list1.getSelectedValue());
    _users.remove(selected);
  }

  /**
   * Callback method for the OK button.
   */
  void _okBtn_actionPerformed(ActionEvent e)
  {
    if (_deletedUsers.size() > 0)
    {
      StringBuffer msg = new StringBuffer("Are you sure you want to delete these users?");
      for (int i = 0; i < _deletedUsers.size(); i++)
        msg.append( "\n" + _deletedUsers.get(i).toString());

      int retval = JOptionPane.showConfirmDialog(this, msg.toString());
      if (retval != JOptionPane.OK_OPTION)
        return;
    }
    _status = JOptionPane.OK_OPTION;
    exitDialog();
  }

  /**
   * Callback method for the Cancel button.
   */
  void _cancelBtn_actionPerformed(ActionEvent e)
  {
    exitDialog();
  }

// DEBUG
  public static void main(String[] args)
  {
    try
    {
      // Make sure the look and feel is set before any components are created.
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (ClassNotFoundException e) { /* do nothing */ }
    catch (InstantiationException e) { /* do nothing */ }
    catch (IllegalAccessException e) { /* do nothing */ }
    catch (UnsupportedLookAndFeelException e) { /* do nothing */ }

//    Assert.setLogFile("assert.log",null);
    ArrayList users = new ArrayList();
    users.add("Deb");
    users.add("Chad");
    users.add("Shawna");
    users.add("Trevor");
    users.add("Stesha");
    DeleteUserDialog dlg = new DeleteUserDialog(null, "Delete User", true, users.toArray());
//      new proto.menuUI.Valid5dxUser("M:/sanon_5dx/java/proto/menuUI/passwords.dat"));
    dlg._startFromMain = true;
    com.agilent.guiUtil.SwingUtils.setScreenPosition(dlg,0.5,0.25);
    int returnVal = dlg.showDialog();
    if (returnVal == JOptionPane.OK_OPTION)
    {
      System.out.println("OK");
      java.util.Iterator iter = dlg.getSelectedList().iterator();
      while (iter.hasNext())
        System.out.println( iter.next());
    }
    else
      System.out.println("Canceled");
  }
  private boolean _startFromMain = false;
// DEBUG
  private JPanel _panel1 = new JPanel();
  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private BorderLayout _centerPanelLayout = new BorderLayout();
  private DefaultListModel _users = new DefaultListModel();
  private JPanel _messagePanel = new JPanel();
  private JLabel _messageLbl = new JLabel();
  private JPanel _listPanel = new JPanel();
  private BorderLayout _listPanelLayout = new BorderLayout();
  private JList _list1 = new JList();
  private JScrollPane _scrollPane1 = new JScrollPane();
  private Border border1;
  private JPanel _okCancelBtnPanel = new JPanel();
  private FlowLayout _flowLayout2 = new FlowLayout();
  private JButton _okBtn = new JButton();
  private JButton _cancelBtn = new JButton();
  private JPanel _deletePanel = new JPanel();
  private JButton _deleteBtn = new JButton();
  private FlowLayout _flowLayout3 = new FlowLayout();
  private GridLayout _gridLayout1 = new GridLayout();

}