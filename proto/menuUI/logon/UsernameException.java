package proto.menuUI.logon;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class UsernameException extends LogonException
{
  // @todo Should be localized string.
  private static final String _INVALID_USER = "The user name is not valid.";

  public UsernameException()
  {
    this(_INVALID_USER);
  }

  public UsernameException( String message )
  {
    super(message);
  }
}