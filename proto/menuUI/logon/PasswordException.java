package proto.menuUI.logon;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class PasswordException extends LogonException
{
  // @todo Should be localized string.
  private static final String _INVALID_PASSWORD = "The password is not correct.";

  public PasswordException()
  {
    this(_INVALID_PASSWORD);
  }

  public PasswordException( String message )
  {
    super(message);
  }
}