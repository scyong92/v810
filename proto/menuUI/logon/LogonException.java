package proto.menuUI.logon;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class LogonException extends Exception
{
  public LogonException( String message )
  {
    super(message);
  }
}