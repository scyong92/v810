package proto.menuUI.logon;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class PasswordFileException extends LogonException
{
  // @todo Should be localized string.
  private static final String _NO_PASSWORD_FILE = "Password file not found or not readable.  Contact the System Administrator.";

  public PasswordFileException()
  {
    this(_NO_PASSWORD_FILE);
  }

  public PasswordFileException( String message )
  {
    super(message);
  }
}