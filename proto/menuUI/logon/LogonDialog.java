package proto.menuUI.logon;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;
import com.agilent.guiUtil.PairLayout;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class LogonDialog extends JDialog
{
  private static final ImageIcon _KEY_ICON = new ImageIcon(proto.menuUI.logon.LogonDialog.class.getResource("key.gif"));
  private int _status = JFileChooser.CANCEL_OPTION;
  private LogonValidationInt _validator = null;

  /**
   * Constructor.
   *
   * @param frame - parent of the dialog, can be null.
   * @param title - title of the dialog.
   * @param modal - determines whether the dialog is modal.
   */
  public LogonDialog(Frame frame, String title, boolean modal, LogonValidationInt validator)
  {
    super(frame, title, modal);
    // Assert( validator != null);
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }

    this.addWindowListener( new WindowAdapter()
    {
      public void windowClosing(WindowEvent e)
      {
        exitLogonDialog();
      }
      public void windowOpened(WindowEvent e)
      {
        _usernameTF.requestFocus();
      }
    });
//    setScreenPosition();
    _validator = validator;
  }

  /**
   * Constructor.
   */
  public LogonDialog( LogonValidationInt validator )
  {
    this(null, "", false, validator);
  }

  /**
   * Create the contents of the dialog.
   */
  void jbInit() throws Exception
  {
    // Define the ActionListeners for the buttons and textfields.
    ActionListener okActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _okBtn_actionPerformed(e);
      }
    };

    ActionListener cancelActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _cancelBtn_actionPerformed(e);
      }
    };

    ActionListener maybeOkActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _maybeOkBtn_actionPerformed(e);
      }
    };


    _messageLbl.setForeground(Color.black);
    _messageLbl.setIcon(_KEY_ICON);
    _messageLbl.setText("Please enter your logon information.");
    _messagePanel.setBorder(BorderFactory.createEmptyBorder(5,5,0,5));
    _flowLayout1.setAlignment(FlowLayout.RIGHT);
    _okBtn.setFont(new java.awt.Font("Dialog", 1, 12));
    _okBtn.setMnemonic('O');
    _cancelBtn.setMnemonic('C');
    _messagePanel.add(_messageLbl, null);

    _userLbl.setText("User Name");
    _usernameTF.setColumns(20);
    _usernameTF.registerKeyboardAction(maybeOkActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_FOCUSED);
    _passwordLbl.setText("Password");
    _passwordPTF.setColumns(20);
    _passwordPTF.registerKeyboardAction(okActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_FOCUSED);
    _centerPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
    _centerPanel.setLayout( new PairLayout() );
    _centerPanel.add(_userLbl, null);
    _centerPanel.add(_usernameTF, null);
    _centerPanel.add(_passwordLbl, null);
    _centerPanel.add(_passwordPTF, null);

    _okBtn.setText("OK");
    _okBtn.addActionListener(okActionListener);
//    _okBtn.registerKeyboardAction(okActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_FOCUSED);
    _cancelBtn.setText("Cancel");
    _cancelBtn.addActionListener(cancelActionListener);
//    _cancelBtn.registerKeyboardAction(cancelActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_FOCUSED);
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,5,5,5));
    _buttonPanel.setLayout(_flowLayout1);
    _buttonPanel.add(_okBtn, null);
    _buttonPanel.add(_cancelBtn, null);

    _panel1.setLayout(_borderLayout1);
    _panel1.setBorder(BorderFactory.createEmptyBorder(0,5,0,5));
    _panel1.add(_messagePanel, BorderLayout.NORTH);
    _panel1.add(_centerPanel, BorderLayout.CENTER);
    _panel1.add(_buttonPanel, BorderLayout.SOUTH);
    getContentPane().add(_panel1);
//    this.getRootPane().setDefaultButton(_okBtn);
  }

  /**
   * Hide the dialog unless started from main method, then do System exit.
   */
  private void exitLogonDialog()
  {
    this.hide();
    if (_startFromMain)
      System.exit(0);
  }

  /**
   * Put the dialog in the upper center of the screen.
   */
  private void setScreenPosition()
  {
    pack();
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension thisSize = getSize();
    if (thisSize.height > screenSize.height)
      thisSize.height = screenSize.height;
    if (thisSize.width > screenSize.width)
      thisSize.width = screenSize.width;
    setLocation( screenSize.width / 2 - thisSize.width / 2, screenSize.height / 4);
  }

  /**
   * Return the user name.
   */
  public String getUsername()
  {
    return this._usernameTF.getText();
  }

  /**
   * Return the password.
   */
  public String getPassword()
  {
    return new String( this._passwordPTF.getPassword() );
  }

  /**
   * Callback method for the OK button.
   */
  void _okBtn_actionPerformed(ActionEvent e)
  {
    String userName = this.getUsername();
    String password = this.getPassword();
// DEBUG
//    System.out.println("OK button pressed");
//    System.out.println("  User: " + userName + "  Password: " + password);
// DEBUG
// DEBUG
//    if (_validator == null)
//      return;
// DEBUG
    boolean validUser = false;
    try
    {
      validUser = _validator.validateUserLogon( userName, password );
    }
    catch (PasswordFileException pfe)
    {
      JOptionPane.showMessageDialog(this, pfe.getMessage(), "No Password File", JOptionPane.ERROR_MESSAGE);
      this._passwordPTF.setText("");
      this._usernameTF.requestFocus();
      this.repaint();
    }
    catch (PasswordException pe)
    {
      JOptionPane.showMessageDialog(this, pe.getMessage(), "Invalid Password", JOptionPane.ERROR_MESSAGE);
      this._passwordPTF.setText("");
      this._passwordPTF.requestFocus();
      this.repaint();
    }
    catch (UsernameException ue)
    {
      JOptionPane.showMessageDialog(this, ue.getMessage(), "Unknown User Name", JOptionPane.ERROR_MESSAGE);
      this._passwordPTF.setText("");
      this._usernameTF.requestFocus();
      this.repaint();
    }
    catch (LogonException le)
    {
      JOptionPane.showMessageDialog(this, "Invalid user name or password.", "Invalid Logon", JOptionPane.ERROR_MESSAGE);
      this._passwordPTF.setText("");
      this._usernameTF.requestFocus();
      this.repaint();
    }

    if (validUser)
    {
      _status = JFileChooser.APPROVE_OPTION;
      this.exitLogonDialog();  // got a good one so get out
    }
  }

  /**
   * Callback method for the Cancel button.
   */
  void _cancelBtn_actionPerformed(ActionEvent e)
  {
    exitLogonDialog();
  }

  /**
   * Callback method when hitting Enter in the user name textfield.
   * Checks to see if a password is required and if not, it calls the
   * OK method.
   */
  private void _maybeOkBtn_actionPerformed(ActionEvent e)
  {
    String userName = this.getUsername();
    String password = this.getPassword();
// DEBUG
//    System.out.println("Maybe OK button pressed");
//    System.out.println("  User: " + userName + "  Password: " + password);
// DEBUG
// DEBUG
//    this._passwordPTF.requestFocus();
//    this.repaint();
//    if (_validator == null)
//      return;
// DEBUG
    boolean validUser = false;
    try
    {
      validUser = _validator.validateUserLogon( userName, password );
    }
    catch (LogonException le)
    {
      // Not a valid user so continue on to get the password.
      this._passwordPTF.requestFocus();
      this.repaint();
    }

    if (validUser)
      this._okBtn_actionPerformed(e);  // let the OK method handle the rest
  }

  public int showDialog()
  {
    this.show();
    return _status;
  }

// DEBUG
  public static void main(String[] args)
  {
    try
    {
      // Make sure the look and feel is set before any components are created.
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (ClassNotFoundException e) { /* do nothing */ }
    catch (InstantiationException e) { /* do nothing */ }
    catch (IllegalAccessException e) { /* do nothing */ }
    catch (UnsupportedLookAndFeelException e) { /* do nothing */ }

//    Assert.setLogFile("assert.log",null);
    LogonDialog loginUnit = new LogonDialog(null, "Logon", true,
      new proto.menuUI.Valid5dxUser("M:/sanon_5dx/java/proto/menuUI/passwords.dat"));
    loginUnit._startFromMain = true;
    com.agilent.guiUtil.SwingUtils.setScreenPosition(loginUnit,0.5,0.25);
    int returnVal = loginUnit.showDialog();
    if (returnVal == JFileChooser.APPROVE_OPTION)
      System.out.println("OK");
    else
      System.out.println("Canceled");
//    loginUnit.show();
  }
  private boolean _startFromMain = false;
// DEBUG
  private JPanel _panel1 = new JPanel();
  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _messagePanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _centerPanel = new JPanel();
  private JLabel _messageLbl = new JLabel();
  private JButton _okBtn = new JButton();
  private JButton _cancelBtn = new JButton();
  private JLabel _userLbl = new JLabel();
  private JTextField _usernameTF = new JTextField();
  private JLabel _passwordLbl = new JLabel();
  private JPasswordField _passwordPTF = new JPasswordField();
  private FlowLayout _flowLayout1 = new FlowLayout();

}