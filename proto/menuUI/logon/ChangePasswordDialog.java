package proto.menuUI.logon;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.agilent.guiUtil.PairLayout;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class ChangePasswordDialog extends JDialog
{
  private int _status = JOptionPane.CANCEL_OPTION;
  private LogonValidationInt _validator = null;


  public ChangePasswordDialog(Frame frame, String title, boolean modal, LogonValidationInt validator)
  {
    super(frame, title, modal);
    // Assert( validator != null);
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
    this.addWindowListener( new WindowAdapter()
    {
      public void windowClosing(WindowEvent e)
      {
        exitDialog();
      }
      public void windowOpened(WindowEvent e)
      {
        _passwordPTF.requestFocus();
      }
    });
    _validator = validator;
  }

  void jbInit() throws Exception
  {
    // Define the ActionListeners for the buttons and textfields.
    ActionListener okActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _okBtn_actionPerformed(e);
      }
    };

    ActionListener cancelActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _cancelBtn_actionPerformed(e);
      }
    };

    ActionListener nextFocusActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _nextFocus_actionPerformed(e);
      }
    };


    _userLbl.setText("User Name");
// DEBUG
    _usernameTF.setText("SUPERVISOR");
// DEBUG
    _usernameTF.setColumns(20);
    _usernameTF.registerKeyboardAction(nextFocusActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_FOCUSED);
    _passwordLbl.setText("Old Password");
    _passwordPTF.setColumns(20);
    _passwordPTF.registerKeyboardAction(nextFocusActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_FOCUSED);
    _newPasswordLbl.setText("New Password");
    _newPasswordPTF.setColumns(20);
    _newPasswordPTF.registerKeyboardAction(nextFocusActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_FOCUSED);
    _confirmPasswordLbl.setText("Confirm New Password");
    _confirmPasswordPTF.setColumns(20);
    _confirmPasswordPTF.registerKeyboardAction(nextFocusActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_FOCUSED);
    _centerPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    _centerPanel.setLayout( new PairLayout() );
    _flowLayout1.setAlignment(FlowLayout.RIGHT);
    _okBtn.setFont(new java.awt.Font("Dialog", 1, 12));
    _okBtn.setMnemonic('O');
    _cancelBtn.setMnemonic('C');
    _centerPanel.add(_userLbl, null);
    _centerPanel.add(_usernameTF, null);
    _centerPanel.add(_passwordLbl, null);
    _centerPanel.add(_passwordPTF, null);
    _centerPanel.add(_newPasswordLbl, null);
    _centerPanel.add(_newPasswordPTF, null);
    _centerPanel.add(_confirmPasswordLbl, null);
    _centerPanel.add(_confirmPasswordPTF, null);

    _okBtn.setText("OK");
    _okBtn.addActionListener(okActionListener);
    _cancelBtn.setText("Cancel");
    _cancelBtn.addActionListener(cancelActionListener);
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,5,5,5));
    _buttonPanel.setLayout(_flowLayout1);
    _buttonPanel.add(_okBtn, null);
    _buttonPanel.add(_cancelBtn, null);

    _panel1.setLayout(_borderLayout1);
    _panel1.setBorder(BorderFactory.createEmptyBorder(0,5,0,5));
    _panel1.add(_centerPanel, BorderLayout.CENTER);
    _panel1.add(_buttonPanel, BorderLayout.SOUTH);
    getContentPane().add(_panel1);
  }

  /**
   * Return the user name.
   */
  public void setUsername(String user)
  {
    this._usernameTF.setText(user);
  }

  /**
   * Return the user name.
   */
  public String getUsername()
  {
    return this._usernameTF.getText();
  }

  /**
   * Return the old password.
   */
  public String getPassword()
  {
    return new String( this._passwordPTF.getPassword() );
  }

  /**
   * Return the new password.
   */
  public String getNewPassword()
  {
    return new String( this._newPasswordPTF.getPassword() );
  }

  /**
   * Return the confirm password.
   */
  public String getConfirmPassword()
  {
    return new String( this._confirmPasswordPTF.getPassword() );
  }

  /**
   * Hide the dialog unless started from main method, then do System exit.
   */
  private void exitDialog()
  {
    this.hide();
    if (_startFromMain)
      System.exit(0);
  }

  public int showDialog()
  {
    this.show();
    return _status;
  }

  /**
   * Callback method for the OK button.
   */
  void _okBtn_actionPerformed(ActionEvent e)
  {
    String userName = this.getUsername();
    String password = this.getPassword();
    boolean validUser = false;

    try
    {
      validUser = _validator.validateUserLogon( userName, password );
    }
    catch (PasswordFileException pfe)
    {
      JOptionPane.showMessageDialog(this, pfe.getMessage(), "No Password File", JOptionPane.ERROR_MESSAGE);
      this._passwordPTF.setText("");
      this._newPasswordPTF.setText("");
      this._confirmPasswordPTF.setText("");
      this._usernameTF.requestFocus();
      this.repaint();
    }
    catch (PasswordException pe)
    {
      JOptionPane.showMessageDialog(this, pe.getMessage(), "Invalid Password", JOptionPane.ERROR_MESSAGE);
      this._passwordPTF.setText("");
      this._newPasswordPTF.setText("");
      this._confirmPasswordPTF.setText("");
      this._passwordPTF.requestFocus();
      this.repaint();
    }
    catch (UsernameException ue)
    {
      JOptionPane.showMessageDialog(this, ue.getMessage(), "Unknown User Name", JOptionPane.ERROR_MESSAGE);
      this._passwordPTF.setText("");
      this._newPasswordPTF.setText("");
      this._confirmPasswordPTF.setText("");
      this._usernameTF.requestFocus();
      this.repaint();
    }
    catch (LogonException le)
    {
      JOptionPane.showMessageDialog(this, "Invalid user name or password.", "Invalid User", JOptionPane.ERROR_MESSAGE);
      this._passwordPTF.setText("");
      this._newPasswordPTF.setText("");
      this._confirmPasswordPTF.setText("");
      this._usernameTF.requestFocus();
      this.repaint();
    }

    if (validUser)
    {
      String newPassword = this.getNewPassword();
      String confirmPassword = this.getConfirmPassword();
      if (newPassword.equals(confirmPassword))
      {
        _status = JOptionPane.OK_OPTION;
        this.exitDialog();  // got a good one so get out
      }
      else
      {
        JOptionPane.showMessageDialog(this, "The new passwords do not match.", "Password Mismatch", JOptionPane.ERROR_MESSAGE);
        this._newPasswordPTF.setText("");
        this._confirmPasswordPTF.setText("");
        this._newPasswordPTF.requestFocus();
        this.repaint();
      }
    }
  }

  /**
   * Callback method for the Cancel button.
   */
  void _cancelBtn_actionPerformed(ActionEvent e)
  {
    _status = JOptionPane.CANCEL_OPTION;
    exitDialog();
  }

  /**
   * Callback method when hitting Enter in the textfields.
   */
  private void _nextFocus_actionPerformed(ActionEvent e)
  {
    JComponent command = (JComponent)e.getSource();
    if (command == this._usernameTF)
      this._passwordPTF.requestFocus();
    else if (command == this._passwordPTF)
      this._newPasswordPTF.requestFocus();
    else if (command == this._newPasswordPTF)
      this._confirmPasswordPTF.requestFocus();
    else if (command == this._confirmPasswordPTF)
      this._okBtn.requestFocus();

    this.repaint();
  }


// DEBUG
  public static void main(String[] args)
  {
    try
    {
      // Make sure the look and feel is set before any components are created.
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (ClassNotFoundException e) { /* do nothing */ }
    catch (InstantiationException e) { /* do nothing */ }
    catch (IllegalAccessException e) { /* do nothing */ }
    catch (UnsupportedLookAndFeelException e) { /* do nothing */ }

//    Assert.setLogFile("assert.log",null);
    ChangePasswordDialog dlg = new ChangePasswordDialog(null, "Change Password", true,
          new proto.menuUI.Valid5dxUser("M:/sanon_5dx/java/proto/menuUI/passwords.dat"));
    dlg._startFromMain = true;
    com.agilent.guiUtil.SwingUtils.setScreenPosition(dlg,0.5,0.25);
    int returnVal = dlg.showDialog();
    if (returnVal == JOptionPane.OK_OPTION)
      System.out.println("OK");
    else
      System.out.println("Canceled");
  }
  private boolean _startFromMain = false;
// DEBUG
  private JPanel _panel1 = new JPanel();
  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JButton _okBtn = new JButton();
  private JButton _cancelBtn = new JButton();
  private JLabel _userLbl = new JLabel();
  private JTextField _usernameTF = new JTextField();
  private JLabel _passwordLbl = new JLabel();
  private JPasswordField _passwordPTF = new JPasswordField();
  private JLabel _newPasswordLbl = new JLabel();
  private JPasswordField _newPasswordPTF = new JPasswordField();
  private JLabel _confirmPasswordLbl = new JLabel();
  private JPasswordField _confirmPasswordPTF = new JPasswordField();
  private FlowLayout _flowLayout1 = new FlowLayout();

}