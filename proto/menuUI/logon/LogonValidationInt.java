package proto.menuUI.logon;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public interface LogonValidationInt
{
  public boolean validateUserLogon( String userName, String password ) throws LogonException;
}