package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class HomeRailsAction extends MenuAction
{
  public HomeRailsAction(Menu5DX parent)
  {
    super("Home Rails", null, "Move panel handling rails to home position", 'H', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._homeRailsMI_actionPerformed(e);
  }
}