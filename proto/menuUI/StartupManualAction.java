package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class StartupManualAction extends MenuAction
{
  public StartupManualAction(Menu5DX parent)
  {
    super("Manual", null, "Run manual startup", 'M', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    System.out.println(this.getTooltip());
// DEBUG
  }
}