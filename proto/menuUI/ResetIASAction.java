package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ResetIASAction extends MenuAction
{
  public ResetIASAction(Menu5DX parent)
  {
    super("Reset Image Analysis (IAS)", null, "Reset the image analysis subsystem", 'I', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._resetIASMI_actionPerformed(e);
  }
}