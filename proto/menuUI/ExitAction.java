package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class ExitAction extends MenuAction
{
  public ExitAction(Menu5DX parent)
  {
    super("Exit", null, "Exit from 5DX", 'X', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
    _parent._exitMI_actionPerformed(e);
  }
}