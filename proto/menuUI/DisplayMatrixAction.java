package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class DisplayMatrixAction extends MenuAction
{
  public DisplayMatrixAction(Menu5DX parent)
  {
    super("Display Matrix", null, "Display the alignment matrix", 'D', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._displayMatMI_actionPerformed(e);
  }
}