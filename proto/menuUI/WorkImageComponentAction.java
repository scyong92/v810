package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class WorkImageComponentAction extends MenuAction
{
  public WorkImageComponentAction(Menu5DX parent)
  {
    super("Component Test", null, "Save images for component test", 'C', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._workImageAutoMI_actionPerformed(e);
  }
}