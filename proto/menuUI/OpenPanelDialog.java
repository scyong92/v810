package proto.menuUI;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import com.agilent.guiUtil.PairLayout;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class OpenPanelDialog extends JDialog
{
  private String[] _lookIn = { "local directory", "cad database" };
  private int _status = JOptionPane.CANCEL_OPTION;
  private Object [] _listData = null;

  public OpenPanelDialog(Frame frame, String title, boolean modal, Object [] listData)
  {
    super(frame, title, modal);
    _listData = listData;
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
    // Set the Window 'X' button to exit the application.
    addWindowListener( new java.awt.event.WindowAdapter()
    { public void windowClosing(java.awt.event.WindowEvent e) { _cancelBtn_actionPerformed(null); } });
    this.setSize(400,300);
  }

  public OpenPanelDialog( Object [] listData )
  {
    this(null, "Open Panel Program", true, listData);
  }

  void jbInit() throws Exception
  {
    _lookInLbl.setText("Look in:");
    _lookInPanel.setBorder(BorderFactory.createEmptyBorder(0,0,10,0));
    _flowLayout1.setAlignment(FlowLayout.RIGHT);
//    _lookInPanel.add(_lookInLbl, null);
//    _lookInPanel.add(_lookInCBox, null);

    _fileList.setListData(_listData);
    _fileList.setVisibleRowCount(8);
    _fileList.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyTyped(KeyEvent e)
      {
        _fileList_keyTyped(e);
      }
    });
    _fileList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _fileList.addMouseListener(new java.awt.event.MouseAdapter()
    {
      public void mouseClicked(MouseEvent e)
      {
        _fileList_mouseClicked(e);
      }
    });
    _fileListScroll.getViewport().add(_fileList, null);
    _fileListPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(93, 93, 93),new Color(134, 134, 134)));
    _fileListPanel.setLayout(fileListPanelLayout);
    _fileListPanel.add(_fileListScroll, BorderLayout.CENTER);

    _filenameLbl.setText("Panel ");
    _selectedFileTF.setEditable(false);
    _selectionPanel.setBorder(BorderFactory.createEmptyBorder(10,0,0,0));
    _selectionPanel.setLayout( new PairLayout() );
//    _selectionPanel.add(_filenameLbl, null);
//    _selectionPanel.add(_selectedFileTF, null);

    _centerPanel.setBorder(BorderFactory.createEmptyBorder(10,5,5,5));
    _centerPanel.setLayout(_centerPanelLayout);
    _centerPanel.add(_lookInPanel, BorderLayout.NORTH);
    _centerPanel.add(_fileListPanel, BorderLayout.CENTER);
    _centerPanel.add(_selectionPanel, BorderLayout.SOUTH);

    _okBtn.setFont(new java.awt.Font("Dialog", 1, 12));
    _okBtn.setMnemonic('O');
    _okBtn.setText("Open");
    _okBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _okBtn_actionPerformed(e);
      }
    });
    _cancelBtn.setMnemonic('C');
    _cancelBtn.setText("Cancel");
    _cancelBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _cancelBtn_actionPerformed(e);
      }
    });
    _gridLayout2.setHgap(5);
    _okCancelPanel.setLayout(_gridLayout2);
    _okCancelPanel.add(_okBtn, null);
    _okCancelPanel.add(_cancelBtn, null);
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,5,5,5));
    _buttonPanel.setLayout(_flowLayout1);
    _buttonPanel.add(_okCancelPanel, null);

    _rootPanel.setLayout(_rootPanelLayout);
    _rootPanel.add(_centerPanel, BorderLayout.CENTER);
    _rootPanel.add(_buttonPanel, BorderLayout.SOUTH);
    getContentPane().add(_rootPanel);
    this.getRootPane().setDefaultButton(_okBtn);
  }

  /**
   * Exit method for the application.  Sets the done flag to true and does
   * a System.exit if the application was started from the class main method.
   */
  private void exitDialog( int exitStatus )
  {
    this.hide();
//    if (_startFromMain)
//      System.exit(exitStatus);
  }

  public int showDialog()
  {
    this.show();
    return _status;
  }

  public String getSelectedFile()
  {
//    return this._selectedFileTF.getText();
    int index = _fileList.getSelectedIndex();
    if (index >= 0)
      return _listData[index].toString();
    return null;
  }

  void _cancelBtn_actionPerformed(ActionEvent e)
  {
    _status = JOptionPane.CANCEL_OPTION;
    exitDialog( 1 );
  }

  void _okBtn_actionPerformed(ActionEvent e)
  {
    String file = getSelectedFile();
    if (file == null || file.equals(""))
      return;
    _status = JOptionPane.OK_OPTION;
    exitDialog(0);
  }

  void _fileList_mouseClicked(MouseEvent e)
  {
    if (_fileList.getSelectedIndex() < 0)
      return;
    String panelName = _listData[ _fileList.getSelectedIndex() ].toString();
    this._selectedFileTF.setText(panelName);
    if (e.getClickCount() > 1)
      this._okBtn_actionPerformed(null);
  }

  private static final int _CHAR_DELTA = 1000;
  private long _time = 0;
  private String _key = "";

  void _fileList_keyTyped(KeyEvent e)
  {
    char ch = e.getKeyChar();
    if (!Character.isLetterOrDigit(ch) && ch != '_')
      return;

    if (_time + _CHAR_DELTA < System.currentTimeMillis())
      _key = "";
    _time = System.currentTimeMillis();

    _key += Character.toLowerCase(ch);
    ListModel model = this._fileList.getModel();
//    int count = model.getSize();
    int count = _listData.length;
    for (int k = 0; k < count; k++)
    {
//      String str = ((String)model.getElementAt(k)).toLowerCase();
      String str = _listData[k].toString().toLowerCase();
      if (str.startsWith(_key))
      {
        this._fileList.setSelectedIndex(k);
        this._fileList.ensureIndexIsVisible(k);
        break;
      }
    }
  }

// DEBUG
  public static void main(String[] args)
  {
    try
    {
      // Make sure the look and feel is set before any components are created.
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (ClassNotFoundException e) { /* do nothing */ }
    catch (InstantiationException e) { /* do nothing */ }
    catch (IllegalAccessException e) { /* do nothing */ }
    catch (UnsupportedLookAndFeelException e) { /* do nothing */ }

    String [] panels = {"JoeyPanel1", "DigbyPanel2", "_AndyPanel3", "Chad_Panel", "zoey" };
    OpenPanelDialog dlg = new OpenPanelDialog(null, "Open Panel Program", true, panels);
    dlg._startFromMain = true;
    int returnVal = dlg.showDialog();
    if (returnVal == JOptionPane.OK_OPTION)
      System.out.println("Selected panel " + dlg.getSelectedFile());
    else
      System.out.println("Canceled");
    System.exit(0);
  }
  private boolean _startFromMain = false;
// DEBUG
  private JPanel _rootPanel = new JPanel();
  private BorderLayout _rootPanelLayout = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _okCancelPanel = new JPanel();
  private JButton _okBtn = new JButton();
  private JButton _cancelBtn = new JButton();
  private BorderLayout _centerPanelLayout = new BorderLayout();
  private JPanel _lookInPanel = new JPanel();
  private JPanel _fileListPanel = new JPanel();
  private JPanel _selectionPanel = new JPanel();
  private JLabel _lookInLbl = new JLabel();
  private JComboBox _lookInCBox = new JComboBox(_lookIn);
  private BorderLayout fileListPanelLayout = new BorderLayout();
  private JLabel _filenameLbl = new JLabel();
  private JTextField _selectedFileTF = new JTextField();
  private JScrollPane _fileListScroll = new JScrollPane();
  private JList _fileList = new JList();
  private FlowLayout _flowLayout1 = new FlowLayout();
  private GridLayout _gridLayout2 = new GridLayout();

}