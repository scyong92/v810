package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class WorkImageBoardAction extends MenuAction
{
  public WorkImageBoardAction(Menu5DX parent)
  {
    super("Board Test", null, "Save images for board test", 'B', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._workImageAutoMI_actionPerformed(e);
  }
}