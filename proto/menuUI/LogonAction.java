package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class LogonAction extends MenuAction
{
  public LogonAction(Menu5DX parent)
  {
    super("Logon", null, "Logon as a new user", 'L', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
//    System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._logonMI_actionPerformed(e);
  }
}