package proto.menuUI;

import java.awt.event.ActionEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class StatusPanelHandlingAction extends MenuAction
{
  public StatusPanelHandlingAction(Menu5DX parent)
  {
    super("Status...", null, "Show panel handling status", 'S', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    System.out.println(this.getTooltip());
// DEBUG
    //this.getParent()._statusPanelHandlingMI_actionPerformed(e);
  }
}