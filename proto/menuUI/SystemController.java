package proto.menuUI;

import java.rmi.*;
import javax.swing.*;
import proto.menuUI.autotest.*;

import com.agilent.util.*;
import com.agilent.xRayTest.util.*;
import com.agilent.xRayTest.gui.*;
import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.business.server.*;
import com.agilent.xRayTest.business.panelInspection.*;
import com.agilent.xRayTest.business.panelDesc.*;

/**
 * Title: SystemController
 * Description: This class does the work of controlling the 5DX system and updating
 * the GUI.  It also holds the data for the 5dx interface.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author  Steve Anonson
 * @version 1.0
 */

public class SystemController implements AutoTestInt
{
// DEBUG
  private ImageIcon _boardIcon = new ImageIcon(proto.operatorUI.Operator5DX.class.getResource("board1.jpg"));
  private ImageIcon _noboardIcon = new ImageIcon(proto.operatorUI.Operator5DX.class.getResource("noboard.gif"));
// DEBUG
  private ControllerObserverInt _uiObserver = null;
  private InspectionObserverInt _inspectionObserver = null;
  private PanelQueueInt _panelQueue = null;

  private ServerAdapter _serverAdapter = null;
  private RemoteInspectionInt _machinePanelInspect = null;
  private RemotePanelDescInt _panelDescription = null;
  private RemoteDosShellInt _dosShell = null;

  private User5dx _currentUser = null;
  private String _currentPanelProgram = null;
  private String _currentPanelName = null;
  private String _currentBoardName = null;
  private int _currentBoardNumber = 0;
  private ImageIcon _currentPanelImage = null;

  private WorkerThread _dosThread = new WorkerThread("DOSThread");
  private WorkerThread _machineThread = new WorkerThread("MachineThread");
  private WorkerThread _autoTestThread = new WorkerThread("AutoTestThread");

  private String _dosCommand = null;
  private boolean _automaticTestMode = false;
  private boolean _paused = false;
  private boolean _stopped = false;

  /**
   * Constructor.
   */
  public SystemController(ControllerObserverInt userInterface, InspectionObserverInt inspectionObserver, PanelQueueInt panelQueue)
  {
    if (userInterface == null)
      throw new IllegalArgumentException("SystemController must have a user interface component.");

    _uiObserver = userInterface;
//    _uiObserver.setController(this);
    _inspectionObserver = inspectionObserver;
    _panelQueue = panelQueue;
    _panelQueue.setController(this);
    setAutomaticTestMode(true);  // start in manual test mode
    _autoTestThread.invokeLater( new Runnable()
    {
      public void run()
      {
//        SystemController.this.inspectionLoop();
        SystemController.this.demo();  // temporary dummy loop for demos.
      }
    });
  }

  /**
   * Set the server that the controller will talk to.
   *
   * @param   server - SeverAdapter to communicate with.
   */
  public void setServer( ServerAdapter server )
  {
    if (server == null)
      throw new IllegalArgumentException("SystemController must have a valid server.");

    _serverAdapter = server;
    // Save references to the interfaces used.
    try
    {
      _machinePanelInspect = _serverAdapter.getInspectionInt();
      _panelDescription = _serverAdapter.getPanelDescInt();
    }
    catch (RemoteException re)
    {
      /** @todo SLA Handle the remote exception */
      _uiObserver.reportException(re);
    }
  }

  /**
   * Enable / disable the auto test loop to keep testing panels from the panel
   * list until empty.
   *
   * @param   flag - true to automatically test next panel, false to pause between panels.
   */
  public void setAutomaticTestMode( boolean flag )
  {
    _automaticTestMode = flag;
    setPaused(!flag);  // pause if in manual test mode
  }

  /**
   * Return whether automatic test mode is active.
   */
  public boolean getAutomaticTestMode()
  {
    return _automaticTestMode;
  }

  /**
   * Load cad for a panel.
   *
   * @param   panelName - String containing the name of the panel.
   */
  public boolean loadCad(String panelName)
  {
    java.util.List warnings = new java.util.ArrayList();
    try
    {
      _panelDescription.loadPanelProgram(DatastoreMode.READ_ONLY, panelName, warnings);
    }
    catch (DatastoreException de)
    {
      /** @todo SLA Report load cad error. */
//      MessageDialog.reportException(_uiObserver, de);
      return false;
    }
    catch (com.agilent.xRayTest.hardware.HardwareException he)
    {
      /** @todo SLA Report load cad error. */
//      MessageDialog.reportException(_uiObserver, he);
      return false;
    }
    catch (RemoteException re)
    {
      /** @todo SLA Report load cad error. */
//      MessageDialog.reportException(this, re);
      return false;
    }

    if (warnings.size() > 0)
    {
      /** @todo SLA Report load cad warnings. */
    }
    return true;
  }

  /**
   * AutoTestInt method
   * Takes the new PanelEntry with only the serial number filled in and assigns
   * the correct panel program and image.  Then the panel is added to the panel queue.
   *
   * @param   panel - PanelEntry to add to the queue.
   */
  public void newPanel(PanelEntry panel)
  {
    if (panel == null)
      return;

    // Look up the panel program associated with the serial number.
    panel.setPanelProgram(_currentPanelProgram);

    // Get the panel image if available.
    panel.setImage(_currentPanelImage);

// DEBUG
    if (panel.getSerialNumber().equals("5"))
    {
      panel.setPanelProgram("Dilbert");
      panel.setImage(_noboardIcon);
    }
    else
    {
      panel.setPanelProgram("NEPCON");
      panel.setImage(_boardIcon);
    }
// DEBUG

    // Add the panel to the panel queue.
    _panelQueue.addPanel( panel );
  }

  /**
   * AutoTestInt method
   * Signals that the start button was pressed.
   */
  public void startTest()
  {
    setRunning();
  }

  /**
   * AutoTestInt method
   * Signals that the pause button was pressed.
   */
  public void pauseTest()
  {
    setPaused(true);
  }

  /**
   * AutoTestInt method
   * Signals that the stop button was pressed.
   */
  public void stopTest()
  {
    setStopped(true);
  }

  /**
   * AutoTestInt method
   *
   * @return true if not paused or stopped.
   */
  public boolean running()
  {
    return (!_stopped && !_paused);
  }

  /**
   * AutoTestInt method
   *
   * @return true if paused.
   */
  public boolean paused()
  {
    return _paused;
  }

  /**
   * AutoTestInt method
   *
   * @return true if stopped.
   */
  public boolean stopped()
  {
    return _stopped;
  }

  /**
   * Set the inspection stopped flag.
   */
  private void setStopped( boolean flag )
  {
    if (_stopped == flag)
      return;
    _stopped = flag;
    if (_stopped)
    {
      setPaused(false);
      _inspectionObserver.setStopped();
    }
  }

  /**
   * Set the inspection paused flag.
   */
  private void setPaused( boolean flag )
  {
    if (_paused == flag)
      return;
    _paused = flag;
    if (_paused && !_stopped)
      _inspectionObserver.setPaused();
  }

  /**
   * Clear the inspection stopped and paused flags.
   */
  private void setRunning()
  {
    setPaused(false);
    setStopped(false);
    _inspectionObserver.setRunning();
  }

  /**
   * This method handled the testing of panels from the list.  If in automatic
   * test mode, panels are removed from the list and inspected without user intervention.
   * In manual mode, the user must press the start button to inspect each panel.
   * This method is run on the auto test thread.
   */
  private void inspectionLoop()
  {
    do {
      // Wait in this loop until a panel is available and we aren't paused or stopped.
      while ( !_panelQueue.panelAvailable() || stopped() || paused())
      {
        try { Thread.sleep(2000); }
        catch (InterruptedException e) { }
      }

      // Take the next panel off the panel list.
      PanelEntry panel = _panelQueue.removePanel();
      if (panel != null)
      {
        if (!panel.getPanelProgram().equalsIgnoreCase(_currentPanelProgram))
        {
          // Load cad if different panel program.

          // Update the image if different panel program.
          _inspectionObserver.newPanel(panel);
        }

        // Initialize for a new inspection.
        _inspectionObserver.newInspection();

        // Load the panel.
        _inspectionObserver.loadPanel(panel);
        /** @todo Call system load panel method. */
        _inspectionObserver.panelLoaded();

        // Inspect the panel.
        /** @todo Call the system inspect panel method with observer. */

        // Save pass / fail data.

        // Unload the panel.
        _inspectionObserver.unloadPanel(panel);
        /** @todo Call system unload panel method. */
        _inspectionObserver.panelUnloaded();
      }

      if (getAutomaticTestMode() == false)
        setPaused(true);  // pause if in manual test mode
    } while (true);
  }

// DEBUG
  private void demo()
  {
    _inspectionObserver.newPanel(null);

    do {
      while ( !_panelQueue.panelAvailable() || stopped() || paused())
      {
        try { Thread.sleep(2000); }
        catch (InterruptedException e) { }
      }

      PanelEntry panel = _panelQueue.removePanel();
      if (panel != null)
      {
        // Load cad if different panel program.

        // Update the image if different panel program.
        _inspectionObserver.newPanel(panel);

        // Initialize for a new inspection.
        _inspectionObserver.newInspection();

        // Load the panel.
        _inspectionObserver.loadPanel(panel);
        try { Thread.sleep(5000); }
        catch (InterruptedException e) { }
        _inspectionObserver.panelLoaded();

        // Inspect the panel.
        String[] msgs = {
          "Checking alignment point 1\n",
          "Checking alignment point 2\n",
          "Checking alignment point 3\n",
          "\nChecking surface map point 72 of 72\n",
          "\nTesting view 154 of 155\n",
          "Testing view 155 of 155\n",
          "\nPins tested:  1726    Pins failed:  543\n",
          "\nAlignment:  00:13    Map:  00:24    Test: 1:26\n",
          "\nTesting Complete\n" };
        int[] delays = { 500, 200, 200, 200, 500, 1000, 1000, 200, 100 };

        _inspectionObserver.newInspection();
        for (int i = 0; i < msgs.length; i++)
        {
          try { Thread.sleep(delays[i]); }
          catch (InterruptedException e) { }
          if (stopped())
          {
            _inspectionObserver.inspectionMessage("\n\n Inspection Aborted\n");
            break;
          }
          _inspectionObserver.inspectionMessage(msgs[i]);
        }

        // Make up some pass / fail data.
        int t1 = (int)(Math.random() * 10);
        int t3 = (int)(Math.random() * 6);
        if (stopped())
          panel.setTestResult(PanelEntry.ABORTED);
        else if (t1 == t3)
          panel.setTestResult(PanelEntry.FAILED);
        else
          panel.setTestResult(PanelEntry.PASSED);

        // Unload the panel.
        _inspectionObserver.unloadPanel(panel);
        try { Thread.sleep(5000); }
        catch (InterruptedException e) { }
        _inspectionObserver.panelUnloaded();
      }

      if (getAutomaticTestMode() == false)
        setPaused(true);  // pause if in manual test mode
    } while (true);
  }
// DEBUG
}
