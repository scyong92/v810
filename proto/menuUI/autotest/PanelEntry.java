package proto.menuUI.autotest;

import javax.swing.*;

/**
 * Title: PanelEntry
 * Description: This class holds the esential information for the auto test function.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class PanelEntry
{
  public static final String PASSED = "Passed";
  public static final String FAILED = "Failed";
  public static final String ABORTED = "Aborted";

  private String _serialNumber = null;
  private String _panelProgram = null;
  private String _testResult = null;
  private ImageIcon _picture = null;

  /**
   * Constructor
   *
   * @param   serialNum - serail number string for the panel.
   * @param   program - name of the panel program associated with this panel.
   * @param   image - panel image or null if no image available.
   */
  public PanelEntry( String serialNum, String program, ImageIcon image )
  {
    _serialNumber = serialNum;
    _panelProgram = program;
    _picture = image;
  }

  /**
   * Constructor
   *
   * @param   serialNum - serail number string for the panel.
   */
  public PanelEntry( String num )
  {
    this(num, null, null);
  }

  // Access methods
  public String getSerialNumber() { return _serialNumber; }
  public String getPanelProgram() { return _panelProgram; }
  public String getTestResult()   { return _testResult; }
  public ImageIcon getImage()     { return _picture; }

  public void setSerialNumber( String serialNum ) { _serialNumber = serialNum; }
  public void setPanelProgram( String program)    { _panelProgram = program; }
  public void setTestResult(String results)       { _testResult = results; }
  public void setImage(ImageIcon image)           { _picture = image; }

  /**
   * Tests for equality by checking for matching serial numbers.
   */
  public boolean equals( PanelEntry rhs )
  {
    return (_serialNumber.equalsIgnoreCase(rhs._serialNumber));
  }

  /**
   * Return a string representation of the panel.  It includes serial number,
   * panel program and test results.
   */
  public String toString()
  {
    String str = "";
    if (_serialNumber != null)
      str += _serialNumber;
    if (_panelProgram != null)
      str += "    " + _panelProgram;
    if (_testResult != null)
      str += "    " + _testResult;
    return str;
  }
}