package proto.menuUI.autotest;

import java.awt.*;
import javax.swing.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class ImagePanel extends JPanel
{
  private ImageIcon _image = null;
  private ArrowAnimation _animatedArrows = null;

  public ImagePanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }

    // Set up the flashing arrows for loading a panel.
    this._animatedArrows = new ArrowAnimation(_leftArrowLbl, _rightArrowLbl);
  }

  void jbInit() throws Exception
  {
    _imageLbl.setHorizontalAlignment(SwingConstants.CENTER);
    _leftArrowLbl.setHorizontalAlignment(SwingConstants.CENTER);
    _rightArrowLbl.setHorizontalAlignment(SwingConstants.CENTER);

    _borderLayout1.setHgap(5);
    this.setLayout(_borderLayout1);
    this.add(_imageLbl, BorderLayout.CENTER);
    this.add(_leftArrowLbl, BorderLayout.WEST);
    this.add(_rightArrowLbl, BorderLayout.EAST);
  }

  /**
   * Set the panel image to be displayed.
   *
   * @param   image - ImageIcon to be displayed.
   */
  public void setImage( ImageIcon image )
  {
    _image = image;
    _imageLbl.setIcon(_image);
  }

  /**
   * Set the desired arrow direction.
   *
   * @param   direction - "up" or "down"
   */
  public void setArrowDirection( String direction )
  {
    _animatedArrows.setArrowDirection(direction);
  }

  /**
   * Starts the arrow animation.
   */
  public void startArrows()
  {
    _animatedArrows.start();  // start the animation
  }

  /**
   * Stop the arrow animation.
   */
  public void stopArrows()
  {
    _animatedArrows.stop();  // start the animation
  }

  private BorderLayout _borderLayout1 = new BorderLayout();
  private JLabel _imageLbl = new JLabel();
  private JLabel _leftArrowLbl = new JLabel();
  private JLabel _rightArrowLbl = new JLabel();
}