package proto.menuUI.autotest;

import javax.swing.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class PanelEntryList extends JList
{
  DefaultListModel _data = new DefaultListModel();

  /**
   * Constructor
   */
  public PanelEntryList()
  {
    this.setModel(_data);
  }

  /**
   * Test for an empty list.  Returns true if the list is empty.
   */
  public boolean empty()
  {
    return ((_data != null) ? (_data.size() == 0) : true);
  }

  /**
   * Return the number of entries on the list.
   *
   */
  public int count()
  {
    return ((_data != null) ? _data.size() : 0);
  }

  /**
   * Add a PanelEntry to the list.
   *
   * @param   item - PanelEntry to add to the list.
   * @return the PanelEntry passed in as a parameter.
   */
  public synchronized PanelEntry enqueue( PanelEntry item )
  {
    if (_data != null)
    {
      _data.addElement(item);
      ensureIndexIsVisible( _data.size() - 1 );
    }
    return item;
  }

  /**
   * Takes the top entry off the list and returns it.
   *
   * @return the top entry in the list.
   */
  public synchronized PanelEntry dequeue()
  {
    PanelEntry p = null;
    if (_data != null && _data.size() > 0)
    {
      p = (PanelEntry)_data.remove(0);
    }
    return p;
  }

  /**
   * Remove all entries in the list.
   */
  public synchronized void removeAll()
  {
    _data.clear();
  }

  /**
   * Remove the selected entry from the list.
   */
  public synchronized void removeSelected()
  {
    int selected = this.getSelectedIndex();
    if (selected >= 0)
      _data.removeElementAt(selected);
  }

  /**
   * Return the list entries as an Object array.
   */
  public Object[] toArray()
  {
    if (_data != null)
      return _data.toArray();
    return null;
  }
}
