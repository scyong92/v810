package proto.menuUI.autotest;

import javax.swing.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class ArrowAnimation implements Runnable
{
  private static final ImageIcon _downArrowIcon = new ImageIcon(proto.operatorUI.Operator5DX.class.getResource("dwnarrow.gif"));
  private static final ImageIcon _upArrowIcon = new ImageIcon(proto.operatorUI.Operator5DX.class.getResource("uparrow.gif"));
  private static final ImageIcon _blankArrowIcon = new ImageIcon(proto.operatorUI.Operator5DX.class.getResource("blankarrow.gif"));

  public static final String UP = "up";
  public static final String DOWN = "down";

  private boolean _running = false;
  private boolean _visible = false;
  private String _direction = UP;
  private Thread _arrowThread = null;
  private JLabel _leftArrow;
  private JLabel _rightArrow;

  public ArrowAnimation(JLabel leftArrow, JLabel rightArrow)
  {
    _leftArrow = leftArrow;
    _rightArrow = rightArrow;
    _leftArrow.setIcon(_blankArrowIcon);
    _rightArrow.setIcon(_blankArrowIcon);

    _arrowThread = new Thread( this );
    _arrowThread.start();
  }

  public void run()
  {
    while (true)
    {
      if (_running)
      {
        // Select the next icon to be displayed.
        ImageIcon icon = _blankArrowIcon;
        if (!_visible)
          icon = ((isDirectionDown()) ? _downArrowIcon : _upArrowIcon);
        _visible = !_visible;
        // Show the icon in the labels
        _leftArrow.setIcon(icon);
        _rightArrow.setIcon(icon);
      }
      try { Thread.sleep(500); } catch (Exception e) { }  // sleep for 0.5 seconds
    }
  }

  // Access methods
  private boolean isDirectionDown() { return (_direction == DOWN); }
  private boolean isDirectionUp()   { return (_direction == UP); }

  /**
   * Set the desired arrow direction.
   *
   * @param   direction - "up" or "down"
   */
  public void setArrowDirection( String direction )
  {
    if (direction.equalsIgnoreCase(DOWN))
      _direction = DOWN;
    else
      _direction = UP;
  }

  /**
   * Starts the animation with the arrows in the desired direction.
   */
  public void start()
  {
    _running = true;
  }

  /**
   * Stop the animation.
   */
  public void stop()
  {
    _running = false;
  }
}