package proto.menuUI.autotest;

import javax.swing.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class AutoTest implements AutoTestInt
{
  private AutoTestPanel _autoTestPanel = null;
  private boolean _paused = false;
  private boolean _stopped = false;
  private String _currentPanelProgram = null;
  private ImageIcon _currentPanelImage = null;

  /**
   * Constructor.
   */
  public AutoTest()
  {
    _autoTestPanel = new AutoTestPanel(this);
  }

  /**
   * Return the GUI part of the auto test.
   */
  public JPanel getAutoTestPanel()
  {
    return _autoTestPanel;
  }

  private void setStopped( boolean flag )
  {
    if (_stopped == flag)
      return;
    _stopped = flag;
    if (_stopped)
    {
      setPaused(false);
      _autoTestPanel.setStopped();
    }
  }

  private void setPaused( boolean flag )
  {
    if (_paused == flag)
      return;
    _paused = flag;
    if (_paused && !_stopped)
      _autoTestPanel.setPaused();
  }

  private void setRunning()
  {
    setPaused(false);
    setStopped(false);
    _autoTestPanel.setRunning();
  }

  /**
   * AutoTestInt method
   * Takes the new PanelEntry with only the serial number filled in and assigns
   * the correct panel program and image.  Then the panel is added to the panel queue.
   *
   * @param   panel - PanelEntry to add to the queue.
   */
  public void newPanel(PanelEntry panel)
  {
    if (panel == null)
      return;

    // Look up the panel program associated with the serial number.
    panel.setPanelProgram(_currentPanelProgram);

    // Get the panel image if available.
    panel.setImage(_currentPanelImage);

    // Add the panel to the panel queue.
    _autoTestPanel.addPanel( panel );
  }

  /**
   * AutoTestInt method
   * Signals that the start button was pressed.
   */
  public void startTest()
  {
    setRunning();
  }

  /**
   * AutoTestInt method
   * Signals that the pause button was pressed.
   */
  public void pauseTest()
  {
    setPaused(true);
  }

  /**
   * AutoTestInt method
   * Signals that the stop button was pressed.
   */
  public void stopTest()
  {
    setStopped(true);
  }

  /**
   * AutoTestInt method
   *
   * @return true if not paused or stopped.
   */
  public boolean running()
  {
    return (!_stopped && !_paused);
  }

  /**
   * AutoTestInt method
   *
   * @return true if paused.
   */
  public boolean paused()
  {
    return _paused;
  }

  /**
   * AutoTestInt method
   *
   * @return true if stopped.
   */
  public boolean stopped()
  {
    return _stopped;
  }

// DEBUG
  private void demo()
  {
    _autoTestPanel.setPanelImage(null);

    do {
      while ( !_autoTestPanel.panelAvailable() || stopped() || paused())
      {
        try { Thread.sleep(2000); }
        catch (InterruptedException e) { }
      }

      PanelEntry panel = _autoTestPanel.removePanel();
      if (panel != null)
      {
        // Load cad if different panel program.

        // Update the image if different panel program.
        _autoTestPanel.setPanelImage(panel.getImage());

        // Initialize for a new inspection.
        _autoTestPanel.newInspection();

        // Load the panel.
        _autoTestPanel.loadPanel(panel);
        try { Thread.sleep(5000); }
        catch (InterruptedException e) { }
        _autoTestPanel.panelLoaded();

        // Inspect the panel.
        String[] msgs = {
          "Checking alignment point 1\n",
          "Checking alignment point 2\n",
          "Checking alignment point 3\n",
          "\nChecking surface map point 72 of 72\n",
          "\nTesting view 154 of 155\n",
          "Testing view 155 of 155\n",
          "\nPins tested:  1726    Pins failed:  543\n",
          "\nAlignment:  00:13    Map:  00:24    Test: 1:26\n",
          "\nTesting Complete\n" };
        int[] delays = { 500, 200, 200, 200, 500, 1000, 1000, 200, 100 };

        _autoTestPanel.newInspection();
        for (int i = 0; i < msgs.length; i++)
        {
          try { Thread.sleep(delays[i]); }
          catch (InterruptedException e) { }
          if (stopped())
          {
            _autoTestPanel.inspectionMessage("\n\n Inspection Aborted\n");
            break;
          }
          _autoTestPanel.inspectionMessage(msgs[i]);
        }

        // Make up some pass / fail data.
        int t1 = (int)(Math.random() * 10);
        int t3 = (int)(Math.random() * 6);
        if (stopped())
          panel.setTestResult(PanelEntry.ABORTED);
        else if (t1 == t3)
          panel.setTestResult(PanelEntry.FAILED);
        else
          panel.setTestResult(PanelEntry.PASSED);

        // Unload the panel.
        _autoTestPanel.unloadPanel(panel);
        try { Thread.sleep(5000); }
        catch (InterruptedException e) { }
        _autoTestPanel.panelUnloaded();
      }
    } while (true);
  }

  public static void main(String[] args)
  {
    try
    {
      // Make sure the look and feel is set before any components are created.
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (ClassNotFoundException e) { /* do nothing */ }
    catch (InstantiationException e) { /* do nothing */ }
    catch (IllegalAccessException e) { /* do nothing */ }
    catch (UnsupportedLookAndFeelException e) { /* do nothing */ }

    JFrame frame = new JFrame("Auto Test");
    AutoTest auto = new AutoTest();
    frame.getContentPane().add(auto.getAutoTestPanel());
    // Set the Window 'X' button to exit the application.
    frame.addWindowListener( new java.awt.event.WindowAdapter()
    { public void windowClosing(java.awt.event.WindowEvent e) { System.exit(0); } });
    frame.pack();
    frame.setSize(700,600);
    com.agilent.guiUtil.SwingUtils.setScreenPosition(frame, 0.5, 0.25);
    frame.show();
    auto.demo();
  }
// DEBUG
}
