package proto.menuUI.autotest;

import java.awt.*;
import java.rmi.*;
import javax.swing.*;
import com.agilent.util.*;
import proto.menuUI.InspectionObserverInt;
import proto.menuUI.PanelQueueInt;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class AutoTestPanel extends JPanel implements InspectionObserverInt, PanelQueueInt
{
  private static final String _TESTING_STOPPED = "STOPPED";
  private static final String _TESTING_PAUSED = "PAUSED";
  private static final String _TESTING_STARTED = "   ";
  private ImageIcon _noboardIcon = new ImageIcon(proto.operatorUI.Operator5DX.class.getResource("noboard.gif"));
  private ImagePanel _imagePanel = null;
  private InspectionPanel _inspectionPanel = null;
  private SerialNumberPanel _serialNumberPanel = null;
  private AutoTestInt _controller = null;

  public AutoTestPanel()
  {
    this(null);
  }

  public AutoTestPanel(AutoTestInt controller)
  {
    _controller = controller;

    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception
  {
    // Create the sub-panels.
    _imagePanel = new ImagePanel();
    _inspectionPanel = new InspectionPanel();
    _serialNumberPanel = new SerialNumberPanel(_controller);

    _westPanel.setLayout(_westPanelLayout);
    _westPanel.add(_serialNumberPanel, BorderLayout.NORTH);
    _westPanel.add(_imagePanel, BorderLayout.SOUTH);

    _centerPanel.setLayout(_centerPanelLayout);
    _centerPanel.add(_inspectionPanel, BorderLayout.CENTER);

    this.setLayout(_borderLayout1);
    this.add(_centerPanel, BorderLayout.CENTER);
    this.add(_westPanel, BorderLayout.WEST);
  }

  /**
   * Set the controller class on this and children.
   *
   * @param   controller - AutoTestInt instance to use as the controller.
   */
  public void setController(AutoTestInt controller)
  {
    _controller = controller;
    _serialNumberPanel.setAutoTestController(controller);
  }

  /**
   * Update the automatic test loop status to show it stopped.
   */
  public void setStopped()
  {
    _serialNumberPanel.setStatus(_TESTING_STOPPED);
  }

  /**
   * Update the automatic test loop status to show it paused.
   */
  public void setPaused()
  {
    _serialNumberPanel.setStatus(_TESTING_PAUSED);
  }

  /**
   * Update the automatic test loop status to show it running.
   */
  public void setRunning()
  {
    _serialNumberPanel.setStatus(_TESTING_STARTED);
  }

  /**
   * Update the image for the panel.
   *
   * @param   picture - ImageIcon for the panel.
   */
  public void setPanelImage( ImageIcon picture )
  {
    if (picture == null)
      _imagePanel.setImage(_noboardIcon);
    else
      _imagePanel.setImage(picture);
  }

  /**
   * InspectionObserverInt method.
   * Signal the user interface that a new panel is being processed.
   *
   * @param   panel - PanelEntry for the new panel.
   */
  public void newPanel( PanelEntry panel )
  {
    if (panel == null)
    {
      this.setPanelImage(null);
      return;
    }
    /** @todo SLA check to see if panel program is changing before update */
    this.setPanelImage(panel.getImage());
  }

  /**
   * Start the load panel animation.
   *
   * @param   panel - PanelEntry for the panel to be loaded.
   */
  public void loadPanel(PanelEntry panel)
  {
    _imagePanel.setArrowDirection(ArrowAnimation.UP);
    _imagePanel.startArrows();
    _inspectionPanel.setMode("Loading");
    _inspectionPanel.setPanelName(panel);
  }

  /**
   * Stop the load panel animation.
   */
  public void panelLoaded()
  {
    _imagePanel.stopArrows();
    _inspectionPanel.setMode("Inspecting");
  }

  /**
   * Start the unload panel animation.
   *
   * @param   panel - PanelEntry for the panel to be unloaded.
   */
  public void unloadPanel(PanelEntry panel)
  {
    _imagePanel.setArrowDirection(ArrowAnimation.DOWN);
    _imagePanel.startArrows();
    _inspectionPanel.setMode("Unloading");
    _inspectionPanel.setPanelName(panel);
  }

  /**
   * Stop the unload panel animation.
   */
  public void panelUnloaded()
  {
    _imagePanel.stopArrows();
//    _inspectionPanel.setPanelName(new PanelEntry(""));
  }

  /**
   * Initialize for a new inspection
   */
  public void newInspection()
  {
    _inspectionPanel.setMode("Inspecting");
    _inspectionPanel.clearMessages();
  }

  /**
   * Send a message to the test messages.
   *
   * @param   message - String to add to the message area.
   */
  public void inspectionMessage(String message)
  {
    _inspectionPanel.message(message);
  }

  /**
   * Add a panel to the panel queue.
   *
   * @param   panel - PanelEntry to add to the list.
   */
  public void addPanel(PanelEntry panel)
  {
    _serialNumberPanel.getPanelList().enqueue(panel);
  }

  /**
   * Remove a panel from the panel queue.
   *
   * @return the top entry from the list.  Null if empty.
   */
  public PanelEntry removePanel()
  {
    if (_serialNumberPanel.getPanelList().empty())
      return null;
    return _serialNumberPanel.getPanelList().dequeue();
  }

  /**
   * Check for a panel on the list.
   *
   * @return the top entry from the list.  Null if empty.
   */
  public boolean panelAvailable()
  {
    return !_serialNumberPanel.getPanelList().empty();
  }

  /**
   * InspectionObserverInt method.
   * Called by the observed inspection class to report progress.
   *
   * @param   ro - Remote observable instance calling this method.
   * @param   arg - Optional argument.
   */
  public void update(RemoteObservableInt ro, Object arg) throws RemoteException
  {
    if (arg == null)
      return;
    String message = arg.toString();
    this.inspectionMessage(message);
  }

  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _westPanel = new JPanel();
  private BorderLayout _centerPanelLayout = new BorderLayout();
  private BorderLayout _westPanelLayout = new BorderLayout();
}
