package proto.menuUI.autotest;

import java.awt.*;
import java.rmi.RemoteException;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.*;
import com.agilent.util.*;

/**
 * Title: InspectionPanel
 * Description: This class implements the part of the auto test GUI that displays
 * the current panel serial number, test mode (load, inspect, unload) and the
 * inspection messages.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class InspectionPanel extends JPanel implements RemoteObserverInt
{
  private String _mode = "";
  private String _panel = "";
  private StringBuffer _buf = new StringBuffer();

  /**
   * Constructor
   */
  public InspectionPanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * Creates the components.
   */
  void jbInit() throws Exception
  {
    _inspectionLbl.setText("Inspecting");
    _inspectionTF.setBackground(SystemColor.info);
    _inspectionTF.setToolTipText("");
    _inspectionTF.setEditable(false);
    _inspectionTF.setMargin(new Insets(0, 5, 0, 0));
    _northPanelLayout.setHgap(2);
    _northPanel.setLayout(_northPanelLayout);
    _northPanel.add(_inspectionLbl, BorderLayout.WEST);
    _northPanel.add(_inspectionTF, BorderLayout.CENTER);

    _messageTA.setEditable(false);
    _messageScrollPane.getViewport().add(_messageTA, null);

    this.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Inspection Messages "),BorderFactory.createEmptyBorder(5,5,5,5)));
    _borderLayout1.setVgap(3);
    this.setLayout(_borderLayout1);
    this.add(_northPanel, BorderLayout.NORTH);
    this.add(_messageScrollPane, BorderLayout.CENTER);
  }

  /**
   * Set the inspection mode (load, inspect, unload).
   *
   * @param   mode - String to display as the mode.
   */
  public void setMode( String mode )
  {
    if (mode == null)
      return;
    _mode = mode;
    // Update the text area on the Swing thread.
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        _inspectionLbl.setText(_mode);
      }
    });
  }

  /**
   * Set the current panel serial number / name.
   *
   * @param   panel - PanelEntry instance for the panel to be inspected.
   */
  public void setPanelName( PanelEntry panel )
  {
    if (panel == null)
      return;
    _panel = panel.toString();
    // Update the text area on the Swing thread.
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        _inspectionTF.setText(_panel);
      }
    });
  }

  /**
   * Clear the previous messages for the message text area.
   */
  public void clearMessages()
  {
    this.message(null, true);
  }

  /**
   * Append a message to the text area.
   *
   * @param   txt - String to add to the end of the messages.
   * @param   clearFirst - Removes previous messages if true.
   */
  public synchronized void message(String txt, boolean clearFirst)
  {
    if (clearFirst)
      _buf.delete(0, _buf.length());  // delete previous messages

    // Add the new message with a new line on the end.
    if (txt != null)
    {
      _buf.append(txt);
    }

    // Update the text area on the Swing thread.
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        _messageTA.setText(_buf.toString());
      }
    });
  }

  /**
   * Adds a message to the text area.
   *
   * @param   txt - String message to be displayed.
   */
  public synchronized void message( String txt )
  {
    this.message(txt, false);
  }

  /**
   * RemoteObserverInt method.
   * Takes the Object passed in, converts it to a String and adds the string to
   * the message text area.
   */
  public void update(RemoteObservableInt ro, Object arg) throws RemoteException
  {
    if (arg == null)
      return;
    String str = arg.toString();
    this.message(str, false);
  }

  private BorderLayout _borderLayout1 = new BorderLayout();
  private JScrollPane _messageScrollPane = new JScrollPane();
  private JPanel _northPanel = new JPanel();
  private JTextField _inspectionTF = new JTextField();
  private JLabel _inspectionLbl = new JLabel();
  private BorderLayout _northPanelLayout = new BorderLayout();
  private JTextArea _messageTA = new JTextArea();
}