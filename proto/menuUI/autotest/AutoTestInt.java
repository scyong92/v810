package proto.menuUI.autotest;

/**
 * Title: AutoTestInt
 * Description: This interface defines the methods called by the auto test GUI
 * to pass information to the controller.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public interface AutoTestInt
{
  public void newPanel(PanelEntry panel);
  public void startTest();
  public void pauseTest();
  public void stopTest();
  public boolean running();
  public boolean paused();
  public boolean stopped();
}