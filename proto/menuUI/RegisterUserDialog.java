package proto.menuUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import proto.menuUI.tree.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class RegisterUserDialog extends JDialog
{
// DEBUG
    static String [] users = { " ", "Deb", "Chad", "Shawna", "Trevor", "Stesha" };
// DEBUG
  private int _status = JOptionPane.CANCEL_OPTION;

  public RegisterUserDialog(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);

    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }

    this.addWindowListener( new WindowAdapter()
    {
      public void windowClosing(WindowEvent e)
      {
        exitDialog();
      }
      public void windowOpened(WindowEvent e)
      {
      }
    });
  }

  private RegisterUserDialog()
  {
    this(null, "Configure User", true);
  }

  void jbInit() throws Exception
  {
    _okBtn.setText("OK");
    _okBtn.setFont(new java.awt.Font("Dialog", 1, 12));
    _okBtn.setMnemonic('O');
    _okBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _okBtn_actionPerformed(e);
      }
    });
    _cancelBtn.setText("Cancel");
    _cancelBtn.setMnemonic('C');
    _cancelBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _cancelBtn_actionPerformed(e);
      }
    });
    _applyBtn.setEnabled(false);
    _applyBtn.setMnemonic('A');
    _applyBtn.setText("Apply");
    _gridLayout2.setHgap(5);
    _okCancelPanel.setLayout(_gridLayout2 );
    _centerSplitPane.setDividerSize(10);
    _permissionTree.setEditable(true);
    _okCancelPanel.add(_okBtn, null);
    _okCancelPanel.add(_cancelBtn, null);
    _okCancelPanel.add(_applyBtn, null);
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,5,5,5));
    _flowLayout1.setAlignment(FlowLayout.RIGHT);
    _buttonPanel.setLayout(_flowLayout1);
    _buttonPanel.add(_okCancelPanel, null);

    _deleteUserBtn.setText("Delete User");
    _changePasswordBtn.setText("Change Password");
    _newGroupBtn.setText("New Group");
    _newUserBtn.setText("New User");
    gridLayout1.setRows(4);
    gridLayout1.setColumns(1);
    gridLayout1.setVgap(5);
    _controlPanel.setLayout(gridLayout1);
    _controlPanel.setBorder(BorderFactory.createEmptyBorder(10,5,5,5));
    _controlPanel.add(_newUserBtn, null);
    _controlPanel.add(_newGroupBtn, null);
    _controlPanel.add(_deleteUserBtn, null);
    _controlPanel.add(_changePasswordBtn, null);
    _userTree.setModel(this.getUserTreeModel());
    jScrollPane2.getViewport().add(_userTree, null);
    _leftPanel.setLayout(borderLayout3);
//    _leftPanel.add(_controlPanel, BorderLayout.SOUTH);
    _leftPanel.add(jScrollPane2, BorderLayout.CENTER);

    JCheckBox checkBox = new JCheckBox();
    TreeCellEditor treeEditor = new DefaultCellEditor( checkBox );
//    TreeCellEditor treeEditor = new DefaultCellEditor( new JTextField() );
    _permissionTree.setCellEditor(treeEditor);
    _permissionTree.setModel(this.getPermissionTreeModel());
    jScrollPane1.getViewport().add(_permissionTree, null);
    _rightPanel.setLayout(borderLayout2);
    _rightPanel.add(jScrollPane1, BorderLayout.CENTER);

    _centerSplitPane.add(_leftPanel, JSplitPane.LEFT);
    _centerSplitPane.add(_rightPanel, JSplitPane.RIGHT);
    _centerPanel.setLayout(borderLayout1);
    _centerPanel.add(_centerSplitPane, BorderLayout.CENTER);

    _panel1.setLayout(_borderLayout1);
    _panel1.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
    _panel1.add(_centerPanel, BorderLayout.CENTER);
    _panel1.add(_buttonPanel, BorderLayout.SOUTH);
    getContentPane().add(_panel1);
  }

  protected TreeModel getPermissionTreeModel()
  {
    DefaultMutableTreeNode      root = new DefaultMutableTreeNode("Permissions");
    DefaultMutableTreeNode      parent;

    parent = new DefaultMutableTreeNode("File");
    root.add(parent);
    parent.add(new DefaultMutableTreeNode("Open Panel Program..."));
    parent.add(new DefaultMutableTreeNode("Logon"));
    parent.add(new DefaultMutableTreeNode("Logout"));
    parent.add(new DefaultMutableTreeNode("Change Password"));
    parent.add(new DefaultMutableTreeNode("Exit"));

    parent = new DefaultMutableTreeNode("System");
    root.add(parent);
    parent.add(new DefaultMutableTreeNode("Startup"));
    DefaultMutableTreeNode subParent = new DefaultMutableTreeNode("Shutdown");
    subParent.add(new DefaultMutableTreeNode("Short Term"));
    subParent.add(new DefaultMutableTreeNode("Long Term"));
    subParent = new DefaultMutableTreeNode("Panel Handling");
    subParent.add(new DefaultMutableTreeNode("Home Rails"));
    subParent.add(new DefaultMutableTreeNode("Reset"));
    subParent.add(new DefaultMutableTreeNode("Status"));
    parent.add(new DefaultMutableTreeNode("Run Selftest"));
    parent.add(new DefaultMutableTreeNode("Reset 5DX"));
    parent.add(new DefaultMutableTreeNode("Reset IAS"));
    parent.add(new DefaultMutableTreeNode("5DX Status..."));

    parent = new DefaultMutableTreeNode("Panel");
    root.add(parent);
    parent.add(new DefaultMutableTreeNode("Load"));
    parent.add(new DefaultMutableTreeNode("Unload"));
    parent.add(new DefaultMutableTreeNode("Align and Map"));
    subParent = new DefaultMutableTreeNode("Align");
    subParent.add(new DefaultMutableTreeNode("Automatic"));
    subParent.add(new DefaultMutableTreeNode("Manual"));
    subParent.add(new DefaultMutableTreeNode("Display Matrix"));
    subParent = new DefaultMutableTreeNode("Map");
    subParent.add(new DefaultMutableTreeNode("Automatic"));
    subParent.add(new DefaultMutableTreeNode("Manual"));
    subParent.add(new DefaultMutableTreeNode("Check Surface Map"));
    subParent.add(new DefaultMutableTreeNode("Verify Laser Repeatability"));

    parent = new DefaultMutableTreeNode("Programming");
    root.add(parent);
    parent.add(new DefaultMutableTreeNode("Algorithm Tuner"));
    parent.add(new DefaultMutableTreeNode("CADLink"));
    parent.add(new DefaultMutableTreeNode("CheckCAD"));
    parent.add(new DefaultMutableTreeNode("Setup Map Points"));
    parent.add(new DefaultMutableTreeNode("Adjust View Focus"));
    parent.add(new DefaultMutableTreeNode("Setup X-out Detection"));
    parent.add(new DefaultMutableTreeNode("Review Defects"));
    parent.add(new DefaultMutableTreeNode("Review Measurements"));
    parent.add(new DefaultMutableTreeNode("Defect Reporter"));

    parent = new DefaultMutableTreeNode("Tools");
    root.add(parent);
    parent.add(new DefaultMutableTreeNode("Collect Workstation Images"));
    parent.add(new DefaultMutableTreeNode("Collect Diagnostic Images"));
    parent.add(new DefaultMutableTreeNode("Collect Repair Images"));
    parent.add(new DefaultMutableTreeNode("Service Menu"));
    parent.add(new DefaultMutableTreeNode("Toolbox"));
    parent.add(new DefaultMutableTreeNode("Options..."));

    parent = new DefaultMutableTreeNode("Help");
    root.add(parent);
    parent.add(new DefaultMutableTreeNode("Contents"));
    parent.add(new DefaultMutableTreeNode("About"));

    parent = new DefaultMutableTreeNode("Test");
    root.add(parent);
    parent.add(new DefaultMutableTreeNode("Automatic"));
    parent.add(new DefaultMutableTreeNode("Panel"));
    return new DefaultTreeModel(root);
  }

  protected TreeModel getUserTreeModel()
  {
    DefaultMutableTreeNode      root = new DefaultMutableTreeNode("Users");
    DefaultMutableTreeNode      parent;

    parent = new DefaultMutableTreeNode("Administrator");
    root.add(parent);

    parent = new DefaultMutableTreeNode("Supervisor");
    root.add(parent);
    parent.add(new DefaultMutableTreeNode("Bill"));
    parent.add(new DefaultMutableTreeNode("Joe"));

    parent = new DefaultMutableTreeNode("Operator");
    root.add(parent);
    parent.add(new DefaultMutableTreeNode("Frank"));
    parent.add(new DefaultMutableTreeNode("Biff"));

    return new DefaultTreeModel(root);
  }
/*
  protected TreeModel getPermissionTreeModel()
  {
    DefaultMutableTreeNode      root = new DefaultMutableTreeNode("Permissions");
    DefaultMutableTreeNode      parent;

    parent = new DefaultMutableTreeNode("File");
    root.add(parent);
    parent.add(new DefaultMutableTreeNode("Open Panel Program..."));
    parent.add(new DefaultMutableTreeNode("Logon"));
    parent.add(new DefaultMutableTreeNode("Logout"));
    parent.add(new DefaultMutableTreeNode("Change Password"));
    parent.add(new DefaultMutableTreeNode("Exit"));

    parent = new DefaultMutableTreeNode("System");
    root.add(parent);
    parent.add(new DefaultMutableTreeNode("Startup"));
    DefaultMutableTreeNode subParent = new DefaultMutableTreeNode("Shutdown");
    subParent.add(new DefaultMutableTreeNode("Short Term"));
    subParent.add(new DefaultMutableTreeNode("Long Term"));
    subParent = new DefaultMutableTreeNode("Panel Handling");
    subParent.add(new DefaultMutableTreeNode("Home Rails"));
    subParent.add(new DefaultMutableTreeNode("Reset"));
    subParent.add(new DefaultMutableTreeNode("Status"));
    parent.add(new DefaultMutableTreeNode("Run Selftest"));
    parent.add(new DefaultMutableTreeNode("Reset 5DX"));
    parent.add(new DefaultMutableTreeNode("Reset IAS"));
    parent.add(new DefaultMutableTreeNode("5DX Status..."));

    parent = new DefaultMutableTreeNode("Panel");
    root.add(parent);
    parent.add(new DefaultMutableTreeNode("Load"));
    parent.add(new DefaultMutableTreeNode("Unload"));
    parent.add(new DefaultMutableTreeNode("Align and Map"));
    subParent = new DefaultMutableTreeNode("Align");
    subParent.add(new DefaultMutableTreeNode("Automatic"));
    subParent.add(new DefaultMutableTreeNode("Manual"));
    subParent.add(new DefaultMutableTreeNode("Display Matrix"));
    subParent = new DefaultMutableTreeNode("Map");
    subParent.add(new DefaultMutableTreeNode("Automatic"));
    subParent.add(new DefaultMutableTreeNode("Manual"));
    subParent.add(new DefaultMutableTreeNode("Check Surface Map"));
    subParent.add(new DefaultMutableTreeNode("Verify Laser Repeatability"));

    parent = new DefaultMutableTreeNode("Programming");
    root.add(parent);
    parent.add(new DefaultMutableTreeNode("Algorithm Tuner"));
    parent.add(new DefaultMutableTreeNode("CADLink"));
    parent.add(new DefaultMutableTreeNode("CheckCAD"));
    parent.add(new DefaultMutableTreeNode("Setup Map Points"));
    parent.add(new DefaultMutableTreeNode("Adjust View Focus"));
    parent.add(new DefaultMutableTreeNode("Setup X-out Detection"));
    parent.add(new DefaultMutableTreeNode("Review Defects"));
    parent.add(new DefaultMutableTreeNode("Review Measurements"));
    parent.add(new DefaultMutableTreeNode("Defect Reporter"));

    parent = new DefaultMutableTreeNode("Tools");
    root.add(parent);
    parent.add(new DefaultMutableTreeNode("Collect Workstation Images"));
    parent.add(new DefaultMutableTreeNode("Collect Diagnostic Images"));
    parent.add(new DefaultMutableTreeNode("Collect Repair Images"));
    parent.add(new DefaultMutableTreeNode("Service Menu"));
    parent.add(new DefaultMutableTreeNode("Toolbox"));
    parent.add(new DefaultMutableTreeNode("Options..."));

    parent = new DefaultMutableTreeNode("Help");
    root.add(parent);
    parent.add(new DefaultMutableTreeNode("Contents"));
    parent.add(new DefaultMutableTreeNode("About"));

    parent = new DefaultMutableTreeNode("Test");
    root.add(parent);
    parent.add(new DefaultMutableTreeNode("Automatic"));
    parent.add(new DefaultMutableTreeNode("Panel"));
    return new DefaultTreeModel(root);
  }
*/
  /**
   * Hide the dialog unless started from main method, then do System exit.
   */
  private void exitDialog()
  {
    this.hide();
    if (_startFromMain)
      System.exit(0);
  }

  public int showDialog()
  {
    this.show();
    return _status;
  }

  /**
   * Callback method for the OK button.
   */
  void _okBtn_actionPerformed(ActionEvent e)
  {
    _status = JOptionPane.OK_OPTION;
    exitDialog();
  }

  /**
   * Callback method for the Cancel button.
   */
  void _cancelBtn_actionPerformed(ActionEvent e)
  {
    exitDialog();
  }

// DEBUG
  public static void main(String[] args)
  {
    try
    {
      // Make sure the look and feel is set before any components are created.
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (ClassNotFoundException e) { /* do nothing */ }
    catch (InstantiationException e) { /* do nothing */ }
    catch (IllegalAccessException e) { /* do nothing */ }
    catch (UnsupportedLookAndFeelException e) { /* do nothing */ }

//    RegisterUserDialog dlg = new RegisterUserDialog(null, "Configure User", true);
    RegisterUserDialog dlg = new RegisterUserDialog();
    dlg._startFromMain = true;
    com.agilent.guiUtil.SwingUtils.setScreenPosition(dlg,0.5,0.25);
    int returnVal = dlg.showDialog();
    if (returnVal == JOptionPane.OK_OPTION)
    {
      System.out.println("OK");
    }
    else
      System.out.println("Canceled");
  }
  private boolean _startFromMain = false;
// DEBUG
  private JPanel _panel1 = new JPanel();
  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _okCancelPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JButton _okBtn = new JButton();
  private JButton _cancelBtn = new JButton();
  private GridLayout _gridLayout2 = new GridLayout();
  private FlowLayout _flowLayout1 = new FlowLayout();
  private JPanel _centerPanel = new JPanel();
  private JSplitPane _centerSplitPane = new JSplitPane();
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel _leftPanel = new JPanel();
  private JPanel _rightPanel = new JPanel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private BorderLayout borderLayout3 = new BorderLayout();
  private JScrollPane jScrollPane1 = new JScrollPane();
  private JTree _permissionTree = new JTree();
  private JScrollPane jScrollPane2 = new JScrollPane();
  private JPanel _controlPanel = new JPanel();
  private JTree _userTree = new JTree();
  private JButton _deleteUserBtn = new JButton();
  private JButton _changePasswordBtn = new JButton();
  private JButton _newGroupBtn = new JButton();
  private JButton _newUserBtn = new JButton();
  private GridLayout gridLayout1 = new GridLayout();
  private JButton _applyBtn = new JButton();
}