package proto.menuUI;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import com.agilent.guiUtil.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author
 * @version 1.0
 */

public class SamplingModeOptionPane extends JPanel
{
  public static final int PANEL_SAMPLING_MODE = 1;
  public static final int COMPONENT_SAMPLING_MODE = 2;
  public static final int PANEL_BASED_SAMPLING = 3;
  public static final int BOARD_BASED_SAMPLING = 4;

  public SamplingModeOptionPane()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    _componentModeRB.setText("Component");
    _componentModeRB.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        _componentModeRB_stateChanged(e);
      }
    });
    _componentModeRB.setEnabled(false);
    _panelModeRB.setText("Panel");
    _panelModeRB.setSelected(true);
    _panelModeRB.setEnabled(false);
    _buttonGroup1.add(_panelModeRB);
    _buttonGroup1.add(_componentModeRB);
    _enableSamplingCkB.setText("Enable sampling");
    _enableSamplingCkB.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        _enableSamplingCkB_stateChanged(e);
      }
    });
    _modePanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Mode "));
    _panelBasedRB.setEnabled(false);
    _panelBasedRB.setSelected(true);
    _panelBasedRB.setText("Panel-based");
    _boardBasedRB.setEnabled(false);
    _boardBasedRB.setText("Board-based");
    _buttonGroup2.add(_panelBasedRB);
    _buttonGroup2.add(_boardBasedRB);
    _sampleLbl.setEnabled(false);
    _sampleLbl.setText("Sample");
    _percenSpinBox.setEnabled(false);
    _percentLbl.setEnabled(false);
    _percentLbl.setText("%");

    _enablePanel.add(_enableSamplingCkB, null);

    _modePanel.setLayout( new FlowLayout(FlowLayout.LEFT) );
    _modePanel.add(_panelModeRB, null);
    _modePanel.add(_componentModeRB, null);

    _border1 = BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134));
    _titledBorder1 = new TitledBorder(_border1," Increment ");
    _incrementPanel.setBorder(_titledBorder1);
    _incrementPanel.add(_panelBasedRB, null);
    _incrementPanel.add(_boardBasedRB, null);

    _border2 = BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134));
    _titledBorder2 = new TitledBorder(_border2," Percentage ");
    _percenPanel.setBorder(_titledBorder2);
    _percenPanel.setLayout( new FlowLayout(FlowLayout.LEFT) );
    _percenPanel.add(_sampleLbl, null);
    _percenPanel.add(_percenSpinBox, null);
    _percenPanel.add(_percentLbl, null);

    _dataPanel.setBorder(BorderFactory.createEmptyBorder(0,15,0,0));
    _dataPanel.setLayout(_verticalFlowLayout2);
    _dataPanel.add(_modePanel, null);
    _dataPanel.add(_incrementPanel, null);
    _dataPanel.add(_percenPanel, null);

    _centerPanel.setLayout(_verticalFlowLayout1);
    _centerPanel.add(_enablePanel, null);
    _centerPanel.add(_dataPanel, null);

    this.setBorder(BorderFactory.createEmptyBorder(10,0,5,0));
    this.setLayout(_borderLayout1);
    this.add(_centerPanel, BorderLayout.CENTER);
  }

  public boolean isSamplingEnabled()  { return this._enableSamplingCkB.isSelected(); }
  public int getSamplingMode()
  {
    if (this._componentModeRB.isSelected())
      return COMPONENT_SAMPLING_MODE;
    else
      return PANEL_SAMPLING_MODE;
  }
  public int getIncrementMode()
  {
    if (this._boardBasedRB.isSelected())
      return BOARD_BASED_SAMPLING;
    else
      return PANEL_BASED_SAMPLING;
  }
  public int getSamplingPercentage()  { return this._percenSpinBox.getValue(); }

  public void setSamplingEnabled(boolean b)  { this._enableSamplingCkB.setSelected(b); }
  public void setSamplingMode(int mode)
  {
    if (mode == COMPONENT_SAMPLING_MODE)
      this._componentModeRB.setSelected(true);
    else
      this._panelModeRB.setSelected(true);
  }
  public void setIncrementMode(int mode)
  {
    if (mode == BOARD_BASED_SAMPLING)
      this._boardBasedRB.setSelected(true);
    else
      this._panelBasedRB.setSelected(true);
  }
  public void setSamplingPercentage(int percent)  { this._percenSpinBox.setValue(percent); }

  /**
   * Stete change callback for the enable sampling check box.  Enable/disable
   * the rest of the components.
   */
  void _enableSamplingCkB_stateChanged(ChangeEvent e)
  {
    boolean enabled = this._enableSamplingCkB.isSelected();
    _panelModeRB.setEnabled(enabled);
    _componentModeRB.setEnabled(enabled);
    if (_componentModeRB.isSelected())
    {
      _panelBasedRB.setEnabled(enabled);
      _boardBasedRB.setEnabled(enabled);
    }
    _sampleLbl.setEnabled(enabled);
    _percenSpinBox.setEnabled(enabled);
    _percentLbl.setEnabled(enabled);
  }

  void _componentModeRB_stateChanged(ChangeEvent e)
  {
    this._panelBasedRB.setEnabled(this._componentModeRB.isSelected());
    this._boardBasedRB.setEnabled(this._componentModeRB.isSelected());
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("SamplingModeOptionPane [");
    buf.append(" Enabled:" + isSamplingEnabled());
    buf.append(", Mode:");
    switch (getSamplingMode())
    {
      case PANEL_SAMPLING_MODE:
        buf.append("Panel");
        break;
      case COMPONENT_SAMPLING_MODE:
        buf.append("Component");
        break;
    }
    buf.append(", Increment:");
    switch (getIncrementMode())
    {
      case PANEL_BASED_SAMPLING:
        buf.append("Panel-based");
        break;
      case BOARD_BASED_SAMPLING:
        buf.append("Board-based");
        break;
    }
    buf.append(", Percent:" + getSamplingPercentage());
    buf.append(" ]");
    return buf.toString();
  }

  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _dataPanel = new JPanel();
  private JPanel _enablePanel = new JPanel();
  private JCheckBox _enableSamplingCkB = new JCheckBox();
  private JPanel _percenPanel = new JPanel();
  private JPanel _incrementPanel = new JPanel();
  private JRadioButton _boardBasedRB = new JRadioButton();
  private JRadioButton _panelBasedRB = new JRadioButton();
  private JLabel _percentLbl = new JLabel();
  private JLabel _sampleLbl = new JLabel();
  private ButtonGroup _buttonGroup1 = new ButtonGroup();
  private ButtonGroup _buttonGroup2 = new ButtonGroup();
  private IntegerSpinBox _percenSpinBox = new IntegerSpinBox(1, 1, 1, 100);
  private VerticalFlowLayout _verticalFlowLayout1 = new VerticalFlowLayout();
  private VerticalFlowLayout _verticalFlowLayout2 = new VerticalFlowLayout(VerticalFlowLayout.FILL);
  private JPanel _modePanel = new JPanel();
  private JRadioButton _panelModeRB = new JRadioButton();
  private JRadioButton _componentModeRB = new JRadioButton();
  private Border _border1;
  private TitledBorder _titledBorder1;
  private Border _border2;
  private TitledBorder _titledBorder2;
}