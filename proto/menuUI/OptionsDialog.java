package proto.menuUI;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.event.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class OptionsDialog extends JDialog
{
  private int _status = JOptionPane.CANCEL_OPTION;

  public OptionsDialog()
  {
    this(null, "", false);
  }

  public OptionsDialog(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }

    loadContents();
  }

  void jbInit() throws Exception
  {
    _okBtn.setText("OK");
    _okBtn.setFont(new java.awt.Font("Dialog", 1, 12));
    _okBtn.setMnemonic('O');
    _okBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _okBtn_actionPerformed(e);
      }
    });
    _cancelBtn.setText("Cancel");
    _cancelBtn.setMnemonic('C');
    _cancelBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _cancelBtn_actionPerformed(e);
      }
    });
    _applyBtn.setEnabled(false);
    _applyBtn.setMnemonic('A');
    _applyBtn.setText("Apply");
    _applyBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _applyBtn_actionPerformed(e);
      }
    });
    _helpBtn.setMnemonic('H');
    _helpBtn.setText("Help");
    _helpBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _helpBtn_actionPerformed(e);
      }
    });
    _gridLayout2.setHgap(5);
    _okCancelPanel.setLayout(_gridLayout2 );
    _okCancelPanel.add(_okBtn, null);
    _okCancelPanel.add(_cancelBtn, null);
//    _okCancelPanel.add(_applyBtn, null);
    _okCancelPanel.add(_helpBtn, null);
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,5,5,5));
    _flowLayout1.setAlignment(FlowLayout.RIGHT);
    _buttonPanel.setLayout(_flowLayout1);
    _buttonPanel.add(_okCancelPanel, null);

    _5dxGlobalPanel.setLayout(_borderLayout2);
    _5dxGlobalPanel.add(_generalPane, BorderLayout.CENTER);
    _startupShutdownPanel.setLayout(_borderLayout3);
    _startupShutdownPanel.add(_startupPane, BorderLayout.CENTER);
    _inspectionPanel.setLayout(_borderLayout5);
    _inspectionPanel.add(_inspectionPane, BorderLayout.CENTER);
    _panelHandlingPanel.setLayout(_borderLayout6);
    _panelHandlingPanel.add(_panelHandlingPane, BorderLayout.CENTER);
    _serialNumberPanel.setLayout(_borderLayout7);
    _serialNumberPanel.add(_serialNumPane, BorderLayout.CENTER);
    _samplingModePanel.setLayout(_borderLayout8);
    _samplingModePanel.add(_samplingModePane, BorderLayout.CENTER);
    _reportsPanel.setLayout(_borderLayout9);
    _reportsPanel.add(_reportsPane, BorderLayout.CENTER);
    _selfTestPanel.setLayout(_borderLayout10);
    _selfTestPanel.add(_selfTestPane, BorderLayout.CENTER);
    _barCodePanel.setLayout(_borderLayout11);
    _barCodePanel.add(_barCodePane, BorderLayout.CENTER);
    //_tabbedPane.add(_5dxGlobalPanel, "General");  // apm -- we don't really need this.  Just a goofy minimum disk space warning
    _tabbedPane.add(_startupShutdownPanel, "Startup / Shutdown");
    _tabbedPane.add(_inspectionPanel, "Inspection");
    _tabbedPane.add(_panelHandlingPanel, "Panel Handling");
    _tabbedPane.add(_serialNumberPanel, "Serial Number");
    _tabbedPane.add(_barCodePanel, "Bar Code");
    _tabbedPane.add(_samplingModePanel, "Sampling Mode");
    _tabbedPane.add(_reportsPanel, "Reports");
    _tabbedPane.add(_selfTestPanel, "Self-test");

    _centerPanel.setLayout(_borderLayout1);
    _centerPanel.add(_tabbedPane, BorderLayout.CENTER);
    _rootPanel.setLayout(_rootPanelLayout);
    _rootPanel.add(_buttonPanel, BorderLayout.SOUTH);
    _rootPanel.add(_centerPanel, BorderLayout.CENTER);
    getContentPane().add(_rootPanel);
  }

  private void exitDialog( int exitStatus )
  {
    this.hide();
  }

  public int showDialog()
  {
    this.show();
    return _status;
  }

  private void loadContents()
  {
// DEBUG
    _generalPane.setFreeSpace(10);

  _startupPane.setAutomaticRestart(true);
  _startupPane.setLogoutShutdown(true);
  _startupPane.setLogoutXrayCurrentOff(true);
  _startupPane.setLogoutRotaryOff(true);
  _startupPane.setShortTermShutdownXrayVoltageOff(true);
  _startupPane.setLongTermShutdownPowerDown(true);

  _inspectionPane.setAutoReviewEnabled(true);
  _inspectionPane.setAutoReviewDefectThreshold(50);
  _inspectionPane.setAlignmentRecovery(InspectionOptionPane.MAP_ALIGNMENT_RECOVERY);
  _inspectionPane.setLogFile(InspectionOptionPane.APPEND_LOG_FILE);

  _panelHandlingPane.setPanelHandlingMode(PanelHandlingOptionPane.MANUAL_FLOW_THROUGH);
  _panelHandlingPane.setMiddelRailPresent(true);
  _panelHandlingPane.setDualPanelMode(true);

  _serialNumPane.setProgramSelection(SerialNumOptionPane.PROGRAM_SELECTION_PRESELECT);
  _serialNumPane.setSerialNumberBased(SerialNumOptionPane.BOARD_BASED_SERIAL_NUMBERS);
  _serialNumPane.setBoardSkipped(true);

  _barCodePane.setBarCodeEnabled(true);
  _barCodePane.setInterfaceModel(BarCodeOptionPane.DL4300_INTERFACE_MODEL);
  _barCodePane.setScanMode(BarCodeOptionPane.CONTINUOUS_SCAN_MODE);
  _barCodePane.setConfiguration(BarCodeOptionPane.MULTI_DROP_CONFIGURATION);
  _barCodePane.setNumberOfHeads(3);

  _samplingModePane.setSamplingEnabled(true);
  _samplingModePane.setSamplingMode(SamplingModeOptionPane.COMPONENT_SAMPLING_MODE);
  _samplingModePane.setIncrementMode(SamplingModeOptionPane.BOARD_BASED_SAMPLING);
  _samplingModePane.setSamplingPercentage(50);

  _reportsPane.setDefectReporterDisplay(ReportsOptionPane.ALWAYS);
  _reportsPane.setDefectReporterPrint(ReportsOptionPane.NEVER);
  _reportsPane.setDefectReporterFile(ReportsOptionPane.DEFECTIVE);

  _selfTestPane.setSelfTestEnabled(true);
  _selfTestPane.setGrayLevelCompensation(true);
  _selfTestPane.setTestFailureAction(SelfTestOptionPane.IGNORE);
  _selfTestPane.setRunAtStartup(true);
  _selfTestPane.setRunEveryNBoards(8);
  _selfTestPane.setRunEveryNPanels(12);
  _selfTestPane.setRunEveryNMinutes(34);
  _selfTestPane.setDisplayResults(true);
  _selfTestPane.setPrintResults(false);
// DEBUG
  }

  private void saveContents()
  {
// DEBUG
  System.out.println(_generalPane.toString());
  System.out.println(_startupPane.toString());
  System.out.println(_inspectionPane.toString());
  System.out.println(_panelHandlingPane.toString());
  System.out.println(_serialNumPane.toString());
  System.out.println(_barCodePane.toString());
  System.out.println(_samplingModePane.toString());
  System.out.println(_reportsPane.toString());
  System.out.println(_selfTestPane.toString());
// DEBUG
  }

  void _okBtn_actionPerformed(ActionEvent e)
  {
    saveContents();
    _status = JOptionPane.OK_OPTION;
    exitDialog( 1 );
  }

  void _cancelBtn_actionPerformed(ActionEvent e)
  {
    _status = JOptionPane.CANCEL_OPTION;
    exitDialog( 0 );
  }

  void _applyBtn_actionPerformed(ActionEvent e)
  {

  }

  void _helpBtn_actionPerformed(ActionEvent e)
  {

  }

// DEBUG
  public static void main(String[] args)
  {
    try
    {
      // Make sure the look and feel is set before any components are created.
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (ClassNotFoundException e) { /* do nothing */ }
    catch (InstantiationException e) { /* do nothing */ }
    catch (IllegalAccessException e) { /* do nothing */ }
    catch (UnsupportedLookAndFeelException e) { /* do nothing */ }

    OptionsDialog dlg = new OptionsDialog(null, "5DX Options", true);
    dlg._startFromMain = true;
    com.agilent.guiUtil.SwingUtils.setScreenPosition(dlg,0.5,0.25);
    int returnVal = dlg.showDialog();
    if (returnVal == JFileChooser.APPROVE_OPTION)
      System.out.println("OK");
    else
      System.out.println("Canceled");
    System.exit(0);
  }
  private boolean _startFromMain = false;
// DEBUG
  private JPanel _rootPanel = new JPanel();
  private BorderLayout _rootPanelLayout = new BorderLayout();
  private JPanel _buttonPanel = new JPanel();
  private GridLayout _gridLayout2 = new GridLayout();
  private JPanel _okCancelPanel = new JPanel();
  private JButton _cancelBtn = new JButton();
  private JButton _okBtn = new JButton();
  private JButton _applyBtn = new JButton();
  private JButton _helpBtn = new JButton();
  private FlowLayout _flowLayout1 = new FlowLayout();
  private JPanel _centerPanel = new JPanel();
  private JTabbedPane _tabbedPane = new JTabbedPane();
  private JPanel _5dxGlobalPanel = new JPanel();
  private JPanel _selfTestPanel = new JPanel();
  private JPanel _samplingModePanel = new JPanel();
  private JPanel _panelHandlingPanel = new JPanel();
  private JPanel _serialNumberPanel = new JPanel();
  private JPanel _reportsPanel = new JPanel();
  private JPanel _startupShutdownPanel = new JPanel();
  private JPanel _inspectionPanel = new JPanel();
  private JPanel _barCodePanel = new JPanel();
  private BorderLayout _borderLayout1 = new BorderLayout();
  private BorderLayout _borderLayout2 = new BorderLayout();
  private BorderLayout _borderLayout3 = new BorderLayout();
  private BorderLayout _borderLayout4 = new BorderLayout();
  private BorderLayout _borderLayout5 = new BorderLayout();
  private BorderLayout _borderLayout6 = new BorderLayout();
  private BorderLayout _borderLayout7 = new BorderLayout();
  private BorderLayout _borderLayout8 = new BorderLayout();
  private BorderLayout _borderLayout9 = new BorderLayout();
  private BorderLayout _borderLayout10 = new BorderLayout();
  private BorderLayout _borderLayout11 = new BorderLayout();

  private StartupOptionPane _startupPane = new StartupOptionPane();
  private SerialNumOptionPane _serialNumPane = new SerialNumOptionPane();
  private InspectionOptionPane _inspectionPane = new InspectionOptionPane();
  private BarCodeOptionPane _barCodePane = new BarCodeOptionPane();
  private PanelHandlingOptionPane _panelHandlingPane = new PanelHandlingOptionPane();
  private SelfTestOptionPane _selfTestPane = new SelfTestOptionPane();
  private SamplingModeOptionPane _samplingModePane = new SamplingModeOptionPane();
  private ReportsOptionPane _reportsPane = new ReportsOptionPane();
  private GeneralOptionPane _generalPane = new GeneralOptionPane();
}