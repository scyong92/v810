package proto.menuUI;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class LoadPanelAction extends MenuAction
{
  private static final ImageIcon _icon = new ImageIcon(Menu5DX.class.getResource("loadpanel.gif"));

  public LoadPanelAction(Menu5DX parent)
  {
    super("Automatic Load", _icon, "Load a panel into the 5DX", 'L', parent);
  }

  public void actionPerformed(ActionEvent e)
  {
// DEBUG
    //System.out.println(this.getTooltip());
// DEBUG
    this.getParent()._loadPanelMI_actionPerformed(e);
  }
}