package proto.menuUI;

import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.border.*;
import java.rmi.*;

//import proto.operatorUI.*;
import proto.menuUI.logon.*;
import proto.menuUI.autotest.*;
import com.agilent.util.*;
import com.agilent.xRayTest.util.*;
import com.agilent.xRayTest.gui.*;
import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.hardware.*;
import com.agilent.xRayTest.gui.algoTuner.*;
import com.agilent.xRayTest.gui.cAndD.*;
import com.agilent.xRayTest.gui.mainMenu.*;
import com.agilent.xRayTest.business.*;
import com.agilent.xRayTest.business.server.*;
import com.agilent.xRayTest.business.panelInspection.*;
import com.agilent.xRayTest.business.panelDesc.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies
 * @author Steve Anonson
 * @version 1.0
 */

public class Menu5DX extends JFrame implements ExitableInt, ControllerObserverInt, InspectionObserverInt
{
  private static final ImageIcon _RED_ICON = new ImageIcon(proto.operatorUI.Operator5DX.class.getResource("redlight.gif"));
  private static final ImageIcon _YELLOW_ICON = new ImageIcon(proto.operatorUI.Operator5DX.class.getResource("yellowlight.gif"));
  private static final ImageIcon _GREEN_ICON = new ImageIcon(proto.operatorUI.Operator5DX.class.getResource("greenlight.gif"));

  private AutoTestPanel _autoTestPanel = new AutoTestPanel(); // proto.operatorUI.AutoTestPanel();
  private FrontPanelStatusPane _statusPanel = new FrontPanelStatusPane();
  private InstantHelpPane _instantHelpPanel = new InstantHelpPane();
  private SystemController _controller = null;

  private boolean _done = false;
  private MenuActions _menuActions = null;
  private User5dx _noUser = new User5dx("<none>", new StandbyMenuEnable());
  private User5dx _currentUser = null;
  private String _currentPanelProgram = null;
  private String _currentPanelProgramVersion = null;
  private String _currentPanelName = null;
  private String _currentBoardName = null;
  private ServerAdapter _serverAdapter = null;
  private RemoteInspectionInt _machinePanelInspect = null;
  private RemotePanelDescInt _panelDescription = null;
  private RemoteDosShellInt _dosShell = null;
  private String _dosCommand = null;
  private DOSShellObserverDialog _dlgout = null;
  private DOSShellObserverDialog _dlgerr = null;
  private WorkerThread _dosThread = new WorkerThread("DOSThread");
  private WorkerThread _machineThread = new WorkerThread("MachineThread");

  private BusyCancelDialog _loadPanelBusyDlg = null;
  private BusyDialog _unloadPanelBusyDlg = null;

  /**
   * Constructor.
   */
  public Menu5DX()
  {
    super(" 5DX ");
    try
    {
      jbInit();
      pack();
    }
    catch(Exception e)
    {
      MessageDialog.reportException(this, e);
    }
    // Set the Window 'X' button to exit the application.
    addWindowListener( new java.awt.event.WindowAdapter()
    { public void windowClosing(java.awt.event.WindowEvent e) { exitApp(); } });

//    _splitPane2.setDividerLocation(0.80);
    _splitPane3.setDividerLocation(0.35);
    com.agilent.guiUtil.SwingUtils.setScreenPosition(this, 0.5, 0.25);

    // Set up glass pane listeners used to disable the interface.
    this.getGlassPane().addMouseListener(new MouseAdapter() { });
    this.getGlassPane().addKeyListener(new KeyAdapter() { });

    // Add listeners to the busy dialogs
    _loadPanelBusyDlg = new BusyCancelDialog(this, "Waiting to load a panel", "Load Panel", false);
    _loadPanelBusyDlg.addCancelActionListener(new ActionListener()
    {
       public void actionPerformed(ActionEvent e)
      {
        doCancelLoadPanel();
      }
    });
    _unloadPanelBusyDlg = new BusyDialog(this, "Waiting to unload a panel", "Unload Panel", false);
    _unloadPanelBusyDlg.addEscapeActionListener(new ActionListener()
    {
       public void actionPerformed(ActionEvent e)
      {
        doCancelUnloadPanel();
      }
    });

    // Create the controller class
    _controller = new SystemController(this, this, this.getPanelQueueInt());
  }

  /**
   * Overriding this method ensures that the frame will never be smaller than
   * the size returned by this method.
   */
  public Dimension getMinimumSize()
  {
    final int minimumWidth = 800;
    final int minimumHeight = 700;
    Dimension min = super.getMinimumSize();  // get recommended minimum size

    if (min.width < minimumWidth)
      min.width = minimumWidth;  // set new minimum width
    if (min.height < minimumHeight)
      min.height = minimumHeight;  // set new minimum height
    return min;
  }

  /**
   * Overriding this method ensures that the frame will never be smaller than
   * the minimim size returned by the getMinimumSize method.
   */
  public Dimension getPreferredSize()
  {
    Dimension prefer = super.getPreferredSize();  // get recommended preferred size
    Dimension min = getMinimumSize();  // get minimum size

    if (prefer.width < min.width)
      prefer.width = min.width;  // set new preferred width
    if (prefer.height < min.height)
      prefer.height = min.height;  // set new preferred height
    return prefer;
  }

  /**
   * Put the frame in the upper center of the screen.
   */
  private void setScreenPosition()
  {
    pack();
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension thisSize = getSize();
    if (thisSize.height > screenSize.height)
      thisSize.height = screenSize.height;
    if (thisSize.width > screenSize.width)
      thisSize.width = screenSize.width;
    setLocation( screenSize.width / 2 - thisSize.width / 2, screenSize.height / 4);
  }

  private void setApplicationTitle( String panelName, String boardName, String version )
  {
    String title = "5DX";
    if (panelName != null)
      title = title + " - " + panelName;
    if (boardName != null)
      title = title + " : " + boardName;
    if (version != null)
      title = title + " : " + version;
    this.setTitle(title);
  }

  /**
   * Return the GUI part of the auto test.
   */
  public InspectionObserverInt getAutoTestUserInt()
  {
    return _autoTestPanel;
  }

  /**
   * Return the GUI part of the auto test.
   */
  public PanelQueueInt getPanelQueueInt()
  {
    return _autoTestPanel;
  }

  /**
   * Return the GUI part of the status area.
   */
//  public StatusUserInt getStatusUserInt()
//  {
//    return _statusPanel;
//  }

  /**
   * Return the done flag.
   */
  public boolean done()       { return _done; }

  /**
   * This method is called to start the application.  The logon dialog is
   * shown first and if a valid user is entered, the interface is shown and
   * the testing thread is started.
   * If the user cancels the logon, the application exits.
   */
  public void startApp()
  {
    try
    {
      _serverAdapter = new ServerAdapter();
    }
    catch (RemoteException re)
    {
      MessageDialog.reportRemoteError(this);
      exitApp();  // get out if ServerAdapter fails
    }

    // Hook the application up to the ServerAdapter
    if (_serverAdapter.connectToServer(this) == false)
    {
      _serverAdapter = null;
      exitApp();  // can't connect so get out
    }

    // Save references to the interfaces used.
    try
    {
      _machinePanelInspect = _serverAdapter.getInspectionInt();
      _panelDescription = _serverAdapter.getPanelDescInt();
    }
    catch (RemoteException re)
    {
      /** @todo SLA Handle the remote exception */
      MessageDialog.reportException(this, re);
    }

    // Show the logon dialog first.
    _currentUser = _noUser;
//    this._logonMI_actionPerformed(null);
    // Update the user name in the status pane
    _statusPanel.setUserName(_currentUser.getUsername());
    this.show();
//    this._autoTestPanel.startTesting();
  }

  /**
   * Exit method for the application.  Sets the done flag to true and does
   * a System.exit if the application was started from the class main method.
   */
  public void exitApp()
  {
    _done = true;
    if (_startFromMain)
      System.exit(0);
  }

  /**
   * Create and combine the components for the GUI.<P>
   * The menu items use MenuActions to set the string, mnemonic and actionPerformed method.
   * Toolbar buttons share these MenuActions so that they can be enabled/disabled together.
   */
  private void jbInit() throws Exception
  {
    _menuActions = new MenuActions(this);

    // File Menu
    _fileMenu.setMnemonic('F');
    _fileMenu.setText("File");
    _fileMenu.setName("File");
    _openPanelMI.setText("Open Panel Program...");
    setMenuItemAction( _openPanelMI, _menuActions.getAction(MenuEnum.OPEN_PANEL_PROGRAM) );
    _logonMI.setText("Logon");
    setMenuItemAction( _logonMI, _menuActions.getAction(MenuEnum.LOGON) );
    _logoutMI.setText("Logout");
    setMenuItemAction( _logoutMI, _menuActions.getAction(MenuEnum.LOGOUT) );
    _newUserMI.setText("Register User");
    setMenuItemAction( _newUserMI, _menuActions.getAction(MenuEnum.NEW_USER) );
    _changePasswordMI.setText("Change Password");
    setMenuItemAction( _changePasswordMI, _menuActions.getAction(MenuEnum.CHANGE_PASSWORD) );
    _exitMI.setText("Exit");
    setMenuItemAction( _exitMI, _menuActions.getAction(MenuEnum.EXIT) );
    _fileMenu.add(_openPanelMI);
    _fileMenu.addSeparator();
    _fileMenu.add(_logonMI);
    _fileMenu.add(_logoutMI);
    _fileMenu.add(_changePasswordMI);
    _fileMenu.add(_newUserMI);
    _fileMenu.addSeparator();
    _fileMenu.add(_exitMI);

    // System Menu
    _systemMenu.setMnemonic('S');
    _systemMenu.setText("System");
    _systemMenu.setName("System");
    _startupMI.setText("Startup");
    setMenuItemAction( _startupMI, _menuActions.getAction(MenuEnum.STARTUP_AUTOMATIC) );
    _shutdownMenu.setMnemonic('D');
    _shutdownMenu.setText("Shutdown");
    _shutdownMenu.setName("Shutdown");
    _shortTermMI.setText("Short Term");
    setMenuItemAction( _shortTermMI, _menuActions.getAction(MenuEnum.SHUTDOWN_SHORT_TERM) );
    _longTermMI.setText("Long Term");
    setMenuItemAction( _longTermMI, _menuActions.getAction(MenuEnum.SHUTDOWN_LONG_TERM) );
    _shutdownMenu.add(_shortTermMI);
    _shutdownMenu.add(_longTermMI);
    _panelHandlingMenu.setMnemonic('H');
    _panelHandlingMenu.setText("Panel Handling");
    _panelHandlingMenu.setName("Panel Handling");
    _homeRailsMI.setText("Home Rails");
    setMenuItemAction( _homeRailsMI, _menuActions.getAction(MenuEnum.HOME_RAILS) );
    _resetPanelHandlingMI.setText("Reset");
    setMenuItemAction( _resetPanelHandlingMI, _menuActions.getAction(MenuEnum.RESET_PANEL_HANDLING) );
    _panelHandlingMenu.add(_homeRailsMI);
    _panelHandlingMenu.add(_resetPanelHandlingMI);
    _reset5DXMI.setText("Reset 5DX");
    setMenuItemAction( _reset5DXMI, _menuActions.getAction(MenuEnum.RESET_5DX) );
    _resetIASMI.setText("Reset Image Analysis (IAS)");
    setMenuItemAction( _resetIASMI, _menuActions.getAction(MenuEnum.RESET_IAS) );
    _status5DXMI.setText("5DX Status...");
    setMenuItemAction( _status5DXMI, _menuActions.getAction(MenuEnum.STATUS_5DX) );
    _systemMenu.add(_startupMI);
    _systemMenu.add(_shutdownMenu);
    _systemMenu.addSeparator();
    _systemMenu.add(_panelHandlingMenu);
    _systemMenu.add(_reset5DXMI);
    _systemMenu.add(_resetIASMI);
    _systemMenu.add(_status5DXMI);

    // Panel Menu
    _panelMenu.setMnemonic('P');
    _panelMenu.setText("Panel");
    _panelMenu.setName("Panel");
    _loadPanelMI.setText("Automaitc Load");
    setMenuItemAction( _loadPanelMI, _menuActions.getAction(MenuEnum.LOAD_PANEL) );
    _unloadPanelMI.setText("Unload");
    setMenuItemAction( _unloadPanelMI, _menuActions.getAction(MenuEnum.UNLOAD_PANEL) );
    _alignAndMapMI.setText("Align and Map");
    setMenuItemAction( _alignAndMapMI, _menuActions.getAction(MenuEnum.ALIGN_AND_MAP) );
    _alignMenu.setText("Align");
    _alignMenu.setName("Align");
    _alignMenu.setMnemonic('A');
    _alignAutoMI.setText("Automatic");
    setMenuItemAction( _alignAutoMI, _menuActions.getAction(MenuEnum.ALIGN_AUTOMATIC) );
    _alignManualMI.setText("Manual");
    setMenuItemAction( _alignManualMI, _menuActions.getAction(MenuEnum.ALIGN_MANUAL) );
    _displayMatMI.setText("Display Matrix");
    setMenuItemAction( _displayMatMI, _menuActions.getAction(MenuEnum.DISPLAY_ALIGNMENT_MATRIX) );
    _alignMenu.add(_alignAutoMI);
    _alignMenu.add(_alignManualMI);
    _alignMenu.add(_displayMatMI);
    _mapMenu.setText("Map");
    _mapMenu.setName("Map");
    _mapMenu.setMnemonic('M');
    _mapAutoMI.setText("Automaitc");
    setMenuItemAction( _mapAutoMI, _menuActions.getAction(MenuEnum.MAP_AUTOMATIC) );
    _mapManualMI.setText("Manual");
    setMenuItemAction( _mapManualMI, _menuActions.getAction(MenuEnum.MAP_MANUAL) );
    _checkSurMapMI.setText("Check Surface Map");
    setMenuItemAction( _checkSurMapMI, _menuActions.getAction(MenuEnum.CHECK_SURFACE_MAP) );
    _verifyLaserMI.setText("Verify Laser Repeatability");
    setMenuItemAction( _verifyLaserMI, _menuActions.getAction(MenuEnum.VERIFY_LASER) );
    _stageSpeedMI.setText("Set Stage Speed");
    setMenuItemAction( _stageSpeedMI, _menuActions.getAction(MenuEnum.STAGE_SPEED) );
    _manualLoadPanelMI.setText("Manual Load");
    setMenuItemAction( _manualLoadPanelMI, _menuActions.getAction(MenuEnum.MANUAL_LOAD_PANEL) );
    _mapMenu.add(_mapAutoMI);
    _mapMenu.add(_mapManualMI);
    _mapMenu.add(_checkSurMapMI);
    _mapMenu.add(_verifyLaserMI);
    _panelMenu.add(_loadPanelMI);
    _panelMenu.add(_unloadPanelMI);
    _panelMenu.add(_alignAndMapMI);
    _panelMenu.add(_alignMenu);
    _panelMenu.add(_mapMenu);
    _panelMenu.add(_stageSpeedMI);
    _panelMenu.add(_manualLoadPanelMI);

    // Programming Menu
    _programmingMenu.setMnemonic('R');
    _programmingMenu.setText("Programming");
    _programmingMenu.setName("Programming");
    _tunerMI.setText("Algorithm Tuner");
    setMenuItemAction( _tunerMI, _menuActions.getAction(MenuEnum.TUNER) );
    _cadlinkMI.setText("CADLink");
    setMenuItemAction( _cadlinkMI, _menuActions.getAction(MenuEnum.CADLINK) );
    _checkcadMI.setText("CheckCAD");
    setMenuItemAction( _checkcadMI, _menuActions.getAction(MenuEnum.CHECKCAD) );
    _xoutMI.setText("Setup X-out Detection");
    setMenuItemAction( _xoutMI, _menuActions.getAction(MenuEnum.XOUT) );
    _mapPointsMI.setText("Setup Map Points");
    setMenuItemAction( _mapPointsMI, _menuActions.getAction(MenuEnum.MAP_POINTS) );
    _viewFocusMI.setText("Adjust View Focus");
    setMenuItemAction( _viewFocusMI, _menuActions.getAction(MenuEnum.VIEW_FOCUS) );
    _reveiwDefMI.setText("Review Defects");
    setMenuItemAction( _reveiwDefMI, _menuActions.getAction(MenuEnum.REVIEW_DEFECTS) );
    _reviewMeasMI.setText("Review Measurements");
    setMenuItemAction( _reviewMeasMI, _menuActions.getAction(MenuEnum.REVIEW_MEASUREMENTS) );
    _defectReporterMI.setText("Defect Reporter");
    setMenuItemAction( _defectReporterMI, _menuActions.getAction(MenuEnum.REPORT_DEFECTS) );
    _programmingMenu.add(_cadlinkMI);
    _programmingMenu.add(_tunerMI);
    _programmingMenu.add(_checkcadMI);
    _programmingMenu.addSeparator();
    _programmingMenu.add(_mapPointsMI);
    _programmingMenu.add(_viewFocusMI);
    _programmingMenu.add(_xoutMI);
    _programmingMenu.addSeparator();
    _programmingMenu.add(_reveiwDefMI);
    _programmingMenu.add(_reviewMeasMI);
    _programmingMenu.add(_defectReporterMI);

    // Tools Menu
    _toolsMenu.setMnemonic('T');
    _toolsMenu.setText("Tools");
    _toolsMenu.setName("Tools");
    _workImageMenu.setText("Collect Workstation Images");
    _workImageMenu.setName("Collect Workstation Images");
    _workImageMenu.setMnemonic('W');
    _workImageAutoMI.setText("Automatic Test");
    setMenuItemAction( _workImageAutoMI, _menuActions.getAction(MenuEnum.WORK_IMAGE_AUTOMATIC) );
    _workImagePanelMI.setText("Panel Test");
    setMenuItemAction( _workImagePanelMI, _menuActions.getAction(MenuEnum.WORK_IMAGE_PANEL) );
    _workImageBoardMI.setText("Board Test");
    setMenuItemAction( _workImageBoardMI, _menuActions.getAction(MenuEnum.WORK_IMAGE_BOARD) );
    _workImageCompMI.setText("Component Test");
    setMenuItemAction( _workImageCompMI, _menuActions.getAction(MenuEnum.WORK_IMAGE_COMPONENT) );
    _workImagePinMI.setText("Pin Test");
    setMenuItemAction( _workImagePinMI, _menuActions.getAction(MenuEnum.WORK_IMAGE_PIN) );
    _workImageJointMI.setText("Joint Type Test");
    setMenuItemAction( _workImageJointMI, _menuActions.getAction(MenuEnum.WORK_IMAGE_JOINT) );
    _workImageSubtypeMI.setText("Subtype Test");
    setMenuItemAction( _workImageSubtypeMI, _menuActions.getAction(MenuEnum.WORK_IMAGE_SUBTYPE) );
    _workImageMenu.add(_workImageAutoMI);
    _workImageMenu.add(_workImagePanelMI);
    _workImageMenu.add(_workImageBoardMI);
    _workImageMenu.add(_workImageCompMI);
    _workImageMenu.add(_workImagePinMI);
    _workImageMenu.add(_workImageJointMI);
    _workImageMenu.add(_workImageSubtypeMI);
    _diagImageMI.setText("Collect Diagnostic Image");
    setMenuItemAction( _diagImageMI, _menuActions.getAction(MenuEnum.DIAGNOSTIC_IMAGE) );
    _repairImageMI.setText("Collect Repair Image");
    setMenuItemAction( _repairImageMI, _menuActions.getAction(MenuEnum.REPAIR_IMAGE) );
    _runSelfTestMI.setText("Run Selftest");
    setMenuItemAction( _runSelfTestMI, _menuActions.getAction(MenuEnum.SELF_TEST) );
    _serviceMI.setText("Service Menu");
    setMenuItemAction( _serviceMI, _menuActions.getAction(MenuEnum.SERVICE_MENU) );
    _toolboxMI.setText("Toolbox");
    setMenuItemAction( _toolboxMI, _menuActions.getAction(MenuEnum.TOOLBOX) );
    _optionsMI.setText("Options...");
    setMenuItemAction( _optionsMI, _menuActions.getAction(MenuEnum.OPTIONS) );
    _toolsMenu.add(_workImageMenu);
    _toolsMenu.add(_diagImageMI);
    _toolsMenu.add(_repairImageMI);
    _toolsMenu.addSeparator();
    _toolsMenu.add(_runSelfTestMI);
    _toolsMenu.add(_serviceMI);
    _toolsMenu.add(_toolboxMI);
    _toolsMenu.addSeparator();
    _toolsMenu.add(_optionsMI);

    // Help Menu
    _helpMenu.setMnemonic('H');
    _helpMenu.setText("Help");
    _helpMenu.setName("Help");
    _helpContentsMI.setText("Contents");
    setMenuItemAction( _helpContentsMI, _menuActions.getAction(MenuEnum.HELP_CONTENTS) );
    _helpAboutMI.setText("About");
    setMenuItemAction( _helpAboutMI, _menuActions.getAction(MenuEnum.HELP_ABOUT) );
    _helpMenu.add(_helpContentsMI);
    _helpMenu.add(_helpAboutMI);

    // Build the menu bar
    _menuBar.add(_fileMenu);
    _menuBar.add(_systemMenu);
    _menuBar.add(_panelMenu);
    _menuBar.add(_programmingMenu);
    _menuBar.add(_toolsMenu);
    _menuBar.add(_helpMenu);
    this.setJMenuBar(_menuBar);

    // 5DX Toolbar
    _5dxToolbar.setFloatable(false);
    _5dxToolbar.add(_menuActions.getAction(MenuEnum.OPEN_PANEL_PROGRAM));
    _5dxToolbar.add(_menuActions.getAction(MenuEnum.STARTUP_AUTOMATIC));
    _5dxToolbar.addSeparator();
    _5dxToolbar.add(_menuActions.getAction(MenuEnum.LOAD_PANEL));
    _5dxToolbar.add(_menuActions.getAction(MenuEnum.UNLOAD_PANEL));
    _5dxToolbar.add(_menuActions.getAction(MenuEnum.ALIGN_AND_MAP));
    _5dxToolbar.addSeparator();
    _5dxToolbar.add(_menuActions.getAction(MenuEnum.HELP_CONTENTS));

    // Programmer Toolbar
    _progToolsPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Programming Tools "));
    _programToolbar.setBorder(BorderFactory.createEmptyBorder(0,15,0,15));
    _programToolbar.setFloatable(false);
    _programToolbar.setOrientation(JToolBar.VERTICAL);
    _programToolbar.addSeparator(new Dimension(0, 25));
    _programToolbar.add(_menuActions.getAction(MenuEnum.CADLINK));
    _programToolbar.addSeparator();
    _programToolbar.add(_menuActions.getAction(MenuEnum.CHECKCAD));
    _programToolbar.addSeparator();
    _programToolbar.add(_menuActions.getAction(MenuEnum.TUNER));
    _programToolbar.addSeparator();
    _programToolbar.add(_menuActions.getAction(MenuEnum.REVIEW_DEFECTS));
    _programToolbar.addSeparator();
    _programToolbar.add(_menuActions.getAction(MenuEnum.REVIEW_MEASUREMENTS));
    _progToolsPanel.add(_programToolbar, null);

    /** @todo SLA Change to message key for LocalizedString */
    _statusPanel.setStatus( new LocalizedString("Ready", null));
    _splitPane3.add(_statusPanel, JSplitPane.RIGHT);
    _splitPane3.add(_instantHelpPanel, JSplitPane.LEFT);

    _topActionPanel.setLayout(borderLayout2);
    _topActionPanel.add(_autoTestPanel, BorderLayout.CENTER);
    _bottomActionPanel.setLayout(borderLayout3);
    _bottomActionPanel.add(_splitPane3, BorderLayout.CENTER);
    _splitPane2.setOrientation(JSplitPane.VERTICAL_SPLIT);
    _splitPane2.setResizeWeight(1);
    _splitPane2.add(_topActionPanel, JSplitPane.TOP);
    _splitPane2.add(_bottomActionPanel, JSplitPane.BOTTOM);
    _actionPanel.setLayout(borderLayout1);
    _actionPanel.add(_splitPane2, BorderLayout.CENTER);
    _splitPane1.add(_actionPanel, JSplitPane.RIGHT);
    _splitPane1.add(_progToolsPanel, JSplitPane.LEFT);
    _centerPanel.setLayout(_centerPanelLayout);
    _centerPanel.add(_splitPane1, BorderLayout.CENTER);

    _rootPanel.setLayout(_rootPanelLayout);
    _rootPanel.add(_5dxToolbar, BorderLayout.NORTH);
    _rootPanel.add(_centerPanel, BorderLayout.CENTER);
    this.getContentPane().add(_rootPanel, BorderLayout.CENTER);
  }

  /**
   * Assigns an MenuAction to a JMenuItem, removing the icon and setting
   * the mnemonic.
   */
  private void setMenuItemAction( JMenuItem mi, MenuAction action )
  {
    mi.setAction(action);
    mi.setIcon(null);
    mi.setMnemonic(action.getMnemonic());
    mi.setName( action.getName() );
  }

  // *************** Access methods ******************************
  public void enableMenu( MenuEnable menuEnable )
  {
    // Set the enable on all the individual menu items.
    this._menuActions.setEnabled(menuEnable);
    // Disable the submenus who have no enabled menu items.

    // Shutdown menu
    if (!menuEnable.get(MenuEnum.SHUTDOWN_LONG_TERM) &&
        !menuEnable.get(MenuEnum.SHUTDOWN_SHORT_TERM))
      this._shutdownMenu.setEnabled(false);
    else
      this._shutdownMenu.setEnabled(true);

    // Panel Handling menu
    if (!menuEnable.get(MenuEnum.HOME_RAILS) &&
        !menuEnable.get(MenuEnum.RESET_PANEL_HANDLING) &&
        !menuEnable.get(MenuEnum.STATUS_PANEL_HANDLING))
      this._panelHandlingMenu.setEnabled(false);
    else
      this._panelHandlingMenu.setEnabled(true);

    // Align menu
    if (!menuEnable.get(MenuEnum.ALIGN_AUTOMATIC) &&
        !menuEnable.get(MenuEnum.ALIGN_MANUAL) &&
        !menuEnable.get(MenuEnum.DISPLAY_ALIGNMENT_MATRIX))
      this._alignMenu.setEnabled(false);
    else
      this._alignMenu.setEnabled(true);

    // Map menu
    if (!menuEnable.get(MenuEnum.MAP_AUTOMATIC) &&
        !menuEnable.get(MenuEnum.MAP_MANUAL) &&
        !menuEnable.get(MenuEnum.CHECK_SURFACE_MAP) &&
        !menuEnable.get(MenuEnum.VERIFY_LASER))
      this._mapMenu.setEnabled(false);
    else
      this._mapMenu.setEnabled(true);

    // Collect workstaticon images menu
    if (!menuEnable.get(MenuEnum.WORK_IMAGE_AUTOMATIC) &&
        !menuEnable.get(MenuEnum.WORK_IMAGE_PANEL))
      this._workImageMenu.setEnabled(false);
    else
      this._workImageMenu.setEnabled(true);

  }

  /**
   * Create a dos shell.
   *
   * @param   io - DOSOutputInt to capture the shell I/O.
   */
  private RemoteDosShellInt getDOSShell()
  {
    if (_serverAdapter != null)
      _dosShell = _serverAdapter.getDosShell();
    return _dosShell;
  }

  /**
   * Send a command to a dos shell.
   *
   * @param   io - DOSOutputInt to capture the shell I/O.
   */
  private boolean sendDOSShellCommand(RemoteDosShellInt dosShell, String cmd, RemoteObserverInt stdout, RemoteObserverInt stderr)
  {
    if (dosShell == null)
      return false;
    _dosShell = dosShell;

    try
    {
      // Attach an observer to the std out stream.
      if (stdout != null)
        _serverAdapter.getDosShellOutputObservableInt().addObserver(stdout);
      else
      {
        if (_dlgout == null)
        {
          _dlgout = new DOSShellObserverDialog(this, "DOS Shell Out", false);
          _serverAdapter.getDosShellOutputObservableInt().addObserver(_dlgout);
        }
      }
      // Attach an observer to the std err stream.
      if (stderr != null)
        _serverAdapter.getDosShellErrorObservableInt().addObserver(stderr);
      else
      {
        if (_dlgerr == null)
        {
          _dlgerr = new DOSShellObserverDialog(this, "DOS Shell Error", false);
          _serverAdapter.getDosShellOutputObservableInt().addObserver(_dlgerr);
        }
      }
      _dosCommand = cmd;
    }
    catch (RemoteException re)
    {
      /** @todo SLA Do something with the remote error. */
      MessageDialog.reportException(this, re);
    }
    playDead(true);  // disable the interface

    _dosThread.invokeLater( new Runnable()
    {
      public void run()
      {
        Menu5DX.this.runDOSShellCommand();
      }
    });
    return true;
  }

  /**
   * This method is run on a separate thread to execute a DOS command.
   */
  private void runDOSShellCommand()
  {
    if (_dosShell == null)
      return;

    try
    {
      _dosShell.showWindow();
      _dosShell.sendCommand(_dosCommand);
//      _dosShell.hideWindow();
//      _dlgout.hide();
//      _dlgerr.hide();
    }
    catch (RemoteException re)
    {
      /** @todo SLA Do something with the remote error. */
      MessageDialog.reportException(this, re);
    }

    playDead(false);  // enable the interface
    sendStatusMessage("DOS command " + _dosCommand + " done");  // Update status
  }

  /**
   * Load cad for a panel.
   *
   * @param   panelName - String containing the name of the panel.
   */
  private boolean loadCad(String panelName)
  {
    java.util.List warnings = new java.util.ArrayList();
    try
    {
      _panelDescription.loadPanelProgram(DatastoreMode.READ_ONLY, panelName, warnings);
    }
    catch (DatastoreException de)
    {
      MessageDialog.reportException(this, de);
      return false;
    }
    catch (com.agilent.xRayTest.hardware.HardwareException he)
    {
      /** @todo SLA Report load cad error. */
      MessageDialog.reportException(this, he);
      return false;
    }
    catch (RemoteException re)
    {
      MessageDialog.reportException(this, re);
      return false;
    }
    return true;
  }

  /**
   * Put a message string in the status message area in the front panel.
   */
  private void sendStatusMessage( String msg )
  {
    final String message = ((msg == null) ? "" : msg);
    /** @todo Change to message key for LocalizedString */
    SwingUtilities.invokeLater( new Runnable()
    {
      public void run()
      {
        Menu5DX.this._statusPanel.setStatus( new LocalizedString(message, null));
      }
    });
  }

  /**
   * This method sets the frame in an inactive state if the flag is true and
   * active if false.  A busy cursor is displayed when inactive.
   * All mouse and keyboard input is absorbed by the glass pane when it is set
   * to visible.
   */
  private void playDead( boolean flag )
  {
    if (flag)
    {
      SwingUtilities.invokeLater( new Runnable()
      {
        public void run()
        {
          Menu5DX.this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
          Menu5DX.this.getGlassPane().setVisible(true);
          Menu5DX.this.getGlassPane().requestFocus();
        }
      });
    }
    else
    {
      SwingUtilities.invokeLater( new Runnable()
      {
        public void run()
        {
          Menu5DX.this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
          Menu5DX.this.getGlassPane().setVisible(false);
        }
      });
    }
  }

  /**
   * Set the controller class on this class.
   *
   * @param   controller - SystemController instance to use as the controller.
   */
  public void setController(AutoTestInt controller)
  {
  }

  /**
   * ControllerObserverInt method.
   * Set the controller class on this class.
   *
   * @param   controller - SystemController instance to use as the controller.
   */
  public void setController(SystemController controller)
  {
    _controller = controller;
  }

  /**
   * ControllerObserverInt method.
   * Report an error from the controller to the user.
   *
   * @param   err - LocalizedString containing the message key and arguements.
   */
  public void reportError( LocalizedString err )
  {
    if (err == null)
      return;
    /** @todo SLA Lookup LocalizedString */
    String error = err.getMessageKey();
    MessageDialog.showErrorDialog(this, new LocalizedString(error, null), new LocalizedString("Warning", null));
  }

  /**
   * ControllerObserverInt method.
   * Report a warning from the controller to the user.
   *
   * @param   warn - LocalizedString containing the message key and arguements.
   */
  public void reportWarning( LocalizedString warn )
  {
    if (warn == null)
      return;
    /** @todo SLA Lookup LocalizedString */
    String warning = warn.getMessageKey();
    MessageDialog.showWarningDialog(this, new LocalizedString(warning, null), new LocalizedString("Warning", null));
  }

  /**
   * ControllerObserverInt method.
   * Report an exception from the controller to the user.
   *
   * @param   e - Exception to be reported.
   */
  public void reportException( Exception e )
  {
    if (e != null)
      MessageDialog.reportException(this, e);
  }

  /**
   * ControllerObserverInt method.
   * This method is called by the controller to send a message to the user
   * interface.
   *
   * @param   message - String containing the message sent from the controller to the interface.
   * @param   arg - Optional argument passed from the controller.  Can be null.
   */
  public void controllerUpdate( String message, Object arg )
  {
  }

  /**
   * ControllerObserverInt method.
   * Signal the user interface that a new panel is being processed.
   *
   * @param   panel - PanelEntry for the new panel.
   */
  public void newPanel( PanelEntry panel )
  {
    if (panel == null)
    {
      _autoTestPanel.setPanelImage(null);
      return;
    }
    /** @todo SLA check to see if panel program is changing before update */
    _autoTestPanel.setPanelImage(panel.getImage());
  }

  /**
   * ControllerObserverInt method.
   * Signal the user interface that a panel is being loaded into the system.
   *
   * @param   panel - PanelEntry for the new panel.
   */
  public void loadPanel(PanelEntry panel)
  {
    _autoTestPanel.loadPanel(panel);
  }

  /**
   * ControllerObserverInt method.
   * Signal the user interface that a panel has been successfully loaded into the system.
   */
  public void panelLoaded()
  {
    _autoTestPanel.panelLoaded();
  }

  /**
   * ControllerObserverInt method.
   * Signal the user interface that a new panel inspection is starting.
   */
  public void newInspection()
  {
    _autoTestPanel.newInspection();
  }

  /**
   * InspectionObserverInt method.
   * Called by the observed inspection class to report progress.
   *
   * @param   ro - Remote observable instance calling this method.
   * @param   arg - Optional argument.
   */
  public void update(RemoteObservableInt ro, Object arg) throws RemoteException
  {
    if (arg == null)
      return;
    String message = arg.toString();
    _autoTestPanel.inspectionMessage(message);
  }

  /**
   * Send a message to the test messages.
   *
   * @param   message - String to add to the message area.
   */
  public void inspectionMessage(String message)
  {
    _autoTestPanel.inspectionMessage(message);
  }

  /**
   * ControllerObserverInt method.
   * Signal the user interface that a panel is being unloaded from the system.
   *
   * @param   panel - PanelEntry for the new panel.
   */
  public void unloadPanel(PanelEntry panel)
  {
    _autoTestPanel.unloadPanel(panel);
  }

  /**
   * ControllerObserverInt method.
   * Signal the user interface that a panel has been successfully unloaded into the system.
   */
  public void panelUnloaded()
  {
    _autoTestPanel.panelUnloaded();
  }

  /**
   * ControllerObserverInt method.
   * Signal the user interface that a panel has been successfully unloaded into the system.
   */
  public void reportStatus(LocalizedString status)
  {
    _statusPanel.setStatus(status);
  }

  /**
   * Update the automatic test loop status to show it stopped.
   */
  public void setStopped()
  {
    _autoTestPanel.setStopped();
  }

  /**
   * Update the automatic test loop status to show it paused.
   */
  public void setPaused()
  {
    _autoTestPanel.setPaused();
  }

  /**
   * Update the automatic test loop status to show it running.
   */
  public void setRunning()
  {
    _autoTestPanel.setRunning();
  }

  // *************** Callback methods ****************************

  /**
   * Callback for the open panel program menu item.
   * This method reads the available panel programs on the system and presents
   * them to the user in a list box.  After selecting one, the program is loaded
   * using the machine thread.
   */
  void _openPanelMI_actionPerformed(ActionEvent e)
  {
    Object [] panels = null;
    try
    {
      panels = com.agilent.xRayTest.business.panelDesc.RemotePanelDesc.getInstance().getPanelNames().toArray();
    }
    catch (java.rmi.RemoteException re)
    {
      /** @todo SLA - What should I do if we get here? */
      MessageDialog.reportException(this, re);
    }

    OpenPanelDialog dlg = new OpenPanelDialog(this, "Open Panel Program", true, panels);
    com.agilent.guiUtil.SwingUtils.centerOnComponent(dlg,this);
    int returnVal = dlg.showDialog();
    if (returnVal == JOptionPane.OK_OPTION)
    {
      _currentPanelProgram = dlg.getSelectedFile();
// DEBUG
      String panelName = dlg.getSelectedFile();
      System.out.println("Selected panel " + panelName);
// DEBUG
      playDead(true);  // disable the interface

      _machineThread.invokeLater( new Runnable()
      {
        public void run()
        {
          Menu5DX.this.doOpenPanelProgram();
        }
      });
    }
  }

  /**
   * This method calls a method to load the panel cad and if successful it updates
   * the application title to include the panel and board names.  If load fails,
   * the status is updated to show that.
   */
  private void doOpenPanelProgram()
  {
    boolean loaded = this.loadCad(_currentPanelProgram);
    if (loaded)
    {
      _currentPanelName = _currentPanelProgram;
      /** @todo SLA fill in the board name */
      _currentBoardName = null;
      this.setApplicationTitle(_currentPanelName, _currentBoardName, _currentPanelProgramVersion);  // Update program name
      sendStatusMessage("Open program complete");  // Update status
    }
    else
    {
      sendStatusMessage("Open program failed");  // Update status
    }
    playDead(false);  // enable the interface
  }

  /**
   * Callback for the exit menu item.
   */
  void _exitMI_actionPerformed(ActionEvent e)
  {
    this.exitApp();
  }

  /**
   * Callback for the logon menu item.
   */
  void _logonMI_actionPerformed(ActionEvent e)
  {
//    Valid5dxUser validUser = new Valid5dxUser("M:/sanon_5dx/java/proto/menuUI/passwords.dat");
//    java.net.URL passwordURL = proto.menuUI.Menu5DX.class.getResource("passwords.dat");
//    String passwordPath = passwordURL.getFile();
    String passwordPath = System.getProperty("PASSWORD", "M:/sanon_5dx/java/proto/menuUI/passwords.dat");
    Valid5dxUser validUser = new Valid5dxUser(passwordPath);
    LogonDialog loginUnit = new LogonDialog(this, "Logon", true, validUser);
    com.agilent.guiUtil.SwingUtils.centerOnComponent(loginUnit,this);
    loginUnit.show();

    String username = null;
    if (validUser.getUser() != null)
      username = validUser.getUser().getUsername();

    _currentUser = ((username != null) ? validUser.getUser() : this._noUser);
    this.enableMenu( _currentUser.getMenuEnable() );
    // Update the user name in the status pane
    _statusPanel.setUserName(_currentUser.getUsername());
  }

  /**
   * Callback for the logout menu item.
   */
  void _logoutMI_actionPerformed(ActionEvent e)
  {
    _currentUser = this._noUser;
    this.enableMenu(_currentUser.getMenuEnable());
    // Update the user name in the status pane
    _statusPanel.setUserName(_currentUser.getUsername());
  }

  /**
   * Callback for the change password menu item.
   */
  void _changePasswordMI_actionPerformed(ActionEvent e)
  {
    String passwordPath = System.getProperty("PASSWORD", "M:/sanon_5dx/java/proto/menuUI/passwords.dat");
    Valid5dxUser validUser = new Valid5dxUser(passwordPath);
    ChangePasswordDialog dlg = new ChangePasswordDialog(this, "Change Password", true, validUser);
    com.agilent.guiUtil.SwingUtils.centerOnComponent(dlg,this);
    int returnVal = dlg.showDialog();
    if (returnVal == JOptionPane.OK_OPTION)
    {
      // Get the new password from the dialog.
      String password = dlg.getNewPassword();
// DEBUG
      System.out.println(" Changing " + dlg.getUsername() + "'s password from " + dlg.getPassword() + " to " + password);
// DEBUG
      // Update the password file.
    }
  }

  /**
   * Callback for the new user menu item.
   */
  void _newUserMI_actionPerformed(ActionEvent e)
  {
    RegisterUserDialog dlg = new RegisterUserDialog(null, "Configure User", true);
    com.agilent.guiUtil.SwingUtils.centerOnComponent(dlg,this);
    int returnVal = dlg.showDialog();
// DEBUG
    if (returnVal == JOptionPane.OK_OPTION)
    {
      System.out.println("OK");
    }
    else
      System.out.println("Canceled");
// DEBUG
  }

  /**
   * Callback for review defects.
   * DOS
   */
  void _reveiwDefMI_actionPerformed(ActionEvent e)
  {
    String cmd = "REVUMODE.EXE";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the options menu item.
   */
  void _optionsMI_actionPerformed(ActionEvent e)
  {
    OptionsDialog dlg = new OptionsDialog(null, "5DX Options", true);
    com.agilent.guiUtil.SwingUtils.centerOnComponent(dlg,this);
    int returnVal = dlg.showDialog();
// DEBUG
    if (returnVal == JOptionPane.OK_OPTION)
    {
      System.out.println("OK");
    }
    else
      System.out.println("Canceled");
// DEBUG
  }

  /**
   * Callback for the load panel menu item.
   */
  void _loadPanelMI_actionPerformed(ActionEvent e)
  {
    if (_machinePanelInspect == null)
    {
      /** @todo SLA report error. */
      MessageDialog.showErrorDialog(this, new LocalizedString("Cannot access system", null), new LocalizedString("Server Access Error", null));
      return;
    }

    _machineThread.invokeLater( new Runnable()
    {
      public void run()
      {
        Menu5DX.this.doLoadPanel();
      }
    });
  }

  /**
   * This method is run on the machine thread to load a panel into the system.
   */
  private void doLoadPanel()
  {
    playDead(true);  // disable the interface
    _loadPanelBusyDlg.show();  // put up the busy dialog

    Exception e = null;
    try
    {
      _machinePanelInspect.loadPanelIntoMachine();
    }
    catch (PanelProgramNotLoadedException pe)
    {
      /** @todo SLA Handle the hardware exception */
      e = pe;
    }
    catch (HardwareException he)
    {
      /** @todo SLA Handle the hardware exception */
      e = he;
    }
    catch (RemoteException re)
    {
      /** @todo SLA Handle the remote exception */
      e = re;
    }

    _loadPanelBusyDlg.hide();  // hide the dialog
    playDead(false);  // enable the interface

    // report any exception
    if (e != null)
    {
      final Exception exception = e;
      SwingUtilities.invokeLater( new Runnable()
      {
        public void run()
        {
          MessageDialog.reportException(Menu5DX.this, exception);
        }
      });
    }
    sendStatusMessage("Panel Loaded");  // Update status
  }

  /**
   * This method is run to cancel load panel.
   */
  private void doCancelLoadPanel()
  {
    _loadPanelBusyDlg.hide();  // hide the dialog

    Exception e = null;
    try
    {
      _machinePanelInspect.cancelLoadPanelIntoMachine();
    }
    catch (HardwareException he)
    {
      /** @todo SLA Handle the hardware exception */
      e = he;
    }
    catch (RemoteException re)
    {
      /** @todo SLA Handle the remote exception */
      e = re;
    }

    playDead(false);  // enable the interface

    if (e != null)
    {
      final Exception exception = e;
      SwingUtilities.invokeLater( new Runnable()
      {
        public void run()
        {
          MessageDialog.reportException(Menu5DX.this, exception);
        }
      });
    }
    sendStatusMessage("Panel Load Canceled");  // Update status
  }

  /**
   * Callback for the unload panel menu item.
   */
  void _unloadPanelMI_actionPerformed(ActionEvent e)
  {
    if (_machinePanelInspect == null)
    {
      /** @todo SLA report error. */
      MessageDialog.showErrorDialog(this, new LocalizedString("Cannot access system", null), new LocalizedString("Server Access Error", null));
      return;
    }

    _machineThread.invokeLater( new Runnable()
    {
      public void run()
      {
        Menu5DX.this.doUnloadPanel();
      }
    });
  }

  /**
   * This method is run on the machine thread to unload a panel from the system.
   */
  private void doUnloadPanel()
  {
    playDead(true);  // disable the interface
    _unloadPanelBusyDlg.show();  // put up the busy dialog

    try
    {
      _machinePanelInspect.unloadPanelFromMachine();
    }
    catch (HardwareException he)
    {
      /** @todo SLA Handle the hardware exception */
      MessageDialog.reportException(this, he);
    }
    catch (RemoteException re)
    {
      /** @todo SLA Handle the remote exception */
      MessageDialog.reportException(this, re);
    }

    _unloadPanelBusyDlg.hide();  // hide the dialog
    playDead(false);  // enable the interface
    sendStatusMessage("Panel Unloaded");  // Update status
  }

  /**
   * This method is run to cancel unload panel.
   */
  private void doCancelUnloadPanel()
  {
    _unloadPanelBusyDlg.hide();  // hide the dialog

    /** @todo SLA Can we cancel an unload panel? */

    playDead(false);  // enable the interface
    sendStatusMessage("Panel Unload Canceled");  // Update status
  }

  /**
   * Callback for the automatic startup menu item.
   * DOS
   */
  void _startupMI_actionPerformed(ActionEvent e)
  {
    String cmd = "SYSCNTRL.EXE -AUTOSTART";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the short term shutdown menu item.
   */
  void _shortTermMI_actionPerformed(ActionEvent e)
  {
    String cmd = "SYSCNTRL.EXE -SHORTSHUT";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the long term shutdown menu item.
   */
  void _longTermMI_actionPerformed(ActionEvent e)
  {
    String cmd = "SYSCNTRL.EXE -LONGSHUT";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the check cad menu item.
   */
  void _checkcadMI_actionPerformed(ActionEvent e)
  {
//    String cmd = "GOTOCAD.EXE";
    String cmd = "VDZ.EXE /P " + _currentPanelName + " /B " + "1" + "/G";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the adjust view focus menu item.
   */
  void _viewFocusMI_actionPerformed(ActionEvent e)
  {
    String cmd = null;
    if (_currentPanelName == null || _currentBoardName == null)
      cmd = "VDZ.EXE";
    else
      cmd = "VDZ.EXE /P " + _currentPanelName + " /B " + _currentBoardName + " /Z";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the align and map menu item.
   */
  void _alignAndMapMI_actionPerformed(ActionEvent e)
  {
    // Do alignment
    String cmd = "ALIGN.EXE";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
    // Do surface map
    cmd = "SURFMAP.EXE FALSE";
    success = sendDOSShellCommand(dosShell, cmd, null, null);
    // Do board thickness (optional)
    cmd = "BRDTHICK.EXE";
    success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the automatic align menu item.
   */
  void _alignAutoMI_actionPerformed(ActionEvent e)
  {
    String cmd = "ALIGN.EXE";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the manual align menu item.
   */
  void _alignManualMI_actionPerformed(ActionEvent e)
  {
    boolean doAllBoards = false;
    /** @todo SLA Ask for current board or all boards. */
    String cmd = ((doAllBoards) ? "ALIGNCAL.EXE" : "ALIGN.EXE -M");
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the display alignment matrix menu item.
   */
  void _displayMatMI_actionPerformed(ActionEvent e)
  {
    String cmd = "SHOWMAT.EXE";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the automatic surface map menu item.
   */
  void _mapAutoMI_actionPerformed(ActionEvent e)
  {
    // Do surface map
    String cmd = "SURFMAP.EXE FALSE";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
    // Do board thickness (optional)
    cmd = "BRDTHICK.EXE";
    success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the manual surface map menu item.
   */
  void _mapManualMI_actionPerformed(ActionEvent e)
  {
    // Do surface map
    String cmd = "SELFFMAP.EXE";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
    // Do board thickness (optional)
    cmd = "BRDTHICK.EXE";
    success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the check surface map menu item.
   */
  void _checkSurMapMI_actionPerformed(ActionEvent e)
  {
    String cmd = "CHKMAP.EXE";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for all collect workstation images menu items.
   */
  void _workImageAutoMI_actionPerformed(ActionEvent e)
  {
    String cmd = "AUTOTEST.EXE";
    if (e.getSource() == _workImageAutoMI)
      cmd = cmd + " -AUTO";
    else if (e.getSource() == _workImagePanelMI)
      cmd = cmd + " -PANEL";
    else if (e.getSource() == _workImageBoardMI)
      cmd = cmd + " -BOARD";
    else if (e.getSource() == _workImageCompMI)
      cmd = cmd + " -COMP";
    else if (e.getSource() == _workImagePinMI)
      cmd = cmd + " -PIN";
    else if (e.getSource() == _workImageJointMI)
      cmd = cmd + " -FAMILY";
    else if (e.getSource() == _workImageSubtypeMI)
      cmd = cmd + " -SUBTYPE";
    else
      return;  // should never happen
    cmd = cmd + " -OLW_IMAGE_CAPTURE";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the collect diagnostic image menu item.
   */
  void _diagImageMI_actionPerformed(ActionEvent e)
  {
    String cmd = "TDWVUE.EXE -SAVE";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the service menu item.
   */
  void _serviceMI_actionPerformed(ActionEvent e)
  {
    String cmd = "DIAG.EXE";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the cadlink menu item.
   */
  void _cadlinkMI_actionPerformed(ActionEvent e)
  {
    String cmd = "CADLINK.EXE";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the setup surface map points menu item.
   */
  void _mapPointsMI_actionPerformed(ActionEvent e)
  {
    String cmd = "PICKPNTS.EXE";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the verify laser repeatability menu item.
   */
  void _verifyLaserMI_actionPerformed(ActionEvent e)
  {
    String cmd = "LSMREP.EXE";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
    /** @todo SLA Bring results into a dialog for review.  LOGDIR/LSMREP.DAT*/
  }

  /**
   * Callback for the review measurements menu item.
   */
  void _reviewMeasMI_actionPerformed(ActionEvent e)
  {
    String cmd = "ALGMET.EXE";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the defect reporter menu item.
   */
  void _defectReporterMI_actionPerformed(ActionEvent e)
  {
    String cmd = "RPT.EXE";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the home rails menu item.
   */
  void _homeRailsMI_actionPerformed(ActionEvent e)
  {
    if (_machinePanelInspect == null)
    {
      /** @todo SLA report error. */
      MessageDialog.showErrorDialog(this, new LocalizedString("Cannot access system", null), new LocalizedString("Server Access Error", null));
      return;
    }
    playDead(true);  // disable the interface

    _machineThread.invokeLater( new Runnable()
    {
      public void run()
      {
        Menu5DX.this.doHomeRails();
      }
    });
  }

  private void doHomeRails()
  {
    try
    {
      _machinePanelInspect.legacyHomeRails();
    }
    catch (HardwareException he)
    {
    /** @todo SLA Handle the hardware exception */
      MessageDialog.reportException(this, he);
    }
    catch (RemoteException re)
    {
    /** @todo SLA Handle the remote exception */
      MessageDialog.reportException(this, re);
    }

    playDead(false);  // enable the interface
    sendStatusMessage("Home rails complete");  // Update status
  }

  /**
   * Callback for the reset panel handling menu item.
   */
  void _resetPanelHandlingMI_actionPerformed(ActionEvent e)
  {
    if (_machinePanelInspect == null)
    {
      /** @todo SLA report error. */
      MessageDialog.showErrorDialog(this, new LocalizedString("Cannot access system", null), new LocalizedString("Server Access Error", null));
      return;
    }
    playDead(true);  // disable the interface

    _machineThread.invokeLater( new Runnable()
    {
      public void run()
      {
        Menu5DX.this.doResetPanelHandling();
      }
    });
  }

  private void doResetPanelHandling()
  {
    try
    {
      _machinePanelInspect.legacyResetPanelHandling();
    }
    catch (HardwareException he)
    {
      /** @todo SLA Handle the hardware exception */
      MessageDialog.reportException(this, he);
    }
    catch (RemoteException re)
    {
      /** @todo SLA Handle the remote exception */
      MessageDialog.reportException(this, re);
    }

    playDead(false);  // enable the interface
    sendStatusMessage("Panel handling reset complete");  // Update status
  }

  /**
   * Callback for the reset 5dx menu item.
   */
  void _reset5DXMI_actionPerformed(ActionEvent e)
  {
    System.out.println("Reset 5DX");
  }

  /**
   * Callback for the reset IAS menu item.
   */
  void _resetIASMI_actionPerformed(ActionEvent e)
  {
    System.out.println("Reset IAS");
  }

  /**
   * Callback for the collect repair image menu item.
   */
  void _repairImageMI_actionPerformed(ActionEvent e)
  {
    System.out.println("Repair Image");
  }

  /**
   * Callback for the 5dx status menu item.
   */
  void _status5DXMI_actionPerformed(ActionEvent e)
  {
    Status5dxDialog dlg = new Status5dxDialog(null, "5DX Status", true);
    com.agilent.guiUtil.SwingUtils.centerOnComponent(dlg,this);
    dlg.setVisible(true);
  }

  /**
   * Callback for the toolbox menu item.
   */
  void _toolboxMI_actionPerformed(ActionEvent e)
  {
    CandDGUI toolbox = new CandDGUI();
    toolbox.start();
    toolbox.setVisible(true);
  }

  /**
   * Callback for the algorithm tuner menu item.
   */
  void _tunerMI_actionPerformed(ActionEvent e)
  {
    AlgoTunerGUI tuner = new AlgoTunerGUI();
    tuner.start();
    tuner.setVisible(true);
  }

  /**
   * Callback for the x out menu item.
   */
  void _xoutMI_actionPerformed(ActionEvent e)
  {
    String cmd = "XOUT.EXE -SET";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the set stage speed menu item.
   */
  void _stageSpeedMI_actionPerformed(ActionEvent e)
  {
    System.out.println("Set Stage Speed");
// Temporary run test command
    String cmd = "AUTOTEST.EXE -PANEL";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
//
  }

  /**
   * Callback for the manual load panel menu item.
   */
  void _manualLoadPanelMI_actionPerformed(ActionEvent e)
  {
    System.out.println("Manual Load Panel");
  }

  /**
   * Callback for the run self tests menu item.
   */
  void _runSelfTestMI_actionPerformed(ActionEvent e)
  {
    String cmd = "'STINTACT.EXE";
    RemoteDosShellInt dosShell = getDOSShell();
    boolean success = sendDOSShellCommand(dosShell, cmd, null, null);
  }

  /**
   * Callback for the help contents menu item.
   */
  void _helpContentsMI_actionPerformed(ActionEvent e)
  {
    System.out.println("Help Contents");
  }

  /**
   * Callback for the about menu item.
   */
  void _helpAboutMI_actionPerformed(ActionEvent e)
  {
    Menu5DXAboutBox dlg = new Menu5DXAboutBox(this);
    com.agilent.guiUtil.SwingUtils.centerOnComponent(dlg,this);
    dlg.show();
  }

  // **************** MAIN ***************************************
  public static void main(String[] args)
  {
    try
    {
      // Make sure the look and feel is set before any components are created.
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (ClassNotFoundException e) { /* do nothing */ }
    catch (InstantiationException e) { /* do nothing */ }
    catch (IllegalAccessException e) { /* do nothing */ }
    catch (UnsupportedLookAndFeelException e) { /* do nothing */ }

    AssertUtil.setUpAssert("5dxMain", null);
    Menu5DX menu5DX = new Menu5DX();
    menu5DX._startFromMain = true;
    menu5DX.startApp();  // start the application
  }
  private boolean _startFromMain = false;

  private JMenuBar _menuBar = new JMenuBar();
  private JPanel _rootPanel = new JPanel();
  private BorderLayout _rootPanelLayout = new BorderLayout();
  private JMenu _fileMenu = new JMenu();
  private JMenuItem _openPanelMI = new JMenuItem();
  private JMenuItem _logonMI = new JMenuItem();
  private JMenuItem _logoutMI = new JMenuItem();
  private JMenuItem _exitMI = new JMenuItem();
  private JMenu _systemMenu = new JMenu();
  private JMenu _panelMenu = new JMenu();
  private JMenu _programmingMenu = new JMenu();
  private JMenu _toolsMenu = new JMenu();
  private JMenu _helpMenu = new JMenu();
  private JMenuItem _startupMI = new JMenuItem();
  private JMenu _shutdownMenu = new JMenu();
  private JMenuItem _runSelfTestMI = new JMenuItem();
  private JMenuItem _reset5DXMI = new JMenuItem();
  private JMenuItem _resetIASMI = new JMenuItem();
  private JMenuItem _status5DXMI = new JMenuItem();
  private JMenuItem _shortTermMI = new JMenuItem();
  private JMenuItem _longTermMI = new JMenuItem();
  private JMenu _panelHandlingMenu = new JMenu();
  private JMenuItem _homeRailsMI = new JMenuItem();
  private JMenuItem _resetPanelHandlingMI = new JMenuItem();
  private JMenuItem _loadPanelMI = new JMenuItem();
  private JMenuItem _unloadPanelMI = new JMenuItem();
  private JMenuItem _alignAndMapMI = new JMenuItem();
  private JMenu _alignMenu = new JMenu();
  private JMenuItem _alignAutoMI = new JMenuItem();
  private JMenuItem _alignManualMI = new JMenuItem();
  private JMenuItem _displayMatMI = new JMenuItem();
  private JMenu _mapMenu = new JMenu();
  private JMenuItem _mapAutoMI = new JMenuItem();
  private JMenuItem _mapManualMI = new JMenuItem();
  private JMenuItem _checkSurMapMI = new JMenuItem();
  private JMenuItem _verifyLaserMI = new JMenuItem();
  private JMenuItem _tunerMI = new JMenuItem();
  private JMenuItem _cadlinkMI = new JMenuItem();
  private JMenuItem _checkcadMI = new JMenuItem();
  private JMenuItem _xoutMI = new JMenuItem();
  private JMenuItem _mapPointsMI = new JMenuItem();
  private JMenuItem _viewFocusMI = new JMenuItem();
  private JMenuItem _reveiwDefMI = new JMenuItem();
  private JMenuItem _reviewMeasMI = new JMenuItem();
  private JMenuItem _defectReporterMI = new JMenuItem();
  private JMenu _workImageMenu = new JMenu();
  private JMenuItem _workImageAutoMI = new JMenuItem();
  private JMenuItem _workImagePanelMI = new JMenuItem();
  private JMenuItem _diagImageMI = new JMenuItem();
  private JMenuItem _repairImageMI = new JMenuItem();
  private JMenuItem _serviceMI = new JMenuItem();
  private JMenuItem _toolboxMI = new JMenuItem();
  private JMenuItem _optionsMI = new JMenuItem();
  private JMenuItem _helpContentsMI = new JMenuItem();
  private JMenuItem _helpAboutMI = new JMenuItem();
  private JToolBar _5dxToolbar = new JToolBar();
  private JPanel _centerPanel = new JPanel();
  private BorderLayout _centerPanelLayout = new BorderLayout();
  private JSplitPane _splitPane1 = new JSplitPane();
  private JPanel _actionPanel = new JPanel();
  private JPanel _progToolsPanel = new JPanel();
  private JToolBar _programToolbar = new JToolBar();
  private JSplitPane _splitPane2 = new JSplitPane();
  private JPanel _topActionPanel = new JPanel();
  private JPanel _bottomActionPanel = new JPanel();
  private BorderLayout borderLayout1 = new BorderLayout();
  private BorderLayout borderLayout2 = new BorderLayout();
  private BorderLayout borderLayout3 = new BorderLayout();
  private JSplitPane _splitPane3 = new JSplitPane();
  private JMenuItem _newUserMI = new JMenuItem();
  private JMenuItem _changePasswordMI = new JMenuItem();
  private BorderLayout borderLayout4 = new BorderLayout();
  private JMenuItem _workImageBoardMI = new JMenuItem();
  private JMenuItem _workImageCompMI = new JMenuItem();
  private JMenuItem _workImagePinMI = new JMenuItem();
  private JMenuItem _workImageJointMI = new JMenuItem();
  private JMenuItem _workImageSubtypeMI = new JMenuItem();
  private JMenuItem _manualLoadPanelMI = new JMenuItem();
  private JMenuItem _stageSpeedMI = new JMenuItem();

}
