package xRayUITester.wrappers;

import abbot.tester.*;
import abbot.finder.matchers.*;
import abbot.finder.*;

import java.awt.*;
import javax.swing.*;


/**
 * Wraps the underlying JList and provides methods to perform common UI actions against it.
 * @author Erica Wheatcroft
 */
public class JLabelWrapper
{
  JLabel _label = null;
  JLabelTester _listTester = new JLabelTester();

  /**
   * Constructor.
   *
   * @param listName The actual variable name of the JLabel in the UI code.
   * @param container The Container which contains the JButton in question.
   * @author Erica Wheatcroft
   */
  public JLabelWrapper(String labelName, Container container)
  {
    Matcher matcher = new NameMatcher(labelName);
    try
    {
      _label = (JLabel) (BasicFinder.getDefault().find(container, matcher));
    }
    catch (ComponentNotFoundException cnfe)
    {
      /** @todo Expect.expect(false); */
    }
    catch (MultipleComponentsFoundException mcfe)
    {
      /** @todo Expect.expect(false); */
    }
  }

  /**
   * @return String
   * @author Erica Wheatcroft
   */
  public String getText()
  {
    assert _label != null : "label is null";
    assert _listTester != null : "label tester is null";
    return _label.getText();
  }

}
