package xRayUITester.wrappers;

import abbot.finder.matchers.*;
import abbot.finder.*;
import abbot.tester.*;
import java.awt.*;
import java.util.*;
import javax.swing.*;

import com.agilent.util.Expect;


/**
 * Wraps the underlying JList and provides methods to perform common UI actions against it.
 * @author Erica Wheatcroft
 */
public class JListWrapper
{
  JList _list = null;
  JListTester _listTester = new JListTester();

 /**
  * Constructor.
  *
  * @param listName The actual variable name of the JButton in the UI code.
  * @param container The Container which contains the JButton in question.
  * @author Erica Wheatcroft
  */
 public JListWrapper(String listName, Container container)
 {
   Matcher matcher = new NameMatcher(listName);
   try
   {
     _list = (JList)(BasicFinder.getDefault().find(container, matcher));
   }
   catch (ComponentNotFoundException cnfe)
   {
     /** @todo Expect.expect(false); */
   }
   catch (MultipleComponentsFoundException mcfe)
   {
     /** @todo Expect.expect(false); */
   }
 }

 /**
  * @return JList
  * @author Erica Wheatcroft
  */
 public JList getList()
 {
   return _list;
 }

 /**
  * select the object / item at the specified index.
  * @param index the index to select within the JList
  * @author Erica Wheatcroft
  */
 public void selectItemAt(int index)
 {
   Expect.expect( _list != null, "the list component is null");
   Expect.expect(_listTester != null,"the list tester is null");
   Expect.expect(index > -1,"index to select is negative: " + index);
   _listTester.actionSelectIndex(_list, index);
 }

 /**
  * Select the first item in the list matching the given String representation of the item.
  * @param text string representation of the item to select
  * @author Erica Wheatcroft
  */
 public void selectItemAt(String text)
 {
   Expect.expect( _list != null, "the list component is null");
   Expect.expect(_listTester != null,"the list tester is null");
   Expect.expect( text != null, "text to select is null");
   _listTester.actionSelectItem(_list, text);
 }

 /**
  * @return String[] represents the list's contents.
  * @author Erica Wheatcroft
  */
 public String[] getItems()
 {
   Expect.expect( _list != null,"the list component is null");
   Expect.expect( _listTester != null,"the list tester is null");
   return _listTester.getContents(_list);
 }

 /**
  * @param index int
  * @author Erica Wheatcroft
  */
 public Object getElementAt(int index)
 {
   Expect.expect( _list != null,"the list component is null");
   Expect.expect( _listTester != null,"the list tester is null");
   Expect.expect(index > -1, "index to select is negative: " + index);
   return _listTester.getElementAt(_list, index);
 }
}
