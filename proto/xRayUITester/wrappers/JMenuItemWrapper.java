package xRayUITester.wrappers;

import java.awt.*;
import javax.swing.*;

import abbot.finder.*;
import abbot.finder.matchers.*;
import abbot.tester.*;

/**
 * Wraps the underlying JMenuItem and provides methods to perform common UI actions against it.
 *
 * @author Matt Wharton
 * @author Erica Wheatcroft
 */
public class JMenuItemWrapper
{
  JMenuItem _menuItem = null;
  JMenuItemTester _menuItemTester = new JMenuItemTester();

  /**
   * Constructor.
   *
   * @param menuItemName The label of the menu item being wrapped.
   * @param container The container of this menu item (the menu bar).
   * @author Matt Wharton
   */
  public JMenuItemWrapper(String menuItemName, Container container)
  {
    // Erica...FYI...looks like we need to refer to menu items by label if we want to use the JMenuItemMatcher.
    Matcher matcher = new JMenuItemMatcher(menuItemName);
    try
    {
      _menuItem = (JMenuItem)(BasicFinder.getDefault().find(container, matcher));
    }
    catch (ComponentNotFoundException cnfe)
    {
      /** @todo -MDW  Expect.expect(false, "Component " + menuItemName + " was not located."); */
    }
    catch (MultipleComponentsFoundException mcfe)
    {
      /** @todo -MDW  Expect.expect(false, "Multiple instances of component " + menuItemName + " were located."); */
    }
  }

  /**
   * Clicks the underlying JMenuItem.
   *
   * @author Matt Wharton
   */
  public void click()
  {
    _menuItemTester.actionClick(_menuItem);
  }
}
