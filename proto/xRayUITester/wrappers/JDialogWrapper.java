package xRayUITester.wrappers;

import java.awt.*;
import javax.swing.*;

import abbot.finder.*;
import abbot.finder.matchers.*;
import abbot.tester.*;

/**
 * Wraps a JDialog and provides methods to perform common UI actions against it.
 * This is the base from which all specific JPanelWrappers should extend.
 *
 * @todo -MDW this needs more work
 *
 * @author Matt Wharton
 */
public class JDialogWrapper
{
  private JDialog _dialog = null;
  private DialogTester _dialogTester = new DialogTester();

  public JDialogWrapper(String dialogName, Container container)
  {
    Matcher matcher = new NameMatcher(dialogName);
    try
    {
      _dialog = (JDialog)(BasicFinder.getDefault().find(container, matcher));
    }
    catch (ComponentNotFoundException cnfe)
    {
      /** @todo Expect.expect(false); */
    }
    catch (MultipleComponentsFoundException mcfe)
    {
      /** @todo Expect.expect(false); */
    }
  }
}
