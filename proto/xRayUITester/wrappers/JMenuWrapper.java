package xRayUITester.wrappers;

import java.awt.*;
import javax.swing.*;

import abbot.finder.*;
import abbot.finder.matchers.*;
import abbot.tester.*;

/**
 * Wrapper for a JMenu.
 *
 * @author Matt Wharton
 * @author Erica Wheatcroft
 */
public class JMenuWrapper
{
  private JMenu _menu = null;
  private ComponentTester _menuTester = new ComponentTester();


  public JMenuWrapper(String name, Container container)
  {
    Matcher matcher = new NameMatcher(name);
    try
    {
      _menu = (JMenu)(BasicFinder.getDefault().find(container, matcher));
    }
    catch (ComponentNotFoundException cnfe)
    {
      /** @todo -MDW Expect.expect(false, "Cannot find JMenu " + name + "!"); */
    }
    catch (MultipleComponentsFoundException mcfe)
    {
      /** @todo -MDW Expect.expect(false, "Multiple instances of JMenu " + name + " found."); */
    }
  }

  /**
   * Sends a click to the underlying JMenu.
   *
   * @author Matt Wharton
   */
  public void click()
  {
    _menuTester.actionClick(_menu);
  }
}
