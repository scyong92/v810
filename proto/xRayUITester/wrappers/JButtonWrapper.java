package xRayUITester.wrappers;

import java.awt.*;
import javax.swing.*;

import abbot.finder.*;
import abbot.finder.matchers.*;
import abbot.tester.*;

/**
 * Wraps the underlying JButton and provides methods to perform common UI actions against it.
 *
 * @author Matt Wharton
 * @author Erica Wheatcroft
 */
public class JButtonWrapper
{
  JButton _button = null;
  JButtonTester _buttonTester = new JButtonTester();

  /**
   * Constructor.
   *
   * @param buttonName The actual variable name of the JButton in the UI code.
   * @param container The Container which contains the JButton in question.
   * @author Matt Wharton
   * @author Erica Wheatcroft
   */
  public JButtonWrapper(String buttonName, Container container)
  {
    Matcher matcher = new NameMatcher(buttonName);
    try
    {
      _button = (JButton)(BasicFinder.getDefault().find(container, matcher));
    }
    catch (ComponentNotFoundException cnfe)
    {
      /** @todo Expect.expect(false); */
    }
    catch (MultipleComponentsFoundException mcfe)
    {
      /** @todo Expect.expect(false); */
    }
  }

  /**
   * Single clicks the underlying JButton.
   *
   * @author Matt Wharton
   */
  public void click()
  {
    _buttonTester.actionClick(_button);
  }

  /**
   * Gets the textual label of the underlying JButton.
   *
   * @return the text on the underlying JButton.
   * @author Matt Wharton
   */
  public String getText()
  {
    return _button.getText();
  }

  /**
   * Gets the standard tooltip text for the underlying JButton.
   *
   * @return the underlying tooltip text for the underlying JButton.
   * @author Matt Wharton
   */
  public String getTooltipText()
  {
    return _button.getToolTipText();
  }
}
