package xRayUITester.wrappers;

import java.awt.*;
import javax.swing.*;

import abbot.finder.*;

/**
 * Wrapper for the Test Development Environment panel.
 *
 * @author Matt Wharton
 * @author Erica Wheatcroft
 */
public class TestDevelopmentEnvironmentPanelWrapper extends JPanelWrapper
{
  // Container which holds this panel.
  private Container _container = null;

  // Panel name.
  String _panelName = null;

  private JMenuBarWrapper _menuBar = null;
  private JMenuWrapper _fileMenu = null;
  private JMenuItemWrapper _importProjectMenuItem = null;

  /**
   * Constructor.
   *
   * @param panelName The name of the panel.
   * @param container The container of this panel.
   * @author Matt Wharton
   */
  public TestDevelopmentEnvironmentPanelWrapper(String panelName, Container container)
  {
    _panelName = panelName;
    _container = container;
    BorderLayout layout = (BorderLayout)((JFrame)container).getContentPane().getLayout();
    _panel = (JPanel)layout.getLayoutComponent(BorderLayout.CENTER);
    _menuBar = new JMenuBarWrapper("_testDevMenu", _container);
  }

  /**
   * Gets a wrapper for the "File" menu.
   *
   * @return a JMenuWrapper for the File menu.
   * @author Matt Wharton
   */
  public JMenuWrapper getFileMenu()
  {
    if (_fileMenu == null)
    {
      _fileMenu = new JMenuWrapper("_fileMenu", _menuBar.getMenuBar());
    }

    return _fileMenu;
  }

  /**
   * Gets a wrapper for the "File->Import Project" menu item.
   *
   * @return a JMenuItemWrapper for the "File->Import Project" menu item.
   * @author Matt Wharton
   */
  public JMenuItemWrapper getImportProjectMenuItem()
  {
    if (_importProjectMenuItem == null)
    {
      // Erica...FYI...looks like we need to refer to menu items by label if we want to use the JMenuItemMatcher.
      _importProjectMenuItem = new JMenuItemWrapper("Import", _menuBar.getMenuBar());
    }

    return _importProjectMenuItem;
  }
}
