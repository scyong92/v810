package xRayUITester.wrappers;

import java.awt.*;
import javax.swing.*;

import abbot.finder.*;
import abbot.finder.matchers.*;
import abbot.tester.*;

/**
 * Wraps a JPanel and provides methods to perform common UI actions against it.
 * This is the base from which all specific JPanelWrappers should extend.
 *
 * @author Matt Wharton
 */
public class JPanelWrapper
{
  protected JPanel _panel = null;
  protected ComponentTester _panelTester = new ComponentTester();

  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  protected JPanelWrapper()
  {
    // Do nothing...
  }

  /**
   * Constructor.
   *
   * @param panelName The panel name to bind to.
   * @param container The Container in which to search for the panel.
   * @author Matt Wharton
   */
  public JPanelWrapper(String panelName, Container container)
  {
    Matcher matcher = new NameMatcher(panelName);
    try
    {
      _panel = (JPanel)(BasicFinder.getDefault().find(container, matcher));
    }
    catch (ComponentNotFoundException cnfe)
    {
      /** @todo Expect.expect(false); */
    }
    catch (MultipleComponentsFoundException mcfe)
    {
      /** @todo Expect.expect(false); */
    }
  }
}
