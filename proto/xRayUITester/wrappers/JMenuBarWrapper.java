package xRayUITester.wrappers;

import java.awt.*;
import javax.swing.*;

import abbot.finder.*;
import abbot.finder.matchers.*;
import abbot.tester.*;

/**
 * Wraps a JMenuBar.
 *
 * @author Matt Wharton
 * @author Erica Wheatcroft
 */
public class JMenuBarWrapper
{
  private JMenuBar _menuBar = null;
  private ComponentTester _menuBarTester = new ComponentTester();

  /**
   * Constructor.
   *
   * @param name The name property of the underlying JMenuBar.
   * @param container The Container of the underlying JMenuBar.
   * @author Matt Wharton
   */
  public JMenuBarWrapper(String name, Container container)
  {
    Matcher matcher = new NameMatcher(name);
    try
    {
      _menuBar = (JMenuBar)(BasicFinder.getDefault().find(container, matcher));
    }
    catch (ComponentNotFoundException cnfe)
    {
      /** @todo -MDW Expect.expect(false, "Cannot find JMenuBar " + name + "!"); */
    }
    catch (MultipleComponentsFoundException mcfe)
    {
      /** @todo -MDW Expect.expect(false, "Multiple instances of JMenuBar " + name + " found."); */
    }
  }

  /**
   * Gets a reference to the underlying JMenuBar.
   *
   * @return a reference to the underlying JMenuBar.
   * @author Matt Wharton
   */
  public JMenuBar getMenuBar()
  {
    return _menuBar;
  }
}
