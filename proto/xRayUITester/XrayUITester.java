package xRayUITester;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import abbot.finder.*;
import abbot.finder.matchers.*;
import abbot.tester.*;
import xRayUITester.wrappers.*;

/**
 * Main class for UI automation framework.
 *
 * @author Matt Wharton
 * @author Erica Wheatcroft
 */
public class XrayUITester
{
  private Window _mainMenuWindow = null;
  private final String _mainMenuTitleRegex = "/Agilent Test.*/";

  private TestDevelopmentEnvironmentPanelWrapper _testDevelopmentEnvironmentPanel = null;

  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  public XrayUITester()
  {
    // Do nothing...
  }

  /**
   * Fires up the main UI window and binds to it.
   *
   * @param initializationDelay Amount of time (in ms) to wait before attempting to latch to the main window.
   * @author Matt Wharton
   */
  public void startUI(long initializationDelay)
  {
    // Just run the main.
    com.agilent.xRayTest.gui.mainMenu.MainMenuGui.main(null);

    // Give it a few seconds to initialize.
    try
    {
      Thread.sleep(initializationDelay);
    }
    catch (InterruptedException ie)
    {
      /** @todo Expect.expect(false); */
    }

    try
    {
      // Attempt to bind to the main UI.
      Matcher matcher = new WindowMatcher(_mainMenuTitleRegex, true);
      _mainMenuWindow = (JFrame)(BasicFinder.getDefault().find(matcher));
    }
    catch (ComponentNotFoundException cnfe)
    {
      /** @todo -MDW Expect.expect(false, "Unable to bind to main window!  Window not found."); */
    }
    catch (MultipleComponentsFoundException mcfe)
    {
      /** @todo -MDW Expect.expect(false, "Unable to bind to main window!  Multiple instances found."); */
    }

    /**@todo -MDW remove */
//    WindowTester windowTester = new WindowTester();
//    windowTester.actionKeyPress(KeyEvent.VK_CONTROL);
//    windowTester.actionKeyPress(_mainMenuWindow, KeyEvent.VK_I);
//    windowTester.delay(3000);
//    Matcher importDialogMatcher = new WindowMatcher("/.*Project.*/");
//    Window importProjectDialog = null;
//    try
//    {
//      importProjectDialog = (Window)(BasicFinder.getDefault().find(
//          _mainMenuWindow,
//          importDialogMatcher));
//    }
//    catch (ComponentNotFoundException cnfe)
//    {
//      return;
//    }
//    catch (MultipleComponentsFoundException mcfe)
//    {
//      return;
//    }
//    windowTester.actionKeyPress(importProjectDialog, KeyEvent.VK_ESCAPE);
//    windowTester.delay(3000);
  }

  /**
   * Unbinds from the main UI and takes it down.
   *
   * @author Matt Wharton
   */
  public void tearDownUI()
  {
    _mainMenuWindow = null;
    com.agilent.xRayTest.gui.mainMenu.MainMenuGui.getInstance().endMainMenu();
  }

  /**
   * Gets a reference to the Test Development Environment panel wrapper.
   *
   * @return a reference to the Test Development Environment panel wrapper.
   * @author Matt Wharton
   */
  public TestDevelopmentEnvironmentPanelWrapper getTestDevelopmentEnvironmentPanel()
  {
    if (_testDevelopmentEnvironmentPanel == null)
    {
      _testDevelopmentEnvironmentPanel = new TestDevelopmentEnvironmentPanelWrapper(
          "_mainPanel",
          _mainMenuWindow);
    }

    return _testDevelopmentEnvironmentPanel;
  }

  public static void main(String[] args)
  {
    XrayUITester uiTester = new XrayUITester();

    uiTester.startUI(7000);

    TestDevelopmentEnvironmentPanelWrapper tdePanel = uiTester.getTestDevelopmentEnvironmentPanel();
    tdePanel.getFileMenu().click();
    tdePanel.getImportProjectMenuItem().click();

    //uiTester.tearDownUI();
  }
}
