package proto.debuggerGUI;

import java.awt.*;
import javax.swing.*;
import java.awt.Component;
import javax.swing.table.TableCellRenderer;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

class CustomerTableRenderer extends JLabel implements TableCellRenderer
{

  public CustomerTableRenderer()
  {
  }

  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    setOpaque(true);
    setHorizontalAlignment(JLabel.CENTER);
    if (isSelected)
    {
      setForeground(table.getSelectionForeground());
      setBackground(table.getSelectionBackground());
    }
    else
    {
      setForeground(Color.black);
      setBackground(Color.white);
      if (row == 13)
        setBackground(new Color(181, 228, 255));
      else
        if (row < 25)
          setBackground(SystemColor.info);
    }
    if (value != null)
    {
      if (value instanceof Float)
      {
        String floatStr = value.toString();

        setText(floatStr.substring(0, floatStr.indexOf(".") + 2));
      }
      else
        setText(value.toString());
    }
    return this;
  }
}
