package proto.debuggerGUI;

import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.util.*;
import javax.swing.*;
import com.borland.jbcl.layout.*;
import javax.swing.border.*;

import proto.algoTuner.*;
import proto.surfaceMapSetupGUI.*;
import com.agilent.guiUtil.*;
import java.io.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class DebuggerFrame extends JFrame
{
  private static int HOME_SCREEN = 1;
  private static int SURFACEMAP_SCREEN = 2;
  private static int TUNER_SCREEN = 3;
  private static int VIEWVERIFER_SCREEN = 4;

  private int _currentScreen = HOME_SCREEN;
  private String laserImageURL;
  private String xrayImageURL;

  private JPanel contentPane;
  private JMenuBar jMenuBar1 = new JMenuBar();
  private JMenu jMenuFile = new JMenu();
  private JMenuItem jMenuFileExit = new JMenuItem();
  private JMenu jMenuHelp = new JMenu();
  private JMenuItem jMenuHelpAbout = new JMenuItem();
  private JToolBar jToolBar = new JToolBar();
  private ImageIcon image1;
  private ImageIcon image2;
  private ImageIcon image3;
  private ImageIcon zoomInimage;
  private ImageIcon zoomOutimage;
  private ImageIcon panimage;
  private JMenu jMenu1 = new JMenu();
  private JMenu jMenu2 = new JMenu();
  private JMenu jMenu3 = new JMenu();
  private JMenuItem jMenuItem1 = new JMenuItem();
  private JMenuItem jMenuItem2 = new JMenuItem();
  private JMenuItem jMenuItem3 = new JMenuItem();
  private JMenuItem jMenuItem4 = new JMenuItem();
  private JMenuItem jMenuItem5 = new JMenuItem();
  private JMenuItem jMenuItem6 = new JMenuItem();
  private JMenuItem jMenuItem7 = new JMenuItem();
  private JMenuItem jMenuItem8 = new JMenuItem();
  private JMenuItem jMenuItem9 = new JMenuItem();
  private JMenuItem jMenuItem10 = new JMenuItem();
  private JMenuItem jMenuItem11 = new JMenuItem();
  private JMenuItem jMenuItem12 = new JMenuItem();
  private JMenuItem jMenuItem13 = new JMenuItem();
  private JPanel jPanel1 = new JPanel();
  private JButton jButton3 = new JButton();
  private JButton jButton2 = new JButton();
  private XYLayout xYLayout1 = new XYLayout();
  private JSplitPane jSplitPane1 = new JSplitPane();
  private JPanel surfaceMapSetupPanel = new JPanel();
  private JPanel tunerPanel = new JPanel();
  private JPanel surfaceMapPanel = new JPanel();
  private JPanel mainDebuggerPanel = new JPanel();
  private JPanel jPanel3 = new JPanel();
  private XYLayout xYLayout3 = new XYLayout();
  private JSplitPane jSplitPane2 = new JSplitPane();
  private BorderLayout borderLayout1 = new BorderLayout();
  private BorderLayout tunerPanelBorderLayout = new BorderLayout();
  private BorderLayout surfaceMapPanelBorderLayout = new BorderLayout();
  private JPanel jPanel4 = new JPanel();
  private JPanel jPanel5 = new JPanel();
  private JEditorPane jEditorPane1 = new JEditorPane();
  private Border border1;
  private JEditorPane jEditorPane2 = new JEditorPane();
  private JScrollPane jScrollPane1 = new JScrollPane();
  private JSortTable jTable1 = null;
  private JLabel statusBar = new JLabel();
  private ButtonGroup buttonGroup1 = new ButtonGroup();
  private BorderLayout borderLayout2 = new BorderLayout();
  private BorderLayout borderLayout3 = new BorderLayout();
  private JToolBar jToolBar1 = new JToolBar();
  private JPanel jPanel6 = new JPanel();
  private JComboBox componentZoomComboBox = new JComboBox();
  private JButton jButton9 = new JButton();
  private JButton jButton7 = new JButton();
  private JButton jButton6 = new JButton();
  private JButton panButton = new JButton();
  private JButton zoomInButton = new JButton();
  private JButton jButton8 = new JButton();
  private JButton zoomOutButton = new JButton();
  private FlowLayout flowLayout1 = new FlowLayout();
  private BorderLayout borderLayout4 = new BorderLayout();
  private JToolBar jToolBar2 = new JToolBar();
  private JPanel jPanel7 = new JPanel();
  private FlowLayout flowLayout2 = new FlowLayout();
  private JButton jButton1 = new JButton();
  private JButton jButton4 = new JButton();
  private JCheckBox jCheckBox2 = new JCheckBox();
  private JCheckBox jCheckBox3 = new JCheckBox();
  private JButton jButton5 = new JButton();
  private JButton jButton10 = new JButton();
  private JButton jButton11 = new JButton();
  private JPanel jPanel8 = new JPanel();
  private JScrollPane jScrollPane2 = new JScrollPane();
  private JScrollPane jScrollPane3 = new JScrollPane();
  private JButton adjustPointsButton = new JButton();
  private XYLayout xYLayout2 = new XYLayout();
  private JButton jButton13 = new JButton();
  private JButton jButton14 = new JButton();
  private JButton jButton15 = new JButton();
  private JButton jButton16 = new JButton();
  private JButton jButton17 = new JButton();
  private JPanel jPanel9 = new JPanel();
  private JLabel jLabel1 = new JLabel();
  private XYLayout xYLayout4 = new XYLayout();
  private JLabel jLabel2 = new JLabel();
  private JLabel jLabel3 = new JLabel();
  private JLabel jLabel4 = new JLabel();
  private JPanel jPanel14 = new JPanel();
  private JPanel jPanel15 = new JPanel();
  private JPanel jPanel16 = new JPanel();
  private JPanel surfaceMapAdjustPointsPanel = new JPanel();
  private XYLayout xYLayout6 = new XYLayout();
  private JButton adjustPointsNextPointButton = new JButton();
  private JButton adjustPointsPrevPointButton = new JButton();
  private JButton adjustPointsGotoPointsButton = new JButton();
  private JTextField adjustPointsGoToTextField = new JTextField();
  private JButton adjustPointsDoneButton = new JButton();
  private JButton adjustPointsCancelButton = new JButton();
  private JLabel jLabel5 = new JLabel();
  private JPanel jPanel10 = new JPanel();
  private XYLayout xYLayout5 = new XYLayout();
  private JLabel jLabel6 = new JLabel();
  private JLabel jLabel7 = new JLabel();
  private JLabel jLabel8 = new JLabel();
  private JLabel jLabel9 = new JLabel();
  private JPanel navigationPanel = new JPanel();
  private Border border2;
  private TitledBorder titledBorder1;
  private JPanel homeWorkSpacePanel = new JPanel();
  private JLabel jLabel17 = new JLabel();
  private JLabel jLabel16 = new JLabel();
  private JLabel jLabel15 = new JLabel();
  private JLabel jLabel14 = new JLabel();
  private JLabel jLabel13 = new JLabel();
  private JLabel jLabel12 = new JLabel();
  private JLabel jLabel11 = new JLabel();
  private JLabel jLabel10 = new JLabel();
  private JPanel jPanel11 = new JPanel();
  private XYLayout xYLayout9 = new XYLayout();
  private JLabel jLabel19 = new JLabel();
  private JLabel jLabel18 = new JLabel();
  private BorderLayout borderLayout5 = new BorderLayout();
  private Border border3;
  private JPanel viewVerifierPanel = new JPanel();
  private BorderLayout borderLayout6 = new BorderLayout();
  private JEditorPane viewVerifierEditorPane = new JEditorPane();
  private Border border4;
  private JPanel jPanel2 = new JPanel();
  private JEditorPane instantHelpEditorPane = new JEditorPane();
  private BorderLayout borderLayout7 = new BorderLayout();
  private Border border5;
  private JPanel jPanel12 = new JPanel();
  private GridLayout gridLayout2 = new GridLayout();
  private com.borland.jbcl.layout.VerticalFlowLayout verticalFlowLayout1 = new com.borland.jbcl.layout.VerticalFlowLayout();
  private JButton surfaceMapSetupButton = new JButton();
  private JButton algorithmTunerButton = new JButton();
  private JButton homeButton = new JButton();
  private JButton viewVerifierButton = new JButton();

  //Construct the frame
  public DebuggerFrame()
  {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  //Component initialization
  private void jbInit() throws Exception
  {
    image1 = new ImageIcon(DebuggerFrame.class.getResource("openFile.gif"));
    image2 = new ImageIcon(DebuggerFrame.class.getResource("closeFile.gif"));
    image3 = new ImageIcon(DebuggerFrame.class.getResource("help.gif"));
    zoomInimage = new ImageIcon(DebuggerFrame.class.getResource("palette_zoomin.gif"));
    zoomOutimage = new ImageIcon(DebuggerFrame.class.getResource("palette_zoomout.gif"));
    panimage = new ImageIcon(DebuggerFrame.class.getResource("palette_hand.gif"));

    contentPane = (JPanel) this.getContentPane();
    border1 = BorderFactory.createLineBorder(Color.black,3);
    border2 = BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(93, 93, 93),new Color(134, 134, 134));
    titledBorder1 = new TitledBorder(border2,"Panel Information", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 16));
    border3 = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,new Color(182, 182, 182),new Color(62, 62, 62),new Color(89, 89, 89)),BorderFactory.createEmptyBorder(10,15,10,15));
    border4 = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED,Color.white,Color.white,new Color(124, 124, 124),new Color(178, 178, 178)),BorderFactory.createEmptyBorder(3,0,0,0));
    border5 = BorderFactory.createMatteBorder(3,0,0,0,SystemColor.controlText);
    contentPane.setLayout(borderLayout2);
    this.setSize(new Dimension(1550, 1150));
    this.setTitle("Debugger Proto");
    jMenuFile.setMnemonic('F');
    jMenuFile.setText("File");
    jMenuFileExit.setMnemonic('x');
    jMenuFileExit.setText("Exit");
    jMenuFileExit.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jMenuFileExit_actionPerformed(e);
      }
    });
    jMenuHelp.setMnemonic('H');
    jMenuHelp.setText("Help");
    jMenuHelpAbout.setText("About");
    jMenuHelpAbout.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jMenuHelpAbout_actionPerformed(e);
      }
    });
    jMenu1.setMnemonic('E');
    jMenu1.setText("Edit");
    jMenu2.setMnemonic('V');
    jMenu2.setText("View");
    jMenu3.setMnemonic('T');
    jMenu3.setText("Tools");
    jMenuItem1.setMnemonic('S');
    jMenuItem1.setText("Save");
    jMenuItem2.setText("Zoom In");
    jMenuItem3.setText("Zoom Out");
    jMenuItem4.setText("Top Side Only");
    jMenuItem5.setText("Bottom Side Only");
    jMenuItem6.setText("Both Sides");
    jMenuItem7.setText("Add Xray Map Point");
    jMenuItem8.setText("Delete Point(s)");
    jMenuItem9.setText("Add Points");
    jMenuItem10.setText("Adjust Points");
    jMenuItem11.setText("Calculate Offset");
    jMenuItem12.setText("Configure Offsets");
    jMenuItem13.setText("Verify Focus");
    jButton3.setIcon(image3);
    jButton3.setToolTipText("Help");
    jButton2.setIcon(image2);
    jButton2.setToolTipText("Close File");
    jPanel1.setLayout(xYLayout1);
    tunerPanel.setLayout(tunerPanelBorderLayout);
    surfaceMapPanel.setLayout(surfaceMapPanelBorderLayout);
    mainDebuggerPanel.setLayout(borderLayout5);
    mainDebuggerPanel.setBorder(BorderFactory.createLineBorder(Color.black));
    surfaceMapSetupPanel.setLayout(xYLayout3);
    surfaceMapSetupPanel.setBorder(BorderFactory.createLineBorder(Color.black));
    jPanel3.setLayout(borderLayout1);
    jPanel3.setBorder(BorderFactory.createLineBorder(Color.black));
    jSplitPane2.setOrientation(JSplitPane.VERTICAL_SPLIT);
    jPanel4.setLayout(borderLayout4);
    jEditorPane1.setBorder(BorderFactory.createLineBorder(Color.black));
    jEditorPane1.setContentType("text/html");
    laserImageURL = "file:" + System.getProperty("user.dir") + System.getProperty("file.separator") + "laser640.html";
    xrayImageURL = "file:" + System.getProperty("user.dir") + System.getProperty("file.separator") + "xray1024.html";
    String cadGraphicsImageURL = "file:" + System.getProperty("user.dir") + System.getProperty("file.separator") + "cadgraphics.html";
    URL laserURL = new URL(laserImageURL);
    URL xrayURL = new URL(xrayImageURL);
    jEditorPane1.setPage(xrayImageURL);
//    jEditorPane1.setText("<html><img SRC=\"laser640.jpg\" height=480 width=640></html>");
//    jEditorPane1.setText("<center><h1>X-Ray and Laser Images</h1></center>");
    jPanel5.setLayout(borderLayout3);
    jEditorPane2.setContentType("text/html");
    jEditorPane2.setBorder(null);
    jEditorPane2.setPage(cadGraphicsImageURL);
    navigationPanel.setBackground(Color.gray);
    navigationPanel.setBorder(border3);
    navigationPanel.setLayout(verticalFlowLayout1);
    jLabel17.setFont(new java.awt.Font("Dialog", 1, 12));
    jLabel17.setText("BGA2:  3245 Joints");
    jLabel16.setFont(new java.awt.Font("Dialog", 1, 12));
    jLabel16.setText("FPGullwing:  3295 Joints");
    jLabel15.setFont(new java.awt.Font("Dialog", 1, 12));
    jLabel15.setText("Chip:  5402 Joints");
    jLabel14.setFont(new java.awt.Font("Dialog", 1, 12));
    jLabel14.setText("Test information:");
    jLabel13.setFont(new java.awt.Font("Dialog", 1, 16));
    jLabel13.setText("Number Tested Joints:  15,983 (95.2%)");
    jLabel12.setFont(new java.awt.Font("Dialog", 1, 16));
    jLabel12.setText("Number of Joints:  16,786");
    jLabel11.setFont(new java.awt.Font("Dialog", 1, 16));
    jLabel11.setText("Current Board: 1");
    jLabel10.setFont(new java.awt.Font("Dialog", 1, 16));
    jLabel10.setText("Panel Name:   E7890-4589");
    jPanel11.setFont(new java.awt.Font("Dialog", 1, 12));
    jPanel11.setBorder(titledBorder1);
    jPanel11.setLayout(xYLayout9);
    jLabel19.setFont(new java.awt.Font("Dialog", 1, 12));
    jLabel19.setText("Connector:  342 Joints");
    jLabel18.setFont(new java.awt.Font("Dialog", 1, 12));
    jLabel18.setText("RES:  2109 Joints");
    AlgoTunerFrame tunerFrame = new AlgoTunerFrame();
    SurfaceMapSetupFrame surfaceMapFrame = new SurfaceMapSetupFrame();
    //Validate frames that have preset sizes
    //Pack frames that have useful preferred size info, e.g. from their layout
    tunerFrame.pack();
    tunerFrame.setCustomizations();
    tunerFrame.pack();
    tunerFrame.setDividerLocation();

    surfaceMapFrame.pack();

    viewVerifierPanel.setLayout(borderLayout6);
    viewVerifierEditorPane.setContentType("text/html");
    String viewVerifierFunctionalityURL = "file:" + System.getProperty("user.dir") + System.getProperty("file.separator") + "viewverifier.html";
    viewVerifierEditorPane.setPage(viewVerifierFunctionalityURL);
    instantHelpEditorPane.setContentType("text/html");
    instantHelpEditorPane.setBorder(null);
    instantHelpEditorPane.setText("<html><h1><center>Instant Help</center></h1>");
    jPanel2.setLayout(borderLayout7);
    jPanel2.setBorder(border5);
    surfaceMapAdjustPointsPanel.setBackground(Color.gray);
    adjustPointsGotoPointsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustPointsGotoPointsButton_actionPerformed(e);
      }
    });
    adjustPointsGoToTextField.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustPointsGoToTextField_actionPerformed(e);
      }
    });

    jButton13.setEnabled(false);
    jButton15.setEnabled(false);
    jButton16.setEnabled(false);
    jButton17.setEnabled(false);
    jButton14.setEnabled(false);
    jPanel12.setLayout(gridLayout2);
    surfaceMapSetupButton.setText("<html><center>Surface<P>Map<P>Setup</center>");
    surfaceMapSetupButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        surfaceMapSetupButton_actionPerformed(e);
      }
    });
    algorithmTunerButton.setText("<html><center>Algorithm<P>Tuner</center></html>");
    algorithmTunerButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        algorithmTunerButton_actionPerformed(e);
      }
    });
    homeButton.setText("<html>Home");
    homeButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        homeButton_actionPerformed(e);
      }
    });
    viewVerifierButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        viewVerifierButton_actionPerformed(e);
      }
    });
    viewVerifierButton.setText("<html><center>View<P>Verifier</center></html>");
    gridLayout2.setColumns(1);
    gridLayout2.setRows(4);
    gridLayout2.setVgap(15);
    jPanel12.setBackground(Color.gray);
    tunerPanel.add(tunerFrame.getAlgoTunerPanel(), BorderLayout.CENTER);
    surfaceMapPanel.add(surfaceMapFrame.getSurfaceMapSetupPanel(), BorderLayout.CENTER);

    jPanel4.setBorder(BorderFactory.createEtchedBorder());
    statusBar.setBorder(BorderFactory.createEtchedBorder());
    statusBar.setText(" ");
    componentZoomComboBox.setPreferredSize(new Dimension(60, 25));
    jButton9.setText("Go!");
    jButton7.setText("Bottom Side");
    jButton6.setText("Top Side");
    panButton.setIcon(panimage);
    zoomInButton.setIcon(zoomInimage);
    jButton8.setText("Both Sides");
    zoomOutButton.setIcon(zoomOutimage);
    jPanel6.setLayout(flowLayout1);
    flowLayout1.setAlignment(FlowLayout.LEFT);
    jPanel7.setLayout(flowLayout2);
    flowLayout2.setAlignment(FlowLayout.LEFT);
    jButton1.setText("Live/Snap");
    jButton4.setText("Stage Motion");
    jCheckBox2.setText("Board Lights");
    jCheckBox3.setText("Laser Lights");
    jButton5.setText("Green Cursor");
    jButton10.setText("Red Cursor");
    jButton11.setText("Camera Settings");
    jPanel8.setBorder(BorderFactory.createEtchedBorder());
    jPanel8.setLayout(xYLayout2);
    adjustPointsButton.setText("Adjust Points");
    adjustPointsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustPointsButton_actionPerformed(e);
      }
    });
    jButton13.setText("Add Points");
    jButton14.setText("Delete Points");
    jButton15.setText("Configure Offsets");
    jButton16.setText("Calculate Offsets");
    jButton17.setText("Verify Focus");
    jPanel9.setBorder(BorderFactory.createLineBorder(Color.black));
    jPanel9.setLayout(xYLayout4);
    jLabel1.setText("Legend:");
    jLabel2.setText("Unadjusted");
    jLabel3.setText("Adjusted");
    jLabel4.setText("Reference Point");
    jPanel14.setBackground(Color.white);
    jPanel14.setBorder(BorderFactory.createLineBorder(Color.black));
    jPanel15.setBackground(SystemColor.info);
    jPanel15.setBorder(BorderFactory.createLineBorder(Color.black));
    jPanel16.setBackground(new Color(181, 228, 255));
    jPanel16.setBorder(BorderFactory.createLineBorder(Color.black));
    surfaceMapAdjustPointsPanel.setBorder(BorderFactory.createLoweredBevelBorder());
    surfaceMapAdjustPointsPanel.setLayout(xYLayout6);
    xYLayout6.setWidth(496);
    xYLayout6.setHeight(314);
    adjustPointsNextPointButton.setText("Next Point");
    adjustPointsNextPointButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustPointsNextPointButton_actionPerformed(e);
      }
    });
    adjustPointsPrevPointButton.setText("Prev Point");
    adjustPointsPrevPointButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustPointsPrevPointButton_actionPerformed(e);
      }
    });
    adjustPointsGotoPointsButton.setText("Go to Point");
    adjustPointsDoneButton.setText("Done");
    adjustPointsDoneButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustPointsDoneButton_actionPerformed(e);
      }
    });
    adjustPointsCancelButton.setText("Cancel");
    adjustPointsCancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustPointsCancelButton_actionPerformed(e);
      }
    });
    jLabel5.setFont(new java.awt.Font("Dialog", 1, 16));
    jLabel5.setText("Adjust Points:");
    jPanel10.setBorder(BorderFactory.createLineBorder(Color.black));
    jPanel10.setLayout(xYLayout5);
    jLabel6.setText("Move stage to adjust point:");
    jLabel7.setText("Point #");
    jLabel8.setText("X:");
    jLabel9.setText("Y:");
    xYLayout3.setWidth(496);
    xYLayout3.setHeight(1040);
    jToolBar.add(jPanel1, null);



    jPanel1.add(jButton2,  new XYConstraints(5, 5, -1, -1));
    jPanel1.add(jButton3,  new XYConstraints(61, 5, -1, -1));


    jMenuFile.add(jMenuItem1);
    jMenuFile.add(jMenuFileExit);
    jMenuHelp.add(jMenuHelpAbout);
    jMenuBar1.add(jMenuFile);
    jMenuBar1.add(jMenu1);
    jMenuBar1.add(jMenu2);
    jMenuBar1.add(jMenu3);
    jMenuBar1.add(jMenuHelp);
    this.setJMenuBar(jMenuBar1);
    contentPane.add(jToolBar, BorderLayout.NORTH);
    contentPane.add(jSplitPane1, BorderLayout.CENTER);
    jSplitPane1.add(jPanel3, JSplitPane.RIGHT);
    jPanel3.add(jSplitPane2, BorderLayout.CENTER);
    jSplitPane2.add(jPanel5, JSplitPane.BOTTOM);
    jPanel5.add(jToolBar1,  BorderLayout.NORTH);
    jPanel5.add(jScrollPane3,  BorderLayout.CENTER);
    jPanel5.add(jPanel2, BorderLayout.SOUTH);
    jPanel2.add(instantHelpEditorPane, BorderLayout.CENTER);
    jScrollPane3.getViewport().add(jEditorPane2, null);
    jToolBar1.add(jPanel6, null);
    jPanel6.add(zoomInButton, null);
    jPanel6.add(zoomOutButton, null);
    jPanel6.add(panButton, null);
    jPanel6.add(jButton6, null);
    jPanel6.add(jButton7, null);
    jPanel6.add(jButton8, null);
    jPanel6.add(componentZoomComboBox, null);
    jPanel6.add(jButton9, null);
    jSplitPane2.add(jPanel4, JSplitPane.TOP);
    jPanel4.add(jToolBar2,  BorderLayout.NORTH);
    jPanel4.add(jScrollPane2,  BorderLayout.CENTER);
    jScrollPane2.getViewport().add(jEditorPane1, null);
    jToolBar2.add(jPanel7, null);
    jPanel7.add(jButton4, null);
    jPanel7.add(jButton11, null);
    jPanel7.add(jButton1, null);
    jPanel7.add(jButton5, null);
    jPanel7.add(jButton10, null);
    jPanel7.add(jCheckBox3, null);
    jPanel7.add(jCheckBox2, null);
//    jSplitPane1.add(surfaceMapSetupPanel, JSplitPane.LEFT);
    jSplitPane1.add(mainDebuggerPanel, JSplitPane.LEFT);
    mainDebuggerPanel.add(navigationPanel, BorderLayout.WEST);
    navigationPanel.add(jPanel12, null);
    jPanel12.add(homeButton, null);
    jPanel12.add(surfaceMapSetupButton, null);
    jPanel12.add(viewVerifierButton, null);
    jPanel12.add(algorithmTunerButton, null);
    surfaceMapSetupPanel.add(jScrollPane1, new XYConstraints(12, 17, 313, 698));
    surfaceMapSetupPanel.add(jPanel8,      new XYConstraints(9, 726, 498, 154));
    jPanel8.add(adjustPointsButton,    new XYConstraints(14, 12, 133, -1));
    jPanel8.add(jButton13,   new XYConstraints(14, 51, 133, -1));
    jPanel8.add(jButton15,  new XYConstraints(155, 12, 133, -1));
    jPanel8.add(jButton16,  new XYConstraints(155, 51, 133, -1));
    jPanel8.add(jButton17,   new XYConstraints(156, 90, 133, -1));
    jPanel8.add(jButton14,   new XYConstraints(14, 90, 133, -1));
    jPanel8.add(jPanel9,    new XYConstraints(307, 12, 174, 105));
    jPanel9.add(jLabel1,   new XYConstraints(5, 4, -1, -1));
    jPanel9.add(jLabel4,   new XYConstraints(48, 81, -1, -1));
    jPanel9.add(jPanel14, new XYConstraints(14, 28, 18, 18));
    jPanel9.add(jPanel15, new XYConstraints(14, 54, 18, 18));
    jPanel9.add(jPanel16,  new XYConstraints(14, 80, 18, 18));
    jPanel9.add(jLabel2, new XYConstraints(48, 29, -1, -1));
    jPanel9.add(jLabel3,  new XYConstraints(48, 55, -1, -1));
    contentPane.add(statusBar, BorderLayout.SOUTH);
    Random random = new Random();
    Vector rowData = new Vector();
    for (int i = 0; i < 60; i++)
    {
      Vector singleRow = new Vector();
      singleRow.add(new Integer(i+1));
      if (i == 29)
        singleRow.add("X-ray");
      else
        singleRow.add("Laser");
      singleRow.add(new Integer(random.nextInt(10000)));
      singleRow.add(new Integer(random.nextInt(10000)));
      if (i == 29)
        singleRow.add(new Float(0.0));
      else
        singleRow.add(new Float(random.nextFloat() + 3.0));
      rowData.add(singleRow);
    }
    Vector columnNames = new Vector();
    columnNames.add("#");
    columnNames.add("Type");
    columnNames.add("X");
    columnNames.add("Y");
    columnNames.add("Offset");
    jTable1 = new JSortTable(rowData, columnNames);
    jTable1.setRowSelectionInterval(0,0);
    jScrollPane1.getViewport().add(jTable1, null);
    jTable1.setDefaultRenderer(Object.class, new CustomerTableRenderer());
    jMenu2.add(jMenuItem2);
    jMenu2.add(jMenuItem3);
    jMenu2.add(jMenuItem4);
    jMenu2.add(jMenuItem5);
    jMenu2.add(jMenuItem6);
    jMenu3.add(jMenuItem10);
    jMenu3.add(jMenuItem12);
    jMenu3.add(jMenuItem11);
    jMenu3.add(jMenuItem13);
    jMenu3.add(jMenuItem7);
    jMenu1.add(jMenuItem9);
    jMenu1.add(jMenuItem8);
    jSplitPane1.setDividerLocation(675);
    jSplitPane2.setDividerLocation(537);

    componentZoomComboBox.addItem("C1");
    componentZoomComboBox.addItem("C2");
    componentZoomComboBox.addItem("C3");
    componentZoomComboBox.addItem("C4");
    componentZoomComboBox.addItem("R1");
    componentZoomComboBox.addItem("R2");
    componentZoomComboBox.addItem("R3");
    componentZoomComboBox.addItem("U1");
    componentZoomComboBox.addItem("U2");
    componentZoomComboBox.addItem("U3");
    componentZoomComboBox.addItem("U4");
    componentZoomComboBox.addItem("U5");

    surfaceMapAdjustPointsPanel.add(adjustPointsDoneButton,     new XYConstraints(376, 16, 98, 30));
    surfaceMapAdjustPointsPanel.add(adjustPointsCancelButton,   new XYConstraints(376, 60, 98, 30));
    jPanel10.add(jLabel8,  new XYConstraints(2, 51, 73, 19));
    jPanel10.add(jLabel9,   new XYConstraints(2, 73, 54, 22));
    jPanel10.add(jLabel6,    new XYConstraints(2, 3, -1, -1));
    jPanel10.add(jLabel7,  new XYConstraints(2, 23, 63, 23));
    surfaceMapAdjustPointsPanel.add(adjustPointsPrevPointButton,    new XYConstraints(185, 68, 113, -1));
    surfaceMapAdjustPointsPanel.add(adjustPointsGoToTextField,   new XYConstraints(305, 106, 72, 22));
    surfaceMapAdjustPointsPanel.add(adjustPointsGotoPointsButton,  new XYConstraints(185, 105, 113, -1));
    surfaceMapAdjustPointsPanel.add(adjustPointsNextPointButton,   new XYConstraints(185, 31, 113, -1));
    surfaceMapAdjustPointsPanel.add(jLabel5, new XYConstraints(5, 2, 134, 28));
    surfaceMapAdjustPointsPanel.add(jPanel10,      new XYConstraints(5, 31, 170, 101));
    mainDebuggerPanel.add(homeWorkSpacePanel, BorderLayout.CENTER);
    homeWorkSpacePanel.add(jPanel11, null);
    jPanel11.add(jLabel13, new XYConstraints(5, 123, -1, 28));
    jPanel11.add(jLabel12, new XYConstraints(5, 81, -1, 41));
    jPanel11.add(jLabel14, new XYConstraints(5, 161, -1, 30));
    jPanel11.add(jLabel15, new XYConstraints(18, 211, -1, -1));
    jPanel11.add(jLabel19, new XYConstraints(18, 233, -1, -1));
    jPanel11.add(jLabel16, new XYConstraints(18, 255, -1, -1));
    jPanel11.add(jLabel18, new XYConstraints(18, 277, -1, -1));
    jPanel11.add(jLabel17, new XYConstraints(18, 189, -1, -1));
    jPanel11.add(jLabel10, new XYConstraints(5, 3, -1, -1));
    jPanel11.add(jLabel11, new XYConstraints(5, 29, -1, -1));
    viewVerifierPanel.add(viewVerifierEditorPane, BorderLayout.CENTER);

  }
  //File | Exit action performed
  public void jMenuFileExit_actionPerformed(ActionEvent e)
  {
    System.exit(0);
  }
  //Help | About action performed
  public void jMenuHelpAbout_actionPerformed(ActionEvent e)
  {
  }
  //Overridden so we can exit when window is closed
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      jMenuFileExit_actionPerformed(null);
    }
  }

  void adjustPointsButton_actionPerformed(ActionEvent e)
  {

    surfaceMapSetupPanel.remove(jPanel8);
    surfaceMapSetupPanel.add(surfaceMapAdjustPointsPanel,   new XYConstraints(9, 726, 498, 154));
    surfaceMapSetupPanel.validate();

    int currentRow = jTable1.getSelectedRow();
    int currentPoint = currentRow + 1;
    jLabel7.setText("Point #" + currentPoint);
    Integer Xpos = (Integer)jTable1.getValueAt(currentRow, 2);
    Integer Ypos = (Integer)jTable1.getValueAt(currentRow, 3);
    jLabel8.setText("X: " + Xpos);
    jLabel9.setText("Y: " + Ypos);
    repaint();

  }

  void adjustPointsDoneButton_actionPerformed(ActionEvent e)
  {
    surfaceMapSetupPanel.remove(surfaceMapAdjustPointsPanel);
    surfaceMapSetupPanel.add(jPanel8,   new XYConstraints(9, 726, 498, 154));
    surfaceMapSetupPanel.validate();
    repaint();
  }

  void adjustPointsCancelButton_actionPerformed(ActionEvent e)
  {
    surfaceMapSetupPanel.remove(surfaceMapAdjustPointsPanel);
    surfaceMapSetupPanel.add(jPanel8,   new XYConstraints(9, 726, 498, 154));
    surfaceMapSetupPanel.validate();
    repaint();
  }

  void adjustPointsNextPointButton_actionPerformed(ActionEvent e)
  {
    int currentRow = jTable1.getSelectedRow();
    int currentColumn = jTable1.getSelectedColumn();
    currentRow++;
    if (currentRow == jTable1.getRowCount())
      currentRow = 0;
    jTable1.setRowSelectionInterval(currentRow, currentRow);

    Rectangle rect = jTable1.getCellRect(currentRow, currentColumn, false);
    jTable1.scrollRectToVisible(rect);

    currentRow++;
    Integer Xpos = (Integer)jTable1.getValueAt(currentRow-1, 2);
    Integer Ypos = (Integer)jTable1.getValueAt(currentRow-1, 3);
    jLabel7.setText("Point #" + currentRow);
    jLabel8.setText("X: " + Xpos);
    jLabel9.setText("Y: " + Ypos);
  }

  void adjustPointsPrevPointButton_actionPerformed(ActionEvent e)
  {
    int currentRow = jTable1.getSelectedRow();
    int currentColumn = jTable1.getSelectedColumn();
    if (currentRow == 0)
      currentRow = jTable1.getRowCount() - 1;
    else
      currentRow--;

    jTable1.setRowSelectionInterval(currentRow, currentRow);
    Rectangle rect = jTable1.getCellRect(currentRow, currentColumn, false);
    jTable1.scrollRectToVisible(rect);

    currentRow++;
    Integer Xpos = (Integer)jTable1.getValueAt(currentRow-1, 2);
    Integer Ypos = (Integer)jTable1.getValueAt(currentRow-1, 3);
    jLabel7.setText("Point #" + currentRow);
    jLabel8.setText("X: " + Xpos);
    jLabel9.setText("Y: " + Ypos);
  }

  void adjustPointsGotoPointsButton_actionPerformed(ActionEvent e)
{
    String goToStr = adjustPointsGoToTextField.getText();
    int goToInt;
    try
    {
      goToInt = Integer.parseInt(goToStr);
    }
    catch (NumberFormatException nfex)
    {
      adjustPointsGoToTextField.setText(null);
      return;
    }


    goToInt--;
    int currentColumn = jTable1.getSelectedColumn();
    jTable1.setRowSelectionInterval(goToInt, goToInt);

    Rectangle rect = jTable1.getCellRect(goToInt, currentColumn, false);
    jTable1.scrollRectToVisible(rect);

    goToInt++;
    Integer Xpos = (Integer)jTable1.getValueAt(goToInt-1, 2);
    Integer Ypos = (Integer)jTable1.getValueAt(goToInt-1, 3);
    jLabel7.setText("Point #" + goToInt);
    jLabel8.setText("X: " + Xpos);
    jLabel9.setText("Y: " + Ypos);
}


  private void adjustPointsGoToTextField_actionPerformed(ActionEvent e)
  {
    String goToStr = adjustPointsGoToTextField.getText();
    int goToInt;
    try
    {
      goToInt = Integer.parseInt(goToStr);
    }
    catch (NumberFormatException nfex)
    {
      adjustPointsGoToTextField.setText(null);
      return;
    }
    goToInt--;
    int currentColumn = jTable1.getSelectedColumn();
    jTable1.setRowSelectionInterval(goToInt, goToInt);

    Rectangle rect = jTable1.getCellRect(goToInt, currentColumn, false);
    jTable1.scrollRectToVisible(rect);

    goToInt++;
    Integer Xpos = (Integer)jTable1.getValueAt(goToInt-1, 2);
    Integer Ypos = (Integer)jTable1.getValueAt(goToInt-1, 3);
    jLabel7.setText("Point #" + goToInt);
    jLabel8.setText("X: " + Xpos);
    jLabel9.setText("Y: " + Ypos);
  }

  private void verifyFocusGotoTextField_actionPerformed(ActionEvent e)
  {
//    String goToStr = verifyFocusGotoTextField.getText();
//    int goToInt;
//    try
//    {
//      goToInt = Integer.parseInt(goToStr);
//    }
//    catch (NumberFormatException nfex)
//    {
//      verifyFocusGotoTextField.setText(null);
//      return;
//    }
//    goToInt--;
//    int currentColumn = jTable1.getSelectedColumn();
//    jTable1.setRowSelectionInterval(goToInt, goToInt);
//
//    Rectangle rect = jTable1.getCellRect(goToInt, currentColumn, false);
//    jTable1.scrollRectToVisible(rect);
  }

  void verifyFocusPrevPointButton_actionPerformed(ActionEvent e)
  {
    int currentRow = jTable1.getSelectedRow();
    int currentColumn = jTable1.getSelectedColumn();
    if (currentRow == 0)
      currentRow = jTable1.getRowCount() - 1;
    else
      currentRow--;

    jTable1.setRowSelectionInterval(currentRow, currentRow);
    Rectangle rect = jTable1.getCellRect(currentRow, currentColumn, false);
    jTable1.scrollRectToVisible(rect);
  }

  void verifyFocusGotoPointButton_actionPerformed(ActionEvent e)
  {
//    String goToStr = verifyFocusGotoTextField.getText();
//    int goToInt;
//    try
//    {
//      goToInt = Integer.parseInt(goToStr);
//    }
//    catch (NumberFormatException nfex)
//    {
//      verifyFocusGotoTextField.setText(null);
//      return;
//    }
//    goToInt--;
//    int currentColumn = jTable1.getSelectedColumn();
//    jTable1.setRowSelectionInterval(goToInt, goToInt);
//
//    Rectangle rect = jTable1.getCellRect(goToInt, currentColumn, false);
//    jTable1.scrollRectToVisible(rect);
//
  }
  void verifyFocusCancelButton_actionPerformed(ActionEvent e)
  {
//    jSplitPane2.remove(jPanel13);
//    jSplitPane2.add(jPanel5);
//    jSplitPane2.setDividerLocation(500);
//    repaint();
  }
  void verifyFocusNextPointButton_actionPerformed(ActionEvent e)
  {
    int currentRow = jTable1.getSelectedRow();
    int currentColumn = jTable1.getSelectedColumn();
    currentRow++;
    if (currentRow == jTable1.getRowCount())
      currentRow = 0;
    jTable1.setRowSelectionInterval(currentRow, currentRow);

    Rectangle rect = jTable1.getCellRect(currentRow, currentColumn, false);
    jTable1.scrollRectToVisible(rect);
  }
  void verifyFocusDoneButton_actionPerformed(ActionEvent e)
  {
//    jSplitPane2.remove(jPanel13);
//    jSplitPane2.add(jPanel5);
//    jSplitPane2.setDividerLocation(500);
//    repaint();
  }

  void surfaceMapSetupButton_actionPerformed(ActionEvent e)
  {
    if (_currentScreen != SURFACEMAP_SCREEN)
    {
      try
      {
        jEditorPane1.setPage(laserImageURL);
      }
      catch (IOException ex)
      {
      }
//    mainDebuggerPanel.remove(homeWorkSpacePanel);
      mainDebuggerPanel.remove(1);
      mainDebuggerPanel.add(surfaceMapPanel, BorderLayout.CENTER);
      validate();
      jSplitPane1.setDividerLocation(675);
      repaint();

      _currentScreen = SURFACEMAP_SCREEN;
    }
  }



  void returnToMainScreen()
  {
    mainDebuggerPanel.remove(1);
    mainDebuggerPanel.add(homeWorkSpacePanel, BorderLayout.CENTER);
    validate();
    jSplitPane1.setDividerLocation(675);
    repaint();
  }

  void algorithmTunerButton_actionPerformed(ActionEvent e)
  {
    if (_currentScreen != TUNER_SCREEN)
    {
      try
      {
        jEditorPane1.setPage(xrayImageURL);
      }
      catch (IOException ex)
      {
      }
//    mainDebuggerPanel.remove(homeWorkSpacePanel);
      mainDebuggerPanel.remove(1);
      mainDebuggerPanel.add(tunerPanel, BorderLayout.CENTER);
      validate();
      jSplitPane1.setDividerLocation(675);
      repaint();

      _currentScreen = TUNER_SCREEN;
    }
  }

  void homeButton_actionPerformed(ActionEvent e)
  {
    if (_currentScreen != HOME_SCREEN)
    {
      returnToMainScreen();
      _currentScreen = HOME_SCREEN;
    }
  }

  void viewVerifierButton_actionPerformed(ActionEvent e)
  {
    if (_currentScreen != VIEWVERIFER_SCREEN)
    {
      try
      {
        jEditorPane1.setPage(xrayImageURL);
      }
      catch (IOException ex)
      {
      }
//    mainDebuggerPanel.remove(homeWorkSpacePanel);
      mainDebuggerPanel.remove(1);
      mainDebuggerPanel.add(viewVerifierPanel, BorderLayout.CENTER);
      validate();
      jSplitPane1.setDividerLocation(675);
      repaint();

      _currentScreen = VIEWVERIFER_SCREEN;
    }
  }
}
