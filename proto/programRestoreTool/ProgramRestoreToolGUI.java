package com.agilent.xRayTest.gui.programRestoreTool;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.filechooser.FileFilter;

import com.agilent.xRayTest.business.*;
import com.agilent.xRayTest.datastore.*;
import com.agilent.xRayTest.gui.mainMenu.*;
import com.agilent.xRayTest.gui.*;
import com.agilent.xRayTest.util.*;
import com.agilent.guiUtil.*;
import com.agilent.util.*;


/**
 * Restore the panel program files from the old .ndf and .rtf zip files or
 * the new .zip file.
 * @author Vincent Wong
 */
public class ProgramRestoreToolGUI extends JDialog
{
  static String _appName = StringLocalizer.keyToString("PRT_GUI_NAME_KEY");
  private static final String _assertLogFile = "ProgramRestoreToolGUI";
  private static final String _zipFileFilterDescription = StringLocalizer.keyToString("PRT_ZIP_FILE_DESCRIPTION_KEY");

  // flag indicating that we are ready to convert the database
  private volatile boolean _backupFileValidated = false;
  private volatile boolean _doneConverting = false;

  // gui components
  private JPanel _mainPanel = new JPanel();
  private GridLayout _mainPanelLayout = new GridLayout();
  private JPanel _selectionPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JButton _restoreButton = new JButton();
  private JButton _exitButton = new JButton();
  private FlowLayout _buttonPanelLayout = new FlowLayout();
  private JPanel _backupFilePanel = new JPanel();
  private GridLayout _selectionPanelLayout = new GridLayout();
  private JLabel _backupFileLabel = new JLabel();
  private JTextField _backupFileTextField = new JTextField();
  private JButton _backupFileBrowseButton = new JButton();
  private FlowLayout _backupFilePanelLayout = new FlowLayout();
  private JFileChooser _fileChooser = null;
  private MainMenuFrame _owner = null;
  private String _currentPanelName = null;

  /**
   * @author Vincent Wong
   */
  static
  {
    try
    {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch(Exception e)
    {
      Assert.logException(e);
    }
  }

  /**
   * @author Vincent Wong
   */
  public ProgramRestoreToolGUI( Frame owner, String currentPanelName, boolean modal )
  {
    AssertUtil.setUpAssert(_assertLogFile, null);

    _owner = (MainMenuFrame)owner;
    _currentPanelName = currentPanelName;

    try
    {
      jbInit();
    }
    catch(RuntimeException e)
    {
      Assert.logException(e);
    }

    java.awt.Image image = com.agilent.xRayTest.images.Image5DX.getImage(
                           com.agilent.xRayTest.images.Image5DX.FRAME_5DX );

    this.setTitle(_appName);
    this.pack();

    // Center the window
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = getSize();

    if (frameSize.height > screenSize.height)
    {
      frameSize.height = screenSize.height;
    }

    if (frameSize.width > screenSize.width)
    {
      frameSize.width = screenSize.width;
    }

    this.setLocation( (screenSize.width - frameSize.width)/2,
                      (screenSize.height - frameSize.height)/2 );

    this.toFront();
    this.setVisible( true );
  }

  private void jbInit()
  {
    _mainPanel.setLayout(_mainPanelLayout);
    _mainPanelLayout.setRows(2);
    _mainPanelLayout.setColumns(1);
    _mainPanelLayout.setHgap(5);
    _mainPanelLayout.setVgap(5);
    _selectionPanel.setLayout(_selectionPanelLayout);
    _restoreButton.setNextFocusableComponent(_exitButton);
    _restoreButton.setActionCommand("restoreButton");
    _restoreButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _restoreButton.setText(StringLocalizer.keyToString("GUI_RESTORE_BUTTON_KEY"));
    _restoreButton.setMnemonic((StringLocalizer.keyToString("GUI_RESTORE_BUTTON_MNEMONIC_KEY")).charAt(0));
    _restoreButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        restoreButton_actionPerformed(e);
      }
    });
    _exitButton.setNextFocusableComponent(_backupFileTextField);
    _exitButton.setMaximumSize(new Dimension(79, 27));
    _exitButton.setMinimumSize(new Dimension(79, 27));
    _exitButton.setPreferredSize(new Dimension(79, 27));
    _exitButton.setActionCommand("exitButton");
    _exitButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _exitButton.setText(StringLocalizer.keyToString("GUI_EXIT_BUTTON_KEY"));
    _exitButton.setMnemonic((StringLocalizer.keyToString("GUI_EXIT_BUTTON_MNEMONIC_KEY")).charAt(0));
    _exitButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        exitButton_actionPerformed(e);
      }
    });
    _buttonPanel.setLayout(_buttonPanelLayout);
    _backupFileLabel.setMaximumSize(new Dimension(80, 17));
    _backupFileLabel.setMinimumSize(new Dimension(80, 17));
    _backupFileLabel.setNextFocusableComponent(_backupFileTextField);
    _backupFileLabel.setPreferredSize(new Dimension(80, 17));
    _backupFileLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _backupFileLabel.setHorizontalTextPosition(SwingConstants.LEFT);
    _backupFileLabel.setLabelFor(_backupFileTextField);
    _backupFileLabel.setText(StringLocalizer.keyToString("PRTGUI_RESTORE_FILE_LABEL_KEY"));
    _backupFileTextField.setNextFocusableComponent(_backupFileBrowseButton);
    _backupFileTextField.setText("");
    _backupFileTextField.setColumns(20);
    _backupFileTextField.setHorizontalAlignment(SwingConstants.LEFT);
    _backupFileTextField.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyPressed(KeyEvent e)
      {
        backupFileTextField_keyPressed(e);
      }
    });
    _backupFileBrowseButton.setMaximumSize(new Dimension(85, 21));
    _backupFileBrowseButton.setMinimumSize(new Dimension(85, 21));
    _backupFileBrowseButton.setNextFocusableComponent(_restoreButton);
    _backupFileBrowseButton.setPreferredSize(new Dimension(85, 21));
    _backupFileBrowseButton.setActionCommand("backupFileBrowseButton");
    _backupFileBrowseButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _backupFileBrowseButton.setText(StringLocalizer.keyToString("GUI_BROWSE_BUTTON_KEY"));
    _backupFileBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        backupFileBrowseButton_actionPerformed(e);
      }
    });
    _backupFilePanel.setLayout(_backupFilePanelLayout);
    this.setResizable(false);
    this.getContentPane().add(_mainPanel, BorderLayout.CENTER);
    _mainPanel.add(_selectionPanel, null);
    _selectionPanel.add(_backupFilePanel, null);
    _backupFilePanel.add(_backupFileLabel, null);
    _backupFilePanel.add(_backupFileTextField, null);
    _backupFilePanel.add(_backupFileBrowseButton, null);
    _mainPanel.add(_buttonPanel, null);
    _buttonPanel.add(_restoreButton, null);
    _buttonPanel.add(_exitButton, null);
    this.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyPressed(KeyEvent e)
      {
        escapeKeyPressed(e);
      }
    });
  }

  /**
   * @author Vincent Wong
   */
  void backupFileBrowseButton_actionPerformed(ActionEvent e)
  {
    String fileNameFullPath = selectFile();
    boolean fileValidated = validateTextField(fileNameFullPath);

    if ( fileValidated == false )
    {
      _backupFileValidated = false;
    }
    else
    {
      _backupFileValidated = true;
      _backupFileTextField.setText(fileNameFullPath);
      _fileChooser.setCurrentDirectory(new File(fileNameFullPath.substring(0,fileNameFullPath.lastIndexOf(File.separator))));
    }
  }

  /**
   * @author Vincent Wong
   */
  void restoreButton_actionPerformed(ActionEvent e)
  {
    JOptionPane.showMessageDialog( this,
      "This function has not been implemented yet.",
      _appName, JOptionPane.INFORMATION_MESSAGE );
  }

  /**
   * When the exit button is pressed the Program Restore Tool will exit.
   * @author Vincent Wong
   */
  void exitButton_actionPerformed(ActionEvent e)
  {
    this.setVisible(false);
    _owner.deactivateJavaGUI();
    dispose();
  }

  /**
   * This method displays a file chooser and return the file
   * selected by the user.
   * @author Vincent Wong
   */
  private String selectFile()
  {
    if ( _fileChooser == null )
    {
       _fileChooser = new JFileChooser();

       // initialize the JFIle Dialog.
       FileFilter fileFilter = _fileChooser.getAcceptAllFileFilter();
       _fileChooser.removeChoosableFileFilter(fileFilter);

       // create and instance of Zip File Filter
       FileFilterUtil zipFileFilter = new FileFilterUtil(FileName.getZipFileExtension(), _zipFileFilterDescription);

       _fileChooser.setFileFilter(zipFileFilter);

       // set the default directory to the system temp directory.
      _fileChooser.setCurrentDirectory( new File(Directory.getTempDir()) );
    }

    int result = _fileChooser.showDialog(this, StringLocalizer.keyToString("GUI_SELECT_KEY"));
    File selectedFile = _fileChooser.getSelectedFile();
    String file = "";

    if ( selectedFile == null )
      return file;
    else
    {
      file = selectedFile.toString();

      if ( file.toLowerCase().endsWith(FileName.getZipFileExtension()) == false )
      {
        int extensionIndex = file.lastIndexOf(".");

        //make sure we don't get an index out of bounds exception
        if ( extensionIndex != -1 )
          file = file.substring(0,file.lastIndexOf("."));

        file = file + FileName.getZipFileExtension();
      }

      return file;
    }
  }

  /**
   * This method will be called to validate text field Values
   * @author Vincent Wong
   */
  private boolean validateTextField(String textFieldValue)
  {
    File file = new File(textFieldValue);

    if ( textFieldValue.equalsIgnoreCase("") )
      return false;

    if ( file.exists() && textFieldValue.endsWith(FileName.getZipFileExtension()) )
      return true;
    else if ( textFieldValue.endsWith(FileName.getZipFileExtension()) )
    {
      JOptionPane.showMessageDialog( this,
        StringLocalizer.keyToString("PRT_FILE_DOES_NOT_EXIST_KEY"),
        _appName, JOptionPane.ERROR_MESSAGE );

      return false;
    }
    else
      return false;
  }

  /**
   * @author Vincent Wong
   */
  void backupFileTextField_keyPressed(KeyEvent e)
  {
    if (e.getKeyCode() == KeyEvent.VK_ENTER)
    {
      if (validateTextField(_backupFileTextField.getText()))
        _backupFileValidated = true;
      else
        _backupFileValidated = false;
    }
  }

  /**
   * Exit this if the escape key is pressed.
   * @author Vincent Wong
   */
  void escapeKeyPressed(KeyEvent e)
  {
    if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
    {
      this.setVisible(false);
      _owner.deactivateJavaGUI();
      dispose();
    }
  }

  /**
   * @author Vincent Wong
   */
  protected void processWindowEvent(WindowEvent e)
  {
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      this.setVisible(false);
      _owner.deactivateJavaGUI();
      dispose();
    }
  }
}
