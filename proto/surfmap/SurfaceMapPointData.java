package proto.surfmap;

/**
 * This class holds the surface map point data that is displayed in the table
 * of the Surface Map Setup GUI.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

class SurfaceMapPointData
{
  // This controls the order of the entries in the table.
  static final int SURFACE_MAP_NUMBER = 0;
  static final int SURFACE_MAP_USED   = 1;
  static final int SURFACE_MAP_HEIGHT = 2;
  static final int SURFACE_MAP_DELTAZ = 3;
  static final int SURFACE_MAP_TYPE   = 4;
  static final int NUMBER_OF_ENTRIES  = 5;  // tells the model how many columns to make

  private int _number;
  private double _height;
  private double _deltaZ;
  private boolean _used;
  private String _type;

  /**
   * Constructor with the surface map data.
   *
   * @author  Steve Anonson
   */
  public SurfaceMapPointData(int number, double height, double deltaZ, boolean used, String type)
  {
    _number = number;
    _height = height;
    _deltaZ = deltaZ;
    _used = used;
    _type = type;
  }

  /**
   * Return the surface map point id number.
   *
   * @author  Steve Anonson
   */
  public int getNumber()
  {
    return _number;
  }

  /**
   * Set the surface map point id number.
   *
   * @author  Steve Anonson
   */
  public void setNumber(int number)
  {
    _number = number;
  }

  /**
   * Return the surface map point z height.
   *
   * @author  Steve Anonson
   */
  public double getHeight()
  {
    return _height;
  }

  /**
   * Set the surface map point z height.
   *
   * @author  Steve Anonson
   */
  public void setHeight(double height)
  {
    _height = height;
  }

  /**
   * Return the surface map point delta z value.
   *
   * @author  Steve Anonson
   */
  public double getDeltaZ()
  {
    return _deltaZ;
  }

  /**
   * Set the surface map point delta z value.
   *
   * @author  Steve Anonson
   */
  public void setDeltaZ(double deltaZ)
  {
    _deltaZ = deltaZ;
  }

  /**
   * Return the surface map point used flag.
   *
   * @author  Steve Anonson
   */
  public boolean getUsed()
  {
    return _used;
  }

  /**
   * Set the surface map point used flag.
   *
   * @author  Steve Anonson
   */
  public void setUsed(boolean used)
  {
    _used = used;
  }

  /**
   * Return the surface map point type.
   *
   * @author  Steve Anonson
   */
  public String getType()
  {
    return _type;
  }

  /**
   * Set the surface map point type.
   *
   * @author  Steve Anonson
   */
  public void setType(String type)
  {
    _type = type;
  }

  /**
   * Return the element from the selected column.  Used by the table.
   *
   * @author  Steve Anonson
   */
  public Object getElement( int index )
  {
    switch (index)
    {
      case SURFACE_MAP_NUMBER: return new Integer(getNumber());
      case SURFACE_MAP_HEIGHT: return new Double(getHeight());
      case SURFACE_MAP_DELTAZ: return new Double(getDeltaZ());
      case SURFACE_MAP_USED: return new Boolean(getUsed());
      case SURFACE_MAP_TYPE: return getType();
    }
    return "";
  }

  /**
   * Set the element to a new value entered from the table.  Currently only the
   * used value can be changed through the table.
   *
   * @author  Steve Anonson
   */
  public void setElement( int index, Object value )
  {
    switch (index)
    {
//      case SURFACE_MAP_NUMBER: setNumber();
//      case SURFACE_MAP_HEIGHT: setHeight();
//      case SURFACE_MAP_DELTAZ: setDeltaZ();
      case SURFACE_MAP_USED: setUsed(((Boolean)value).booleanValue());
//      case SURFACE_MAP_TYPE: setType();
    }
  }
}