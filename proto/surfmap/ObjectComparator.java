package proto.surfmap;

import java.lang.IllegalArgumentException;
import java.util.Date;

/**
 * This class provides a static compare method for two objects that checks the
 * object class and compares them appropriately.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class ObjectComparator
{
  /**
   * Compare two items from a column according to their class.
   *
   * @author  Steve Anonson
   * @throws     IllegalArgumentException if the classes don't match
   * @return    -1 if obj1 < obj2, 0 if obj1 == obj2 and 1 if obj1 > obj2
   */
  public static int compare(Object obj1, Object obj2)
  {
    // Check for nulls
    if (obj1 == null && obj2 == null)
      return 0;
    else if (obj1 == null)
      return -1;  // define null less than everything
    else if (obj2 == null)
      return 1;

    if (obj1.getClass() != obj2.getClass())
      throw new IllegalArgumentException("Mismatched classes");

    // Compare based upon the class type
    if (obj1 instanceof Number)
    {
      Number n1 = (Number)obj1;
      double d1 = n1.doubleValue();
      Number n2 = (Number)obj2;
      double d2 = n2.doubleValue();
      return ((d1 < d2) ? -1 : ((d1 > d2) ? 1 : 0));
    }
    else if (obj1 instanceof String)
    {
      String s1 = (String)obj1;
      String s2 = (String)obj2;
      int result = s1.compareTo(s2);
      return ((result < 0) ? -1 : ((result > 0) ? 1 : 0));
    }
    else if (obj1 instanceof Boolean)
    {
      Boolean bool1 = (Boolean)obj1;
      boolean b1 = bool1.booleanValue();
      Boolean bool2 = (Boolean)obj2;
      boolean b2 = bool2.booleanValue();
      return ((b1 == b2) ? 0 : ((b1) ? 1 : -1));  // false < true
    }
    else if (obj1 instanceof Date)
    {
      Date d1 = (Date)obj1;
      long n1 = d1.getTime();
      Date d2 = (Date)obj2;
      long n2 = d2.getTime();
      return ((n1 < n2) ? -1 : ((n1 > n2) ? 1 : 0));
    }
    else
    { // if all else fails, compare their string representations
      String s1 = obj1.toString();
      String s2 = obj2.toString();
      int result = s1.compareTo(s2);
      return ((result < 0) ? -1 : ((result > 0) ? 1 : 0));
    }
  }

  /**
   * Constructor is private to prevent creating an instance of the class.
   */
  private ObjectComparator()
  {
  }
}