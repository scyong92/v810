package proto.surfmap;

import com.agilent.util.WorkerThread;

/**
 * This is an abstract base class used to create a class for running time
 * intensive tasks.  This class will handle putting the task on a WorkerThread.
 * The run() method should be overridden in the derived class to do the actual
 * task work.  The other methods provide hooks to use for attaching a progress
 * monitor to the task.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

abstract class LongTask implements Runnable
{
  protected String _taskMessage;  // String describing the task, used in progress monitor
  protected String _statMessage;  // Task progress string, i.e. 2 of 15 done
  private WorkerThread _worker = null;

  /**
   * Constructor
   *
   * @author  Steve Anonson
   */
  public LongTask()
  {
  }

  /**
   * This run method must be overridden by the derived class and here is where
   * you put the actual task work.
   *
   * @author  Steve Anonson
   */
  public void run()
  {
    // Must be overridden in for the derived class to actually do some work.
    throw new UnsupportedOperationException("The run method must be overridden.");
  }

  /**
   * Called to start the task.
   *
   * @author  Steve Anonson
   */
  public void go()
  {
    _worker = new WorkerThread("Long task " + this.getClass().getName());
    _worker.invokeLater(this);
  }

  /**
   * Called to find out how much work needs to be done.
   *
   * @author  Steve Anonson
   */
  public abstract int getLengthOfTask();

  /**
   * Called to find out how much has been done.
   *
   * @author  Steve Anonson
   */
  public abstract int getCurrent();

  /**
   * Called to stop the task.  This is where you would put code to cancel
   * a task.
   *
   * @author  Steve Anonson
   */
  public abstract void stop();

  /**
   * Called to find out if the task has completed.
   *
   * @author  Steve Anonson
   */
  public boolean isDone()
  {
    return (getCurrent() >= getLengthOfTask());
  }

  /**
   * Called to get a status message.
   *
   * @author  Steve Anonson
   */
  public String getStatusMessage()
  {
    return _statMessage;
  }

  /**
   * Called to get a task message.
   *
   * @author  Steve Anonson
   */
  public String getTaskMessage()
  {
    return _taskMessage;
  }
}