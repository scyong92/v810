package proto.surfmap;

/**
 * This class holds the format data for a table column.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

class TableColumnFormat
{
  private String _title;
  private int _width;
  private int _alignment;

  /**
   * Construtor
   *
   * @author Steve Anonson
   * @param   title       String to display in the column header
   * @param   width       column width in pixels
   * @param   alignment   horizontal alignment used when rendering a column cell
   */
  public TableColumnFormat( String title, int width, int alignment )
  {
    _title = title;
    _width = width;
    _alignment = alignment;
  }

  /**
   * Return the column title.
   *
   * @author Steve Anonson
   * @return	the column header string
   */
  public String getTitle()
  {
    return _title;
  }

  /**
   * Return the column width in pixels.
   *
   * @author Steve Anonson
   * @return	the column width
   */
  public int getWidth()
  {
    return _width;
  }

  /**
   * Return the horizontal alignment to used in a cell.
   *
   * @author Steve Anonson
   * @return	the column alignment
   */
  public int getAlignment()
  {
    return _alignment;
  }
}