package proto.surfmap;

import java.util.Comparator;

/**
 * This class implements a Comparator used when sorting column data in the
 * surface map table.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class SurfMapTableComparator implements Comparator
{
  protected int _sortColumn;
  protected boolean _accendingSort;

  /**
   * Constructor.
   *
   * @author  Steve Anonson
   */
  public SurfMapTableComparator(int sortColumn, boolean accendingSort)
  {
    _sortColumn = sortColumn;
    _accendingSort = accendingSort;
  }

  /**
   * Compare two items from a column according to their class.
   *
   * @author  Steve Anonson
   * @throws     IllegalArgumentException if not SurfaceMapPointData instances
   * @return    -1 if obj1 < obj2, 0 if obj1 == obj2 and 1 if obj1 > obj2
   */
  public int compare(Object obj1, Object obj2)
  {
    // Check for nulls
    if (obj1 == null && obj2 == null)
      return 0;
    else if (obj1 == null)
      return -1;  // define null less than everything
    else if (obj2 == null)
      return 1;

    if (!(obj1 instanceof SurfaceMapPointData) || !(obj2 instanceof SurfaceMapPointData))
      throw new IllegalArgumentException("Arguements must be an instance of SurfaceMapPointData.");

    SurfaceMapPointData rowA = (SurfaceMapPointData)obj1;
    SurfaceMapPointData rowB = (SurfaceMapPointData)obj2;
    Object o1 = rowA.getElement(this._sortColumn);
    Object o2 = rowB.getElement(this._sortColumn);

    int result = ObjectComparator.compare(o1, o2);  // compares items depending upon their class

    if (!this._accendingSort)
      result = -result;
    return result;
  }

  /**
   * Return true if two comparators have the same column and sort order.
   *
   * @author  Steve Anonson
   * @return    true if comparators are equal
   */
  public boolean equals(Object obj)
  {
    if (obj instanceof SurfMapTableComparator)
    {
      SurfMapTableComparator compObj = (SurfMapTableComparator)obj;
      return (compObj._sortColumn == this._sortColumn && compObj._accendingSort == this._accendingSort);
    }
    return false;
  }
}