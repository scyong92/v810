package proto.surfmap;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

/**
 * This dialog displays information about the Surface Map Setup GUI in response
 * to selecting "About" in the Help menu.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class SurfMapProtoAboutBox extends JDialog implements ActionListener
{
  private String product = "<html>Surface Map Editor (prototype)";
  private String version = "Version: " + com.agilent.xRayTest.util.Version.getVersion();
  private String copyright = "<html>Agilent Technologies, Inc.<p>All Rights Reserved<p>" + com.agilent.xRayTest.util.Version.getCopyRight();
  private String comments = "";

  /**
   * Constructor.
   *
   * @author  Steve Anonson
   */
  public SurfMapProtoAboutBox(Frame parent)
  {
    super(parent, true);
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    pack();
  }

  /**
   * Component initialization
   *
   * @author  Steve Anonson
   */
  private void jbInit() throws Exception
  {
    ImageIcon _imageIcon = new ImageIcon(com.agilent.xRayTest.images.Image5DX.class.getResource("agilent_stack.gif"));
    _imageLabel.setIcon( _imageIcon );
    this.setTitle("About");
    setResizable(false);
    _panel1.setLayout(_borderLayout1);
    _panel2.setLayout(_borderLayout2);
    _insetsPanel1.setLayout(_flowLayout1);
    _insetsPanel2.setLayout(_flowLayout1);
    _insetsPanel2.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    _productLbl.setText(product);
    _versionLbl.setText(version);
    _copyrightLbl.setText(copyright);
    _commentsLbl.setText(comments);
    _insetsPanel3.setLayout(_gridBagLayout1);
    _insetsPanel3.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    _okBtn.setText("OK");
    _okBtn.addActionListener(this);
    _insetsPanel2.add(_imageLabel, null);
    _panel2.add(_insetsPanel2, BorderLayout.WEST);
    this.getContentPane().add(_panel1, null);
    _insetsPanel3.add(_productLbl, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    _insetsPanel3.add(_versionLbl, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    _insetsPanel3.add(_copyrightLbl, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    _insetsPanel3.add(_commentsLbl, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    _panel2.add(_insetsPanel3, BorderLayout.CENTER);
    _insetsPanel1.add(_okBtn, null);
    _panel1.add(_insetsPanel1, BorderLayout.SOUTH);
    _panel1.add(_panel2, BorderLayout.NORTH);
  }

  /**
   * Overridden so we can exit when window is closed
   *
   * @author  Steve Anonson
   */
  protected void processWindowEvent(WindowEvent e)
  {
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      cancel();
    }
    super.processWindowEvent(e);
  }

  /**
   * Close the dialog
   *
   * @author  Steve Anonson
   */
  void cancel()
  {
    dispose();
  }

  /**
   * "OK" button callback method.
   *
   * @author  Steve Anonson
   */
  public void actionPerformed(ActionEvent e)
  {
    if (e.getSource() == _okBtn)
    {
      cancel();
    }
  }

  private JPanel _panel1 = new JPanel();
  private JPanel _panel2 = new JPanel();
  private JPanel _insetsPanel1 = new JPanel();
  private JPanel _insetsPanel2 = new JPanel();
  private JPanel _insetsPanel3 = new JPanel();
  private JButton _okBtn = new JButton();
  private JLabel _imageLabel = new JLabel();
  private JLabel _productLbl = new JLabel();
  private JLabel _versionLbl = new JLabel();
  private JLabel _copyrightLbl = new JLabel();
  private JLabel _commentsLbl = new JLabel();
  private BorderLayout _borderLayout1 = new BorderLayout();
  private BorderLayout _borderLayout2 = new BorderLayout();
  private FlowLayout _flowLayout1 = new FlowLayout();
  private GridBagLayout _gridBagLayout1 = new GridBagLayout();
}