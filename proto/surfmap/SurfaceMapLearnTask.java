package proto.surfmap;

/**
 * This class encapsulates the learn surface map task.  The run method calls
 * the business layer class to start learning.  The task is put on a WorkerThread
 * by the LongTask class.  This class provides methods used by a progress monitor.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

class SurfaceMapLearnTask extends LongTask
{
  private int _current = 0;
  private boolean _stopped = false;

  /**
   * Constructor.
   *
   * @author Steve Anonson
   */
  public SurfaceMapLearnTask()
  {
    _taskMessage = "Learning Surface Map Features                             ";
    _statMessage = "Completed " + 0 + " out of " + getLengthOfTask() + " camera strips.";
  }

  /**
   * This method does the actual task on the WorkerThread.  Put the calls to
   * the business layer class to start learning here.
   *
   * @author Steve Anonson
   */
  public void run()
  {
    _current = 0;
    while (_current < getLengthOfTask() && !_stopped)
    {
      _current++;
      _statMessage = "Completed " + _current + " out of " + getLengthOfTask() + " camera strips.";
      try { Thread.sleep(500); }
      catch (Exception e) { }
    }
  }

  /**
   * Return the current progress of the task.
   *
   * @author Steve Anonson
   */
  public int getCurrent()
  {
    /**@todo implement this proto.surfmap.LongTask abstract method*/
// DEBUG
    return _current;
// DEBUG
  }

  /**
   * Return the length of the task to measure progress.
   *
   * @author Steve Anonson
   */
  public int getLengthOfTask()
  {
    /**@todo implement this proto.surfmap.LongTask abstract method*/
// DEBUG
    return 25;
// DEBUG
  }

  /**
   * Stop the task.  If the task was not completed when this method is called,
   * the already learned data may have to be backed out here if the business
   * layer class doesn't do it.
   *
   * @author Steve Anonson
   */
  public void stop()
  {
    /**@todo implement this proto.surfmap.LongTask abstract method*/
// DEBUG
    _stopped = true;
// DEBUG
  }
}
