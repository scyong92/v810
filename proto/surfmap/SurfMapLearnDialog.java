package proto.surfmap;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/**
 * This dialog lets the user change the learning parameters and then click the
 * "start learning" button to initiate the task.  The dialog changes to a
 * progress monitor while the task is running.  It monitors the cancel button
 * and the task's isDone() method to end.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class SurfMapLearnDialog extends JDialog implements ActionListener
{
  private static final int _ONE_SECOND = 1000;

  private boolean _done = false;
  private int _counter = 0;
  private LongTask _task = null;
  private Timer _timer = null;

  /**
   * Constructor.
   *
   * @author Steve Anonson
   */
  public SurfMapLearnDialog(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
    // Create the timer for the progress monitor.
    _timer = new Timer(_ONE_SECOND, this);
  }

  /**
   * Create the dialog contents.
   *
   * @author Steve Anonson
   */
  void jbInit() throws Exception
  {
    // Control buttons
    _okBtn.setFont(new java.awt.Font("Dialog", 1, 12));
    _okBtn.setMnemonic('S');
    _okBtn.setText("Start Learning");
    _okBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _okBtn_actionPerformed(e);
      }
    });
    _cancelBtn.setMnemonic('C');
    _cancelBtn.setText("Cancel");
    _cancelBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _cancelBtn_actionPerformed(e);
      }
    });
    _helpBtn.setMnemonic('H');
    _helpBtn.setText("Help");
    _helpBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _helpBtn_actionPerformed(e);
      }
    });
    _buttonGridLayout.setHgap(5);
    _okCancelPanel.setLayout(_buttonGridLayout);
    _okCancelPanel.add(_okBtn, null);
    _okCancelPanel.add(_cancelBtn, null);
    _okCancelPanel.add(_helpBtn, null);
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,5,5,5));
    _buttonPanelLayout.setAlignment(FlowLayout.RIGHT);
    _buttonPanel.setLayout(_buttonPanelLayout);
    _buttonPanel.add(_okCancelPanel, null);

    // Parameter panel
// DEBUG
    _placeholderLbl.setHorizontalAlignment(SwingConstants.CENTER);
    _placeholderLbl.setText("The learning parameters go here.");
// DEBUG
    _centerPanel.setLayout(borderLayout1);
    _centerPanel.add(_parameterPanel, BorderLayout.NORTH);
    _centerPanel.add(_placeholderLbl, BorderLayout.CENTER);
    _mainCardPanel.setLayout(_mainCardPanelLayout);
    _mainCardPanel.add(_centerPanel, BorderLayout.CENTER);
    _mainCardPanel.add(_buttonPanel, BorderLayout.SOUTH);

    // Progress panel
    //Make a temporary progress monitor so that the dialog size is correctly calculated.
    _progressMonitor = new ProgressMonitorPane("Default task message", "Default task note.  ", 0, 100);
    _progressCardPanel.setLayout(_progressCardPanelLayout);
    _progressCardPanel.add(_progressMonitor, BorderLayout.SOUTH);

    _rootPanel.setLayout(_cardLayout);
    _rootPanel.add(_mainCardPanel, "_mainCardPanel");
    _rootPanel.add(_progressCardPanel, "_progressCardPanel");
    getContentPane().add(_rootPanel);
  }

  /**
   * Brings the parameter panel to the front.
   *
   * @author Steve Anonson
   */
  private void showMainCard()
  {
    _cardLayout.show(_rootPanel, "_mainCardPanel");
  }

  /**
   * Brings the progress monitor panel to the front.
   *
   * @author Steve Anonson
   */
  private void showProgressCard()
  {
    _cardLayout.show(_rootPanel, "_progressCardPanel");
  }

  /**
   * Exit method for the dialog.  Just hides the dialog.
   *
   * @author Steve Anonson
   */
  private void exitDialog()
  {
    this.hide();
    if (_startFromMain)
      System.exit(0);
  }

  /**
   * This method is call if the user click the cancel button in the progress
   * monitor.  It reverts the dialog back to the parameter panel.
   *
   * @author Steve Anonson
   */
  private void learningCanceled()
  {
// DEBUG
    System.out.println("Learning was canceled, so clean up");
// DEBUG
    showMainCard();
  }

  /**
   * This method is called when the learning task ends.  It exits the dialog.
   *
   * @author Steve Anonson
   */
  private void learningDone()
  {
// DEBUG
    System.out.println("Learning is done");
// DEBUG
    exitDialog();
  }

  /**
   * ActionListener method used by the progress monitor.  It is called periodically
   * by the Timer so that the task progress can be read from the task and sent
   * to the progress monitor.  It also checks to see if the cancel button was
   * pressed in the monitor.
   *
   * @author Steve Anonson
   */
  public void actionPerformed(ActionEvent e)
  {
    if (_progressMonitor == null)
      return;

    if (_progressMonitor.isCanceled() || _task.isDone())
    {
      // The task is either done or the cancel button was pressed.
      _progressMonitor.close();
      _task.stop();
      _timer.stop();
      if (!_task.isDone())
        this.learningCanceled();  // clean up the partially learned data
      else
        this.learningDone();  // task is completed
    }
    else
    {
      // Continue running and update the progress monitor.
      _progressMonitor.setNote(_task.getStatusMessage());
      _progressMonitor.setProgress(_task.getCurrent());
    }
  }

  /**
   * "Start Learning" button callback method.
   *
   * @author Steve Anonson
   */
  void _okBtn_actionPerformed(ActionEvent e)
  {
    // Create a surface map learning task that runs in another thread.
    _task = new SurfaceMapLearnTask();
    // Create a progress monitor for the learning task.
    _progressCardPanel.remove(_progressMonitor);
    _progressMonitor = new ProgressMonitorPane(_task.getTaskMessage(),
                                                _task.getStatusMessage(),
                                                0,
                                                _task.getLengthOfTask());
    _progressMonitor.setProgress(0);
    _progressCardPanel.add(_progressMonitor);
    showProgressCard();

    _task.go();
    _timer.start();
  }

  /**
   * Cancel button callback method.
   *
   * @author Steve Anonson
   */
  void _cancelBtn_actionPerformed(ActionEvent e)
  {
    exitDialog();
  }

  /**
   * Bring up help for the dialog.
   *
   * @author Steve Anonson
   */
  void _helpBtn_actionPerformed(ActionEvent e)
  {
// DEBUG
    System.out.println("Get Help!");
// DEBUG
  }

// DEBUG
  public static void main(String[] args)
  {
    try
    {
      // Make sure the look and feel is set before any components are created.
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (Exception e) { /* do nothing */ }

    SurfMapLearnDialog dlg = new SurfMapLearnDialog(null, "Learn Surface Map (main)", true);
    dlg._startFromMain = true;
    com.agilent.util.SwingUtils.setScreenPosition(dlg,0.5,0.25);
    dlg.show();
  }
  boolean _startFromMain = false;
// DEBUG
  private JPanel _rootPanel = new JPanel();
  private JPanel _mainCardPanel = new JPanel();
  private JPanel _progressCardPanel = new JPanel();
  private BorderLayout _mainCardPanelLayout = new BorderLayout();
  private BorderLayout _progressCardPanelLayout = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private FlowLayout _buttonPanelLayout = new FlowLayout();
  private JPanel _okCancelPanel = new JPanel();
  private GridLayout _buttonGridLayout = new GridLayout();
  private JButton _okBtn = new JButton();
  private JButton _cancelBtn = new JButton();
  private JButton _helpBtn = new JButton();
  private JPanel _parameterPanel = new JPanel();
  private BorderLayout borderLayout1 = new BorderLayout();
  private CardLayout _cardLayout = new CardLayout();
  private ProgressMonitorPane _progressMonitor = null;
  private JLabel _placeholderLbl = new JLabel();
}