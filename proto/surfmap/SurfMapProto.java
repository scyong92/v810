package proto.surfmap;

import javax.swing.UIManager;
import java.awt.*;
import com.agilent.util.SwingUtils;

/**
 * Surface Map Setup application.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class SurfMapProto
{
  private SurfMapProtoFrame _frame = null;
  private boolean _packFrame = false;

  /**
   * Construct the application
   */
  public SurfMapProto()
  {
    _frame = new SurfMapProtoFrame(this);
    //Validate frames that have preset sizes
    //Pack frames that have useful preferred size info, e.g. from their layout
    if (_packFrame)
    {
      _frame.pack();
    }
    else
    {
      _frame.validate();
    }
    //Center the window
    SwingUtils.centerOnScreen(_frame);
  }

  /**
   * Starts the application.
   *
   * @author  Steve Anonson
   */
  public void startApp()
  {
    if (_frame != null)
    {
      _frame.initialize();  // tell the frame to load new cad
      _frame.setVisible(true);  // show the GUI
    }
  }

  /**
   * Exit method for the application.  Does a System.exit() if the application
   * was started from the class main method.
   *
   * @author  Steve Anonson
   */
  public void exitApp()
  {
// DEBUG
    System.out.println("Exiting Application");
// DEBUG
    if (_startFromMain)
      System.exit(0);

    if (_frame != null)
      _frame.setVisible(false);
  }

  /**Main method*/
  public static void main(String[] args)
  {
    try
    {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    SurfMapProto app = new SurfMapProto();
    app._startFromMain = true;
    app.startApp();
  }
  private boolean _startFromMain = false;
}