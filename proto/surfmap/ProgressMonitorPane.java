package proto.surfmap;

import javax.swing.*;

/**
 * This class implements the ProgressMonitor functionality in a panel that can
 * be added within a frame or dialog.  The operations were borrowed from the Java
 * ProgressMonitor class.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class ProgressMonitorPane extends JOptionPane
{
  private JProgressBar  _progressBar = null;
  private JLabel        _noteLabel = null;
  private String        _note;
  private Object        _message;
  private int           _min;
  private int           _max;

  private int           _currentValue;
  protected int         _lastDisplayed;
  protected int         _reportDelta;
  private Object[]      _cancelOption = new Object[1];

  /**
   * Constructor.
   *
   * @param   message   Object containing the task message.
   * @param   note      String for the task progress status.  Can be null.
   * @param   min       Minimum value for the progress bar.
   * @param   max       Maximum value for the progress bar.
   */
  public ProgressMonitorPane(Object message,
                             String note,
                             int min,
                             int max)
  {
    this(new Object[] { message, ((note != null) ? new JLabel(note) : null), new JProgressBar()});

    this._message = message;
    this._note = note;
    this._min = min;
    this._max = max;
    _cancelOption[0] = UIManager.getString("OptionPane.cancelButtonText");

    // Calculate the reporting increment.
    _reportDelta = (max - min) / 100;
    if (_reportDelta < 1)
      _reportDelta = 1;
    _currentValue = min;
    if (_progressBar != null)
    {
      _progressBar.setMinimum(min);
      _progressBar.setMaximum(max);
      _progressBar.setStringPainted(true);
    }
  }

  /**
   * Constructor that creates the JOptionPane.
   * The components that make up the progress monitor are passed in as an Object[]
   * paramter.  The first entry is the task message.  The second is a JLabel for
   * the task progress note which may be null.  The last is the progress bar.
   *
   * @param   messageList   Object array containing the components for the progress monitor.
   */
  private ProgressMonitorPane(Object messageList)
  {
    super(messageList,
          JOptionPane.INFORMATION_MESSAGE,
          JOptionPane.DEFAULT_OPTION,
          null,
          new Object[] {UIManager.getString("OptionPane.cancelButtonText")},
          null);
    Object obj = ((Object[])messageList)[1];
    if (obj != null)
    {
      if (obj instanceof JLabel)
        _noteLabel = (JLabel)obj;
    }
    obj = ((Object[])messageList)[2];
    if (obj != null)
    {
      if (obj instanceof JProgressBar)
        _progressBar = (JProgressBar)obj;
    }
  }

  /**
   * Return the maximum number of characters on a line.
   */
  public int getMaxCharactersPerLineCount()
  {
    return 60;
  }

  /**
   * Indicate the progress of the operation being monitored.
   * If the specified value is >= the maximum, the progress
   * monitor is closed.
   *
   * @param newValue an int specifying the current value, between the
   *        maximum and minimum specified for this component
   */
  public void setProgress(int newValue)
  {
    _currentValue = newValue;
    if (newValue >= _max)
    {
      close();
    }
    else if (newValue >= _lastDisplayed + _reportDelta)
    {
      _lastDisplayed = newValue;
      if (_progressBar != null)
      {
        _progressBar.setValue(newValue);
      }
    }
  }

  /**
   * Indicate that the operation is complete.  This happens automatically
   * when the value set by setProgress is >= max, but it may be called
   * earlier if the operation ends early.
   */
  public void close()
  {
  }

  /**
   * Returns the minimum value -- the lower end of the progress value.
   *
   * @return an int representing the minimum value
   * @see #setMinimum
   */
  public int getMinimum()
  {
    return _min;
  }

  /**
   * Specifies the minimum value.
   *
   * @param m  an int specifying the minimum value
   * @see #getMinimum
   */
  public void setMinimum(int minimum)
  {
    _min = minimum;
  }

  /**
   * Returns the maximum value -- the higher end of the progress value.
   *
   * @return an int representing the maximum value
   * @see #setMaximum
   */
  public int getMaximum()
  {
    return _max;
  }

  /**
   * Specifies the maximum value.
   *
   * @param m  an int specifying the maximum value
   * @see #getMaximum
   */
  public void setMaximum(int maximum)
  {
    _max = maximum;
  }

  /**
   * Returns true if the user hits the Cancel button in the progress pane.
   */
  public boolean isCanceled()
  {
    Object v = this.getValue();
    return ((v != null) && (_cancelOption.length == 1) && (v.equals(_cancelOption[0])));
  }

  /**
   * Specifies the additional note that is displayed along with the
   * progress message. Used, for example, to show which file the
   * is currently being copied during a multiple-file copy.
   *
   * @param note  a String specifying the note to display
   * @see #getNote
   */
  public void setNote(String note)
  {
    this._note = note;
    if (_noteLabel != null)
    {
      _noteLabel.setText(note);
    }
  }

  /**
   * Specifies the additional note that is displayed along with the
   * progress message.
   *
   * @return a String specifying the note to display
   * @see #setNote
   */
  public String getNote()
  {
    return _note;
  }
}