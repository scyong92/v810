package proto.surfmap;

import java.awt.event.*;
import javax.swing.JTable;
import javax.swing.table.*;

/**
 * Derived MouseAdapter that saves an instance of the JTable that it is attached
 * to for use in the methods.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

class TableColumnListener extends MouseAdapter
{
  protected JTable _table;

  /**
   * Constructor.
   *
   * @author  Steve Anonson
   */
  public TableColumnListener(JTable table)
  {
    _table = table;
  }
}