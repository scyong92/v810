package proto.surfmap;

import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.table.*;

/**
 * Table model for the table in the surface map setup GUI.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 * @see SurfaceMapPointData
 */

class SurfMapTableModel extends AbstractTableModel
{
  private Vector _rowData = new Vector();
  private TableColumnFormat _columnHeadings[] = null;

  private int _sortColumn = 0;  // last column that was sorted on
  private boolean _accendingSort = true;  // direction of the sort

  /**
   * Constructor
   * Sets up the column headings.
   *
   * @author  Steve Anonson
   */
  public SurfMapTableModel()
  {
    _columnHeadings = new TableColumnFormat[ SurfaceMapPointData.NUMBER_OF_ENTRIES ];
    _columnHeadings[ SurfaceMapPointData.SURFACE_MAP_NUMBER ] = new TableColumnFormat("#", 100, JLabel.CENTER);
    _columnHeadings[ SurfaceMapPointData.SURFACE_MAP_HEIGHT ] = new TableColumnFormat("Height", 100, JLabel.CENTER);
    _columnHeadings[ SurfaceMapPointData.SURFACE_MAP_DELTAZ ] = new TableColumnFormat("Delta z", 100, JLabel.CENTER);
    _columnHeadings[ SurfaceMapPointData.SURFACE_MAP_USED   ] = new TableColumnFormat("Used", 100, JLabel.CENTER);
    _columnHeadings[ SurfaceMapPointData.SURFACE_MAP_TYPE   ] = new TableColumnFormat("Type", 100, JLabel.CENTER);
    setDefaultData();
  }

  /**
   * Returns the number of columns in the model. A <code>JTable</code> uses
   * this method to determine how many columns it should create and display by
   * default.
   *
   * @author  Steve Anonson
   * @return the number of columns in the model
   */
  public int getColumnCount()
  {
    return _columnHeadings.length;
  }

  /**
   * Returns the value for the cell at <code>columnIndex</code> and
   * <code>rowIndex</code>.
   *
   *
   * @author  Steve Anonson
   * @param	rowIndex	the row whose value is to be queried
   * @param	columnIndex 	the column whose value is to be queried
   * @return	the value Object at the specified cell
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    if (rowIndex < 0 || rowIndex > getRowCount())
      return "";
    SurfaceMapPointData row = (SurfaceMapPointData)_rowData.elementAt(rowIndex);
    return row.getElement(columnIndex);
  }

  /**
   * Returns the number of rows in the model. A <code>JTable</code> uses this
   * method to determine how many rows it should display.  This method should
   * be quick, as it is called frequently during rendering.
   *
   * @author  Steve Anonson
   * @return the number of rows in the model
   */
  public int getRowCount()
  {
    return ((_rowData == null) ? 0 : _rowData.size());
  }

  /**
   * Returns <code>class</code> of column at <code>columnIndex</code>.
   * The class returned determines how a cell is rendered and edited.
   *
   * @author  Steve Anonson
   * @param columnIndex  the column being queried
   * @return the class of the column
   */
  public Class getColumnClass( int columnIndex )
  {
    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * Returns a name for the column.  If <code>column</code> cannot be found,
   * returns an empty string.
   *
   * @author  Steve Anonson
   * @param column  the column being queried
   * @return a string containing the name of <code>column</code>
   */
  public String getColumnName( int columnIndex )
  {
    if (columnIndex < 0 || columnIndex > getColumnCount())
      return "";
    return _columnHeadings[columnIndex].getTitle();
  }

  /**
   * Returns true for all editable columns, false otherwise. Only the "used" column is editable.
   *
   * @author  Steve Anonson
   * @param  rowIndex  the row being queried
   * @param  columnIndex the column being queried
   * @return true if column 3
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return (columnIndex == SurfaceMapPointData.SURFACE_MAP_USED);  // only "Used" is editable
  }

  /**
   * Set the value for a cell.  Only the "used" column is editable.
   *
   * @author  Steve Anonson
   * @param  aValue   value to assign to cell
   * @param  rowIndex   row of cell
   * @param  columnIndex  column of cell
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    if (columnIndex == SurfaceMapPointData.SURFACE_MAP_USED)
    {
      SurfaceMapPointData row = (SurfaceMapPointData)_rowData.elementAt(rowIndex);
      row.setElement(columnIndex, aValue);
    }
  }

  /**
   * Sort the data by the data in the selected column.  If this is the first
   * time for this column sort in assending order if this is the second time,
   * sort in decending order.  Alternate from then on.
   *
   * @author  Steve Anonson
   */
  public void sortColumn(int columnIndex)
  {
    if (columnIndex < 0 || columnIndex > getColumnCount())
      return;
    if (_sortColumn == columnIndex)
      _accendingSort = !_accendingSort;  // change sort direction
    else
    {
      _sortColumn = columnIndex;
      _accendingSort = true;
    }
    java.util.Collections.sort(this._rowData,
      new SurfMapTableComparator(this._sortColumn, this._accendingSort));
  }

  /**
   * Add a surface map point to the table data.
   *
   * @author  Steve Anonson
   * @param   surfaceMapPoint   surface map point data
   */
  public void addSurfaceMapPoint(SurfaceMapPointData surfaceMapPoint)
  {
    _rowData.addElement( surfaceMapPoint );
  }

// DEBUG
  /**
   * Fills the vector with default data.
   */
  protected void setDefaultData()
  {
    _rowData.removeAllElements();
    _rowData.addElement( new SurfaceMapPointData(1, 1.234, 0.000, true,  "Feature"));
    _rowData.addElement( new SurfaceMapPointData(2, 2.434, 0.000, false, "Feature"));
    _rowData.addElement( new SurfaceMapPointData(3, 5.200, 0.005, true,  "Feature"));
    _rowData.addElement( new SurfaceMapPointData(4, 1.567, 0.000, false, "Feature"));
    _rowData.addElement( new SurfaceMapPointData(5, 1.765, 0.001, true,  "Feature"));
    _rowData.addElement( new SurfaceMapPointData(6, 0.034, 0.000, true,  "X-ray"));
    _rowData.addElement( new SurfaceMapPointData(7, 3.009, 0.000, true,  "X-ray"));
  }
// DEBUG
}