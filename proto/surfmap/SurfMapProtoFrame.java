package proto.surfmap;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import com.agilent.xRayTest.gui.drawCAD.*;
import javax.swing.border.*;

/**
 * Surface map setup GUI frame.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class SurfMapProtoFrame extends JFrame
{
  private SurfMapProto      _parent = null;
  private ImageIcon         _helpImage;
  private ImageIcon         _camera1Image;
  private ImageIcon         _camera2Image;
  private ImageIcon         _checkmarkImage;
  private SurfMapTableModel _tableModel = null;

  /**
   * Constructor
   *
   * @author  Steve Anonson
   */
  public SurfMapProtoFrame(SurfMapProto parent)
  {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    Image image = com.agilent.xRayTest.images.Image5DX.getImage( com.agilent.xRayTest.images.Image5DX.FRAME_5DX );
    this.setIconImage(image);
    _parent = parent;
  }

  /**
   * Component initialization
   *
   * @author  Steve Anonson
   */
  private void jbInit() throws Exception
  {
    _helpImage = new ImageIcon(proto.surfmap.SurfMapProtoFrame.class.getResource("help.gif"));
    _camera1Image = new ImageIcon(proto.surfmap.SurfMapProtoFrame.class.getResource("board_gray.jpg"));
    _camera2Image = new ImageIcon(proto.surfmap.SurfMapProtoFrame.class.getResource("board_gray2.jpg"));
    //setIconImage(Toolkit.getDefaultToolkit().createImage(SurfMapProtoFrame.class.getResource("[Your Icon]")));
    _checkmarkImage = new ImageIcon(proto.surfmap.SurfMapProtoFrame.class.getResource("checkmark.gif"));

    _contentPane = (JPanel) this.getContentPane();
    _contentPane.setLayout(_borderLayout1);
    this.setSize(new Dimension(800, 550));
    this.setTitle(" Surface Map Prototype ");

    // File menu
    _fileMenu.setMnemonic('F');
    _fileMenu.setText("File");
    _fileSaveMI.setMnemonic('S');
    _fileSaveMI.setText("Save");
    _fileExitMI.setMnemonic('X');
    _fileExitMI.setText("Exit");
    _fileExitMI.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _fileExitMI_actionPerformed(e);
      }
    });
    _fileMenu.add(_fileSaveMI);
    _fileMenu.add(_fileExitMI);

    // View menu
    _viewMenu.setMnemonic('V');
    _viewMenu.setText("View");
    _viewZoomInMI.setMnemonic('I');
    _viewZoomInMI.setText("Zoom In");
    _viewZoomOutMI.setMnemonic('O');
    _viewZoomOutMI.setText("Zoom Out");
    _viewZoomToMI.setMnemonic('T');
    _viewZoomToMI.setText("Zoom To Highlight");
    _viewFlipMI.setMnemonic('F');
    _viewFlipMI.setText("Flip");
    _viewResetMI.setMnemonic('R');
    _viewResetMI.setText("Reset");
    _viewFindMI.setMnemonic('F');
    _viewFindMI.setText("Find...");
    _viewMenu.add(_viewZoomInMI);
    _viewMenu.add(_viewZoomOutMI);
    _viewMenu.add(_viewZoomToMI);
    _viewMenu.add(_viewFlipMI);
    _viewMenu.add(_viewResetMI);
    _viewMenu.addSeparator();
    _viewMenu.add(_viewFindMI);

    // Tools menu
    _toolsMenu.setMnemonic('T');
    _toolsMenu.setText("Tools");
    _toolsLearnMI.setMnemonic('L');
    _toolsLearnMI.setText("Learn Map Points");
    _toolsLearnMI.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _toolsLearnMI_actionPerformed(e);
      }
    });
    _toolsAdjustMI.setMnemonic('A');
    _toolsAdjustMI.setText("Adjust Z Height");
    _toolsAddMI.setMnemonic('X');
    _toolsAddMI.setText("Add X-ray Map Point");
    _toolsMenu.add(_toolsLearnMI);
    _toolsMenu.add(_toolsAdjustMI);
    _toolsMenu.add(_toolsAddMI);
    _toolsMenu.addSeparator();

    // Help menu
    _helpMenu.setMnemonic('H');
    _helpMenu.setText("Help");
    _helpContents.setMnemonic('C');
    _helpContents.setText("Contents");
    _helpAboutMI.setMnemonic('A');
    _helpAboutMI.setText("About");
    _helpAboutMI.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _helpAboutMI_actionPerformed(e);
      }
    });
    _helpMenu.add(_helpContents);
    _helpMenu.add(_helpAboutMI);

    _menuBar.add(_fileMenu);
    _menuBar.add(_viewMenu);
    _menuBar.add(_toolsMenu);
    _menuBar.add(_helpMenu);

    // Tool bar
    _saveBtn.setText("Save");
    _zoomInBtn.setText("Zoom In");
    _zoomOutBtn.setText("Zoom Out");
    _zoomToBtn.setText("Zoom To");
    _flipBtn.setText("Flip");
    _resetBtn.setText("Reset");
    _findBtn.setText("Find");
    _helpBtn.setIcon(_helpImage);
    _helpBtn.setToolTipText("Help");
    _toolBar.add(_saveBtn);
    _toolBar.addSeparator();
    _toolBar.addSeparator();
    _toolBar.add(_helpBtn);
    _toolBar.addSeparator();
    _toolBar.add(_zoomInBtn);
    _toolBar.add(_zoomOutBtn);
    _toolBar.add(_zoomToBtn);
    _toolBar.add(_resetBtn);
    _toolBar.add(_flipBtn);
    _toolBar.addSeparator();
    _toolBar.add(_findBtn);

    // Control panel
    _learnBtn.setText("Learn Surface Map");
    _learnBtn.setIcon(_checkmarkImage);
    _learnBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _toolsLearnMI_actionPerformed(e);
      }
    });
    _adjustBtn.setEnabled(false);
    _adjustBtn.setText("Set Delta Z");
    _controlPanel.setBorder(BorderFactory.createEmptyBorder(15,5,10,5));
    _controlPanel.setLayout(_controlPanelLayout);
    _controlPanelLayout.setColumns(2);
    _controlPanelLayout.setHgap(10);
    _controlPanel.add(_learnBtn, null);
    _controlPanel.add(_adjustBtn, null);

    // Camera images
    camera1Lbl.setHorizontalAlignment(SwingConstants.CENTER);
    camera1Lbl.setHorizontalTextPosition(SwingConstants.CENTER);
    camera1Lbl.setVerticalTextPosition(SwingConstants.BOTTOM);
    camera1Lbl.setIcon(_camera1Image);
    camera1Lbl.setText("Camera 1");
    camera2Lbl.setHorizontalAlignment(SwingConstants.CENTER);
    camera2Lbl.setHorizontalTextPosition(SwingConstants.CENTER);
    camera2Lbl.setVerticalTextPosition(SwingConstants.BOTTOM);
    camera2Lbl.setIcon(_camera2Image);
    camera2Lbl.setText("Camera 2");
    _cameraPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Camera Image "));
    gridLayout1.setColumns(2);
    _cameraPanel.setLayout(gridLayout1);
    _cameraPanel.add(camera1Lbl, null);
    _cameraPanel.add(camera2Lbl, null);

    _imagePanel.setLayout(_borderLayout5);
    _imagePanel.add(_controlPanel, BorderLayout.NORTH);
    _imagePanel.add(_cameraPanel, BorderLayout.CENTER);

    // Surface map table
    initTable();  // setup the table
    _scrollPane1.getViewport().add(_dataTable, null);
    _dataPanel.setLayout(_borderLayout4);
    _dataPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134))," Surface Map Points "));
    _dataPanel.add(_scrollPane1, BorderLayout.CENTER);

    _westPanel.setLayout(_borderLayout2);
    _westPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
    _westPanel.add(_imagePanel, BorderLayout.SOUTH);
    _westPanel.add(_dataPanel, BorderLayout.CENTER);

    jLabel1.setBackground(Color.black);
    jLabel1.setFont(new java.awt.Font("Dialog", 1, 18));
    jLabel1.setForeground(Color.green);
    jLabel1.setOpaque(true);
    jLabel1.setHorizontalAlignment(SwingConstants.CENTER);
    jLabel1.setText("CAD drawing");
//    _cadDrawingPanel = new DrawCadPanel(640, 480);
//    _cadDrawingPanel.add(jLabel1);
    _drawingPanel.setLayout(_borderLayout3);
//    _drawingPanel.add(_cadDrawingPanel, BorderLayout.CENTER);
    _drawingPanel.add(jLabel1, BorderLayout.CENTER);

    _splitPane1.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
    _splitPane1.add(_drawingPanel, JSplitPane.RIGHT);
    _splitPane1.add(_westPanel, JSplitPane.LEFT);

    _contentPane.add(_toolBar, BorderLayout.NORTH);
    _contentPane.add(_splitPane1, BorderLayout.CENTER);
    this.setJMenuBar(_menuBar);
  }

  /**
   * Does the initialization of the JTable.
   *
   * @author  Steve Anonson
   */
  private void initTable()
  {
    // Create the table model for the surface map data and add to table.
    _tableModel = new SurfMapTableModel();
    _dataTable.setModel(_tableModel);

    // Set single selection mode.
//    _dataTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    // Add a JCheckBox as the editor in the "used" column.
    TableColumn column = _dataTable.getColumnModel().getColumn(SurfaceMapPointData.SURFACE_MAP_USED);
    JCheckBox booleanEditor = new JCheckBox();
    booleanEditor.setHorizontalAlignment(SwingConstants.CENTER);
    column.setCellEditor(new DefaultCellEditor( booleanEditor ));

    // Put a center alignment JTextField renderer in the "type" column.
    column = _dataTable.getColumnModel().getColumn(SurfaceMapPointData.SURFACE_MAP_TYPE);
    DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
    renderer.setHorizontalAlignment(SwingConstants.CENTER);
    column.setCellRenderer(renderer);

    // Add a column listener to catch clicks in the header for sorting.
    JTableHeader header = _dataTable.getTableHeader();
    header.addMouseListener( new TableColumnListener(_dataTable)
    {
      public void mouseClicked(MouseEvent e)
      {
        // Get the column heading that was click in.
        TableColumnModel columnModel = _table.getColumnModel();
        int columnModelIndex = columnModel.getColumnIndexAtX(e.getX());
        int columnIndex = columnModel.getColumn(columnModelIndex).getModelIndex();
        if (columnIndex < 0)
          return;

        // Call the sort method in the table model pass in the column.
        SurfMapTableModel model = SurfMapProtoFrame.this._tableModel;
        model.sortColumn(columnIndex);  // sort data
        _table.tableChanged( new TableModelEvent(model));  // trigger a change event
        _table.repaint();
      }
    });
  }

  /**
   * Initialize the GUI.  The cad is loaded here and if not found an error
   * is shown to the user.
   *
   * @author  Steve Anonson
   */
  public void initialize()
  {
  }

  /**
   * Exit method.  It confirms if the user hasn't save changes.
   *
   * @author  Steve Anonson
   */
  public void exitFrame()
  {
    if (_parent != null)
      _parent.exitApp();
  }

  /**
   * File | Exit action performed
   *
   * @author  Steve Anonson
   */
  public void _fileExitMI_actionPerformed(ActionEvent e)
  {
    exitFrame();
  }

  /**
   * Tools | Learn Surface Map action performed.
   * Called by menu item and button.
   *
   * @author  Steve Anonson
   */
  void _toolsLearnMI_actionPerformed(ActionEvent e)
  {
    SurfMapLearnDialog dlg = new SurfMapLearnDialog(this, " Learn Surface Map ", true);
    dlg.setLocationRelativeTo(this);
    dlg.show();
  }

  /**
   * Help | About action performed
   *
   * @author  Steve Anonson
   */
  public void _helpAboutMI_actionPerformed(ActionEvent e)
  {
    SurfMapProtoAboutBox dlg = new SurfMapProtoAboutBox(this);
    Dimension dlgSize = dlg.getPreferredSize();
    Dimension frmSize = getSize();
    Point loc = getLocation();
    dlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
    dlg.setModal(true);
    dlg.show();
  }

  /**
   * Overridden so we can exit when window is closed
   *
   * @author  Steve Anonson
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      _fileExitMI_actionPerformed(null);
    }
  }

  private JPanel _contentPane;
  private JMenuBar _menuBar = new JMenuBar();
  private JMenu _fileMenu = new JMenu();
  private JMenuItem _fileExitMI = new JMenuItem();
  private JMenu _helpMenu = new JMenu();
  private JMenuItem _helpAboutMI = new JMenuItem();
  private JToolBar _toolBar = new JToolBar();
  private JButton _saveBtn = new JButton();
  private JButton _findBtn = new JButton();
  private JButton _zoomInBtn = new JButton();
  private JButton _zoomOutBtn = new JButton();
  private JButton _zoomToBtn = new JButton();
  private JButton _flipBtn = new JButton();
  private JButton _resetBtn = new JButton();
  private JButton _helpBtn = new JButton();
  private BorderLayout _borderLayout1 = new BorderLayout();
  private JPanel _drawingPanel = new JPanel();
//  private DrawCadPanel _cadDrawingPanel = null;
  private JPanel _westPanel = new JPanel();
  private BorderLayout _borderLayout2 = new BorderLayout();
  private BorderLayout _borderLayout3 = new BorderLayout();
  private JLabel jLabel1 = new JLabel();
  private JScrollPane _scrollPane1 = new JScrollPane();
  private JTable _dataTable = new JTable(10, 5);
  private JPanel _imagePanel = new JPanel();
  private JPanel _dataPanel = new JPanel();
  private BorderLayout _borderLayout4 = new BorderLayout();
  private BorderLayout _borderLayout5 = new BorderLayout();
  private JSplitPane _splitPane1 = new JSplitPane();
  private JPanel _cameraPanel = new JPanel();
  private GridLayout gridLayout1 = new GridLayout();
  private JLabel camera1Lbl = new JLabel();
  private JLabel camera2Lbl = new JLabel();
  private JMenuItem _helpContents = new JMenuItem();
  private JMenuItem _fileSaveMI = new JMenuItem();
  private JMenu _viewMenu = new JMenu();
  private JMenuItem _viewZoomInMI = new JMenuItem();
  private JMenuItem _viewZoomOutMI = new JMenuItem();
  private JMenuItem _viewZoomToMI = new JMenuItem();
  private JMenuItem _viewFlipMI = new JMenuItem();
  private JMenuItem _viewResetMI = new JMenuItem();
  private JMenu _toolsMenu = new JMenu();
  private JMenuItem _toolsLearnMI = new JMenuItem();
  private JMenuItem _toolsAdjustMI = new JMenuItem();
  private JMenuItem _toolsAddMI = new JMenuItem();
  private JMenuItem _viewFindMI = new JMenuItem();
  private JPanel _controlPanel = new JPanel();
  private GridLayout _controlPanelLayout = new GridLayout();
  private JButton _learnBtn = new JButton();
  private JButton _adjustBtn = new JButton();
}