
/**
 * Title:        Algorithm Tuner<p>
 * Description:  <p>
 * Copyright:    Copyright (c) 1999<p>
 * Company:      Agilent Technologies<p>
 * @author Andy Mechtenberg
 * @version
 */
package proto.algoTuner;

import java.util.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.Component;
import javax.swing.JTable;
import java.util.EventObject;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

public class CustomThresholdEditor implements TableCellEditor
{
  private TableCellEditor _editor, _defaultEditor;
  private Hashtable _editors;
  private JTable _table;

  public CustomThresholdEditor(JTable table)
  {
    _table = table;
    _editors = new Hashtable();
    _defaultEditor = new DefaultCellEditor(new JTextField());
  }

  void setEditorAt(int row, TableCellEditor editor)
  {
    _editors.put(new Integer(row), editor);
  }

  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
  {
    return _editor.getTableCellEditorComponent(table, value, isSelected, row, column);
  }

  public Object getCellEditorValue()
  {
    return _editor.getCellEditorValue();
  }

  public boolean isCellEditable(EventObject anEvent)
  {
    selectEditor((MouseEvent) anEvent);
    return _editor.isCellEditable(anEvent);
  }

  public boolean shouldSelectCell(EventObject anEvent)
  {
    selectEditor((MouseEvent) anEvent);
    return _editor.shouldSelectCell(anEvent);
  }

  public boolean stopCellEditing()
  {
    return _editor.stopCellEditing();
  }

  public void cancelCellEditing()
  {
    _editor.cancelCellEditing();
  }

  public void addCellEditorListener(CellEditorListener l)
  {
    _editor.addCellEditorListener(l);
  }

  public void removeCellEditorListener(CellEditorListener l)
  {
    _editor.removeCellEditorListener(l);
  }

  private void selectEditor(MouseEvent e)
  {
    int row;
    if (e == null)
    {
      row = _table.getSelectionModel().getAnchorSelectionIndex();
    }
    else
    {
      row = _table.rowAtPoint(e.getPoint());
    }
    _editor = (TableCellEditor)_editors.get(new Integer(row));
    if (_editor == null)
      _editor = _defaultEditor;
  }
}
