
/**
 * Title:        Algorithm Tuner<p>
 * Description:  <p>
 * Copyright:    Copyright (c) 1999<p>
 * Company:      Agilent Technologies<p>
 * @author Andy Mechtenberg
 * @version
 */
package proto.algoTuner;

import java.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import java.awt.event.*;

public class AlgorithmConfigurationDlg extends JDialog
{
  final String FEEDBACK_EMPTY_MESSAGE = "No Diagnostics Specified";
  final String LEARNING_EMPTY_MESSAGE = "No Learning Specified";
  final String ONOFF_EMPTY_MESSAGE = "No Algorithms turned off";

  private FeedbackConfiguration _feedbackConfiguration = null;
  private FeedbackConfiguration _origFeedbackConfig = null;

  private FamilyAlgoPairing _learningConfiguration = null;
  private FamilyAlgoPairing _origLearningConfig = null;

  private FamilyAlgoPairing _onOffConfiguration = null;
  private FamilyAlgoPairing _origOnOffConfig = null;

  private AlgoTunerFrame _frame = null;

  JPanel panel1 = new JPanel();
  BorderLayout borderLayout1 = new BorderLayout();
  JPanel centerPanel = new JPanel();
  JPanel southPanel = new JPanel();
  JPanel northPanel = new JPanel();
  JPanel jPanel4 = new JPanel();
  JPanel jPanel5 = new JPanel();
  GridLayout gridLayout1 = new GridLayout();
  JComboBox _familyComboBox = new JComboBox();
  JLabel jLabel1 = new JLabel();
  JComboBox _algorithmComboBox = new JComboBox();
  JLabel jLabel2 = new JLabel();
  FlowLayout flowLayout1 = new FlowLayout();
  FlowLayout flowLayout2 = new FlowLayout();
  JButton _okButton = new JButton();
  JButton _cancelButton = new JButton();
  FlowLayout flowLayout3 = new FlowLayout();
  JTabbedPane jTabbedPane1 = new JTabbedPane();
  JPanel feedbackPanel = new JPanel();
  JPanel learningPanel = new JPanel();
  JPanel onOffPanel = new JPanel();
  BorderLayout borderLayout2 = new BorderLayout();
  JPanel jPanel6 = new JPanel();
  JPanel jPanel7 = new JPanel();
  JPanel jPanel8 = new JPanel();
  Border border1;
  TitledBorder titledBorder1;
  JPanel jPanel9 = new JPanel();
  BorderLayout borderLayout3 = new BorderLayout();
  GridLayout gridLayout2 = new GridLayout();
  JCheckBox _markMarginalJointsCheckBox = new JCheckBox();
  JCheckBox _markAcceptedJointsCheckBox = new JCheckBox();
  JCheckBox _drawAdvancedCheckBox = new JCheckBox();
  JCheckBox _diagnosticTextCheckBox = new JCheckBox();
  JCheckBox _drawBasicCheckBox = new JCheckBox();
  JCheckBox _pauseRejectedJointsCheckBox = new JCheckBox();
  JCheckBox _pauseMarginalJointsCheckBox = new JCheckBox();
  JCheckBox _drawMeasCheckBox = new JCheckBox();
  JCheckBox _pauseAcceptedJointsCheckBox = new JCheckBox();
  JCheckBox _markRejectedJointsCheckBox = new JCheckBox();
  JPanel jPanel10 = new JPanel();
  JButton _allFeedbackOffButton = new JButton();
  JButton _allFeedbackOnButton = new JButton();
  BorderLayout borderLayout4 = new BorderLayout();
  JPanel jPanel11 = new JPanel();
  JButton _removeFeedbackButton = new JButton();
  JButton _addFeedbackButton = new JButton();
  JButton _removeAllFeedbackButton = new JButton();
  GridLayout gridLayout3 = new GridLayout();
  JPanel jPanel12 = new JPanel();
  JPanel jPanel13 = new JPanel();
  BorderLayout borderLayout5 = new BorderLayout();
  JList _feedbackList;
  JList _learningList;
  JList _onOffList;
  JScrollPane jScrollPane1;
  private DefaultListModel _feedbackListModel;
  private DefaultListModel _learningListModel;
  private DefaultListModel _onOffListModel;
  Border border2;
  TitledBorder titledBorder2;
  BorderLayout borderLayout6 = new BorderLayout();
  JPanel jPanel1 = new JPanel();
  JPanel jPanel2 = new JPanel();
  BorderLayout borderLayout7 = new BorderLayout();
  JPanel jPanel14 = new JPanel();
  JButton _addJointLearningButton = new JButton();
  JButton _removeAllLearningButton = new JButton();
  JButton _removeLearningButton = new JButton();
  JButton _addSubtypeLearningButton = new JButton();
  BorderLayout borderLayout8 = new BorderLayout();
  JScrollPane jScrollPane2;
  Border border3;
  TitledBorder titledBorder3;
  JPanel jPanel15 = new JPanel();
  JPanel jPanel16 = new JPanel();
  JPanel jPanel17 = new JPanel();
  JPanel jPanel18 = new JPanel();
  JButton _addOffAlgorithmButton = new JButton();
  JScrollPane jScrollPane3;
  JPanel jPanel19 = new JPanel();
  JPanel jPanel20 = new JPanel();
  JPanel jPanel21 = new JPanel();
  GridLayout gridLayout5 = new GridLayout();
  BorderLayout borderLayout9 = new BorderLayout();
  BorderLayout borderLayout10 = new BorderLayout();
  BorderLayout borderLayout11 = new BorderLayout();
  JButton _removeOffAlgorithmButton = new JButton();
  JButton _removeAllAlgorithmOffButton = new JButton();
  JPanel jPanel110 = new JPanel();
  BorderLayout borderLayout12 = new BorderLayout();
  Border border4;
  TitledBorder titledBorder4;
  JButton _deleteLearnedDataButton = new JButton();
  GridLayout gridLayout6 = new GridLayout();
  JButton jButton1 = new JButton();
  JButton jButton2 = new JButton();
  GridLayout gridLayout4 = new GridLayout();
  Border border5;
  Border border6;
  JPanel jPanel3 = new JPanel();

  public AlgorithmConfigurationDlg(AlgoTunerFrame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    _frame = frame;
    if (frame != null)
    {
      _feedbackConfiguration = frame.getFeedbackConfiguration();
      _learningConfiguration = frame.getLearningConfiguration();
      _onOffConfiguration    = frame.getOnOffConfiguration();
    }
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public AlgorithmConfigurationDlg()
  {
    this(null, "", false);
  }

  void jbInit() throws Exception
  {
    _feedbackListModel = new DefaultListModel();
    _learningListModel = new DefaultListModel();
    _onOffListModel = new DefaultListModel();
    _feedbackList = new JList(_feedbackListModel);
    _learningList = new JList(_learningListModel);
    _onOffList = new JList(_onOffListModel);
    jScrollPane1 = new JScrollPane(_feedbackList);
    jScrollPane2 = new JScrollPane(_learningList);
    jScrollPane3 = new JScrollPane(_onOffList);

    border5 = BorderFactory.createEmptyBorder(10,0,0,0);
    border6 = BorderFactory.createEmptyBorder(0,10,10,0);
    _feedbackList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _learningList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _onOffList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    initializeFeedbackList();
    initializeLearningList();
    initializeOnOffList();

    _origFeedbackConfig = (FeedbackConfiguration)_feedbackConfiguration.clone();
    _origLearningConfig = (FamilyAlgoPairing)_learningConfiguration.clone();
    _origOnOffConfig = (FamilyAlgoPairing)_onOffConfiguration.clone();

    _feedbackList.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent lse)
      {
	feedbackListSelectionChanged(lse);
      }
    });

    border1 = new EtchedBorder(EtchedBorder.RAISED,Color.white,new Color(134, 134, 134));
    titledBorder1 = new TitledBorder(border1,"Diagnostics Definition");
    border2 = new EtchedBorder(EtchedBorder.RAISED,Color.white,new Color(134, 134, 134));
    titledBorder2 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134)),"Diagnostics Turned On");
    border3 = new EtchedBorder(EtchedBorder.RAISED,Color.white,new Color(134, 134, 134));
    titledBorder3 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134)),"Learning Turned On");

    border4 = new EtchedBorder(EtchedBorder.RAISED,Color.white,new Color(134, 134, 134));
    titledBorder4 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134)),"Algorithms Turned OFF");
    titledBorder4.setTitleColor(Color.red);
//    titledBorder4 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134)),"Algorithms Turned OFF",
//                                     TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.PLAIN, 12),Color.red);
    panel1.setLayout(borderLayout1);
    northPanel.setLayout(gridLayout1);
    gridLayout1.setRows(2);
    jLabel1.setPreferredSize(new Dimension(70, 17));
    jLabel1.setText("Family:");
    jLabel2.setPreferredSize(new Dimension(70, 17));
    jLabel2.setText("Algorithm:");
    jPanel5.setLayout(flowLayout1);
    flowLayout1.setAlignment(FlowLayout.LEFT);
    jPanel4.setLayout(flowLayout2);
    flowLayout2.setAlignment(FlowLayout.LEFT);
    _okButton.setPreferredSize(new Dimension(73, 27));
    _okButton.setText("OK");
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	okButton_actionPerformed(e);
      }
    });
    _cancelButton.setText("Cancel");
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	cancelButton_actionPerformed(e);
      }
    });
    southPanel.setLayout(flowLayout3);
    flowLayout3.setAlignment(FlowLayout.RIGHT);
    flowLayout3.setHgap(20);
    flowLayout3.setVgap(15);
    feedbackPanel.setLayout(borderLayout2);
    jPanel7.setLayout(borderLayout3);
    jPanel7.setBorder(titledBorder1);
    jPanel9.setLayout(gridLayout2);
    gridLayout2.setRows(10);
    _markMarginalJointsCheckBox.setSelected(true);
    _markMarginalJointsCheckBox.setText("Mark Marginal Joints");
    _markMarginalJointsCheckBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	feedbackCheckBox_actionPerformed(e);
      }
    });
    _markAcceptedJointsCheckBox.setSelected(true);
    _markAcceptedJointsCheckBox.setText("Mark Accepted Joints");
    _markAcceptedJointsCheckBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	feedbackCheckBox_actionPerformed(e);
      }
    });
    _drawAdvancedCheckBox.setSelected(true);
    _drawAdvancedCheckBox.setText("Draw Advanced Plots");
    _drawAdvancedCheckBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	feedbackCheckBox_actionPerformed(e);
      }
    });
    _diagnosticTextCheckBox.setSelected(true);
    _diagnosticTextCheckBox.setText("Display Diagnostic Text");
    _diagnosticTextCheckBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	feedbackCheckBox_actionPerformed(e);
      }
    });
    _drawBasicCheckBox.setSelected(true);
    _drawBasicCheckBox.setText("Draw Basic Plots");
    _drawBasicCheckBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	feedbackCheckBox_actionPerformed(e);
      }
    });
    _pauseRejectedJointsCheckBox.setSelected(true);
    _pauseRejectedJointsCheckBox.setText("Pause On Rejected Joints");
    _pauseRejectedJointsCheckBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	feedbackCheckBox_actionPerformed(e);
      }
    });
    _pauseMarginalJointsCheckBox.setSelected(true);
    _pauseMarginalJointsCheckBox.setText("Pause On Marginal Joints");
    _pauseMarginalJointsCheckBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	feedbackCheckBox_actionPerformed(e);
      }
    });
    _drawMeasCheckBox.setSelected(true);
    _drawMeasCheckBox.setText("Draw Measurement Regions");
    _drawMeasCheckBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	feedbackCheckBox_actionPerformed(e);
      }
    });
    _pauseAcceptedJointsCheckBox.setSelected(true);
    _pauseAcceptedJointsCheckBox.setText("Pause On Accpeted Joints");
    _pauseAcceptedJointsCheckBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	feedbackCheckBox_actionPerformed(e);
      }
    });
    _markRejectedJointsCheckBox.setSelected(true);
    _markRejectedJointsCheckBox.setText("Mark Rejected Joints");
    _markRejectedJointsCheckBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	feedbackCheckBox_actionPerformed(e);
      }
    });
    _allFeedbackOffButton.setText("All Off");
    _allFeedbackOffButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	allFeedbackOffButton_actionPerformed(e);
      }
    });
    _allFeedbackOnButton.setText("All On");
    _allFeedbackOnButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	allFeedbackOnButton_actionPerformed(e);
      }
    });
    jPanel6.setLayout(borderLayout4);
    _removeFeedbackButton.setText("Remove");
    _removeFeedbackButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	removeFeedbackButton_actionPerformed(e);
      }
    });
    _addFeedbackButton.setText("Add");
    _addFeedbackButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	addFeedbackButton_actionPerformed(e);
      }
    });
    _removeAllFeedbackButton.setText("Remove All");
    _removeAllFeedbackButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	removeAllFeedbackButton_actionPerformed(e);
      }
    });
    jPanel11.setLayout(gridLayout3);
    gridLayout3.setHgap(5);
    gridLayout3.setRows(3);
    gridLayout3.setVgap(10);
    jPanel12.setPreferredSize(new Dimension(10, 100));
    jPanel13.setPreferredSize(new Dimension(10, 90));
    jPanel8.setLayout(borderLayout5);
    jPanel9.setPreferredSize(new Dimension(183, 220));
    borderLayout2.setHgap(10);
    jPanel8.setBorder(titledBorder2);
    jPanel8.setPreferredSize(new Dimension(270, 160));
    learningPanel.setLayout(borderLayout6);
    borderLayout6.setHgap(10);
    jPanel1.setLayout(borderLayout7);
    jPanel14.setLayout(gridLayout4);
    _addJointLearningButton.setText("Add Joint Learning");
    _addJointLearningButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	addJointLearningButton_actionPerformed(e);
      }
    });
    _removeAllLearningButton.setText("Remove All");
    _removeAllLearningButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	removeAllLearningButton_actionPerformed(e);
      }
    });
    _removeLearningButton.setText("Remove");
    _removeLearningButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	removeLearningButton_actionPerformed(e);
      }
    });
    _addSubtypeLearningButton.setText("Add Subtype Learning");
    _addSubtypeLearningButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	addSubtypeLearningButton_actionPerformed(e);
      }
    });
    jPanel2.setLayout(borderLayout8);
    jPanel2.setBorder(titledBorder3);
    jPanel2.setPreferredSize(new Dimension(270, 160));
    jPanel15.setBorder(border6);
    jPanel15.setPreferredSize(new Dimension(10, 75));
    jPanel15.setLayout(gridLayout6);
    jPanel17.setPreferredSize(new Dimension(10, 100));
    jPanel18.setLayout(gridLayout5);
    _addOffAlgorithmButton.setText("Add");
    _addOffAlgorithmButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	addOffAlgorithmButton_actionPerformed(e);
      }
    });
    jPanel19.setPreferredSize(new Dimension(150, 10));
    jPanel20.setLayout(borderLayout9);
    jPanel21.setLayout(borderLayout10);
    gridLayout5.setHgap(5);
    gridLayout5.setRows(3);
    gridLayout5.setVgap(10);
    borderLayout11.setHgap(10);
    _removeOffAlgorithmButton.setText("Remove");
    _removeOffAlgorithmButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	removeOffAlgorithmButton_actionPerformed(e);
      }
    });
    _removeAllAlgorithmOffButton.setText("Remove All");
    _removeAllAlgorithmOffButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	removeAllAlgorithmOffButton_actionPerformed(e);
      }
    });
    jPanel110.setPreferredSize(new Dimension(10, 90));
    onOffPanel.setLayout(borderLayout12);
    jPanel20.setBorder(titledBorder4);
    jPanel20.setPreferredSize(new Dimension(270, 160));
    borderLayout12.setHgap(20);
    _familyComboBox.setPreferredSize(new Dimension(125, 21));
    _familyComboBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	familyComboBox_actionPerformed(e);
      }
    });
    _algorithmComboBox.setPreferredSize(new Dimension(125, 21));
    _algorithmComboBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	algorithmComboBox_actionPerformed(e);
      }
    });

    jTabbedPane1.addChangeListener(new ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
	jTabbedPane1StateChanged(e);
      }
    });

    _deleteLearnedDataButton.setText("Delete Joint Data");
    gridLayout6.setHgap(5);
    gridLayout6.setRows(2);
    gridLayout6.setVgap(10);
    jButton1.setText("Delete Subtype Data");
    jButton2.setText("Write Subtype Data");
    gridLayout4.setRows(4);
    gridLayout4.setVgap(15);
    jPanel16.setPreferredSize(new Dimension(130, 10));
    borderLayout7.setHgap(10);
    borderLayout7.setVgap(10);
    jPanel14.setBorder(border5);
    jPanel3.setPreferredSize(new Dimension(10, 100));
    getContentPane().add(panel1);
    panel1.add(centerPanel, BorderLayout.CENTER);
    centerPanel.add(jTabbedPane1, null);
    jTabbedPane1.add(feedbackPanel, "Diagnostics");
    feedbackPanel.add(jPanel6, BorderLayout.CENTER);
    jPanel6.add(jPanel11, BorderLayout.CENTER);
    jPanel11.add(_addFeedbackButton, null);
    jPanel11.add(_removeFeedbackButton, null);
    jPanel11.add(_removeAllFeedbackButton, null);
    jPanel6.add(jPanel12, BorderLayout.SOUTH);
    jPanel6.add(jPanel13, BorderLayout.NORTH);
    feedbackPanel.add(jPanel7, BorderLayout.WEST);
    jPanel7.add(jPanel9, BorderLayout.NORTH);

    jPanel9.add(_markRejectedJointsCheckBox, null);
    jPanel9.add(_markMarginalJointsCheckBox, null);
    jPanel9.add(_markAcceptedJointsCheckBox, null);

    jPanel9.add(_drawMeasCheckBox, null);
    jPanel9.add(_drawBasicCheckBox, null);
    jPanel9.add(_drawAdvancedCheckBox, null);

    jPanel9.add(_diagnosticTextCheckBox, null);

    jPanel9.add(_pauseRejectedJointsCheckBox, null);
    jPanel9.add(_pauseMarginalJointsCheckBox, null);
    jPanel9.add(_pauseAcceptedJointsCheckBox, null);

    jPanel7.add(jPanel10, BorderLayout.SOUTH);
    jPanel10.add(_allFeedbackOnButton, null);
    jPanel10.add(_allFeedbackOffButton, null);
    feedbackPanel.add(jPanel8, BorderLayout.EAST);
    jPanel8.add(jScrollPane1, BorderLayout.CENTER);
    jTabbedPane1.add(learningPanel, "Learning");
    learningPanel.add(jPanel1, BorderLayout.CENTER);
    jPanel1.add(jPanel14, BorderLayout.CENTER);
    jPanel14.add(_addJointLearningButton, null);
    jPanel14.add(_addSubtypeLearningButton, null);
    jPanel14.add(_removeLearningButton, null);
    jPanel14.add(_removeAllLearningButton, null);
//    jPanel1.add(jPanel15, BorderLayout.SOUTH);
    jPanel15.add(jButton2, null);
    jPanel15.add(jButton1, null);
    jPanel15.add(_deleteLearnedDataButton, null);
    jPanel1.add(jPanel16, BorderLayout.WEST);
    jPanel1.add(jPanel3, BorderLayout.SOUTH);
    learningPanel.add(jPanel2, BorderLayout.EAST);
    jPanel2.add(jScrollPane2, BorderLayout.CENTER);
    jTabbedPane1.add(onOffPanel, "ON/OFF");
    onOffPanel.add(jPanel19, BorderLayout.WEST);
    onOffPanel.add(jPanel20, BorderLayout.EAST);
    jPanel20.add(jScrollPane3, BorderLayout.CENTER);
    onOffPanel.add(jPanel21, BorderLayout.CENTER);
    jPanel21.add(jPanel18, BorderLayout.CENTER);
    jPanel18.add(_addOffAlgorithmButton, null);
    jPanel18.add(_removeOffAlgorithmButton, null);
    jPanel18.add(_removeAllAlgorithmOffButton, null);
    jPanel21.add(jPanel17, BorderLayout.SOUTH);
    jPanel21.add(jPanel110, BorderLayout.NORTH);
    panel1.add(southPanel, BorderLayout.SOUTH);
    southPanel.add(_okButton, null);
    southPanel.add(_cancelButton, null);
    panel1.add(northPanel, BorderLayout.NORTH);
    northPanel.add(jPanel5, null);
    jPanel5.add(jLabel1, null);
    jPanel5.add(_familyComboBox, null);
    northPanel.add(jPanel4, null);
    jPanel4.add(jLabel2, null);
    jPanel4.add(_algorithmComboBox, null);

    // populate comboboxes
    String[] families = FamilyDataset.getFamilies();
    for (int i = 0; i < families.length; i++)
      _familyComboBox.addItem(families[i]);
    _familyComboBox.setSelectedIndex(0);

//    String[] algos = Algorithms.getAlgorithmsForFamily((String)_familyComboBox.getSelectedItem());

//    for (int i = 0; i < algos.length; i++)
//      _algorithmComboBox.addItem(algos[i]);
//    _algorithmComboBox.addItem("All");
    _algorithmComboBox.setSelectedIndex(0);

    setAllButtonEnableState();
  }

  private void setAllButtonEnableState()
  {
    // feedback tab
    _addFeedbackButton.setEnabled(anyFeedbackChecksOn());
    if (_feedbackListModel.getSize() == 1)
    {
      String element = (String)_feedbackListModel.elementAt(0);
      if (element.equals(FEEDBACK_EMPTY_MESSAGE))
      {
	_removeAllFeedbackButton.setEnabled(false);
	_removeFeedbackButton.setEnabled(false);
      }
      else
      {
	_removeAllFeedbackButton.setEnabled(true);
	_removeFeedbackButton.setEnabled(true);
      }
    }


    // learning tab
    // only turn on learning where appropriate based on the following table
    //
    //                    | all:short  |  FPGullwing:SPC  |  FPGullwing:Shape  |  BGA2:SPC
    //  ------------------|------------|------------------|--------------------|----------
    //  Joint Learning    |    X       |                  |         X          |
    //                    |            |                  |                    |
    //  Subtype Learning  |            |        X         |         X          |     X

    // updated table (1/8/2001)
    // Joint Learning:
    //   all:short
    //   FPGullwing:SPC
    //   Gullwing:SPC
    //   JLead:SPC
    //   Socket:SPC
    //   Chip:SPC
    //   SOT:SPC
    //   LCC:SPC
    //   TAB:SPC
    //
    // Subtype Learning
    //   FPGullwing:SPC
    //   BGA2:SPC
    //   Connector:SPC
    //   Connector:Open
    //   FPGullwing:Shape
    //   Gullwing:Shape
    //   LCC:Shape
    //   Universal:Edge1
    //   Universal_2D:Edge2

    String family = (String)_familyComboBox.getSelectedItem();
    String algo = (String)_algorithmComboBox.getSelectedItem();
    if (family == null)
      return;
    if (algo == null)
      return;

    _addJointLearningButton.setEnabled(false);
    _addSubtypeLearningButton.setEnabled(false);

    if (algo.equals("Short"))
    {
      _addJointLearningButton.setEnabled(true);
    }
    if (algo.equals("SPC"))
    {
      if (family.equals("FPGullwing"))
	 _addJointLearningButton.setEnabled(true);
      if (family.equals("Gullwing"))
	 _addJointLearningButton.setEnabled(true);
      if (family.equals("JLead"))
	 _addJointLearningButton.setEnabled(true);
      if (family.equals("Socket"))
	 _addJointLearningButton.setEnabled(true);
      if (family.equals("CAP"))
	 _addJointLearningButton.setEnabled(true);
      if (family.equals("Chip"))
	 _addJointLearningButton.setEnabled(true);
      if (family.equals("SOT"))
	 _addJointLearningButton.setEnabled(true);
      if (family.equals("LCC"))
	 _addJointLearningButton.setEnabled(true);
      if (family.equals("TAB"))
	 _addJointLearningButton.setEnabled(true);
    }

    // subtype learning button
    if (algo.equals("SPC"))
    {
      if (family.equals("FPGullwing"))
	 _addSubtypeLearningButton.setEnabled(true);
      if (family.equals("BGA2"))
	 _addSubtypeLearningButton.setEnabled(true);
      if (family.equals("Connector"))
	 _addSubtypeLearningButton.setEnabled(true);
    }
    if (algo.equals("Open"))
    {
      if (family.equals("Connector"))
	 _addSubtypeLearningButton.setEnabled(true);
    }
    if (algo.equals("Shape"))
    {
      if (family.equals("FPGullwing"))
	 _addSubtypeLearningButton.setEnabled(true);
      if (family.equals("Gullwing"))
	 _addSubtypeLearningButton.setEnabled(true);
      if (family.equals("LCC"))
	 _addSubtypeLearningButton.setEnabled(true);
    }
    if ((algo.equals("Universal")) && (family.equals("Edge1")))
    {
       _addSubtypeLearningButton.setEnabled(true);
    }
    if ((algo.equals("Universal_2D")) && (family.equals("Edge2")))
    {
       _addSubtypeLearningButton.setEnabled(true);
    }


    if (_learningListModel.getSize() == 1)
    {
      String element = (String)_learningListModel.elementAt(0);
      if (element.equals(LEARNING_EMPTY_MESSAGE))
      {
	_removeAllLearningButton.setEnabled(false);
	_removeLearningButton.setEnabled(false);
      }
      else
      {
	_removeAllLearningButton.setEnabled(true);
	_removeLearningButton.setEnabled(true);
      }
    }

    // on off tab
    if (_onOffListModel.getSize() == 1)
    {
      String element = (String)_onOffListModel.elementAt(0);
      if (element.equals(ONOFF_EMPTY_MESSAGE))
      {
	_removeAllAlgorithmOffButton.setEnabled(false);
	_removeOffAlgorithmButton.setEnabled(false);
      }
      else
      {
	_removeAllAlgorithmOffButton.setEnabled(true);
	_removeOffAlgorithmButton.setEnabled(true);
      }
    }
  }

  void cancelButton_actionPerformed(ActionEvent e)
  {
    _frame.setFeedbackConfiguration(_origFeedbackConfig);
    _frame.setLearningConfiguration(_origLearningConfig);
    _frame.setOnOffConfiguration(_origOnOffConfig);
    dispose();
  }

  void okButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  void familyComboBox_actionPerformed(ActionEvent e)
  {
    String family = (String)_familyComboBox.getSelectedItem();
    _algorithmComboBox.removeAllItems();
    String[] algos = Algorithms.getAlgorithmsForFamily((String)_familyComboBox.getSelectedItem());
    for (int i = 0; i < algos.length; i++)
      _algorithmComboBox.addItem(algos[i]);
    _algorithmComboBox.setSelectedIndex(0);

    // if the current tab is learning -- change the algo "All" to "Short"
    if (family.equalsIgnoreCase("All"))
    {
      if (jTabbedPane1.getSelectedIndex() == 1)
      {
	_algorithmComboBox.removeAllItems();
	_algorithmComboBox.addItem("Short");
      }
      else // if the current tab is feedback or on/off -- change the algo "All" plus some algs
      {
	_algorithmComboBox.removeAllItems();
	_algorithmComboBox.addItem("All");
	_algorithmComboBox.addItem("Locator");
	_algorithmComboBox.addItem("SPC");
	_algorithmComboBox.addItem("Excess");
	_algorithmComboBox.addItem("Insufficient");
	_algorithmComboBox.addItem("Misalign");
	_algorithmComboBox.addItem("Open");
	_algorithmComboBox.addItem("Short");
      }
    }

  }


  void algorithmComboBox_actionPerformed(ActionEvent e)
  {
    String family = (String)_familyComboBox.getSelectedItem();
    String algo = (String)_algorithmComboBox.getSelectedItem();
    if (family != null && algo != null)
      setAllButtonEnableState();
  }

  void addFeedbackButton_actionPerformed(ActionEvent e)
  {
    String family = (String)_familyComboBox.getSelectedItem();
    String algo = (String)_algorithmComboBox.getSelectedItem();

    FeedbackSettings fbs = new FeedbackSettings();
    SetupFeedbackSettings(fbs);
    _feedbackConfiguration.addSetting(family, algo, fbs);

    initializeFeedbackList();
    _feedbackList.setSelectedIndex(_feedbackListModel.getSize()-1);
  }

  void feedbackListSelectionChanged(ListSelectionEvent lse)
  {
    if (lse.getValueIsAdjusting())
      return;

    JList theList = (JList)lse.getSource();
    String element = (String)theList.getSelectedValue();

    //System.out.println(element);
    String family = element.substring(0, element.indexOf(' ',0));
    String algo = element.substring(element.lastIndexOf(' ')+1, element.length());
    //System.out.println(">"+family+"<" + " - " + ">"+algo+"<");

    FeedbackSettings fbs = _feedbackConfiguration.getFeedbackSettings(family, algo);
    if (fbs != null)
      SetupFeedbackCheckBoxes(fbs);
  }

  void SetupFeedbackSettings(FeedbackSettings fbs)
  {
    fbs.setMarkRejectedJoints(_markRejectedJointsCheckBox.isSelected());
    fbs.setMarkMarginalJoints(_markMarginalJointsCheckBox.isSelected());
    fbs.setMarkAcceptedJoints(_markAcceptedJointsCheckBox.isSelected());

    fbs.setDrawMeas(_drawMeasCheckBox.isSelected());
    fbs.setDrawBasic(_drawBasicCheckBox.isSelected());
    fbs.setDrawAdvanced(_drawAdvancedCheckBox.isSelected());

    fbs.setDiagnosticText(_diagnosticTextCheckBox.isSelected());

    fbs.setPauseRejectedJoints(_pauseRejectedJointsCheckBox.isSelected());
    fbs.setPauseMarginalJoints(_pauseMarginalJointsCheckBox.isSelected());
    fbs.setPauseAcceptedJoints(_pauseAcceptedJointsCheckBox.isSelected());
  }

  boolean anyFeedbackChecksOn()
  {
    if (_markRejectedJointsCheckBox.isSelected())
      return true;
    if (_markMarginalJointsCheckBox.isSelected())
      return true;
    if (_markAcceptedJointsCheckBox.isSelected())
      return true;
    if (_drawMeasCheckBox.isSelected())
      return true;
    if (_drawBasicCheckBox.isSelected())
      return true;
    if (_drawAdvancedCheckBox.isSelected())
      return true;
    if (_diagnosticTextCheckBox.isSelected())
      return true;
    if (_pauseRejectedJointsCheckBox.isSelected())
      return true;
    if (_pauseMarginalJointsCheckBox.isSelected())
      return true;
    if (_pauseAcceptedJointsCheckBox.isSelected())
      return true;

    return false;

  }

  void SetupFeedbackCheckBoxes(FeedbackSettings fbs)
  {
    _markRejectedJointsCheckBox.setSelected(fbs.getMarkRejectedJoints());
    _markMarginalJointsCheckBox.setSelected(fbs.getMarkMarginalJoints());
    _markAcceptedJointsCheckBox.setSelected(fbs.getMarkAcceptedJoints());

    _drawMeasCheckBox.setSelected(fbs.getDrawMeas());
    _drawBasicCheckBox.setSelected(fbs.getDrawBasic());
    _drawAdvancedCheckBox.setSelected(fbs.getDrawAdvanced());

    _diagnosticTextCheckBox.setSelected(fbs.getDiagnosticText());

    _pauseRejectedJointsCheckBox.setSelected(fbs.getPauseRejectedJoints());
    _pauseMarginalJointsCheckBox.setSelected(fbs.getPauseMarginalJoints());
    _pauseAcceptedJointsCheckBox.setSelected(fbs.getPauseAcceptedJoints());
  }

  void initializeFeedbackList()
  {
    // initial list setup goes here
    _feedbackListModel.clear();
    if (_feedbackConfiguration.anyFeebackSettings())
    {
      for (Iterator it = _feedbackConfiguration.getSettings().iterator(); it.hasNext(); )
      {
	String family = (String)it.next();
	String algo   = (String)it.next();
	_feedbackListModel.addElement(family + " - " + algo);
      }
    }
    else
    {
      _feedbackListModel.addElement(FEEDBACK_EMPTY_MESSAGE);
    }
    setAllButtonEnableState();
  }

  void initializeLearningList()
  {
    // initial list setup goes here
    _learningListModel.clear();
    if (_learningConfiguration.anyPairSettings())
    {
      for (Iterator it = _learningConfiguration.getSettings().iterator(); it.hasNext(); )
      {
	String family = (String)it.next();
	String algo   = (String)it.next();
	_learningListModel.addElement(family + " - " + algo);
      }
    }
    else
    {
      _learningListModel.addElement(LEARNING_EMPTY_MESSAGE);
    }
    setAllButtonEnableState();
  }

  void initializeOnOffList()
  {
    // initial list setup goes here
    _onOffListModel.clear();
    if (_onOffConfiguration.anyPairSettings())
    {
      for (Iterator it = _onOffConfiguration.getSettings().iterator(); it.hasNext(); )
      {
	String family = (String)it.next();
	String algo   = (String)it.next();
	_onOffListModel.addElement(family + " - " + algo);
      }
    }
    else
    {
      _onOffListModel.addElement(ONOFF_EMPTY_MESSAGE);
    }
    setAllButtonEnableState();
  }


  void removeAllFeedbackButton_actionPerformed(ActionEvent e)
  {
    _feedbackConfiguration.removeAllSettings();
    initializeFeedbackList();
  }

  void removeFeedbackButton_actionPerformed(ActionEvent e)
  {
    int index = _feedbackList.getSelectedIndex();
    System.out.println(index);
    if (index == -1)  // no current selection, do nothing
      return;

    String element = (String)_feedbackList.getSelectedValue();
    if (element.equals(FEEDBACK_EMPTY_MESSAGE))
      return;
    //System.out.println(element);
    String family = element.substring(0, element.indexOf(' ',0));
    String algo = element.substring(element.lastIndexOf(' ')+1, element.length());
    //System.out.println(">"+family+"<" + " - " + ">"+algo+"<");

    _feedbackConfiguration.removeSetting(family, algo);
    initializeFeedbackList();

    if (_feedbackListModel.getSize() == 0)
      return;

    if (index == _feedbackListModel.getSize())
      index--;
    _feedbackList.setSelectedIndex(index);
  }

  void allFeedbackOnButton_actionPerformed(ActionEvent e)
  {
    _markRejectedJointsCheckBox.setSelected(true);
    _markMarginalJointsCheckBox.setSelected(true);
    _markAcceptedJointsCheckBox.setSelected(true);

    _drawMeasCheckBox.setSelected(true);
    _drawBasicCheckBox.setSelected(true);
    _drawAdvancedCheckBox.setSelected(true);

    _diagnosticTextCheckBox.setSelected(true);

    _pauseRejectedJointsCheckBox.setSelected(true);
    _pauseMarginalJointsCheckBox.setSelected(true);
    _pauseAcceptedJointsCheckBox.setSelected(true);

    setAllButtonEnableState();
  }

  void allFeedbackOffButton_actionPerformed(ActionEvent e)
  {
    _markRejectedJointsCheckBox.setSelected(false);
    _markMarginalJointsCheckBox.setSelected(false);
    _markAcceptedJointsCheckBox.setSelected(false);

    _drawMeasCheckBox.setSelected(false);
    _drawBasicCheckBox.setSelected(false);
    _drawAdvancedCheckBox.setSelected(false);

    _diagnosticTextCheckBox.setSelected(false);

    _pauseRejectedJointsCheckBox.setSelected(false);
    _pauseMarginalJointsCheckBox.setSelected(false);
    _pauseAcceptedJointsCheckBox.setSelected(false);

    setAllButtonEnableState();
  }

  void addOffAlgorithmButton_actionPerformed(ActionEvent e)
  {
    String family = (String)_familyComboBox.getSelectedItem();
    String algo = (String)_algorithmComboBox.getSelectedItem();

    _onOffConfiguration.addSetting(family, algo);

    initializeOnOffList();
    _onOffList.setSelectedIndex(_onOffListModel.getSize()-1);
  }

  void removeOffAlgorithmButton_actionPerformed(ActionEvent e)
  {
    int index = _onOffList.getSelectedIndex();
    if (index == -1)  // no current selection, do nothing
      return;

    String element = (String)_onOffList.getSelectedValue();
    if (element.equals(ONOFF_EMPTY_MESSAGE))
      return;
    //System.out.println(element);
    String family = element.substring(0, element.indexOf(' ',0));
    String algo = element.substring(element.lastIndexOf(' ')+1, element.length());
    //System.out.println(">"+family+"<" + " - " + ">"+algo+"<");

    _onOffConfiguration.removeSetting(family, algo);
    initializeOnOffList();

    if (_onOffListModel.getSize() == 0)
      return;

    if (index == _onOffListModel.getSize())
      index--;
    _onOffList.setSelectedIndex(index);
  }

  void removeAllAlgorithmOffButton_actionPerformed(ActionEvent e)
  {
    _onOffConfiguration.removeAllSettings();
    initializeOnOffList();
  }

  void addJointLearningButton_actionPerformed(ActionEvent e)
  {
    String family = (String)_familyComboBox.getSelectedItem();
    String algo = (String)_algorithmComboBox.getSelectedItem();

    algo = algo + " (Joint)";

    _learningConfiguration.addSetting(family, algo);

    initializeLearningList();
    _learningList.setSelectedIndex(_learningListModel.getSize()-1);
  }

  void addSubtypeLearningButton_actionPerformed(ActionEvent e)
  {
    String family = (String)_familyComboBox.getSelectedItem();
    String algo = (String)_algorithmComboBox.getSelectedItem();

    algo = algo + " (Subtype)";

    _learningConfiguration.addSetting(family, algo);

    initializeLearningList();
    _learningList.setSelectedIndex(_learningListModel.getSize()-1);
  }

  void removeLearningButton_actionPerformed(ActionEvent e)
  {
    int index = _learningList.getSelectedIndex();
    if (index == -1)  // no current selection, do nothing
      return;

    String element = (String)_learningList.getSelectedValue();
    if (element.equals(LEARNING_EMPTY_MESSAGE))
      return;
    //System.out.println(element);
    String family = element.substring(0, element.indexOf(' ',0));
    String algo = element.substring(element.lastIndexOf(' ')+1, element.length());
    //System.out.println(">"+family+"<" + " - " + ">"+algo+"<");

    _learningConfiguration.removeSetting(family, algo);
    initializeLearningList();

    if (_learningListModel.getSize() == 0)
      return;

    if (index == _learningListModel.getSize())
      index--;
    _learningList.setSelectedIndex(index);
  }

  void removeAllLearningButton_actionPerformed(ActionEvent e)
  {
    _learningConfiguration.removeAllSettings();
    initializeLearningList();
  }

  void feedbackCheckBox_actionPerformed(ActionEvent e)
  {
    setAllButtonEnableState();
  }
  void jTabbedPane1StateChanged(ChangeEvent e)
  {
    String family = (String)_familyComboBox.getSelectedItem();
    if (family == null)
      return;
    // if the current tab is learning -- change the algo "All" to "Short"
    // if the current tab is learning -- change the algo "All" to "Short"
    if (family.equalsIgnoreCase("All"))
    {
      if (jTabbedPane1.getSelectedIndex() == 1)
      {
	_algorithmComboBox.removeAllItems();
	_algorithmComboBox.addItem("Short");
      }
      else // if the current tab is feedback or on/off -- change the algo "All" plus some algs
      {
	_algorithmComboBox.removeAllItems();
	_algorithmComboBox.addItem("All");
	_algorithmComboBox.addItem("Locator");
	_algorithmComboBox.addItem("SPC");
	_algorithmComboBox.addItem("Excess");
	_algorithmComboBox.addItem("Insufficient");
	_algorithmComboBox.addItem("Misalign");
	_algorithmComboBox.addItem("Open");
	_algorithmComboBox.addItem("Short");
      }
    }
  }
}
