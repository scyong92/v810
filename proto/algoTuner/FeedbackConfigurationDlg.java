
/**
 * Title:        Algorithm Tuner<p>
 * Description:  <p>
 * Copyright:    Copyright (c) 1999<p>
 * Company:      Agilent Technologies<p>
 * @author Andy Mechtenberg
 * @version
 */
package proto.algoTuner;

import java.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;

public class FeedbackConfigurationDlg extends JDialog
{

  final String EMPTY_MESSAGE = "No Feedback Specified";
  JPanel panel1 = new JPanel();
  BorderLayout borderLayout1 = new BorderLayout();

  FeedbackConfiguration _feedbackConfiguration = null;
  JPanel jPanel1 = new JPanel();
  JPanel jPanel2 = new JPanel();
  Border border1;
  JPanel jPanel3 = new JPanel();
  JPanel jPanel4 = new JPanel();
  JPanel jPanel5 = new JPanel();
  JPanel jPanel6 = new JPanel();
  JPanel jPanel7 = new JPanel();
  JComboBox _familyComboBox = new JComboBox();
  JLabel jLabel1 = new JLabel();
  JComboBox _algorithmComboBox = new JComboBox();
  JLabel jLabel2 = new JLabel();
  GridLayout gridLayout1 = new GridLayout();
  JButton _addButton = new JButton();
  JPanel jPanel8 = new JPanel();
  JPanel jPanel9 = new JPanel();
  JButton _okButton = new JButton();
  JButton _cancelButton = new JButton();
  JButton _deleteAllButton = new JButton();
  JButton _deleteButton = new JButton();
  GridLayout gridLayout2 = new GridLayout();
  FlowLayout flowLayout1 = new FlowLayout();
  FlowLayout flowLayout2 = new FlowLayout();
  BorderLayout borderLayout2 = new BorderLayout();
  JLabel jLabel3 = new JLabel();
  JList jList1;
  JScrollPane jScrollPane1;
  private DefaultListModel _listModel;

  public FeedbackConfigurationDlg(Frame frame, String title, boolean modal, FeedbackConfiguration feedbackConfiguration)
  {
    super(frame, title, modal);
    _feedbackConfiguration = feedbackConfiguration;
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public FeedbackConfigurationDlg()
  {
    this(null, "", false, new FeedbackConfiguration());
  }

  void jbInit() throws Exception
  {
    _listModel = new DefaultListModel();
    border1 = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(178, 178, 178),new Color(124, 124, 124)),BorderFactory.createEmptyBorder(5,5,5,5));
    panel1.setLayout(borderLayout1);

    if (_feedbackConfiguration.getSettings().size() == 0)
    {
      _listModel.addElement(EMPTY_MESSAGE);
    }
    else
    {
      for (Iterator it = _feedbackConfiguration.getSettings().iterator(); it.hasNext(); )
      {
        String family = (String)it.next();
        String algo   = (String)it.next();
        _listModel.addElement(family + " - " + algo);
      }
    }
    jList1 = new JList(_listModel);
    jScrollPane1 = new JScrollPane(jList1);
    jPanel2.setLayout(borderLayout2);
    jLabel1.setPreferredSize(new Dimension(55, 17));
    jLabel1.setText("Family:");
    jLabel2.setText("Algorithm:");
    jPanel5.setLayout(gridLayout1);
    gridLayout1.setRows(2);
    _addButton.setMnemonic('A');
    _addButton.setText("Add");
    _addButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        _addButton_actionPerformed(e);
      }
    });
    _okButton.setFont(new java.awt.Font("Dialog", 1, 12));
    _okButton.setMnemonic('O');
    _okButton.setText("Ok");
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        _okButton_actionPerformed(e);
      }
    });
    _cancelButton.setMnemonic('C');
    _cancelButton.setText("Cancel");
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        _cancelButton_actionPerformed(e);
      }
    });
    _deleteAllButton.setMnemonic('L');
    _deleteAllButton.setText("Delete All");
    _deleteAllButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        _deleteAllButton_actionPerformed(e);
      }
    });
    _deleteButton.setMnemonic('D');
    _deleteButton.setText("Delete");
    _deleteButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        _deleteButton_actionPerformed(e);
      }
    });
    jPanel1.setLayout(gridLayout2);
    jPanel8.setLayout(flowLayout1);
    flowLayout1.setAlignment(FlowLayout.RIGHT);
    jPanel9.setLayout(flowLayout2);
    flowLayout2.setAlignment(FlowLayout.LEFT);
    jLabel3.setText("Current Feedback Settings:");
    _familyComboBox.setPreferredSize(new Dimension(125, 21));
    _familyComboBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        _familyComboBox_actionPerformed(e);
      }
    });
    _algorithmComboBox.setPreferredSize(new Dimension(125, 21));
//    jList1.setPreferredSize(new Dimension(0, 120));
    getContentPane().add(panel1);
    panel1.add(jPanel1, BorderLayout.SOUTH);
    jPanel1.add(jPanel9, null);
    jPanel9.add(_deleteButton, null);
    jPanel9.add(_deleteAllButton, null);
    jPanel1.add(jPanel8, null);
    jPanel8.add(_okButton, null);
    jPanel8.add(_cancelButton, null);
    panel1.add(jPanel2, BorderLayout.CENTER);
    jPanel2.add(jLabel3, BorderLayout.NORTH);
    jPanel2.add(jScrollPane1, BorderLayout.CENTER);
   // jScrollPane1.getViewport().add(jList1, null);
    panel1.add(jPanel3, BorderLayout.NORTH);
    jPanel3.add(jPanel5, null);
    jPanel5.add(jPanel7, null);
    jPanel7.add(jLabel1, null);
    jPanel7.add(_familyComboBox, null);
    jPanel5.add(jPanel6, null);
    jPanel6.add(jLabel2, null);
    jPanel6.add(_algorithmComboBox, null);
    jPanel3.add(jPanel4, null);
    jPanel4.add(_addButton, null);

    // populate comboboxes
    String[] families = FamilyDataset.getFamilies();
    for (int i = 0; i < families.length; i++)
      _familyComboBox.addItem(families[i]);
    _familyComboBox.setSelectedIndex(0);

    String[] algos = Algorithms.getAlgorithmsForFamily((String)_familyComboBox.getSelectedItem());
    for (int i = 0; i < algos.length; i++)
      _algorithmComboBox.addItem(algos[i]);
    _algorithmComboBox.setSelectedIndex(0);

  }

  void _okButton_actionPerformed(ActionEvent e)
  {
    _feedbackConfiguration.removeAllSettings();

    if (_listModel.isEmpty())
    {
      dispose();
      return;
    }

    String firstMessage = (String)_listModel.firstElement();
    if (firstMessage.equals(EMPTY_MESSAGE) == false)
      for (int i = 0; i < _listModel.getSize(); i++)
      {
        String element = (String)_listModel.getElementAt(i);
        String family = element.substring(0, element.indexOf(' ',0));
        String algo = element.substring(element.lastIndexOf(' ')+1, element.length());
        System.out.println(">"+family+"<" + " - " + ">"+algo+"<");
  //      _feedbackConfiguration.addSetting(family, algo);
      }
    dispose();
  }

  void _cancelButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  void _familyComboBox_actionPerformed(ActionEvent e)
  {
    String family = (String)_familyComboBox.getSelectedItem();
    _algorithmComboBox.removeAllItems();
    String[] algos = Algorithms.getAlgorithmsForFamily((String)_familyComboBox.getSelectedItem());
    for (int i = 0; i < algos.length; i++)
      _algorithmComboBox.addItem(algos[i]);
    _algorithmComboBox.setSelectedIndex(0);
  }

  void _addButton_actionPerformed(ActionEvent e)
  {
    String family = (String)_familyComboBox.getSelectedItem();
    String algo = (String)_algorithmComboBox.getSelectedItem();

    String firstMessage = (String)_listModel.firstElement();
    if (firstMessage.equals(EMPTY_MESSAGE))
      _listModel.clear();

    _listModel.addElement(family + " - " + algo);
  }

  void _deleteButton_actionPerformed(ActionEvent e)
  {
    int index = jList1.getSelectedIndex();
    if (index == -1)
      return;
    _listModel.remove(index);
  }

  void _deleteAllButton_actionPerformed(ActionEvent e)
  {
    _listModel.clear();
    _listModel.addElement(EMPTY_MESSAGE);
  }
}
