
/**
 * Title:        Algorithm Tuner<p>
 * Description:  <p>
 * Copyright:    Copyright (c) 1999<p>
 * Company:      Agilent Technologies<p>
 * @author Andy Mechtenberg
 * @version
 */
package proto.algoTuner;

import javax.swing.*;
import javax.swing.text.*;

import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class FloatNumberField extends JTextField
{
  private Toolkit toolkit;
  private NumberFormat floatFormatter;

  public FloatNumberField(float value, int columns)
  {
    super(columns);
    toolkit = Toolkit.getDefaultToolkit();
    floatFormatter = NumberFormat.getNumberInstance(Locale.US);
    floatFormatter.setParseIntegerOnly(false);
    setValue(value);
  }

  public float getValue()
  {
    float retVal = 0;
    try
    {
      retVal = floatFormatter.parse(getText()).floatValue();
    }
    catch (ParseException e)
    {
      // This should never happen because insertString allows
      // only properly formatted data to get in the field.
      toolkit.beep();
    }
    return retVal;
  }

  public void setValue(float value)
  {
    setText(floatFormatter.format(value));
  }

  protected Document createDefaultModel()
  {
    return new FloatNumberDocument();
  }

  protected class FloatNumberDocument extends PlainDocument
  {
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
    {
      char[] source = str.toCharArray();
      char[] result = new char[source.length];
      int j = 0;

      for (int i = 0; i < result.length; i++)
      {
        if (Character.isDigit(source[i]) || source[i] == '.')
          result[j++] = source[i];
        else
        {
          toolkit.beep();
          System.err.println("insertString: " + source[i]);
        }
      }
      super.insertString(offs, new String(result, 0, j), a);
    }
  }
}
