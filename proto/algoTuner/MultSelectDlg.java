package proto.algoTuner;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/**
 * Title:        Algorithm Tuner
 * Description:
 * Copyright:    Copyright (c) 1999
 * Company:      Agilent Technologies
 * @author Andy Mechtenberg
 * @version
 */

public class MultSelectDlg extends JDialog
{
  AlgoTunerFrame _frame;

  JPanel panel1 = new JPanel();
  GridLayout gridLayout1 = new GridLayout();
  JCheckBox jCheckBox1 = new JCheckBox();
  JCheckBox jCheckBox2 = new JCheckBox();
  JCheckBox jCheckBox3 = new JCheckBox();
  JCheckBox jCheckBox4 = new JCheckBox();
  JCheckBox jCheckBox5 = new JCheckBox();
  JCheckBox jCheckBox6 = new JCheckBox();
  JCheckBox jCheckBox7 = new JCheckBox();
  JCheckBox jCheckBox8 = new JCheckBox();
  JPanel jPanel1 = new JPanel();
  JButton jButton1 = new JButton();

  public MultSelectDlg(AlgoTunerFrame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    _frame = frame;
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public MultSelectDlg()
  {
    this(null, "", false);
  }
  void jbInit() throws Exception
  {
    panel1.setLayout(gridLayout1);
    jCheckBox1.setText("Heel");
    jCheckBox2.setText("jCheckBox2");
    jCheckBox3.setText("Smear");
    jCheckBox4.setText("jCheckBox4");
    jCheckBox5.setText("Pad");
    jCheckBox6.setText("jCheckBox6");
    jCheckBox7.setText("Toe");
    jCheckBox8.setText("Center");
    //gridLayout1.setRows(4);
    gridLayout1.setRows(3);
    gridLayout1.setColumns(2);
    jButton1.setPreferredSize(new Dimension(64, 20));
    jButton1.setText("Done");
    jButton1.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
	jButton1_actionPerformed(e);
      }
    });
    getContentPane().add(panel1);
    panel1.add(jCheckBox1, null);
    panel1.add(jCheckBox8, null);
    panel1.add(jCheckBox7, null);
    //panel1.add(jCheckBox6, null);
    panel1.add(jCheckBox5, null);
    //panel1.add(jCheckBox4, null);
    panel1.add(jCheckBox3, null);
    this.getContentPane().add(jPanel1, BorderLayout.SOUTH);
    jPanel1.add(jButton1, null);
    //panel1.add(jCheckBox2, null);
  }

  void jButton1_actionPerformed(ActionEvent e)
  {
    dispose();
  }
}
