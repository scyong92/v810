
/**
 * Title:        Algorithm Tuner<p>
 * Description:  <p>
 * Copyright:    Copyright (c) 1999<p>
 * Company:      Agilent Technologies<p>
 * @author Andy Mechtenberg
 * @version
 */
package proto.algoTuner;

import java.awt.*;
import javax.swing.*;

public class ConfigurationSettingsDlg extends JDialog
{
  JPanel panel1 = new JPanel();
  BorderLayout borderLayout1 = new BorderLayout();
  JLabel jLabel1 = new JLabel();

  public ConfigurationSettingsDlg(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public ConfigurationSettingsDlg()
  {
    this(null, "", false);
  }

  void jbInit() throws Exception
  {
    panel1.setLayout(borderLayout1);
    jLabel1.setFont(new java.awt.Font("Dialog", 0, 24));
    jLabel1.setText("Setup your configuration here!");
    getContentPane().add(panel1);
    panel1.add(jLabel1, BorderLayout.CENTER);
  }
}
