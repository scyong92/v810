
/**
 * Title:        Algorithm Tuner<p>
 * Description:  <p>
 * Copyright:    Copyright (c) 1999<p>
 * Company:      Agilent Technologies<p>
 * @author Andy Mechtenberg
 * @version
 */
package proto.algoTuner;

import java.util.*;

public class FamilyAlgoPairing implements Cloneable
{
  private Map _familyAlgos;

  public FamilyAlgoPairing()
  {
    _familyAlgos = new HashMap();
  }

  protected Object clone()
  {
    FamilyAlgoPairing fap = new FamilyAlgoPairing();
    Iterator it = _familyAlgos.entrySet().iterator();
    while (it.hasNext())
    {
      Map.Entry e = (Map.Entry)it.next();
      Set algset = (Set)e.getValue();

      for (Iterator valiter = algset.iterator(); valiter.hasNext();)
      {
         //allSettings.add(e.getKey());
         String algo = (String)valiter.next();
         //allSettings.add(algosetting.getAlgorithm());
       }
       //fap._familyAlgos.put(a,b)   // fill in
    }
    return fap;
  }

  void addSetting(String family, String algo)
   {
    Set algset = (Set)_familyAlgos.get(family);
    if (algset != null)
    {
      algset.add(algo);
    }
    else
    {
      Set s = new HashSet();
      s.add(algo);
      _familyAlgos.put(family, s);
    }
   }

   void removeSetting(String family, String algo)
   {
     Set algoset = (Set)_familyAlgos.get(family);
     algoset.remove(algo);

     // if the set is now empty, remove the key from the map as well
     if (algoset.size() == 0)
       _familyAlgos.remove(family);
   }

   // remove all algos for the specified family
   void removeSetting(String family)
   {
     _familyAlgos.remove(family);
   }

   void removeAllSettings()
   {
     _familyAlgos = null;
     _familyAlgos = new HashMap();
   }

   boolean anyPairSettings()
   {
     if (_familyAlgos.size() == 0)
       return false;
     else
       return true;
   }

   List getSettings()
   {
     List allSettings = new LinkedList();

     for (Iterator it = _familyAlgos.entrySet().iterator(); it.hasNext();)
     {
       Map.Entry e = (Map.Entry)it.next();
       Set algset = (Set)e.getValue();
       for (Iterator valiter = algset.iterator(); valiter.hasNext();)
       {
         allSettings.add(e.getKey());
         allSettings.add(valiter.next());
       }
//       System.out.println(e.getKey() + ": " + e.getValue());

     }
     return allSettings;
   }

   List getSettings(String family)
   {
     List familySettings = new LinkedList();
     // build a list of all algos for the passed in family

     Set algoset = (Set)_familyAlgos.get(family);
     if (algoset == null)
        return null;
     for (Iterator it = algoset.iterator(); it.hasNext();)
     {
       familySettings.add(family);
       familySettings.add(it.next());
     }
     return familySettings;
   }
}
