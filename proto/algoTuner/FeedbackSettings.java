
/**
 * Title:        Algorithm Tuner<p>
 * Description:  <p>
 * Copyright:    Copyright (c) 1999<p>
 * Company:      Agilent Technologies<p>
 * @author Andy Mechtenberg
 * @version
 */
package proto.algoTuner;

public class FeedbackSettings
{
   private boolean _markRejectedJoints = true;
   private boolean _markMarginalJoints = true;
   private boolean _markAcceptedJoints = true;
   private boolean _drawMeas = true;
   private boolean _drawBasic = true;
   private boolean _drawAdvanced = true;
   private boolean _diagnosticText = true;
   private boolean _pauseRejectedJoints = true;
   private boolean _pauseMarginalJoints = true;
   private boolean _pauseAcceptedJoints = true;

   public FeedbackSettings()
   {
   }

   void setMarkRejectedJoints(boolean val)
   {
     _markRejectedJoints = val;
   }
   void setMarkMarginalJoints(boolean val)
   {
     _markMarginalJoints = val;
   }
   void setMarkAcceptedJoints(boolean val)
   {
     _markAcceptedJoints = val;
   }
   void setDrawMeas(boolean val)
   {
     _drawMeas = val;
   }
   void setDrawBasic(boolean val)
   {
     _drawBasic = val;
   }
   void setDrawAdvanced(boolean val)
   {
     _drawAdvanced = val;
   }
   void setDiagnosticText(boolean val)
   {
     _diagnosticText = val;
   }
   void setPauseRejectedJoints(boolean val)
   {
     _pauseRejectedJoints = val;
   }
   void setPauseMarginalJoints(boolean val)
   {
     _pauseMarginalJoints = val;
   }
   void setPauseAcceptedJoints(boolean val)
   {
     _pauseAcceptedJoints = val;
   }

   // All the Gets go here
   boolean getMarkRejectedJoints()
   {
     return _markRejectedJoints;
   }
   boolean getMarkMarginalJoints()
   {
     return _markMarginalJoints;
   }
   boolean getMarkAcceptedJoints()
   {
     return _markAcceptedJoints;
   }
   boolean getDrawMeas()
   {
     return _drawMeas;
   }
   boolean getDrawBasic()
   {
     return _drawBasic;
   }
   boolean getDrawAdvanced()
   {
     return _drawAdvanced;
   }
   boolean getDiagnosticText()
   {
     return _diagnosticText;
   }
   boolean getPauseRejectedJoints()
   {
     return _pauseRejectedJoints;
   }
   boolean getPauseMarginalJoints()
   {
     return _pauseMarginalJoints;
   }
   boolean getPauseAcceptedJoints()
   {
     return _pauseAcceptedJoints;
   }

}
