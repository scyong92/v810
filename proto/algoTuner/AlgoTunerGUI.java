package proto.algoTuner;

import javax.swing.UIManager;
import java.awt.*;

public class AlgoTunerGUI
{
  boolean packFrame = true;

  //Construct the application
  public AlgoTunerGUI()
  {
    AlgoTunerFrame frame = new AlgoTunerFrame();
    //Validate frames that have preset sizes
    //Pack frames that have useful preferred size info, e.g. from their layout
    if (packFrame)
      frame.pack();
    else
      frame.validate();
    frame.setCustomizations();
    frame.pack();
    //Center the window
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = frame.getSize();
    if (frameSize.height > screenSize.height)
      frameSize.height = screenSize.height;
    if (frameSize.width > screenSize.width)
      frameSize.width = screenSize.width;
    frame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
    frame.setVisible(true);
    frame.setDividerLocation();
  }

  //Main method
  public static void main(String[] args)
  {
    try
    {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch(Exception e)
    {
    }
    AgilentSplashScreen splash = new AgilentSplashScreen("Algorithm Tuner");
    splash.started();
    new AlgoTunerGUI();
    splash.finished();
  }
}
