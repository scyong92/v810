
/**
 * Title:        Algorithm Tuner<p>
 * Description:  <p>
 * Copyright:    Copyright (c) 1999<p>
 * Company:      Agilent Technologies<p>
 * @author Andy Mechtenberg
 * @version
 */
package proto.algoTuner;

public class Algorithms
{
 private static String[] _bga2Algos = {"Locator", "SPC", "Excess", "Insufficient", "Misalignment", "Open", "Shape", "Short", "Voiding"};
  private static String[] _capAlgos = {"Locator", "SPC", "Excess", "Insufficient", "Misalignment", "Open", "Short"};
  private static String[] _fpgullwingAlgos = {"Locator", "SPC", "Excess", "Insufficient", "Misalignment", "Open", "Shape", "Short"};
  private static String[] _jleadAlgos = {"Locator", "SPC", "Excess", "Insufficient", "Misalignment", "Open", "Short"};
  private static String[] _resAlgos = {"Locator", "SPC", "Excess", "Insufficient", "Misalignment", "Open", "Short"};

  private static String[] _all = {"All"};

  public Algorithms()
  {
  }
  static String[] getAlgorithmsForFamily(String family)
  {
    if (family.equalsIgnoreCase("BGA2"))
      return _bga2Algos;
    if (family.equalsIgnoreCase("CAP"))
      return _capAlgos;
    if (family.equalsIgnoreCase("FPGULLWING"))
      return _fpgullwingAlgos;
    if (family.equalsIgnoreCase("JLEAD"))
      return _jleadAlgos;
    if (family.equalsIgnoreCase("RES"))
      return _resAlgos;

    // family must be all
    return _all;
  }
}
