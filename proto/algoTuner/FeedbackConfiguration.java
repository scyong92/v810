
/**
 * Title:        Algorithm Tuner<p>
 * Description:  <p>
 * Copyright:    Copyright (c) 1999<p>
 * Company:      Agilent Technologies<p>
 * @author Andy Mechtenberg
 * @version
 */
package proto.algoTuner;

import java.util.*;

public class FeedbackConfiguration implements Cloneable
{
  private Map _familyAlgos = null;

  public FeedbackConfiguration()
  {
    _familyAlgos = new TreeMap();
  }

  public FeedbackConfiguration(Map m)
  {
    if (_familyAlgos != null)
      _familyAlgos.clear();
    _familyAlgos = null;
    _familyAlgos = new TreeMap();
//    _familyAlgos = m;
  }

  protected Object clone()
  {
    FeedbackConfiguration fbc = new FeedbackConfiguration();
    Iterator it = _familyAlgos.entrySet().iterator();
    while (it.hasNext())
    {
      Map.Entry e = (Map.Entry)it.next();
      Set algset = (Set)e.getValue();

      for (Iterator valiter = algset.iterator(); valiter.hasNext();)
      {
         //allSettings.add(e.getKey());
         AlgoSettingPair algosetting = (AlgoSettingPair)valiter.next();
         //allSettings.add(algosetting.getAlgorithm());
       }
       //fbc._familyAlgos.put(a,b)   // fill in
    }
    return fbc;
  }

  // new
  void addSetting(String family, String algo, FeedbackSettings settings)
  {
    Set algoset = (Set)_familyAlgos.get(family);
    if (algoset != null)  // the family already has an entry, so delve deeper for the algo
    {
      // iterate through the set to see if the algo passed in is currently present
      boolean found = false;
      for (Iterator it = algoset.iterator(); !found && it.hasNext();)
      {
        AlgoSettingPair algosetting = (AlgoSettingPair)it.next();
        if (algosetting.getAlgorithm().equals(algo))
        {
          algosetting.replaceSettings(settings);
          found = true;
        }
      }
      if (!found)
      {
        // add the new algo/setting pair to the family set
        AlgoSettingPair asp = new AlgoSettingPair(algo, settings);  // the algo/setting pair
        algoset.add(asp);
      }
    }
    else  // the family is new, so create a new top level set and add one map to that set
    {
      Set s = new TreeSet(new AlgoSettingPair());  // container for the algo/setting map.  The passed param is the comparator
      AlgoSettingPair asp = new AlgoSettingPair(algo, settings);  // the algo/setting pair
      s.add(asp);               // add pairing to the set

      _familyAlgos.put(family, s);  // add the set to overall map
    }
  }

  // if the family algo pair is in the stucture, return the feedbackSettings class for it.
  // otherwise return null
  FeedbackSettings getFeedbackSettings(String family, String algo)
  {
    Set algoset = (Set)_familyAlgos.get(family);
    if (algoset != null)  // the family already has an entry, so delve deeper for the algo
    {
      // iterate through the set to see if the algo passed in is currently present
      boolean found = false;
      for (Iterator it = algoset.iterator(); !found && it.hasNext();)
      {
        AlgoSettingPair algosetting = (AlgoSettingPair)it.next();
        if (algosetting.getAlgorithm().equals(algo))
        {
          found = true;
          return algosetting.getSettings();
        }
      }
    }
    return null;
  }

  void removeSetting(String family, String algo)
  {
    Set algoset = (Set)_familyAlgos.get(family);
    if (algoset != null)  // the family exists
    {
      // iterate through the set to find the algo
      boolean found = false;
      for (Iterator it = algoset.iterator(); !found && it.hasNext();)
      {
        AlgoSettingPair algosetting = (AlgoSettingPair)it.next();
        if (algosetting.getAlgorithm().equals(algo))
        {
          algoset.remove(algosetting);
          found = true;
        }
      }
    }
    // if the set is now empty, remove the key from the map as well
    if (algoset.size() == 0)
      _familyAlgos.remove(family);
  }

   // remove all algos for the specified family
   void removeSetting(String family)
   {
     _familyAlgos.remove(family);
   }

   void removeAllSettings()
   {
     _familyAlgos = null;
     _familyAlgos = new TreeMap();
   }

   boolean anyFeebackSettings()
   {
     if (_familyAlgos.size() == 0)
       return false;
     else
       return true;
   }

   List getSettings()  // this will just return string pair lists of family/algo.  Not the settings data
   {
     List allSettings = new LinkedList();

     for (Iterator it = _familyAlgos.entrySet().iterator(); it.hasNext();)
     {
       Map.Entry e = (Map.Entry)it.next();
       Set algset = (Set)e.getValue();

       for (Iterator valiter = algset.iterator(); valiter.hasNext();)
       {
         allSettings.add(e.getKey());
         AlgoSettingPair algosetting = (AlgoSettingPair)valiter.next();
         allSettings.add(algosetting.getAlgorithm());
       }
     }
     return allSettings;
   }

   List getSettings(String family)
   {
     List familySettings = new LinkedList();
     // build a list of all algos for the passed in family

     Set algoset = (Set)_familyAlgos.get(family);
     if (algoset == null)
        return null;
     for (Iterator it = algoset.iterator(); it.hasNext();)
     {
       familySettings.add(family);
       familySettings.add(it.next());
     }
     return familySettings;
   }
}
