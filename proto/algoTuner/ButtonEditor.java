package proto.algoTuner;

/**
 * Title:        Algorithm Tuner
 * Description:
 * Copyright:    Copyright (c) 1999
 * Company:      Agilent Technologies
 * @author Andy Mechtenberg
 * @version
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;

public class ButtonEditor extends DefaultCellEditor {
  protected JButton button;
  private String    label;
  private boolean   isPushed;
  private AlgoTunerFrame _frame;
  private Point _editLocationPoint;

  public ButtonEditor(JCheckBox checkBox, AlgoTunerFrame frame) {
    super(checkBox);
    _frame = frame;
    button = new JButton();
    button.setOpaque(true);
    button.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
	fireEditingStopped();
      }
    });
  }

  public Component getTableCellEditorComponent(JTable table, Object value,
		   boolean isSelected, int row, int column) {
    if (isSelected) {
      button.setForeground(table.getSelectionForeground());
      button.setBackground(table.getSelectionBackground());
    } else{
      button.setForeground(table.getForeground());
      button.setBackground(table.getBackground());
    }
    label = (value ==null) ? "" : value.toString();
    button.setText( label );
    isPushed = true;
    Point tp = table.getLocationOnScreen();
    Point cp = table.getCellRect(row, column, true).getLocation();
//    System.out.println("Table location is " + tp.x + " " + tp.y);
//    System.out.println("Cell location is " + cp.x + " " + cp.y);
    _editLocationPoint = new Point(tp.x + cp.x, tp.y + cp.y);
    return button;
  }

  public Object getCellEditorValue() {
    if (isPushed)
    {
      MultSelectDlg dlg = new MultSelectDlg(_frame, "", true);
      dlg.setLocation(_editLocationPoint);
      System.out.println("Before show");
      dlg.setResizable(false);

      dlg.show();
      System.out.println("After show");
/*
      MultSelectWnd wnd = new MultSelectWnd(_frame);
      wnd.setLocation(_editLocationPoint);
      System.out.println("Before show");
      if (wnd.isShowing())
	 System.out.println("Window is showing");
      wnd.showwnd();
      if (wnd.isShowing())
	 System.out.println("Window is showing");
      if (wnd.isVisible())
	 System.out.println("Window is visible");
      if (wnd.isValid())
	 System.out.println("Window is valid");

      System.out.println("After show");
*/
    }
    isPushed = false;
    return new String( "(Partial)" ) ;
  }

  public boolean stopCellEditing()
  {
    System.out.println("stopCellEditing");
    isPushed = false;
    return super.stopCellEditing();
  }

  protected void fireEditingStopped()
  {
    System.out.println("Fire!");
    super.fireEditingStopped();
  }
}
