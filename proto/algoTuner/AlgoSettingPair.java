
/**
 * Title:        Algorithm Tuner<p>
 * Description:  <p>
 * Copyright:    Copyright (c) 1999<p>
 * Company:      Agilent Technologies<p>
 * @author Andy Mechtenberg
 * @version
 */
package proto.algoTuner;

import java.util.*;

public class AlgoSettingPair implements Comparator
{
  private String _algorithm;
  private FeedbackSettings _settings;

  public AlgoSettingPair(String alg, FeedbackSettings setting)
  {
    _algorithm = alg;
    _settings = setting;
  }

  public AlgoSettingPair()
  {
    this("", null);
  }

  String getAlgorithm()
  {
    return _algorithm;
  }

  FeedbackSettings getSettings()
  {
    return _settings;
  }

  void replaceSettings(FeedbackSettings newSettings)
  {
    _settings = newSettings;
  }

  public int compare(Object o1, Object o2)
  {
    AlgoSettingPair asp1 = (AlgoSettingPair)o1;
    AlgoSettingPair asp2 = (AlgoSettingPair)o2;
    String s1 = asp1.getAlgorithm();
    String s2 = asp2.getAlgorithm();
    int len1 = s1.length();
    int len2 = s2.length();
    for (int i=0, n=Math.min(len1, len2); i<n; i++)
    {
      char c1 = s1.charAt(i);
      char c2 = s2.charAt(i);
      if (c1 != c2)
        return c1 - c2;
    }
    return len1 - len2;
  }

  public boolean equals(Object obj)
  {
    return obj.equals(this);
  }
}
