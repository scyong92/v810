
/**
 * Title:        Algorithm Tuner<p>
 * Description:  <p>
 * Copyright:    Copyright (c) 1999<p>
 * Company:      Agilent Technologies<p>
 * @author Andy Mechtenberg
 * @version
 */
package proto.algoTuner;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.border.*;

public class CopyThresholdsDlg extends JDialog
{
  JPanel panel1 = new JPanel();
  BorderLayout borderLayout1 = new BorderLayout();
  JPanel jPanel1 = new JPanel();
  Border border1;
  JList jList1;
  Border border2;
  private DefaultListModel _subtypeListModel;
  Border border3;
  TitledBorder titledBorder1;
  Border border4;
  FlowLayout flowLayout1 = new FlowLayout();
  JPanel jPanel15 = new JPanel();
  JPanel jPanel8 = new JPanel();
  JPanel jPanel7 = new JPanel();
  JPanel jPanel3 = new JPanel();
  JButton _allSubtypesButton = new JButton();
  FlowLayout flowLayout4 = new FlowLayout();
  JScrollPane jScrollPane1 = new JScrollPane(jList1);
  BorderLayout borderLayout3 = new BorderLayout();
  BorderLayout borderLayout2 = new BorderLayout();
  JPanel jPanel9 = new JPanel();
  JPanel jPanel6 = new JPanel();
  JPanel jPanel5 = new JPanel();
  JPanel jPanel4 = new JPanel();
  FlowLayout flowLayout6 = new FlowLayout();
  JPanel jPanel14 = new JPanel();
  JPanel jPanel12 = new JPanel();
  FlowLayout flowLayout3 = new FlowLayout();
  JPanel jPanel11 = new JPanel();
  FlowLayout flowLayout2 = new FlowLayout();
  JPanel jPanel10 = new JPanel();
  GridLayout gridLayout1 = new GridLayout();
  JRadioButton jRadioButton3 = new JRadioButton();
  JRadioButton jRadioButton2 = new JRadioButton();
  JLabel jLabel3 = new JLabel();
  BorderLayout borderLayout6 = new BorderLayout();
  JLabel jLabel2 = new JLabel();
  BorderLayout borderLayout5 = new BorderLayout();
  JRadioButton jRadioButton1 = new JRadioButton();
  JLabel jLabel1 = new JLabel();
  BorderLayout borderLayout4 = new BorderLayout();
  BorderLayout borderLayout8 = new BorderLayout();
  BorderLayout borderLayout9 = new BorderLayout();
  JPanel jPanel16 = new JPanel();
  JLabel jLabel4 = new JLabel();
  FlowLayout flowLayout5 = new FlowLayout();
  JPanel jPanel17 = new JPanel();
  FlowLayout flowLayout7 = new FlowLayout();
  JPanel jPanel2 = new JPanel();
  JButton _okButton = new JButton();
  JButton _cancelButton = new JButton();
  BorderLayout borderLayout7 = new BorderLayout();

  public CopyThresholdsDlg(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public CopyThresholdsDlg()
  {
    this(null, "", false);
  }

  void jbInit() throws Exception
  {
    border1 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134)),"Thresholds to Copy: "),BorderFactory.createEmptyBorder(0,5,0,5));
    border2 = BorderFactory.createEmptyBorder(0,10,0,10);
    _subtypeListModel = new DefaultListModel();
    jList1 = new JList(_subtypeListModel);
    border3 = BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134));
    titledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134)),"To Subtype: ");
    border4 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134)),"To Subtype: ");
    jScrollPane1 = new JScrollPane(jList1);
    jList1.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

    _subtypeListModel.addElement("1");
    _subtypeListModel.addElement("3");
    _subtypeListModel.addElement("6");
    _subtypeListModel.addElement("7");
    _subtypeListModel.addElement("12");
    _subtypeListModel.addElement("13");
    _subtypeListModel.addElement("20");
    _subtypeListModel.addElement("23");


    panel1.setLayout(borderLayout1);

    jPanel1.setLayout(flowLayout1);
    borderLayout1.setHgap(20);
    jPanel8.setLayout(borderLayout3);
    jPanel8.setBorder(border2);
    jPanel7.setLayout(flowLayout4);
    jPanel3.setLayout(borderLayout2);
    jPanel3.setBorder(border4);
    jPanel3.setPreferredSize(new Dimension(200, 200));
    _allSubtypesButton.setPreferredSize(new Dimension(100, 20));
    _allSubtypesButton.setText("All");
    _allSubtypesButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
	allSubtypesButton_actionPerformed(e);
      }
    });
    jScrollPane1.setPreferredSize(new Dimension(130, 100));
    borderLayout3.setHgap(5);
    borderLayout3.setVgap(5);
    jPanel9.setLayout(borderLayout4);
    jPanel6.setLayout(borderLayout5);
    jPanel5.setLayout(gridLayout1);
    jPanel5.setBorder(border1);
    jPanel5.setPreferredSize(new Dimension(200, 200));
    jPanel4.setLayout(borderLayout6);
    flowLayout6.setAlignment(FlowLayout.LEFT);
    flowLayout6.setHgap(25);
    jPanel12.setLayout(flowLayout6);
    flowLayout3.setAlignment(FlowLayout.LEFT);
    flowLayout3.setHgap(25);
    jPanel11.setLayout(flowLayout3);
    flowLayout2.setAlignment(FlowLayout.LEFT);
    flowLayout2.setHgap(25);
    jPanel10.setLayout(flowLayout2);
    gridLayout1.setRows(3);
    gridLayout1.setVgap(10);
    jRadioButton3.setText("Entire Algorithm:");
    jRadioButton3.setMnemonic('A');
    jRadioButton2.setText("Entire Family:");
    jRadioButton2.setMnemonic('F');
    jLabel3.setForeground(Color.blue);
    jLabel3.setText("FPGullwing");
    jLabel2.setForeground(Color.blue);
    jLabel2.setText("Locator");
    jRadioButton1.setText("Single Threshold:");
    jRadioButton1.setMnemonic('S');
    jRadioButton1.setSelected(true);
    jLabel1.setForeground(Color.blue);
    jLabel1.setText("Heel Search Technique");
    jPanel14.setLayout(borderLayout8);
    jPanel15.setLayout(borderLayout9);
    jLabel4.setText("Current Subtype:  17");
    jPanel16.setLayout(flowLayout5);
    flowLayout5.setAlignment(FlowLayout.LEFT);
    flowLayout5.setHgap(0);
    jPanel17.setLayout(flowLayout7);
    flowLayout7.setAlignment(FlowLayout.RIGHT);
    flowLayout7.setHgap(0);
    jPanel16.setPreferredSize(new Dimension(121, 37));
    borderLayout8.setVgap(5);
    borderLayout9.setVgap(5);
    _okButton.setPreferredSize(new Dimension(73, 27));
    _okButton.setText("Ok");
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	okButton_actionPerformed(e);
      }
    });
    _cancelButton.setText("Cancel");
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	cancelButton_actionPerformed(e);
      }
    });
    jPanel17.setPreferredSize(new Dimension(161, 37));
    jPanel2.setLayout(borderLayout7);
    borderLayout7.setHgap(10);
    flowLayout1.setHgap(20);
    jPanel4.setBorder(BorderFactory.createLineBorder(Color.black));
    jPanel6.setBorder(BorderFactory.createLineBorder(Color.black));
    jPanel9.setBorder(BorderFactory.createLineBorder(Color.black));
    getContentPane().add(panel1);
    panel1.add(jPanel1, BorderLayout.CENTER);
    jPanel1.add(jPanel14, null);
    jPanel14.add(jPanel5, BorderLayout.CENTER);
    jPanel5.add(jPanel9, null);
    jPanel9.add(jRadioButton1, BorderLayout.CENTER);
    jPanel9.add(jPanel10, BorderLayout.SOUTH);
    jPanel10.add(jLabel1, null);
    jPanel5.add(jPanel6, null);
    jPanel6.add(jRadioButton3, BorderLayout.CENTER);
    jPanel6.add(jPanel11, BorderLayout.SOUTH);
    jPanel11.add(jLabel2, null);
    jPanel5.add(jPanel4, null);
    jPanel4.add(jPanel12, BorderLayout.SOUTH);
    jPanel12.add(jLabel3, null);
    jPanel4.add(jRadioButton2, BorderLayout.CENTER);
    jPanel14.add(jPanel16, BorderLayout.SOUTH);
    jPanel16.add(jLabel4, null);
    jPanel1.add(jPanel15, null);
    jPanel15.add(jPanel3, BorderLayout.CENTER);
    jPanel3.add(jPanel8, BorderLayout.CENTER);
    jPanel8.add(jScrollPane1, BorderLayout.CENTER);
    jPanel3.add(jPanel7, BorderLayout.SOUTH);
    jPanel7.add(_allSubtypesButton, null);
    jPanel15.add(jPanel17, BorderLayout.SOUTH);
    jPanel17.add(jPanel2, null);
    jPanel2.add(_okButton, BorderLayout.WEST);
    jPanel2.add(_cancelButton, BorderLayout.EAST);

    ButtonGroup bg = new ButtonGroup();
    bg.add(jRadioButton1);
    bg.add(jRadioButton2);
    bg.add(jRadioButton3);

  }

  void okButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  void cancelButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  void allSubtypesButton_actionPerformed(ActionEvent e)
  {
    int allValues[];
    allValues = new int[_subtypeListModel.getSize()];
    for (int i = 0; i < _subtypeListModel.getSize(); i++)
      allValues[i] = i;
    jList1.setSelectedIndices(allValues);
  }
  void _okButton_actionPerformed(ActionEvent e)
  {

  }
  void _cancelButton_actionPerformed(ActionEvent e)
  {

  }
  void _allSubtypesButton_actionPerformed(ActionEvent e)
  {

  }

}
