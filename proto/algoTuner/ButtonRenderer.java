package proto.algoTuner;

/**
 * Title:        Algorithm Tuner
 * Description:
 * Copyright:    Copyright (c) 1999
 * Company:      Agilent Technologies
 * @author Andy Mechtenberg
 * @version
 */

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

class MultiSelectPanel extends JPanel
{
  JLabel label = new JLabel("(All)");
  JButton button;
  MultiSelectPanel()
  {
    setLayout(new BorderLayout());
    label.setBackground(Color.white);
    label.setOpaque(true);
    add(label, BorderLayout.CENTER);
    button = new JButton("...");
    //add(button, BorderLayout.EAST);
    button.setPreferredSize(new Dimension(15,15));
  }
  public void setText(String text)
  {
    label.setText(text);
  }
  public void addButton()
  {
    add(button, BorderLayout.EAST);
  }
  public void removeButton()
  {
    remove(button);
  }
}

public class ButtonRenderer extends MultiSelectPanel implements TableCellRenderer {

  public ButtonRenderer() {
    setOpaque(true);
  }

  public Component getTableCellRendererComponent(JTable table, Object value,
       boolean isSelected, boolean hasFocus, int row, int column)
  {
    System.out.println("Button Renderer is selected: " + isSelected);
    if (isSelected)
      addButton();
    else
      removeButton();

    if (isSelected) {
      setForeground(table.getSelectionForeground());
      setBackground(table.getSelectionBackground());
    } else{
      setForeground(table.getForeground());
      setBackground(UIManager.getColor("Button.background"));
    }
    setText( (value ==null) ? "" : value.toString() );
    return this;
  }
}
