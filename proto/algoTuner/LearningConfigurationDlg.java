
/**
 * Title:        Algorithm Tuner<p>
 * Description:  <p>
 * Copyright:    Copyright (c) 1999<p>
 * Company:      Agilent Technologies<p>
 * @author Andy Mechtenberg
 * @version
 */
package proto.algoTuner;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class LearningConfigurationDlg extends JDialog
{
  JPanel panel1 = new JPanel();
  BorderLayout borderLayout1 = new BorderLayout();
  JButton _writeLearnedDataBuitton = new JButton();
  BorderLayout borderLayout27 = new BorderLayout();
  JPanel jPanel15 = new JPanel();
  JPanel jPanel14 = new JPanel();
  JCheckBox _subtypeLearningCheckBox = new JCheckBox();
  JCheckBox _jointLearningCheckBox = new JCheckBox();
  BorderLayout borderLayout13 = new BorderLayout();
  Box box2;
  JPanel jPanel6 = new JPanel();
  GridLayout gridLayout6 = new GridLayout();
  JButton _deleteLearnedDataButton = new JButton();
  JPanel jPanel20 = new JPanel();
  BorderLayout borderLayout28 = new BorderLayout();

  public LearningConfigurationDlg(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public LearningConfigurationDlg()
  {
    this(null, "", false);
  }

  void jbInit() throws Exception
  {
    box2 = Box.createVerticalBox();
    panel1.setLayout(borderLayout1);
    _writeLearnedDataBuitton.setText("Write Learned Data");
    jPanel15.setLayout(borderLayout27);
    jPanel14.setMinimumSize(new Dimension(0, 0));
    jPanel14.setLayout(gridLayout6);
    _subtypeLearningCheckBox.setText("Subtype Learning");
    _subtypeLearningCheckBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        subtypeLearningCheckBox_actionPerformed(e);
      }
    });
    _jointLearningCheckBox.setText("Joint Learning");
    _jointLearningCheckBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        jointLearningCheckBox_actionPerformed(e);
      }
    });
    jPanel6.setMinimumSize(new Dimension(0, 0));
    jPanel6.setLayout(borderLayout28);
    gridLayout6.setRows(2);
    gridLayout6.setVgap(5);
    _deleteLearnedDataButton.setText("Delete Learned Data");
    jPanel20.setLayout(borderLayout13);
    gridLayout6.setRows(2);
    gridLayout6.setVgap(5);
    _deleteLearnedDataButton.setPreferredSize(new Dimension(150, 27));
    _deleteLearnedDataButton.setText("Delete Learned Data");
    _writeLearnedDataBuitton.setPreferredSize(new Dimension(150, 27));
    _writeLearnedDataBuitton.setText("Write Learned Data");
    _jointLearningCheckBox.setText("Joint Learning");
    _subtypeLearningCheckBox.setText("Subtype Learning");
    getContentPane().add(panel1);
    panel1.add(jPanel14, BorderLayout.SOUTH);
    jPanel14.add(jPanel6, null);
    jPanel6.add(_deleteLearnedDataButton, BorderLayout.WEST);
    jPanel14.add(jPanel15, null);
    jPanel15.add(_writeLearnedDataBuitton, BorderLayout.WEST);
    panel1.add(jPanel20, BorderLayout.NORTH);
    jPanel20.add(box2, BorderLayout.CENTER);
    box2.add(_jointLearningCheckBox, null);
    box2.add(_subtypeLearningCheckBox, null);
  }

 void jointLearningCheckBox_actionPerformed(ActionEvent e)
  {
  }

  void subtypeLearningCheckBox_actionPerformed(ActionEvent e)
  {
 }
}
