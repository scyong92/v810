package proto.algoTuner;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import javax.swing.border.*;

/**
 * Title:        Algorithm Tuner
 * Description:
 * Copyright:    Copyright (c) 1999
 * Company:      Agilent Technologies
 * @author Andy Mechtenberg
 * @version
 */

public class ThresholdDescDlg extends JDialog
{
  boolean detailedInfo = false;
  JPanel panel1 = new JPanel();
  BorderLayout borderLayout = new BorderLayout();
  BorderLayout borderLayout1 = new BorderLayout();
  JPanel jPanel19 = new JPanel();
  JPanel detailPane = new JPanel();
  JLabel label = new JLabel();
  JButton _moreInfoButton = new JButton();
  JPanel jPanel20 = new JPanel();
  JPanel jPanel46 = new JPanel();
  JPanel jPanel45 = new JPanel();
  BorderLayout borderLayout5 = new BorderLayout();
  BorderLayout borderLayout25 = new BorderLayout();
  BorderLayout borderLayout24 = new BorderLayout();
  JButton _okButton = new JButton();
  BorderLayout borderLayout2 = new BorderLayout();
  Border border1;
  Border border2;
  JPanel jPanel1 = new JPanel();
  JLabel jLabel12 = new JLabel();
  JPanel jPanel15 = new JPanel();
  FlowLayout flowLayout5 = new FlowLayout();
  BorderLayout borderLayout3 = new BorderLayout();
  JEditorPane _threshDescEditorPane = new JEditorPane();
  JScrollPane jScrollPane5 = new JScrollPane();
  JPanel jPanel44 = new JPanel();
  BorderLayout borderLayout23 = new BorderLayout();
  AlgoTunerFrame _frame;

  public ThresholdDescDlg(AlgoTunerFrame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    _frame = frame;
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public ThresholdDescDlg()
  {
    this(null, "", false);
  }
  void jbInit() throws Exception
  {
    border1 = BorderFactory.createEmptyBorder(10,10,10,10);
    border2 = BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.gray,Color.black,new Color(134, 134, 134),new Color(93, 93, 93));
    panel1.setLayout(borderLayout1);
    jPanel19.setLayout(borderLayout25);
    jPanel20.setLayout(borderLayout5);
    jPanel45.setLayout(borderLayout24);
    borderLayout24.setHgap(5);
    _moreInfoButton.setText("Advanced Info ...");
    _moreInfoButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
	moreInfoButton_actionPerformed(e);
      }
    });
    panel1.setPreferredSize(new Dimension(547, 240));
    _okButton.setText("Ok");
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
	okButton_actionPerformed(e);
      }
    });
    jPanel46.setLayout(borderLayout2);
    jPanel46.setBorder(border1);
    jLabel12.setIcon(null);
    jPanel15.setLayout(flowLayout5);
    jPanel15.setBackground(SystemColor.info);
    flowLayout5.setAlignment(FlowLayout.RIGHT);
    jPanel1.setLayout(borderLayout3);
    _threshDescEditorPane.setContentType("text/html");
    _threshDescEditorPane.setBackground(SystemColor.info);
    _threshDescEditorPane.setPage(AlgoTunerFrame.class.getResource("nothresh.html"));
    _threshDescEditorPane.setEditable(false);
    jScrollPane5.setBorder(null);
    jScrollPane5.setToolTipText("");
    jPanel44.setLayout(borderLayout23);
    jPanel1.setBorder(border2);
    label.setHorizontalAlignment(SwingConstants.CENTER);
    getContentPane().add(panel1);
    panel1.add(jPanel20, BorderLayout.CENTER);
    jPanel20.add(jPanel19, BorderLayout.SOUTH);
    jPanel19.add(jPanel45, BorderLayout.SOUTH);
    jPanel19.add(jPanel46, BorderLayout.NORTH);
    jPanel46.add(_okButton, BorderLayout.EAST);
    jPanel46.add(_moreInfoButton, BorderLayout.WEST);
    jPanel20.add(jPanel1, BorderLayout.CENTER);
    jPanel1.add(jPanel15, BorderLayout.EAST);
    jPanel15.add(jLabel12, null);
    jPanel1.add(jPanel44, BorderLayout.CENTER);
    jPanel44.add(jScrollPane5, BorderLayout.CENTER);
    jScrollPane5.getViewport().add(_threshDescEditorPane, null);
    ImageIcon helpImage = new ImageIcon(AlgoTunerFrame.class.getResource("ToeSrchHelp.gif"));
    label.setIcon(helpImage);
    detailPane.setLayout(borderLayout);
    detailPane.add(label, BorderLayout.CENTER);

  }

  void moreInfoButton_actionPerformed(ActionEvent e)
  {
    this.hide();
    if (!detailedInfo)
    {
      jPanel20.remove(jPanel1);
      jPanel20.add(detailPane, BorderLayout.CENTER);
      jPanel20.revalidate();
      jPanel20.repaint();
      this.setSize(new Dimension(380, 570));
      _moreInfoButton.setText("Basic Info ...");
      detailedInfo = true;
    }
    else
    {
      jPanel20.remove(detailPane);
      jPanel20.add(jPanel1, BorderLayout.CENTER);
      jPanel20.revalidate();
      jPanel20.repaint();
      this.setSize(new Dimension(564, 265));
      _moreInfoButton.setText("Advanced Info ...");
      detailedInfo = false;
    }


    //this.pack();
    this.show();
  }
  void setInfo(URL text, Icon jointImage)
  {
    try
    {
      _threshDescEditorPane.setPage(text);
      jLabel12.setIcon(jointImage);
    }
    catch (IOException ioe)
    {
	System.out.println("EXCEPTION");
    }

  }

  void okButton_actionPerformed(ActionEvent e)
  {
    setVisible(false);
    _frame._showDescriptionCheckBox.setSelected(false);
  }
}
