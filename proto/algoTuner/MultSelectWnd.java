package proto.algoTuner;

/**
 * Title:        Algorithm Tuner
 * Description:
 * Copyright:    Copyright (c) 1999
 * Company:      Agilent Technologies
 * @author Andy Mechtenberg
 * @version
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import javax.swing.border.*;

public class MultSelectWnd extends JWindow
{

  public MultSelectWnd()
  {
    this(null);
  }
  AlgoTunerFrame _frame;

  JCheckBox jCheckBox2 = new JCheckBox();
  JCheckBox jCheckBox4 = new JCheckBox();
  JCheckBox jCheckBox6 = new JCheckBox();
  JPanel jPanel2 = new JPanel();
  JCheckBox jCheckBox7 = new JCheckBox();
  JCheckBox jCheckBox5 = new JCheckBox();
  GridLayout gridLayout1 = new GridLayout();
  JPanel panel1 = new JPanel();
  JCheckBox jCheckBox3 = new JCheckBox();
  JCheckBox jCheckBox1 = new JCheckBox();
  JCheckBox jCheckBox8 = new JCheckBox();
  JPanel jPanel1 = new JPanel();
  JButton jButton1 = new JButton();
  BorderLayout borderLayout1 = new BorderLayout();
  Border border1;

  public MultSelectWnd(AlgoTunerFrame frame)
  {
    _frame = frame;
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception
  {
    border1 = BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.black,1),BorderFactory.createEmptyBorder(0,3,0,0));
    jCheckBox2.setText("jCheckBox2");
    jCheckBox4.setText("jCheckBox4");
    jCheckBox6.setText("jCheckBox6");
    //gridLayout1.setRows(4);
    //panel1.add(jCheckBox6, null);
    //panel1.add(jCheckBox4, null);
    jCheckBox7.setText("Toe");
    jCheckBox5.setText("Pad");
    gridLayout1.setRows(3);
    gridLayout1.setColumns(2);
    panel1.setLayout(gridLayout1);
    jCheckBox3.setText("Smear");
    jCheckBox1.setText("Heel");
    jCheckBox8.setText("Center");
    jButton1.setPreferredSize(new Dimension(64, 20));
    jButton1.setText("Done");
    jButton1.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
	jButton1_actionPerformed(e);
      }
    });
    jPanel2.setLayout(borderLayout1);
    jPanel2.setBorder(border1);
    this.getContentPane().add(jPanel2, BorderLayout.CENTER);
    jPanel2.add(jPanel1, BorderLayout.SOUTH);
    jPanel1.add(jButton1, null);
    jPanel2.add(panel1, BorderLayout.CENTER);
    //panel1.add(jCheckBox2, null);
    panel1.add(jCheckBox1, null);
    panel1.add(jCheckBox8, null);
    panel1.add(jCheckBox7, null);
    panel1.add(jCheckBox5, null);
    panel1.add(jCheckBox3, null);
  }

  void jButton1_actionPerformed(ActionEvent e)
  {
    dispose();
  }
  void showwnd()
  {
    System.out.println("Inside show -- before");
    super.show();
    System.out.println("Inside show -- after");
  }
}
