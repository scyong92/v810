
/**
 * Title:        Algorithm Tuner<p>
 * Description:  <p>
 * Copyright:    Copyright (c) 1999<p>
 * Company:      Agilent Technologies<p>
 * @author Andy Mechtenberg
 * @version
 */
package proto.algoTuner;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;

public class FeedbackSettingsDlg extends JDialog
{
   JPanel panel1 = new JPanel();
   JPanel jPanel1 = new JPanel();
   JButton _allOnButton = new JButton();
   JButton _allOffButton = new JButton();
   JCheckBox _markRejectedJointsCheckBox = new JCheckBox();
   JCheckBox _markMarginalJointsCheckBox = new JCheckBox();
   JCheckBox _markAcceptedJointsCheckBox = new JCheckBox();
   JCheckBox _drawMeasCheckBox = new JCheckBox();
   JCheckBox _drawBasicCheckBox = new JCheckBox();
   JCheckBox _drawAdvancedCheckBox = new JCheckBox();
   JCheckBox _diagnosticTextCheckBox = new JCheckBox();
   JCheckBox _pauseRejectedJointsCheckBox = new JCheckBox();
   JCheckBox _pauseMarginalJointsCheckBox = new JCheckBox();
   JCheckBox _pauseAcceptedJointsCheckBox = new JCheckBox();
   JButton _cancelButton = new JButton();
   JButton _okButton = new JButton();
   BorderLayout borderLayout1 = new BorderLayout();
   JPanel jPanel2 = new JPanel();
   GridLayout gridLayout1 = new GridLayout();
   JPanel jPanel3 = new JPanel();
   JPanel jPanel4 = new JPanel();
   GridLayout gridLayout2 = new GridLayout();

   FeedbackSettings _feedbackSettings;


   public FeedbackSettingsDlg(Frame frame, String title, boolean modal, FeedbackSettings feedbackSettings)
   {

      super(frame, title, modal);
      try  {
         jbInit();
         _feedbackSettings = feedbackSettings;
         _markRejectedJointsCheckBox.setSelected(feedbackSettings.getMarkRejectedJoints());
         _markMarginalJointsCheckBox.setSelected(feedbackSettings.getMarkMarginalJoints());
         _markAcceptedJointsCheckBox.setSelected(feedbackSettings.getMarkAcceptedJoints());
         _drawMeasCheckBox.setSelected(feedbackSettings.getDrawMeas());
         _drawBasicCheckBox.setSelected(feedbackSettings.getDrawBasic());
         _drawAdvancedCheckBox.setSelected(feedbackSettings.getDrawAdvanced());
         _diagnosticTextCheckBox.setSelected(feedbackSettings.getDiagnosticText());
         _pauseRejectedJointsCheckBox.setSelected(feedbackSettings.getPauseRejectedJoints());
         _pauseMarginalJointsCheckBox.setSelected(feedbackSettings.getPauseMarginalJoints());
         _pauseAcceptedJointsCheckBox.setSelected(feedbackSettings.getPauseAcceptedJoints());
         pack();
      }
      catch(Exception ex) {
         ex.printStackTrace();
      }
   }

   public FeedbackSettingsDlg() {
      this(null, "", false, new FeedbackSettings());
   }

   void jbInit() throws Exception {
      panel1.setLayout(borderLayout1);
      _allOnButton.setText("All on");
      _allOnButton.addActionListener(new java.awt.event.ActionListener() {

         public void actionPerformed(ActionEvent e) {
            _allOnButton_actionPerformed(e);
         }
      });
      _allOffButton.setText("All off");
      _allOffButton.addActionListener(new java.awt.event.ActionListener() {

         public void actionPerformed(ActionEvent e) {
            _allOffButton_actionPerformed(e);
         }
      });
      _markRejectedJointsCheckBox.setText("Mark Rejected Joints");
      _markMarginalJointsCheckBox.setText("Mark Marginal Joints");
      _markAcceptedJointsCheckBox.setText("Mark Accepted Joints");
      _drawMeasCheckBox.setText("Draw Measurement Regions");
      _drawBasicCheckBox.setText("Draw Basic Plots");
      _drawAdvancedCheckBox.setText("Draw Advanced Plots");
      _diagnosticTextCheckBox.setText("Display Diagnostic Text");
      _pauseRejectedJointsCheckBox.setText("Pause On Rejected Joints");
      _pauseMarginalJointsCheckBox.setText("Pause On Marginal Joints");
      _pauseAcceptedJointsCheckBox.setText("Pause On Accpeted Joints");
      _cancelButton.setText("Cancel");
      _cancelButton.addActionListener(new java.awt.event.ActionListener() {

         public void actionPerformed(ActionEvent e) {
            _cancelButton_actionPerformed(e);
         }
      });
      _okButton.setText("Ok");
      _okButton.addActionListener(new java.awt.event.ActionListener() {

         public void actionPerformed(ActionEvent e) {
            _okButton_actionPerformed(e);
         }
      });
      jPanel2.setLayout(gridLayout1);
      gridLayout1.setRows(10);
      borderLayout1.setHgap(30);
      jPanel1.setLayout(gridLayout2);
      gridLayout2.setRows(2);
      getContentPane().add(panel1);
      panel1.add(jPanel1, BorderLayout.SOUTH);
      jPanel1.add(jPanel4, null);
      jPanel4.add(_allOnButton, null);
      jPanel4.add(_allOffButton, null);
      jPanel1.add(jPanel3, null);
      jPanel3.add(_okButton, null);
      jPanel3.add(_cancelButton, null);
      panel1.add(jPanel2, BorderLayout.EAST);
      jPanel2.add(_markRejectedJointsCheckBox, null);
      jPanel2.add(_markMarginalJointsCheckBox, null);
      jPanel2.add(_markAcceptedJointsCheckBox, null);
      jPanel2.add(_drawMeasCheckBox, null);
      jPanel2.add(_drawBasicCheckBox, null);
      jPanel2.add(_drawAdvancedCheckBox, null);
      jPanel2.add(_diagnosticTextCheckBox, null);
      jPanel2.add(_pauseRejectedJointsCheckBox, null);
      jPanel2.add(_pauseMarginalJointsCheckBox, null);
      jPanel2.add(_pauseAcceptedJointsCheckBox, null);
   }

   void _okButton_actionPerformed(ActionEvent e)
   {
     // update feedback options class
     _feedbackSettings.setMarkRejectedJoints(_markRejectedJointsCheckBox.isSelected());
     _feedbackSettings.setMarkMarginalJoints(_markMarginalJointsCheckBox.isSelected());
     _feedbackSettings.setMarkAcceptedJoints(_markAcceptedJointsCheckBox.isSelected());
     _feedbackSettings.setDrawMeas(_drawMeasCheckBox.isSelected());
     _feedbackSettings.setDrawBasic(_drawBasicCheckBox.isSelected());
     _feedbackSettings.setDrawAdvanced(_drawAdvancedCheckBox.isSelected());
     _feedbackSettings.setDiagnosticText(_diagnosticTextCheckBox.isSelected());
     _feedbackSettings.setPauseRejectedJoints(_pauseRejectedJointsCheckBox.isSelected());
     _feedbackSettings.setPauseMarginalJoints(_pauseMarginalJointsCheckBox.isSelected());
     _feedbackSettings.setPauseAcceptedJoints(_pauseAcceptedJointsCheckBox.isSelected());
     dispose();
   }

   void _cancelButton_actionPerformed(ActionEvent e)
   {
      dispose();
   }

   void _allOnButton_actionPerformed(ActionEvent e)
   {
     _markRejectedJointsCheckBox.setSelected(true);
     _markMarginalJointsCheckBox.setSelected(true);
     _markAcceptedJointsCheckBox.setSelected(true);
     _drawMeasCheckBox.setSelected(true);
     _drawBasicCheckBox.setSelected(true);
     _drawAdvancedCheckBox.setSelected(true);
     _diagnosticTextCheckBox.setSelected(true);
     _pauseRejectedJointsCheckBox.setSelected(true);
     _pauseMarginalJointsCheckBox.setSelected(true);
     _pauseAcceptedJointsCheckBox.setSelected(true);
   }

   void _allOffButton_actionPerformed(ActionEvent e)
   {
     _markRejectedJointsCheckBox.setSelected(false);
     _markMarginalJointsCheckBox.setSelected(false);
     _markAcceptedJointsCheckBox.setSelected(false);
     _drawMeasCheckBox.setSelected(false);
     _drawBasicCheckBox.setSelected(false);
     _drawAdvancedCheckBox.setSelected(false);
     _diagnosticTextCheckBox.setSelected(false);
     _pauseRejectedJointsCheckBox.setSelected(false);
     _pauseMarginalJointsCheckBox.setSelected(false);
     _pauseAcceptedJointsCheckBox.setSelected(false);
   }
}

