package proto.algoTuner;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.io.*;
//import com.agilent.xRayTest.util.*;
import javax.swing.border.*;

public class AlgoTunerFrame extends JFrame
{
  JMenuBar menuBar1 = new JMenuBar();
  JMenu menuFile = new JMenu();
  JMenuItem menuFileExit = new JMenuItem();
  JMenu menuHelp = new JMenu();
  JMenuItem menuHelpAbout = new JMenuItem();
  JToolBar toolBar = new JToolBar();
  ImageIcon image1;
  ImageIcon image2;
  ImageIcon image3;
  ImageIcon jointImage;

  ImageIcon pauseImage;
  ImageIcon runImage;
  ImageIcon stopImage;
  ImageIcon runToBreakImage;
  ImageIcon runToJointImage;
  ImageIcon runToViewImage;
  ImageIcon runToEndImage;
  ImageIcon tunerImage;

  ImageIcon pauseDisabledImage;
  ImageIcon runDisabledImage;
  ImageIcon stopDisabledImage;
  ImageIcon runToBreakDisabledImage;
  ImageIcon runToJointDisabledImage;
  ImageIcon runToViewDisabledImage;
  ImageIcon runToEndDisabledImage;


  JLabel statusBar = new JLabel();
  BorderLayout borderLayout1 = new BorderLayout();
  JPanel jPanel2 = new JPanel();
  JPanel jPanel1 = new JPanel();
  JPanel jPanel3 = new JPanel();
  BorderLayout borderLayout2 = new BorderLayout();
  JPanel jPanel4 = new JPanel();
  JScrollPane jScrollPane2 = new JScrollPane();
  JTable jTable2;
  JPanel jPanel5 = new JPanel();
  JPanel jPanel9 = new JPanel();
  JComboBox _algorithmComboBox1 = new JComboBox();
  JLabel jLabel2 = new JLabel();
  JPanel jPanel11 = new JPanel();

  AlgoTableModel _standardThresholdModel = null;
  AlgoTableModel _advancedThresholdModel = null;
  JSplitPane jSplitPane2 = new JSplitPane();
//  JTextArea jEditorPane1 = new JTextArea();
  BorderLayout borderLayout8 = new BorderLayout();
  FlowLayout flowLayout3 = new FlowLayout();
  JPanel jPanel13 = new JPanel();
  Border border1;
  JPanel jPanel18 = new JPanel();
  BorderLayout borderLayout11 = new BorderLayout();
  Border border2;
  Border border3;
  JLabel jLabel5 = new JLabel();
  JRadioButton _allViewsRadioButton = new JRadioButton();
  JRadioButton _viewFromRadioButton = new JRadioButton();
  JTextField _viewFromTextField = new JTextField();
  JPanel jPanel23 = new JPanel();
  GridLayout gridLayout6 = new GridLayout();
  FlowLayout flowLayout16 = new FlowLayout();
  JPanel jPanel26 = new JPanel();
  JLabel jLabel9 = new JLabel();
  FlowLayout flowLayout17 = new FlowLayout();
  BorderLayout borderLayout3 = new BorderLayout();
  BorderLayout borderLayout12 = new BorderLayout();
  JPanel jPanel37 = new JPanel();
  BorderLayout borderLayout14 = new BorderLayout();
  Border border4;
  Border border5;
  JPanel jPanel16 = new JPanel();
  JPanel _statusPanel = new JPanel();
  GridLayout gridLayout8 = new GridLayout();
  JTextField _viewToTextField = new JTextField();

  // create classes as basis for dialog boxes
  FeedbackConfiguration _feedbackConfiguration = new FeedbackConfiguration();
  FamilyAlgoPairing _learningConfiguration = new FamilyAlgoPairing();
  FamilyAlgoPairing _onOffConfiguration = new FamilyAlgoPairing();

  FeedbackSettings _feedbackSettings = new FeedbackSettings();
  JMenu _optionsMenu = new JMenu();
  JMenuItem _configurationSettings = new JMenuItem();
  JMenu _actionsMenu = new JMenu();
  JMenuItem _nextMeasurementMenuItem = new JMenuItem();
  JMenuItem _nextJointMenuItem = new JMenuItem();
  JMenuItem _nextViewMenuItem = new JMenuItem();
  JMenuItem _toCompletionMenuItem = new JMenuItem();
  JMenuItem _pauseMenuItem = new JMenuItem();
  JMenuItem _abortMenuItem = new JMenuItem();
  JCheckBoxMenuItem _generateDiagnosticsMeasurementsCheckBoxMenuItem1 = new JCheckBoxMenuItem();
  JPanel jPanel10 = new JPanel();
  JTextField _runDescriptionTextField = new JTextField();
  JLabel jLabel4 = new JLabel();
  Border border6;
  BorderLayout borderLayout4 = new BorderLayout();
  BorderLayout borderLayout6 = new BorderLayout();
  JPanel jPanel12 = new JPanel();
  JTabbedPane jTabbedPane1 = new JTabbedPane();
  JScrollPane jScrollPane1 = new JScrollPane();
  JTable jTable1;
  BorderLayout borderLayout7 = new BorderLayout();
  FlowLayout flowLayout1 = new FlowLayout();
  JLabel jLabel10 = new JLabel();
  JPanel jPanel14 = new JPanel();
  JCheckBoxMenuItem _withFeedbackCheckBoxMenuItem2 = new JCheckBoxMenuItem();
  JCheckBoxMenuItem _withLearningCheckBoxMenuItem3 = new JCheckBoxMenuItem();
  JMenuItem _runMenuItem = new JMenuItem();
  JPanel jPanel17 = new JPanel();
  BorderLayout borderLayout10 = new BorderLayout();
  JLabel jLabel13 = new JLabel();
  JList jList1 = new JList();
  JPanel jPanel21 = new JPanel();
  JButton jButton4 = new JButton();
  JButton jButton5 = new JButton();
  GridLayout gridLayout2 = new GridLayout();
  JButton jButton6 = new JButton();
  JButton jButton7 = new JButton();
  JButton jButton8 = new JButton();
  JPanel jPanel22 = new JPanel();
  BorderLayout borderLayout13 = new BorderLayout();
  JCheckBox jCheckBox1 = new JCheckBox();
  FlowLayout flowLayout7 = new FlowLayout();
  JButton _deleteLearnedDataButton = new JButton();
  JButton _writeLearnedDataButton = new JButton();
  JButton _copyThresholdButton = new JButton();
  Border border7;
  FlowLayout flowLayout6 = new FlowLayout();
  JPanel jPanel24 = new JPanel();
  JPanel jPanel25 = new JPanel();
  JPanel jPanel27 = new JPanel();
  JLabel _testToRunLabel = new JLabel();
  FlowLayout flowLayout10 = new FlowLayout();
  Border border8;
  Border border9;
  Border border10;
  JPanel jPanel28 = new JPanel();
  BorderLayout borderLayout15 = new BorderLayout();
  JPanel jPanel31 = new JPanel();
  JButton jButton1 = new JButton();
  JButton jButton2 = new JButton();
  JPanel jPanel38 = new JPanel();
  JPanel jPanel39 = new JPanel();
  BorderLayout borderLayout16 = new BorderLayout();
  JCheckBox jCheckBox2 = new JCheckBox();
  JCheckBox jCheckBox3 = new JCheckBox();
  JCheckBox jCheckBox4 = new JCheckBox();
  JTextField jTextField1 = new JTextField();
  JLabel jLabel11 = new JLabel();
  Border border11;
  TitledBorder titledBorder1;
  BorderLayout borderLayout18 = new BorderLayout();
  BorderLayout borderLayout19 = new BorderLayout();
  Border border12;
  Border border13;
  Border border14;
  Border border15;
  Border border16;
  JMenu _displayMenu = new JMenu();
  JMenuItem _displayCADStatusMenuItem = new JMenuItem();
  JMenuItem _displayMachineStatusMenuItem = new JMenuItem();
  JPanel jPanel48 = new JPanel();
  JPanel jPanel36 = new JPanel();
  JCheckBox _showDescriptionCheckBox = new JCheckBox();
  BorderLayout borderLayout28 = new BorderLayout();
  Border border17;
  Border border18;
  Border border19;
  BorderLayout borderLayout29 = new BorderLayout();
  JPanel jPanel50 = new JPanel();
  JLabel _subtypeLabel = new JLabel();
  JLabel _familyLabel = new JLabel();
  JRadioButton _standardThresholdsRadioButton = new JRadioButton();
  JRadioButton _advancedThresholdsRadioButton = new JRadioButton();
  GridLayout gridLayout4 = new GridLayout();
  FlowLayout flowLayout8 = new FlowLayout();
  FlowLayout flowLayout9 = new FlowLayout();
  JButton _toCompletionButton = new JButton();
  JScrollPane jScrollPane6 = new JScrollPane();
  JPanel jPanel110 = new JPanel();
  JButton _nextViewButton = new JButton();
  JButton _stopButton = new JButton();
  JPanel jPanel311 = new JPanel();
  JTabbedPane jTabbedPane2 = new JTabbedPane();
  JButton _nextMeasurementButton = new JButton();
  GridLayout gridLayout9 = new GridLayout();
  JButton _nextJointButton = new JButton();
  JButton _pauseButton = new JButton();
  BorderLayout borderLayout9 = new BorderLayout();
  JTextArea _messagesTextArea1 = new JTextArea();
  JScrollPane jScrollPane7 = new JScrollPane();
  JPanel jPanel111 = new JPanel();
  BorderLayout borderLayout27 = new BorderLayout();
  JTextArea _messagesTextArea2 = new JTextArea();

  ThresholdDescDlg _threshDescDlg;
  JPanel jPanel49 = new JPanel();
  JCheckBox _feedbackCheckBox = new JCheckBox();
  JCheckBox _subtypeLearningCheckBox = new JCheckBox();
  JCheckBox _jointLearningCheckBox = new JCheckBox();
  JPanel jPanel51 = new JPanel();
  JButton _configureFeedbackButton = new JButton();
  JPanel jPanel54 = new JPanel();
  BorderLayout borderLayout30 = new BorderLayout();
  JPanel jPanel55 = new JPanel();
  JLabel jLabel19 = new JLabel();
  JLabel jLabel20 = new JLabel();
  JLabel jLabel21 = new JLabel();
  JLabel jLabel22 = new JLabel();
  JLabel jLabel23 = new JLabel();
  JLabel jLabel24 = new JLabel();
  GridLayout gridLayout5 = new GridLayout();
  Border border20;
  JToolBar jToolBar1 = new JToolBar();
  JPanel jPanel53 = new JPanel();
  JPanel jPanel52 = new JPanel();
  BorderLayout borderLayout17 = new BorderLayout();
  JButton _nextJointButton1 = new JButton();
  JButton _pauseButton1 = new JButton();
  JButton _toCompletionButton1 = new JButton();
  JButton _nextViewButton1 = new JButton();
  JButton _runButton = new JButton();
  JButton _nextMeasurementButton1 = new JButton();
  JPanel jPanel35 = new JPanel();
  JButton _stopButton1 = new JButton();
  JToggleButton _familySubtypeToggleButton = new JToggleButton();
  JToggleButton _devicePinToggleButton = new JToggleButton();
  JToggleButton _boardViewToggleButton = new JToggleButton();
  JPanel jPanel29 = new JPanel();
  JPanel jPanel34 = new JPanel();
  JPanel jPanel30 = new JPanel();
  JLabel jLabel18 = new JLabel();
  JLabel jLabel17 = new JLabel();
  JLabel jLabel16 = new JLabel();
  JTextField _viewToTextField1 = new JTextField();
  JPanel jPanel47 = new JPanel();
  JRadioButton _allViewsRadioButton1 = new JRadioButton();
  FlowLayout flowLayout19 = new FlowLayout();
  FlowLayout flowLayout18 = new FlowLayout();
  FlowLayout flowLayout13 = new FlowLayout();
  JLabel jLabel8 = new JLabel();
  JRadioButton _viewFromRadioButton1 = new JRadioButton();
  JPanel jPanel211 = new JPanel();
  BorderLayout borderLayout26 = new BorderLayout();
  JPanel jPanel210 = new JPanel();
  GridLayout gridLayout10 = new GridLayout();
  JTextField _viewFromTextField1 = new JTextField();
  JComboBox _boardComboBox = new JComboBox();
  JPanel jPanel8 = new JPanel();
  JPanel jPanel7 = new JPanel();
  JPanel jPanel6 = new JPanel();
  JLabel jLabel14 = new JLabel();
  JPanel jPanel43 = new JPanel();
  FlowLayout flowLayout4 = new FlowLayout();
  JPanel jPanel42 = new JPanel();
  JPanel jPanel41 = new JPanel();
  FlowLayout flowLayout2 = new FlowLayout();
  JPanel jPanel40 = new JPanel();
  GridLayout gridLayout7 = new GridLayout();
  JComboBox _subtypeComboBox = new JComboBox();
  JComboBox _familyComboBox = new JComboBox();
  GridLayout gridLayout1 = new GridLayout();
  JTextField jTextField2 = new JTextField();
  JLabel jLabel3 = new JLabel();
  JLabel jLabel1 = new JLabel();
  BorderLayout borderLayout22 = new BorderLayout();
  BorderLayout borderLayout21 = new BorderLayout();
  BorderLayout borderLayout20 = new BorderLayout();
  Border border21;
  JComboBox _testModeSelectComboBox = new JComboBox();
  JPanel jPanel57 = new JPanel();
  JLabel jLabel25 = new JLabel();
  JPanel jPanel56 = new JPanel();
  JTextField jTextField4 = new JTextField();
  BorderLayout borderLayout31 = new BorderLayout();
  BorderLayout borderLayout32 = new BorderLayout();
  JPanel jPanel19 = new JPanel();
  JButton _moreInfoButton = new JButton();
  JPanel jPanel20 = new JPanel();
  JPanel jPanel46 = new JPanel();
  BorderLayout borderLayout5 = new BorderLayout();
  BorderLayout borderLayout25 = new BorderLayout();
  JPanel jPanel45 = new JPanel();
  JToggleButton _showDescToggleButton = new JToggleButton();
  Border border22;
  Border border23;
  JPanel jPanel58 = new JPanel();
  BorderLayout borderLayout24 = new BorderLayout();
  JScrollPane jScrollPane5 = new JScrollPane();
  JPanel jPanel44 = new JPanel();
  BorderLayout borderLayout23 = new BorderLayout();
  JEditorPane jEditorPane1 = new JEditorPane();
  JLabel jLabel12 = new JLabel();
  FlowLayout flowLayout5 = new FlowLayout();
  JPanel jPanel15 = new JPanel();
  JPanel jPanel59 = new JPanel();
  FlowLayout flowLayout11 = new FlowLayout();
  JPanel jPanel33 = new JPanel();
  JComboBox _deviceComboBox = new JComboBox();
  JLabel jLabel6 = new JLabel();
  JPanel jPanel32 = new JPanel();
  JComboBox _pinComboBox = new JComboBox();
  JLabel jLabel7 = new JLabel();
  FlowLayout flowLayout12 = new FlowLayout();
  GridLayout gridLayout11 = new GridLayout();
  BorderLayout borderLayout33 = new BorderLayout();
  JPanel jPanel412 = new JPanel();
  GridLayout gridLayout12 = new GridLayout();
  JPanel jPanel60 = new JPanel();
  JPanel jPanel61 = new JPanel();
  JLabel jLabel28 = new JLabel();
  JLabel jLabel27 = new JLabel();
  FlowLayout flowLayout14 = new FlowLayout();
  FlowLayout flowLayout15 = new FlowLayout();
  JMenuItem _saveDiagnosticImageMenuItem = new JMenuItem();

  //Construct the frame
  public AlgoTunerFrame()
  {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  //Component initialization
  private void jbInit() throws Exception
  {
    border4 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134)),"Next:"),BorderFactory.createEmptyBorder(5,5,5,5));
    border5 = BorderFactory.createEmptyBorder(10,0,0,0);
    border6 = BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134));

    border7 = BorderFactory.createEmptyBorder(0,0,0,10);
    border8 = BorderFactory.createEmptyBorder(5,0,0,0);
    border9 = BorderFactory.createEmptyBorder(5,0,0,0);
    border10 = BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.lightGray,Color.white,new Color(134, 134, 134),new Color(93, 93, 93));
    border11 = BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134));
    titledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134)),"Run Settings: ");
    border12 = BorderFactory.createEmptyBorder(0,10,5,10);
    border13 = BorderFactory.createEmptyBorder(0,5,8,10);
    border14 = BorderFactory.createEmptyBorder(5,5,10,10);
    border15 = BorderFactory.createEmptyBorder(0,5,0,0);
    border16 = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(93, 93, 93),new Color(134, 134, 134)),BorderFactory.createEmptyBorder(5,5,5,5));
    border17 = BorderFactory.createEmptyBorder(5,5,5,5);
    border18 = BorderFactory.createLineBorder(SystemColor.controlText,1);
    border19 = BorderFactory.createLineBorder(SystemColor.controlText,1);
    //titledBorder1.setTitleFont(new Font("Dialog", Font.BOLD, 12));
    border20 = BorderFactory.createEmptyBorder(5,20,5,0);
    border21 = BorderFactory.createEmptyBorder(5,10,5,10);
    border22 = BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(134, 134, 134),new Color(93, 93, 93));
    border23 = BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(178, 178, 157),new Color(124, 124, 109));
    JPopupMenu.setDefaultLightWeightPopupEnabled(false);
    image1 = new ImageIcon(AlgoTunerFrame.class.getResource("copy.gif"));
    image2 = new ImageIcon(AlgoTunerFrame.class.getResource("save.gif"));
    image3 = new ImageIcon(AlgoTunerFrame.class.getResource("cut.gif"));
    jointImage = new ImageIcon(AlgoTunerFrame.class.getResource("fpgullwing.gif"));

    pauseImage = new ImageIcon(AlgoTunerFrame.class.getResource("pause.gif"));
    runImage = new ImageIcon(AlgoTunerFrame.class.getResource("start.gif"));
    stopImage = new ImageIcon(AlgoTunerFrame.class.getResource("stop.gif"));
    runToBreakImage = new ImageIcon(AlgoTunerFrame.class.getResource("runtobreak.gif"));
    runToEndImage = new ImageIcon(AlgoTunerFrame.class.getResource("runtoend.gif"));
    runToJointImage = new ImageIcon(AlgoTunerFrame.class.getResource("runtojoint.gif"));
    runToViewImage = new ImageIcon(AlgoTunerFrame.class.getResource("runtoview.gif"));

    runDisabledImage = new ImageIcon(AlgoTunerFrame.class.getResource("startdisabled.gif"));
    stopDisabledImage = new ImageIcon(AlgoTunerFrame.class.getResource("stopdisabled.gif"));
    pauseDisabledImage = new ImageIcon(AlgoTunerFrame.class.getResource("pausedisabled.gif"));
    runToBreakDisabledImage = new ImageIcon(AlgoTunerFrame.class.getResource("runtobreakdisabled.gif"));
    runToJointDisabledImage = new ImageIcon(AlgoTunerFrame.class.getResource("runtojointdisabled.gif"));
    runToViewDisabledImage = new ImageIcon(AlgoTunerFrame.class.getResource("runtoviewdisabled.gif"));
    runToEndDisabledImage = new ImageIcon(AlgoTunerFrame.class.getResource("runtoenddisabled.gif"));


    tunerImage = new ImageIcon(AlgoTunerFrame.class.getResource("tuner.gif"));

    //jointImage = new ImageIcon(com.agilent.xRayTest.gui.proto.algoTuner.AlgoTunerFrame.class.getResource("basic.gif"));

    border1 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,new java.awt.Color(134, 134, 134)),BorderFactory.createEmptyBorder(10,10,0,10));
    border2 = BorderFactory.createEmptyBorder(10,10,0,10);
    border3 = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.lightGray,Color.white,new Color(93, 93, 93),new Color(134, 134, 134)),BorderFactory.createEmptyBorder(0,10,10,10));
    this.getContentPane().setLayout(borderLayout1);
    this.setTitle("Algorithm Tuner - Panel: NEPCON_WIDE  Board: WIDE_BOTTOM");
    this.setIconImage(tunerImage.getImage());
    statusBar.setBorder(border10);
    statusBar.setText("Status:  Ready");

    _standardThresholdModel = new AlgoTableModel(1);
    _advancedThresholdModel = new AlgoTableModel(2);
    jTable1 = new JTable(_standardThresholdModel);
    jTable2 = new JTable(_advancedThresholdModel);
    jTable1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    jTable2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    // add the listener for a table row selection
    ListSelectionModel rowSM = jTable1.getSelectionModel();
    rowSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        //System.out.println("Table Row changed");
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        if (lsm.isSelectionEmpty())
        {
        }
        else
        {
          int selectedRow = lsm.getMinSelectionIndex();
//          jTable1.editCellAt(selectedRow, 1);
        }
      }
    });

    final IntegerNumberField integerField = new IntegerNumberField(0, 5);
    integerField.setHorizontalAlignment(IntegerNumberField.CENTER);
    DefaultCellEditor integerEditor = new DefaultCellEditor(integerField)
    {
      public Object getCellEditorValue()
      {
        return new Integer(integerField.getValue());
      }
    };

    integerEditor.setClickCountToStart(1);
    integerField.addFocusListener(new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        //System.out.println("float focus gained");
        integerField.selectAll();
      }
    });

    final FloatNumberField floatField = new FloatNumberField(0, 5);
    floatField.setHorizontalAlignment(FloatNumberField.CENTER);
    DefaultCellEditor floatEditor = new DefaultCellEditor(floatField)
    {
      public Object getCellEditorValue()
      {
        return new Float(floatField.getValue());
      }
    };

    floatEditor.setClickCountToStart(1);

    floatField.addFocusListener(new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        //System.out.println("float focus gained");
        floatField.selectAll();
      }
    });

    JCheckBox boolBox = new JCheckBox();
    JComboBox toeSearchTechiqueComboBox = new JComboBox();
    toeSearchTechiqueComboBox.addItem("Thickness");
    toeSearchTechiqueComboBox.addItem("Side Fillet");
    toeSearchTechiqueComboBox.addItem("Avg Toe/Side");

    JComboBox heelSearchTechniqueComboBox = new JComboBox();
    heelSearchTechniqueComboBox.addItem("Profile Start");
    heelSearchTechniqueComboBox.addItem("Cad Position");

    JComboBox useLearnedDataComboBox = new JComboBox();
    useLearnedDataComboBox.addItem("Yes");
    useLearnedDataComboBox.addItem("No");

    CustomThresholdEditor rowEditor = new CustomThresholdEditor(jTable1);
    rowEditor.setEditorAt(0, new DefaultCellEditor(heelSearchTechniqueComboBox));
    rowEditor.setEditorAt(1, floatEditor);
    rowEditor.setEditorAt(2, floatEditor);
    rowEditor.setEditorAt(3, floatEditor);
    rowEditor.setEditorAt(4, floatEditor);
    rowEditor.setEditorAt(5, new DefaultCellEditor(useLearnedDataComboBox));
    rowEditor.setEditorAt(6, new DefaultCellEditor(toeSearchTechiqueComboBox));
    jTable1.getColumn("Value").setCellEditor(rowEditor);

    jTable1.setDefaultRenderer(Object.class, new CustomThresholdRenderer());
    //jTable2.setDefaultRenderer(Object.class, new CustomThresholdRenderer());
//    CustomThresholdRenderer boolRenderer = new CustomThresholdRenderer();
//    CheckBoxRenderer checkBoxRenderer = new CheckBoxRenderer();
//    boolRenderer.add(5, checkBoxRenderer );
//    jTable1.getColumn("Value").setCellRenderer(boolRenderer);

    // add a listener for frame resizing
    addComponentListener(new ComponentAdapter()
    {
      public void componentResized(ComponentEvent e)
      {
        resizeTablePanel();
      }
    });

    menuFile.setText("File");
    menuFileExit.setText("Exit");
    menuFileExit.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        fileExit_actionPerformed(e);
      }
    });
    menuHelp.setText("Help");
    menuHelpAbout.setText("About");
    menuHelpAbout.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        helpAbout_actionPerformed(e);
      }
    });
    toolBar.setBorder(BorderFactory.createEtchedBorder());
    jPanel2.setLayout(borderLayout2);
    jPanel1.setLayout(borderLayout3);
    jPanel4.setBorder(border3);
    jPanel4.setLayout(borderLayout4);
    jLabel2.setText("Algorithm:");
    jPanel5.setLayout(flowLayout6);
    jPanel11.setLayout(flowLayout1);
    jPanel3.setLayout(borderLayout8);
    jPanel9.setLayout(flowLayout3);
    flowLayout3.setAlignment(FlowLayout.LEFT);
    jPanel18.setLayout(borderLayout11);
    jLabel5.setPreferredSize(new Dimension(55, 17));
    jLabel5.setText("Views:");
    _allViewsRadioButton.setText("All");
    _allViewsRadioButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        _allViewsRadioButton_actionPerformed(e);
      }
    });
    _viewFromRadioButton.setToolTipText("");
    _viewFromRadioButton.setText("From");
    _viewFromRadioButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        _viewFromRadioButton_actionPerformed(e);
      }
    });
    _viewFromTextField.setMinimumSize(new Dimension(4, 60));
    _viewFromTextField.setPreferredSize(new Dimension(100, 21));
    _viewFromTextField.setText("1");
    jPanel13.setLayout(gridLayout6);
    jPanel23.setLayout(flowLayout16);
    gridLayout6.setRows(2);
    flowLayout16.setAlignment(FlowLayout.LEFT);
    jPanel26.setLayout(flowLayout17);
    flowLayout17.setAlignment(FlowLayout.LEFT);
    jLabel9.setPreferredSize(new Dimension(55, 17));
    jPanel37.setLayout(borderLayout14);
    jPanel11.setBorder(border5);
    //_configureFeedbackButton.setPreferredSize(new Dimension(100, 25));
    _statusPanel.setLayout(gridLayout8);
    _algorithmComboBox1.setPreferredSize(new Dimension(125, 21));
    _viewToTextField.setPreferredSize(new Dimension(100, 21));
    _viewToTextField.setText("78");
    _optionsMenu.setText("Options");
    _configurationSettings.setText("Configuration...");
    _configurationSettings.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        _configurationSettings_actionPerformed(e);
      }
    });
    _actionsMenu.setText("Actions");
    _nextMeasurementMenuItem.setText("Next Measurement");
    KeyStroke f5KeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0);
    _nextMeasurementMenuItem.setAccelerator(f5KeyStroke);

    _nextJointMenuItem.setText("Next Joint");
    KeyStroke f6KeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0);
    _nextJointMenuItem.setAccelerator(f6KeyStroke);

    _nextViewMenuItem.setText("Next View");
    KeyStroke f7KeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0);
    _nextViewMenuItem.setAccelerator(f7KeyStroke);

    _toCompletionMenuItem.setText("To Completion");
    KeyStroke f8KeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0);
    _toCompletionMenuItem.setAccelerator(f8KeyStroke);

    _pauseMenuItem.setText("Pause");
    KeyStroke f9KeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_F9, 0);
    _pauseMenuItem.setAccelerator(f9KeyStroke);

    _abortMenuItem.setText("Abort");
    KeyStroke f12KeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0);
    _abortMenuItem.setAccelerator(f12KeyStroke);
    _abortMenuItem.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        stopButton_actionPerformed(e);
      }
    });

    _generateDiagnosticsMeasurementsCheckBoxMenuItem1.setSelected(true);
    _generateDiagnosticsMeasurementsCheckBoxMenuItem1.setText("Save All Measurements");
    jLabel4.setText("Run Description:");
    jPanel10.setLayout(borderLayout6);
    jPanel16.setBorder(border6);
    jPanel16.setLayout(flowLayout7);
    borderLayout6.setHgap(20);
    borderLayout6.setVgap(5);
    jPanel12.setLayout(borderLayout7);

    //jEditorPane1.setBackground(Color.lightGray);

//    jEditorPane1.setEditable(false);

//    jEditorPane1.setText("Improved Threshold description based on current active threshold" + "<br>" +
//                         "Incorporating links to other thresholds for easy navigation" + "<br>" +
//                         "Note also the revised threshold names." + "<br>" +
//                         "Depends on: <a href=file://c:test.html>Threshold 1</a>" + "<br>" +
//                         "Affects: <a href=file://c:test.html>Threshold 2</a>" + "<br>" +
//                         "Old Threshold Name:  OLD_THRESHOLD_NAME");

//    jEditorPane1.setPage("file:///m:/andy5dx/java/com/agilent/mtd/agt5dx/gui/algoTuner/fpgullwingnoi.html");

    jSplitPane2.setOrientation(JSplitPane.VERTICAL_SPLIT);
//    jSplitPane2.setBackground(new Color(0, 120, 120));
//    jPanel18.setBackground(SystemColor.activeCaption);
//    jPanel18.setBackground(new Color(0, 120, 120));
    jLabel10.setText("To:");
    _withFeedbackCheckBoxMenuItem2.setText("With Feedback");
    KeyStroke f3KeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0);
    _withFeedbackCheckBoxMenuItem2.setAccelerator(f3KeyStroke);

    _withFeedbackCheckBoxMenuItem2.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        withFeedback_actionPerformed(e);
      }
    });
    _withLearningCheckBoxMenuItem3.setText("With Learning");
    KeyStroke f2KeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0);
    _withLearningCheckBoxMenuItem3.setAccelerator(f2KeyStroke);
    _withLearningCheckBoxMenuItem3.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        withLearning_actionPerformed(e);
      }
    });

    _runMenuItem.setText("Run");
    KeyStroke f4KeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_F4, 0);
    _runMenuItem.setAccelerator(f4KeyStroke);

    _runMenuItem.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        runButton_actionPerformed(e);
      }
    });
    jPanel17.setLayout(borderLayout10);
    jLabel13.setToolTipText("");
    jLabel13.setText("Defect List:");
    jButton4.setText("Delete Defect");
    jButton5.setText("Next Defect");
    jPanel21.setLayout(gridLayout2);
    gridLayout2.setRows(5);
    gridLayout2.setVgap(3);
    jButton6.setText("Print Report");
    jButton7.setText("Save Image");
    jButton8.setText("Previous Defect");
    borderLayout10.setHgap(5);
    jPanel22.setLayout(borderLayout13);
    jCheckBox1.setText("With Feedback");
    _deleteLearnedDataButton.setPreferredSize(new Dimension(55, 21));
    _deleteLearnedDataButton.setToolTipText("Delete Learned Data...");
    _deleteLearnedDataButton.setIcon(image3);
    _deleteLearnedDataButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        deleteLearnedDataButton_actionPerformed(e);
      }
    });
    _writeLearnedDataButton.setPreferredSize(new Dimension(55, 31));
    _writeLearnedDataButton.setToolTipText("Write Learned Data");
    _writeLearnedDataButton.setIcon(image2);
    _writeLearnedDataButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        writeLearnedDataButton_actionPerformed(e);
      }
    });
    _copyThresholdButton.setPreferredSize(new Dimension(55, 31));
    _copyThresholdButton.setToolTipText("Copy Thresholds...");
    _copyThresholdButton.setIcon(image1);

    _copyThresholdButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        copyThresholdButton_actionPerformed(e);
      }
    });
    jPanel22.setBorder(border7);
    flowLayout6.setAlignment(FlowLayout.LEFT);
    flowLayout6.setHgap(25);
    flowLayout6.setVgap(0);
    jPanel14.setLayout(borderLayout29);
    jPanel25.setLayout(borderLayout28);
    jPanel24.setLayout(flowLayout8);
    _testToRunLabel.setText("Test To Run:  ");
    jPanel27.setLayout(flowLayout10);
    flowLayout10.setAlignment(FlowLayout.LEFT);
    flowLayout10.setHgap(0);
    flowLayout10.setVgap(0);
    flowLayout1.setVgap(0);
    jPanel27.setBorder(border8);
    jPanel10.setBorder(border9);
    jPanel28.setLayout(borderLayout15);
    jButton1.setText("jButton1");
    jButton2.setText("jButton2");
    jPanel31.setLayout(borderLayout16);
    jCheckBox2.setText("jCheckBox2");
    jCheckBox3.setText("jCheckBox3");
    jCheckBox4.setText("jCheckBox4");
    jTextField1.setText("jTextField1");
    jLabel11.setText("Run Description:");
    jPanel31.setFont(new java.awt.Font("Dialog", 1, 12));
    jPanel31.setBorder(titledBorder1);
    jPanel28.setBorder(border3);
    borderLayout15.setHgap(10);
    borderLayout15.setVgap(10);
    jPanel39.setLayout(borderLayout18);
    jPanel38.setLayout(borderLayout19);
    jPanel39.setBorder(border12);
    jPanel38.setBorder(border12);
    _displayMenu.setText("Display");
    _displayCADStatusMenuItem.setText("CAD Status");
    _displayCADStatusMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        displayCADStatusMenuItem_actionPerformed(e);
      }
    });
    _displayMachineStatusMenuItem.setText("Machine Status");
    _displayMachineStatusMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        displayMachineStatusMenuItem_actionPerformed(e);
      }
    });
    _showDescriptionCheckBox.setPreferredSize(new Dimension(129, 17));
    _showDescriptionCheckBox.setSelected(true);
    _showDescriptionCheckBox.setText("Show Descriptions");
    _showDescriptionCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showDescriptionCheckBox_actionPerformed(e);
      }
    });
    jPanel14.setBorder(border17);
    jPanel36.setLayout(flowLayout9);
    _subtypeLabel.setText("Subtype:  ALL");
    _familyLabel.setPreferredSize(new Dimension(200, 17));
    _familyLabel.setText("Family:  ALL");
    _standardThresholdsRadioButton.setPreferredSize(new Dimension(75, 17));
    _standardThresholdsRadioButton.setSelected(true);
    _standardThresholdsRadioButton.setText("Standard");
    _standardThresholdsRadioButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        standardThresholdsRadioButton_actionPerformed(e);
      }
    });
    _advancedThresholdsRadioButton.setPreferredSize(new Dimension(78, 17));
    _advancedThresholdsRadioButton.setText("Advanced");
    _advancedThresholdsRadioButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        advancedThresholdsRadioButton_actionPerformed(e);
      }
    });
    jPanel50.setLayout(gridLayout4);
    gridLayout4.setRows(2);
    gridLayout4.setVgap(5);
    flowLayout8.setAlignment(FlowLayout.LEFT);
    flowLayout9.setAlignment(FlowLayout.LEFT);
    _toCompletionButton.setEnabled(false);
    _toCompletionButton.setText("Finish");
    jScrollPane6.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    jPanel110.setLayout(borderLayout9);
    _nextViewButton.setEnabled(false);
    _nextViewButton.setText("View");
    _stopButton.setEnabled(false);
    _stopButton.setFont(new java.awt.Font("Dialog", 1, 12));
    _stopButton.setText("Abort");
    _stopButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        stopButton_actionPerformed(e);
      }
    });
    jPanel311.setLayout(gridLayout9);
    jPanel311.setBorder(border4);

    _nextMeasurementButton.setEnabled(false);
    _nextMeasurementButton.setDisabledIcon(null);
    _nextMeasurementButton.setText("Meas.");
    gridLayout9.setColumns(6);
    gridLayout9.setHgap(3);
    _nextJointButton.setEnabled(false);
    _nextJointButton.setText("Joint");
    _pauseButton.setEnabled(false);
    _pauseButton.setText("Pause");
    _pauseButton.setIcon(pauseImage);
    _pauseButton.setDisabledIcon(pauseDisabledImage);
    _messagesTextArea1.setText("Fillet Length should be set for proper operator.\n");
    _messagesTextArea1.setBackground(SystemColor.info);
    _messagesTextArea1.setEditable(false);
    jPanel111.setLayout(borderLayout27);
    _messagesTextArea2.setText("");

    _messagesTextArea2.setEditable(false);
    _feedbackCheckBox.setMnemonic('0');
    _feedbackCheckBox.setNextFocusableComponent(_jointLearningCheckBox);
    _feedbackCheckBox.setText("Diagnostics");
    _feedbackCheckBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        withFeedback_actionPerformed(e);
      }
    });
    _subtypeLearningCheckBox.setText("Subtype Learning");
    _subtypeLearningCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
    _jointLearningCheckBox.setText("Joint Learning");
    _jointLearningCheckBox.setHorizontalAlignment(SwingConstants.RIGHT);
    _jointLearningCheckBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        withLearning_actionPerformed(e);
      }
    });
    _configureFeedbackButton.setText("Configure. . .");
    _configureFeedbackButton.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        _configureFeedbackButton_actionPerformed(e);
      }
    });
    //_runButton.setText("Run");
    //_stopButton1.setText("Abort");
    //_nextMeasurementButton1.setText("Meas.");
    //_nextJointButton1.setText("Joint");
    //_nextViewButton1.setText("View");
    //_toCompletionButton1.setText("Finish");
    jPanel54.setLayout(borderLayout30);
    jLabel19.setText("Pin:");
    jLabel20.setText("View:");
    jLabel21.setText("Subtype:");
    jLabel22.setText("Component:");
    jLabel23.setText("Algorithm:");
    jLabel24.setText("Family:");
    jPanel55.setLayout(gridLayout5);
    gridLayout5.setRows(2);
    gridLayout5.setColumns(3);
    jPanel55.setBackground(SystemColor.info);
    jPanel55.setBorder(border20);
    _nextJointButton1.setEnabled(false);
    _nextJointButton1.setToolTipText("Run to next pin");
    _nextJointButton1.setIcon(runToJointImage);
    _nextJointButton1.setDisabledIcon(runToJointDisabledImage);
    _pauseButton1.setEnabled(false);
    _pauseButton1.setToolTipText("Pause");
    _pauseButton1.setIcon(pauseImage);
    _pauseButton1.setDisabledIcon(pauseDisabledImage);
    _toCompletionButton1.setEnabled(false);
    _toCompletionButton1.setToolTipText("Run to completion");
    _toCompletionButton1.setIcon(runToEndImage);
    _toCompletionButton1.setDisabledIcon(runToEndDisabledImage);
    _nextViewButton1.setEnabled(false);
    _nextViewButton1.setToolTipText("Run to next view");
    _nextViewButton1.setDisabledIcon(runToViewDisabledImage);
    _nextViewButton1.setIcon(runToViewImage);
    _runButton.setPreferredSize(new Dimension(103, 27));
    _runButton.setToolTipText("Start test");
    _runButton.setIcon(runImage);
    _runButton.setDisabledIcon(runDisabledImage);
    _runButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        runButton_actionPerformed(e);
      }
    });
    _nextMeasurementButton1.setEnabled(false);
    _nextMeasurementButton1.setToolTipText("Run to break");
    _nextMeasurementButton1.setDisabledIcon(null);
    _nextMeasurementButton1.setIcon(runToBreakImage);
    _nextMeasurementButton1.setDisabledIcon(runToBreakDisabledImage);
    jPanel35.setLayout(borderLayout17);
    _stopButton1.setEnabled(false);
    _stopButton1.setFont(new java.awt.Font("Dialog", 1, 12));
    _stopButton1.setToolTipText("Stop");
    _stopButton1.setIcon(stopImage);
    _stopButton1.setDisabledIcon(stopDisabledImage);
    _stopButton1.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        stopButton_actionPerformed(e);
      }
    });
    _familySubtypeToggleButton.setText("Family/Subtype");
    _familySubtypeToggleButton.setSelected(true);
    _familySubtypeToggleButton.setToolTipText("Switch to Family/Subtype Mode");
    _familySubtypeToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        familySubtypeToggleButton_actionPerformed(e);
      }
    });
    _devicePinToggleButton.setText("Device/Pin");
    _devicePinToggleButton.setToolTipText("Switch to Device/Pin Mode");
    _devicePinToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        devicePinToggleButton_actionPerformed(e);
      }
    });
    _boardViewToggleButton.setText("Board/View");
    _boardViewToggleButton.setToolTipText("Switch to Board/View Mode");
    _boardViewToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        boardViewToggleButton_actionPerformed(e);
      }
    });
    jPanel29.setLayout(borderLayout33);
    jPanel34.setLayout(flowLayout13);
    jPanel30.setLayout(borderLayout26);
    jLabel18.setText("To:");
    jLabel17.setPreferredSize(new Dimension(55, 17));
    jLabel16.setText("Views:");
    jLabel16.setPreferredSize(new Dimension(55, 17));
    _viewToTextField1.setEnabled(false);
    _viewToTextField1.setText("78");
    _viewToTextField1.setPreferredSize(new Dimension(80, 21));
    jPanel47.setLayout(gridLayout10);
    _allViewsRadioButton1.setSelected(true);
    _allViewsRadioButton1.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        _allViewsRadioButton1_actionPerformed(e);
      }
    });
    _allViewsRadioButton1.setText("All");
    flowLayout19.setAlignment(FlowLayout.LEFT);
    flowLayout18.setAlignment(FlowLayout.LEFT);
    flowLayout13.setAlignment(FlowLayout.LEFT);
    jLabel8.setPreferredSize(new Dimension(55, 17));
    jLabel8.setText("Board:");
    _viewFromRadioButton1.setToolTipText("");
    _viewFromRadioButton1.setText("From");
    _viewFromRadioButton1.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        _viewFromRadioButton1_actionPerformed(e);
      }
    });
    jPanel211.setLayout(flowLayout19);
    borderLayout26.setHgap(10);
    jPanel210.setLayout(flowLayout18);
    gridLayout10.setRows(2);
    _viewFromTextField1.setMinimumSize(new Dimension(4, 60));
    _viewFromTextField1.setPreferredSize(new Dimension(80, 21));
    _viewFromTextField1.setText("1");
    _viewFromTextField1.setEnabled(false);
    _boardComboBox.setPreferredSize(new Dimension(125, 21));
    jPanel8.setLayout(flowLayout4);
    jPanel7.setLayout(flowLayout2);
    jPanel6.setLayout(borderLayout20);
    jLabel14.setText("Subtype Comment:");
    jLabel14.setVerticalAlignment(SwingConstants.BOTTOM);
    jLabel14.setVerticalTextPosition(SwingConstants.BOTTOM);
    jPanel43.setLayout(borderLayout22);
    jPanel43.setBorder(border15);
    flowLayout4.setAlignment(FlowLayout.LEFT);
    jPanel42.setLayout(borderLayout21);
    jPanel42.setBorder(border14);
    jPanel41.setLayout(gridLayout7);
    flowLayout2.setAlignment(FlowLayout.LEFT);
    jPanel40.setLayout(gridLayout1);
    gridLayout7.setRows(2);
    gridLayout7.setColumns(1);
    _subtypeComboBox.setPreferredSize(new Dimension(100, 21));
    _subtypeComboBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        subtypeComboBox_actionPerformed(e);
      }
    });
    _familyComboBox.setPreferredSize(new Dimension(100, 21));
    _familyComboBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        familyComboBox_actionPerformed(e);
      }
    });
    gridLayout1.setRows(2);
    gridLayout1.setColumns(1);
    jTextField2.setPreferredSize(new Dimension(100, 20));
    jLabel3.setPreferredSize(new Dimension(55, 17));
    jLabel3.setText("Subtype:");
    jLabel1.setPreferredSize(new Dimension(55, 17));
    jLabel1.setText("Family:");
    borderLayout21.setHgap(5);
    borderLayout19.setHgap(5);
    _testModeSelectComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        testModeSelectComboBox_actionPerformed(e);
      }
    });
    _testModeSelectComboBox.setPreferredSize(new Dimension(130, 21));
    _testModeSelectComboBox.setToolTipText("Test type selection");
    jLabel25.setText("Threshold Comment:");
    jPanel56.setLayout(borderLayout31);
    jPanel56.setBorder(border21);
    borderLayout31.setHgap(5);
    jPanel57.setLayout(borderLayout32);
    jPanel19.setLayout(borderLayout25);
    _moreInfoButton.setText("More Info . . .");
    _moreInfoButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        moreInfoButton_actionPerformed(e);
      }
    });
    jPanel20.setLayout(borderLayout5);
    _showDescToggleButton.setText("Show Description");
    _showDescToggleButton.setSelected(true);
    _showDescToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showDescToggleButton_actionPerformed(e);
      }
    });
    jPanel58.setLayout(borderLayout24);
    jScrollPane5.setBorder(null);
    jScrollPane5.setPreferredSize(new Dimension(300, 180));
    jScrollPane5.setToolTipText("");
    jPanel44.setLayout(borderLayout23);
    jEditorPane1.setContentType("text/html");
    jEditorPane1.setBackground(SystemColor.info);
    jEditorPane1.setPage(AlgoTunerFrame.class.getResource("nothresh.html"));
    jLabel12.setIcon(null);
    jLabel12.setIcon(jointImage);
    flowLayout5.setAlignment(FlowLayout.RIGHT);
    jPanel15.setLayout(flowLayout5);
    jPanel15.setBackground(SystemColor.info);
    jPanel58.setBorder(border22);
    flowLayout11.setAlignment(FlowLayout.LEFT);
    jPanel33.setLayout(flowLayout11);
    _deviceComboBox.setPreferredSize(new Dimension(125, 21));
    jLabel6.setPreferredSize(new Dimension(55, 17));
    jLabel6.setText("Device:");
    jPanel32.setLayout(flowLayout12);
    _pinComboBox.setPreferredSize(new Dimension(125, 21));
    jLabel7.setPreferredSize(new Dimension(55, 17));
    jLabel7.setText("Pin:");
    flowLayout12.setAlignment(FlowLayout.LEFT);
    jPanel59.setLayout(gridLayout11);
    gridLayout11.setRows(2);
    jPanel412.setLayout(gridLayout12);
    gridLayout12.setRows(2);
    gridLayout12.setColumns(1);
    jLabel28.setText("Family:  FPGullwing");
    jLabel27.setText("Subtype: SOIC.Top");
    jPanel61.setLayout(flowLayout14);
    flowLayout14.setAlignment(FlowLayout.LEFT);
    jPanel60.setLayout(flowLayout15);
    flowLayout15.setAlignment(FlowLayout.LEFT);
    borderLayout33.setHgap(10);
    _saveDiagnosticImageMenuItem.setText("Save Diagnostic Image . . .");
    menuFile.add(_saveDiagnosticImageMenuItem);
    menuFile.add(menuFileExit);
    menuHelp.add(menuHelpAbout);
    menuBar1.add(menuFile);
    menuBar1.add(_actionsMenu);
    menuBar1.add(_displayMenu);
    menuBar1.add(_optionsMenu);
    menuBar1.add(menuHelp);
    this.setJMenuBar(menuBar1);
    this.getContentPane().add(jPanel2, BorderLayout.CENTER);
    jPanel2.add(jPanel3, BorderLayout.CENTER);
    jPanel3.add(jPanel1, BorderLayout.NORTH);
    jPanel1.add(jPanel18, BorderLayout.NORTH);
    jPanel18.add(jPanel6, BorderLayout.CENTER);
    jPanel6.add(jPanel40, BorderLayout.WEST);
    jPanel40.add(jPanel7, null);
    jPanel7.add(jLabel1, null);
    jPanel7.add(_familyComboBox, null);
    jPanel40.add(jPanel8, null);
    jPanel8.add(jLabel3, null);
    jPanel8.add(_subtypeComboBox, null);
    jPanel6.add(jPanel41, BorderLayout.CENTER);
    jPanel41.add(jPanel43, null);
    jPanel43.add(jLabel14, BorderLayout.WEST);
    jPanel41.add(jPanel42, null);
    jPanel42.add(jTextField2, BorderLayout.CENTER);
//    jPanel6.add(jPanel9, null);
    jPanel9.add(jLabel2, null);
    jPanel9.add(_algorithmComboBox1, null);
    //_runSelectionsTabbedPane.add(jPanel13, "Views");
    jPanel13.add(jPanel23, null);
    jPanel23.add(jLabel5, null);
    jPanel23.add(_allViewsRadioButton, null);
    jPanel13.add(jPanel26, null);

    jPanel26.add(jLabel9, null);
    jPanel26.add(_viewFromRadioButton, null);

    jPanel26.add(_viewFromTextField, null);
    jPanel26.add(jLabel10, null);
    jPanel26.add(_viewToTextField, null);

    //jPanel1.add(jPanel4, BorderLayout.SOUTH);
    jPanel4.add(jPanel5, BorderLayout.CENTER);
    jPanel4.add(jPanel10, BorderLayout.SOUTH);
    jPanel10.add(jLabel4, BorderLayout.WEST);
    //jPanel10.add(_runDescriptionTextField, BorderLayout.CENTER);
    jPanel4.add(jPanel27, BorderLayout.NORTH);
    jPanel27.add(_testToRunLabel, null);
    jPanel1.add(jPanel28, BorderLayout.CENTER);
    jPanel28.add(jPanel31, BorderLayout.CENTER);
    jPanel31.add(jPanel39, BorderLayout.NORTH);
    jPanel39.add(jPanel49, BorderLayout.WEST);
    jPanel49.add(_feedbackCheckBox, null);
    jPanel49.add(_subtypeLearningCheckBox, null);
    jPanel49.add(_jointLearningCheckBox, null);
    jPanel39.add(jPanel51, BorderLayout.EAST);
    jPanel51.add(_configureFeedbackButton, null);
    jPanel31.add(jPanel38, BorderLayout.SOUTH);
    jPanel38.add(jLabel11, BorderLayout.WEST);
    jPanel38.add(_runDescriptionTextField, BorderLayout.CENTER);
    jPanel28.add(jPanel54, BorderLayout.SOUTH);
    jPanel54.add(jToolBar1, BorderLayout.NORTH);
    jToolBar1.add(jPanel35, null);
    jPanel35.add(jPanel52, BorderLayout.WEST);
    jPanel52.add(_runButton, null);
    jPanel52.add(_pauseButton1, null);
    jPanel52.add(new JLabel("   "));
    jPanel53.add(_stopButton1, null);
    jPanel35.add(jPanel53, BorderLayout.EAST);
    jPanel52.add(_nextMeasurementButton1, null);
    jPanel52.add(_nextJointButton1, null);
    jPanel52.add(_nextViewButton1, null);
    jPanel52.add(_toCompletionButton1, null);
    jPanel5.add(jPanel11, null);
    jPanel11.add(jPanel16, null);
//    jPanel16.add(_feedbackCheckBox, null);
    jPanel16.add(jPanel22, null);
    //jPanel22.add(_subtypeLearningCheckBox, BorderLayout.CENTER);
    //jPanel22.add(_jointLearningCheckBox, BorderLayout.SOUTH);
    //jPanel22.add(_feedbackCheckBox, BorderLayout.NORTH);
    //jPanel16.add(_configureFeedbackButton, null);
    jPanel3.add(jPanel37, BorderLayout.CENTER);
    jPanel37.add(jSplitPane2, BorderLayout.CENTER);
    jSplitPane2.add(jPanel12, JSplitPane.BOTTOM);
    jPanel12.add(jTabbedPane1, BorderLayout.CENTER);
//    jPanel311.add(jPanel310, null);
//    jTabbedPane2.add(jPanel17, "Review Defects");
    jPanel17.add(jLabel13, BorderLayout.NORTH);
    jPanel17.add(jList1, BorderLayout.CENTER);
    jPanel17.add(jPanel21, BorderLayout.EAST);
    jPanel21.add(jButton5, null);
    jPanel21.add(jButton8, null);
    jPanel21.add(jButton7, null);
    jPanel21.add(jButton6, null);
    jPanel21.add(jButton4, null);
    jTabbedPane1.add(jScrollPane1, "Standard");
    jScrollPane1.getViewport().add(jTable1, null);
    jTabbedPane1.add(jScrollPane2, "Advanced");
    jPanel12.add(jPanel14, BorderLayout.NORTH);
    jPanel14.add(jPanel25, BorderLayout.EAST);
    jPanel25.add(jPanel36, BorderLayout.EAST);
    //jPanel36.add(_showDescriptionCheckBox, null);
    jPanel14.add(jPanel24, BorderLayout.CENTER);
    jPanel24.add(_standardThresholdsRadioButton, null);
    jPanel24.add(_advancedThresholdsRadioButton, null);
    jPanel14.add(jPanel50, BorderLayout.WEST);
    jPanel50.add(_familyLabel, null);
    jPanel50.add(_subtypeLabel, null);
    jPanel12.add(jPanel57, BorderLayout.SOUTH);
    jPanel57.add(jPanel56, BorderLayout.NORTH);
    jPanel56.add(jLabel25, BorderLayout.WEST);
    jPanel56.add(jTextField4, BorderLayout.CENTER);
    jPanel57.add(jPanel20, BorderLayout.SOUTH);
    jPanel20.add(jPanel19, BorderLayout.SOUTH);
    jPanel19.add(jPanel46, BorderLayout.NORTH);
    jPanel46.add(_moreInfoButton, null);
    jPanel20.add(jPanel58, BorderLayout.CENTER);
    jPanel58.add(jPanel44, BorderLayout.CENTER);
    jPanel44.add(jScrollPane5, BorderLayout.CENTER);
    jPanel58.add(jPanel15, BorderLayout.EAST);
    jPanel15.add(jLabel12, null);
    jScrollPane5.getViewport().add(jEditorPane1, null);
    jPanel57.add(jPanel45, BorderLayout.CENTER);
    jPanel45.add(_showDescToggleButton, null);
    jSplitPane2.add(jPanel111, JSplitPane.TOP);
    jPanel111.add(jScrollPane7, BorderLayout.CENTER);
    jPanel111.add(jPanel55, BorderLayout.NORTH);
    jPanel55.add(jLabel24, null);
    jPanel55.add(jLabel23, null);
    jPanel55.add(jLabel22, null);
    jPanel55.add(jLabel21, null);
    jPanel55.add(jLabel20, null);
    jPanel55.add(jLabel19, null);
    jScrollPane7.getViewport().add(_messagesTextArea2, null);
    jScrollPane2.getViewport().add(jTable2, null);
    this.getContentPane().add(toolBar, BorderLayout.NORTH);

    toolBar.addSeparator();
    toolBar.add(_writeLearnedDataButton, null);
    toolBar.add(_deleteLearnedDataButton, null);
    toolBar.addSeparator();
    toolBar.add(_copyThresholdButton, null);
    toolBar.addSeparator();

    //toolBar.add(_familySubtypeToggleButton, null);
    //toolBar.add(_devicePinToggleButton, null);
    //toolBar.add(_boardViewToggleButton, null);

    toolBar.add(_testModeSelectComboBox, null);

    _testModeSelectComboBox.addItem("Family/Subtype");
    _testModeSelectComboBox.addItem("Device/Pin");
    _testModeSelectComboBox.addItem("Board/Views");

    _testModeSelectComboBox.setMaximumSize(_testModeSelectComboBox.getPreferredSize());

    this.getContentPane().add(_statusPanel, BorderLayout.SOUTH);
    _statusPanel.add(statusBar, null);
    jPanel2.setPreferredSize(new Dimension(540, 1000));  // for 1600x1200
    //jPanel2.setPreferredSize(new Dimension(560, 660));  // for 1024x768

    ButtonGroup stateButtonGroup = new ButtonGroup();
    stateButtonGroup.add(_familySubtypeToggleButton);
    stateButtonGroup.add(_devicePinToggleButton);
    stateButtonGroup.add(_boardViewToggleButton);

    ButtonGroup viewButtonGroup = new ButtonGroup();
    viewButtonGroup.add(_allViewsRadioButton1);
    viewButtonGroup.add(_viewFromRadioButton1);
    _optionsMenu.add(_configurationSettings);
    _optionsMenu.add(_withFeedbackCheckBoxMenuItem2);
    _optionsMenu.add(_withLearningCheckBoxMenuItem3);
    _optionsMenu.add(_generateDiagnosticsMeasurementsCheckBoxMenuItem1);
    _actionsMenu.add(_runMenuItem);
    _actionsMenu.add(_nextMeasurementMenuItem);
    _actionsMenu.add(_nextJointMenuItem);
    _actionsMenu.add(_nextViewMenuItem);
    _actionsMenu.add(_toCompletionMenuItem);
    _actionsMenu.add(_pauseMenuItem);
    _actionsMenu.add(_abortMenuItem);
    _allViewsRadioButton.setSelected(true);
    _viewFromTextField.setEnabled(false);

    _viewToTextField.setEnabled(false);

    setResizable(true);

    ButtonGroup thresholdsButtonGroup = new ButtonGroup();
    thresholdsButtonGroup.add(_standardThresholdsRadioButton);
    thresholdsButtonGroup.add(_advancedThresholdsRadioButton);
    _displayMenu.add(_displayCADStatusMenuItem);
    _displayMenu.add(_displayMachineStatusMenuItem);
    jTabbedPane2.add(jPanel110, "Run Control");
    jPanel110.add(jScrollPane6, BorderLayout.CENTER);
    jPanel110.add(jPanel311, BorderLayout.SOUTH);
    jPanel311.add(_nextMeasurementButton, null);
    jPanel311.add(_nextJointButton, null);
    jPanel311.add(_nextViewButton, null);
    jPanel311.add(_toCompletionButton, null);
    jPanel311.add(_pauseButton, null);
    jPanel311.add(_stopButton, null);
    jScrollPane6.getViewport().add(_messagesTextArea1, null);
    _threshDescDlg = new ThresholdDescDlg(this,"Threshold Description", false);
    _threshDescDlg.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    _threshDescDlg.addWindowListener(new WindowAdapter()
    {
      public void windowClosing(WindowEvent e)
      {
        _showDescriptionCheckBox.setSelected(false);
        _threshDescDlg.setVisible(false);
      }
    });
    jPanel29.add(jPanel59, BorderLayout.WEST);
    jPanel59.add(jPanel33, null);
    jPanel33.add(jLabel6, null);
    jPanel33.add(_deviceComboBox, null);
    jPanel59.add(jPanel32, null);
    jPanel32.add(jLabel7, null);
    jPanel32.add(_pinComboBox, null);
    jPanel29.add(jPanel412, BorderLayout.CENTER);
    jPanel412.add(jPanel61, null);
    jPanel61.add(jLabel28, null);
    jPanel412.add(jPanel60, null);
    jPanel60.add(jLabel27, null);
    jPanel29.setPreferredSize(new Dimension(285, 70));
    jPanel30.add(jPanel34, BorderLayout.WEST);
    jPanel34.add(jLabel8, null);
    jPanel34.add(_boardComboBox, null);
    jPanel30.add(jPanel47, BorderLayout.CENTER);
    jPanel47.add(jPanel210, null);
    jPanel210.add(jLabel16, null);
    jPanel210.add(_allViewsRadioButton1, null);
    jPanel47.add(jPanel211, null);
    jPanel211.add(jLabel17, null);
    jPanel211.add(_viewFromRadioButton1, null);
    jPanel211.add(_viewFromTextField1, null);
    jPanel211.add(jLabel18, null);
    jPanel211.add(_viewToTextField1, null);
  }

  public void setCustomizations()
  {
//    jTable1.getColumnModel().getColumn(0).setPreferredWidth(200);
//    jTable1.sizeColumnsToFit(-1);
//    jTable2.getColumnModel().getColumn(0).setPreferredWidth(200);
//    jTable2.sizeColumnsToFit(-1);
    populateComboBoxes();
    setupAlgorithmTables(true);



    //jSplitPane2.setDividerLocation(.60);
//    jEditorPane1.setEditable(true);
    jEditorPane1.setEditable(false);
  }

  public void setDividerLocation()
  {
    jSplitPane2.setDividerLocation(.34);  // for 1600x1200
    //jSplitPane2.setDividerLocation(.49);  // for 1024x768

    //Dimension dlgSize = _threshDescDlg.getPreferredSize();
    //Dimension frmSize = getSize();
    //Point loc = getLocation();
    //_threshDescDlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x + 100, (frmSize.height - dlgSize.height) / 2 + loc.y);
    //_threshDescDlg.setVisible(true);
  }

  // takes the algos specified by the family and adds tabs/tables to the correct panel
  private void setupAlgorithmTables(boolean standard)
  {
    // clear out all the tabs first
    _standardThresholdsRadioButton.setEnabled(true);
    _advancedThresholdsRadioButton.setEnabled(true);
    jTabbedPane1.removeAll();
    String currentFamily = (String)_familyComboBox.getSelectedItem();
    String currentSubtype = (String)_subtypeComboBox.getSelectedItem();
    _familyLabel.setText("Family:  " + currentFamily);
    _subtypeLabel.setText("Subtype:  " + currentSubtype);
    _testToRunLabel.setText("Family:  " + currentFamily + "   Subtype:  " + currentSubtype );
    _runDescriptionTextField.setText("Family:  " + currentFamily + "   Subtype:  " + currentSubtype );
    //titledBorder1.setTitle("Family:  " + currentFamily + "   Subtype:  " + currentSubtype );
    if ((currentFamily == null) || (currentFamily.equalsIgnoreCase("All")) || (currentSubtype == null) || (currentSubtype.equalsIgnoreCase("All")))
    {
      _standardThresholdsRadioButton.setEnabled(false);
      _advancedThresholdsRadioButton.setEnabled(false);
      JLabel label = new JLabel("Must specify both Family and Subtype.");
      JPanel panel = new JPanel();
      panel.add(label);
      jTabbedPane1.add(panel, "No thresholds");
      _threshDescDlg.setInfo(AlgoTunerFrame.class.getResource("nothresh.html"), null);
      try {jEditorPane1.setPage(AlgoTunerFrame.class.getResource("nothresh.html"));}
      catch (IOException ioe){}
      jLabel12.setIcon(null);
      _moreInfoButton.setEnabled(false);

      return;
    }

    _threshDescDlg.setInfo(AlgoTunerFrame.class.getResource("fpgullwingnoi3.html"), jointImage);
    try {jEditorPane1.setPage(AlgoTunerFrame.class.getResource("fpgullwingnoi3.html"));}
    catch (IOException ioe){}
      //jEditorPane1.setPage(AlgoTunerFrame.class.getResource("helptest.html"));
    jLabel12.setIcon(jointImage);
    _moreInfoButton.setEnabled(true);

    String[] algos = Algorithms.getAlgorithmsForFamily(currentFamily);
    for (int i = 0; i < algos.length; i++)
    {
      JTable tt;
      JScrollPane jsp = new JScrollPane();
      jTabbedPane1.add(jsp, algos[i]);
      if (standard)
        tt = new JTable(_standardThresholdModel);
      else
      {
        tt = new JTable(_advancedThresholdModel);
        tt.setBackground(Color.yellow);
      }

      jsp.getViewport().add(tt);//, null);
      if (standard)
        setupStandardTableRenderer(tt);
      else
        setupAdvancedTableRenderer(tt);
//      jTabbedPane1.add(jScrollPane2, "test2");
//      jScrollPane1.getViewport().add(jTable2, null);
    }

  }

  private void setupStandardTableRenderer(JTable jTable1)
  {
    jTable1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    jTable1.getColumnModel().getColumn(0).setPreferredWidth(200);
    jTable1.sizeColumnsToFit(-1);

    // add the listener for a table row selection
//    ListSelectionModel rowSM = jTable1.getSelectionModel();
//    rowSM.addListSelectionListener(new ListSelectionListener()
//    {
//      public void valueChanged(ListSelectionEvent e)
//      {
//	//System.out.println("Table Row changed");
//	ListSelectionModel lsm = (ListSelectionModel)e.getSource();
//	if (lsm.isSelectionEmpty())
//	{
//	}
//	else
//	{
//	  int selectedRow = lsm.getMinSelectionIndex();
          //jTable1.editCellAt(selectedRow, 1);
//	}
//      }
//    });

    final IntegerNumberField integerField = new IntegerNumberField(0, 5);
    integerField.setHorizontalAlignment(IntegerNumberField.CENTER);
    DefaultCellEditor integerEditor = new DefaultCellEditor(integerField)
    {
      public Object getCellEditorValue()
      {
        return new Integer(integerField.getValue());
      }
    };

    integerEditor.setClickCountToStart(1);
    integerField.addFocusListener(new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        //System.out.println("float focus gained");
        integerField.selectAll();
      }
    });

    final FloatNumberField floatField = new FloatNumberField(0, 5);
    floatField.setHorizontalAlignment(FloatNumberField.CENTER);
    DefaultCellEditor floatEditor = new DefaultCellEditor(floatField)
    {
      public Object getCellEditorValue()
      {
        return new Float(floatField.getValue());
      }
    };

    floatEditor.setClickCountToStart(1);

    floatField.addFocusListener(new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        //System.out.println("float focus gained");
        floatField.selectAll();
      }
    });

    JCheckBox boolBox = new JCheckBox();
    JComboBox toeSearchTechiqueComboBox = new JComboBox();
    toeSearchTechiqueComboBox.addItem("Thickness");
    toeSearchTechiqueComboBox.addItem("Side Fillet");
    toeSearchTechiqueComboBox.addItem("Avg Toe/Side");

    JComboBox heelSearchTechniqueComboBox = new JComboBox();
    heelSearchTechniqueComboBox.addItem("Profile Start");
    heelSearchTechniqueComboBox.addItem("Cad Position");

    JComboBox useLearnedDataComboBox = new JComboBox();
    useLearnedDataComboBox.addItem("Yes");
    useLearnedDataComboBox.addItem("No");

    CustomThresholdEditor rowEditor = new CustomThresholdEditor(jTable1);
    rowEditor.setEditorAt(0, new DefaultCellEditor(heelSearchTechniqueComboBox));
    rowEditor.setEditorAt(1, floatEditor);
    rowEditor.setEditorAt(2, floatEditor);
    rowEditor.setEditorAt(3, floatEditor);
    rowEditor.setEditorAt(4, floatEditor);
    rowEditor.setEditorAt(5, new DefaultCellEditor(useLearnedDataComboBox));
    rowEditor.setEditorAt(6, new DefaultCellEditor(toeSearchTechiqueComboBox));
    jTable1.getColumn("Value").setCellEditor(rowEditor);

    jTable1.setDefaultRenderer(Object.class, new CustomThresholdRenderer());
  }

  private void setupAdvancedTableRenderer(JTable jTable1)
  {
    jTable1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    jTable1.getColumnModel().getColumn(0).setPreferredWidth(200);
    jTable1.sizeColumnsToFit(-1);

    // add the listener for a table row selection
    ListSelectionModel rowSM = jTable1.getSelectionModel();
    rowSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        //System.out.println("Table Row changed");
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        if (lsm.isSelectionEmpty())
        {
        }
        else
        {
          int selectedRow = lsm.getMinSelectionIndex();
          //jTable1.editCellAt(selectedRow, 1);
        }
      }
    });

    final IntegerNumberField integerField = new IntegerNumberField(0, 5);
    integerField.setHorizontalAlignment(IntegerNumberField.CENTER);
    DefaultCellEditor integerEditor = new DefaultCellEditor(integerField)
    {
      public Object getCellEditorValue()
      {
        return new Integer(integerField.getValue());
      }
    };

    integerEditor.setClickCountToStart(1);
    integerField.addFocusListener(new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        //System.out.println("float focus gained");
        integerField.selectAll();
      }
    });

    final FloatNumberField floatField = new FloatNumberField(0, 5);
    floatField.setHorizontalAlignment(FloatNumberField.CENTER);
    DefaultCellEditor floatEditor = new DefaultCellEditor(floatField)
    {
      public Object getCellEditorValue()
      {
        return new Float(floatField.getValue());
      }
    };

    floatEditor.setClickCountToStart(1);

    floatField.addFocusListener(new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        //System.out.println("float focus gained");
        floatField.selectAll();
      }
    });

//    JCheckBox boolBox = new JCheckBox();
//    JComboBox toeSearchTechiqueComboBox = new JComboBox();
//    toeSearchTechiqueComboBox.addItem("Thickness");
//    toeSearchTechiqueComboBox.addItem("Side Fillet");
//    toeSearchTechiqueComboBox.addItem("Avg Toe/Side");
//
//    JComboBox heelSearchTechniqueComboBox = new JComboBox();
//    heelSearchTechniqueComboBox.addItem("Profile Start");
//    heelSearchTechniqueComboBox.addItem("Cad Position");
//
//    JComboBox omitLearnedDataComboBox = new JComboBox();
//    omitLearnedDataComboBox.addItem(new JCheckBox("Yes"));
//    omitLearnedDataComboBox.addItem(new JCheckBox("No"));

    CustomThresholdEditor rowEditor = new CustomThresholdEditor(jTable1);
    rowEditor.setEditorAt(0, integerEditor);
    rowEditor.setEditorAt(1, integerEditor);
    rowEditor.setEditorAt(2, integerEditor);
    rowEditor.setEditorAt(3, integerEditor);
    rowEditor.setEditorAt(4, new ButtonEditor(new JCheckBox(), this));
    rowEditor.setEditorAt(5, integerEditor);


    jTable1.getColumn("Value").setCellEditor(rowEditor);

    jTable1.setDefaultRenderer(Object.class, new CustomThresholdRenderer());

    CustomThresholdRenderer multRenderer = new CustomThresholdRenderer();
    ButtonRenderer buttonRenderer = new ButtonRenderer();
    multRenderer.add(4, buttonRenderer );
    jTable1.getColumn("Value").setCellRenderer(multRenderer);

  }

  private void populateComboBoxes()
  {
    String[] families = FamilyDataset.getFamilies();
    for (int i = 0; i < families.length; i++)
      _familyComboBox.addItem(families[i]);
    _familyComboBox.setSelectedIndex(0);

    String[] algos = Algorithms.getAlgorithmsForFamily((String)_familyComboBox.getSelectedItem());

//    _subtypeComboBox.addItem("All");

    _deviceComboBox.addItem("C1");
    _deviceComboBox.addItem("C2");
    _deviceComboBox.addItem("R1");
    _deviceComboBox.addItem("R2");
    _deviceComboBox.addItem("R3");
    _deviceComboBox.addItem("U1");
    _deviceComboBox.addItem("U2");

    _pinComboBox.addItem("All");
    _pinComboBox.addItem("1");
    _pinComboBox.addItem("2");
    _pinComboBox.addItem("3");
    _pinComboBox.addItem("4");
    _pinComboBox.addItem("5");
    _pinComboBox.addItem("6");

    _boardComboBox.addItem("All");
    _boardComboBox.addItem("1");
    _boardComboBox.addItem("2");
    _boardComboBox.addItem("3");
  }

  private void resizeTablePanel()
  {
//    System.out.println("--------");
//    System.out.println("Whole GUI: " + jPanel2.getSize().height);
//    System.out.println("JTabbedPane of table:" + jTabbedPane1.getSize().height);
//    System.out.println("JPane37:" + jPanel37.getSize().height);
//    System.out.println("JPane12:" + jPanel12.getSize().height);
//    System.out.println("JPane1:" + jPanel1.getSize().height);
//    System.out.println("JPane3:" + jPanel3.getSize().height);
//    System.out.println(jPanel1.getSize().height);

//    jScrollPane1.setPreferredSize(new Dimension(0, jPanel12.getSize().height));// - jPanel1.getSize().height));
  }

  //File | Exit action performed
  public void fileExit_actionPerformed(ActionEvent e)
  {
    System.exit(0);
  }

  //Help | About action performed
  public void helpAbout_actionPerformed(ActionEvent e)
  {
    AlgoTunerFrame_AboutBox dlg = new AlgoTunerFrame_AboutBox(this);
    Dimension dlgSize = dlg.getPreferredSize();
    Dimension frmSize = getSize();
    Point loc = getLocation();
    dlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
    dlg.setModal(true);
    dlg.show();
  }

  //Overridden so we can exit on System Close
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    if(e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      fileExit_actionPerformed(null);
    }
  }

  void _allViewsRadioButton_actionPerformed(ActionEvent e)
  {
    _viewFromTextField.setEnabled(false);
    _viewToTextField.setEnabled(false);
  }

  void _viewFromRadioButton_actionPerformed(ActionEvent e)
  {
    _viewFromTextField.setEnabled(true);
    _viewFromTextField.setBackground(Color.white);
    _viewToTextField.setEnabled(true);
    _viewToTextField.setBackground(Color.white);

  }

  void runButton_actionPerformed(ActionEvent e)
  {
    _runButton.setEnabled(false);

    // enable the run control buttons
    setControlsTo(true);

    jLabel19.setText("Pin:  34");
    jLabel20.setText("View:  29");
    jLabel21.setText("Subtype:  " + (String)_subtypeComboBox.getSelectedItem());
    jLabel22.setText("Component:  U12");
    jLabel23.setText("Algorithm:  LOCATOR");
    jLabel24.setText("Family:  " + (String)_familyComboBox.getSelectedItem());

    _messagesTextArea2.append("---------- FPGullwing SPC ----------\n");
    _messagesTextArea2.append("Using the average of background profiles from both sides.\n");
    _messagesTextArea2.append("This plot shows the location of maximum heel thickness.\n");
    _messagesTextArea2.append("HEEL_TECHNIQUE: (1) Using fillet edge for heel search reference point.\n");
    _messagesTextArea2.append("This plot shows the region used for heel search.\n");
    _messagesTextArea2.append("This plot shows the region used for heel measurements.\n");
    _messagesTextArea2.append("---------- FPGullwing SPC ---------\n");
    _messagesTextArea2.append("---------- FPGullwing SPC ----------\n");
    _messagesTextArea2.append("Using the average of background profiles from both sides.\n");
    _messagesTextArea2.append("This plot shows the location of maximum heel thickness.\n");
    _messagesTextArea2.append("HEEL_TECHNIQUE: (1) Using fillet edge for heel search reference point.\n");
    _messagesTextArea2.append("This plot shows the region used for heel search.\n");
    _messagesTextArea2.append("This plot shows the region used for heel measurements.\n");
    _messagesTextArea2.append("---------- FPGullwing SPC ----------\n");
    _messagesTextArea2.append("---------- FPGullwing SPC ----------\n");
    _messagesTextArea2.append("Using the average of background profiles from both sides.\n");
    _messagesTextArea2.append("This plot shows the location of maximum heel thickness.\n");
    _messagesTextArea2.append("HEEL_TECHNIQUE: (1) Using fillet edge for heel search reference point.\n");
    _messagesTextArea2.append("This plot shows the region used for heel search.\n");
    _messagesTextArea2.append("This plot shows the region used for heel measurements.\n");
    _messagesTextArea2.append("---------- FPGullwing SPC ----------\n");

    // make the messages tab the active tab
 //   _runSelectionsTabbedPane.setSelectedIndex(4);
    //jTabbedPane2.setSelectedIndex(1);
  }

  void stopButton_actionPerformed(ActionEvent e)
  {
    _runButton.setEnabled(true);

    // enable the run control buttons
    setControlsTo(false);

    jLabel19.setText("Pin:");
    jLabel20.setText("View:");
    jLabel21.setText("Subtype:");
    jLabel22.setText("Component:");
    jLabel23.setText("Algorithm:");
    jLabel24.setText("Family:");
  }

  private void setControlsTo(boolean testRunning)
  {
    // these controls
    _nextMeasurementButton1.setEnabled(testRunning);
    _nextJointButton1.setEnabled(testRunning);
    _nextViewButton1.setEnabled(testRunning);
    _toCompletionButton1.setEnabled(testRunning);
    _pauseButton1.setEnabled(testRunning);
    _stopButton1.setEnabled(testRunning);

    //_familyComboBox.setEnabled(!testRunning);
    //_subtypeComboBox.setEnabled(!testRunning);
    _feedbackCheckBox.setEnabled(!testRunning);
    _subtypeLearningCheckBox.setEnabled(!testRunning);
    _jointLearningCheckBox.setEnabled(!testRunning);
    _configureFeedbackButton.setEnabled(!testRunning);
    _runDescriptionTextField.setEnabled(!testRunning);

    _standardThresholdModel.setTestRunning(testRunning);
    _advancedThresholdModel.setTestRunning(testRunning);
  }

  void _configureFeedbackButton_actionPerformed(ActionEvent e)
  {
    AlgorithmConfigurationDlg dlg = new AlgorithmConfigurationDlg(this, "Algorithm Configuration", true);
    Dimension dlgSize = dlg.getPreferredSize();
    Dimension frmSize = getSize();
    Point loc = getLocation();
    dlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
    dlg.show();
  }

  void familyComboBox_actionPerformed(ActionEvent e)
  {
    String family = (String)_familyComboBox.getSelectedItem();

    //System.out.println("Family combo box action event with selection " + family);

//    _algorithmComboBox.removeAllItems();
    String[] algos = Algorithms.getAlgorithmsForFamily((String)_familyComboBox.getSelectedItem());
    setupAlgorithmTables(_standardThresholdsRadioButton.isSelected());

    _subtypeComboBox.removeAllItems();
    _subtypeComboBox.addItem("All");
    if (!family.equalsIgnoreCase("All"))
    {
      _subtypeComboBox.addItem("1");
      _subtypeComboBox.addItem("3");
      _subtypeComboBox.addItem("6");
      _subtypeComboBox.addItem("7");
      _subtypeComboBox.addItem("12");
      _subtypeComboBox.addItem("13");
      _subtypeComboBox.addItem("20");
      _subtypeComboBox.addItem("23");

    }

//    for (int i = 0; i < algos.length; i++)
//      _algorithmComboBox.addItem(algos[i]);
//    _algorithmComboBox.setSelectedIndex(0);
  }

  void _feedbackConfigurationsMenuItem_actionPerformed(ActionEvent e)
  {
    _configureFeedbackButton_actionPerformed(e);
  }

  void _algorithmOnOff_actionPerformed(ActionEvent e)
  {
    AlgorithmConfigurationDlg dlg = new AlgorithmConfigurationDlg();
    Dimension dlgSize = dlg.getPreferredSize();
    Dimension frmSize = getSize();
    Point loc = getLocation();
    dlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
    dlg.show();
  }

  void _learningSettings_actionPerformed(ActionEvent e)
  {
    LearningConfigurationDlg dlg = new LearningConfigurationDlg(this, "Learning Settings", true);
    Dimension dlgSize = dlg.getPreferredSize();
    Dimension frmSize = getSize();
    Point loc = getLocation();
    dlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
    dlg.show();
  }

  void _configurationSettings_actionPerformed(ActionEvent e)
  {
    _configureFeedbackButton_actionPerformed(e);
//    AlgorithmConfigurationDlg dlg = new AlgorithmConfigurationDlg();
//    Dimension dlgSize = dlg.getPreferredSize();
//    Dimension frmSize = getSize();
//    Point loc = getLocation();
//    dlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
//    dlg.show();
  }

  void _feedbackSettingsMenuItem_actionPerformed(ActionEvent e)
  {
     FeedbackSettingsDlg dlg = new FeedbackSettingsDlg(this, "Feedback Settings", true, _feedbackSettings);
     Dimension dlgSize = dlg.getPreferredSize();
     Dimension frmSize = getSize();
     Point loc = getLocation();
     dlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
     dlg.setModal(true);
     dlg.show();

  }

  FeedbackConfiguration getFeedbackConfiguration()
  {
    return _feedbackConfiguration;
  }

  void setFeedbackConfiguration(FeedbackConfiguration feedbackConfig)
  {
    _feedbackConfiguration = feedbackConfig;
  }

  FamilyAlgoPairing getLearningConfiguration()
  {
    return _learningConfiguration;
  }

  void setLearningConfiguration(FamilyAlgoPairing learningConfig)
  {
    _learningConfiguration = learningConfig;
  }

  FamilyAlgoPairing getOnOffConfiguration()
  {
    return _onOffConfiguration;
  }

  void setOnOffConfiguration(FamilyAlgoPairing onOffConfig)
  {
    _onOffConfiguration = onOffConfig;
  }

  void withFeedback_actionPerformed(ActionEvent e)
  {
    if (e.getSource().equals(_feedbackCheckBox))
    {
      _withFeedbackCheckBoxMenuItem2.setSelected(_feedbackCheckBox.isSelected());
    }
    if (e.getSource().equals(_withFeedbackCheckBoxMenuItem2))
    {
      _feedbackCheckBox.setSelected(_withFeedbackCheckBoxMenuItem2.isSelected());
    }
  }

  void withLearning_actionPerformed(ActionEvent e)
  {
    if (e.getSource().equals(_jointLearningCheckBox))
    {
      _withLearningCheckBoxMenuItem3.setSelected(_jointLearningCheckBox.isSelected());
    }
    if (e.getSource().equals(_withLearningCheckBoxMenuItem3))
    {
      _jointLearningCheckBox.setSelected(_withLearningCheckBoxMenuItem3.isSelected());
      _subtypeLearningCheckBox.setSelected(_withLearningCheckBoxMenuItem3.isSelected());
    }
  }

  void deleteLearnedDataButton_actionPerformed(ActionEvent e)
  {
     DeleteLearnedDataDlg dlg = new DeleteLearnedDataDlg(this, "Delete Learned Data", true);
     Dimension dlgSize = dlg.getPreferredSize();
     Dimension frmSize = getSize();
     Point loc = getLocation();
     dlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
     dlg.setModal(true);
     dlg.show();
  }

  void copyThresholdButton_actionPerformed(ActionEvent e)
  {
     CopyThresholdsDlg dlg = new CopyThresholdsDlg(this, "Copy Thresholds", true);
     Dimension dlgSize = dlg.getPreferredSize();
     Dimension frmSize = getSize();
     Point loc = getLocation();
     dlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
     dlg.setModal(true);
     dlg.show();

  }

  void standardThresholdsRadioButton_actionPerformed(ActionEvent e)
  {
    setupAlgorithmTables(true);
  }

  void advancedThresholdsRadioButton_actionPerformed(ActionEvent e)
  {
    setupAlgorithmTables(false);
  }

  void subtypeComboBox_actionPerformed(ActionEvent e)
  {
    String subtype = (String)_subtypeComboBox.getSelectedItem();
    setupAlgorithmTables(_standardThresholdsRadioButton.isSelected());
  }

  void displayCADStatusMenuItem_actionPerformed(ActionEvent e)
  {
    String message = new String();
    message = "Panel: NEPCON_WIDE\n";
    message += "Board: WIDE_BOTTOM\n";
    message += "Number of Boards: 1\n";
    message += "CAD Status: Local, Ver 1.0";

    JOptionPane.showMessageDialog(this, message, "CAD Info", JOptionPane.INFORMATION_MESSAGE);
  }

  void displayMachineStatusMenuItem_actionPerformed(ActionEvent e)
  {
    String message = new String();
    message = "Current: 100.42\n";
    message += "Voltage: 159.73\n";
    message += "Stage Speed: 3\n";
    message += "Calibrated Machine: Yes\n\n";
    if (_runButton.isEnabled() == false)
    {
      message += "View:  3\n";
      message += "FOV: 400 - 1024\n";
      message += "Integration Cycles: 2\n";
      message += "Camera Index: 3";
    }

    JOptionPane.showMessageDialog(this, message, "Machine Info", JOptionPane.INFORMATION_MESSAGE);
  }
  void _allViewsRadioButton1_actionPerformed(ActionEvent e)
  {
    _viewFromTextField1.setEnabled(false);
    _viewToTextField1.setEnabled(false);
  }
  void _viewFromRadioButton1_actionPerformed(ActionEvent e)
  {
    _viewFromTextField1.setEnabled(true);
    _viewFromTextField1.setBackground(Color.white);
    _viewToTextField1.setEnabled(true);
    _viewToTextField1.setBackground(Color.white);
  }
  void _standardThresholdsRadioButton_actionPerformed(ActionEvent e)
  {

  }
  void _advancedThresholdsRadioButton_actionPerformed(ActionEvent e)
  {

  }
  void _stopButton_actionPerformed(ActionEvent e)
  {

  }
  void _stopButton1_actionPerformed(ActionEvent e)
  {

  }

  void showDescriptionCheckBox_actionPerformed(ActionEvent e)
  {
    if (_showDescriptionCheckBox.isSelected())
      _threshDescDlg.setVisible(true);
    else
      _threshDescDlg.setVisible(false);
  }

  void _feedbackCheckBox_actionPerformed(ActionEvent e)
  {

  }
  void _jointLearningCheckBox_actionPerformed(ActionEvent e)
  {

  }

  void writeLearnedDataButton_actionPerformed(ActionEvent e)
  {
    int answer = JOptionPane.showConfirmDialog(this, "Do you want to update thresholds with learned data?",
                                                     "Update Thresholds?", JOptionPane.YES_NO_OPTION);
    if (answer == JOptionPane.YES_OPTION)
    // do the work here!
       JOptionPane.showMessageDialog(this, "Threshold file sucessfully updated with learned data.",
                                           "Write Learned Data", JOptionPane.INFORMATION_MESSAGE);
  }
  void _subtypeComboBox_actionPerformed(ActionEvent e)
  {

  }
  void _familyComboBox_actionPerformed(ActionEvent e)
  {

  }

  void familySubtypeToggleButton_actionPerformed(ActionEvent e)
  {
    jPanel18.removeAll();
    jPanel18.add(jPanel6, BorderLayout.CENTER);
    jPanel18.revalidate();
    jPanel18.repaint();
    //this.invalidate();
//    System.out.println("FamilySubtype action");
  }

  void devicePinToggleButton_actionPerformed(ActionEvent e)
  {
    jPanel18.removeAll();
    jPanel18.add(jPanel29, BorderLayout.CENTER);
    jPanel18.revalidate();
    jPanel18.repaint();

    String currentDevice = (String)_deviceComboBox.getSelectedItem();
    String currentPin = (String)_pinComboBox.getSelectedItem();
    _runDescriptionTextField.setText("Device:  " + currentDevice + "   Pin:  " + currentPin );
    //this.invalidate();

  //  System.out.println("DevicePin action");
  }

  void boardViewToggleButton_actionPerformed(ActionEvent e)
  {
    jPanel18.removeAll();
    jPanel18.add(jPanel30, BorderLayout.CENTER);
    jPanel18.revalidate();
    jPanel18.repaint();
    //this.invalidate();
    //System.out.println("BoardView action");
  }

  void testModeSelectComboBox_actionPerformed(ActionEvent e)
  {
    int index = _testModeSelectComboBox.getSelectedIndex();
    if (index == 0) // family/subtype
    {
      jPanel18.removeAll();
      jPanel18.add(jPanel6, BorderLayout.CENTER);
      jPanel18.revalidate();
      jPanel18.repaint();

      String currentFamily = (String)_familyComboBox.getSelectedItem();
      String currentSubtype = (String)_subtypeComboBox.getSelectedItem();
      _runDescriptionTextField.setText("Family:  " + currentFamily + "   Subtype:  " + currentSubtype );
    }
    else if (index == 1) // device/pin
    {
      jPanel18.removeAll();
      jPanel18.add(jPanel29, BorderLayout.CENTER);
      jPanel18.revalidate();
      jPanel18.repaint();

      String currentDevice = (String)_deviceComboBox.getSelectedItem();
      String currentPin = (String)_pinComboBox.getSelectedItem();
      _runDescriptionTextField.setText("Device:  " + currentDevice + "   Pin:  " + currentPin );
    }
    else // board/view
    {
      jPanel18.removeAll();
      jPanel18.add(jPanel30, BorderLayout.CENTER);
      jPanel18.revalidate();
      jPanel18.repaint();

      String currentBoard = (String)_boardComboBox.getSelectedItem();
      String currentView;
      if (_allViewsRadioButton1.isSelected())
        currentView = "All";
      else
        currentView = _viewFromTextField1.getText() + " to " + _viewToTextField1.getText();
      _runDescriptionTextField.setText("Board:  " + currentBoard + "   Views:  " + currentView );
    }
  }

  void moreInfoButton_actionPerformed(ActionEvent e)
  {
    ImageIcon helpImage = new ImageIcon(AlgoTunerFrame.class.getResource("ToeSrchHelp.gif"));
    JOptionPane pane = new JOptionPane("", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION,  helpImage);
    JDialog dlg = pane.createDialog(this, "More Info...");
    dlg.setModal(false);
    dlg.pack();
    dlg.show();
  }

  void showDescToggleButton_actionPerformed(ActionEvent e)
  {
    if (_showDescToggleButton.isSelected())
    {
      Dimension panelsize = jPanel20.getSize();
      Dimension framesize = this.getSize();

      jPanel57.add(jPanel20, BorderLayout.SOUTH);
      this.setSize(framesize.width, framesize.height + panelsize.height);
      jPanel57.revalidate();
      jPanel57.repaint();
    }
    else // hide jPanel20
    {
      Dimension panelsize = jPanel20.getSize();
      Dimension framesize = this.getSize();

      jPanel57.remove(jPanel20);
      this.setSize(framesize.width, framesize.height - panelsize.height);
      jPanel57.revalidate();
      jPanel57.repaint();
    }

  }
  public JPanel getAlgoTunerPanel()
  {
    return jPanel2;
  }

}
