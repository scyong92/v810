package proto.algoTuner;

import java.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.table.TableCellRenderer;

public class CustomThresholdRenderer extends JLabel implements TableCellRenderer
{
  private Hashtable _renderers;
  private TableCellRenderer _renderer, _defaultRenderer;

  public CustomThresholdRenderer()
  {
    _renderers = new Hashtable();
    _defaultRenderer = new DefaultTableCellRenderer();
  }

  void add(int row, TableCellRenderer renderer)
  {
    _renderers.put(new Integer(row), renderer);
  }

  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    _renderer = (TableCellRenderer)_renderers.get(new Integer(row));
    if (_renderer != null)
      //_renderer = _defaultRenderer;
      return _renderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

    setOpaque(true);
    if (isSelected)
    {
      setForeground(table.getSelectionForeground());
      setBackground(table.getSelectionBackground());
    }
    else
    {
      setForeground(table.getForeground());
      setBackground(table.getBackground());
    }

    if (hasFocus)
      setBorder(BorderFactory.createLineBorder(Color.yellow));
    else
      setBorder(null);

    if (value != null)
    {
      String text = value.toString();
      setText(text);
      if (table.getColumnName(column).compareToIgnoreCase("Value") == 0)
      {
	setForeground(Color.black);
	String defaultValue = (String)table.getModel().getValueAt(row, 2);//    getValueAt(row, defaultColumn).toString();
       // System.out.println("value: " + text + " default " + defaultValue);
	if (text.equalsIgnoreCase(defaultValue))
	  setBackground(Color.white);
	else
	{
//          System.out.println("Not the same");
	  setBackground(Color.green);
	}
      }
    }

    return this;
  }
}
