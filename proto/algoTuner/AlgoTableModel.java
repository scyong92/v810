package proto.algoTuner;

import javax.swing.table.*;
import java.util.*;
import javax.swing.*;

public class AlgoTableModel extends AbstractTableModel
{
  private AlgoTunerFrame _mainFrame;
  private boolean testRunning = false;
  private List _displayData = null;  // a list of lists of strings -- data to be displayed
  private final String[] _thresholdColumnNames = {"Name",
						   "Value",
						   "Default",
						   "Low Limit",
						   "High Limit"};

  private final Object[] _fpgullwing_spc_basic_1 = {"Heel Search Technique", "Profile Start", "Profile Start", "", ""};
  private final Object[] _fpgullwing_spc_basic_2 = {"Heel Size",             new Float(0.4), "0.4", "0.01", "10.0"};
  private final Object[] _fpgullwing_spc_basic_3 = {"Pin Length",            new Float(0.0), "0.0", "0.0", "500.0"};
  private final Object[] _fpgullwing_spc_basic_4 = {"Center Position",       new Float(0.5), "0.5", "0.0", "1.0"};
  private final Object[] _fpgullwing_spc_basic_5 = {"Heel Search Distance",  new Float(2.0), "2.0", "0.0", "10.0"};
  private final Object[] _fpgullwing_spc_basic_6 = {"Learned Joint Enable",  "No", "No", "", ""};
  private final Object[] _fpgullwing_spc_basic_7 = {"Toe Search Technique",  "Avg Toe/Side", "Avg Toe/Side", "", ""};

  private final Object[] _fpgullwing_spc_advanced_1 = {"Secondary Heel Search Distance",    "2", "2", "0", "10"};
  private final Object[] _fpgullwing_spc_advanced_2 = {"Toe Search Edge Fraction",   "0", "0", "0", "1"};
  private final Object[] _fpgullwing_spc_advanced_3 = {"Smear Region Width",    "1", "1", "0", "1"};
  private final Object[] _fpgullwing_spc_advanced_4 = {"Smear Region Shift",   "2", "2", "0", "100"};
  private final Object[] _fpgullwing_spc_advanced_5 = {"Omit Learned Items", "(All)", "All", "None", "All"};
  private final Object[] _fpgullwing_spc_advanced_6 = {"Background Profile Width", "0", "0", "0", "25"};

  public AlgoTableModel()
  {
    _displayData = new LinkedList();
    _displayData.add(_fpgullwing_spc_basic_1);
    _displayData.add(_fpgullwing_spc_basic_2);
    _displayData.add(_fpgullwing_spc_basic_3);
    _displayData.add(_fpgullwing_spc_basic_4);
    _displayData.add(_fpgullwing_spc_basic_5);
  }

  public AlgoTableModel(int thredType)
  {
    _displayData = new LinkedList();
    if (thredType == 1)
    {
      _displayData.add(_fpgullwing_spc_basic_1);
      _displayData.add(_fpgullwing_spc_basic_2);
      _displayData.add(_fpgullwing_spc_basic_3);
      _displayData.add(_fpgullwing_spc_basic_4);
      _displayData.add(_fpgullwing_spc_basic_5);
      _displayData.add(_fpgullwing_spc_basic_6);
      _displayData.add(_fpgullwing_spc_basic_7);
    }
    else if (thredType == 2)  //advanced
    {
      _displayData.add(_fpgullwing_spc_advanced_1);
      _displayData.add(_fpgullwing_spc_advanced_2);
      _displayData.add(_fpgullwing_spc_advanced_3);
      _displayData.add(_fpgullwing_spc_advanced_4);
      _displayData.add(_fpgullwing_spc_advanced_5);
      _displayData.add(_fpgullwing_spc_advanced_6);
    }
  }

  void setTestRunning(boolean state)
  {
    testRunning = state;
  }

    // from AbstractTableModel
  public int getColumnCount()
  {
    return _thresholdColumnNames.length;
  }

  // from AbstractTableModel
  public int getRowCount()
  {
    if (_displayData == null)
      return 0;
    else
      return _displayData.size();
  }

  // from AbstractTableModel
  public Object getValueAt(int row, int col)
  {
    Object[] rowList = (Object[])_displayData.get(row);
    Object columnInfo = (Object)rowList[col];
    return columnInfo;
//    return ((List)_displayData.get(row)).get(col);
  }

  // from AbstractTableModel
  public String getColumnName(int col)
  {
    return (String)_thresholdColumnNames[col];
  }

  public boolean isCellEditable(int row, int col)
  {
    if (testRunning)
      return false;

    if (col == 1)
      return true;
    else
      return false;
  }
//  public Class getColumnClass(int c)
//  {
//     return getValueAt(0, c).getClass();
//  }

  public void setValueAt(Object value, int row, int col)
  {
    //System.out.println("row: " + row + " col: " + col + " value: " + value);

    String valstr = value.toString();
    Integer intval = null;
    Float floatval = null;
    Boolean boolval = null;
    Object colobject = getValueAt(row,col);

    if (colobject instanceof Boolean)
    {
      boolval = new Boolean(valstr);
      Object[] rowList = (Object[])_displayData.get(row);
      rowList[col] = boolval;
    }
    else if (colobject instanceof Integer)
    {
      try
      {
	intval = new Integer(valstr);
	Object[] rowList = (Object[])_displayData.get(row);
	rowList[col] = new Integer(intval.toString());
      }
      catch (NumberFormatException nfe)
      {
	intval = null;
      }
    }
    else if (colobject instanceof Float)
    {
      try
      {
	floatval = new Float(valstr);
	Object[] rowList = (Object[])_displayData.get(row);
	rowList[col] = new Float(floatval.toString());
      }
      catch (NumberFormatException nfe1)
      {
	floatval = null;
      }
    }
    else // string
    {
      Object[] rowList = (Object[])_displayData.get(row);
      rowList[col] = value;
    }

//    if (intval != null)
//    {
//      Object[] rowList = (Object[])_displayData.get(row);
//      rowList[col] = new Integer(intval.toString());
//    }
//    if (floatval != null)
//    {
//      Object[] rowList = (Object[])_displayData.get(row);
//      rowList[col] = new Float(floatval.toString());
//    }
  }

}
