package proto.algoTuner;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import javax.swing.border.*;

/**
  * This class implements a splash screen with the application name at the
  * top, Agilent logo in the middle and a progress indicator at the bottom.
  * The application name is passed in through the constructor.
  *
  * @author Steve Anonson
  */
public class AgilentSplashScreen extends JWindow
{
  private final static int _DELAY = 100;
  private final static int _MAX_DOTS = 40;  // maximum number of dot in the animation
  private final static int _MIN_SHOW_TIME = 2000;  // minumum time to show the screen to avoid flashing
  private String _appName = null;
  private int _dots = 1;
  private String _loadingStr = " Loading .";
  private Timer _timer = null;

  private JPanel _borderPanel = new JPanel();
  private BorderLayout _borderPanelLayout = new BorderLayout();
  private JLabel _centerLabel = new JLabel();
  private JLabel _southLabel = new JLabel();
  private JLabel _northLabel = new JLabel();
  private Border _southLabBorder = null;
  private Border _northLabBorder = null;
  private Border _borderPanelBorder = null;

  /**
    * Constructor to create a splash screen.
    *
    * @param appName - String containing the name of the application
    * @author Steve Anonson
    */
  public AgilentSplashScreen( String appName )
  {
    _appName = appName;
    jbInit();
    ImageIcon icon = new ImageIcon(AlgoTunerFrame.class.getResource("splash_5dx.gif"));

    if (icon != null)
      _centerLabel.setIcon(icon);
    else
      _centerLabel.setText(" Agilent Technologies ");

    this.setCursor( new Cursor(Cursor.WAIT_CURSOR) );
    this.pack();
    this.centerOnScreen();

    // Create a Timer that will call the updateScreen() method
    _timer = new Timer(_DELAY, new ActionListener()
    {
      public void actionPerformed(ActionEvent evt)
      {
  SwingUtilities.invokeLater( new Runnable() {
    public void run()
    {
      updateScreen();
    }
  } );
      }
    } );  // end new Timer
  }

  /**
    * Default constructor.
    *
    * @author Steve Anonson
    */
  public AgilentSplashScreen()
  {
    this( null );
  }

  /**
    * This method pops up the splash screen and starts the loadins animation timer.
    *
    * @author Steve Anonson
    */
  public void started()
  {
    show();
    _timer.start();
    // Make sure screen appears a minimum amount of time to avoid flashing.
    try { Thread.sleep( _MIN_SHOW_TIME ); }
    catch (InterruptedException e) { /* do nothing */ }
  }

  /**
    * This method stops the animation timers and distroys the screen.
    *
    * @author Steve Anonson
    */
  public void finished()
  {
    int showTime = _dots * _DELAY;
    if (showTime < _MIN_SHOW_TIME)
    {
      // Make sure screen appears a minimum amount of time to avoid flashing.
      try { Thread.sleep( _MIN_SHOW_TIME - showTime ); }
      catch (InterruptedException e) { /* do nothing */ }
    }
    _timer.stop();
    hide();
    dispose();
  }

  /**
    * This method creats the contents of the splash screen.  It consists of
    * a label at the top to display the application name, the Agilent logo in
    * a label in the middle and a loading animation in a label at the bottom.
    *
    * @author Steve Anonson
    */
  private void jbInit()
  {
    _southLabBorder = BorderFactory.createEmptyBorder(4,0,0,0);
    _northLabBorder = BorderFactory.createEmptyBorder(0,0,4,0);
    _borderPanelBorder = BorderFactory.createEmptyBorder(4,4,4,4);
    this.getContentPane().setBackground(new Color(50, 85, 145));
    _borderPanel.setBackground(new Color(50, 85, 145));
    _borderPanel.setBorder(_borderPanelBorder);
    _borderPanel.setLayout(_borderPanelLayout);
    if (_appName != null)
    {
      _northLabel.setText(_appName);
    }
    _centerLabel.setForeground(Color.black);
    _centerLabel.setBorder(BorderFactory.createRaisedBevelBorder());
    _centerLabel.setBackground(Color.black);
    _centerLabel.setFont(new java.awt.Font("Serif", 1, 48));
    _centerLabel.setOpaque(true);
    _centerLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _southLabel.setForeground(Color.white);
    _southLabel.setBorder(_southLabBorder);
    _southLabel.setFont(new java.awt.Font("SansSerif", 1, 12));
    _southLabel.setText(_loadingStr);
    _northLabel.setForeground(Color.white);
    _northLabel.setBorder(_northLabBorder);
    _northLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _northLabel.setBackground(SystemColor.menu);
    _northLabel.setFont(new java.awt.Font("SansSerif", 1, 24));
    this.getContentPane().add(_borderPanel, BorderLayout.CENTER);
    _borderPanel.add(_centerLabel, BorderLayout.CENTER);
    _borderPanel.add(_southLabel, BorderLayout.SOUTH);
    _borderPanel.add(_northLabel, BorderLayout.NORTH);
  }

  /**
    * This method positions the splash screen in the middle of the display screen.
    *
    * @author Steve Anonson
    */
  private void centerOnScreen()
  {
    // Position the dialog in the center of the screen.
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = getSize();
    if (frameSize.height > screenSize.height)
      frameSize.height = screenSize.height;
    if (frameSize.width > screenSize.width)
      frameSize.width = screenSize.width;
    setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
  }

  /**
    * This method handles the animation in the bottom label.  The word "Loading"
    * appears with dot "." added after it.  When 20 dots are reached it reset to
    * one dot again.
    *
    * @author Steve Anonson
    */
  private void updateScreen()
  {
    _dots++;
    if (_dots < 0)
      _dots = 1;
    if ((_dots % _MAX_DOTS) == 1)
      _loadingStr = " Loading .";
    else
      _loadingStr = _loadingStr + " .";
    _southLabel.setText(_loadingStr);
    if (_dots > _MAX_DOTS)
      _dots = 0;
  }
/*
  public static void main(String[] args)
  {
    com.agilent.xRayTest.util.AssertUtil.setUpAssert("AgilentSplashScreen", null);
    AgilentSplashScreen splashScreen = new AgilentSplashScreen("Splash Screen Test");
//    AgilentSplashScreen splashScreen = new AgilentSplashScreen();
    splashScreen.invokedStandalone = true;
    splashScreen.started();
//    splashScreen.finished();
  }
  private boolean invokedStandalone = false;
*/
}
