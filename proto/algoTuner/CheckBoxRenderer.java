
/**
 * Title:        Algorithm Tuner<p>
 * Description:  <p>
 * Copyright:    Copyright (c) 1999<p>
 * Company:      Agilent Technologies<p>
 * @author Andy Mechtenberg
 * @version
 */
package proto.algoTuner;

import javax.swing.*;
import javax.swing.JCheckBox;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

class CheckBoxRenderer extends JCheckBox implements TableCellRenderer
{

  public CheckBoxRenderer()
  {
    setHorizontalAlignment(JLabel.CENTER);
  }

  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    if (isSelected)
    {
      setForeground(table.getSelectionForeground());
      setBackground(table.getSelectionBackground());
    }
    else
    {
      setForeground(table.getForeground());
      setBackground(table.getBackground());
      //setBackground(java.awt.Color.green);
    }
    setSelected((value != null && ((Boolean)value).booleanValue()));
    return this;
  }
}
