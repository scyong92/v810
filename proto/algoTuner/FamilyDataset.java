
/**
 * Title:        Algorithm Tuner<p>
 * Description:  <p>
 * Copyright:    Copyright (c) 1999<p>
 * Company:      Agilent Technologies<p>
 * @author Andy Mechtenberg
 * @version
 */
package proto.algoTuner;

public class FamilyDataset
{
  private static String[] _familiesInCad = {"All", "BGA2", "Cap", "FPGullwing", "JLead", "Res"};
  public FamilyDataset()
  {
  }

  static String[] getFamilies()
  {
    return _familiesInCad;
  }
}
