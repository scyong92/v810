
/**
 * Title:        Algorithm Tuner<p>
 * Description:  <p>
 * Copyright:    Copyright (c) 1999<p>
 * Company:      Agilent Technologies<p>
 * @author Andy Mechtenberg
 * @version
 */
package proto.algoTuner;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
//import com.borland.jbcl.layout.*;
import java.awt.event.*;

public class DeleteLearnedDataDlg extends JDialog
{
  JPanel panel1 = new JPanel();
  BorderLayout borderLayout1 = new BorderLayout();
  JPanel jPanel1 = new JPanel();
  JPanel jPanel2 = new JPanel();
  JPanel jPanel3 = new JPanel();
  GridLayout gridLayout1 = new GridLayout();
  JButton _cancelButton = new JButton();
  JButton _okButton = new JButton();
  Border border1;
  GridLayout gridLayout2 = new GridLayout();
  FlowLayout flowLayout1 = new FlowLayout();
  JPanel jPanel4 = new JPanel();
  JPanel jPanel5 = new JPanel();
  JComboBox _familyComboBox = new JComboBox();
  JLabel jLabel1 = new JLabel();
  JComboBox _algorithmComboBox = new JComboBox();
  JLabel jLabel2 = new JLabel();
  JPanel jPanel6 = new JPanel();
  JPanel jPanel7 = new JPanel();
  JCheckBox jCheckBox1 = new JCheckBox();
  JCheckBox jCheckBox2 = new JCheckBox();
  FlowLayout flowLayout2 = new FlowLayout();
  FlowLayout flowLayout3 = new FlowLayout();
  FlowLayout flowLayout4 = new FlowLayout();
  FlowLayout flowLayout5 = new FlowLayout();

  public DeleteLearnedDataDlg(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public DeleteLearnedDataDlg()
  {
    this(null, "", false);
  }

  void jbInit() throws Exception
  {
    border1 = BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134));
    panel1.setLayout(borderLayout1);
    jPanel3.setLayout(gridLayout1);
    _cancelButton.setText("Cancel");
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	cancelButton_actionPerformed(e);
      }
    });
    _okButton.setText("Ok");
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
	okButton_actionPerformed(e);
      }
    });
    jPanel2.setBorder(border1);
    jPanel2.setLayout(gridLayout2);
    gridLayout2.setRows(2);
    jPanel1.setLayout(flowLayout1);
    flowLayout1.setHgap(30);
    gridLayout1.setRows(2);
    gridLayout1.setColumns(1);
    jLabel1.setPreferredSize(new Dimension(55, 17));
    jLabel1.setText("Family:");
    jLabel2.setPreferredSize(new Dimension(55, 17));
    jLabel2.setText("Subtype:");
    jCheckBox1.setText("Delete Learned Joint Data");
    jCheckBox2.setText("Delete Learned Subtype Data");
    jPanel4.setLayout(flowLayout2);
    flowLayout2.setAlignment(FlowLayout.LEFT);
    jPanel5.setLayout(flowLayout3);
    flowLayout3.setAlignment(FlowLayout.LEFT);
    jPanel7.setLayout(flowLayout4);
    jPanel6.setLayout(flowLayout5);
    flowLayout4.setAlignment(FlowLayout.LEFT);
    flowLayout5.setAlignment(FlowLayout.LEFT);
    _familyComboBox.setPreferredSize(new Dimension(120, 21));
    _algorithmComboBox.setPreferredSize(new Dimension(120, 21));
    getContentPane().add(panel1);
    panel1.add(jPanel1, BorderLayout.SOUTH);
    jPanel1.add(_okButton, null);
    jPanel1.add(_cancelButton, null);
    panel1.add(jPanel2, BorderLayout.CENTER);
    jPanel2.add(jPanel7, null);
    jPanel7.add(jCheckBox1, null);
    jPanel2.add(jPanel6, null);
    jPanel6.add(jCheckBox2, null);
    panel1.add(jPanel3, BorderLayout.NORTH);
    jPanel3.add(jPanel4, null);
    jPanel4.add(jLabel1, null);
    jPanel4.add(_familyComboBox, null);
    jPanel3.add(jPanel5, null);
    jPanel5.add(jLabel2, null);
    jPanel5.add(_algorithmComboBox, null);

// populate comboboxes
    String[] families = FamilyDataset.getFamilies();
    for (int i = 0; i < families.length; i++)
      _familyComboBox.addItem(families[i]);
    _familyComboBox.setSelectedIndex(0);

    String[] algos = Algorithms.getAlgorithmsForFamily((String)_familyComboBox.getSelectedItem());

    for (int i = 0; i < algos.length; i++)
      _algorithmComboBox.addItem(algos[i]);
//    _algorithmComboBox.addItem("All");
    _algorithmComboBox.setSelectedIndex(0);
  }

  void okButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  void cancelButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }
}
