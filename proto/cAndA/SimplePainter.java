package com.agilent.xRayTest.gui.cAndA;

import java.awt.*;
import java.awt.image.*;
import javax.swing.*;

/**
 * Title:        SimplePainter
 * Description:  Helper class for painting an image in a Component-based class
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies
 * @author       Tony Turner
 * @version 1.0
 */

public class SimplePainter extends JComponent
{
  private BufferedImage bufferedImage, newBufferedImage;
;
  private static int MARGIN = 10;
  private Dimension size;
  private Paint gradientPaint;
  private MediaTracker tracker = new MediaTracker(this);
  private Image image;

  public SimplePainter(String imageName, int sp_x, int sp_y, int sp_w, int sp_h)
  {
    // load the image
    Image image = Toolkit.getDefaultToolkit().getImage(imageName);
    tracker.addImage(image,0);
    try
    {
      tracker.waitForAll();
    }
    catch (InterruptedException e)
    {
      throw new Error("could not load " + imageName
                             + " with MediaTracker");
    }

    //set up the image size
    int w = 600 + 2*MARGIN;
    int h = 600 + 2*MARGIN;
    size = new Dimension(w,h);

    // set up the parent buffered image, and grab a subimage from it
    bufferedImage = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_GRAY);
    bufferedImage.createGraphics().drawImage(image,0, 0, w, h, null);

    newBufferedImage = bufferedImage.getSubimage(sp_x,sp_y,sp_w,sp_h);


  }
  // Dimension, in user space, that the SimplePainter paints,
  // starting from (0,0)
  public Dimension getSize()
  {
    return size;
  }

  public void render(Graphics2D g)
  {
    // render the subimage from the parent image
    g.drawImage(newBufferedImage, MARGIN, MARGIN, null);
  }
}