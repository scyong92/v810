package com.agilent.xRayTest.gui.cAndA;

import java.awt.*;
import javax.swing.*;
import java.beans.*;
import java.awt.event.*;

/**
 * Title:        5DX Calibration Utilities
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Tony Turner
 * @version 1.0
 */

public class AlignLineScanCameraYawPanel extends JPanel
{
  BorderLayout borderLayout1 = new BorderLayout();
  private JPanel alignLineScanCameraYawPanel = new JPanel();
  private JLabel alignLineScanCameraYawLabel = new JLabel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private JSplitPane jSplitPane1 = new JSplitPane();
  private JPanel jPanel1 = new JPanel();
  private JPanel jPanel2 = new JPanel();
  private BorderLayout borderLayout3 = new BorderLayout();
  private BorderLayout borderLayout4 = new BorderLayout();
  private JPanel jPanel3 = new JPanel();
  private Box box1;
  private JButton btnGoodFrontYawAngle = new JButton();
  private JButton btnBadFrontYawAngle = new JButton();
  private JLabel jLabel1 = new JLabel();
  private JPanel jPanel4 = new JPanel();
  private Box box2;
  private JButton btnGoodRearYawAngle = new JButton();
  private JButton btnBadRearYawAngle = new JButton();
  private JLabel jLabel2 = new JLabel();
  private boolean _goodFrontYawAngle;
  private boolean _goodRearYawAngle;
  transient private PropertyChangeSupport propertyChangeListeners = new PropertyChangeSupport(this);
  private boolean _frontDecided;
  private boolean _rearDecided;
  private boolean _bothDecided;

  public AlignLineScanCameraYawPanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    set_frontDecided(false);
    set_rearDecided(false);
    box1 = Box.createHorizontalBox();
    box2 = Box.createHorizontalBox();
    this.setLayout(borderLayout1);
    alignLineScanCameraYawLabel.setFont(new java.awt.Font("Dialog", 1, 18));
    alignLineScanCameraYawLabel.setHorizontalAlignment(SwingConstants.CENTER);
    alignLineScanCameraYawLabel.setText("Align Line Scan Camera Yaw Angle");
    alignLineScanCameraYawPanel.setLayout(borderLayout2);
    jSplitPane1.setDividerSize(5);
    jPanel1.setLayout(borderLayout3);
    jPanel2.setLayout(borderLayout4);
    btnGoodFrontYawAngle.setText("Aligned");
    btnGoodFrontYawAngle.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnGoodFrontYawAngle_actionPerformed(e);
      }
    });
    btnBadFrontYawAngle.setText("Can\'t Align");
    btnBadFrontYawAngle.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnBadFrontYawAngle_actionPerformed(e);
      }
    });
    jLabel1.setText("Front Camera Image Goes Here");
    btnGoodRearYawAngle.setText("Aligned");
    btnGoodRearYawAngle.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnGoodRearYawAngle_actionPerformed(e);
      }
    });
    btnBadRearYawAngle.setText("Can\'t Align");
    btnBadRearYawAngle.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnBadRearYawAngle_actionPerformed(e);
      }
    });
    jLabel2.setText("Rear Camera Image Goes Here");
    this.add(alignLineScanCameraYawPanel, BorderLayout.CENTER);
    alignLineScanCameraYawPanel.add(alignLineScanCameraYawLabel, BorderLayout.NORTH);
    alignLineScanCameraYawPanel.add(jSplitPane1, BorderLayout.CENTER);
    jSplitPane1.add(jPanel1, JSplitPane.LEFT);
    jPanel1.add(jPanel3, BorderLayout.SOUTH);
    jPanel3.add(box1, null);
    box1.add(btnGoodFrontYawAngle, null);
    box1.add(btnBadFrontYawAngle, null);
    jPanel1.add(jLabel1, BorderLayout.CENTER);
    jSplitPane1.add(jPanel2, JSplitPane.RIGHT);
    jPanel2.add(jPanel4, BorderLayout.SOUTH);
    jPanel4.add(box2, null);
    box2.add(btnGoodRearYawAngle, null);
    box2.add(btnBadRearYawAngle, null);
    jPanel2.add(jLabel2, BorderLayout.CENTER);
    jSplitPane1.setDividerLocation(190);
  }
  public void set_goodFrontYawAngle(boolean new_goodFrontYawAngle)
  {
    _goodFrontYawAngle = new_goodFrontYawAngle;
    set_frontDecided(true);
    if ((_frontDecided) && (_rearDecided))
    {
      set_bothDecided(true);
    }

  }
  public boolean is_goodFrontYawAngle()
  {
    return _goodFrontYawAngle;
  }
  public void set_goodRearYawAngle(boolean new_goodRearYawAngle)
  {
    _goodRearYawAngle = new_goodRearYawAngle;
    set_rearDecided(true);
    if ((_frontDecided) && (_rearDecided))
    {
      set_bothDecided(true);
    }
  }
  public boolean is_goodRearYawAngle()
  {
    return _goodRearYawAngle;
  }
  public synchronized void removePropertyChangeListener(PropertyChangeListener l)
  {
    super.removePropertyChangeListener(l);
    propertyChangeListeners.removePropertyChangeListener(l);
  }
  public synchronized void addPropertyChangeListener(PropertyChangeListener l)
  {
    super.addPropertyChangeListener(l);
    propertyChangeListeners.addPropertyChangeListener(l);
  }
  public void set_frontDecided(boolean new_frontDecided)
  {
    _frontDecided = new_frontDecided;
  }
  public boolean is_frontDecided()
  {
    return _frontDecided;
  }
  public void set_rearDecided(boolean new_rearDecided)
  {
    _rearDecided = new_rearDecided;
  }
  public boolean is_rearDecided()
  {
    return _rearDecided;
  }
  public void set_bothDecided(boolean new_bothDecided)
  {
/** @todo act _ go back to frmCalibration and add listener adapter code for these four cases next */
    boolean  old_bothDecided = _bothDecided;
    _bothDecided = new_bothDecided;
    if (is_goodFrontYawAngle() && is_goodRearYawAngle())
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontYawGood_rearYawGood", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }
    else if (! is_goodFrontYawAngle() && is_goodRearYawAngle())
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontYawBad_rearYawGood", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }
    else if (is_goodFrontYawAngle() && ! is_goodRearYawAngle())
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontYawGood_rearYawBad", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }
    else
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontYawBad_rearYawBad", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }

  }
  public boolean is_bothDecided()
  {
    return _bothDecided;
  }

  void btnGoodFrontYawAngle_actionPerformed(ActionEvent e)
  {
    set_goodFrontYawAngle(true);
    btnGoodFrontYawAngle.setBackground(Color.green);
    btnBadFrontYawAngle.setBackground(Color.lightGray);
  }

  void btnBadFrontYawAngle_actionPerformed(ActionEvent e)
  {
    set_goodFrontYawAngle(false);
    btnGoodFrontYawAngle.setBackground(Color.lightGray);
    btnBadFrontYawAngle.setBackground(Color.red);
  }

  void btnGoodRearYawAngle_actionPerformed(ActionEvent e)
  {
    set_goodRearYawAngle(true);
    btnGoodRearYawAngle.setBackground(Color.green);
    btnBadRearYawAngle.setBackground(Color.lightGray);
  }

  void btnBadRearYawAngle_actionPerformed(ActionEvent e)
  {
    set_goodRearYawAngle(false);
    btnGoodRearYawAngle.setBackground(Color.lightGray);
    btnBadRearYawAngle.setBackground(Color.red);
  }

}
