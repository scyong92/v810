package com.agilent.xRayTest.gui.cAndA;

import java.awt.*;
import javax.swing.*;
import java.beans.*;
import java.awt.event.*;
import java.awt.image.*;

/**
 * Title:        5DX Calibration Utilities
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Tony Turner
 * @version 1.0
 */

public class AdjustLineScanCameraFocalDistancePanel extends JPanel
{
  BorderLayout borderLayout1 = new BorderLayout();
  private JPanel adjustLineScanCameraFocalDistancePanel = new JPanel();
  private JLabel adjustLineScanCameraFocalDistanceLabel = new JLabel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private JSplitPane jSplitPane1 = new JSplitPane();
  private JPanel jPanel1 = new JPanel();
  private JPanel jPanel2 = new JPanel();
  private BorderLayout borderLayout3 = new BorderLayout();
  private BorderLayout borderLayout4 = new BorderLayout();
  private JPanel jPanel3 = new JPanel();
  private Box box1;
  private JButton btnGoodFrontFocalDistance = new JButton();
  private JButton btnBadFrontFocalDistance = new JButton();
  private JLabel frontCameraImageLabel = new JLabel();
  private JPanel jPanel4 = new JPanel();
  private Box box2;
  private JButton btnGoodRearFocalDistance = new JButton();
  private JButton btnBadRearFocalDistance = new JButton();
  private JLabel rearCameraImageLabel = new JLabel();
  private boolean _goodFrontFocalDistance;
  private boolean _goodRearFocalDistance;
  transient private PropertyChangeSupport propertyChangeListeners = new PropertyChangeSupport(this);
  private boolean _frontDecided;
  private boolean _rearDecided;
  private boolean _bothDecided;
  //private BufferedImage frontFocalDistanceBufferedImage = new BufferedImage();
  //private BufferedImage rearFocalDistanceBufferedImage = new BufferedImage();

  public AdjustLineScanCameraFocalDistancePanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    set_frontDecided(false);
    set_rearDecided(false);
    box1 = Box.createHorizontalBox();
    box2 = Box.createHorizontalBox();
    this.setLayout(borderLayout1);
    adjustLineScanCameraFocalDistanceLabel.setFont(new java.awt.Font("Dialog", 1, 18));
    adjustLineScanCameraFocalDistanceLabel.setHorizontalAlignment(SwingConstants.CENTER);
    adjustLineScanCameraFocalDistanceLabel.setText("Adjust Line Scan Camera FocalDistance");
    adjustLineScanCameraFocalDistancePanel.setLayout(borderLayout2);
    jSplitPane1.setDividerSize(5);
    jPanel1.setLayout(borderLayout3);
    jPanel2.setLayout(borderLayout4);
    btnGoodFrontFocalDistance.setText("Adjusted");
    btnGoodFrontFocalDistance.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnGoodFrontFocalDistance_actionPerformed(e);
      }
    });
    btnBadFrontFocalDistance.setText("Can\'t Adjust");
    btnBadFrontFocalDistance.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnBadFrontFocalDistance_actionPerformed(e);
      }
    });
    frontCameraImageLabel.setText("Front Camera Image Goes Here");
    btnGoodRearFocalDistance.setText("Adjusted");
    btnGoodRearFocalDistance.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnGoodRearFocalDistance_actionPerformed(e);
      }
    });
    btnBadRearFocalDistance.setText("Can\'t Adjust");
    btnBadRearFocalDistance.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnBadRearFocalDistance_actionPerformed(e);
      }
    });
    rearCameraImageLabel.setText("Rear Camera Image Goes Here");
    this.add(adjustLineScanCameraFocalDistancePanel, BorderLayout.CENTER);
    adjustLineScanCameraFocalDistancePanel.add(adjustLineScanCameraFocalDistanceLabel, BorderLayout.NORTH);
    adjustLineScanCameraFocalDistancePanel.add(jSplitPane1, BorderLayout.CENTER);
    jSplitPane1.add(jPanel1, JSplitPane.LEFT);
    jPanel1.add(jPanel3, BorderLayout.SOUTH);
    jPanel3.add(box1, null);
    box1.add(btnGoodFrontFocalDistance, null);
    box1.add(btnBadFrontFocalDistance, null);
    jPanel1.add(frontCameraImageLabel, BorderLayout.CENTER);
    jSplitPane1.add(jPanel2, JSplitPane.RIGHT);
    jPanel2.add(jPanel4, BorderLayout.SOUTH);
    jPanel4.add(box2, null);
    box2.add(btnGoodRearFocalDistance, null);
    box2.add(btnBadRearFocalDistance, null);

    jPanel2.add(rearCameraImageLabel, BorderLayout.CENTER);
    jSplitPane1.setDividerLocation(190);
  }
  public void set_goodFrontFocalDistance(boolean new_goodFrontFocalDistance)
  {
    _goodFrontFocalDistance = new_goodFrontFocalDistance;
    set_frontDecided(true);
    if ((_frontDecided) && (_rearDecided))
    {
      set_bothDecided(true);
    }

  }
  public boolean is_goodFrontFocalDistance()
  {
    return _goodFrontFocalDistance;
  }
  public void set_goodRearFocalDistance(boolean new_goodRearFocalDistance)
  {
    _goodRearFocalDistance = new_goodRearFocalDistance;
    set_rearDecided(true);
    if ((_frontDecided) && (_rearDecided))
    {
      set_bothDecided(true);
    }
  }
  public boolean is_goodRearFocalDistance()
  {
    return _goodRearFocalDistance;
  }
  public synchronized void removePropertyChangeListener(PropertyChangeListener l)
  {
    super.removePropertyChangeListener(l);
    propertyChangeListeners.removePropertyChangeListener(l);
  }
  public synchronized void addPropertyChangeListener(PropertyChangeListener l)
  {
    super.addPropertyChangeListener(l);
    propertyChangeListeners.addPropertyChangeListener(l);
  }
  public void set_frontDecided(boolean new_frontDecided)
  {
    _frontDecided = new_frontDecided;
  }
  public boolean is_frontDecided()
  {
    return _frontDecided;
  }
  public void set_rearDecided(boolean new_rearDecided)
  {
    _rearDecided = new_rearDecided;
  }
  public boolean is_rearDecided()
  {
    return _rearDecided;
  }
  public void set_bothDecided(boolean new_bothDecided)
  {
/** @todo act _ go back to frmCalibration and add listener adapter code for these four cases next */
    boolean  old_bothDecided = _bothDecided;
    _bothDecided = new_bothDecided;
    if (is_goodFrontFocalDistance() && is_goodRearFocalDistance())
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontFocalDistanceGood_rearFocalDistanceGood", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }
    else if (! is_goodFrontFocalDistance() && is_goodRearFocalDistance())
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontFocalDistanceBad_rearFocalDistanceGood", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }
    else if (is_goodFrontFocalDistance() && ! is_goodRearFocalDistance())
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontFocalDistanceGood_rearFocalDistanceBad", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }
    else
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontFocalDistanceBad_rearFocalDistanceBad", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }

  }
  public boolean is_bothDecided()
  {
    return _bothDecided;
  }

  void btnGoodFrontFocalDistance_actionPerformed(ActionEvent e)
  {
    set_goodFrontFocalDistance(true);
    btnGoodFrontFocalDistance.setBackground(Color.green);
    btnBadFrontFocalDistance.setBackground(Color.lightGray);
  }

  void btnBadFrontFocalDistance_actionPerformed(ActionEvent e)
  {
    set_goodFrontFocalDistance(false);
    btnGoodFrontFocalDistance.setBackground(Color.lightGray);
    btnBadFrontFocalDistance.setBackground(Color.red);
  }

  void btnGoodRearFocalDistance_actionPerformed(ActionEvent e)
  {
    set_goodRearFocalDistance(true);
    btnGoodRearFocalDistance.setBackground(Color.green);
    btnBadRearFocalDistance.setBackground(Color.lightGray);
  }

  void btnBadRearFocalDistance_actionPerformed(ActionEvent e)
  {
    set_goodRearFocalDistance(false);
    btnGoodRearFocalDistance.setBackground(Color.lightGray);
    btnBadRearFocalDistance.setBackground(Color.red);
  }

}
