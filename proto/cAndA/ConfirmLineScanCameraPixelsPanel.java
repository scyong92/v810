package com.agilent.xRayTest.gui.cAndA;

import java.awt.*;
import javax.swing.*;
import java.beans.*;
import java.awt.event.*;

/**
 * Title:        5DX Calibration Utilities
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Tony Turner
 * @version 1.0
 */

public class ConfirmLineScanCameraPixelsPanel extends JPanel
{
  BorderLayout borderLayout1 = new BorderLayout();
  private JPanel confirmLineScanCameraPixelsPanel = new JPanel();
  private JLabel confirmLineScanCameraPixelsLabel = new JLabel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private JSplitPane cameraPixelSplitPane = new JSplitPane();
  private JPanel jPanel1 = new JPanel();
  private JPanel jPanel2 = new JPanel();
  private BorderLayout borderLayout3 = new BorderLayout();
  private BorderLayout borderLayout4 = new BorderLayout();
  private JPanel jPanel3 = new JPanel();
  private Box box1;
  private JButton btnGoodFrontCCD = new JButton();
  private JButton btnBadFrontCCD = new JButton();
  private JPanel jPanel4 = new JPanel();
  private Box box2;
  private JButton btnGoodRearCCD = new JButton();
  private JButton btnBadRearCCD = new JButton();
  private boolean _goodFrontCCD;
  private boolean _goodRearCCD;
  transient private PropertyChangeSupport propertyChangeListeners = new PropertyChangeSupport(this);
  private boolean _frontDecided;
  private boolean _rearDecided;
  private boolean _bothDecided;
  private BorderLayout borderLayout5 = new BorderLayout();
  private JPanel frontCameraImagePanel = new JPanel();
  private BorderLayout borderLayout6 = new BorderLayout();
  private JPanel rearCameraImagePanel = new JPanel();

  public ConfirmLineScanCameraPixelsPanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    set_frontDecided(false);
    set_rearDecided(false);
    box1 = Box.createHorizontalBox();
    box2 = Box.createHorizontalBox();
    this.setLayout(borderLayout1);
    confirmLineScanCameraPixelsLabel.setFont(new java.awt.Font("Dialog", 1, 18));
    confirmLineScanCameraPixelsLabel.setHorizontalAlignment(SwingConstants.CENTER);
    confirmLineScanCameraPixelsLabel.setText("Confirm Line Scan Camera Pixels");
    confirmLineScanCameraPixelsPanel.setLayout(borderLayout2);
    cameraPixelSplitPane.setDividerSize(5);
    jPanel1.setLayout(borderLayout3);
    jPanel2.setLayout(borderLayout4);
    btnGoodFrontCCD.setText("Good CCD");
    btnGoodFrontCCD.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnGoodFrontCCD_actionPerformed(e);
      }
    });
    btnBadFrontCCD.setText("Bad  CCD");
    btnBadFrontCCD.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnBadFrontCCD_actionPerformed(e);
      }
    });
    btnGoodRearCCD.setText("Good CCD");
    btnGoodRearCCD.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnGoodRearCCD_actionPerformed(e);
      }
    });
    btnBadRearCCD.setText("Bad  CCD");
    btnBadRearCCD.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnBadRearCCD_actionPerformed(e);
      }
    });
    this.setMaximumSize(new Dimension(800, 400));
    this.setMinimumSize(new Dimension(800, 400));
    this.setPreferredSize(new Dimension(800, 400));
    frontCameraImagePanel.setLayout(borderLayout5);
    rearCameraImagePanel.setLayout(borderLayout6);
    this.add(confirmLineScanCameraPixelsPanel, BorderLayout.CENTER);
    confirmLineScanCameraPixelsPanel.add(confirmLineScanCameraPixelsLabel, BorderLayout.NORTH);
    confirmLineScanCameraPixelsPanel.add(cameraPixelSplitPane, BorderLayout.CENTER);
    cameraPixelSplitPane.add(jPanel1, JSplitPane.LEFT);
    jPanel1.add(jPanel3, BorderLayout.SOUTH);
    jPanel3.add(box1, null);
    box1.add(btnGoodFrontCCD, null);
    box1.add(btnBadFrontCCD, null);
    jPanel1.add(frontCameraImagePanel, BorderLayout.CENTER);
    cameraPixelSplitPane.add(jPanel2, JSplitPane.RIGHT);
    jPanel2.add(jPanel4, BorderLayout.SOUTH);
    jPanel4.add(box2, null);
    box2.add(btnGoodRearCCD, null);
    box2.add(btnBadRearCCD, null);
    jPanel2.add(rearCameraImagePanel, BorderLayout.CENTER);
    double bob = .5;
    cameraPixelSplitPane.setDividerLocation (bob);
    frontCameraImagePanel.add(new ScreenRendering("d:\\matrixcouponfortonyttif.gif",0,0,300,300), BorderLayout.CENTER);
    rearCameraImagePanel.add(new ScreenRendering("d:\\matrixcouponfortonyttif.gif",300,0,300,300), BorderLayout.CENTER);

  }
  public void set_goodFrontCCD(boolean new_goodFrontCCD)
  {
    _goodFrontCCD = new_goodFrontCCD;
    set_frontDecided(true);
    if ((_frontDecided) && (_rearDecided))
    {
      set_bothDecided(true);
    }

  }
  public boolean is_goodFrontCCD()
  {
    return _goodFrontCCD;
  }
  public void set_goodRearCCD(boolean new_goodRearCCD)
  {
    _goodRearCCD = new_goodRearCCD;
    set_rearDecided(true);
    if ((_frontDecided) && (_rearDecided))
    {
      set_bothDecided(true);
    }
  }
  public boolean is_goodRearCCD()
  {
    return _goodRearCCD;
  }
  public synchronized void removePropertyChangeListener(PropertyChangeListener l)
  {
    super.removePropertyChangeListener(l);
    propertyChangeListeners.removePropertyChangeListener(l);
  }
  public synchronized void addPropertyChangeListener(PropertyChangeListener l)
  {
    super.addPropertyChangeListener(l);
    propertyChangeListeners.addPropertyChangeListener(l);
  }
  public void set_frontDecided(boolean new_frontDecided)
  {
    _frontDecided = new_frontDecided;
  }
  public boolean is_frontDecided()
  {
    return _frontDecided;
  }
  public void set_rearDecided(boolean new_rearDecided)
  {
    _rearDecided = new_rearDecided;
  }
  public boolean is_rearDecided()
  {
    return _rearDecided;
  }
  public void set_bothDecided(boolean new_bothDecided)
  {
/** @todo act _ go back to frmCalibration and add listener adapter code for these four cases next */
    boolean  old_bothDecided = _bothDecided;
    _bothDecided = new_bothDecided;
    if (is_goodFrontCCD() && is_goodRearCCD())
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontCCDGood_rearCCDGood", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }
    else if (! is_goodFrontCCD() && is_goodRearCCD())
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontCCDBad_rearCCDGood", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }
    else if (is_goodFrontCCD() && ! is_goodRearCCD())
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontCCDGood_rearCCDBad", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }
    else
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontCCDBad_rearCCDBad", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }

  }
  public boolean is_bothDecided()
  {
    return _bothDecided;
  }

  void btnGoodFrontCCD_actionPerformed(ActionEvent e)
  {
    set_goodFrontCCD(true);
    btnGoodFrontCCD.setBackground(Color.green);
    btnBadFrontCCD.setBackground(Color.lightGray);
  }

  void btnBadFrontCCD_actionPerformed(ActionEvent e)
  {
    set_goodFrontCCD(false);
    btnGoodFrontCCD.setBackground(Color.lightGray);
    btnBadFrontCCD.setBackground(Color.red);
  }

  void btnGoodRearCCD_actionPerformed(ActionEvent e)
  {
    set_goodRearCCD(true);
    btnGoodRearCCD.setBackground(Color.green);
    btnBadRearCCD.setBackground(Color.lightGray);
  }

  void btnBadRearCCD_actionPerformed(ActionEvent e)
  {
    set_goodRearCCD(false);
    btnGoodRearCCD.setBackground(Color.lightGray);
    btnBadRearCCD.setBackground(Color.red);
  }

}
