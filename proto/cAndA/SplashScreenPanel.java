package com.agilent.xRayTest.gui.cAndA;

import java.awt.*;
import javax.swing.*;

/**
 * Title:        5DX Calibration Utilities
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Tony Turner
 * @version 1.0
 */

public class SplashScreenPanel extends JPanel
{
  BorderLayout borderLayout1 = new BorderLayout();
  private JPanel splashScreenPanel = new JPanel();
  private JLabel splashScreenLabel = new JLabel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private Box box1;
  private JLabel jLabel1 = new JLabel();
  private JLabel jLabel2 = new JLabel();
  private JLabel jLabel3 = new JLabel();
  private JLabel jLabel4 = new JLabel();
  private JLabel jLabel5 = new JLabel();
  private JLabel jLabel6 = new JLabel();

  public SplashScreenPanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    box1 = Box.createVerticalBox();
    this.setLayout(borderLayout1);
    splashScreenLabel.setFont(new java.awt.Font("Dialog", 1, 18));
    splashScreenLabel.setHorizontalAlignment(SwingConstants.CENTER);
    splashScreenLabel.setText("5DX Surface Map Subsystem Calibration");
    splashScreenPanel.setLayout(borderLayout2);
    jLabel2.setToolTipText("");
    jLabel2.setText("Version 1.0.0");
    jLabel3.setToolTipText("");
    jLabel3.setText("                 ");
    jLabel4.setText("Agilent Technologies 5DX Calibration Utility");
    jLabel1.setPreferredSize(new Dimension(10, 10));
    jLabel1.setHorizontalAlignment(SwingConstants.CENTER);
    jLabel1.setText("                                        ");
    jLabel5.setText("                       ");
    jLabel6.setText("Copyright 2001");
    this.add(splashScreenPanel, BorderLayout.CENTER);
    splashScreenPanel.add(splashScreenLabel, BorderLayout.NORTH);
    splashScreenPanel.add(box1, BorderLayout.CENTER);
    box1.add(jLabel1, null);
    box1.add(jLabel4, null);
    box1.add(jLabel3, null);
    box1.add(jLabel2, null);
    box1.add(jLabel5, null);
    box1.add(jLabel6, null);
  }
}