package com.agilent.xRayTest.gui.cAndA;

import java.awt.*;
import javax.swing.*;
import java.beans.*;
import java.awt.event.*;

/**
 * Title:        5DX Calibration Utilities
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Tony Turner
 * @version 1.0
 */

public class ConfirmPhotogrammetryLightSourcePanel extends JPanel
{
  BorderLayout borderLayout1 = new BorderLayout();
  private JPanel confirmPhotogrammetryLightSourcePanel = new JPanel();
  private JLabel confirmPhotogrammetryLightSourceLabel = new JLabel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private JPanel jPanel1 = new JPanel();
  //private XYLayout xYLayout1 = new XYLayout();
  private JTextField jTextField2 = new JTextField();
  private JTextField jTextField1 = new JTextField();
  private JLabel jLabel2 = new JLabel();
  private JLabel jLabel1 = new JLabel();
  private JLabel jLabel6 = new JLabel();
  private JLabel jLabel7 = new JLabel();
  private JTextField jTextField4 = new JTextField();
  private JTextField jTextField3 = new JTextField();
  private JLabel jLabel4 = new JLabel();
  private JLabel jLabel3 = new JLabel();
  private JTextField jTextField7 = new JTextField();
  private JLabel jLabel9 = new JLabel();
  private JButton btnContinue = new JButton();
  private JRadioButton jRadioButton1 = new JRadioButton();
  private JRadioButton jRadioButton2 = new JRadioButton();
  private JRadioButton jRadioButton3 = new JRadioButton();
  private JRadioButton jRadioButton4 = new JRadioButton();
  private boolean _lightSourceConfirmed;
  transient private PropertyChangeSupport propertyChangeListeners = new PropertyChangeSupport(this);

  public ConfirmPhotogrammetryLightSourcePanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    confirmPhotogrammetryLightSourceLabel.setFont(new java.awt.Font("Dialog", 1, 18));
    confirmPhotogrammetryLightSourceLabel.setHorizontalAlignment(SwingConstants.CENTER);
    confirmPhotogrammetryLightSourceLabel.setText("Confirm Photogrammetry Light Source");
    confirmPhotogrammetryLightSourcePanel.setLayout(borderLayout2);
    //jPanel1.setLayout(xYLayout1);
    jTextField2.setText("180.00");
    jTextField1.setText("200.00");
    jLabel2.setText("Camera Reading GL");
    jLabel1.setText("Commanded GL");
    jLabel6.setText("Front Camera");
    jLabel7.setText("Rear Camera");
    jTextField4.setText("200.00");
    jTextField3.setText("180.00");
    jLabel4.setText("Commanded GL");
    jLabel3.setText("Camera Reading GL");
    jTextField7.setText("180.00");
    jLabel9.setText("Sensor Reading GL");
    btnContinue.setText("Continue");
    btnContinue.addActionListener(new ConfirmPhotogrammetryLightSourcePanel_btnContinue_actionAdapter(this));
    jRadioButton1.setText("Light Sensor not functioning properly");
    jRadioButton2.setText("Light Source not functioning properly");
    jRadioButton3.setText("Front Camera not functioning properly");
    jRadioButton4.setText("Rear Camera not functioning properly");
    this.add(confirmPhotogrammetryLightSourcePanel, BorderLayout.CENTER);
    confirmPhotogrammetryLightSourcePanel.add(confirmPhotogrammetryLightSourceLabel, BorderLayout.NORTH);
    confirmPhotogrammetryLightSourcePanel.add(jPanel1, BorderLayout.CENTER);
//    jPanel1.add(jLabel4, new XYConstraints(203, 39, -1, -1));
//    jPanel1.add(jLabel3, new XYConstraints(203, 69, -1, -1));
//    jPanel1.add(jTextField1, new XYConstraints(129, 39, -1, -1));
//    jPanel1.add(jLabel1, new XYConstraints(7, 39, -1, -1));
//    jPanel1.add(jTextField4, new XYConstraints(331, 39, -1, -1));
//    jPanel1.add(jLabel2, new XYConstraints(7, 69, -1, -1));
//    jPanel1.add(jLabel9, new XYConstraints(70, 99, -1, -1));
//    jPanel1.add(jTextField7, new XYConstraints(196, 98, -1, -1));
//    jPanel1.add(jRadioButton1, new XYConstraints(53, 127, -1, -1));
//    jPanel1.add(jRadioButton2, new XYConstraints(53, 148, -1, -1));
//    jPanel1.add(jRadioButton3, new XYConstraints(53, 170, -1, -1));
//    jPanel1.add(jRadioButton4, new XYConstraints(53, 191, -1, -1));
//    jPanel1.add(btnContinue, new XYConstraints(141, 234, -1, -1));
//    jPanel1.add(jTextField2, new XYConstraints(128, 69, -1, -1));
//    jPanel1.add(jTextField3, new XYConstraints(331, 69, -1, -1));
//    jPanel1.add(jLabel6, new XYConstraints(48, 5, -1, -1));
//    jPanel1.add(jLabel7, new XYConstraints(254, 5, -1, -1));
  }
  public void set_lightSourceConfirmed(boolean new_lightSourceConfirmed)
  {
    boolean  oldLightSourceConfirmed = _lightSourceConfirmed;
    propertyChangeListeners.firePropertyChange("isLightSourceConfirmed", new Boolean(oldLightSourceConfirmed), new Boolean(new_lightSourceConfirmed));
    _lightSourceConfirmed = new_lightSourceConfirmed;
  }
  public boolean is_lightSourceConfirmed()
  {
    return _lightSourceConfirmed;
  }
  public synchronized void removePropertyChangeListener(PropertyChangeListener l)
  {
    super.removePropertyChangeListener(l);
    propertyChangeListeners.removePropertyChangeListener(l);
  }
  public synchronized void addPropertyChangeListener(PropertyChangeListener l)
  {
    super.addPropertyChangeListener(l);
    propertyChangeListeners.addPropertyChangeListener(l);
  }

  void btnContinue_actionPerformed(ActionEvent e)
  {
    set_lightSourceConfirmed(true);
  }

}

class ConfirmPhotogrammetryLightSourcePanel_btnContinue_actionAdapter implements java.awt.event.ActionListener
{
  private ConfirmPhotogrammetryLightSourcePanel adaptee;

  ConfirmPhotogrammetryLightSourcePanel_btnContinue_actionAdapter(ConfirmPhotogrammetryLightSourcePanel adaptee)
  {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e)
  {
    adaptee.btnContinue_actionPerformed(e);
  }
}