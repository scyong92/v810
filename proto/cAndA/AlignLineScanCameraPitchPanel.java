package com.agilent.xRayTest.gui.cAndA;

import java.awt.*;
import javax.swing.*;
import java.beans.*;
import java.awt.event.*;
import java.awt.image.*;

/**
 * Title:        5DX Calibration Utilities
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Tony Turner
 * @version 1.0
 */

public class AlignLineScanCameraPitchPanel extends JPanel
{
  BorderLayout borderLayout1 = new BorderLayout();
  private JPanel alignLineScanCameraPitchPanel = new JPanel();
  private JLabel alignLineScanCameraPitchLabel = new JLabel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private JSplitPane jSplitPane1 = new JSplitPane();
  private JPanel jPanel1 = new JPanel();
  private JPanel jPanel2 = new JPanel();
  private BorderLayout borderLayout3 = new BorderLayout();
  private BorderLayout borderLayout4 = new BorderLayout();
  private JPanel jPanel3 = new JPanel();
  private Box box1;
  private JButton btnGoodFrontPitchAngle = new JButton();
  private JButton btnBadFrontPitchAngle = new JButton();
  private JPanel jPanel4 = new JPanel();
  private Box box2;
  private JButton btnGoodRearPitchAngle = new JButton();
  private JButton btnBadRearPitchAngle = new JButton();
  private JLabel jLabel2 = new JLabel();
  private boolean _goodFrontPitchAngle;
  private boolean _goodRearPitchAngle;
  transient private PropertyChangeSupport propertyChangeListeners = new PropertyChangeSupport(this);
  private boolean _frontDecided;
  private boolean _rearDecided;
  private boolean _bothDecided;
  private JScrollPane frontCameraImageScrollPane = new JScrollPane();

  public AlignLineScanCameraPitchPanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    MediaTracker tracker = new MediaTracker(this);
    String frontImageName = new String("c:\\matrixcouponfortonyttif.gif");
    Image frontCameraImage = Toolkit.getDefaultToolkit().getImage(frontImageName);
    tracker.addImage(frontCameraImage,0);
    try
    {
      tracker.waitForAll();
    }
    catch(InterruptedException ie)
    {
      throw new Error("Could not load " + frontImageName
                             + " with MediaTracker");
    }

    frontCameraImageScrollPane.add(new JLabel(new ImageIcon(frontCameraImage)));

    set_frontDecided(false);
    set_rearDecided(false);
    box1 = Box.createHorizontalBox();
    box2 = Box.createHorizontalBox();
    this.setLayout(borderLayout1);
    alignLineScanCameraPitchLabel.setFont(new java.awt.Font("Dialog", 1, 18));
    alignLineScanCameraPitchLabel.setHorizontalAlignment(SwingConstants.CENTER);
    alignLineScanCameraPitchLabel.setText("Align Line Scan Camera Pitch Angle");
    alignLineScanCameraPitchPanel.setLayout(borderLayout2);
    jSplitPane1.setDividerSize(5);
    jPanel1.setLayout(borderLayout3);
    jPanel2.setLayout(borderLayout4);
    btnGoodFrontPitchAngle.setText("Aligned");
    btnGoodFrontPitchAngle.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnGoodFrontPitchAngle_actionPerformed(e);
      }
    });
    btnBadFrontPitchAngle.setText("Can\'t Align");
    btnBadFrontPitchAngle.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnBadFrontPitchAngle_actionPerformed(e);
      }
    });
    btnGoodRearPitchAngle.setText("Aligned");
    btnGoodRearPitchAngle.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnGoodRearPitchAngle_actionPerformed(e);
      }
    });
    btnBadRearPitchAngle.setText("Can\'t Align");
    btnBadRearPitchAngle.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnBadRearPitchAngle_actionPerformed(e);
      }
    });
    jLabel2.setText("Rear Camera Image Goes Here");
    this.add(alignLineScanCameraPitchPanel, BorderLayout.CENTER);
    alignLineScanCameraPitchPanel.add(alignLineScanCameraPitchLabel, BorderLayout.NORTH);
    alignLineScanCameraPitchPanel.add(jSplitPane1, BorderLayout.CENTER);
    jSplitPane1.add(jPanel1, JSplitPane.LEFT);
    jPanel1.add(jPanel3, BorderLayout.SOUTH);
    jPanel3.add(box1, null);
    box1.add(btnGoodFrontPitchAngle, null);
    box1.add(btnBadFrontPitchAngle, null);
    jPanel1.add(frontCameraImageScrollPane, BorderLayout.CENTER);
    jSplitPane1.add(jPanel2, JSplitPane.RIGHT);
    jPanel2.add(jPanel4, BorderLayout.SOUTH);
    jPanel4.add(box2, null);
    box2.add(btnGoodRearPitchAngle, null);
    box2.add(btnBadRearPitchAngle, null);
    jPanel2.add(jLabel2, BorderLayout.CENTER);
    jSplitPane1.setDividerLocation(190);
  }
  public void set_goodFrontPitchAngle(boolean new_goodFrontPitchAngle)
  {
    _goodFrontPitchAngle = new_goodFrontPitchAngle;
    set_frontDecided(true);
    if ((_frontDecided) && (_rearDecided))
    {
      set_bothDecided(true);
    }

  }
  public boolean is_goodFrontPitchAngle()
  {
    return _goodFrontPitchAngle;
  }
  public void set_goodRearPitchAngle(boolean new_goodRearPitchAngle)
  {
    _goodRearPitchAngle = new_goodRearPitchAngle;
    set_rearDecided(true);
    if ((_frontDecided) && (_rearDecided))
    {
      set_bothDecided(true);
    }
  }
  public boolean is_goodRearPitchAngle()
  {
    return _goodRearPitchAngle;
  }
  public synchronized void removePropertyChangeListener(PropertyChangeListener l)
  {
    super.removePropertyChangeListener(l);
    propertyChangeListeners.removePropertyChangeListener(l);
  }
  public synchronized void addPropertyChangeListener(PropertyChangeListener l)
  {
    super.addPropertyChangeListener(l);
    propertyChangeListeners.addPropertyChangeListener(l);
  }
  public void set_frontDecided(boolean new_frontDecided)
  {
    _frontDecided = new_frontDecided;
  }
  public boolean is_frontDecided()
  {
    return _frontDecided;
  }
  public void set_rearDecided(boolean new_rearDecided)
  {
    _rearDecided = new_rearDecided;
  }
  public boolean is_rearDecided()
  {
    return _rearDecided;
  }
  public void set_bothDecided(boolean new_bothDecided)
  {
/** @todo act _ go back to frmCalibration and add listener adapter code for these four cases next */
    boolean  old_bothDecided = _bothDecided;
    _bothDecided = new_bothDecided;
    if (is_goodFrontPitchAngle() && is_goodRearPitchAngle())
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontPitchGood_rearPitchGood", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }
    else if (! is_goodFrontPitchAngle() && is_goodRearPitchAngle())
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontPitchBad_rearPitchGood", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }
    else if (is_goodFrontPitchAngle() && ! is_goodRearPitchAngle())
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontPitchGood_rearPitchBad", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }
    else
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontPitchBad_rearPitchBad", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }

  }
  public boolean is_bothDecided()
  {
    return _bothDecided;
  }

  void btnGoodFrontPitchAngle_actionPerformed(ActionEvent e)
  {
    set_goodFrontPitchAngle(true);
    btnGoodFrontPitchAngle.setBackground(Color.green);
    btnBadFrontPitchAngle.setBackground(Color.lightGray);
  }

  void btnBadFrontPitchAngle_actionPerformed(ActionEvent e)
  {
    set_goodFrontPitchAngle(false);
    btnGoodFrontPitchAngle.setBackground(Color.lightGray);
    btnBadFrontPitchAngle.setBackground(Color.red);
  }

  void btnGoodRearPitchAngle_actionPerformed(ActionEvent e)
  {
    set_goodRearPitchAngle(true);
    btnGoodRearPitchAngle.setBackground(Color.green);
    btnBadRearPitchAngle.setBackground(Color.lightGray);
  }

  void btnBadRearPitchAngle_actionPerformed(ActionEvent e)
  {
    set_goodRearPitchAngle(false);
    btnGoodRearPitchAngle.setBackground(Color.lightGray);
    btnBadRearPitchAngle.setBackground(Color.red);
  }

}
