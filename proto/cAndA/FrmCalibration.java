package com.agilent.xRayTest.gui.cAndA;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.beans.*;
import javax.swing.border.*;
import java.util.*;
import com.agilent.xRayTest.hardware.*;
import com.agilent.xRayTest.gui.cAndA.*;
/**
 * Title:        5DX Calibration Utilities
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Tony Turner
 * @version 1.0
 */

public class FrmCalibration extends JFrame implements PropertyChangeListener
{
  // SurfaceMappingCalibration _surfaceMappingCalibration =
  //                          new SurfaceMappingCalibration();
  JPanel contentPane;
  JMenuBar jMenuBar1 = new JMenuBar();
  JMenu jMenuFile = new JMenu();
  JMenuItem jMenuFileExit = new JMenuItem();
  JMenu jMenuHelp = new JMenu();
  JMenuItem jMenuHelpAbout = new JMenuItem();
  JToolBar jToolBar = new JToolBar();
  JButton jButton1 = new JButton();
  JButton jButton2 = new JButton();
  JButton jButton3 = new JButton();
  ImageIcon image1;
  ImageIcon image2;
  ImageIcon image3;
  JLabel statusBar = new JLabel();
  BorderLayout borderLayout1 = new BorderLayout();
  JPanel calibrationMainPanel = new JPanel();
  BorderLayout borderLayout2 = new BorderLayout();
  JSplitPane calibrationMainSplitPane = new JSplitPane();
  JPanel calibrationPanel = new JPanel();
  JPanel utilityPanel = new JPanel();
  BorderLayout borderLayout4 = new BorderLayout();
  JSplitPane utilityMainSplitPane = new JSplitPane();
  JPanel utilityDescriptionAndStatusPanel = new JPanel();
  JPanel utilityCardsPanel = new JPanel();
  BorderLayout borderLayout5 = new BorderLayout();
  JSplitPane utilityDescriptionAndStatusSplitPane = new JSplitPane();
  JPanel utilityDescriptionPanel = new JPanel();
  JPanel utilityStatusPanel = new JPanel();
  BorderLayout borderLayout6 = new BorderLayout();
  BorderLayout borderLayout7 = new BorderLayout();
  JScrollPane utilityDescriptionScrollPane = new JScrollPane();
  JScrollPane utilityStatusScrollPane = new JScrollPane();
  JTextArea utilityDescriptionTextArea = new JTextArea();
  JTextArea utilityStatusTextArea = new JTextArea();

  SplashScreenPanel splashScreenPanel = new SplashScreenPanel();

  InitialLineScanCameraAssemblySetupPanel initialLineScanCameraAssemblySetupPanel = new InitialLineScanCameraAssemblySetupPanel();

  ConfirmLineScanCameraPixelsPanel confirmLineScanCameraPixelsPanel = new ConfirmLineScanCameraPixelsPanel();
  ConfirmPhotogrammetryLightSourcePanel confirmPhotogrammetryLightSourcePanel = new ConfirmPhotogrammetryLightSourcePanel();

  AlignLineScanCameraPitchPanel alignLineScanCameraPitchPanel = new AlignLineScanCameraPitchPanel();
  AlignLineScanCameraYawPanel alignLineScanCameraYawPanel = new AlignLineScanCameraYawPanel();
  AdjustLineScanCameraFocalDistancePanel adjustLineScanCameraFocalDistancePanel = new AdjustLineScanCameraFocalDistancePanel();
  AlignLineScanCameraPairFieldsOfViewPanel alignLineScanCameraPairFieldsOfViewPanel = new AlignLineScanCameraPairFieldsOfViewPanel();
  FinalBenchTestPhotogrammetrySubsystemPanel finalBenchTestPhotogrammetrySubsystemPanel = new FinalBenchTestPhotogrammetrySubsystemPanel();

  CardLayout cardLayout1 = new CardLayout();

  private JButton btnInitialSetup = new JButton();

  private JButton btnConfirmLightSource = new JButton();
  private JButton btnConfirmCCDPixels = new JButton();

  private JButton btnAlignCameraPitch = new JButton();
  private JButton btnAlignCameraYaw = new JButton();
  private JButton btnAdjustFocus = new JButton();
  private JButton btnAlignFieldOfViews = new JButton();
  private JButton btnFinalTest = new JButton();

  private FlowLayout flowLayout1 = new FlowLayout();
  private JLabel lblSurfMap = new JLabel();
  private TitledBorder titledBorder1;
  private TitledBorder titledBorder2;

  private boolean _initialSetupComplete;
  private boolean _lightSourceFunctional;
  private boolean _cameraCCDPixelsGood;
  private boolean _cameraPitchCorrect;
  private boolean _cameraPairYawCorrect;
  private boolean _cameraPairFocalDistanceCorrect;
  private boolean _cameraPairFOVOverlappingCorrect;
  private boolean _cameraPairFinalBenchTestGood;
  private boolean _frontCameraCCDPixelsGood;
  private boolean _rearCameraCCDPixelsGood;
  private boolean _frontCameraFocalDistanceCorrect;
  private boolean _rearCameraFocalDistanceCorrect;
  private boolean _frontCameraPitchCorrect;
  private boolean _rearCameraPitchCorrect;
  private boolean _frontCameraYawCorrect;
  private boolean _rearCameraYawCorrect;

  private LineScanCameraPairInstalledEnum _installed;
  private JButton btnAdjustMagnification = new JButton();
  private SurfaceMappingCalibration _surfaceMappingCalibration;
  private LineScanCameras _lineScanCameras;
          // =
          //       LineScanCameraAcquisitionProperties.getCameraPairInstalled();

  /**Construct the frame*/
  public FrmCalibration() {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }
  /**Component initialization*/
  private void jbInit() throws Exception  {
  //_installed = LineScanCameraAcquisitionProperties.getCameraPairInstalled();
  _installed = LineScanCameraPairInstalledEnum.TOP_AND_BOTTOM_PAIR_INSTALLED;;
    image1 = new ImageIcon(FrmCalibration.class.getResource("openFile.gif"));
    image2 = new ImageIcon(FrmCalibration.class.getResource("closeFile.gif"));
    image3 = new ImageIcon(FrmCalibration.class.getResource("help.gif"));
    //setIconImage(Toolkit.getDefaultToolkit().createImage(FrmCalibration.class.getResource("[Your Icon]")));
    contentPane = (JPanel) this.getContentPane();
    titledBorder1 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),2),"Next Step");
    titledBorder2 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),2),"Status");
    contentPane.setLayout(borderLayout1);
//    this.setSize(new Dimension(792, 527));
    this.setSize(new Dimension(900, 600));
    this.setTitle("5DX Calibration Utilities");
    statusBar.setText(" ");
    jMenuFile.setText("File");
    jMenuFileExit.setText("Exit");
    jMenuFileExit.addActionListener( new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jMenuFileExit_actionPerformed(e);
      }
    });
    jMenuHelp.setText("Help");
    jMenuHelpAbout.setText("About");
    jMenuHelpAbout.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jMenuHelpAbout_actionPerformed(e);
      }
    });
    jButton1.setIcon(image1);
    jButton1.setToolTipText("Open File");
    jButton2.setIcon(image2);
    jButton2.setToolTipText("Close File");
    jButton3.setIcon(image3);
    jButton3.setToolTipText("Help");
    calibrationMainPanel.setLayout(borderLayout2);
    calibrationPanel.setLayout(flowLayout1);
    utilityPanel.setLayout(borderLayout4);
    utilityMainSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
    utilityDescriptionAndStatusPanel.setLayout(borderLayout5);
    utilityDescriptionPanel.setLayout(borderLayout6);
    utilityStatusPanel.setLayout(borderLayout7);
    utilityCardsPanel.setLayout(cardLayout1);
    utilityDescriptionTextArea.setBorder(titledBorder1);
    utilityDescriptionTextArea.setText("");
    utilityStatusTextArea.setBorder(titledBorder2);
    utilityStatusTextArea.setText("");

    lblSurfMap.setHorizontalAlignment(SwingConstants.CENTER);
    lblSurfMap.setText("Test Steps");

    btnInitialSetup.setNextFocusableComponent(btnConfirmLightSource);
    btnInitialSetup.setToolTipText("Initial Line Scan Camera Assembly Setup");
    btnInitialSetup.setFocusPainted(true);
    btnInitialSetup.setText("Initial Setup");
    btnInitialSetup.setActionCommand("Initial Line Scan Camera Assembly Setup");
    btnInitialSetup.setEnabled(true);
    btnInitialSetup.addActionListener(new CalibrationCommandActionAdapter(this));

    btnConfirmLightSource.setNextFocusableComponent(btnConfirmCCDPixels);
    btnConfirmLightSource.setPreferredSize(new Dimension(169, 27));
    btnConfirmLightSource.setToolTipText("Confirm Photogrammetry Light Source");
    btnConfirmLightSource.setText("Confirm Light & Cameras");
    btnConfirmLightSource.setActionCommand("Confirm Photogrammetry Light Source");
    btnConfirmLightSource.setEnabled(false);
    btnConfirmLightSource.addActionListener(new CalibrationCommandActionAdapter(this));

    btnConfirmCCDPixels.setNextFocusableComponent(btnAlignCameraPitch);
    btnConfirmCCDPixels.setToolTipText("Confirm Line Scan Camera Pixels");
    btnConfirmCCDPixels.setText("Confirm Pixels");
    btnConfirmCCDPixels.setActionCommand("Confirm Line Scan Camera Pixels");
    btnConfirmCCDPixels.setEnabled(false);
    btnConfirmCCDPixels.addActionListener(new CalibrationCommandActionAdapter(this));

    btnAlignCameraPitch.setNextFocusableComponent(btnAlignCameraYaw);
    btnAlignCameraPitch.setToolTipText("Align Line Scan Camera Pitch");
    btnAlignCameraPitch.setText("Align Camera Pitch");
    btnAlignCameraPitch.setActionCommand("Align Line Scan Camera Pitch");
    btnAlignCameraPitch.setEnabled(false);
    btnAlignCameraPitch.addActionListener(new CalibrationCommandActionAdapter(this));

    btnAlignCameraYaw.setNextFocusableComponent(btnAdjustFocus);
    btnAlignCameraYaw.setToolTipText("Align Line Scan Camera Yaw");
    btnAlignCameraYaw.setText("Align Camera Yaw");
    btnAlignCameraYaw.setActionCommand("Align Line Scan Camera Yaw");
    btnAlignCameraYaw.setEnabled(false);
    btnAlignCameraYaw.addActionListener(new CalibrationCommandActionAdapter(this));

    btnAdjustFocus.setNextFocusableComponent(btnAlignFieldOfViews);
    btnAdjustFocus.setToolTipText("Adjust Line Scan Camera Focus");
    btnAdjustFocus.setText("Adjust Focus");
    btnAdjustFocus.setActionCommand("Adjust Line Scan Camera Focus");
    btnAdjustFocus.setEnabled(false);
    btnAdjustFocus.addActionListener(new CalibrationCommandActionAdapter(this));

    btnAlignFieldOfViews.setNextFocusableComponent(btnFinalTest);
    btnAlignFieldOfViews.setToolTipText("ensure both cameras point to the same place");
    btnAlignFieldOfViews.setText("Align Fields Of View");
    btnAlignFieldOfViews.setActionCommand("Align Line Scan Camera Pair Fields Of View");
    btnAlignFieldOfViews.setEnabled(false);
    btnAlignFieldOfViews.addActionListener(new CalibrationCommandActionAdapter(this));

    btnFinalTest.setBackground(Color.white);
    btnFinalTest.setToolTipText("Final Bench Test Photogrammetry Subsystem");
    btnFinalTest.setText("Final Test");
    btnFinalTest.setActionCommand("Final Bench Test Photogrammetry Subsystem");
    btnFinalTest.setEnabled(false);
    btnFinalTest.addActionListener(new CalibrationCommandActionAdapter(this));

    utilityDescriptionTextArea.setEditable(false);
    utilityDescriptionTextArea.setLineWrap(true);
    utilityStatusTextArea.setEditable(false);
    utilityStatusTextArea.setLineWrap(true);

    initialLineScanCameraAssemblySetupPanel.addPropertyChangeListener(this);

    confirmLineScanCameraPixelsPanel.addPropertyChangeListener(this);
    confirmPhotogrammetryLightSourcePanel.addPropertyChangeListener(this);

    alignLineScanCameraPitchPanel.addPropertyChangeListener(this);
    alignLineScanCameraYawPanel.addPropertyChangeListener(this);
    adjustLineScanCameraFocalDistancePanel.addPropertyChangeListener(this);
    alignLineScanCameraPairFieldsOfViewPanel.addPropertyChangeListener(this);

    finalBenchTestPhotogrammetrySubsystemPanel.addPropertyChangeListener(this);

    calibrationMainPanel.setPreferredSize(new Dimension(1000, 350));
    btnAdjustMagnification.setEnabled(false);
    btnAdjustMagnification.setText("Adjust Magnification");
    jToolBar.add(jButton1);
    jToolBar.add(jButton2);
    jToolBar.add(jButton3);
    jMenuFile.add(jMenuFileExit);
    jMenuHelp.add(jMenuHelpAbout);
    jMenuBar1.add(jMenuFile);
    jMenuBar1.add(jMenuHelp);
    this.setJMenuBar(jMenuBar1);
    contentPane.add(jToolBar, BorderLayout.NORTH);
    contentPane.add(statusBar, BorderLayout.SOUTH);
    contentPane.add(calibrationMainPanel, BorderLayout.CENTER);
    calibrationMainPanel.add(calibrationMainSplitPane, BorderLayout.CENTER);
    calibrationMainSplitPane.add(calibrationPanel, JSplitPane.LEFT);
    calibrationPanel.add(lblSurfMap, null);
    calibrationPanel.add(btnInitialSetup, null);
    calibrationPanel.add(btnConfirmLightSource, null);
    calibrationPanel.add(btnConfirmCCDPixels, null);
    calibrationPanel.add(btnAlignCameraPitch, null);
    calibrationPanel.add(btnAlignCameraYaw, null);
    calibrationPanel.add(btnAdjustMagnification, null);
    calibrationPanel.add(btnAdjustFocus, null);
    calibrationPanel.add(btnAlignFieldOfViews, null);
    calibrationPanel.add(btnFinalTest, null);
    calibrationMainSplitPane.add(utilityPanel, JSplitPane.RIGHT);
    utilityPanel.add(utilityMainSplitPane, BorderLayout.CENTER);
    utilityMainSplitPane.add(utilityDescriptionAndStatusPanel, JSplitPane.BOTTOM);
    utilityDescriptionAndStatusPanel.add(utilityDescriptionAndStatusSplitPane, BorderLayout.CENTER);
    utilityDescriptionAndStatusSplitPane.add(utilityDescriptionPanel, JSplitPane.LEFT);
    utilityDescriptionPanel.add(utilityDescriptionScrollPane, BorderLayout.CENTER);
    utilityDescriptionScrollPane.getViewport().add(utilityDescriptionTextArea, null);
    utilityDescriptionAndStatusSplitPane.add(utilityStatusPanel, JSplitPane.RIGHT);
    utilityStatusPanel.add(utilityStatusScrollPane, BorderLayout.CENTER);
    utilityStatusScrollPane.getViewport().add(utilityStatusTextArea, null);
    utilityMainSplitPane.add(utilityCardsPanel, JSplitPane.TOP);

    utilityCardsPanel.add( splashScreenPanel, "5DX Photogrammetry Subsystem Bench Calibration Utility");
    utilityCardsPanel.add( initialLineScanCameraAssemblySetupPanel, "Initial Line Scan Camera Assembly Setup");
    utilityCardsPanel.add( confirmPhotogrammetryLightSourcePanel, "Confirm Photogrammetry Light Source");
    utilityCardsPanel.add( confirmLineScanCameraPixelsPanel, "Confirm Line Scan Camera Pixels");
    utilityCardsPanel.add(alignLineScanCameraPitchPanel, "Align Line Scan Camera Pitch");
    utilityCardsPanel.add( alignLineScanCameraYawPanel, "Align Line Scan Camera Yaw");
    utilityCardsPanel.add(adjustLineScanCameraFocalDistancePanel, "Adjust Line Scan Camera Focal Distance");
    utilityCardsPanel.add(alignLineScanCameraPairFieldsOfViewPanel, "Align Line Scan Camera Pair Fields Of View");
    utilityCardsPanel.add( finalBenchTestPhotogrammetrySubsystemPanel, "Final Bench Test Photogrammetry Subsystem");

    ((CardLayout)utilityCardsPanel.getLayout()).show(utilityCardsPanel,"5DX Photogrammetry Subsystem Bench Calibration Utility");

    calibrationMainSplitPane.setDividerLocation(200);
    utilityMainSplitPane.setDividerLocation(300);
    utilityDescriptionAndStatusSplitPane.setDividerLocation(300);
  }


  /**File | Exit action performed*/
  public void jMenuFileExit_actionPerformed(ActionEvent e) {
    System.exit(0);
  }
  /**Help | About action performed*/
  public void jMenuHelpAbout_actionPerformed(ActionEvent e) {
    FrmCalibration_AboutBox dlg = new FrmCalibration_AboutBox(this);
    Dimension dlgSize = dlg.getPreferredSize();
    Dimension frmSize = getSize();
    Point loc = getLocation();
    dlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
    dlg.setModal(true);
    dlg.show();
  }
  /**Overridden so we can exit when window is closed*/
  protected void processWindowEvent(WindowEvent e) {
    super.processWindowEvent(e);
    if (e.getID() == WindowEvent.WINDOW_CLOSING) {
      jMenuFileExit_actionPerformed(null);
    }
  }

class CalibrationCommandActionAdapter implements java.awt.event.ActionListener
{
  private FrmCalibration adaptee;

  CalibrationCommandActionAdapter(FrmCalibration adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    if (e.getActionCommand() == btnInitialSetup.getActionCommand())
    {
      ((CardLayout)utilityCardsPanel.getLayout()).show(utilityCardsPanel,btnInitialSetup.getActionCommand());
    }
    else if (e.getActionCommand() == btnConfirmLightSource.getActionCommand())
    {
      ((CardLayout)utilityCardsPanel.getLayout()).show(utilityCardsPanel,btnConfirmLightSource.getActionCommand());
    }
    else if (e.getActionCommand() == btnConfirmCCDPixels.getActionCommand())
    {
      ((CardLayout)utilityCardsPanel.getLayout()).show(utilityCardsPanel,btnConfirmCCDPixels.getActionCommand());
    }
    else if (e.getActionCommand() == btnAlignCameraPitch.getActionCommand())
    {
      ((CardLayout)utilityCardsPanel.getLayout()).show(utilityCardsPanel,btnAlignCameraPitch.getActionCommand());
    }
    else if (e.getActionCommand() == btnAlignCameraYaw.getActionCommand())
    {
      ((CardLayout)utilityCardsPanel.getLayout()).show(utilityCardsPanel,btnAlignCameraYaw.getActionCommand());
    }
    else if (e.getActionCommand() == btnAdjustFocus.getActionCommand())
    {
      ((CardLayout)utilityCardsPanel.getLayout()).show(utilityCardsPanel,btnAdjustFocus.getActionCommand());
    }
    else if (e.getActionCommand() == btnAlignFieldOfViews.getActionCommand())
    {
      ((CardLayout)utilityCardsPanel.getLayout()).show(utilityCardsPanel,btnAlignFieldOfViews.getActionCommand());
    }
    else if (e.getActionCommand() == btnFinalTest.getActionCommand())
    {
      ((CardLayout)utilityCardsPanel.getLayout()).show(utilityCardsPanel,btnFinalTest.getActionCommand());
    }
  }
}

  /**
   * @author : Tony Turner
   * @date : 7/30/01
   * @version : 1.0.0
   *
   * Description : Initial Setup Complete set/get
   */

  public void set_initialSetupComplete(boolean new_initialSetupComplete)
  {
    _initialSetupComplete = new_initialSetupComplete;
  }
  public boolean is_initialSetupComplete()
  {
    return _initialSetupComplete;
  }

  /**
   * @author : Tony Turner
   * @date : 7/30/01
   * @version : 1.0.0
   *
   * Description : Initial Setup Complete set/get
   */

  public void set_lightSourceFunctional(boolean new_lightSourceFunctional)
  {
    _lightSourceFunctional = new_lightSourceFunctional;
  }
  public boolean is_lightSourceFunctional()
  {
    return _lightSourceFunctional;
  }

  /**
   * @author : Tony Turner
   * @date : 7/30/01
   * @version : 1.0.0
   *
   * Description : Initial Setup Complete set/get
   */

  public void set_cameraCCDPixelsGood(boolean new_cameraCCDPixelsGood)
  {
    _cameraCCDPixelsGood = new_cameraCCDPixelsGood;
  }
  public boolean is_cameraCCDPixelsGood()
  {
    return _cameraCCDPixelsGood;
  }

  /**
   * @author : Tony Turner
   * @date : 7/30/01
   * @version : 1.0.0
   *
   * Description : Initial Setup Complete set/get
   */

  public void set_cameraPitchCorrect(boolean new_cameraPitchCorrect)
  {
    _cameraPitchCorrect = new_cameraPitchCorrect;
  }
  public boolean is_cameraPitchCorrect()
  {
    return _cameraPitchCorrect;
  }

  /**
   * @author : Tony Turner
   * @date : 7/30/01
   * @version : 1.0.0
   *
   * Description : Initial Setup Complete set/get
   */

  public void set_cameraPairYawCorrect(boolean new_cameraPairYawCorrect)
  {
    _cameraPairYawCorrect = new_cameraPairYawCorrect;
  }
  public boolean is_cameraPairYawCorrect()
  {
    return _cameraPairYawCorrect;
  }

  /**
   * @author : Tony Turner
   * @date : 7/30/01
   * @version : 1.0.0
   *
   * Description : Initial Setup Complete set/get
   */

  public void set_cameraPairFocalDistanceCorrect(boolean new_cameraPairFocalDistanceCorrect)
  {
    _cameraPairFocalDistanceCorrect = new_cameraPairFocalDistanceCorrect;
  }
  public boolean is_cameraPairFocalDistanceCorrect()
  {
    return _cameraPairFocalDistanceCorrect;
  }

  /**
   * @author : Tony Turner
   * @date : 7/30/01
   * @version : 1.0.0
   *
   * Description : Initial Setup Complete set/get
   */

  public void set_cameraPairFOVOverlappingCorrect(boolean new_cameraPairFOVOverlappingCorrect)
  {
    _cameraPairFOVOverlappingCorrect = new_cameraPairFOVOverlappingCorrect;
  }
  public boolean is_cameraPairFOVOverlappingCorrect()
  {
    return _cameraPairFOVOverlappingCorrect;
  }

  /**
   * @author : Tony Turner
   * @date : 7/30/01
   * @version : 1.0.0
   *
   * Description : Initial Setup Complete set/get
   */

  public void set_cameraPairFinalBenchTestGood(boolean new_cameraPairFinalBenchTestGood)
  {
    _cameraPairFinalBenchTestGood = new_cameraPairFinalBenchTestGood;
  }
  public boolean is_cameraPairFinalBenchTestGood()
  {
    return _cameraPairFinalBenchTestGood;
  }
  public void itemStateChanged(ItemEvent e)
  {
  }
  public void propertyChange(PropertyChangeEvent evt)
  {
    /**
     * This property change event signifies that the initial setup is complete
     */

    if (evt.getPropertyName() == "isInitialSetupComplete")
    {
      set_initialSetupComplete(true);
      btnInitialSetup.setBackground(Color.green);
      btnConfirmLightSource.setEnabled(true);

      utilityDescriptionTextArea.setText("Inital Setup Completed Successfully");
      utilityStatusTextArea.setText("Click on the Confirm Light Source Button To Continue");
    }
    /**
     * The following properties are associated with the Initial Setup dialog box
     * and are used to modify the content of the Status and Info boxes
     */

    else if (evt.getPropertyName() == "isSubAssemblyBuilt")
    {
      utilityDescriptionTextArea.setText("Hook the new subassembly up to the surface mapping test station");
      utilityStatusTextArea.setText("Surface Mapping Subassembly Successfully Built");
    }
    else if (evt.getPropertyName() == "isHookedUp")
    {
      utilityDescriptionTextArea.setText("Install the Alignment Coupon in the Plane of Focus");
      utilityStatusTextArea.setText("Surface Mapping subassembly Successfully Hooked Up");
    }
    else if (evt.getPropertyName() == "isCouponInstalled")
    {
      utilityDescriptionTextArea.setText("Set the light source to maximum intensity");
      utilityStatusTextArea.setText("Coupon Successfully Installed");
    }
    else if (evt.getPropertyName() == "isLightSourceSetToMaximumIntensity")
    {
      utilityDescriptionTextArea.setText("Verify that the Light Sensor is Operational");
      utilityStatusTextArea.setText("Light Source Successfully Setup");
    }
    else if (evt.getPropertyName() == "isSensorVerified")
    {
      utilityDescriptionTextArea.setText("Confirm Light source next");
      utilityStatusTextArea.setText("Light Sensor Successfully Verified");
    }
    else
    {
    }

    /**
     * This property change event signifies that the Light Source Confirmation is complete
     */

    if (evt.getPropertyName() == "isLightSourceConfirmed")
    {
      set_lightSourceFunctional(true);
      btnConfirmLightSource.setBackground(Color.green);
      btnConfirmCCDPixels.setEnabled(true);

      utilityDescriptionTextArea.setText("Light Source Confirmation Completed Successfully");
      utilityStatusTextArea.setText("Click on the Confirm Pixels Button To Continue");
    }


    /**
     * This property change event signifies that the Line Scan Camera Pixels check is complete
     */


    if (evt.getPropertyName() == "_bothDecided_frontCCDGood_rearCCDGood")
    {
      set_cameraCCDPixelsGood(true);
      btnConfirmCCDPixels.setBackground(Color.green);
      btnAlignCameraPitch.setEnabled(true);

      utilityDescriptionTextArea.setText("Camera Pixel Confirmation Completed Successfully");
      utilityStatusTextArea.setText("Click on the 'Align Camera Pitch' Button To Continue");
    }

    /**
     * The following properties are associated with the CCD Pixel Integrity dialog box
     * and are used to modify the content of the Status and Info boxes
     */


    else if (evt.getPropertyName() == "_bothDecided_frontCCDGood_rearCCDBad")
    {
      utilityDescriptionTextArea.setText("Replace Rear Camera and start entire test over");
      utilityStatusTextArea.setText("Rear Camera CCD is Defective .. ");
    }
    else if (evt.getPropertyName() == "_bothDecided_frontCCDBad_rearCCDGood")
    {
      utilityDescriptionTextArea.setText("Replace Front Camera and start entire test over");
      utilityStatusTextArea.setText("Front Camera CCD is Defective .. ");
    }
    else if (evt.getPropertyName() == "_bothDecided_frontCCDBad_rearCCDBad")
    {
      utilityDescriptionTextArea.setText("Replace Both Cameras and start entire test over");
      utilityStatusTextArea.setText("Front and Rear Camera CCDs are Defective .. ");
    }
    else
    {
    }

    /**
     * This property change event signifies that the Line Scan Camera Pitch Angles check is complete
     */


    if (evt.getPropertyName() == "_bothDecided_frontPitchGood_rearPitchGood")
    {
      set_cameraPitchCorrect(true);
      set_frontCameraPitchCorrect(true);
      set_rearCameraPitchCorrect(true);

      btnAlignCameraPitch.setBackground(Color.green);

      btnAlignCameraYaw.setEnabled(true);

      utilityDescriptionTextArea.setText("Camera Pitch Angle Adjustment Completed Successfully");
      utilityStatusTextArea.setText("Click on the 'Align Camera Yaw' Button To Continue");
    }

    /**
     * The following properties are associated with the Pitch Angle adjustment dialog box
     * and are used to modify the content of the Status and Info boxes
     */


    else if (evt.getPropertyName() == "_bothDecided_frontPitchGood_rearPitchBad")
    {
      utilityDescriptionTextArea.setText("Contact your Production Engineer before continuing...");
      utilityStatusTextArea.setText("Rear Camera Pitch Cannot be Adjusted .. ");
    }
    else if (evt.getPropertyName() == "_bothDecided_frontPitchBad_rearPitchGood")
    {
      utilityDescriptionTextArea.setText("Contact your Production Engineer before continuing...");
      utilityStatusTextArea.setText("Front Camera Pitch Cannot be Adjusted .. ");
    }
    else if (evt.getPropertyName() == "_bothDecided_frontPitchBad_rearPitchBad")
    {
      utilityDescriptionTextArea.setText("Contact your Production Engineer before continuing...");
      utilityStatusTextArea.setText("Both Front and Rear Camera Pitch Cannot be Adjusted .. ");
    }
    else
    {
    }

    /**
     * This property change event signifies that the Line Scan Camera Yaw Angles check is complete
     */


    if (evt.getPropertyName() == "_bothDecided_frontYawGood_rearYawGood")
    {
      set_cameraPairYawCorrect(true);
      set_frontCameraYawCorrect(true);
      set_rearCameraYawCorrect(true);

      btnAlignCameraYaw.setBackground(Color.green);

      btnAdjustFocus.setEnabled(true);

      utilityDescriptionTextArea.setText("Camera Yaw Angle Adjustment Completed Successfully");
      utilityStatusTextArea.setText("Click on the 'Align Camera Yaw' Button To Continue");
    }

    /**
     * The following properties are associated with the Yaw Angle adjustment dialog box
     * and are used to modify the content of the Status and Info boxes
     */


    else if (evt.getPropertyName() == "_bothDecided_frontYawGood_rearYawBad")
    {
      utilityDescriptionTextArea.setText("Contact your Production Engineer before continuing...");
      utilityStatusTextArea.setText("Rear Camera Yaw Cannot be Adjusted .. ");
    }
    else if (evt.getPropertyName() == "_bothDecided_frontYawBad_rearYawGood")
    {
      utilityDescriptionTextArea.setText("Contact your Production Engineer before continuing...");
      utilityStatusTextArea.setText("Front Camera Yaw Cannot be Adjusted .. ");
    }
    else if (evt.getPropertyName() == "_bothDecided_frontYawBad_rearYawBad")
    {
      utilityDescriptionTextArea.setText("Contact your Production Engineer before continuing...");
      utilityStatusTextArea.setText("Both Front and Rear Camera Yaw Cannot be Adjusted .. ");
    }
    else
    {
    }


    /**
     * This property change event signifies that the Line Scan Camera Focal Distance s check is complete
     */


    if (evt.getPropertyName() == "_bothDecided_frontFocalDistanceGood_rearFocalDistanceGood")
    {
      set_cameraPairFocalDistanceCorrect(true);
      set_frontCameraFocalDistanceCorrect(true);
      set_rearCameraFocalDistanceCorrect(true);

      btnAdjustFocus.setBackground(Color.green);

      btnAlignFieldOfViews.setEnabled(true);

      utilityDescriptionTextArea.setText("Camera Focal Distance  Adjustment Completed Successfully");
      utilityStatusTextArea.setText("Click on the 'Align Camera Focal Distance' Button To Continue");
    }

    /**
     * The following properties are associated with the FocalDistance  adjustment dialog box
     * and are used to modify the content of the Status and Info boxes
     */


    else if (evt.getPropertyName() == "_bothDecided_frontFocalDistanceGood_rearFocalDistanceBad")
    {
      utilityDescriptionTextArea.setText("Contact your Production Engineer before continuing...");
      utilityStatusTextArea.setText("Rear Camera Focal Distance Cannot be Adjusted .. ");
    }
    else if (evt.getPropertyName() == "_bothDecided_frontFocalDistanceBad_rearFocalDistanceGood")
    {
      utilityDescriptionTextArea.setText("Contact your Production Engineer before continuing...");
      utilityStatusTextArea.setText("Front Camera Focal Distance Cannot be Adjusted .. ");
    }
    else if (evt.getPropertyName() == "_bothDecided_frontFocalDistanceBad_rearFocalDistanceBad")
    {
      utilityDescriptionTextArea.setText("Contact your Production Engineer before continuing...");
      utilityStatusTextArea.setText("Both Front and Rear Camera Focal Distance Cannot be Adjusted .. ");
    }
    else
    {
    }

    /**
     * This property change event signifies that the Line Scan Camera Fields Of View s check is complete
     */


    if (evt.getPropertyName() == "_bothDecided_frontFieldsOfViewGood_rearFieldsOfViewGood")
    {
      set_cameraPairFOVOverlappingCorrect(true);

      btnAlignFieldOfViews.setBackground(Color.green);

      btnFinalTest.setEnabled(true);

      utilityDescriptionTextArea.setText("Camera Fields Of View  Adjustment Completed Successfully");
      utilityStatusTextArea.setText("Click on the 'Align Camera Fields Of View' Button To Continue");
    }

    /**
     * The following properties are associated with the FieldsOfView  adjustment dialog box
     * and are used to modify the content of the Status and Info boxes
     */


    else if (evt.getPropertyName() == "_bothDecided_frontFieldsOfViewGood_rearFieldsOfViewBad")
    {
      utilityDescriptionTextArea.setText("Contact your Production Engineer before continuing...");
      utilityStatusTextArea.setText("Rear Camera Fields Of View Cannot be Adjusted .. ");
    }
    else if (evt.getPropertyName() == "_bothDecided_frontFieldsOfViewBad_rearFieldsOfViewGood")
    {
      utilityDescriptionTextArea.setText("Contact your Production Engineer before continuing...");
      utilityStatusTextArea.setText("Front Camera Fields Of View Cannot be Adjusted .. ");
    }
    else if (evt.getPropertyName() == "_bothDecided_frontFieldsOfViewBad_rearFieldsOfViewBad")
    {
      utilityDescriptionTextArea.setText("Contact your Production Engineer before continuing...");
      utilityStatusTextArea.setText("Both Front and Rear Camera Fields Of View Cannot be Adjusted .. ");
    }
    else
    {
    }


//    utilityCardsPanel.add( finalBenchTestPhotogrammetrySubsystemPanel, "Final Bench Test Photogrammetry Subsystem");
    /**
     * This property change event signifies that the XXX is complete
     */

/*
    if (evt.getPropertyName() == "xxx")
    {
      set_xxx(true);
      btn_nextxxx.setEnabled(true);

      utilityDescriptionTextArea.setText("XXX Completed Successfully");
      utilityStatusTextArea.setText("Click on the _next_xxx Button To Continue");
    }
*/
    /**
     * The following properties are associated with the XXX dialog box
     * and are used to modify the content of the Status and Info boxes
     */

/*
    else if (evt.getPropertyName() == "is_sub_xxx_1")
    {
      utilityDescriptionTextArea.setText("xxx_1 description");
      utilityStatusTextArea.setText("XXX_1 Successfully Executed");
    }
    else if (evt.getPropertyName() == "is_sub_xxx_2")
    {
      utilityDescriptionTextArea.setText("xxx_2 description");
      utilityStatusTextArea.setText("XXX_2 Successfully Executed");
    }
    else
    {
    }
  }
*/

}
  public void set_frontCameraCCDPixelsGood(boolean new_frontCameraCCDPixelsGood)
  {
    _frontCameraCCDPixelsGood = new_frontCameraCCDPixelsGood;
  }
  public boolean is_frontCameraCCDPixelsGood()
  {
    return _frontCameraCCDPixelsGood;
  }
  public void set_rearCameraCCDPixelsGood(boolean new_rearCameraCCDPixelsGood)
  {
    _rearCameraCCDPixelsGood = new_rearCameraCCDPixelsGood;
  }
  public boolean is_rearCameraCCDPixelsGood()
  {
    return _rearCameraCCDPixelsGood;
  }
  public void set_frontCameraFocalDistanceCorrect(boolean new_frontCameraFocalDistanceCorrect)
  {
    _frontCameraFocalDistanceCorrect = new_frontCameraFocalDistanceCorrect;
  }
  public boolean is_frontCameraFocalDistanceCorrect()
  {
    return _frontCameraFocalDistanceCorrect;
  }
  public void set_rearCameraFocalDistanceCorrect(boolean new_rearCameraFocalDistanceCorrect)
  {
    _rearCameraFocalDistanceCorrect = new_rearCameraFocalDistanceCorrect;
  }
  public boolean is_rearCameraFocalDistanceCorrect()
  {
    return _rearCameraFocalDistanceCorrect;
  }
  public void set_frontCameraPitchCorrect(boolean new_frontCameraPitchCorrect)
  {
    _frontCameraPitchCorrect = new_frontCameraPitchCorrect;
  }
  public boolean is_frontCameraPitchCorrect()
  {
    return _frontCameraPitchCorrect;
  }
  public void set_rearCameraPitchCorrect(boolean new_rearCameraPitchCorrect)
  {
    _rearCameraPitchCorrect = new_rearCameraPitchCorrect;
  }
  public boolean is_rearCameraPitchCorrect()
  {
    return _rearCameraPitchCorrect;
  }
  public void set_frontCameraYawCorrect(boolean new_frontCameraYawCorrect)
  {
    _frontCameraYawCorrect = new_frontCameraYawCorrect;
  }
  public boolean is_frontCameraYawCorrect()
  {
    return _frontCameraYawCorrect;
  }
  public void set_rearCameraYawCorrect(boolean new_rearCameraYawCorrect)
  {
    _rearCameraYawCorrect = new_rearCameraYawCorrect;
  }
  public boolean is_rearCameraYawCorrect()
  {
    return _rearCameraYawCorrect;
  }

  public void confirmBasicFunctionality()
  {
        // message #1.1 to _lineScanCameraImageAcquisitionProperties:LineScanCameraImageAcquisitionProperties
//        LineScanCameraPairInstalledEnum _installed = LineScanCameraPairInstalledEnum.BOTTOM_PAIR_INSTALLED;
        //_installed = LineScanCameraAcquisitionProperties.getCameraPairInstalled();
        // message #1.2 to _surfaceMappingCalibration:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
        //_surfaceMappingCalibration.confirmBasicOperation();
    }

  public void alignCameras()
  {
/*    try
    {
        // message #2.1 to _surfaceMappingCalibration:com.agilent.xRayTest.hardware.SurfaceMappingCalibration
        //_surfaceMappingCalibration.alignCameras();
    }
    catch(HardwareException he)
    {
*/
      /** @todo act - add an exception handler for FrmCalibration.alignCameras */
/*
    }
*/
    }

  public void confirmLinearity() {
  }

}