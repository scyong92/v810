package com.agilent.xRayTest.gui.cAndA;

import java.awt.*;
import javax.swing.*;
import java.beans.*;
import java.awt.event.*;

/**
 * Title:        5DX Calibration Utilities
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Tony Turner
 * @version 1.0
 */

public class AlignLineScanCameraPairFieldsOfViewPanel extends JPanel
{
  BorderLayout borderLayout1 = new BorderLayout();
  private JPanel alignLineScanCameraPairFieldsOfViewPanel = new JPanel();
  private JLabel alignLineScanCameraPairFieldsOfViewLabel = new JLabel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private JSplitPane jSplitPane1 = new JSplitPane();
  private JPanel jPanel1 = new JPanel();
  private JPanel jPanel2 = new JPanel();
  private BorderLayout borderLayout3 = new BorderLayout();
  private BorderLayout borderLayout4 = new BorderLayout();
  private JPanel jPanel3 = new JPanel();
  private Box box1;
  private JButton btnGoodFrontFieldsOfView = new JButton();
  private JButton btnBadFrontFieldsOfView = new JButton();
  private JLabel jLabel1 = new JLabel();
  private JPanel jPanel4 = new JPanel();
  private Box box2;
  private JButton btnGoodRearFieldsOfView = new JButton();
  private JButton btnBadRearFieldsOfView = new JButton();
  private JLabel jLabel2 = new JLabel();
  private boolean _goodFrontFieldsOfView;
  private boolean _goodRearFieldsOfView;
  transient private PropertyChangeSupport propertyChangeListeners = new PropertyChangeSupport(this);
  private boolean _frontDecided;
  private boolean _rearDecided;
  private boolean _bothDecided;

  public AlignLineScanCameraPairFieldsOfViewPanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    set_frontDecided(false);
    set_rearDecided(false);
    box1 = Box.createHorizontalBox();
    box2 = Box.createHorizontalBox();
    this.setLayout(borderLayout1);
    alignLineScanCameraPairFieldsOfViewLabel.setFont(new java.awt.Font("Dialog", 1, 18));
    alignLineScanCameraPairFieldsOfViewLabel.setHorizontalAlignment(SwingConstants.CENTER);
    alignLineScanCameraPairFieldsOfViewLabel.setText("Align Line Scan Camera FieldsOfView ");
    alignLineScanCameraPairFieldsOfViewPanel.setLayout(borderLayout2);
    jSplitPane1.setDividerSize(5);
    jPanel1.setLayout(borderLayout3);
    jPanel2.setLayout(borderLayout4);
    btnGoodFrontFieldsOfView.setText("Aligned");
    btnGoodFrontFieldsOfView.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnGoodFrontFieldsOfView_actionPerformed(e);
      }
    });
    btnBadFrontFieldsOfView.setText("Can\'t Align");
    btnBadFrontFieldsOfView.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnBadFrontFieldsOfView_actionPerformed(e);
      }
    });
    jLabel1.setText("Front Camera Image Goes Here");
    btnGoodRearFieldsOfView.setText("Aligned");
    btnGoodRearFieldsOfView.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnGoodRearFieldsOfView_actionPerformed(e);
      }
    });
    btnBadRearFieldsOfView.setText("Can\'t Align");
    btnBadRearFieldsOfView.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnBadRearFieldsOfView_actionPerformed(e);
      }
    });
    jLabel2.setText("Rear Camera Image Goes Here");
    this.add(alignLineScanCameraPairFieldsOfViewPanel, BorderLayout.CENTER);
    alignLineScanCameraPairFieldsOfViewPanel.add(alignLineScanCameraPairFieldsOfViewLabel, BorderLayout.NORTH);
    alignLineScanCameraPairFieldsOfViewPanel.add(jSplitPane1, BorderLayout.CENTER);
    jSplitPane1.add(jPanel1, JSplitPane.LEFT);
    jPanel1.add(jPanel3, BorderLayout.SOUTH);
    jPanel3.add(box1, null);
    box1.add(btnGoodFrontFieldsOfView, null);
    box1.add(btnBadFrontFieldsOfView, null);
    jPanel1.add(jLabel1, BorderLayout.CENTER);
    jSplitPane1.add(jPanel2, JSplitPane.RIGHT);
    jPanel2.add(jPanel4, BorderLayout.SOUTH);
    jPanel4.add(box2, null);
    box2.add(btnGoodRearFieldsOfView, null);
    box2.add(btnBadRearFieldsOfView, null);
    jPanel2.add(jLabel2, BorderLayout.CENTER);
    jSplitPane1.setDividerLocation(190);
  }
  public void set_goodFrontFieldsOfView(boolean new_goodFrontFieldsOfView)
  {
    _goodFrontFieldsOfView = new_goodFrontFieldsOfView;
    set_frontDecided(true);
    if ((_frontDecided) && (_rearDecided))
    {
      set_bothDecided(true);
    }

  }
  public boolean is_goodFrontFieldsOfView()
  {
    return _goodFrontFieldsOfView;
  }
  public void set_goodRearFieldsOfView(boolean new_goodRearFieldsOfView)
  {
    _goodRearFieldsOfView = new_goodRearFieldsOfView;
    set_rearDecided(true);
    if ((_frontDecided) && (_rearDecided))
    {
      set_bothDecided(true);
    }
  }
  public boolean is_goodRearFieldsOfView()
  {
    return _goodRearFieldsOfView;
  }
  public synchronized void removePropertyChangeListener(PropertyChangeListener l)
  {
    super.removePropertyChangeListener(l);
    propertyChangeListeners.removePropertyChangeListener(l);
  }
  public synchronized void addPropertyChangeListener(PropertyChangeListener l)
  {
    super.addPropertyChangeListener(l);
    propertyChangeListeners.addPropertyChangeListener(l);
  }
  public void set_frontDecided(boolean new_frontDecided)
  {
    _frontDecided = new_frontDecided;
  }
  public boolean is_frontDecided()
  {
    return _frontDecided;
  }
  public void set_rearDecided(boolean new_rearDecided)
  {
    _rearDecided = new_rearDecided;
  }
  public boolean is_rearDecided()
  {
    return _rearDecided;
  }
  public void set_bothDecided(boolean new_bothDecided)
  {
/** @todo act _ go back to frmCalibration and add listener adapter code for these four cases next */
    boolean  old_bothDecided = _bothDecided;
    _bothDecided = new_bothDecided;
    if (is_goodFrontFieldsOfView() && is_goodRearFieldsOfView())
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontFieldsOfViewGood_rearFieldsOfViewGood", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }
    else if (! is_goodFrontFieldsOfView() && is_goodRearFieldsOfView())
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontFieldsOfViewBad_rearFieldsOfViewGood", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }
    else if (is_goodFrontFieldsOfView() && ! is_goodRearFieldsOfView())
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontFieldsOfViewGood_rearFieldsOfViewBad", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }
    else
    {
        propertyChangeListeners.firePropertyChange("_bothDecided_frontFieldsOfViewBad_rearFieldsOfViewBad", new Boolean(old_bothDecided), new Boolean(new_bothDecided));
    }

  }
  public boolean is_bothDecided()
  {
    return _bothDecided;
  }

  void btnGoodFrontFieldsOfView_actionPerformed(ActionEvent e)
  {
    set_goodFrontFieldsOfView(true);
    btnGoodFrontFieldsOfView.setBackground(Color.green);
    btnBadFrontFieldsOfView.setBackground(Color.lightGray);
  }

  void btnBadFrontFieldsOfView_actionPerformed(ActionEvent e)
  {
    set_goodFrontFieldsOfView(false);
    btnGoodFrontFieldsOfView.setBackground(Color.lightGray);
    btnBadFrontFieldsOfView.setBackground(Color.red);
  }

  void btnGoodRearFieldsOfView_actionPerformed(ActionEvent e)
  {
    set_goodRearFieldsOfView(true);
    btnGoodRearFieldsOfView.setBackground(Color.green);
    btnBadRearFieldsOfView.setBackground(Color.lightGray);
  }

  void btnBadRearFieldsOfView_actionPerformed(ActionEvent e)
  {
    set_goodRearFieldsOfView(false);
    btnGoodRearFieldsOfView.setBackground(Color.lightGray);
    btnBadRearFieldsOfView.setBackground(Color.red);
  }

}
