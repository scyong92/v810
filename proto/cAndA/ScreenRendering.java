package com.agilent.xRayTest.gui.cAndA;

import java.awt.*;
import java.awt.Component;

/**
 * Title:        ScreenRendering
 * Description:  Render an image into a frame
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies
 * @author       Tony Turner
 * @version 1.0
 */

public class ScreenRendering extends Component
{
  private SimplePainter painter;

  public ScreenRendering(String imageName, int x, int y, int w, int h)
  {
    painter = new SimplePainter(imageName, x, y, w, h);
  }

  public Dimension getMinimumSize()
  {
    return painter.getSize();
  }

  public Dimension getPreferredSize()
  {
    return painter.getSize();
  }

  public void paint(Graphics _g)
  {
    Graphics2D g = (Graphics2D)_g;
    painter.render(g);
  }
}