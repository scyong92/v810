package com.agilent.xRayTest.gui.cAndA;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.beans.*;

/**
 * Title:        5DX Calibration Utilities
 * Description:  Initial Line Scan Camera Assembly Setup GUI
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author Tony Turner
 * @version 1.0
 */
public class InitialLineScanCameraAssemblySetupPanel extends JPanel
{
  BorderLayout borderLayout1 = new BorderLayout();
  private JPanel initialLineScanCameraAssemblySetupPanel = new JPanel();
  private JLabel initialLineScanCameraAssemblySetupLabel = new JLabel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private JPanel jPanel1 = new JPanel();
  private FlowLayout flowLayout1 = new FlowLayout();
  private JCheckBox cbBuildSubassembly = new JCheckBox();
  private JCheckBox cbHookSubassemblyToTestStation = new JCheckBox();
  private JCheckBox cbPlaceAlignmentCouponInPlaneOfFocus = new JCheckBox();
  private JCheckBox cbSetLightSourceToMaximumIntensity = new JCheckBox();
  private JCheckBox cbVerifyThatTheLightSensorIsOperational = new JCheckBox();
  transient private PropertyChangeSupport propertyChangeListeners = new PropertyChangeSupport(this);
  private boolean _subAssemblyBuilt = false;
  private boolean _hookedUp = false;
  private boolean _couponInstalled = false;
  private boolean _lightSourceSetToMaximumIntensity = false;
  private boolean _sensorVerified = false;
  private boolean _initialSetupComplete = false;

  public InitialLineScanCameraAssemblySetupPanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    initialLineScanCameraAssemblySetupLabel.setFont(new java.awt.Font("Dialog", 1, 18));
    initialLineScanCameraAssemblySetupLabel.setHorizontalAlignment(SwingConstants.CENTER);
    initialLineScanCameraAssemblySetupLabel.setText("Initial Setup");
    initialLineScanCameraAssemblySetupPanel.setLayout(borderLayout2);
    jPanel1.setLayout(flowLayout1);

    cbBuildSubassembly.setText("Build The Surface Mapping Subassembly");
    cbBuildSubassembly.setSelected(false);
    cbBuildSubassembly.addItemListener(new InitialLineScanCameraAssemblySetupPanel_itemAdapter(this));

    cbHookSubassemblyToTestStation.setText("Hook Subassembly to Test Station");
    cbHookSubassemblyToTestStation.setSelected(false);
    cbHookSubassemblyToTestStation.addItemListener(new InitialLineScanCameraAssemblySetupPanel_itemAdapter(this));

    cbPlaceAlignmentCouponInPlaneOfFocus.setText("Place Alignment Coupon in Plane of Focus");
    cbPlaceAlignmentCouponInPlaneOfFocus.setSelected(false);
    cbPlaceAlignmentCouponInPlaneOfFocus.addItemListener(new InitialLineScanCameraAssemblySetupPanel_itemAdapter(this));

    cbSetLightSourceToMaximumIntensity.setText("Set Light Source to Maximum Intensity");
    cbSetLightSourceToMaximumIntensity.setSelected(false);
    cbSetLightSourceToMaximumIntensity.addItemListener(new InitialLineScanCameraAssemblySetupPanel_itemAdapter(this));

    cbVerifyThatTheLightSensorIsOperational.setText("Verify that the Light Sensor is Operational");
    cbVerifyThatTheLightSensorIsOperational.setSelected(false);
    cbVerifyThatTheLightSensorIsOperational.addItemListener(new InitialLineScanCameraAssemblySetupPanel_itemAdapter(this));

    this.add(initialLineScanCameraAssemblySetupPanel, BorderLayout.CENTER);
    initialLineScanCameraAssemblySetupPanel.add(initialLineScanCameraAssemblySetupLabel, BorderLayout.NORTH);
    initialLineScanCameraAssemblySetupPanel.add(jPanel1, BorderLayout.CENTER);

    jPanel1.add(cbBuildSubassembly, null);
    jPanel1.add(cbHookSubassemblyToTestStation, null);
    jPanel1.add(cbPlaceAlignmentCouponInPlaneOfFocus, null);
    jPanel1.add(cbSetLightSourceToMaximumIntensity, null);
    jPanel1.add(cbVerifyThatTheLightSensorIsOperational, null);
  }

  /**
   * @author : Tony Turner
   * @date : 7/30/01
   * @version : 1.0.0
   *
   * Description : add/remove Property Change Listener
   */

  public synchronized void removePropertyChangeListener(PropertyChangeListener l)
  {
    super.removePropertyChangeListener(l);
    propertyChangeListeners.removePropertyChangeListener(l);
  }
  public synchronized void addPropertyChangeListener(PropertyChangeListener l)
  {
    super.addPropertyChangeListener(l);
    propertyChangeListeners.addPropertyChangeListener(l);
  }

  /**
   * @author : Tony Turner
   * @date : 7/30/01
   * @version : 1.0.0
   *
   * Description : _subAssemblyBuilt set/get
   */

  public void set_subAssemblyBuilt(boolean new_subAssemblyBuilt)
  {
    boolean  oldIsSubAssemblyBuilt = _subAssemblyBuilt;
    propertyChangeListeners.firePropertyChange("isSubAssemblyBuilt", new Boolean(oldIsSubAssemblyBuilt), new Boolean(new_subAssemblyBuilt));
    _subAssemblyBuilt = new_subAssemblyBuilt;
  }
  public boolean is_subAssemblyBuilt()
  {
    return _subAssemblyBuilt;
  }

  /**
   * @author : Tony Turner
   * @date : 7/30/01
   * @version : 1.0.0
   *
   * Description : _hookedUp set/get
   */

  public void set_hookedUp(boolean new_hookedUp)
  {
    boolean  oldIsHookedUp = _hookedUp;
    propertyChangeListeners.firePropertyChange("isHookedUp", new Boolean(oldIsHookedUp), new Boolean(new_hookedUp));
    _hookedUp = new_hookedUp;
  }
  public boolean is_hookedUp()
  {
    return _hookedUp;
  }

  /**
   * @author : Tony Turner
   * @date : 7/30/01
   * @version : 1.0.0
   *
   * Description : _couponInstalled set/get
   */

  public void set_couponInstalled(boolean new_couponInstalled)
  {
    boolean  oldIsCouponInstalled = _couponInstalled;
    propertyChangeListeners.firePropertyChange("isCouponInstalled", new Boolean(oldIsCouponInstalled), new Boolean(new_couponInstalled));
    _couponInstalled = new_couponInstalled;
  }
  public boolean is_couponInstalled()
  {
    return _couponInstalled;
  }

  /**
   * @author : Tony Turner
   * @date : 7/30/01
   * @version : 1.0.0
   *
   * Description : _lightSourceSetToMaximumIntensity set/get
   */

  public void set_lightSourceSetToMaximumIntensity(boolean new_lightSourceSetToMaximumIntensity)
  {
    boolean  oldIsLightSourceSetToMaximumIntensity = _lightSourceSetToMaximumIntensity;
    propertyChangeListeners.firePropertyChange("isLightSourceSetToMaximumIntensity", new Boolean(oldIsLightSourceSetToMaximumIntensity), new Boolean(new_lightSourceSetToMaximumIntensity));
    _lightSourceSetToMaximumIntensity = new_lightSourceSetToMaximumIntensity;
  }
  public boolean is_lightSourceSetToMaximumIntensity()
  {
    return _lightSourceSetToMaximumIntensity;
  }

  /**
   * @author : Tony Turner
   * @date : 7/30/01
   * @version : 1.0.0
   *
   * Description : _sensorVerified set/get
   */

  public void set_sensorVerified(boolean new_sensorVerified)
  {
    boolean  oldIsSensorVerified = _sensorVerified;
    propertyChangeListeners.firePropertyChange("isSensorVerified", new Boolean(oldIsSensorVerified), new Boolean(new_sensorVerified));
    _sensorVerified = new_sensorVerified;
  }
  public boolean is_sensorVerified()
  {
    return _sensorVerified;
  }

  /**
   * @author : Tony Turner
   * @date : 7/30/01
   * @version : 1.0.0
   *
   * Description : _initialSetupComplete set/get
   */

  public void set_initialSetupComplete(boolean new_initialSetupComplete)
  {
    boolean  oldIsInitialSetupComplete = _initialSetupComplete;
    propertyChangeListeners.firePropertyChange("isInitialSetupComplete", new Boolean(oldIsInitialSetupComplete), new Boolean(new_initialSetupComplete));
    _initialSetupComplete = new_initialSetupComplete;
  }
  public boolean is_initialSetupComplete()
  {
    return _initialSetupComplete;
  }

  /**
   * @author : Tony Turner
   * @date : 7/30/01
   * @version : 1.0.0
   *
   * Description : item listener adapter class
   */

class InitialLineScanCameraAssemblySetupPanel_itemAdapter implements java.awt.event.ItemListener
{
  private InitialLineScanCameraAssemblySetupPanel adaptee;

  InitialLineScanCameraAssemblySetupPanel_itemAdapter(InitialLineScanCameraAssemblySetupPanel adaptee)
  {
    this.adaptee = adaptee;
  }
  public void itemStateChanged(ItemEvent e)
  {
    Object source = e.getItemSelectable();

    if (source == adaptee.cbBuildSubassembly)
    {
      adaptee.set_subAssemblyBuilt(true);
    }
    else if (source == adaptee.cbHookSubassemblyToTestStation)
    {
      adaptee.set_hookedUp(true);
    }
    else if (source == adaptee.cbPlaceAlignmentCouponInPlaneOfFocus)
    {
      adaptee.set_couponInstalled(true);
    }
    else if (source == adaptee.cbSetLightSourceToMaximumIntensity)
    {
      adaptee.set_lightSourceSetToMaximumIntensity(true);
    }
    else if (source == adaptee.cbVerifyThatTheLightSensorIsOperational)
    {
      adaptee.set_sensorVerified(true);
    }
    if ( adaptee.is_subAssemblyBuilt() == true &&
         adaptee.is_hookedUp() &&
         adaptee.is_couponInstalled() &&
         adaptee.is_lightSourceSetToMaximumIntensity() &&
         adaptee.is_sensorVerified())
        {
          adaptee.set_initialSetupComplete(true);
        }

  }
}
}

