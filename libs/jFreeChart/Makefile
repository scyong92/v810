############################################################################
# FILE  : Makefile
# AUTHOR: Bill Darbie, et. al
##############################################################################
# make this work like a POSIX Makefile - standard is good
.POSIX:
# set what shell to use
SHELL=sh
# clear out all default SUFFIX rules
.SUFFIXES:

TEMP_DIR=$(TEMP)/jreCopy


# PE: Copy the jar files and the dll to each jre location.

JRE_LIB_EXT_FILES = \
  jfreechart-1.0.1.jar \
  jcommon-1.0.0.jar

JRE_BIN_FILES = \

all: lock copy javacWarningSuppress unlock

copy: alwaysDoThis
	@echo -e "\n#### copying JFreeChart package to the proper locations ####"
	[ -e $(AXI_XRAY_JRE_HOME)/lib/ext ] || mkdir -p $(AXI_XRAY_JRE_HOME)/lib/ext
	[ -e $(REL_JRE_HOME)/lib/ext ] || mkdir -p $(REL_JRE_HOME)/lib/ext
	[ -e $(JAVA_HOME)/jre/lib/ext ] || mkdir -p $(JAVA_HOME)/jre/lib/ext
	[ -e $(AXI_XRAY_JRE_HOME)/bin ] || mkdir -p $(AXI_XRAY_JRE_HOME)/bin
	[ -e $(REL_JRE_HOME)/bin ] || mkdir -p $(REL_JRE_HOME)/bin
	@for i in $(JRE_LIB_EXT_FILES) $(JRE_BIN_FILES); \
	 do\
	   if [ -e $(AXI_XRAY_JRE_HOME)/lib/ext/$$i ] ; \
	   then \
	     echo not copying $$i it already exists in $(AXI_XRAY_JRE_HOME)\/lib\/ext ; \
	   else \
	    echo copying $$i to $(AXI_XRAY_JRE_HOME)\/lib\/ext ; \
	    cp $$i $(AXI_XRAY_JRE_HOME)/lib/ext; \
	   fi ; \
	   if [ -e $(REL_JRE_HOME)/lib/ext/$$i ] ; \
	   then \
	     echo not copying $$i it already exists in $(REL_JRE_HOME)\/lib\/ext ; \
	   else \
	     echo copying $$i to $(REL_JRE_HOME)\/lib\/ext ; \
	     cp $$i $(REL_JRE_HOME)/lib/ext; \
	   fi ; \
	 done

# JavaFreeChart wants a jcommon-1.0.0.jar to be in lib/ext/lib when compiling, this target
# puts an extra copy in that directory
javacWarningSuppress: alwaysDoThis
	mkdir -p $(AXI_XRAY_JRE_HOME)/lib/ext/lib
	cp jcommon-1.0.0.jar $(AXI_XRAY_JRE_HOME)/lib/ext/lib

clean:
	@for file in $(JRE_LIB_EXT_FILES) $(JRE_BIN_FILES); \
	do \
		echo Removing  $(AXI_XRAY_JRE_HOME)/lib/ext/$$file; \
		rm -f $(AXI_XRAY_JRE_HOME)/lib/ext/$$file; \
		echo Removing  $(REL_JRE_HOME)/lib/ext/$$file; \
		rm -f $(REL_JRE_HOME)/lib/ext/$$file; \
	done
	rm -f $(AXI_XRAY_JRE_HOME)/lib/ext/lib/jcommon-1.0.0.jar

lock: alwaysDoThis
	@echo -e "\n#### creating a lock for a jre copy ####"
	mkdir -p $(TEMP_DIR)
	@TEMP=""; \
        PID="0"; \
	while ([ "$$TEMP" != "$$PID" ]); \
        do \
	  while ([ -e $(TEMP_DIR)/copyInProcess ]); \
          do \
            echo "Another process is copying files to $(JAVA_HOME)/lib, waiting for it to finish..."; \
            sleep 5; \
          done; \
	  PID=$$$$; \
	  echo $$PID > $(TEMP_DIR)/copyInProcess; \
	  sleep 1; \
	  TEMP=`cat $(TEMP_DIR)/copyInProcess`; \
        done

unlock: alwaysDoThis
	@echo -e "\n#### removing the lock for a jre copy ####"
	rm -f $(TEMP_DIR)/copyInProcess

alwaysDoThis:
