import javax.swing.UIManager;
import java.awt.*;

/**
 * This is a special one customer kludge for StakTek.
 *
 * This program works for panels that have trays which contain
 * boards in straight rows and columns.  The panel program is
 * set up so that the panel is viewed as having a single board
 * rather than multiple boards.  Occasionally at the end of a production
 * run StakTek wants to only partially fill this tray with boards.
 * With the current 5dx software this will cause many failures on
 * all the boards that are not loaded.  This program gives them
 * a way to test a partially loaded tray.
 *
 * It works this way:
 * The customer will:
 *  - click an icon on the desktop to start this program
 *  - select the panel program name or enter a serial number
 *  - select the row and column where the first empty tray is
 *    It is assumed that all the trays below this one are
 *    also empty (going left to right, top to bottom along the
 *    rows and columns).  If a tray contains a component used for
 *    alignment, then that tray is assumed to always be loaded.
 *  - this program will:
 *    - copy the original program to a new directory
 *      using the panel name program_partialTray
 *    - run the compiler 
 *    - modify the component.ndf file to false
 *      out the appropriate components
 *    - run the compiler again to get the component changes
 *  - the user will then need to select program_partialTray and run
 *    the inspection
 *
 * This program will take a program and copy it to another name
 * called program_partialTray.  The GUI will ask the user
 * to enter which
 *
 *
 *
 * @author Bill Darbie
 */
public class StakTekApplication
{
  private boolean _packFrame = true;

  /**
   * @author Bill Darbie
   */
  public StakTekApplication()
  {
    Frame frame = new StakTekFrame();
    //Validate frames that have preset sizes
    //Pack frames that have useful preferred size info, e.g. from their layout
    if (_packFrame)
    {
      frame.pack();
    }
    else
    {
      frame.validate();
    }
    //Center the window
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = frame.getSize();
    if (frameSize.height > screenSize.height)
    {
      frameSize.height = screenSize.height;
    }
    if (frameSize.width > screenSize.width)
    {
      frameSize.width = screenSize.width;
    }
    frame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
    frame.setVisible(true);
  }

  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    try
    {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    new StakTekApplication();
  }
}
