@echo off
REM set PATH to %AGT5DX_JRE_HOME%\bin to find jre junk
REM set PATH to %AGT5DX_HOME%\bin to find nativeInt.dll
set PATH=%AGT5DX_JRE_HOME%\bin;%AGT5DX_HOME%\bin;%PATH%
REM classpath is set to agt5dx.jar to find the class files
REM classpath is set to config to pick up properties and gif files

echo #### The following environment variables must be set properly ####
echo  AGT5DX_JRE_HOME=%AGT5DX_JRE_HOME%
echo  AGT5DX_HOME=%AGT5DX_HOME%

REM show that java version being used
%AGT5DX_JRE_HOME%\bin\java -version

REM use this one normally
start %AGT5DX_JRE_HOME%\bin\javaw -DusePanelName=false -DAGT5DX_HOME=%AGT5DX_HOME% -DassertLogDir=%AGT5DX_HOME%\log -DAGT5DX_PANELS=%AGT5DX_PANELS% -DNDFROOT=%NDFROOT% -DRTFROOT=%RTFROOT% -DRESROOT=%RESROOT% -DTEMP_DIR=%TEMP% -classpath %AGT5DX_HOME%\bin\stakTek.jar;%AGT5DX_HOME%\bin\agt5dx82.jar;%AGT5DX_HOME%\bin\agt5dx.jar;%AGT5DX_HOME% StakTekApplication

REM uncomment the line below to see print statements
REM %AGT5DX_JRE_HOME%\bin\java -DusePanelName=false -DAGT5DX_HOME=%AGT5DX_HOME% -DassertLogDir=%AGT5DX_HOME%\log -DAGT5DX_PANELS=%AGT5DX_PANELS% -DNDFROOT=%NDFROOT% -DRTFROOT=%RTFROOT% -DRESROOT=%RESROOT% -DTEMP_DIR=%TEMP% -classpath "%AGT5DX_HOME%\bin\stakTek.jar;%AGT5DX_HOME%\bin\agt5dx82.jar;%AGT5DX_HOME%\bin\agt5dx.jar;%AGT5DX_HOME%" StakTekApplication



