import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.util.regex.*;
import java.io.*;

import com.agilent.mtd.agt5dx.datastore.*;
import com.agilent.mtd.agt5dx.gui.*;
import com.agilent.mtd.agt5dx.util.*;
import com.agilent.mtd.util.*;
import com.agilent.mtd.guiUtil.*;
import com.agilent.mtd.agt5dx.business.server.*;
import javax.swing.border.*;

/**
 * This is the main frame used for the StakTek program.
 * See StakTekApplication for an explanation of what
 * the program is for.
 * @author Bill Darbie
 */
public class StakTekFrame extends JFrame
{
  //  private static String _partialTraySuffix = "_partialTray";
  private static String _partialTraySuffix = "p";
  private static String[] _letters = {"A", "B", "C", "D", "E",
                                      "F", "G", "H", "I", "J",
                                      "K", "L"};
//   private static String[] _letters = {"A", "B", "C", "D", "E",
//                                       "F", "G", "H", "I", "J",
//                                       "K", "L", "M", "N", "O",
//                                       "P", "Q", "R", "S", "T",
//                                       "U", "V", "W", "X", "Y",
//                                      "Z" };
  private JPanel _contentPane;
  private JPanel _titlePanel = new JPanel();
  private JPanel _centerPanel = new JPanel();
  private JLabel _titleLabel = new JLabel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();
  private JButton _exitButton = new JButton();
  private JButton _compileButton = new JButton();
  private GridLayout gridLayout2 = new GridLayout();
  private FlowLayout flowLayout1 = new FlowLayout();
  private FlowLayout flowLayout2 = new FlowLayout();
  private JPanel _outerRowColPanel = new JPanel();
  private BorderLayout borderLayout1 = new BorderLayout();

  private String _panelName;
  private String _partialTrayPanelName;
  private String _panelNameHashed;
  private String _partialTrayPanelNameHashed;
  private String _emptyRowLetter;
  private String _emptyColNumStr;
  private int _emptyColNum;
  private java.util.List _boardNames = new ArrayList();
  private java.util.List _alignmentComponentSlots = new ArrayList();
  private String _serialNumber;
  private Map _serialNumRegExToPanelNameMap = new HashMap();
  private java.util.List _regExpressions = new ArrayList();
  private String _serialNumFile;
  private boolean _compileFailed = false;
  private JPanel _outerPanelNameEntryPanel = new JPanel();
  private JPanel _innerPanelNameEntryPanel = new JPanel();
  private GridLayout _gridLayout4 = new GridLayout();
  private JLabel _panelNameLabel = new JLabel();
  private JComboBox _panelNameComboBox = new JComboBox(FileName.getNdfPanelNames().toArray());
  private JTextField _serialNumberTextField = new JTextField();
  private JLabel _serialNumberLabel = new JLabel();
  private FlowLayout _flowLayout3 = new FlowLayout();
  private JPanel _innerRowColPanel = new JPanel();
  private JLabel _firtEmptyColLabel = new JLabel();
  private JComboBox _firstEmptyRowComboBox = new JComboBox(_letters);
  private JLabel _firstEmptyRowLabel = new JLabel();
  private JTextField _firstEmptyColTextField = new JTextField();
  private GridLayout _gridLayout1 = new GridLayout();
  private FlowLayout _flowLayout4 = new FlowLayout();
  private BorderLayout _borderLayout2 = new BorderLayout();
  private TitledBorder _titledBorder1;
  private TitledBorder _titledBorder2;
  private Border _border1;
  private WorkerThread _workerThread = new WorkerThread("worker thread");
  private GridLayout gridLayout1 = new GridLayout();
  private java.util.List _noModSlots = new ArrayList();

  /**
   * @author Bill Darbie
   */
  public StakTekFrame()
  {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try
    {
      jbInit();
      String usePanelNameStr = System.getProperty("usePanelName", "false");
      if (usePanelNameStr.equalsIgnoreCase("true") == false)
      {
        _innerPanelNameEntryPanel.remove(_panelNameComboBox);
        _innerPanelNameEntryPanel.remove(_panelNameLabel);
        _gridLayout4.setRows(1);
      }
      SwingUtils.setFont(this, FontUtil.getDefaultFont());
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void jbInit() throws Exception
  {
    _contentPane = (JPanel) this.getContentPane();
    _titledBorder2 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),2),"Select the  row and column for the first empty tray slot");
    _border1 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),2),"Select a panel");
    _gridLayout1.setColumns(2);
    _gridLayout1.setHgap(5);
    _gridLayout1.setRows(2);
    _gridLayout1.setVgap(10);
    _innerRowColPanel.setLayout(_gridLayout1);
    _firstEmptyRowLabel.setIconTextGap(4);
    _firstEmptyRowLabel.setText("First empty row:");
    _firtEmptyColLabel.setText("First empty column:");
    _contentPane.setLayout(borderLayout1);
    this.setSize(new Dimension(400, 300));
    this.setTitle("StakTek Partial Tray ");
    _titlePanel.setLayout(flowLayout2);
    _centerPanel.setLayout(_borderLayout2);
    _titleLabel.setFont(new java.awt.Font("Dialog", 1, 11));
    _titleLabel.setText("Enter the information below");
    _buttonPanel.setLayout(flowLayout1);
    _innerButtonPanel.setLayout(gridLayout2);
    _exitButton.setText("Exit");
    _exitButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _compileButton.setText("Create partial tray program");
    _compileButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        compileButton_actionPerformed(e);
      }
    });
    _outerRowColPanel.setLayout(_flowLayout4);
    _outerPanelNameEntryPanel.setLayout(_flowLayout3);
    _innerPanelNameEntryPanel.setLayout(_gridLayout4);
    _gridLayout4.setColumns(2);
    _gridLayout4.setHgap(5);
    _gridLayout4.setRows(2);
    _gridLayout4.setVgap(10);
    _panelNameLabel.setText("Panel name:");
    _serialNumberTextField.setText("");
    _serialNumberTextField.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        serialNumberTextField_keyReleased(e);
      }
    });
    _serialNumberLabel.setRequestFocusEnabled(true);
    _serialNumberLabel.setText("Part number:");
    _outerPanelNameEntryPanel.setBorder(_border1);
    _outerRowColPanel.setBorder(_titledBorder2);
    gridLayout2.setHgap(10);
    _flowLayout3.setAlignment(FlowLayout.LEFT);
    _flowLayout4.setAlignment(FlowLayout.LEFT);
    _borderLayout2.setHgap(0);
    _borderLayout2.setVgap(10);
    _centerPanel.add(_outerPanelNameEntryPanel, BorderLayout.NORTH);
    _centerPanel.add(_outerRowColPanel, BorderLayout.CENTER);
    _contentPane.add(_titlePanel, BorderLayout.NORTH);
    _titlePanel.add(_titleLabel, null);
    _contentPane.add(_centerPanel, BorderLayout.CENTER);
    _contentPane.add(_buttonPanel, BorderLayout.SOUTH);
    _buttonPanel.add(_innerButtonPanel, null);
    _innerButtonPanel.add(_compileButton, null);
    _innerButtonPanel.add(_exitButton, null);
    _outerPanelNameEntryPanel.add(_innerPanelNameEntryPanel, null);
    _innerPanelNameEntryPanel.add(_serialNumberLabel, null);
    _innerPanelNameEntryPanel.add(_serialNumberTextField, null);
    _innerPanelNameEntryPanel.add(_panelNameLabel, null);
    _innerPanelNameEntryPanel.add(_panelNameComboBox, null);
    _outerRowColPanel.add(_innerRowColPanel, null);
    _innerRowColPanel.add(_firstEmptyRowLabel, null);
    _innerRowColPanel.add(_firstEmptyRowComboBox, null);
    _innerRowColPanel.add(_firtEmptyColLabel, null);
    _innerRowColPanel.add(_firstEmptyColTextField, null);
  }

  /**
   * @author Bill Darbie
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      System.exit(0);
    }
  }

  /**
   * @author Bill Darbie
   */
  private void readInDataFromGui()
  {
    _panelName = (String)_panelNameComboBox.getSelectedItem();
    _serialNumber = (String)_serialNumberTextField.getText();
    _emptyRowLetter = (String)_firstEmptyRowComboBox.getSelectedItem();
    _emptyColNumStr = _firstEmptyColTextField.getText();
  }

  /**
   * @author Bill Darbie
   */
  private void readSerialNumFile() throws DatastoreException
  {
    _serialNumRegExToPanelNameMap.clear();

    String configDir = Directory.getLegacyConfigDir();
    // Filename.java does not have this so just brute force it
    _serialNumFile = configDir + File.separator + "serialNo.dat";

    FileReader fr = null;
    try
    {
      fr = new FileReader(_serialNumFile);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException ex = new CannotOpenFileDatastoreException(_serialNumFile);
      ex.initCause(fnfe);
      throw ex;
    }
    BufferedReader is = new BufferedReader(fr);

    // looking for something like "02?9* panelName"
    Pattern pattern = Pattern.compile("^\\s*([^#\\s]+)\\s+([^#\\s]+)");
    try
    {
      String line = null;
      do
      {
        try
        {
          line = is.readLine();
        }
        catch(IOException ioe)
        {
          DatastoreException ex = new CannotWriteDatastoreException(_serialNumFile);
          ex.initCause(ioe);
          throw ex;
        }

        if (line == null)
          continue;

        Matcher matcher = pattern.matcher(line);
        if (matcher.find())
        {
          // the serial number uses 2 wildcards:
          // * = 0 or more characters (reg expression: [^\s\n]*)
          // ? = 1 character          (reg expression: [^\s\n])
          String serialNum = matcher.group(1);
          String panelName = matcher.group(2);

          String serialNumRegEx = "^" + serialNum;
          serialNumRegEx = serialNumRegEx.replaceAll("\\*", "[^\\\\s\\\\n]*");
          serialNumRegEx = serialNumRegEx.replaceAll("\\?", "[^\\\\s\\\\n]");
          serialNumRegEx = serialNumRegEx + "$";
          System.out.println("mapping serial number regex \"" + serialNumRegEx + "\" to panel program: " + panelName);
          _regExpressions.add(serialNumRegEx);
          _serialNumRegExToPanelNameMap.put(serialNumRegEx, panelName);
        }
      } while(line != null);
    }
    finally
    {
      // close the panel.ndf file
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch(IOException ioe)
        {
          // do nothing
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private boolean findPanelNameFromSerialNumber() throws DatastoreException
  {
    readSerialNumFile();
    boolean match = false;

    // see if the serial number entered matches anything in the serial number file
    // iterate over the regular expressions in the same order they were listed in the file
    Iterator it = _regExpressions.iterator();
    while(it.hasNext())
    {
      String regEx = (String)it.next();
      Pattern pattern = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
      Matcher matcher = pattern.matcher(_serialNumber);
      if (matcher.find())
      {
        // we found a match, get the panel name
        _panelName = (String)_serialNumRegExToPanelNameMap.get(regEx);
        match = true;
        System.out.println("serial number lookup found panel name: " + _panelName);
        break;
      }
    }

    LocalizedString title = new LocalizedString("Data Entry Error", null);
    if (match == false)
    {
      LocalizedString message = new LocalizedString("The serial number did not match anything in the " + _serialNumFile + " file.", null);
      showError(message, title);
      return false;
    }

    // now check to see if the panel name is valid
    java.util.List panelNames = FileName.getNdfPanelNames();
    if (panelNames.contains(_panelName) == false)
    {
      LocalizedString message = new LocalizedString("The serial number matched a panel program name that does not exist.  The program name was " + _panelName + ".", null);
      showError(message, title);
      return false;
    }

    return true;
  }

  /**
   * @author Bill Darbie
   */
  private boolean verifyData() throws DatastoreException
  {
    Assert.expect(_emptyRowLetter.length() == 1);
    _emptyColNum = 0;

    LocalizedString title = new LocalizedString("Data Entry Error", null);
    // check that the first empty column is a postive non zero number
    try
    {
      _emptyColNum = StringUtil.convertStringToPositiveInt(_emptyColNumStr);
    }
    catch (BadFormatException bfe)
    {
      LocalizedString message = new LocalizedString("The first empty column must be a positive integer.", null);
      showError(message, title);
      return false;
    }

    // if the serial number was entered, look up which panel program should be used.
    if (_serialNumber.equals("") == false)
    {
      if (findPanelNameFromSerialNumber() == false)
        return false;
    }

    return true;
  }

  /**
   * @author Bill Darbie
   */
  private void createPartialTrayDirectory() throws DatastoreException
  {
    // figure out the correct names
    //    if (_panelName.endsWith(_partialTraySuffix))
    //    {
    //      _panelName = _panelName.substring(0, _panelName.length() - _partialTraySuffix.length());
    //    }
    _partialTrayPanelName = _panelName + _partialTraySuffix;
    _panelNameHashed = HashUtil.hashName(_panelName);
    _partialTrayPanelNameHashed = HashUtil.hashName(_partialTrayPanelName);

    // remove any existing partial ndf directory
    String partialTrayNdfDir = Directory.getLegacyNdfPanelDir(_partialTrayPanelName);
    if (FileUtil5dx.exists(partialTrayNdfDir))
    {
      System.out.println("removing previous partial tray ndf directory " + partialTrayNdfDir);
      FileUtil5dx.delete(partialTrayNdfDir);
    }

    // create a new one
    System.out.println("creating a new partial tray ndf directory " + partialTrayNdfDir);
    FileUtil5dx.mkdirs(partialTrayNdfDir);

    // remove any existing partial rtf directory
    String partialTrayRtfDir = Directory.getLegacyRtfPanelDir(_partialTrayPanelName);
    if (FileUtil5dx.exists(partialTrayRtfDir))
    {
      System.out.println("removing previous partial tray rtf directory " + partialTrayRtfDir);
      FileUtil5dx.delete(partialTrayRtfDir);
    }

    // create a new one
    System.out.println("creating a new partial tray rtf directory " + partialTrayRtfDir);
    FileUtil5dx.mkdirs(partialTrayRtfDir);
  }

  /**
   * @author Bill Darbie
   */
  private void copyNdfAndRtfFilesToThePartialTrayDirectory() throws DatastoreException
  {
    String origNdfPanelDir = Directory.getLegacyNdfPanelDir(_panelName);
    String partialTrayNdfPanelDir = Directory.getLegacyNdfPanelDir(_partialTrayPanelName);

    String origRtfPanelDir = Directory.getLegacyRtfPanelDir(_panelName);
    String partialTrayRtfPanelDir = Directory.getLegacyRtfPanelDir(_partialTrayPanelName);

    if (FileUtil5dx.exists(origNdfPanelDir))
    {
      System.out.println("copying contents of " + origNdfPanelDir + " to " + partialTrayNdfPanelDir);
      FileUtil5dx.copyDirectoryRecursivelyPreserveTimeStamp(origNdfPanelDir, partialTrayNdfPanelDir);
    }
    if (FileUtil5dx.exists(origRtfPanelDir))
    {
      System.out.println("copying contents of " + origRtfPanelDir + " to " + partialTrayRtfPanelDir);
      FileUtil5dx.copyDirectoryRecursivelyPreserveTimeStamp(origRtfPanelDir, partialTrayRtfPanelDir);
    }
  }

  /**
   * @author Bill Darbie
   */
  private void modifyPanelDirIdDatFile() throws DatastoreException
  {
    // change the panel name in dir_id.dat
    String ndfDir = Directory.getLegacyNdfPanelDir(_partialTrayPanelName);
    String dirIdFullPath = ndfDir + File.separator + FileName.getLegacyDirIdFileName();

    // delete the existing one
    if (FileUtil5dx.exists(dirIdFullPath))
    {
      System.out.println("deleting " + dirIdFullPath);
      FileUtil5dx.delete(dirIdFullPath);

      // create a new one
      FileWriter fw = null;
      PrintWriter pw = null;
      try
      {
        fw = new FileWriter(dirIdFullPath);

        BufferedWriter bw = new BufferedWriter(fw);
        pw = new PrintWriter(bw);
        pw.println(_partialTrayPanelName);
      }
      catch(IOException ioe)
      {
        DatastoreException ex = new CannotCreateFileDatastoreException(dirIdFullPath);
        ex.initCause(ioe);
        throw ex;
      }
      finally
      {
        if (pw != null)
          pw.close();
      }
    }
    System.out.println("created a new " + dirIdFullPath + " with the panel name " + _partialTrayPanelName);
  }

  /**
   * @author Bill Darbie
   */
  private void createPartialTrayProgram() throws DatastoreException
  {
    createPartialTrayDirectory();
    copyNdfAndRtfFilesToThePartialTrayDirectory();
    modifyPanelDirIdDatFile();
  }

  /**
   * @author Bill Darbie
   */
  private void findComponentsUsedForAlignment() throws DatastoreException
  {
    _alignmentComponentSlots.clear();

    Set alignmentComponentSet = new HashSet();
    // look for a line like: .ALIGNMENT_PADS: 1 regDesA 1 refDesB 2
    Pattern pattern = Pattern.compile("^\\s*\\.ALIGNMENT_PADS:\\s+\\d\\s+(.*)", Pattern.CASE_INSENSITIVE);

    Iterator it = _boardNames.iterator();
    while(it.hasNext())
    {
      String boardName = (String)it.next();
      String boardNdf = FileName.getLegacyBoardNdfFullPath(_partialTrayPanelName, boardName);

      FileReader fr = null;
      try
      {
        fr = new FileReader(boardNdf);
      }
      catch(FileNotFoundException fnfe)
      {
        DatastoreException ex = new CannotOpenFileDatastoreException(boardNdf);
        ex.initCause(fnfe);
        throw ex;
      }
      BufferedReader is = new BufferedReader(fr);

      try
      {
        String line = null;
        do
        {
          try
          {
            line = is.readLine();
          }
          catch(IOException ioe)
          {
            DatastoreException ex = new CannotWriteDatastoreException(boardNdf);
            ex.initCause(ioe);
            throw ex;
          }

          if (line == null)
            continue;

          Matcher matcher = pattern.matcher(line);
          if (matcher.find())
          {
            String refDesAndPinPairList = matcher.group(1);
            String[] strings = refDesAndPinPairList.split("\\s+");
            for(int i = 0; i < strings.length; ++i)
            {
              if ((i % 2) == 0)
              {
                String alignmentRefDes = strings[i];
                alignmentComponentSet.add(alignmentRefDes);
              }
            }
          }
        } while(line != null);
      }
      finally
      {
        // close the panel.ndf file
        if (is != null)
        {
          try
          {
            is.close();
          }
          catch(IOException ioe)
          {
            // do nothing
          }
        }
      }
    }

    Set rowColSet = new HashSet();
    Pattern rowColPattern = Pattern.compile("^([a-zA-Z][0-9]+)");
    it = alignmentComponentSet.iterator();
    while(it.hasNext())
    {
      String alignComponent = (String)it.next();
      System.out.println("found alignment component: " + alignComponent);

      // get the slot for this component
      Matcher matcher = rowColPattern.matcher(alignComponent);
      if (matcher.find())
      {
        String rowCol = matcher.group(1);
        rowColSet.add(rowCol);
      }
      else
      {
        Assert.expect(false);
      }
    }

    _alignmentComponentSlots.addAll(rowColSet);
    it = _alignmentComponentSlots.iterator();
    while(it.hasNext())
    {
      String rowCol = (String)it.next();
      System.out.println("identified row and column location that should always be loaded for alignment: " + rowCol);
    }
  }

  /**
   * @author Bill Darbie
   */
  private void findBoardNames() throws DatastoreException
  {
    _boardNames.clear();

    String panelNdf = FileName.getLegacyPanelNdfFullPath(_partialTrayPanelName);
    // open the panel.ndf file
    FileReader fr = null;
    try
    {
      fr = new FileReader(panelNdf);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException ex = new CannotOpenFileDatastoreException(panelNdf);
      ex.initCause(fnfe);
      throw ex;
    }
    BufferedReader is = new BufferedReader(fr);

    Pattern pattern = Pattern.compile("^\\s*@(\\w+)\\s");

    // parse to find all board names
    try
    {
      String line = null;
      do
      {
        try
        {
          line = is.readLine();
        }
        catch(IOException ioe)
        {
          DatastoreException ex = new CannotWriteDatastoreException(panelNdf);
          ex.initCause(ioe);
          throw ex;
        }
        if (line == null)
          continue;

        // look for @boardName
        Matcher matcher = pattern.matcher(line);
        if (matcher.find())
        {
          String boardName = matcher.group(1);
          _boardNames.add(boardName);
          System.out.println("found board " + boardName);
        }

      } while(line != null);
    }
    finally
    {
      // close the panel.ndf file
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch(IOException ioe)
        {
          // do nothing
        }
      }
    }

  }

  /**
   * @author Bill Darbie
   */
  private void readComponentsThatShouldNotBeModified() throws DatastoreException
  {
    _noModSlots.clear();
    String ndfDir = Directory.getLegacyNdfPanelDir(_partialTrayPanelName);
    String fileName = ndfDir + File.separator + "noModSlotsForPartialTray.dat";

    if (FileUtil5dx.exists(fileName) == false)
      System.out.println(fileName + " does not exist, will set all non-alignment components to no-test");
    else
    {
      FileReader fr = null;
      try
      {
        fr = new FileReader(fileName);
      }
      catch(FileNotFoundException fnfe)
      {
        DatastoreException ex = new CannotOpenFileDatastoreException(fileName);
        ex.initCause(fnfe);
        throw ex;
      }
      BufferedReader is = new BufferedReader(fr);

      Pattern pattern = Pattern.compile("^\\s*([a-zA-Z]\\d+)\\s*");
      try
      {
        Set noModSlotSet = new HashSet();
        String line = null;
        do
        {
          try
          {
            line = is.readLine();
          }
          catch(IOException ioe)
          {
            DatastoreException de = new DatastoreException(fileName);
            de.initCause(ioe);
            throw de;
          }
          if (line == null)
            continue;

          Matcher matcher = pattern.matcher(line);
          if (matcher.find())
          {
            String rowCol = matcher.group(1);
            noModSlotSet.add(rowCol);
          }
        } while (line != null);

        _noModSlots.addAll(noModSlotSet);
        Iterator it = _noModSlots.iterator();
        while(it.hasNext())
        {
          String rowCol = (String)it.next();
          System.out.println("components in slot " + rowCol + " will not be modified to no-test");
        }
      }
      finally
      {
        try
        {
          if (is != null)
            is.close();
        }
        catch(IOException ioe)
        {
          // do nothing
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private void modifyComponentNdfFileToFalseOutAppropriateComponents() throws DatastoreException
  {
    findBoardNames();

    // figure out which components are used as alignment points
    findComponentsUsedForAlignment();

    readComponentsThatShouldNotBeModified();

    // false out all components after the first empty tray EXCEPT the ones
    // used for alignment

    // for each board, make a copy of component.ndf to component.ndf.orig
    Iterator bit = _boardNames.iterator();
    while(bit.hasNext())
    {
      String sideBoardName = (String)bit.next();

      // we will write to a tempory file first, if everything works at the end
      // we copy the temporary file over the original
      String componentNdf = FileName.getLegacyComponentNdfFullPath(_partialTrayPanelName, sideBoardName);
      String tempComponentNdf = componentNdf + ".temp";
      if (FileUtil5dx.exists(tempComponentNdf))
        FileUtil5dx.delete(tempComponentNdf);
      FileUtil5dx.copy(componentNdf, tempComponentNdf);

      // open component.ndf for reading
      FileReader fr = null;
      try
      {
        fr = new FileReader(componentNdf);
      }
      catch(FileNotFoundException fnfe)
      {
        DatastoreException ex = new CannotOpenFileDatastoreException(componentNdf);
        ex.initCause(fnfe);
        throw ex;
      }
      BufferedReader is = new BufferedReader(fr);

      // open component.ndf.temp for writing
      FileWriter fw = null;
      try
      {
        fw = new FileWriter(tempComponentNdf);
      }
      catch(IOException ioe)
      {
        DatastoreException ex = new CannotCreateFileDatastoreException(tempComponentNdf);
        ex.initCause(ioe);
        throw ex;
      }
      BufferedWriter bw = new BufferedWriter(fw);
      PrintWriter os = new PrintWriter(bw);

      // look for a line that looks like @boardName
      Pattern refDesPattern = Pattern.compile("^\\s*@(\\w+)\\s");
      // look for a reference designator that is something like a1_something
      Pattern rowColPattern = Pattern.compile("^(([a-zA-Z])([0-9]+))");

      // now read each line of the temp file and write out the new line of the original file
      // with some lines being modified to set the correct components to no-test
      try
      {
        String line = null;
        do
        {
          try
          {
            line = is.readLine();
          }
          catch(IOException ioe)
          {
            DatastoreException de = new DatastoreException(tempComponentNdf);
            de.initCause(ioe);
            throw de;
          }
          if (line == null)
            continue;

          // determine if the line should be modified here
          Matcher matcher = refDesPattern.matcher(line);
          if (matcher.find())
          {
            String refDes = matcher.group(1);

            matcher = rowColPattern.matcher(refDes);
            if(matcher.find())
            {
              String rowColStr = matcher.group(1);
              String rowStr = matcher.group(2);
              String colStr = matcher.group(3);

              boolean modifyComponent = true;
              Iterator sit = _alignmentComponentSlots.iterator();
              while(sit.hasNext())
              {
                String alignCompRowCol = (String)sit.next();
                if (alignCompRowCol.equalsIgnoreCase(rowColStr))
                {
                  System.out.println("LEAVING alignment component unchanged: " + refDes);
                  modifyComponent = false;
                  break;
                }
              }
              if (modifyComponent)
              {
                sit = _noModSlots.iterator();
                while(sit.hasNext())
                {
                  String noModRowCol = (String)sit.next();
                  if(noModRowCol.equalsIgnoreCase(rowColStr))
                  {
                    System.out.println("LEAVING no mod component unchanged: " + refDes);
                    modifyComponent = false;
                    break;
                  }
                }
              }

              if (modifyComponent)
              {
                Assert.expect(rowStr.length() == 1);
                int rowNum = (int)rowStr.toUpperCase().charAt(0);
                int colNum = 0;
                try
                {
                  colNum = StringUtil.convertStringToInt(colStr);
                }
                catch(BadFormatException bfe)
                {
                  bfe.printStackTrace();
                  Assert.expect(false);
                }
                int emptyRowNum = (int)_emptyRowLetter.charAt(0);
                if((rowNum < emptyRowNum) ||
                   ((rowNum == emptyRowNum) && (colNum >= _emptyColNum)))
                {
                  // this component needs to be set to no-test
                  System.out.println("setting component " + refDes + " to no-test");
                  line = line.replaceFirst("[tT][rR][uU][eE]", "false");
                }
              }
            }
          }

          // then print out the line
          os.println(line);

        } while (line != null);
      }
      finally
      {
        try
        {
          if (is != null)
            is.close();
        }
        catch(IOException ioe)
        {
          // do nothing
        }
        if (os != null)
          os.close();
      }

      // everything worked fine so copy the temporary file over the original
      FileUtil5dx.copy(tempComponentNdf, componentNdf);
      FileUtil5dx.delete(tempComponentNdf);
    }
  }

  /**
   * @author Bill Darbie
   */
  private void compilePartialTrayProgram() throws DatastoreException
  {
    final ServerDosShell dosShell = ServerDosShell.getInstance();

    // running cadtran in silent mode. (-s)
//    String command = Directory.getLegacyMasterDir() + File.separator + "cadtran.exe" + " \"" + _partialTrayPanelName +
//                     "\" -s " + "-n" + Directory.getLegacyNdfDir() + " " + "-r" + Directory.getLegacyRtfDir();
    final String command = Directory.getLegacyMasterDir() + File.separator +
                     "cadtran.exe" + " " +
                     _partialTrayPanelName;
    System.out.println("compiling program " + _partialTrayPanelName);
    System.out.println(command);
    boolean passed = dosShell.sendCommand(command);
    System.out.println("finished compiling");
  }

  /**
   * @author Bill Darbie
   */
  void compileButton_actionPerformed(ActionEvent e)
  {
    disableButtonsAndCursor();

    readInDataFromGui();
    final StakTekFrame stakTekFrame = this;
    _workerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (verifyData())
          {
            createPartialTrayProgram();
//            compilePartialTrayProgram();
            modifyComponentNdfFileToFalseOutAppropriateComponents();
            compilePartialTrayProgram();
            enableButtonsAndCursor();
          }
          else
          {
            enableButtonsAndCursor();
          }
        }
        catch(DatastoreException de)
        {
          enableButtonsAndCursor();
          LocalizedString title = new LocalizedString("Program Error", null);
          LocalizedString message = de.getLocalizedString();
          stakTekFrame.showError(message, title);
        }
      }
    });
  }

  /**
   * @author Bill Darbie
   */
  private void disableButtonsAndCursor()
  {
    _compileButton.setEnabled(false);
//    _exitButton.setEnabled(false);
    _contentPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
  }

  /**
   * @author Bill Darbie
   */
  private void enableButtonsAndCursor()
  {
    _compileButton.setEnabled(true);
//    _exitButton.setEnabled(true);
    _contentPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
  }

  /**
   * @author Bill Darbie
   */
  private void showError(final LocalizedString message, final LocalizedString title)
  {
    final StakTekFrame stakTekFrame = this;
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        MessageDialog.showErrorDialog(stakTekFrame, message, title, true);
      }
    });
  }

  /**
   * @author Bill Darbie
   */
  void cancelButton_actionPerformed(ActionEvent e)
  {
    System.exit(0);
  }

  /**
   * @author Bill Darbie
   */
  void serialNumberTextField_keyReleased(KeyEvent e)
  {
    if (_serialNumberTextField.getText().length() > 0)
      _panelNameComboBox.setEnabled(false);
    else
      _panelNameComboBox.setEnabled(true);
  }
}
